#  ______________________________________________
# |                                              | #
# |               func_convert.ps1               | #
# |______________________________________________| #


# ========================= #
#          CONVERT          #
#
#	DESCRIPTION : converting content for all APPs.


# ConvertTo-Digit.
#	DESCRIPTION : display a number with a specific digit.
#	RETURN : string.
function ConvertTo-Digit {
	Param (
		[int]$NUMBER,
		[int]$DIGIT
	)
	$RESULT = "{0:d$DIGIT}" -f [int]$NUMBER;
	return $RESULT;
}


# Convert-TextWithSpace.
#	DESCRIPTION : display a string with space to complete a specific length.
#	RETURN : string.
function Convert-TextWithSpace {
	Param (
		[Parameter(Mandatory=$true)][string]$TEXT,
		[Parameter(Mandatory=$true)][int]$PAD
	)
	$LENGTH = $TEXT.Length;
	If ($LENGTH -lt $PAD) { $TEXT = $TEXT.PadRight($PAD) }
	return $TEXT;
}


# ConvertTo-UTF8BOM.
#	DESCRIPTION : workaround function for correctly convert files in UTF8 with BOOM.
#	RETURN : x
function ConvertTo-UTF8BOM {
	Param (
		[Parameter(Mandatory=$true)][string]$PATH,
		[Parameter(Mandatory=$true)][string]$FILTER
	)
	Get-ChildItem "$PATH" -Filter "$FILTER" | Foreach-Object {
		$Local:CURRENT = (Split-Path -Path $_ -Leaf)
		$Local:NEW = "_" + $CURRENT;
		If (Test-Path -Path "${PATH}\${NEW}") { Remove-Item -Path "${PATH}\${NEW}" -Force; }
		Get-Content -Path "${PATH}\${CURRENT}" -Encoding UTF8 | Out-File -Encoding UTF8 "$PATH\${NEW}";
		Remove-Item -Path "${PATH}\${CURRENT}" -Force;
		Rename-Item -Path "${PATH}\${NEW}" -NewName "${PATH}\${CURRENT}" -Force;
	}
}



# next added content date : 20240810_012450.
#  _________________________________________
# |                                         | #
# |               func_os.ps1               | #
# |_________________________________________| #


# ==================== #
#          OS          #
#
#	DESCRIPTION : interacting with OS for all APPs.


# Get-Hostname.
#	DESCRIPTION : get hostname device.
#	RETURN : string.
function Get-Hostname { return $env:COMPUTERNAME; }


# Get-OSName.


# Get-OSVersion.


# Get-OSMode.


# Get-OSInterface.
#	DESCRIPTION : get interface device.
#	RETURN : string.
function Get-OSInterface {
	# test : server OR desktop.
	If ((Get-CimInstance Win32_OperatingSystem).ProductType -gt 1) { return "server"; } Else { return "desktop"; }
}


# Get-OSLanguage.
#	DESCRIPTION : get current language.
#	RETURN : string.
function Get-OSLanguage { return $(Get-WinSystemLocale).Name; }



# next added content date : 20240810_012450.
#  ___________________________________________
# |                                           | #
# |               func_pkgs.ps1               | #
# |___________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Verify-Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Verify-Software {
	Param (
		[Parameter(Mandatory=$True)][string]$NAME,
		[Parameter(Mandatory=$True)][string]$TYPE,
		[string]$ACTION,
		[bool]$DEPEND
	)
	# Tests : type software.
	If ($TYPE -eq "module") {
		If (Get-Module | Where-Object {$_.Name -eq $NAME}) {
			[string]$RESULT = "loaded";
		} Else {
			# Test : action.
			If ($ACTION -eq "load") {
				# Test : result after call.
				If (!(Load-Module -NAME "$NAME" -DEPEND $DEPEND)) {
					[string]$RESULT = "fail-load";
				} Else {
					[string]$RESULT = $(Load-Module -NAME "$NAME" -DEPEND $DEPEND);
				}
			} Else {
				# tmp default.
				[string]$RESULT = "fail-action";
			}
		}
	} ElseIf ($TYPE -eq "command") {
		# tmp default.
		[string]$RESULT = "fail-command";
	} ElseIf ($TYPE -eq "service") {
		# tmp default.
		[string]$RESULT = "fail-service";
	} Else {
		# tmp default.
		[string]$RESULT = "fail-unknow";
	}
	return $RESULT;
}


# Load-Module.
#	DESCRIPTION : test a module with load, import and install parameters.
#	RETURN : string or boolean.
#   NOTE : Import or Install module commands can return error code even if module is imported or installed.
function Load-Module {
	Param (
		[Parameter(Mandatory=$True)][string]$NAME,
		[bool]$DEPEND
	)
	# Test : availability.
	If (Get-Module -ListAvailable | Where-Object {$_.Name -eq $NAME}) {
		# Test : importability.
		Import-Module $NAME -Force | Out-Null;
		If (Get-Module -ListAvailable | Where-Object {$_.Name -eq $NAME}) {
			[string]$RESULT = "imported";
		} Else {
			If ($DEPEND -eq $True) { [string]$RESULT = "fail-import"; } Else { [string]$RESULT = "skip-import"; }
		}
	} Else {
		# Test : installability.
		If (Find-Module -Name $NAME -ErrorAction "SilentlyContinue" | Where-Object {$_.Name -eq $NAME}) {
			# Test : installed.
			Install-Module -Name $NAME -Force -Confirm:$False | Out-Null;
			Import-Module $NAME -Force | Out-Null;
			If (Get-Module -ListAvailable | Where-Object {$_.Name -eq $NAME}) {
				[string]$RESULT = "installed";
			} Else {
				If ($DEPEND -eq $True) { [string]$RESULT = "fail-install"; } Else { [string]$RESULT = "skip-install"; }
			}
		} Else {
			If ($DEPEND -eq $True) { [bool]$RESULT = $False; } Else { [string]$RESULT = "skip-available"; }
		}
	}
	return $RESULT;
}


# Start-Service.



# next added content date : 20240810_012450.




# next added content date : 20240810_012450.
#  ______________________________________________
# |                                              | #
# |               func_generic.ps1               | #
# |______________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : everything generic for all APPs.


# Stop-App.
#	DESCRIPTION : end program.
#	RETURN : x
function Stop-App {
	Param (
		[int]$CODE
	)
	Break;
}



# next added content date : 20240810_012450.
#  _________________________________________
# |                                         | #
# |               func_fs.ps1               | #
# |_________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interacting for all APPs.


# Test-Dir.
#	DESCRIPTION : check directory.
#	RETURN : bool.
function Test-Dir {
	Param (
		[Parameter(Mandatory=$true)][string]$PATH,
		[bool]$CREATE,
		[bool]$QUIET
	)
	If (Test-Path -Path "$PATH") {
		[bool]$RESULT = $True;
	} Else {
		If ($CREATE -eq $True) { New-Item -ItemType "directory" -Path "$PATH" -Force | Out-Null; }
		[bool]$RESULT = $False;
	}
	If ($QUIET -ne $True) { return $RESULT; }
}


# Test-File.
#	DESCRIPTION : check file.
#	RETURN : bool.
function Test-File {
	Param (
		[string]$PATH,
		[bool]$CREATE,
		[string]$CONTENT,
		[bool]$OVERWRITE,
		[bool]$QUIET
	)
	If (Test-Path -Path "$PATH") {
		[bool]$RESULT = $True;
	} Else {
		If ($CREATE -eq $True) { New-Item -ItemType "file" -Path "$PATH" -Force | Out-Null; }
		[bool]$RESULT = $False;
	}
	If ((-not([string]::IsNullOrWhiteSpace($CONTENT))) -And ($OVERWRITE -eq $True)) {
		Set-Content -Path "$PATH" -Value "$CONTENT" | Out-Null;
	}
	If ($QUIET -ne $True) { return $RESULT; }
}



# next added content date : 20240810_012450.
#  ___________________________________________
# |                                           | #
# |               func_user.ps1               | #
# |___________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test-Root.
#	DESCRIPTION : check if you are root / local administrator.
#	RETURN : int.
function Test-Root { [Security.Principal.WindowsIdentity]::GetCurrent().Groups -contains 'S-1-5-32-544'; }


# Get-UserCurrent.


# Get-UserLogged.
#	DESCRIPTION : get hostname device.
#	RETURN : string or bool.
function Get-UserLogged {
	Param (
		[string]$COMPUTER
	)
	$SESSION = quser /server:$COMPUTER;
	If ($SESSION) {
		[string]$RESULT = ((($SESSION) -Split '\n' | Select-Object -Skip 1 ) -Replace '\s+', ';' -Split ';')[1];
	} Else {
		[bool]$RESULT = $False;
	}
	return $RESULT;
}


# Test-UserLogged.
