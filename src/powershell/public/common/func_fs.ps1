#  _________________________________________
# |                                         | #
# |               func_fs.ps1               | #
# |_________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interacting for all APPs.


# Test-Dir.
#	DESCRIPTION : check directory.
#	RETURN : bool.
function Test-Dir {
	Param (
		[Parameter(Mandatory=$true)][string]$PATH,
		[bool]$CREATE,
		[bool]$QUIET
	)
	If (Test-Path -Path "$PATH") {
		[bool]$RESULT = $True;
	} Else {
		If ($CREATE -eq $True) { New-Item -ItemType "directory" -Path "$PATH" -Force | Out-Null; }
		[bool]$RESULT = $False;
	}
	If ($QUIET -ne $True) { return $RESULT; }
}


# Test-File.
#	DESCRIPTION : check file.
#	RETURN : bool.
function Test-File {
	Param (
		[string]$PATH,
		[bool]$CREATE,
		[string]$CONTENT,
		[bool]$OVERWRITE,
		[bool]$QUIET
	)
	If (Test-Path -Path "$PATH") {
		[bool]$RESULT = $True;
	} Else {
		If ($CREATE -eq $True) { New-Item -ItemType "file" -Path "$PATH" -Force | Out-Null; }
		[bool]$RESULT = $False;
	}
	If ((-not([string]::IsNullOrWhiteSpace($CONTENT))) -And ($OVERWRITE -eq $True)) {
		Set-Content -Path "$PATH" -Value "$CONTENT" | Out-Null;
	}
	If ($QUIET -ne $True) { return $RESULT; }
}