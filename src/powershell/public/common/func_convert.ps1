#  ______________________________________________
# |                                              | #
# |               func_convert.ps1               | #
# |______________________________________________| #


# ========================= #
#          CONVERT          #
#
#	DESCRIPTION : converting content for all APPs.


# ConvertTo-Digit.
#	DESCRIPTION : display a number with a specific digit.
#	RETURN : string.
function ConvertTo-Digit {
	Param (
		[int]$NUMBER,
		[int]$DIGIT
	)
	$RESULT = "{0:d$DIGIT}" -f [int]$NUMBER;
	return $RESULT;
}


# Convert-TextWithSpace.
#	DESCRIPTION : display a string with space to complete a specific length.
#	RETURN : string.
function Convert-TextWithSpace {
	Param (
		[Parameter(Mandatory=$true)][string]$TEXT,
		[Parameter(Mandatory=$true)][int]$PAD
	)
	$LENGTH = $TEXT.Length;
	If ($LENGTH -lt $PAD) { $TEXT = $TEXT.PadRight($PAD) }
	return $TEXT;
}


# ConvertTo-UTF8BOM.
#	DESCRIPTION : workaround function for correctly convert files in UTF8 with BOOM.
#	RETURN : x
function ConvertTo-UTF8BOM {
	Param (
		[Parameter(Mandatory=$true)][string]$PATH,
		[Parameter(Mandatory=$true)][string]$FILTER
	)
	Get-ChildItem "$PATH" -Filter "$FILTER" | Foreach-Object {
		$Local:CURRENT = (Split-Path -Path $_ -Leaf)
		$Local:NEW = "_" + $CURRENT;
		If (Test-Path -Path "${PATH}\${NEW}") { Remove-Item -Path "${PATH}\${NEW}" -Force; }
		Get-Content -Path "${PATH}\${CURRENT}" -Encoding UTF8 | Out-File -Encoding UTF8 "$PATH\${NEW}";
		Remove-Item -Path "${PATH}\${CURRENT}" -Force;
		Rename-Item -Path "${PATH}\${NEW}" -NewName "${PATH}\${CURRENT}" -Force;
	}
}
