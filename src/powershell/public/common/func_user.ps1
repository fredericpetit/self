#  ___________________________________________
# |                                           | #
# |               func_user.ps1               | #
# |___________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test-Root.
#	DESCRIPTION : check if you are root / local administrator.
#	RETURN : int.
function Test-Root { [Security.Principal.WindowsIdentity]::GetCurrent().Groups -contains 'S-1-5-32-544'; }


# Get-UserCurrent.


# Get-UserLogged.
#	DESCRIPTION : get hostname device.
#	RETURN : string or bool.
function Get-UserLogged {
	Param (
		[string]$COMPUTER
	)
	$SESSION = quser /server:$COMPUTER;
	If ($SESSION) {
		[string]$RESULT = ((($SESSION) -Split '\n' | Select-Object -Skip 1 ) -Replace '\s+', ';' -Split ';')[1];
	} Else {
		[bool]$RESULT = $False;
	}
	return $RESULT;
}


# Test-UserLogged.