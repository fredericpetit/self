#  ___________________________________________
# |                                           | #
# |               func_pkgs.ps1               | #
# |___________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Verify-Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Verify-Software {
	Param (
		[Parameter(Mandatory=$True)][string]$NAME,
		[Parameter(Mandatory=$True)][string]$TYPE,
		[string]$ACTION,
		[bool]$DEPEND
	)
	# Tests : type software.
	If ($TYPE -eq "module") {
		If (Get-Module | Where-Object {$_.Name -eq $NAME}) {
			[string]$RESULT = "loaded";
		} Else {
			# Test : action.
			If ($ACTION -eq "load") {
				# Test : result after call.
				If (!(Load-Module -NAME "$NAME" -DEPEND $DEPEND)) {
					[string]$RESULT = "fail-load";
				} Else {
					[string]$RESULT = $(Load-Module -NAME "$NAME" -DEPEND $DEPEND);
				}
			} Else {
				# tmp default.
				[string]$RESULT = "fail-action";
			}
		}
	} ElseIf ($TYPE -eq "command") {
		# tmp default.
		[string]$RESULT = "fail-command";
	} ElseIf ($TYPE -eq "service") {
		# tmp default.
		[string]$RESULT = "fail-service";
	} Else {
		# tmp default.
		[string]$RESULT = "fail-unknow";
	}
	return $RESULT;
}


# Load-Module.
#	DESCRIPTION : test a module with load, import and install parameters.
#	RETURN : string or boolean.
#   NOTE : Import or Install module commands can return error code even if module is imported or installed.
function Load-Module {
	Param (
		[Parameter(Mandatory=$True)][string]$NAME,
		[bool]$DEPEND
	)
	# Test : availability.
	If (Get-Module -ListAvailable | Where-Object {$_.Name -eq $NAME}) {
		# Test : importability.
		Import-Module $NAME -Force | Out-Null;
		If (Get-Module -ListAvailable | Where-Object {$_.Name -eq $NAME}) {
			[string]$RESULT = "imported";
		} Else {
			If ($DEPEND -eq $True) { [string]$RESULT = "fail-import"; } Else { [string]$RESULT = "skip-import"; }
		}
	} Else {
		# Test : installability.
		If (Find-Module -Name $NAME -ErrorAction "SilentlyContinue" | Where-Object {$_.Name -eq $NAME}) {
			# Test : installed.
			Install-Module -Name $NAME -Force -Confirm:$False | Out-Null;
			Import-Module $NAME -Force | Out-Null;
			If (Get-Module -ListAvailable | Where-Object {$_.Name -eq $NAME}) {
				[string]$RESULT = "installed";
			} Else {
				If ($DEPEND -eq $True) { [string]$RESULT = "fail-install"; } Else { [string]$RESULT = "skip-install"; }
			}
		} Else {
			If ($DEPEND -eq $True) { [bool]$RESULT = $False; } Else { [string]$RESULT = "skip-available"; }
		}
	}
	return $RESULT;
}


# Start-Service.