#  ______________________________________________
# |                                              | #
# |               func_generic.ps1               | #
# |______________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : everything generic for all APPs.


# Stop-App.
#	DESCRIPTION : end program.
#	RETURN : x
function Stop-App {
	Param (
		[int]$CODE
	)
	Break;
}