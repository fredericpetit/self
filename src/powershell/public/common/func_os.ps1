#  _________________________________________
# |                                         | #
# |               func_os.ps1               | #
# |_________________________________________| #


# ==================== #
#          OS          #
#
#	DESCRIPTION : interacting with OS for all APPs.


# Get-Hostname.
#	DESCRIPTION : get hostname device.
#	RETURN : string.
function Get-Hostname { return $env:COMPUTERNAME; }


# Get-OSName.


# Get-OSVersion.


# Get-OSMode.


# Get-OSInterface.
#	DESCRIPTION : get interface device.
#	RETURN : string.
function Get-OSInterface {
	# test : server OR desktop.
	If ((Get-CimInstance Win32_OperatingSystem).ProductType -gt 1) { return "server"; } Else { return "desktop"; }
}


# Get-OSLanguage.
#	DESCRIPTION : get current language.
#	RETURN : string.
function Get-OSLanguage { return $(Get-WinSystemLocale).Name; }