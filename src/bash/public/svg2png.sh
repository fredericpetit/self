#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               svg2png.sh               | #
# |________________________________________| #


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -A APP;
APP["NAME"]="svg2png";
APP["SHORTNAME"]="S2P";
APP["DESCRIPTION"]="Convert SVG files to PNG format with predefined tools.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENT"]="program inkscape#command imagemagick";
APP["PARAM"]="PATH TOOL SIZE";
APP["PARAM_DSC"]="Path where perform the changes (puting files in one or multiple folders). | default is '${PWD}'.#Choice between some tools. | default is 'inkscape'.#Size for the converted file. | no default.";
APP["OPTION"]="QUIET HELP VERSION";
APP["OPTION_DSC"]="Be silent. | default is 'false'.#Show man.#Show version.";
APP["EXAMPLE"]="${APP[NAME]} -PATH \"/home/adminlocal/files\" -TOOL \"inkscape\" -SIZE \"16x16\" -QUIET";
APP["TODO"]="x";
APP["NOTE"]="x";
APP["CREDIT"]="x";
APP["CHANGELOG"]="x";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="0.0.1";
APP["LICENSE"]="GPL-3.0-or-later";


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

declare -A CONF;
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
CONF["SEPARATOR"]="/";
CONF["IFS"]=$IFS;
# Test & Loop : includes.
if [[ "${0##*.}" == "sh" ]]; then
	for var_file in "${CONF[PATH]}${CONF[SEPARATOR]}common${CONF[SEPARATOR]}"func_*.sh; do source "$var_file"; done;
fi;
# Dates.
CONF["DATE_START"]="$(date "+%d/%m/%Y - %Hh%M")";
CONF["DATE_START_EXEC"]="$(date "+%Y-%m-%d %H:%M:%S")";
CONF["DATE_END"]="";
CONF["DATE_FILE"]="$(date "+%Y%m%d_%H%M%S")";


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -A PARAM;
# Loop : arguments.
while [[ $# -gt 0 ]]; do
	case "$1" in
		-PATH)
			PARAM["PATH"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-TOOL)
			PARAM["TOOL"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-SIZE)
			PARAM["SIZE"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-QUIET)
			PARAM["QUIET"]=true
			shift
			;;
		-HELP)
			Stop_App -CODE 0 -HELP
			shift
			;;
		-VERSION|-v)
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			shift
			;;
		*)
			Stop_App -CODE 1 -TEXT "unknown parameter : '$1'." -HELP
			shift
			;;
	esac
done;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -A DEFAULT;
DEFAULT["PATH"]="${PWD}";
DEFAULT["TOOL"]="inkscape";
DEFAULT["QUIET"]=false;
# Some script definitions.
if [ -z "${PARAM[PATH]}" ]; then PARAM["PATH"]="${DEFAULT[PATH]}"; fi;
if [ -z "${PARAM[TOOL]}" ]; then PARAM["TOOL"]="${DEFAULT[TOOL]}"; fi;
if [ -n  "${PARAM[SIZE]}" ]; then 
	if [[ "${PARAM[SIZE]}" == *x* ]]; then PARAM["SIZE_W"]="${PARAM[SIZE]%x*}"; PARAM["SIZE_H"]="${PARAM[SIZE]#*x}"; fi;
fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;
declare -a PARAM_SOFTWARE=("inkscape" "imagemagick");
if ! Search_InArray -SEARCH "${PARAM[TOOL]}" -ARRAY "${PARAM_SOFTWARE[*]}"; then Stop_App -CODE 1 -TEXT "unknown tool." -HELP; fi;


# =========================== #
#          FUNCTIONS          #

# ConvertTo_PNG.
#	DESCRIPTION : convert by choosing tool.
#	RETURN : string.
function ConvertTo_PNG() {
	# Some definition.
	local var_result;
	local var_file;
	local var_current_folder;
	local var_current_file;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-FILE)
				var_file="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done
	var_current_folder=$(dirname "$var_file");
	var_current_file=$(basename "$var_file" | sed 's/\.[^.]*$//');
	Test_Dir -PATH "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}" -CREATE;
	# Tests : tool.
	if [[ $(Test_Software -NAME "${PARAM[TOOL]}" -TYPE "command" -DEPEND) == "available" ]]; then
		if [[ ${PARAM[TOOL]} == "inkscape" ]]; then
			if [ -n "${PARAM[SIZE]}" ]; then
				Test_Dir -PATH "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}${CONF[SEPARATOR]}${PARAM[SIZE_W]}x${PARAM[SIZE_H]}" -CREATE;
				inkscape "$var_file" -o "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}${CONF[SEPARATOR]}${PARAM[SIZE_W]}x${PARAM[SIZE_H]}${CONF[SEPARATOR]}${var_current_file}.png" -w "${PARAM[SIZE_W]}" -h "${PARAM[SIZE_H]}" &> /dev/null;
				if Test_File -PATH "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}${CONF[SEPARATOR]}${PARAM[SIZE_W]}x${PARAM[SIZE_H]}${CONF[SEPARATOR]}${var_current_file}.png"; then
					var_result="success";
				else
					var_result="fail-tool";
				fi;
			else
				inkscape "$var_file" -o "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}${CONF[SEPARATOR]}${var_current_file}.png" &> /dev/null;
				if Test_File -PATH "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}${CONF[SEPARATOR]}${var_current_file}.png"; then
					var_result="success";
				else
					var_result="fail-tool";
				fi;
			fi;
		elif [[ ${PARAM[TOOL]} == "imagemagick" ]]; then
			convert "$var_file" "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}${CONF[SEPARATOR]}${var_current_file}.png" &> /dev/null;
			if Test_File -PATH "${var_current_folder}${CONF[SEPARATOR]}${APP[NAME]}${CONF[SEPARATOR]}${var_current_file}.png"; then
				var_result="success";
			else
				var_result="fail-tool";
			fi;
		fi;
	else
		var_result="fail-software";
	fi;
	echo $var_result;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_convert;
	local var_read;
	local var_read_name;
	# Test : content exists.
	if ! Test_DirPopulated -PATH "${PARAM[PATH]}" -EXTENSION ".svg"; then
		Stop_App -CODE 1 -TEXT "dir not populated.";
	else
		# Loop : SVG.
		find "${PARAM[PATH]}" -type f -name "*.svg" | while read -r var_read; do
			var_convert="$(ConvertTo_PNG -FILE "$var_read")";
			var_read_name=$(basename "$var_read");
			if [[ "${PARAM[QUIET]}" != true ]]; then Write_Script -TEXT "check file '${var_read_name}'." -CHECK; fi;
			if [[ "$var_convert" != "success" ]]; then
				if [[ "${PARAM[QUIET]}" != true ]]; then  Write_Script -TEXT "'${var_convert}' (${PARAM[TOOL]})." -CODE 1 -CHECKED; fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then Write_Script -TEXT "'${var_read_name}' file converted." -CODE 0 -CHECKED; fi;
			fi;
		done;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;