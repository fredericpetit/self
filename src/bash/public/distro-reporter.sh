#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_os.sh               | #
# |________________________________________| #


# ==================== #
#          OS          #
#
#	DESCRIPTION : interacting with OS for all APPs.


function Set_Placeholder5() {
	echo "placeholder";
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_network.sh               | #
# |_____________________________________________| #


# ========================= #
#          NETWORK          #
#
#	DESCRIPTION : network state for all APPs.

# Get_FirstNetwork.
#	DESCRIPTION : get name of the first network interface.
#	RETURN : string or code.
function Get_FirstNetwork() {
	# Some definition.
	local var_result;
	local var_interfaces;
	var_result="fail-network";
	var_interfaces=$(ip -o link show | awk -F': ' '{print $2}');
	# Loop : interfaces.
	for interface in $var_interfaces; do
		if [[ $interface != "lo" ]]; then var_result="$interface"; break; fi;
	done
	echo "$var_result";
}


# Get_Distant.
#	DESCRIPTION : get distant resource.
#	RETURN : string or code.
#	TO-DO : git auth.
function Get_Distant() {
	# Some definition.
	local var_method;
	local var_type;
	local var_url;
	local var_path;
	local var_credential;
	local var_username;
	local var_password;
	local var_overwrite;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-METHOD)
				var_method="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Some default.
	if [[ -z "$var_method" ]]; then var_method="wget"; fi;
	if [[ -z "$var_type" ]]; then var_type="file"; fi;
	# Test : auth.
	if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
		var_username=$(echo -e "$var_credential" | cut -d ':' -f 1)
		var_password=$(echo -e "$var_credential" | cut -d ':' -f 2)
	fi;
	# Tests : method.
	if [[ "$var_method" == "wget" ]]; then
		# Tests : file, string, ping.
		if [[ "$var_type" == "file" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		elif [[ "$var_type" == "string" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				fi;
			fi;
			# Test : good content.
			if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
		elif [[ "$var_type" == "ping" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		fi;
	elif [[ "$var_method" == "curl" ]]; then
		# Test : auth.
		if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
			if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
				if curl -s "$var_url" -u "${var_username}:${var_password}" | grep browser_download_url | cut -d '"' -f 4 | wget --no-config --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			else
				if curl -s "$var_url" -u "${var_username}:${var_password}" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			fi;
		else
			if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
				if curl -s "$var_url" | grep browser_download_url | cut -d '"' -f 4 | wget --no-config --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			else
				if curl -s "$var_url" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			fi;
		fi;
	elif [[ "$var_method" == "git" ]]; then
		if [[ "$var_overwrite" = true ]]; then
			if git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; then return 0; else return 1; fi;
		else
			git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; return 0;
		fi;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_fs.sh               | #
# |________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interaction for all APPs.

# Restore_IFS.
#	DESCRIPTION : restore IFS.
function Restore_IFS() { IFS=${CONF["IFS"]}; }


# Test_Dir.
#	DESCRIPTION : check directory.
#	RETURN : code.
function Test_Dir() {
	# Some definition.
	local var_path;
	local var_create;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : folder.
	if [[ -n "$var_path" ]]; then
		if [[ "$var_create" = true ]]; then mkdir -p "$var_path" &> /dev/null; fi;
		if [ -d "$var_path" ]; then
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod -R "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown -R "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else return 1; fi;
	else return 1; fi;
}


# Test_DirPopulated().
#	DESCRIPTION : check if folder haz content in it.
#	RETURN : code.
function Test_DirPopulated() {
	# Some definition.
	local var_path;
	local var_extension;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-EXTENSION)
				var_extension="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	if [[ -n "$(ls -A "$var_path")" ]]; then
		if [[ -n "$var_extension" ]]; then
			if find "$var_path" -type f -name "*$var_extension" &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		else
			return 0;
		fi;
	else
		return 1;
	fi;
}


# Test_File.
#	DESCRIPTION : check file.
#	RETURN : code.
function Test_File() {
	# Some definition.
	local var_path;
	local var_overwrite;
	local var_create;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		if [ -f "$var_path" ]; then
			if [[ "$var_overwrite" = true ]]; then
				rm "$var_path" &> /dev/null;
				Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				touch "$var_path" &> /dev/null;
			fi;
		else
			if [[ "$var_overwrite" = true ]] || [[ "$var_create" = true ]]; then
				Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				touch "$var_path" &> /dev/null;
			fi;
		fi;
		if [ -f "$var_path" ]; then
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else return 1; fi;
	else return 1; fi;
}


# Set_File.
#	DESCRIPTION : create file.
#	RETURN : code.
function Set_File() {
	# Some definition.
	local var_path;
	local var_raw;
	local var_source;
	local var_append;
	local var_encode;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-RAW)
				var_raw="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-APPEND)
				var_append=true
				shift
				;;
			-ENCODE)
				var_encode="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		touch "$var_path" &> /dev/null;
		# Tests : echo, cp.
		if [[ -n "$var_raw" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				echo "$var_raw" | tee -a "$var_path" > /dev/null;
			else
				# Test : base64.
				if [[ -n "$var_encode" ]]; then
					echo "$var_raw" | base64 -d > "$var_path";
				else
					echo "$var_raw" | tee "$var_path" > /dev/null;
				fi;
			fi;
		elif [[ -n "$var_source" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				cat "$var_source" >> "$var_path";
			else
				cp "$var_path" "$var_source" &> /dev/null;
			fi;
		else
			return 1;
		fi;
	else return 1; fi;
}


# Find_FileLine.
#	DESCRIPTION : find line started by pattern in a file.
#	RETURN : string or code.
function Find_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file;
	local var_line;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE)
				var_file="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file"; then
		var_line=$(grep "$var_pattern" "$var_file" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line" ]]; then echo "$var_line"; else return 1; fi;
	else
		return 1;
	fi;
}


# Compare_FileLine.
#	DESCRIPTION : search 1st line started by pattern in 1st file, compare it to 2nd file.
#	RETURN : string or code.
function Compare_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file1;
	local var_file2;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1" && Test_File -PATH "$var_file2"; then
		var_line1=$(grep "^$var_pattern" "$var_file1" | head -n 1);
		var_line2=$(grep "^$var_pattern" "$var_file2" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line1" && "$var_line1" != false && "$var_line1" != "null" && \
			-n "$var_line2" && "$var_line2" != false && "$var_line2" != "null" ]]; then
			# Test : return file number 2 content for different content, return false for same content.
			if [[ "$var_line1" != "$var_line2" ]]; then echo "$var_line2"; else return 1; fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Backup_FileLine.
#	DESCRIPTION : recover line in 1st file from a 2nd file.
#	RETURN : string or code.
#	TODO : verify character | escaping.
function Backup_FileLine() {
	# Some definition.
	local var_pattern;
	local var_default;
	local var_file1;
	local var_file2;
	local var_diff;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DEFAULT)
				var_default="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1"; then
		var_diff=$(Compare_FileLine -PATTERN "$var_pattern" -FILE1 "$var_file1" -FILE2 "$var_file2");
		var_pattern="^${var_pattern}";
		# Tests : replace by old content for different content, replace by default for same/empty content.
		if [[ -n "$var_diff" && "$var_diff" != false ]]; then
			sed -i "s|${var_pattern}|${var_diff}|" "$var_file1";
			echo "old";
		elif [[ -n "$var_default" ]]; then
			echo -e "$var_default" >> "$var_file1";
			echo "default";
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_user.sh               | #
# |__________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test-Root.
#	DESCRIPTION : check root/sudo.
#	RETURN : int.
function Test-Root {
	if [[ "$(id -u)" -eq 0 ]]; then
		return 0;
	elif groups | grep -qE '\bsudo\b|\bwheel\b'; then
		return 0;
	else
		return 1;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
# shellcheck disable=SC2329
# shellcheck disable=SC2034
#  _____________________________________________
# |                                             | #
# |               func_generic.sh               | #
# |_____________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : everything generic for all APPs.


# DATE.
declare -A DATE;
DATE["END"]="";
# y-m-d : jekyll file name markdown.
DATE["CURRENT_DATE"]="$(date "+%Y-%m-%d")";
# d-m-y - h:m:s : jekyll inside markdown file.
DATE["FULL_TIMESTAMP"]="$(date "+%Y-%m-%d %H:%M:%S")";
DATE["COMPACT_TIMESTAMP"]="$(date "+%Y%m%d_%H%M%S")";
DATE["FORMATTED_DATE_TIME"]="$(date "+%d/%m/%Y - %Hh%M")";


# COLOR.
declare -A COLOR;
COLOR[BOLD]='\e[1m';
COLOR[RED_LIGHT]='\e[1;38;5;160m';
COLOR[GREEN_LIGHT]='\e[1;38;5;34m';
COLOR[GREEN_DARK]='\e[1;38;5;28m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;179m';
COLOR[YELLOW_DARK]='\e[1;38;5;178m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;208m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';
COLOR[STOP]='\e[0m';
COLOR[TICK]="[${COLOR[GREEN_LIGHT]}✔${COLOR[STOP]}]";
COLOR[CROSS]="[${COLOR[RED_LIGHT]}✘${COLOR[STOP]}]";


# Write_Script.
#	DESCRIPTION : custom output of the APP.
#	RETURN : string.
function Write_Script() {
	# Some definition.
	local var_text;
	local var_code;
	local var_check;
	local var_checked;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-CODE)
				var_code="$2"
				shift 2
				;;
			-CHECK)
				var_check=true
				shift
				;;
			-CHECKED)
				var_checked=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then 
		# Tests : state, change former line.
		if [ -n "$var_check" ]; then
			echo -ne "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		elif [ -n "$var_checked" ]; then
			if [[ "$var_code" -eq 1 ]]; then
				echo -e "\\r ${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[CROSS]} ERROR : $var_text";
			elif [[ "$var_code" -eq 0 ]]; then
				echo -e "\\r ${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[TICK]} $var_text";
			fi;
		else
			echo -e "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		fi;
	fi;
}


# Get_Help.
#	DESCRIPTION : usage of the APP.
#	RETURN : string.
function Get_Help() {
	# Some definition.
	# requirement.
	local var_requirement;
	local var_requirement_text;
	IFS='#' read -ra var_requirement <<< "${APP[REQUIREMENT]}";
	for element in "${var_requirement[@]}"; do
		var_requirement_text+="$(echo -e "\t${element}\n ")";
	done;
	# param.
	local var_param;
	local var_param_text;
	IFS=' ' read -ra var_param <<< "${APP[PARAM]}";
	for element in "${var_param[@]}"; do var_param_text+="$element "; done;
	local var_param_dsc;
	local var_param_dsc_count;
	local var_param_dsc_text;
	IFS='#' read -ra var_param_dsc <<< "${APP[PARAM_DSC]}";
	local var_param_dsc_count=0
	for element in "${var_param_dsc[@]}"; do
		var_param_dsc_text+="$(echo -e "\t-${var_param[$var_param_dsc_count]} [ value ]\n\t\t${element}\n ")";
		var_param_dsc_count="$((var_param_dsc_count+1))";
	done;
	# option.
	local var_option;
	local var_option_text;
	IFS=' ' read -ra var_option <<< "${APP[OPTION]}";
	for element in "${var_option[@]}"; do var_option_text+="$element "; done;
	local var_option_dsc;
	local var_option_dsc_count;
	local var_option_dsc_text;
	IFS='#' read -ra var_option_dsc <<< "${APP[OPTION_DSC]}";
	local var_option_dsc_count=0
	for element in "${var_option_dsc[@]}"; do
		var_option_dsc_text+="$(echo -e "\t-${var_option[$var_option_dsc_count]}\n\t\t${element}\n ")";
		var_option_dsc_count="$((var_option_dsc_count+1))";
	done;
	# note.
	local var_note;
	local var_note_text;
	IFS='#' read -ra var_note <<< "${APP[NOTE]}";
	for element in "${var_note[@]}"; do
		var_note_text+="$(echo -e "\t${element}\n ")";
	done;
	# changelog.
	local var_changelog;
	local var_changelog_text;
	IFS='#' read -ra var_changelog <<< "${APP[CHANGELOG]}";
	for element in "${var_changelog[@]}"; do
		var_changelog_text+="$(echo -e "\t${element}\n ")";
	done;
	Restore_IFS;
cat <<EOUSAGE

SYNOPSIS
	${APP[NAME]} - ${APP[DESCRIPTION]}

HOME
	${APP[URL]}

REQUIREMENT
$var_requirement_text
UTILISATION
	${APP[NAME]} PARAM [ $var_param_text] OPTION [ $var_option_text]

PARAM
$var_param_dsc_text
OPTION
$var_option_dsc_text
EXAMPLE
	${APP[EXAMPLE]}

TO-DO
	${APP[TODO]}

NOTE
$var_note_text
CREDIT & THANKS
	${APP[CREDIT]}

CHANGELOG
$var_changelog_text
METADATA
	Author : ${APP[AUTHOR]}
	Version : ${APP[VERSION]}
	License : ${APP[LICENSE]}

EOUSAGE
}


# Test_Param.
#	DESCRIPTION : test arguments.
#	RETURN : string.
function Test_Param() {
	# Some definition.
	local var_param;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PARAM)
				var_param="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests :param mispelled, empty.
	if [[ "$var_param" == "-"* ]]; then
		Stop_App  -CODE 1 -TEXT "bad parameter (cant' starting by '-').";
	elif [[ -z "$var_param" ]]; then
		Stop_App  -CODE 1 -TEXT "bad parameter (cant' be empty).";
	fi;
}


# Get_StartingApp.
#	DESCRIPTION : script starter pack.
#	RETURN : string.
function Get_StartingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Title.
		echo -e "${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: ${CONF[DATE_START]} ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
		# Place & soft env.
		echo -e "- work in '${CONF[PATH]}'.";
		if [ -n "$SNAP" ]; then
			echo -e "- Snap environment detected.";
		elif [ -n "$FLATPAK_ID" ]; then
			echo -e "- Flatpak environment detected.";
		fi;
		echo "";
	fi;
	# Test : requirement.
	if [[ -n ${APP[REQUIREMENT]} && ${APP[REQUIREMENT]} != "x" ]]; then
		# Some definition.
		local var_element;
		local var_type;
		local var_name;
		local var_dependencie;
		local var_software;
		IFS='#';
		# Loop : requirement.
		for var_element in ${APP[REQUIREMENT]}; do
			# Space delimiter.
			var_type=$(echo "$var_element" | cut -d ' ' -f 1);
			var_name=$(echo "$var_element" | cut -d ' ' -f 2);
			var_dependencie=$(echo "$var_element" | cut -d ' ' -f 3);
			var_software=$(Test_Software -NAME "$var_name" -TYPE "$var_type");
			# Test : software.
			if [[ "$var_software" == "available" ]]; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- requirement '${var_type}' ${var_name} available.";
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- requirement '${var_type}' ${var_name} not available (error '${var_software}').";
				fi;
				# Test : dependencie.
				if [[ "$var_dependencie" == "mandatory" ]]; then
					Stop_App  -CODE 1 -TEXT "'${var_type}' ${var_name} mandatory.";
				fi;
			fi;
		done;
		Restore_IFS;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Output (data backup).
	if [[ "${PARAM[BACKUP]}" == true ]]; then
		# Test : output date name mode, output custom name mode (two files).
		if [[ "${PARAM[OUTPUT]}" =~ ${DEFAULT[OUTPUT]}\.(csv|xls|xlsx|json|txt)$ ]]; then
			# Test : get previous output old.
			if [[ -n "${PARAM[OUTPUT_BACKUP]}" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) in '${PARAM[OUTPUT_BACKUP]}'.";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (date name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (date name mode).";
				fi;
			fi;
		else
			# Test : move previous output to old.
			if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT]}"; then
					mv "${PARAM[OUTPUT]}" "${PARAM[OUTPUT_BACKUP]}";
					# Test : move all file formats.
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.json"; then
						mv "${PARAM[OUTPUT]%.csv}.json" "${PARAM[OUTPUT_BACKUP]%.csv}.json";
					fi;
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) moved to '${PARAM[OUTPUT_BACKUP]}'.";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (custom name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (custom name mode).";
				fi;
			fi;
		fi;
		if [[ "${PARAM[QUIET]}" != true && -z "${PARAM[OUTPUT]}" ]]; then echo ""; fi;
	fi;
	# Output (main).
	if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" && "${PARAM[RAW]}" != true ]]; then
		if Test_File -PATH "${PARAM[OUTPUT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current output to '${PARAM[OUTPUT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "output can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Report.
	if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]]; then
		if Test_File -PATH "${PARAM[REPORT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- report to '${PARAM[REPORT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "report can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
}


# Get_EndingApp.
#	DESCRIPTION : script ending pack.
#	RETURN : string.
function Get_EndingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Some definition.
		local var_date_start;
		local var_date_end;
		local var_date_end_seconds;
		var_date_start=$(date -d "$(date "+%Y-%m-%d %H:%M:%S")" "+%s");
		var_date_end=$(date "+%s");
		var_date_end_seconds=$((var_date_end - var_date_start));
		echo -e "\n${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: executed in ${var_date_end_seconds} seconds ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
	fi;
}


# Stop_App.
#	DESCRIPTION : end program.
#	RETURN : string and code.
function Stop_App() {
	# Some definition.
	local var_code;
	local var_text;
	local var_quiet;
	local var_help;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CODE)
				var_code="$2"
				shift 2
				;;
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-QUIET)
				var_quiet=true
				shift
				;;
			-HELP)
				var_help=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : splash error.
	if [[ "$var_quiet" != true && -n "$var_text" ]]; then
		Write_Script -TEXT "EXIT ERROR ! ${var_text}";
	fi;
	# Test : splash man.
	if [[ "$var_help" == true ]]; then Get_Help; fi;
	wait;
	exit "$var_code";
}


# Debug_Start.
#	DESCRIPTION : debug app.
function Debug_Start() { set -x; }


# Debug_Stop.
#	DESCRIPTION : debug app.
function Debug_Stop() { set +x; }


# Add_Book.
#	DESCRIPTION : construct associative array.
function Add_Book() {
	# Some definition.
	local var_name="$1";
	# Test : book exists.
	if ! declare -p "$var_name" &>/dev/null; then declare -gA "$var_name"; fi;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		local var_mode;
		local var_current_index;
		local var_current_release;
		local var_archive_index;
		local var_archive_release;
		local var_element_suffixes;
		local var_element_suffixes_var;
		local var_array;
		local var_array_keys;
		local var_array_suffixes;
		# mode, indexes & releases.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release");
		# Loop : var_suffix key.
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			var_element_suffixes_var="var_${var_element_suffixes}";
			declare "${var_element_suffixes_var}"="${var_element}_${var_element_suffixes}";
		done;
		# Construct index & release values from Get_Distribution.
		IFS=',' read -ra var_array <<< "$var_data";
		Restore_IFS;
		declare -a var_array_keys=("$var_mode" "$var_current_index" "$var_current_release" "$var_archive_index" "$var_archive_release");
		# Loop var_suffix value.
		for i in "${!var_array_keys[@]}"; do
			eval "${var_name}[\"${var_array_keys[$i]}\"]=\"${var_array[$i]}\"";
		done;
	elif [[ "$var_name" == "TMP" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		eval "${var_name}[\"${var_element}\"]=\"${var_data}\"";
	fi;
}


# Get_Book.
#	DESCRIPTION : get associative array.
#	RETURN : string or code.
function Get_Book() {
	# Some definition.
	local var_name;
	local var_element;
	local var_data;
	local var_array_suffixes;
	local var_element_suffixes;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-ELEMENT)
				var_element="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Loop: var_suffix key.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release")
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			if [[ "$var_data" == "$var_element_suffixes" ]]; then var_result="${DISTRIBUTION[${var_element}_$var_element_suffixes]}"; break; fi;
		done;
	fi;
	# Test : return.
	if [[ -n "$var_result" && "$var_result" != "" ]]; then echo "$var_result"; else return 1; fi;
}


# Remove_Book.
#	DESCRIPTION : remove associative array.
function Remove_Book() {
	# Some definition.
	local var_name="$1";
	unset "$var_name";
}


# Remove_DuplicateInString.
#	DESCRIPTION : remove duplicate value in string.
#	RETURN : string.
function Remove_DuplicateInString() {
	# Some definition.
	local var_string;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	echo "$var_string" | tr ' ' '\n' | sort | uniq | xargs;
}


# Search_InArray.
#	DESCRIPTION : search if value exists in array.
#	RETURN : code.
function Search_InArray() {
	# Some definition.
	local var_search;
	local var_array;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-SEARCH)
				var_search="$2"
				shift 2
				;;
			-ARRAY)
				var_array="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : need all param.
	if [ -z "$var_search" ] || [ -z "$var_array" ]; then return 1; fi;
	# IFS for delimit array.
	IFS=" " read -ra var_array_read <<< "$var_array";
	Restore_IFS;
	# Loop : search in array.
	for element in "${var_array_read[@]}"; do
		if [ "$element" == "$var_search" ]; then return 0; fi;
	done;
	return 1;
}


# Measure_MaxInArray.
#	DESCRIPTION : calculate longest word in array.
#	RETURN : string.
function Measure_MaxInArray() {
	# Some definition.
	local var_count;
	local var_array;
	local var_word;
	local var_length;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		var_count=0;
		for element in "${!var_array[@]}"; do
			var_word="${var_array[$element]}";
			var_length=${#var_word};
			if [ "$var_length" -gt "$var_count" ]; then var_count="$var_length"; fi;
		done;
		echo "$var_count";
	fi;
}


# Set_RepeatCharacter.
#	DESCRIPTION : calculate how many same character repeat needed.
#	RETURN : string.
function Set_RepeatCharacter() {
	# Some definition.
	local var_character;
	local var_count;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-CHARACTER)
				var_character="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : if arg empty.
	if [ -z "$var_character" ] || [ -z "$var_count" ]; then
		return 1;
	else
		var_result="";
		for ((i=0; i < "$var_count"; i++)); do
			var_result+="$var_character";
		done;
	fi;
	echo "$var_result";
}


# Limit_StringLength.
#	DESCRIPTION : limit number of character in a string.
#	RETURN : string.
#	TODO : replace -DIRECTION by -POSITION.
function Limit_StringLength() {
	# Some definition.
	local var_string;
	local var_count;
	local var_count_before;
	local var_count_after;
	local var_direction;
	local var_spacer;
	local var_spacer_length;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			-DIRECTION)
				var_direction="$2"
				shift 2
				;;
			-SPACER)
				var_spacer="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some defaults.
	if [ -z "$var_direction" ]; then var_direction="after"; fi;
	if [ -z "$var_spacer" ]; then var_spacer="(...)"; fi;
	var_spacer_length=${#var_spacer};
	# Test : if arg empty.
	if [ -z "$var_string" ] || [ -z "$var_count" ]; then
		return 1;
	else
		# Test : cut needed.
		if [ ${#var_string} -gt "$var_count" ]; then
			# Add spacer length to cut count.
			var_count_before=$(( ${#var_string}-var_count+var_spacer_length+1 ));
			var_count_after=$(( var_count-var_spacer_length ));
			# Tests : sub place.
			if [ "$var_direction" = "before" ]; then
				var_result+="$var_spacer";
				var_result+=$(echo "$var_string" | cut -c "$var_count_before"-"${#var_string}");
			elif [ "$var_direction" = "after" ]; then
				var_result+=$(echo "$var_string" | cut -c 1-"$var_count_after");
				var_result+="$var_spacer";
			fi;
			echo "$var_result";
		else
			echo "$var_string";
		fi;
	fi;
}


# Get_TableHeader.
#	DESCRIPTION : display a table index.
#	RETURN : string.
function Get_TableHeader() {
	# Some definition.
	local var_array;
	local var_element;
	local var_param;
	local var_param_length;
	local var_dot_length;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a name param and the max character count a value of this param can be in the rows.
		for element_header in "${!var_array[@]}"; do
			var_element="${var_array[$element_header]}";
			var_param=$(echo "$var_element" | cut -d ":" -f1);
			var_param_length="${#var_param}";
			var_dot_length=$(echo "$var_element" | cut -d ":" -f2);
			# Calculate how many characters to add more.
			var_param_length_more=$(( var_dot_length-var_param_length ));
			var_dot_length_more=$(( var_param_length-var_dot_length ));
			# Ready to construct header.
			var_line1+="$var_param";
			var_line1+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_param_length_more");
			var_line1+="  ";
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length");
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length_more");
			var_line2+="  ";
		done;
		echo -e "$var_line1";
		echo -e "$var_line2";
	fi;
}


# Get_TableRow.
#	DESCRIPTION : display a table row.
#	RETURN : string.
function Get_TableRow() {
	# Some definition.
	local var_array;
	local var_action;
	local var_element;
	local var_value;
	local var_value_length;
	local var_max;
	local var_line;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			-ACTION)
				var_action="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a value, the max character count this value can be.
		for element_row in "${!var_array[@]}"; do
			var_element="${var_array[$element_row]}";
			var_value=$(echo "$var_element" | cut -d ":" -f1);
			var_max=$(echo "$var_element" | cut -d ":" -f2);
			# Test : tick cross.
			if [[ "$var_value" == "tick" || "$var_value" == "cross" ]]; then
				if [[ "$var_value" == "tick" ]]; then
					var_value="${COLOR[TICK]}";
				elif [[ "$var_value" == "cross" ]]; then
					var_value="${COLOR[CROSS]}";
				fi;
				var_value_length=3;
			# TODO
			# elif [[ "$var_value" == *"tick"* || "$var_value" == *"cross"* ]]; then
			else
				var_value_length="${#var_value}";
			fi;
			# Calculate how many space to add more.
			var_value_length_more=$(( var_max-var_value_length ));
			# Ready to construct row.
			var_line+="$var_value";
			var_line+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_value_length_more");
			var_line+="  ";
		done;
		# Test : replace line.
		if [[ "$var_action" == "check" ]]; then
			echo -ne "$var_line";
		else
			echo -e "\\r$var_line";
		fi;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_pkgs.sh               | #
# |__________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Test_Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Test_Software() {
	# Some definition.
	local var_result;
	local var_name;
	local var_type;
	#local var_action; // SC2034 disable.
	#local var_depend; // SC2034 disable.
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-ACTION)
				#var_action="$2" // SC2034 disable.
				shift 2
				;;
			-DEPEND)
				#var_depend=true // SC2034 disable.
				shift
				;;
			*)
				shift
				;;
		esac
	done
	# Tests : type software.
	if [[ "$var_type" == "program" ]]; then
		# TMP : import OverDeploy routine.
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-program";
		fi;
	elif [[ "$var_type" == "service" ]]; then
		# Test : systemd check.
		# SRC : https://stackoverflow.com/a/53640320/2704550.
		if [[ $(systemctl list-units --all --type service --full --no-legend "${var_name}.service" | sed 's/^\s*//g' | cut -f1 -d' ') == "${var_name}.service" ]]; then
			var_result="available";
		else
			var_result="fail-service";
		fi;
	elif [[ "$var_type" == "command" ]]; then
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-command";
		fi;
	else
		# tmp default.
		var_result="fail-unknow";
	fi;
	echo "$var_result";
}


# Start_Software.
#	DESCRIPTION : test effective restarting of a service.
#	RETURN : code.
function Start_Software() {
	# Some definition.
	local var_name;
	local var_type;
	local var_command;
	local var_user;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-COMMAND)
				var_command="$2"
				shift 2
				;;
			-USER)
				var_user="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done
	# Test : daemon, service.
	if [[ "$var_type" == "daemon" ]]; then
		if $var_command &> /dev/null; then return 0; else return 1; fi;
	elif [[ "$var_type" == "service" ]]; then
		su -l "$var_user" -c "systemctl restart ${var_name}";
		# Test : service state.
		if su -l "$var_user" -c "systemctl is-active --quiet ${var_name}"; then return 0; else return 1; fi;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_convert.sh               | #
# |_____________________________________________| #


# ========================= #
#          CONVERT          #
#
#	DESCRIPTION : converting content for all APPs.


function Set_Placeholder1() {
	echo "placeholder";
}



# shellcheck source=/dev/null
# shellcheck disable=SC2329
# shellcheck disable=SC2034
#  ________________________________________________
# |                                                | #
# |               distro-reporter.sh               | #
# |________________________________________________| #


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -A APP;
APP["NAME"]="distro-reporter";
APP["SHORTNAME"]="DR";
APP["DESCRIPTION"]="Analyze Distro Tracker report output and publish updates to Facebook, Jekyll, & Wordpress.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENT"]="program wget mandatory#program jq mandatory#program curl mandatory";
APP["PARAM"]="SOURCE DESTINATION FB_ID FB_TOKEN OUTPUT INCREMENT";
APP["PARAM_DSC"]="Local filepath or distant url to JSON data. | no default.#Choice between \"facebook\", \"jekyll\" or \"wordpress\". | no default.#Facebook page ID. | no default.#Facebook user token. | no default.#For file output, folder path to store the final result, without last slash. | default is '${PWD}/'.#Increment security for keeping and don't remove previous datas. New and old datas need to be in the same directory. | default is 'true'.";
APP["OPTION"]="RAW QUIET HELP VERSION";
APP["OPTION_DSC"]="Be silent. | default is 'false'.#Show man.#Show version.";
APP["EXAMPLE"]="${APP[NAME]} -DESTINATION \"jekyll\" -QUIET";
APP["TODO"]="x";
APP["NOTE"]="x";
APP["CREDIT"]="x";
APP["CHANGELOG"]="0.0.1 - Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="0.0.1";
APP["LICENSE"]="GPL-3.0-or-later";


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

declare -A CONF;
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
CONF["SEPARATOR"]="/";
CONF["IFS"]=$IFS;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -A PARAM;
# Loop : arguments.
while [[ $# -gt 0 ]]; do
	case "$1" in
		-SOURCE|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-DESTINATION|-d)
			PARAM["DESTINATION"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-FB_ID)
			PARAM["FB_ID"]="https://graph.facebook.com/${2}/feed"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-FB_TOKEN)
			PARAM["FB_TOKEN"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-OUTPUT|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-INCREMENT|-i)
			PARAM["INCREMENT"]=true
			shift
			;;
		-QUIET|-q)
			PARAM["QUIET"]=true
			shift
			;;
		-HELP|-h)
			Stop_App -CODE 0 -HELP
			shift
			;;
		-VERSION|-v)
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			shift
			;;
		*)
			Stop_App -CODE 1 -TEXT "unknown parameter : '$1'." -HELP
			shift
			;;
	esac
done;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -A DEFAULT;
DEFAULT["OUTPUT"]="${PWD}${CONF[SEPARATOR]}";
DEFAULT["INCREMENT"]=true;
DEFAULT["QUIET"]=false;
# Some script definitions.
if [ -z "${PARAM[SOURCE]}" ]; then Stop_App -CODE 1 -TEXT "need '-SOURCE' parameter." -HELP; fi;
if [ -z "${PARAM[DESTINATION]}" ]; then Stop_App -CODE 1 -TEXT "need '-DESTINATION' parameter." -HELP; fi;
if [[ "${PARAM[DESTINATION]}" == "facebook" ]] && [[ -z "${PARAM[FB_ID]}" || -z "${PARAM[FB_TOKEN]}" ]]; then Stop_App -CODE 1 -TEXT "need ID & token parameters for 'Facebook' destination." -HELP; fi;
if [ -z "${PARAM[INCREMENT]}" ]; then PARAM["INCREMENT"]="${DEFAULT[INCREMENT]}"; fi;
if [[ -z "${PARAM[OUTPUT]}" && "${PARAM[INCREMENT]}" == false && "${PARAM[DESTINATION]}" == "jekyll" ]]; then
	# Jekyll default with no backup security.
	PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${DATE[CURRENT_DATE]}-report.md";
elif [[ -n "${PARAM[OUTPUT]}" && "${PARAM[INCREMENT]}" == false && "${PARAM[DESTINATION]}" == "jekyll" ]]; then
	# Jekyll custom with no backup security.
	PARAM["OUTPUT"]="${PARAM[OUTPUT]%/}${CONF[SEPARATOR]}${DATE[CURRENT_DATE]}-report.md";
elif [[ -z "${PARAM[OUTPUT]}" && "${PARAM[INCREMENT]}" == true && "${PARAM[DESTINATION]}" == "jekyll" ]]; then
	# Jekyll default with backup security.
	# Loop : last file win previous data state.
	for var_param_output_single_file in "${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[CURRENT_DATE]}-"*-report.md; do
		var_param_output_last_file="$var_param_output_single_file";
	done;
	var_param_output_last_file_id="${var_param_output_last_file#*-*-*-}";
	var_param_output_last_file_id="${var_param_output_last_file_id%-report.md}";
	# Test : several reports for the day.
	if [[ -n "$var_param_output_last_file_id" && "$var_param_output_last_file_id" -ge 0 ]]; then
		var_param_output_last_file_id=$((var_param_output_last_file_id + 1));
		PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[CURRENT_DATE]}-${var_param_output_last_file_id}-report.md";
	else
		PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[CURRENT_DATE]}-0-report.md";
	fi;
elif [[ -n "${PARAM[OUTPUT]}" && "${PARAM[INCREMENT]}" == true && "${PARAM[DESTINATION]}" == "jekyll" ]]; then
	# Jekyll custom with backup security.
	# Loop : last file win previous data state.
	for var_param_output_single_file in "${PARAM[OUTPUT]%/}${CONF[SEPARATOR]}${DATE[CURRENT_DATE]}-"*-report.md; do
		var_param_output_last_file="$var_param_output_single_file";
	done;
	var_param_output_last_file_id="${var_param_output_last_file#*-*-*-}";
	var_param_output_last_file_id="${var_param_output_last_file_id%-report.md}";
	# Test : several reports for the day.
	if [[ -n "$var_param_output_last_file_id" && "$var_param_output_last_file_id" -ge 0 ]]; then
		var_param_output_last_file_id=$((var_param_output_last_file_id + 1));
		PARAM["OUTPUT"]="${PARAM[OUTPUT]%/}${CONF[SEPARATOR]}${DATE[CURRENT_DATE]}-${var_param_output_last_file_id}-report.md";
	else
		PARAM["OUTPUT"]="${PARAM[OUTPUT]%/}${CONF[SEPARATOR]}${DATE[CURRENT_DATE]}-0-report.md";
	fi;
fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Publish_Report.
#	DESCRIPTION : use Facebook API, construct markdown content for Jekyll, or use Wordpress API.
function Publish_Report() {
	# Some definition.
	local var_content;
	local var_array;
	local var_data;
	local var_name;
	local var_release_new;
	local var_release_old;
	local var_source_new;
	local var_source_old;
	local var_post_fb;
	local var_response_fb;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-CONTENT)
				var_content="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : Facebook API, Jekyll Markdown file.
	if [[ "${PARAM[DESTINATION]}" == "facebook" ]]; then
		var_post_fb+="Report - ${DATE[FULL_TIMESTAMP]} +0200\n\n";
		# Loop : distribution.
		var_array=$(echo "$var_content" | jq -c -r '.distro[]');
		for var_data in $var_array; do
			var_name=$(echo "$var_data" | jq -c -r '.name');
			var_release_new=$(echo "$var_data" | jq -c -r '.version.new');
			var_release_old=$(echo "$var_data" | jq -c -r '.version.old');
			var_source_new=$(echo "$var_data" | jq -c -r '.source.new');
			var_source_old=$(echo "$var_data" | jq -c -r '.source.old');
			# Set file.
			if [[ "$var_release_new" != "null" && "$var_release_old" != "null" && "$var_source_new" != "null" && "$var_source_old" != "null" ]]; then
				var_post_fb+="distribution ${var_name^^} :\n- last release from '${var_release_old}' to '${var_release_new}'\n- source from '${var_source_old}' to '${var_source_new}'\n\n";
			elif [[ "$var_release_new" != "null" && "$var_release_old" != "null" ]]; then
				var_post_fb+="distribution ${var_name^^} :\n- last release from '${var_release_old}' to '${var_release_new}'\n\n";
			elif [[ "$var_source_new" != "null" && "$var_source_old" != "null" ]]; then
				var_post_fb+="distribution ${var_name^^} :\n- source from '${var_source_old}' to '${var_source_new}'\n\n";
			fi;
			Write_Script -TEXT "report for distribution '${var_name}'.";
		done;
		#var_response_fb=$(curl -X POST -F "message=${var_post_fb}" -F "access_token=${PARAM[FB_TOKEN]}" "${PARAM[FB_ID]}");
		var_response_fb=$(curl -X POST "${PARAM[FB_ID]}?access_token=${PARAM[FB_TOKEN]}" -H "Content-Type: application/json" -d "{\"message\":\"${var_post_fb}\"}");

		Write_Script -TEXT "API response : '${var_response_fb}'";
	elif [[ "${PARAM[DESTINATION]}" == "jekyll" ]]; then
		# Set file.
		echo -e "---\ntitle: \"Report ${DATE[CURRENT_DATE]}\"\n\nlayout: post\ncategories: changes\ndate: ${DATE[FULL_TIMESTAMP]} +0200\n\nauthor: reporter\n---\n\n" > "${PARAM[OUTPUT]}";
		# Loop : distribution.
		var_array=$(echo "$var_content" | jq -c -r '.distro[]');
		for var_data in $var_array; do
			var_name=$(echo "$var_data" | jq -c -r '.name');
			var_release_new=$(echo "$var_data" | jq -c -r '.version.new');
			var_release_old=$(echo "$var_data" | jq -c -r '.version.old');
			var_source_new=$(echo "$var_data" | jq -c -r '.source.new');
			var_source_old=$(echo "$var_data" | jq -c -r '.source.old');
			# Set file.
			if [[ "$var_release_new" != "null" && "$var_release_old" != "null" && "$var_source_new" != "null" && "$var_source_old" != "null" ]]; then
				echo -e "distribution ${var_name} :\n- last release from '${var_release_old}' to '${var_release_new}'\n- source from '${var_source_old}' to '${var_source_new}'\n\n">> "${PARAM[OUTPUT]}";
			elif [[ "$var_release_new" != "null" && "$var_release_old" != "null" ]]; then
				echo -e "distribution ${var_name} :\n- last release from '${var_release_old}' to '${var_release_new}'\n\n">> "${PARAM[OUTPUT]}";
			elif [[ "$var_source_new" != "null" && "$var_source_old" != "null" ]]; then
				echo -e "distribution ${var_name} :\n- source from '${var_source_old}' to '${var_source_new}'\n\n">> "${PARAM[OUTPUT]}";
			fi;
			Write_Script -TEXT "report for distribution '${var_name}'.";
		done;
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_content;
	local var_content_length;
	# Test : custom data.
	if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]] || Test_File -PATH "${PARAM[SOURCE]}"; then
		# Test : content exists.
		if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]]; then
			var_content="$(Get_Distant -URL "${PARAM[SOURCE]}" -TYPE "string")";
		else
			var_content="$(<"${PARAM[SOURCE]}")";
		fi;
		var_content_length="$(echo "$var_content" | jq -r ".distro | length")";
		# Test : content valid.
		if [[ "$var_content_length" -gt 0 ]]; then
			# Test : software.
			if [[ "${PARAM[DESTINATION]}" =~ ^(facebook|jekyll|wordpress)$ ]]; then
				if Publish_Report -CONTENT "$var_content"; then
					Write_Script -TEXT "report published.";
				else
					Stop_App -CODE 1 -TEXT "report failed.";
				fi;
			else
				Stop_App -CODE 1 -TEXT "invalid '-DESTINATION' parameter.";
			fi;
		else
			# Tur on '-INCREMENT' for avoid previous data removing if empty.
			rm "${PARAM[OUTPUT]}";
			Write_Script -TEXT "no report needed.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "need valid value for '-SOURCE' parameter.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;