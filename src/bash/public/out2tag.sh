#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               out2tag.sh               | #
# |________________________________________| #


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -A APP;
APP["NAME"]="out2tag";
APP["SHORTNAME"]="02T";
APP["DESCRIPTION"]="Redirect command output to a tag in a file.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENT"]="x";
APP["PARAM"]="COMMAND TAG ID OUTPUT";
APP["PARAM_DSC"]="Text source from output command. | no default.#TAG name for insertion. | default is 'blockquote'.#ID tag for insertion. | default is 'out2tag'.#File path of the documentation. | default is '${PWD}/README.md'.";
APP["OPTION"]="QUIET HELP VERSION";
APP["OPTION_DSC"]="Be silent. | default is 'false'.#Show man.#Show version.";
APP["EXAMPLE"]="${APP[NAME]} -COMMAND \"./${APP[NAME]}.sh -HELP\" -TAG \"blockquote\" -ID \"out2tag\" -OUTPUT \"README.md\" -QUIET";
APP["TODO"]="x";
APP["NOTE"]="x";
APP["CREDIT"]="x";
APP["CHANGELOG"]="0.2.0 - Get line position content instead tag with only id.#0.1.0 - Add TAG parameter, new text function added.#0.0.1 - Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="0.2.0";
APP["LICENSE"]="GPL-3.0-or-later";


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

declare -A CONF;
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
CONF["SEPARATOR"]="/";
CONF["IFS"]=$IFS;
# Test & Loop : includes.
if [[ "${0##*.}" == "sh" ]]; then
	for var_file in "${CONF[PATH]}${CONF[SEPARATOR]}common${CONF[SEPARATOR]}"func_*.sh; do source "$var_file"; done;
fi;
# Dates.
CONF["DATE_START"]="$(date "+%d/%m/%Y - %Hh%M")";
CONF["DATE_START_EXEC"]="$(date "+%Y-%m-%d %H:%M:%S")";
CONF["DATE_END"]="";
CONF["DATE_FILE"]="$(date "+%Y%m%d_%H%M%S")";


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -A PARAM;
# Loop : arguments.
while [[ $# -gt 0 ]]; do
	case "$1" in
		-COMMAND)
			PARAM["COMMAND"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-TAG)
			PARAM["TAG"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-ID)
			PARAM["ID"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-OUTPUT)
			PARAM["OUTPUT"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-QUIET)
			PARAM["QUIET"]=true
			shift
			;;
		-HELP)
			Stop_App -CODE 0 -HELP
			shift
			;;
		-VERSION|-v)
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			shift
			;;
		*)
			Stop_App -CODE 1 -TEXT "unknown parameter : '$1'." -HELP
			shift
			;;
	esac
done;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -A DEFAULT;
DEFAULT["TAG"]="blockquote";
DEFAULT["ID"]="out2tag";
DEFAULT["OUTPUT"]="${PWD}${CONF[SEPARATOR]}README.md";
DEFAULT["QUIET"]=false;
# Some script definitions.
if [ -z "${PARAM[COMMAND]}" ]; then Stop_App -CODE 1 -TEXT "need '-COMMAND' parameter." -HELP; fi;
if [ -z "${PARAM[TAG]}" ]; then PARAM["TAG"]="${DEFAULT[TAG]}"; fi;
if [ -z "${PARAM[ID]}" ]; then PARAM["ID"]="${DEFAULT[ID]}"; fi;
if [ -z "${PARAM[OUTPUT]}" ]; then PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}"; else PARAM["OUTPUT"]=${PARAM[OUTPUT]%/}; fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Add_Out2Tag.
#	DESCRIPTION : determine where inject command output.
function Add_Out2Tag() {
	# Some definition.
	local var_file_line_position;
	local var_file_line_content;
	local var_file_output;
	local var_file_slim;
	# Define insert position (minus 1 needed for 'sed r' inject method).
	var_file_line_position=$(grep -n "<${PARAM[TAG]}[^>]*id=\"${PARAM[ID]}\"[^>]*>" "${PARAM[OUTPUT]}" | cut -d ':' -f 1);
	# Test : not find.
	if [[ "$var_file_line_position" -ge 1 ]]; then
		# Re-define insert position (minus 1 needed for 'sed r' inject method).
		if [[ "${PARAM[QUIET]}" != true ]]; then
			Write_Script -TEXT "content line position is '${var_file_line_position}'."
		fi;
		var_file_line_content=$(sed "${var_file_line_position}q;d" "${PARAM[OUTPUT]}");
		var_file_line_position=$(("$var_file_line_position"-1));
		# Set double output solution.
		var_file_output="${PARAM[OUTPUT]}.output.tmp";
		var_file_slim="${PARAM[OUTPUT]}.slim.tmp";
		# Construct command output.
		echo -e "${var_file_line_content}" > "$var_file_output";
		${PARAM[COMMAND]} >> "$var_file_output" 2>&1;
		echo -e "</${PARAM[TAG]}>" >> "$var_file_output";
		echo -e "$(sed G "$var_file_output")" > "$var_file_output";
		# Remove old content & send to slim file.
		sed -e "/${var_file_line_content}/,/<\/${PARAM[TAG]}>/d" "${PARAM[OUTPUT]}" > "$var_file_slim";
		# Test : first line position.
		if [[ "$var_file_line_position" == 0 ]]; then
			# Crush and append original.
			cat "$var_file_output" > "${PARAM[OUTPUT]}";
			cat "$var_file_slim" >> "${PARAM[OUTPUT]}";
		else
			# Inject command output into slim file & just crush original.
			sed "${var_file_line_position}r ${var_file_output}" "$var_file_slim" > "${PARAM[OUTPUT]}";
		fi;
		rm "$var_file_output";
		rm "$var_file_slim";
	else
		Stop_App -CODE 1 -TEXT "tag '${PARAM[TAG]}' not finded.";
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Test : command.
	if ${PARAM[COMMAND]} &> /dev/null; then
		if [[ "${PARAM[QUIET]}" != true ]]; then
			Write_Script -TEXT "check command '${PARAM[COMMAND]}'.";
		fi;
		# Test : execute.
		if Add_Out2Tag; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				Write_Script -TEXT "'${PARAM[COMMAND]}' command checked.";
			fi;
		else
			if [[ "${PARAM[QUIET]}" != true ]]; then
				Write_Script -TEXT "ERROR : '${PARAM[COMMAND]}'.";
			fi;
		fi;
	else
		Stop_App -CODE 1 -TEXT "command '${PARAM[COMMAND]}' can't be performed.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;