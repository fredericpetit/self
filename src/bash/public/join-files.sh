#!/bin/bash
# shellcheck source=/dev/null
#  ___________________________________________
# |                                           | #
# |               join-files.sh               | #
# |___________________________________________| #


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -A APP;
APP["NAME"]="join-files";
APP["SHORTNAME"]="joinf";
APP["DESCRIPTION"]="Join some files into single file.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENT"]="x";
APP["PARAM"]="FILES PATTERN OUTPUT";
APP["PARAM_DSC"]="Path of files to join. | no default.#String to remove before join files | default is the bash shebang.#File path to store the final result. | default is '${PWD}/${APP[SHORTNAME]}-DATE.out'.";
APP["OPTION"]="QUIET HELP VERSION";
APP["OPTION_DSC"]="Be silent. | default is 'false'.#Show man.#Show version.";
APP["EXAMPLE"]="${APP[NAME]} -FILES \"func1.sh,func2.sh\" -PATTERN \"#!/bin/bash\" -OUTPUT \"funcs\" -QUIET";
APP["TODO"]="x";
APP["NOTE"]="x";
APP["CREDIT"]="x";
APP["CHANGELOG"]="0.0.2 - Add Test_File security#0.0.1 - Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="0.0.2";
APP["LICENSE"]="GPL-3.0-or-later";


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

declare -A CONF;
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
CONF["SEPARATOR"]="/";
CONF["IFS"]=$IFS;
# Test & Loop : includes.
if [[ "${0##*.}" == "sh" ]]; then
	for var_file in "${CONF[PATH]}${CONF[SEPARATOR]}common${CONF[SEPARATOR]}"func_*.sh; do source "$var_file"; done;
fi;
# Dates.
CONF["DATE_START"]="$(date "+%d/%m/%Y - %Hh%M")";
CONF["DATE_START_EXEC"]="$(date "+%Y-%m-%d %H:%M:%S")";
CONF["DATE_END"]="";
CONF["DATE_FILE"]="$(date "+%Y%m%d_%H%M%S")";


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -A PARAM;
# Loop : arguments.
while [[ $# -gt 0 ]]; do
	case "$1" in
		-FILES)
			PARAM["FILES"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-PATTERN)
			PARAM["PATTERN"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-OUTPUT)
			PARAM["OUTPUT"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-QUIET)
			PARAM["QUIET"]=true
			shift
			;;
		-HELP)
			Stop_App -CODE 0 -HELP
			shift
			;;
		-VERSION|-v)
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			shift
			;;
		*)
			Stop_App -CODE 1 -TEXT "unknown parameter : '$1'." -HELP
			shift
			;;
	esac
done;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -A DEFAULT;
DEFAULT["PATTERN"]="#!/bin/bash";
DEFAULT["OUTPUT"]="${PWD}${CONF[SEPARATOR]}${APP[NAME]}-${CONF[DATE_FILE]}.out";
DEFAULT["QUIET"]=false;
# Some script definitions.
if [ -z "${PARAM[FILES]}" ]; then Stop_App -CODE 1 -TEXT "need '-FILES' parameter." -HELP; fi;
if [[ ! "${PARAM[FILES]}" =~ "," ]]; then Stop_App -CODE 1 -TEXT "need more than one item in '-FILES' parameter." -HELP; fi;
if [ -z "${PARAM[PATTERN]}" ]; then PARAM["PATTERN"]="${DEFAULT[PATTERN]}"; fi;
if [ -z "${PARAM[OUTPUT]}" ]; then PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}"; else PARAM["OUTPUT"]=${PARAM[OUTPUT]%/}; fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_array;
	local var_count;
	local var_storage;
	local var_storage_firstline;
	local var_date;
	IFS=',' read -ra var_array <<< "${PARAM[FILES]}";
	Restore_IFS;
	var_count=1;
	# Test : init output.
	if ! Test_File -PATH "${PARAM[OUTPUT]}" -OVERWRITE; then Stop_App -CODE 1 -TEXT "output can't be writed"; fi;
	# Loop : collect in a new file.
	for element in "${var_array[@]}"; do
		var_storage=$(<"$element");
		var_storage_firstline=$(head -n 1 "$element");
		var_date=${CONF[DATE_FILE]};
		# Remove pattern starting the second file.
		if [[ $var_count -eq 1 ]]; then
			if [[ "${PARAM[QUIET]}" != true ]]; then echo "- file #${var_count} '${element}' - first file."; fi;
			echo "$var_storage" > "${PARAM[OUTPUT]}";
		elif [[ "$var_storage_firstline" == "${PARAM[PATTERN]}" ]]; then
			if [[ "${PARAM[QUIET]}" != true ]]; then echo "- file #${var_count} '${element}' - add with pattern removed."; fi;
			echo -e "\n\n\n# next added content date : ${var_date}." >> "${PARAM[OUTPUT]}";
			var_storage=$(tail -n +2 <<< "$var_storage");
			echo "$var_storage" >> "${PARAM[OUTPUT]}";
		else
			if [[ "${PARAM[QUIET]}" != true ]]; then echo "- file #${var_count} '${element}' - simple add."; fi;
			echo -e "\n\n\n# next added content date : ${var_date}." >> "${PARAM[OUTPUT]}";
			echo "$var_storage" >> "${PARAM[OUTPUT]}";
		fi;
		var_count="$((var_count+1))";
	done;
	# Test : correct output.
	if ! Test_File -PATH "${PARAM[OUTPUT]}"; then Stop_App -CODE 1 -TEXT "output can't be writed"; fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;