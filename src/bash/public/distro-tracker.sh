#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2329
# shellcheck disable=SC2034
#  _______________________________________________
# |                                               | #
# |               distro-tracker.sh               | #
# |_______________________________________________| #


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-tracker";
APP["SHORTNAME"]="DT";
APP["DESCRIPTION"]="Find out latest stable versions of GNU/Linux & BSD distributions.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENT"]="program wget mandatory#program jq mandatory";
APP["PARAM"]="SOURCE DISTRO CLI LENGTH OUTPUT REPORT BACKUP";
APP["PARAM_DSC"]="Local filepath or distant url to JSON data. | no default.#Choice between some GNU/Linux & BSD distributions or all registered. | default is 'all'.#Force CLI ISO distribution if available. | default is 'false'.#Number of valid releases needed. | default is '5'.#File path to store the final result, without extension, by date name mode or custom name mode. | default is '${PWD}/${APP[SHORTNAME]}-DATE.csv'.#Generate a JSON report file, only if new releases detected, in same output path, into a file named '*.report.json'. | default is 'false'.#Backup security for keeping and don't remove previous datas if entire website checking fails. New and old datas need to be in the same directory. | default is 'false'.";
APP["OPTION"]="QUIET HELP VERSION";
APP["OPTION_DSC"]="Be silent. | default is 'false'.#Show man.#Show version.";
APP["EXAMPLE"]="${APP[NAME]} -d \"debian,fedora,freebsd,mageia,slackware\" -l \"10\" -o \"distro-tracker\" -s -q";
APP["TODO"]="x";
APP["NOTE"]="First of all, some distributions have two general indexes to scan to retrieve the latest versions, with main and archive. Information gived in terminal output.#Alma, CentOS, Fedora & MX have a main index where everything is listed, but some versions are downloaded from another place.#Alpine, Debian, Kaisen & Ubuntu lists beta/RC versions in the index of a release.#Fedora lists in the index of a release an \"OSB\" version, a version built with osbuilder.#Alma and CentOS list \"non-existent\" versions - a version 9 which refers to a 9.3, a 9-stream twin of 9, well ...#Alma, Fedora & Ubuntu have both ui or cli mode.#Alpine & XPC-NG nead a sort with head because many releases listed in single release page.";
APP["CREDIT"]="x";
APP["CHANGELOG"]="1.0.0 - Expansion to 22 GNU/Linux distributions with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, and XCP-ng. General mechanism for retrieving version numbers using the -LENGTH parameter has been revised for more logic : not just X tested versions are returned, but now X valid versions. Addition of the -SOURCE parameter to choose tracking URLs via a local or remote JSON file. Addition of the -REPORT parameter to generate a JSON report to notify of any changes in the latest release of each distribution. Activation of the -ENV parameter. Renaming of the DISTRIB parameter to -DISTRO, -DEPTH to -LENGTH, -SECURITY to -BACKUP. Addition of one-letter shortcuts for each parameter and option. Addition of the Compare_NokFilter function to better manage and clarify versions tagged \"NOK\" (beta, RC, etc.): all pingable versions are retrieved, with a final filter on stable versions.#0.5.0 - Add -SECURITY option for copy old datas to new datas if wget fails on entire website.#0.4.0 - Up to 15 distributions by default.#0.3.0 - New strong REGEXP, Add -LENGTH parameter, Add command JQ requirement, Add JSON output, Up to 12 distributions GNU/Linux & BSD.#0.2.0 - Separate checks on index page & release page.#0.1.0 - Add CSV output.#0.0.1 - Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="1.0.0";
APP["LICENSE"]="GPL-3.0-or-later";


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

declare -gA CONF;
CONF["DEBUG"]=false;
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
CONF["SEPARATOR"]="/";
CONF["IFS"]=$IFS;
# Test & Loop : includes.
if [[ "${0##*.}" == "sh" ]]; then
	for var_file in "${CONF[PATH]}${CONF[SEPARATOR]}common${CONF[SEPARATOR]}"func_*.sh; do source "$var_file"; done;
fi;
# Dates.
CONF["DATE_START"]="$(date "+%d/%m/%Y - %Hh%M")";
CONF["DATE_START_EXEC"]="$(date "+%Y-%m-%d %H:%M:%S")";
CONF["DATE_END"]="";
CONF["DATE_FILE"]="$(date "+%Y%m%d_%H%M%S")";


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ $# -gt 0 ]]; do
	case "$1" in
		-SOURCE|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-DISTRO|-d)
			PARAM["DISTRO"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-CLI|-c)
			PARAM["CLI"]=true
			shift
			;;
		-LENGTH|-l)
			PARAM["LENGTH"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-OUTPUT|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-REPORT|-r)
			PARAM["REPORT"]=true
			shift
			;;
		-BACKUP|-b)
			PARAM["BACKUP"]=true
			shift
			;;
		-QUIET|-q)
			PARAM["QUIET"]=true
			shift
			;;
		-HELP|-h)
			Stop_App -CODE 0 -HELP
			shift
			;;
		-VERSION|-v)
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			shift
			;;
		*)
			Stop_App -CODE 1 -TEXT "unknown parameter : '$1'." -HELP
			shift
			;;
	esac
done;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["DISTRO"]="alma,alpine,arch,centos,clonezilla,debian,fedora,freebsd,kaisen,kali,mageia,manjaro,mint,mx,opensuse,opnsense,parrot,pfsense,proxmox,slackware,ubuntu,xcpng";
# DEFAULT["DISTRO"] is a string, need array for Search_InArray custom PARAM["DISTRO"].
IFS=',' read -ra var_default_distro_array <<< "${DEFAULT["DISTRO"]}";
Restore_IFS;
DEFAULT["CLI"]=false;
DEFAULT["LENGTH"]="5";
DEFAULT["OUTPUT"]="${PWD}${CONF[SEPARATOR]}${APP[SHORTNAME]}-${CONF[DATE_FILE]}";
DEFAULT["REPORT"]=false;
DEFAULT["BACKUP"]=false;
DEFAULT["QUIET"]=false;
# Some script definitions.
if [ -z "${PARAM[SOURCE]}" ]; then
	# Default book DISTRIBUTION.
	Add_Book "DISTRIBUTION" "alma" "uicli,https://repo.almalinux.org/almalinux,https://repo.almalinux.org/almalinux/xxx/isos/x86_64,,https://vault.almalinux.org/xxx/isos/x86_64";
	Add_Book "DISTRIBUTION" "alpine" "cli,https://dl-cdn.alpinelinux.org/alpine,https://dl-cdn.alpinelinux.org/alpine/xxx/releases/x86_64,,";
	Add_Book "DISTRIBUTION" "arch" "ui,https://geo.mirror.pkgbuild.com/iso,https://geo.mirror.pkgbuild.com/iso/xxx,,";
	Add_Book "DISTRIBUTION" "centos" "ui,https://mirror.in2p3.fr/pub/linux/centos-stream,https://mirror.in2p3.fr/pub/linux/centos-stream/xxx/BaseOS/x86_64/iso,https://mirror.in2p3.fr/pub/linux/centos-vault,https://mirror.in2p3.fr/pub/linux/centos-vault/xxx/isos/x86_64";
	Add_Book "DISTRIBUTION" "clonezilla" "ui,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable/xxx,,";
	Add_Book "DISTRIBUTION" "debian" "ui,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/mirror/cdimage/archive,https://cdimage.debian.org/mirror/cdimage/archive/xxx/amd64/iso-dvd";
	Add_Book "DISTRIBUTION" "fedora" "uicli,https://mirror.in2p3.fr/pub/fedora/linux/releases,https://mirror.in2p3.fr/pub/fedora/linux/releases/xxx/Workstation/x86_64/iso,,https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/xxx/Workstation/x86_64/iso";
	Add_Book "DISTRIBUTION" "freebsd" "ui,https://download.freebsd.org/releases/ISO-IMAGES,https://download.freebsd.org/releases/ISO-IMAGES/xxx,,";
	Add_Book "DISTRIBUTION" "kaisen" "ui,https://iso.kaisenlinux.org/rolling,https://iso.kaisenlinux.org/rolling,,";
	Add_Book "DISTRIBUTION" "kali" "ui,https://cdimage.kali.org,https://cdimage.kali.org/kali-xxx,,";
	Add_Book "DISTRIBUTION" "mageia" "ui,https://fr2.rpmfind.net/linux/mageia/iso,https://fr2.rpmfind.net/linux/mageia/iso/xxx/Mageia-xxx-x86_64,,";
	Add_Book "DISTRIBUTION" "manjaro" "ui,https://manjaro.org/products/download/x86,https://download.manjaro.org/kde/xxx,,";
	Add_Book "DISTRIBUTION" "mx" "ui,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Old,https://sourceforge.net/projects/mx-linux/files/Old/MX-xxx/KDE";
	Add_Book "DISTRIBUTION" "mint" "ui,https://mirrors.edge.kernel.org/linuxmint/stable,https://mirrors.edge.kernel.org/linuxmint/stable/xxx,,";
	Add_Book "DISTRIBUTION" "opensuse" "ui,https://fr2.rpmfind.net/linux/opensuse/distribution/leap,https://fr2.rpmfind.net/linux/opensuse/distribution/leap/xxx/iso,,";
	Add_Book "DISTRIBUTION" "opnsense" "cli,https://mirror.wdc1.us.leaseweb.net/opnsense/releases,https://mirror.wdc1.us.leaseweb.net/opnsense/releases/xxx,,";
	Add_Book "DISTRIBUTION" "parrot" "ui,https://deb.parrot.sh/parrot/iso,https://deb.parrot.sh/parrot/iso/xxx,,";
	Add_Book "DISTRIBUTION" "pfsense" "cli,https://atxfiles.netgate.com/mirror/downloads,https://atxfiles.netgate.com/mirror/downloads,,";
	Add_Book "DISTRIBUTION" "proxmox" "cli,https://enterprise.proxmox.com/iso,https://enterprise.proxmox.com/iso,,";
	Add_Book "DISTRIBUTION" "slackware" "ui,https://mirrors.slackware.com/slackware/slackware-iso,https://mirrors.slackware.com/slackware/slackware-iso/slackware64-xxx-iso,,";
	Add_Book "DISTRIBUTION" "ubuntu" "uicli,https://releases.ubuntu.com,https://releases.ubuntu.com/xxx,,";
	Add_Book "DISTRIBUTION" "xcpng" "cli,https://updates.xcp-ng.org/isos,https://updates.xcp-ng.org/isos/xxx,,";
else
	# Custom book DISTRIBUTION.
	if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]]; then
		var_data_distro="$(Get_Distant -URL "${PARAM[SOURCE]}" -TYPE "string")";
	else
		if Test_File -PATH "${PARAM[SOURCE]}"; then
			var_data_distro="$(<"${PARAM[SOURCE]}")";
		else
			Stop_App -CODE 1 -TEXT "file in '-SOURCE' parameter is not available.";
		fi;
	fi;
	# Loop : Process Substitution.
	while read -ra var_data_distro_array; do
		var_param_file_distro=$(echo "$var_data_distro_array" | jq -c -r '.name');
		var_param_file_values=$(echo "$var_data_distro_array" | jq -c -r '[.mode, .current_index, .current_release, .archive_index, .archive_release] | join(",")');
		# Test : invalid distribution.
		if Search_InArray -SEARCH "$var_param_file_distro" -ARRAY "${var_default_distro_array[*]}"; then
			Add_Book "DISTRIBUTION" "$var_param_file_distro" "$var_param_file_values";
		else
			Stop_App -CODE 1 -TEXT "unknown distribution : '$var_param_file_distro'.";
		fi;
	done < <(echo "$var_data_distro" | jq -c -r '.distro[]');
fi;
if [ -z "${PARAM[DISTRO]}" ] || [[ "${PARAM[DISTRO]}" == "all" ]]; then
	PARAM["DISTRO"]="${DEFAULT[DISTRO]}";
else
	PARAM["DISTRO"]=$(echo "${PARAM["DISTRO"]}" | tr ',' '\n' | sort | tr '\n' ',');
fi;
IFS=',' read -ra var_param_distro_array <<< "${PARAM["DISTRO"]}";
Restore_IFS;
# Loop : distribution.
for var_param_distro_array_element in "${var_param_distro_array[@]}"; do
	# Test : invalid distribution.
	if ! Search_InArray -SEARCH "$var_param_distro_array_element" -ARRAY "${var_default_distro_array[*]}"; then
		Stop_App -CODE 1 -TEXT "unknown distribution : '$var_param_distro_array_element'.";
	fi;
done;
if [ -z "${PARAM[CLI]}" ]; then PARAM["CLI"]="${DEFAULT[CLI]}"; fi;
if [ -z "${PARAM[LENGTH]}" ]; then PARAM["LENGTH"]="${DEFAULT[LENGTH]}"; fi;
if [ -z "${PARAM[OUTPUT]}" ]; then
	# Loop : last file win previous data state.
	for var_param_output_single_file in "${PWD}${CONF[SEPARATOR]}${APP[SHORTNAME]}"*.csv; do
		PARAM["OUTPUT_BACKUP"]="$var_param_output_single_file";
	done;
	if [[ "${PARAM[REPORT]}" == true ]]; then PARAM["REPORT"]=${DEFAULT[OUTPUT]}.report.json; fi;
	PARAM["OUTPUT"]=${DEFAULT[OUTPUT]}.csv;
else
	PARAM["OUTPUT_BACKUP"]=${PARAM[OUTPUT]%/}.archive.csv;
	if [[ "${PARAM[REPORT]}" == true ]]; then PARAM["REPORT"]=${PARAM[OUTPUT]%/}.report.json; fi;
	PARAM["OUTPUT"]=${PARAM[OUTPUT]%/}.current.csv;
fi;
if [ -z "${PARAM[REPORT]}" ]; then PARAM["REPORT"]="${DEFAULT[REPORT]}"; fi;
if [ -z "${PARAM[BACKUP]}" ]; then PARAM["BACKUP"]="${DEFAULT[BACKUP]}"; fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Get_AllVersion.
#	DESCRIPTION : parse HTML index page with some regex.
#	RETURN : string.
function Get_AllVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_data_current;
	local var_data_archive;
	local var_parse;
	local var_parse_current;
	local var_parse_archive;
	local var_array_release;
	local var_array_sorted_release;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_release");
	# Construct HTML data.
	var_data_current=$(Get_Distant -URL "$var_distribution_data_current_index" -TYPE "string");
	if [[ "$var_distribution_data_archive_index" != "" ]]; then
		var_data_archive=$(Get_Distant -URL "$var_distribution_data_archive_index" -TYPE "string");
	fi;
	# Tests : construct parsing for the distribution.
	if [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS.
		#
		var_parse_current=$(Get_AllVersionGeneric -DATA "$var_data_current");
		var_parse_archive=$(Get_AllVersionGeneric -DATA "$var_data_archive");
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "clonezilla" ]]; then
		#
		# CLONEZILLA.
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '<a[^>]*href="[^"]*.iso"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | grep -Po '(?<=href=")[^?"]*' | sed 's/debian-//g' | sed 's/-amd64-DVD-1.iso//g' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/"[A-Za-z][^"]*"/""/g' | sed 's/<a href="[^"]*-live">//g' | grep -Po '(?<=href=")[^?"]*' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '(?<=/MX-)[^"]+(?=_KDE_x64.iso/download")' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | sed 's/MX-//g' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" =~ ^(kaisen|manjaro|pfsense|proxmox)$ ]]; then
		#
		# KAISEN, MANJARO, PFSENSE, PROXMOX.
		#
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="(kaisenlinuxrolling|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)[^"]*(-amd64-MATE.iso|manjaro-kde-|-RELEASE-amd64.iso.gz|\d+\.iso)"' | sed 's/href="//g' | sed 's/"//g' | sed -E 's/(kaisenlinuxrolling|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)//g' | sed -E 's/(-amd64-MATE.iso|\/manjaro-kde-.*.iso|-RELEASE-amd64.iso.gz|.iso)//g' | tr '\n' ',');
	else
		#
		# GENERIC.
		#
		var_parse=$(Get_AllVersionGeneric -DATA "$var_data_current");
	fi;
	# Test : data exists & NO SPACE ALLOWED.
	if [[ -n "$var_parse" && "$var_parse" != "" && "$var_parse" != "null" && ! "$var_parse" =~ " " ]]; then
		# Loop : construct & sort by most recent versions.
		IFS=',' read -ra var_array_release <<< "$var_parse";
		mapfile -t var_array_sorted_release < <(printf "%s\n" "${var_array_release[@]}" | sort -Vr);
		# Construct return.
		var_result=("$(IFS=,; echo "${var_array_sorted_release[*]}")");
		Restore_IFS;
		echo "${var_result[@]}";
	else
		echo "";
	fi;
}

# Get_AllVersionGeneric.
#	DESCRIPTION : parse some HTML index page with single regex.
#	RETURN : string.
function Get_AllVersionGeneric() {
	# Some definition.
	local var_data;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	var_result=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/<a href="RPM-[^"]*">//g' | sed 's/<a href="almalinux-[^"]*">//g' | sed 's/<a href="slackware-[^"]*">//g' | grep -Po '(?<=href=")[^?"]*' | sed "s/\\bkali-//g" | sed "s/\\bv//g" | sed "s/\\bslackware64-//g" | sed "s/-iso\\b//g" | tr '\n' ',');
	echo "$var_result";
}

# Get_SingleVersion.
#	DESCRIPTION : parse HTML release page with some regex.
#	RETURN : string.
function Get_SingleVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_version;
	local var_position;
	local var_url;
	local var_filename;
	local var_date;
	local var_data;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			-POSITION)
				var_position="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_release");
	# Test : construct pre-built url & date.
	var_url="${var_distribution_data_current_release//xxx/$var_version}";
	# Tests : construct return filename:url.
	if [[ "$var_distribution" == "alma" ]]; then
		#
		# ALMA - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"isos/"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		fi;
		# Test: CLI usage.
		if [[ -n "${PARAM[CLI]}" && "${PARAM[CLI]}" == true ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-minimal.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-dvd.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "alpine" ]]; then
		#
		# ALPINE - many releases by release page (need sort).
		#
		var_url="${var_distribution_data_current_release//xxx/v${var_version}}";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-standard-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		fi;
	elif [[ "$var_distribution" == "arch" ]]; then
		#
		# ARCH - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"iso"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-LiveDVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-netinstall.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*latest-x86_64-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "clonezilla" ]]; then
		#
		# CLONEZILLA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-amd64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed "s/.*\/$var_version\///g" | sed 's/\/download//g');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN - one release by release page.
		# remember to match DVD-1 pattern with old releases.
		#
		# Test : fix debian separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="indexlist">(.*?)</table>' | grep -Po '<td class="indexcolname">(.*?)</td>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"debian-${var_version}-amd64-DVD-1.iso\"" | sed 's/href="//g' | sed 's/"//g' | head -n 1);
	elif [[ "$var_distribution" == "fedora" ]]; then
		#
		# FEDORA - one release by release page, uicli.
		# remember to not match -osb- pattern.
		#
		# Test: CLI usage.
		if [[ -n "${PARAM[CLI]}" && "${PARAM[CLI]}" == true ]]; then
			var_url="${var_url//Workstation/Server}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_url="${var_url//Workstation/Server}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Server-dvd-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Workstation-Live-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-Live-Workstation-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_url="${var_url//$var_version\/Workstation\/x86_64\/iso/$var_version\/Fedora\/x86_64\/iso}";
					var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
					var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-[^"]*-x86_64-DVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		fi;
	elif [[ "$var_distribution" == "freebsd" ]]; then
		#
		# FREEBSD - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"FreeBSD-${var_version}-RELEASE-amd64-dvd1.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kaisen" ]]; then
		#
		# KAISEN - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}[^\"]*-amd64-MATE.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kali" ]]; then
		#
		# KALI - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="kali-linux-[^"]*-installer-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mageia" ]]; then
		#
		# MAGEIA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "manjaro" ]]; then
		#
		# MANJARO - one release in one page.
		#
		var_data=$(Get_Distant -URL "$var_distribution_data_current_index" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}/manjaro-kde-[^\"]*.iso\"" | sed 's/href="//g' | sed 's/"//g' | sed 's/.*\///g');
	elif [[ "$var_distribution" == "mint" ]]; then
		#
		# MINT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*cinnamon-64bit.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX - one release by release page.
		#
		# Test : fix mx separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*_x64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed 's/.*KDE\///g' | sed 's/\/download//g');
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_url="${var_url//\/KDE/}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*MX-${var_version}_x64.iso/download\"" | sed 's/href="//g' | sed 's/"//g' | sed "s/.*MX-${var_version}\///g" | sed 's/\/download//g');
		fi;
	elif [[ "$var_distribution" == "opensuse" ]]; then
		#
		# OPENSUSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/openSUSE-[^"]*-Build[^"]*.iso//g' | sed 's/openSUSE-[^"]*-Current[^"]*.iso//g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="openSUSE-[^"]*-DVD-x86_64[^>]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "opnsense" ]]; then
		#
		# OPNSENSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po "<table.*</table>" | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso.bz2"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "parrot" ]]; then
		#
		# PARROT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-security-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "pfsense" ]]; then
		#
		# PFSENSE - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-${var_version}-[^\"]*.iso.gz\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "proxmox" ]]; then
		#
		# PROXMOX - all releases in one page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"proxmox-ve_${var_version}.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "slackware" ]]; then
		#
		# SLACKWARE - one release by release page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="slackware64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "ubuntu" ]]; then
		#
		# UBUNTU - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test: CLI usage.
		if [[ -n "${PARAM[CLI]}" && "${PARAM[CLI]}" == true ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-live-server-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-desktop-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "xcpng" ]]; then
		#
		# XCP-NG - many releases by release page (need sort).
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"xcp-ng-${var_version}.([0-9]-[0-9]|[0-9]|[0-9]-[a-z]+[0-9]).iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
	fi;
	# Test : get NaN error explicitly.
	if [[ -z "$var_filename" || "$var_filename" == "" || ${#var_filename} -gt 60 ]]; then var_filename="NaN"; fi;
	# Construct return filename;url;date.
	var_date=$(Get_SingleVersionDateGeneric -DATA "$var_data" -FILENAME "$var_filename");
	echo "$var_filename;$var_url/$var_filename;$var_date";
}

# Get_SingleVersionDateGeneric.
#	DESCRIPTION : convert some date formats to one.
#	RETURN : string.
function Get_SingleVersionDateGeneric() {
	# Some definition.
	local var_data;
	local var_filename;
	local var_date;
	local var_date_day;
	local var_date_month;
	local var_date_year;
	local var_func;
	local var_func_dating;
	local var_return;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# BASH_REMATCH.
	declare -A var_date_map=( ["Jan"]="01" ["Feb"]="02" ["Mar"]="03" ["Apr"]="04" ["May"]="05" ["Jun"]="06" ["Jul"]="07" ["Aug"]="08" ["Sep"]="09" ["Oct"]="10" ["Nov"]="11" ["Dec"]="12" );
	# 'DD-MMM-YYYY' = alma, alpine, arch, mint, parrot, pfsense, proxmox, slackware, xcpng.
	func_234() {
		var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title="[^"]*"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*\d{2}-[A-Za-z]+-\d{4}" | grep -Po "\d{2}-[A-Za-z]+-\d{4}");
		if [[ "$var_date" =~ ^([0-9]{2})-([A-Za-z]{3})-([0-9]{4})$ ]]; then
			var_date_day=${BASH_REMATCH[1]};
			var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
			var_date_year=${BASH_REMATCH[3]};
			echo "$var_date_year/$var_date_month/$var_date_day";
		fi;
	}
	# 'YYYY-MM-DD' = centos, clonezilla, debian, fedora, kaisen, mageia, mx, opensuse, ubuntu.
	func_422() {
		var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title="[^"]*"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*\d{4}-\d{2}-\d{2}" | grep -Po "\d{4}-\d{2}-\d{2}");
		if [[ "$var_date" =~ ^([0-9]{4})-([0-9]{2})-([0-9]{2})$ ]]; then
			var_date_year=${BASH_REMATCH[1]};
			var_date_month=${BASH_REMATCH[2]};
			var_date_day=${BASH_REMATCH[3]};
			echo "$var_date_year/$var_date_month/$var_date_day";
		fi;
	}
	# 'YYYY-MMM-DD' = freebsd, kali, opnsense.
	func_432() {
		var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title="[^"]*"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*\d{4}-[A-Za-z]+-\d{2}" | grep -Po "\d{4}-[A-Za-z]+-\d{2}");
		if [[ "$var_date" =~ ^([0-9]{4})-([A-Za-z]{3})-([0-9]{2})$ ]]; then
			var_date_year=${BASH_REMATCH[1]};
			var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
			var_date_day=${BASH_REMATCH[3]};
			echo "$var_date_year/$var_date_month/$var_date_day";
		fi;
	}
	var_result="rolling";
	var_func=("func_234" "func_422" "func_432");
	# Loop : remove <td>, <span></span>, title, all inside <a>XXX</a>, and get date.
	for var_func_dating in "${var_func[@]}"; do
		var_return=$($var_func_dating);
		# Test : instead perform head -n 1 each function, get NaN error explicitly.
		if [[ -n "$var_return" && "$var_return" =~ " " ]]; then
			var_result="NaN";
			break;
		elif [[ -n "$var_return" && "$var_return" != "" ]]; then
			var_result="$var_return";
			break;
		fi;
	done;
	echo "$var_result";
}

# Compare_NokFilter.
#	DESCRIPTION : filter release by nok patterns.
#	RETURN : string or code.
function Compare_NokFilter() {
	# Some definition.
	local var_distribution;
	local var_release;
	local var_filename;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DISTRO)
				var_distribution="$2"
				shift 2
				;;
			-RELEASE)
				var_release="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : nok & specific invalid.
	case "$var_distribution" in
		alpine)
			[[ "$var_filename" == *"_rc"* ]] && var_result="nok-rc"
			;;
		alma)
			if [[ "$var_release" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_release" =~ ^[0-9]$ ]]; then
				var_result="nok-version"
			fi
			;;
		debian)
			if [[ "$var_release" == *"_r"* ]]; then
				var_result="nok-rc"
			elif [[ "$var_release" == *"-"* ]]; then
				var_result="nok-version"
			fi
			;;
		freebsd)
			[[ "$var_filename" == *"RC"* ]] && var_result="nok-rc"
			;;
		kaisen)
			[[ "$var_release" == *"RC"* ]] && var_result="nok-rc"
			;;
		parrot)
			[[ "$var_release" == *"-beta"* ]] && var_result="nok-beta"
			;;
		ubuntu)
			[[ "$var_filename" == *"-beta-"* ]] && var_result="nok-beta"
			;;
		xcpng)
			if [[ "$var_filename" == *"-alpha"* ]]; then
				var_result="nok-alpha"
			elif [[ "$var_filename" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_filename" == *"-rc"* ]]; then
				var_result="nok-rc"
			fi
			;;
	esac
	# Test : string, code.
	if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
}

# Add_Report.
#	DESCRIPTION : add distribution with new release and/or new source to JSON report.
function Add_Report() {
	# Some definition.
	local var_distribution;
	local var_release;
	local var_release_new;
	local var_release_old;
	local var_source;
	local var_source_new;
	local var_source_old;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DISTRO)
				var_distribution="$2"
				shift 2
				;;
			-RELEASE)
				var_release="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : release, source.
	if [[ -n "$var_release" ]]; then
		var_release_new="$(echo "$var_release" | cut -d ":" -f1)";
		var_release_old="$(echo "$var_release" | cut -d ":" -f2)";
	fi;
	if [[ -n "$var_source" ]]; then
		var_source_new="$(echo "$var_source" | cut -d ":" -f1)";
		var_source_old="$(echo "$var_source" | cut -d ":" -f2)";
	fi;
	# Tests : release, source.
	if [[ "$var_release_new" != "$var_release_old" && "$var_source_new" != "$var_source_old" ]]; then
		if jq --arg distribution "$var_distribution" \
		--arg release_new "$var_release_new" \
		--arg release_old "$var_release_old" \
		--arg source_new "$var_source_new" \
		--arg source_old "$var_source_old" \
		'.distro += [{"name": $distribution, "release": {"new": $release_new, "old": $release_old}, "source": {"new": $source_new, "old": $source_old}}]' \
		"${PARAM[REPORT]}" > "${PARAM[REPORT]}.tmp" && mv "${PARAM[REPORT]}.tmp" "${PARAM[REPORT]}"; then
			return 0;
		else
			return 1;
		fi;
	elif [[ "$var_release_new" != "$var_release_old" ]]; then
		if jq --arg distribution "$var_distribution" \
		--arg release_new "$var_release_new" \
		--arg release_old "$var_release_old" \
		'.distro += [{"name": $distribution, "release": {"new": $release_new, "old": $release_old}}]' \
		"${PARAM[REPORT]}" > "${PARAM[REPORT]}.tmp" && mv "${PARAM[REPORT]}.tmp" "${PARAM[REPORT]}"; then
			return 0;
		else
			return 1;
		fi;
	elif [[ "$var_source_new" != "$var_source_old" ]]; then
		if jq --arg distribution "$var_distribution" \
		--arg source_new "$var_source_new" \
		--arg source_old "$var_source_old" \
		'.distro += [{"name": $distribution, "source": {"new": $source_new, "old": $source_old}}]' \
		"${PARAM[REPORT]}" > "${PARAM[REPORT]}.tmp" && mv "${PARAM[REPORT]}.tmp" "${PARAM[REPORT]}"; then
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}

# Convert_CSVToJSON.
#	DESCRIPTION : data CSV to data JSON.
function Convert_CSVToJSON() {
	# Some definition.
	local var_data_imported;
	local var_data_rewrited;
	var_data_imported=$(jq -R '{distro: [
		inputs | split(",") | {
			name: .[0],
			source: .[9],
			tmp: {
				all: .[3] | split(" ") | map({version: .}),
				valid: .[4] | split(" ") | map({version: .}),
				nok: .[5] | split(" ") | map({version: .})
			},
			releases: {
				latest: {version: .[1], date: .[2], url: .[6]},
				all: .[3] | split(" ") | map({version: ""}),
				valid: .[7] | split(" ") | map({version: "", url: .}),
				nok: .[8] | split(" ") | map({version: "", url: .})
			}
		}
	]}' < "${PARAM[OUTPUT]}");
	var_data_rewrited="$(echo "$var_data_imported" | jq '.distro[] |= (
		.releases.all = [
			( range(0; (.releases.all | length)) as $i | .tmp.all[$i].version )
		] | 
		.releases.valid = [ 
			( range(0; (.releases.valid | length)) as $i | { version: .tmp.valid[$i].version, url: .releases.valid[$i].url } )
		] | 
		.releases.nok = [ 
			( range(0; (.releases.nok | length)) as $i | { version: .tmp.nok[$i].version, url: .releases.nok[$i].url } )
		] | 
		del(.tmp)
		)'
	)";
	# Final JSON output with SC2001 shellcheck solution.
	echo "$var_data_rewrited" > "${PARAM[OUTPUT]%.csv}.json";
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	# array.
	declare -a var_array_param_distribution;
	declare -a var_array_return_distribution;
	local var_array_return_distribution_length;
	# element.
	local var_distribution_name;
	local var_distribution_mode;
	local var_distribution_indexes;
	local var_release_version;
	local var_release_version_display;
	local var_release_array_filenameurl;
	local var_release_filename;
	local var_release_filename_display;
	local var_release_url;
	local var_release_url_clean;
	local var_release_url_display;
	local var_release_url_valid_last;
	local var_release_date;
	local var_release_date_valid_last;
	local var_release_note;
	local var_release_distant;
	local var_release_check;
	local var_release_nok;
	# count.
	local var_count_release_all;
	local var_count_release_valid;
	# data backup.
	local var_return_backup;
	local var_return_backup_release;
	local var_return_backup_source;
	# result.
	declare -a var_array_result_distribution_nok;
	declare -a var_array_result_distribution_nok_url;
	declare -a var_array_result_distribution_valid;
	local var_array_result_distribution_valid_display;
	declare -a var_array_result_distribution_valid_url;
	# Construct : read data.
	IFS=',' read -ra var_array_param_distribution <<< "${PARAM[DISTRO]}";
	Restore_IFS;
	# Test : DEBUG.
 	if [[ "${CONF[DEBUG]}" == false ]]; then
		# Test : Table with param and max value length.
		if [[ "${PARAM[QUIET]}" != true ]]; then Get_TableHeader -ARRAY "ID:4 Version:10 Filename:40 Ping:4 URL:40 Reject:16"; fi;
		# Start or RàZ CSV output.
		echo -e "Name,VersionLatest,VersionLatestDate,VersionAll,VersionValid,VersionNOK,UrlLatest,UrlValid,UrlNOK,Source" > "${PARAM[OUTPUT]}";
		# Test : report need previous data.
		if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]] && Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
			# Start or RàZ JSON report.
			jq --null-input --arg date "${CONF[DATE_START_EXEC]}" '{"date": $date,"distro":[]}' > "${PARAM[REPORT]}";
		fi;
		# Loop : distribution, with 2 counts control (all & valid).
		for var_distribution_name in "${var_array_param_distribution[@]}"; do
			# Env & Indexes.
			var_distribution_mode="${DISTRIBUTION[${var_distribution_name}_mode]}";
			var_distribution_indexes=$([[ "${DISTRIBUTION[${var_distribution_name}_archive_index]}" == "" ]] && echo "1 index" || echo "2 indexes");
			# Construct : read release.
			IFS=',' read -ra var_array_return_distribution <<< "$(Get_AllVersion -NAME "$var_distribution_name")";
			var_array_return_distribution_length=${#var_array_return_distribution[@]};
			Restore_IFS;
			# Test : parsing not available.
			if [ "$var_array_return_distribution_length" -ne 0 ]; then
				# Display.
				if [[ "${PARAM[QUIET]}" != true ]]; then echo -e "\n${COLOR[GREEN_DARK]}$var_distribution_name${COLOR[STOP]} - $var_array_return_distribution_length release(s) from $var_distribution_indexes ('${var_distribution_mode}' available) :"; fi;
				# Count.
				var_count_release_all=0;
				var_count_release_valid=0;
				# Loop : release.
				while [[ "$var_count_release_valid" -lt "${PARAM[LENGTH]}" && "$var_count_release_all" -lt "$var_array_return_distribution_length" ]]; do
					var_release_version=${var_array_return_distribution[$var_count_release_all]};
					var_release_array_filenameurl=$(Get_SingleVersion -NAME "$var_distribution_name" -VERSION "$var_release_version" -POSITION "$var_count_release_all");
					var_release_filename=$(echo "$var_release_array_filenameurl" | cut -d ";" -f1);
					var_release_url=$(echo "$var_release_array_filenameurl" | cut -d ";" -f2);
					var_release_date=$(echo "$var_release_array_filenameurl" | cut -d ";" -f3);
					# Display.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						var_release_version_display=$(Limit_StringLength -STRING "$var_release_version" -COUNT 10 -DIRECTION "after");
						var_release_filename_display=$(Limit_StringLength -STRING "$var_release_filename" -COUNT 40 -DIRECTION "after");
						var_release_url_clean="${var_release_url#http://}";
						var_release_url_clean="${var_release_url_clean#https://}";
						var_release_url_display=$(Limit_StringLength -STRING "$var_release_url_clean" -COUNT 40 -DIRECTION "after");
						var_release_note="";
					fi;
					# Test : line check.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 [-]:5" -ACTION "check";
					fi;
					# Test : single release file available.
					if Get_Distant -URL "$var_release_url" -TYPE "ping"; then
						var_release_distant=true;
						var_release_check="tick";
					else
						var_release_distant=false;
						var_release_check="cross";
					fi;
					# Test : single release filter NOK with void for no interacting with echo.
					if Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename" &> /dev/null; then var_release_nok=true; else var_release_nok=false; fi;
					# Tests : NOK.
					if [[ "$var_release_nok" == true || "$var_release_distant" == false || "$var_release_filename" == "" ]]; then
						if [[ "$var_release_nok" == true ]]; then
							var_release_note="$(Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename")";
						elif [[ "$var_release_distant" == false ]]; then
							var_release_note="nok-ping";
						elif [[ "$var_release_filename" == "" ]]; then
							var_release_note="nok-empty";
						fi;
						# Construct nok distribution return filtered.
						var_array_result_distribution_nok+=("$var_release_version");
						var_array_result_distribution_nok_url+=("$var_release_url");
					else
						# Construct valid distribution return filtered.
						var_array_result_distribution_valid+=("$var_release_version");
						var_array_result_distribution_valid_url+=("$var_release_url");
						var_count_release_valid="$((var_count_release_valid+1))";
						# First and only definition of url & date.
						if [[ -z "$var_release_url_valid_last" || "$var_release_url_valid_last" == "" ]]; then var_release_url_valid_last="$var_release_url"; fi;
						if [[ -z "$var_release_date_valid_last" || "$var_release_date_valid_last" == "" ]]; then var_release_date_valid_last="$var_release_date"; fi;
					fi;
					# Test : line checked.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 ${var_release_check}:5 ${var_release_url_display}:40 ${var_release_note}:16" -ACTION "checked";
					fi;
					var_count_release_all="$((var_count_release_all+1))";
				done;
				# Final release display output.
				if [ ${#var_array_result_distribution_valid[@]} -le 0 ]; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "${COLOR[RED_LIGHT]}no valid versions to display.${COLOR[STOP]}";
					fi;
				else
					if [[ -n ${var_array_result_distribution_valid[*]:1} ]]; then
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]} ${var_array_result_distribution_valid[*]:1}"
					else
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]}";
					fi;
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "latest stable versions are '${var_array_result_distribution_valid_display}'.";
					fi;
				fi;
				# Final release CSV output.
				echo -e "${var_distribution_name},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
				# Test : report need previous data.
				if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]] && Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
					var_return_backup_release="$(echo "$var_return_backup" | cut -d "," -f2)";
					var_return_backup_source="$(echo "$var_return_backup" | cut -d "," -f10)";
					# Test : new release, soure changed.
					if [[ "$var_return_backup_release" != "${var_array_result_distribution_valid[0]}" || "$var_return_backup_source" != "current" ]]; then
						if ! Add_Report -DISTRO "${var_distribution_name}" -RELEASE "${var_array_result_distribution_valid[0]}:${var_return_backup_release}" -SOURCE "current:${var_return_backup_source}"; then
							Stop_App -CODE 1 -TEXT "report can't be generated.";
						fi;
					fi;
				fi;
				# RàZ nok distribution.
				var_array_result_distribution_nok=();
				var_array_result_distribution_nok_url=();
				# RàZ valid distribution.
				var_array_result_distribution_valid=();
				var_array_result_distribution_valid_display="";
				var_array_result_distribution_valid_url=();
				var_release_date_valid_last="";
				var_release_url_valid_last="";
				var_return_backup="";
			else
				# Test : BACKUP.
				if [[ "${PARAM[BACKUP]}" == true ]]; then
					# Test : backup need previous data.
					if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
						var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
						# Test : pattern.
						if [[ -n "$var_return_backup" ]]; then
							echo -e "${var_return_backup}" >> "${PARAM[OUTPUT]}";
							sed -i "/^${var_distribution_name}/s/,current/,archive/" "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty, content recovered from backup.${COLOR[STOP]}";
							fi;
							# Test : report need previous data.
							if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]]; then
								var_return_backup_source="$(echo "$var_return_backup" | cut -d "," -f10)";
								# Test : source changed.
								if [[ "$var_return_backup_source" != "archive" ]]; then
									if Add_Report -DISTRO "${var_distribution_name}" -SOURCE "archive:${var_return_backup_source}"; then
										if [[ "${PARAM[QUIET]}" != true ]]; then
											echo -e "report added.";
										fi;
									else
										Stop_App -CODE 1 -TEXT "report can't be added.";
									fi;
								fi;
							fi;
						else
							echo -e "${var_distribution_name},,,,,,,,,current" >> "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous data in backup file, default content pushed.${COLOR[STOP]}";
							fi;
						fi;
					else
						echo -e "${var_distribution_name},,,,,,,,,current" >> "${PARAM[OUTPUT]}";
						if [[ "${PARAM[QUIET]}" != true ]]; then
							echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous backup file, default content pushed.${COLOR[STOP]}";
						fi;
					fi;
				else
					# Empty record.
					echo -e "${var_distribution_name},,,,,,,,,current" >> "${PARAM[OUTPUT]}";
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and data backup disabled, default content pushed.${COLOR[STOP]}";
					fi;
				fi;
			fi;
		done;
		# Final CSV output to JSON output.
		Convert_CSVToJSON;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;