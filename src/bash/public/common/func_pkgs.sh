#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_pkgs.sh               | #
# |__________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Test_Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Test_Software() {
	# Some definition.
	local var_result;
	local var_name;
	local var_type;
	#local var_action; // SC2034 disable.
	#local var_depend; // SC2034 disable.
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-ACTION)
				#var_action="$2" // SC2034 disable.
				shift 2
				;;
			-DEPEND)
				#var_depend=true // SC2034 disable.
				shift
				;;
			*)
				shift
				;;
		esac
	done
	# Tests : type software.
	if [[ "$var_type" == "program" ]]; then
		# TMP : import OverDeploy routine.
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-program";
		fi;
	elif [[ "$var_type" == "service" ]]; then
		# Test : systemd check.
		# SRC : https://stackoverflow.com/a/53640320/2704550.
		if [[ $(systemctl list-units --all --type service --full --no-legend "${var_name}.service" | sed 's/^\s*//g' | cut -f1 -d' ') == "${var_name}.service" ]]; then
			var_result="available";
		else
			var_result="fail-service";
		fi;
	elif [[ "$var_type" == "command" ]]; then
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-command";
		fi;
	else
		# tmp default.
		var_result="fail-unknow";
	fi;
	echo "$var_result";
}


# Start_Software.
#	DESCRIPTION : test effective restarting of a service.
#	RETURN : code.
function Start_Software() {
	# Some definition.
	local var_name;
	local var_type;
	local var_command;
	local var_user;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-COMMAND)
				var_command="$2"
				shift 2
				;;
			-USER)
				var_user="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done
	# Test : daemon, service.
	if [[ "$var_type" == "daemon" ]]; then
		if $var_command &> /dev/null; then return 0; else return 1; fi;
	elif [[ "$var_type" == "service" ]]; then
		su -l "$var_user" -c "systemctl restart ${var_name}";
		# Test : service state.
		if su -l "$var_user" -c "systemctl is-active --quiet ${var_name}"; then return 0; else return 1; fi;
	fi;
}