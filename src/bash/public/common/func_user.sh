#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_user.sh               | #
# |__________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test-Root.
#	DESCRIPTION : check root/sudo.
#	RETURN : int.
function Test-Root {
	if [[ "$(id -u)" -eq 0 ]]; then
		return 0;
	elif groups | grep -qE '\bsudo\b|\bwheel\b'; then
		return 0;
	else
		return 1;
	fi;
}