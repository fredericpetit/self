#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2329
# shellcheck disable=SC2034
#  _____________________________________________
# |                                             | #
# |               func_generic.sh               | #
# |_____________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : everything generic for all APPs.


# DATE.
declare -A DATE;
DATE["END"]="";
# y-m-d : jekyll file name markdown.
DATE["CURRENT_DATE"]="$(date "+%Y-%m-%d")";
# d-m-y - h:m:s : jekyll inside markdown file.
DATE["FULL_TIMESTAMP"]="$(date "+%Y-%m-%d %H:%M:%S")";
DATE["COMPACT_TIMESTAMP"]="$(date "+%Y%m%d_%H%M%S")";
DATE["FORMATTED_DATE_TIME"]="$(date "+%d/%m/%Y - %Hh%M")";


# COLOR.
declare -A COLOR;
COLOR[BOLD]='\e[1m';
COLOR[RED_LIGHT]='\e[1;38;5;160m';
COLOR[GREEN_LIGHT]='\e[1;38;5;34m';
COLOR[GREEN_DARK]='\e[1;38;5;28m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;179m';
COLOR[YELLOW_DARK]='\e[1;38;5;178m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;208m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';
COLOR[STOP]='\e[0m';
COLOR[TICK]="[${COLOR[GREEN_LIGHT]}✔${COLOR[STOP]}]";
COLOR[CROSS]="[${COLOR[RED_LIGHT]}✘${COLOR[STOP]}]";


# Write_Script.
#	DESCRIPTION : custom output of the APP.
#	RETURN : string.
function Write_Script() {
	# Some definition.
	local var_text;
	local var_code;
	local var_check;
	local var_checked;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-CODE)
				var_code="$2"
				shift 2
				;;
			-CHECK)
				var_check=true
				shift
				;;
			-CHECKED)
				var_checked=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then 
		# Tests : state, change former line.
		if [ -n "$var_check" ]; then
			echo -ne "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		elif [ -n "$var_checked" ]; then
			if [[ "$var_code" -eq 1 ]]; then
				echo -e "\\r ${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[CROSS]} ERROR : $var_text";
			elif [[ "$var_code" -eq 0 ]]; then
				echo -e "\\r ${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[TICK]} $var_text";
			fi;
		else
			echo -e "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		fi;
	fi;
}


# Get_Help.
#	DESCRIPTION : usage of the APP.
#	RETURN : string.
function Get_Help() {
	# Some definition.
	# requirement.
	local var_requirement;
	local var_requirement_text;
	IFS='#' read -ra var_requirement <<< "${APP[REQUIREMENT]}";
	for element in "${var_requirement[@]}"; do
		var_requirement_text+="$(echo -e "\t${element}\n ")";
	done;
	# param.
	local var_param;
	local var_param_text;
	IFS=' ' read -ra var_param <<< "${APP[PARAM]}";
	for element in "${var_param[@]}"; do var_param_text+="$element "; done;
	local var_param_dsc;
	local var_param_dsc_count;
	local var_param_dsc_text;
	IFS='#' read -ra var_param_dsc <<< "${APP[PARAM_DSC]}";
	local var_param_dsc_count=0
	for element in "${var_param_dsc[@]}"; do
		var_param_dsc_text+="$(echo -e "\t-${var_param[$var_param_dsc_count]} [ value ]\n\t\t${element}\n ")";
		var_param_dsc_count="$((var_param_dsc_count+1))";
	done;
	# option.
	local var_option;
	local var_option_text;
	IFS=' ' read -ra var_option <<< "${APP[OPTION]}";
	for element in "${var_option[@]}"; do var_option_text+="$element "; done;
	local var_option_dsc;
	local var_option_dsc_count;
	local var_option_dsc_text;
	IFS='#' read -ra var_option_dsc <<< "${APP[OPTION_DSC]}";
	local var_option_dsc_count=0
	for element in "${var_option_dsc[@]}"; do
		var_option_dsc_text+="$(echo -e "\t-${var_option[$var_option_dsc_count]}\n\t\t${element}\n ")";
		var_option_dsc_count="$((var_option_dsc_count+1))";
	done;
	# note.
	local var_note;
	local var_note_text;
	IFS='#' read -ra var_note <<< "${APP[NOTE]}";
	for element in "${var_note[@]}"; do
		var_note_text+="$(echo -e "\t${element}\n ")";
	done;
	# changelog.
	local var_changelog;
	local var_changelog_text;
	IFS='#' read -ra var_changelog <<< "${APP[CHANGELOG]}";
	for element in "${var_changelog[@]}"; do
		var_changelog_text+="$(echo -e "\t${element}\n ")";
	done;
	Restore_IFS;
cat <<EOUSAGE

SYNOPSIS
	${APP[NAME]} - ${APP[DESCRIPTION]}

HOME
	${APP[URL]}

REQUIREMENT
$var_requirement_text
UTILISATION
	${APP[NAME]} PARAM [ $var_param_text] OPTION [ $var_option_text]

PARAM
$var_param_dsc_text
OPTION
$var_option_dsc_text
EXAMPLE
	${APP[EXAMPLE]}

TO-DO
	${APP[TODO]}

NOTE
$var_note_text
CREDIT & THANKS
	${APP[CREDIT]}

CHANGELOG
$var_changelog_text
METADATA
	Author : ${APP[AUTHOR]}
	Version : ${APP[VERSION]}
	License : ${APP[LICENSE]}

EOUSAGE
}


# Test_Param.
#	DESCRIPTION : test arguments.
#	RETURN : string.
function Test_Param() {
	# Some definition.
	local var_param;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PARAM)
				var_param="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests :param mispelled, empty.
	if [[ "$var_param" == "-"* ]]; then
		Stop_App  -CODE 1 -TEXT "bad parameter (cant' starting by '-').";
	elif [[ -z "$var_param" ]]; then
		Stop_App  -CODE 1 -TEXT "bad parameter (cant' be empty).";
	fi;
}


# Get_StartingApp.
#	DESCRIPTION : script starter pack.
#	RETURN : string.
function Get_StartingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Title.
		echo -e "${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: ${CONF[DATE_START]} ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
		# Place & soft env.
		echo -e "- work in '${CONF[PATH]}'.";
		if [ -n "$SNAP" ]; then
			echo -e "- Snap environment detected.";
		elif [ -n "$FLATPAK_ID" ]; then
			echo -e "- Flatpak environment detected.";
		fi;
		echo "";
	fi;
	# Test : requirement.
	if [[ -n ${APP[REQUIREMENT]} && ${APP[REQUIREMENT]} != "x" ]]; then
		# Some definition.
		local var_element;
		local var_type;
		local var_name;
		local var_dependencie;
		local var_software;
		IFS='#';
		# Loop : requirement.
		for var_element in ${APP[REQUIREMENT]}; do
			# Space delimiter.
			var_type=$(echo "$var_element" | cut -d ' ' -f 1);
			var_name=$(echo "$var_element" | cut -d ' ' -f 2);
			var_dependencie=$(echo "$var_element" | cut -d ' ' -f 3);
			var_software=$(Test_Software -NAME "$var_name" -TYPE "$var_type");
			# Test : software.
			if [[ "$var_software" == "available" ]]; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- requirement '${var_type}' ${var_name} available.";
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- requirement '${var_type}' ${var_name} not available (error '${var_software}').";
				fi;
				# Test : dependencie.
				if [[ "$var_dependencie" == "mandatory" ]]; then
					Stop_App  -CODE 1 -TEXT "'${var_type}' ${var_name} mandatory.";
				fi;
			fi;
		done;
		Restore_IFS;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Output (data backup).
	if [[ "${PARAM[BACKUP]}" == true ]]; then
		# Test : output date name mode, output custom name mode (two files).
		if [[ "${PARAM[OUTPUT]}" =~ ${DEFAULT[OUTPUT]}\.(csv|xls|xlsx|json|txt)$ ]]; then
			# Test : get previous output old.
			if [[ -n "${PARAM[OUTPUT_BACKUP]}" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) in '${PARAM[OUTPUT_BACKUP]}'.";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (date name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (date name mode).";
				fi;
			fi;
		else
			# Test : move previous output to old.
			if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT]}"; then
					mv "${PARAM[OUTPUT]}" "${PARAM[OUTPUT_BACKUP]}";
					# Test : move all file formats.
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.json"; then
						mv "${PARAM[OUTPUT]%.csv}.json" "${PARAM[OUTPUT_BACKUP]%.csv}.json";
					fi;
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) moved to '${PARAM[OUTPUT_BACKUP]}'.";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (custom name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (custom name mode).";
				fi;
			fi;
		fi;
		if [[ "${PARAM[QUIET]}" != true && -z "${PARAM[OUTPUT]}" ]]; then echo ""; fi;
	fi;
	# Output (main).
	if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" && "${PARAM[RAW]}" != true ]]; then
		if Test_File -PATH "${PARAM[OUTPUT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current output to '${PARAM[OUTPUT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "output can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Report.
	if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]]; then
		if Test_File -PATH "${PARAM[REPORT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- report to '${PARAM[REPORT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "report can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
}


# Get_EndingApp.
#	DESCRIPTION : script ending pack.
#	RETURN : string.
function Get_EndingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Some definition.
		local var_date_start;
		local var_date_end;
		local var_date_end_seconds;
		var_date_start=$(date -d "$(date "+%Y-%m-%d %H:%M:%S")" "+%s");
		var_date_end=$(date "+%s");
		var_date_end_seconds=$((var_date_end - var_date_start));
		echo -e "\n${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: executed in ${var_date_end_seconds} seconds ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
	fi;
}


# Stop_App.
#	DESCRIPTION : end program.
#	RETURN : string and code.
function Stop_App() {
	# Some definition.
	local var_code;
	local var_text;
	local var_quiet;
	local var_help;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CODE)
				var_code="$2"
				shift 2
				;;
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-QUIET)
				var_quiet=true
				shift
				;;
			-HELP)
				var_help=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : splash error.
	if [[ "$var_quiet" != true && -n "$var_text" ]]; then
		Write_Script -TEXT "EXIT ERROR ! ${var_text}";
	fi;
	# Test : splash man.
	if [[ "$var_help" == true ]]; then Get_Help; fi;
	wait;
	exit "$var_code";
}


# Debug_Start.
#	DESCRIPTION : debug app.
function Debug_Start() { set -x; }


# Debug_Stop.
#	DESCRIPTION : debug app.
function Debug_Stop() { set +x; }


# Add_Book.
#	DESCRIPTION : construct associative array.
function Add_Book() {
	# Some definition.
	local var_name="$1";
	# Test : book exists.
	if ! declare -p "$var_name" &>/dev/null; then declare -gA "$var_name"; fi;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		local var_mode;
		local var_current_index;
		local var_current_release;
		local var_archive_index;
		local var_archive_release;
		local var_element_suffixes;
		local var_element_suffixes_var;
		local var_array;
		local var_array_keys;
		local var_array_suffixes;
		# mode, indexes & releases.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release");
		# Loop : var_suffix key.
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			var_element_suffixes_var="var_${var_element_suffixes}";
			declare "${var_element_suffixes_var}"="${var_element}_${var_element_suffixes}";
		done;
		# Construct index & release values from Get_Distribution.
		IFS=',' read -ra var_array <<< "$var_data";
		Restore_IFS;
		declare -a var_array_keys=("$var_mode" "$var_current_index" "$var_current_release" "$var_archive_index" "$var_archive_release");
		# Loop var_suffix value.
		for i in "${!var_array_keys[@]}"; do
			eval "${var_name}[\"${var_array_keys[$i]}\"]=\"${var_array[$i]}\"";
		done;
	elif [[ "$var_name" == "TMP" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		eval "${var_name}[\"${var_element}\"]=\"${var_data}\"";
	fi;
}


# Get_Book.
#	DESCRIPTION : get associative array.
#	RETURN : string or code.
function Get_Book() {
	# Some definition.
	local var_name;
	local var_element;
	local var_data;
	local var_array_suffixes;
	local var_element_suffixes;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-ELEMENT)
				var_element="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Loop: var_suffix key.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release")
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			if [[ "$var_data" == "$var_element_suffixes" ]]; then var_result="${DISTRIBUTION[${var_element}_$var_element_suffixes]}"; break; fi;
		done;
	fi;
	# Test : return.
	if [[ -n "$var_result" && "$var_result" != "" ]]; then echo "$var_result"; else return 1; fi;
}


# Remove_Book.
#	DESCRIPTION : remove associative array.
function Remove_Book() {
	# Some definition.
	local var_name="$1";
	unset "$var_name";
}


# Remove_DuplicateInString.
#	DESCRIPTION : remove duplicate value in string.
#	RETURN : string.
function Remove_DuplicateInString() {
	# Some definition.
	local var_string;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	echo "$var_string" | tr ' ' '\n' | sort | uniq | xargs;
}


# Search_InArray.
#	DESCRIPTION : search if value exists in array.
#	RETURN : code.
function Search_InArray() {
	# Some definition.
	local var_search;
	local var_array;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-SEARCH)
				var_search="$2"
				shift 2
				;;
			-ARRAY)
				var_array="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : need all param.
	if [ -z "$var_search" ] || [ -z "$var_array" ]; then return 1; fi;
	# IFS for delimit array.
	IFS=" " read -ra var_array_read <<< "$var_array";
	Restore_IFS;
	# Loop : search in array.
	for element in "${var_array_read[@]}"; do
		if [ "$element" == "$var_search" ]; then return 0; fi;
	done;
	return 1;
}


# Measure_MaxInArray.
#	DESCRIPTION : calculate longest word in array.
#	RETURN : string.
function Measure_MaxInArray() {
	# Some definition.
	local var_count;
	local var_array;
	local var_word;
	local var_length;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		var_count=0;
		for element in "${!var_array[@]}"; do
			var_word="${var_array[$element]}";
			var_length=${#var_word};
			if [ "$var_length" -gt "$var_count" ]; then var_count="$var_length"; fi;
		done;
		echo "$var_count";
	fi;
}


# Set_RepeatCharacter.
#	DESCRIPTION : calculate how many same character repeat needed.
#	RETURN : string.
function Set_RepeatCharacter() {
	# Some definition.
	local var_character;
	local var_count;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-CHARACTER)
				var_character="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : if arg empty.
	if [ -z "$var_character" ] || [ -z "$var_count" ]; then
		return 1;
	else
		var_result="";
		for ((i=0; i < "$var_count"; i++)); do
			var_result+="$var_character";
		done;
	fi;
	echo "$var_result";
}


# Limit_StringLength.
#	DESCRIPTION : limit number of character in a string.
#	RETURN : string.
#	TODO : replace -DIRECTION by -POSITION.
function Limit_StringLength() {
	# Some definition.
	local var_string;
	local var_count;
	local var_count_before;
	local var_count_after;
	local var_direction;
	local var_spacer;
	local var_spacer_length;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			-DIRECTION)
				var_direction="$2"
				shift 2
				;;
			-SPACER)
				var_spacer="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some defaults.
	if [ -z "$var_direction" ]; then var_direction="after"; fi;
	if [ -z "$var_spacer" ]; then var_spacer="(...)"; fi;
	var_spacer_length=${#var_spacer};
	# Test : if arg empty.
	if [ -z "$var_string" ] || [ -z "$var_count" ]; then
		return 1;
	else
		# Test : cut needed.
		if [ ${#var_string} -gt "$var_count" ]; then
			# Add spacer length to cut count.
			var_count_before=$(( ${#var_string}-var_count+var_spacer_length+1 ));
			var_count_after=$(( var_count-var_spacer_length ));
			# Tests : sub place.
			if [ "$var_direction" = "before" ]; then
				var_result+="$var_spacer";
				var_result+=$(echo "$var_string" | cut -c "$var_count_before"-"${#var_string}");
			elif [ "$var_direction" = "after" ]; then
				var_result+=$(echo "$var_string" | cut -c 1-"$var_count_after");
				var_result+="$var_spacer";
			fi;
			echo "$var_result";
		else
			echo "$var_string";
		fi;
	fi;
}


# Get_TableHeader.
#	DESCRIPTION : display a table index.
#	RETURN : string.
function Get_TableHeader() {
	# Some definition.
	local var_array;
	local var_element;
	local var_param;
	local var_param_length;
	local var_dot_length;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a name param and the max character count a value of this param can be in the rows.
		for element_header in "${!var_array[@]}"; do
			var_element="${var_array[$element_header]}";
			var_param=$(echo "$var_element" | cut -d ":" -f1);
			var_param_length="${#var_param}";
			var_dot_length=$(echo "$var_element" | cut -d ":" -f2);
			# Calculate how many characters to add more.
			var_param_length_more=$(( var_dot_length-var_param_length ));
			var_dot_length_more=$(( var_param_length-var_dot_length ));
			# Ready to construct header.
			var_line1+="$var_param";
			var_line1+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_param_length_more");
			var_line1+="  ";
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length");
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length_more");
			var_line2+="  ";
		done;
		echo -e "$var_line1";
		echo -e "$var_line2";
	fi;
}


# Get_TableRow.
#	DESCRIPTION : display a table row.
#	RETURN : string.
function Get_TableRow() {
	# Some definition.
	local var_array;
	local var_action;
	local var_element;
	local var_value;
	local var_value_length;
	local var_max;
	local var_line;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			-ACTION)
				var_action="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a value, the max character count this value can be.
		for element_row in "${!var_array[@]}"; do
			var_element="${var_array[$element_row]}";
			var_value=$(echo "$var_element" | cut -d ":" -f1);
			var_max=$(echo "$var_element" | cut -d ":" -f2);
			# Test : tick cross.
			if [[ "$var_value" == "tick" || "$var_value" == "cross" ]]; then
				if [[ "$var_value" == "tick" ]]; then
					var_value="${COLOR[TICK]}";
				elif [[ "$var_value" == "cross" ]]; then
					var_value="${COLOR[CROSS]}";
				fi;
				var_value_length=3;
			# TODO
			# elif [[ "$var_value" == *"tick"* || "$var_value" == *"cross"* ]]; then
			else
				var_value_length="${#var_value}";
			fi;
			# Calculate how many space to add more.
			var_value_length_more=$(( var_max-var_value_length ));
			# Ready to construct row.
			var_line+="$var_value";
			var_line+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_value_length_more");
			var_line+="  ";
		done;
		# Test : replace line.
		if [[ "$var_action" == "check" ]]; then
			echo -ne "$var_line";
		else
			echo -e "\\r$var_line";
		fi;
	fi;
}