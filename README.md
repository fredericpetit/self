# :sun_with_face: Welcome to my Self ! :sun_with_face:

_Personnal main development repository for utilities with common functions._
(click on the links to find out more details)

### /org/bash work :

![distro-family](assets/img/distro-family.png)

<details>
<summary><b>Click here to show model tree for each single project.</b></summary>

```

project
│
└─── data
│   │   ...
│
└─── dev
│   │
│   └─── core
│   │   │   ...
│   │
│   └─── man
│   │   │   project.X
│   │   │   project.md
│   │
│   └─── standalone
│       │   ...
│
└─── prod
    │
    └─── package
    │   │
    │   └─── debian
    │   │   │   ...
    │   │
    │   └─── flatpak
    │   │   │    ...
    │   │
    │   └─── snapcraft
    │       │    ...
    │
    └─── standalone
        │   ...

```

</details>


---

## 1. [:yellow_heart: distro-tracker](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-tracker)

**distro-tracker** is the IT monitoring tool for find out latest stable versions of GNU/Linux & BSD distributions.

:white_check_mark: Available in stable 1.1.1 version, in source script, Debian & Snapcraft packages.

- **Main features :**
  - 25 distributions - linux kernel included - to track
  - using multiple regular expressions
  - releases versions & releases downloadable links
  - standardizes the output into CSV, JSON, or YAML files
  - intern backup system

---

## 2. [:computer: distro-url](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-url)

**distro-url** generate Distro Tracker URLs for analysis.

:white_check_mark: Available in stable 1.0.0 version, in source script.

- **Main features :**
  - listing in CSV, JSON or YAML
  - datas never empty for security

---

## 3. [:computer: distro-reporter](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-reporter)

**distro-reporter** analyze Distro Tracker output for report new versions (with their download URLs) or source changes.

:white_check_mark: Available in stable 1.0.0 version, in source script, Debian & Snapcraft packages.

- **Main features :**
  - data auto-discovery
  - generate JSON or YAML reports

---

## 4. [:computer: distro-publisher](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-publisher)

**distro-publisher** treat Distro Reporter output.

:white_check_mark: Available in stable 1.0.0 version, in source script.

- **Main features :**
  - data auto-discovery
  - transform the reports into Facebook posts, Jekyll posts, etc ...

---

## 5. [:computer: distro-converter](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-reporter)

**distro-converter** export Distro Tracker output in multiple formats.

:warning: :construction: :construction: :construction: :warning: no stable version yet.

- **Main features :**
  - transform from and in CSV, JSON, YAML, HTML or Markdown
  - customize output, like list or table style for HTML

---

## 6. [:computer: distro-widget](https://gitlab.com/fredericpetit/self/-/tree/main/org/bash/distro-widget)

**distro-widget** set a fancy widget with Conky.

:white_check_mark: Available in stable 1.0.0 version, in source script, Debian & Snapcraft packages.

- **Main features :**
  - generates some stats with Distro Tracker datas integration
  - tiny service to refresh datas

---


🚀 **Take advantage of these tools today** to turn your ideas into reality and improve your daily efficiency !