# EN










# FR

## Généraliste.

1. Respect des recommandations par https://www.shellcheck.net/.
1. Toujours retourner un code ou afficher une chaîne de caractères/entier.
1. `[[ ]]` est plus fort que `[ ]`.
1. Restaurer l'IFS original avec la fonction `Restore_IFS`.
1. Par défaut pour un dossier, ajouter un caractère de séparation `/` ou `\` dans le path avec `${CONF[SEPARATOR]}` qui stocke la bonne valeur suivant GNU/Linux, BSD ou Windows.

## Nommages.

1. Nommage des **fonctions** selon les verbes suggérés par Microsoft : https://learn.microsoft.com/fr-fr/powershell/scripting/developer/cmdlet/approved-verbs-for-windows-powershell-commands.
1. Utiliser la convention de nommage `var_*` pour les **variables** , et ne pas utiliser de noms en majuscules (sauf pour les tableaux associatifs), car elles sont "réservées" pour l'utilisation par le shell.
1. Utiliser la convention de nommage `func_*` pour les **fichiers** contenant des fonctions réutilisables (dans `/common`).

## Tableaux (Bash)

### Tableaux indicés.
```
# Boucle avec les valeurs directement.
for value in "${array[@]}"; do
	echo "Value: ${value}.";
done;
```
- ${array[@]} donne la liste des valeurs contenues dans le tableau array.

### Tableaux associatifs / dictionnaires.
```
# Boucle avec les indexs.
for index in "${!array[@]}"; do
	echo "Index: ${index}, Value: ${array[$index]}.";
done;
```
- ${!array[@]} donne la liste des index du tableau array.

# Sources.

- https://xavki.blog/gitlab-tutorials-et-formation/
- https://stackoverflow.com/a/8123841/2704550
- https://blog.jbriault.fr/quelques-astuces-avec-gitlab-ci/
- https://www.kgaut.net/snippets/2022/gitlab-ci-passer-un-job-en-fonction-du-message-de-commit.html
- https://neron-joseph.medium.com/tag-your-git-repo-using-gitlabs-ci-cd-pipeline-a7963c2274d2