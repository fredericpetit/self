document.addEventListener("DOMContentLoaded", () => {
	const table = document.querySelector("#table-sortable");
	const headers = table.querySelectorAll("th.sortable");

	headers.forEach(header => {
		const button = header.querySelector("button");

		button.addEventListener("click", () => {
		const column = header.dataset.column;
		const order = header.dataset.order;

		const tbody = table.querySelector("tbody");
		const rows = Array.from(tbody.querySelectorAll("tr"));

		const sortedRows = rows.sort((a, b) => {
			const aText = a.querySelector(`td:nth-child(${getColumnIndex(column, headers) + 1})`).textContent.trim();
			const bText = b.querySelector(`td:nth-child(${getColumnIndex(column, headers) + 1})`).textContent.trim();

			if (!isNaN(aText) && !isNaN(bText)) {
			// Sort numerically
			return order === "asc" ? aText - bText : bText - aText;
			}

			// Sort alphabetically
			return order === "asc"
			? aText.localeCompare(bText)
			: bText.localeCompare(aText);
		});

		// Reorder rows in the table
		tbody.innerHTML = "";
		sortedRows.forEach(row => tbody.appendChild(row));

		// Toggle the sort order and change button icon
		header.dataset.order = order === "asc" ? "desc" : "asc";
		updateSortIcon(button, order);
		});
	});

	// Helper to get the column index from the headers
	function getColumnIndex(column, headers) {
		return Array.from(headers).findIndex(header => header.dataset.column === column);
	}

	// Update the sort icon depending on the order
	function updateSortIcon(button, order) {
		const icon = button.querySelector("i");
		icon.classList.remove(order === "asc" ? "bi-arrow-down-up" : "bi-arrow-up-down");
		icon.classList.add(order === "asc" ? "bi-arrow-up-down" : "bi-arrow-down-up");
	}
});