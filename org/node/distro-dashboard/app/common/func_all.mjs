// Importing.
// main.
import { CONF } from '../app.mjs';
// file management.
import path from 'path';
import os from 'os';
import { promises as fs } from 'fs';
import { readFile } from 'fs/promises';
// communication.
import https from 'https';
import { pipeline } from "stream/promises";
// execution.
import { exec } from 'child_process';



// =========================== #
//          FUNCTIONS          #

/**
* Get_GlobalOsVersion.
* DESCRIPTION : Retrieves the name and version of the current Linux distribution 
* by parsing the /etc/os-release file.
*
* @returns {Promise<Object|null>} - Resolves with an object containing the distribution name, 
* version, and version ID, or null in case of an error.
*/
export async function Get_GlobalOsVersion() {
	try {
		if (os.platform() === 'linux') {
			// Some definition.
			// Handle Linux distribution information.
			const data = await readFile('/etc/os-release', 'utf8');
			const osInfo = Object.fromEntries(
				data.split('\n').filter(line => line.includes('=')).map(line => {
					const [key, value] = line.split('=');
					return [key, value.replace(/"/g, '')];
				})
			);
			return {
				name: osInfo.NAME.replace(/GNU\/Linux/g, "").replace(/\s+/g, ""),
				version: osInfo.VERSION_ID
			};
		} else if (os.platform() === 'freebsd' || os.platform() === 'darwin') {
			// Some definition.
			// Handle BSD and macOS systems.
			const version = os.release(); // Kernel version.
			const name = os.platform() === 'darwin' ? 'macOS' : 'FreeBSD';
			return {
				name: name,
				version: version, // BSD does not have a separate version ID.
			};
		} else {
			Log_Global('ERROR : unsupported OS finded.');
			return {
				name: os.type(),
				version: os.release()
			};
		}
	} catch (error) {
		Log_Global('ERROR : OS Version not finded : ', error.message);
		return {
			name: "Unknown",
			version: "Unknown"
		};
	}
}

/**
 * Find_GlobalLatestMatchingOs.
 * DESCRIPTION : Find the latest version of a matching distribution.
 * 
 * @param {Object} dataTracker - The tracker data containing distributions.
 * @param {Object} dataSelf - The current OS information.
 * @returns {Object} - An object containing latestInitialVersion and latestRewrittenVersion.
 */
export function Find_GlobalLatestMatchingOs(dataTracker, dataSelf) {
	// Normalize distribution name (remove "GNU/Linux" and spaces, lowercase).
	const cleanedName = dataSelf.name.toLowerCase();
	// Find matching distribution.
	const matchingDistro = dataTracker.distro.find(
		(distro) => distro.name.replace(/\s+/g, "").toLowerCase() === cleanedName
	);
	// Get latest version if found.
	const latestInitialVersion = matchingDistro ? matchingDistro.releases?.latest?.version || null : null;
	// Rewrite latest version for Debian (extract only digits before first dot).
	const latestRewrittenVersion = (dataSelf.name.toLowerCase().includes("debian") && latestInitialVersion)
		? latestInitialVersion.split(".")[0]
		: latestInitialVersion;
	return { latestInitialVersion, latestRewrittenVersion };
}

/**
 * Get_GlobalSoftwareVersion.
 * DESCRIPTION : Retrieves the current version of a software by running its `--version` command.
 *
 * @param {string} filePath - Path to the software binary.
 * @returns {Promise<string>} - Resolves with the version string.
 */
export function Get_GlobalSoftwareVersion(filePath) {
	if (!filePath || typeof filePath !== 'string') {
		const errorMessage = 'Invalid file path provided to Get_GlobalSoftwareVersion.';
		Log_Global(`ERROR : ${errorMessage}`);
		return Promise.reject(new Error(errorMessage));
	}
	return new Promise((resolve, reject) => {
		exec(`${filePath} --version`, (error, stdout, stderr) => {
			// Tests : check for execution or stderr errors.
			if (error) {
				Log_Global(`ERROR : Failed to execute "${filePath} --version" : ${error.message}`);
				return reject(new Error(`Error executing ${filePath} --version.`));
			}
			if (stderr) {
				Log_Global(`ERROR : "${filePath} --version" produced stderr : ${stderr.trim()}`);
				return reject(new Error(`Error retrieving version for ${filePath}: ${stderr.trim()}`));
			}
			// Trim and resolve the version output.
			//Log_Global(`INFO : Successfully retrieved version for ${filePath}.`);
			resolve(stdout.trim());
		});
	});
}

/**
 * Get_GlobalLogFilePath.
 * DESCRIPTION : Retrieves the log file path by its name from the configuration.
 *
 * @param {string} fileName - Name of the log file.
 * @returns {string|null} - Full path to the log file, or null if not found.
 */
export function Get_GlobalLogFilePath(fileName) {
	return CONF.logs.find((logPath) => logPath.includes(fileName)) || null;
}

/**
 * Log_Global.
 * DESCRIPTION : Appends a message to the application log file.
 *
 * @param {string} message - The log message to write.
 */
export async function Log_Global(message) {
	try {
		await fs.appendFile(CONF.log, `${new Date().toISOString()} - ${message}\n`);
	} catch (error) {
		console.error(`ERROR : Failed to write to log file : ${error.message}`);
	}
}

/**
 * Send_GlobalCommand.
 * DESCRIPTION : Executes a shell command and returns the output or an error.
 *
 * @param {string} command - Shell command to execute.
 * @returns {Promise<string>} - Resolves with the stdout of the command.
 */
export function Send_GlobalCommand(command) {
	return new Promise((resolve, reject) => {
		exec(command, (error, stdout, stderr) => {
			if (error) {
				Log_Global(`ERROR : Failed to execute command : ${error.message}`);
				return reject(new Error(stderr));
			}
			resolve(stdout.trim());
		});
	});
  }
  
/**
 * Test_GlobalFile.
 * DESCRIPTION : Checks if a file exists at the given path.
 *
 * @param {string} filePath - Path to the file.
 * @returns {Promise<boolean>} - Resolves with true if the file exists, false otherwise.
 */
export async function Test_GlobalFile(filePath) {
	try {
		await fs.access(filePath);
		return true;
	} catch {
		return false;
	}
}  

/**
 * Get_GlobalFileInfo.
 * DESCRIPTION : Retrieves file information (size, owner, permissions, version, etc.).
 *
 * @param {string} filePath - Path to the file.
 * @returns {Promise<Object>} - Resolves with file information.
 */
export async function Get_GlobalFileInfo(filePath) {
	// Some definition.
	let version = null;
	try {
		const stats = await fs.stat(filePath);
		// Test : check if the file is a binary in `/var/node/bin` to fetch version.
		if (filePath.startsWith('/var/node/bin')) {
			try {
				version = await Get_GlobalSoftwareVersion(filePath);
			} catch (error) {
				Log_Global(`ERROR : Failed to retrieve version for ${filePath} : ${error.message}`);
				version = 'error-version';
			}
		}
		return {
			exists: true,
			size: stats.size,
			owner: stats.uid,
			group: stats.gid,
			permissions: stats.mode,
			modified: stats.mtime,
			version
		};
	} catch (error) {
		// Log the error and return default values if the file does not exist or another error occurs.
		Log_Global(`ERROR : Failed to retrieve file info for ${filePath} : ${error.message}`);
		return {
			exists: false,
			size: null,
			owner: null,
			group: null,
			permissions: null,
			modified: null,
			version: null
		};
	}
}

/**
 * Get_GlobalDistant.
 * DESCRIPTION : Downloads a distant resource (file, script, etc.) to a local temporary path.
 *
 * @param {string} fileUrl - URL of the distant resource.
 * @returns {Promise<boolean>} - Resolves with true if the file was successfully downloaded.
 */
export async function Get_GlobalDistant(fileUrl) {
	// Some definition.
	const fileName = path.basename(fileUrl);
	const destinationPath = fileUrl.endsWith(".sh") ? path.join("/tmp", path.basename(fileName, ".sh")) : path.join("/tmp", fileName);
	try {
		const response = await new Promise((resolve, reject) => {
			https.get(fileUrl, (res) => {
			if (res.statusCode === 200) {
				resolve(res);
			} else {
				reject(new Error(`Failed to download. Status : ${res.statusCode}`));
			}
			}).on("error", (err) => reject(err));
		});
		const fileStream = await fs.open(destinationPath, "w");
		await pipeline(response, fileStream.createWriteStream());
		await fileStream.close();
		Log_Global(`INFO : File downloaded to ${destinationPath}.`);
		return true;
	} catch (error) {
		Log_Global(`ERROR : Failed to download ${fileUrl} : ${error.message}`);
		return false;
	}
}