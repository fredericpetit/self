// Importing.
// common.
import {
	Get_GlobalOsVersion,
	Find_GlobalLatestMatchingOs,
	Get_GlobalFileInfo,
	Test_GlobalFile,
	Get_GlobalLogFilePath,
	Log_Global,
	Send_GlobalCommand,
	Get_GlobalDistant
} from './common/func_all.mjs';
// web.
import serveIndex from 'serve-index';
// file management.
import path from 'path';
import { promises as fs } from 'fs';
// communication.
import { createServer } from 'http';
// design.
import express from 'express';
import expressLayouts from 'express-ejs-layouts';
// websocket.
import { Server } from 'socket.io';
import chokidar from 'chokidar';
// execution.
import { fileURLToPath } from 'url';
// check.
import { param, validationResult } from 'express-validator';



// # ======================================== #
// #          PARAMETERS (variables)          #

// Init.
const app = express();
const server = createServer(app);
const io = new Server(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST"]
	}
});
const port = 3000;

export const CONF = {
	data: "/var/node/data",
	log: "/var/log/distro/dashboard.log",
	binaries: [
		'/var/node/bin/distro-downloader',
		'/var/node/bin/distro-tracker',
		'/var/node/bin/distro-reporter'
	],
	logs: [
		'/var/log/distro/downloader.log',
		'/var/log/distro/tracker.log',
		'/var/log/distro/reporter.log',
		'/var/log/distro/dashboard.log'
	]
};

// Watching.
const watcher = chokidar.watch(['/var/node/bin', '/var/log/distro'], {
	persistent: true,
	ignoreInitial: true
});

// Get the current directory.
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);



// ============================= #
//          APPLICATION          #

// Views.
app.set("view engine", "ejs");
app.set("views", "./views");

// Layouts.
app.use(expressLayouts);
app.set("layout", "../layouts/default.ejs");

// Statics.
app.use("/data", express.static(CONF.data, { extensions: ["csv", "yaml", "json", "md"] }));
app.use("/data", serveIndex(CONF.data, { icons: true }));
app.use("/assets/css", express.static(path.join(__dirname, "assets/css")));
app.use("/assets/js", express.static(path.join(__dirname, "assets/js")));
app.use("/assets/img", express.static(path.join(__dirname, "assets/img")));
app.use("/assets/vendor/socket.io", express.static(path.join(__dirname, "node_modules/socket.io/client-dist")));
app.use("/assets/vendor/bootstrap", express.static(path.join(__dirname, "node_modules/bootstrap/dist")));
app.use("/assets/vendor/bootstrap-table", express.static(path.join(__dirname, "node_modules/bootstrap-table/dist")));
app.use("/assets/vendor/fontawesome", express.static(path.join(__dirname, "node_modules/@fortawesome/fontawesome-free")));
app.use("/assets/vendor/ubuntu", express.static(path.join(__dirname, "node_modules/@fontsource/ubuntu")));

// Middleware generic.
app.use((req, res, next) => {
	res.locals.site = {
		title: "Distro Dashboard",
		description: "Distro Dashboard with Tracker :: Find out latest stable versions of GNU/Linux & BSD distributions.",
		revision: process.env.REVISION || "undefined"
	};
	next();
});

// Route Root.
app.get("/", (req, res) => {
	res.redirect("/tracker");
});

// Route - Tracker.
app.get("/tracker", async (req, res) => {
	try {
		// TMP
		const apiResponse = await fetch(`http://localhost:3000/api/load/tracker`);
		if (!apiResponse.ok) {
			Log_Global(`ERROR : Failed to fetch data from /api/load/tracker.`);
			return res.status(500).send("Error fetching data for the page.");
		}
		// Some definition.
		const dataTracker = await apiResponse.json();
		const dataSelf = await Get_GlobalOsVersion();
		//Log_Global(`INFO : dataTracker = ${JSON.stringify(dataTracker.distro, null)}`);
		// Get the latest version if a matching distribution is found.
		const matchingOs = Find_GlobalLatestMatchingOs(dataTracker, dataSelf);
		Log_Global(`INFO : Host : ${JSON.stringify(dataSelf.name, null)} - version ${JSON.stringify(matchingOs.latestInitialVersion, null)} vs version ${JSON.stringify(matchingOs.latestRewrittenVersion, null)}.`);
		res.render("tracker", {
			title: "Index",
			data: dataTracker,
			host: dataSelf || { name: "Unknown", version: "Unknown" },
			matching: { 
				latest: { 
					version: {
						initial: matchingOs.latestInitialVersion,
						rewritten: matchingOs.latestRewrittenVersion
					}
				} 
			}
		});
	} catch (error) {
		Log_Global(`ERROR : Unable to render index page : ${error.message}`);
		res.status(500).send("Error rendering the page.");
	}
});

// Route - Reporter.
app.get("/reporter", async (req, res) => {
	res.render("reporter", {
		title: "Reporter"
	});
});

// Route - Monitoring.
app.get("/monitoring", async (req, res) => {
	res.render("monitoring", {
		title: "Monitoring"
	});
});

// Route - Management.
app.get("/management", async (req, res) => {
	const binariesInfos = await Promise.all(
		CONF.binaries.map(async (file) => {
			const info = await Get_GlobalFileInfo(file);
			return { path: file, info };
		})
	);
	const logsInfos = await Promise.all(
		CONF.logs.map(async (file) => {
			const info = await Get_GlobalFileInfo(file);
			return { path: file, info };
		})
	);
	res.render("management", {
		title: "Management",
		binariesInfos: binariesInfos,
		logsInfos: logsInfos
	});
});

// Route API - placeholder.
app.get("/api", (req, res) => {
	return res.status(400).json({ error: "error" });
});

// Route API INFO - file.
app.get(
	"/api/info/:file",
	param('file')
		.isIn([
			'bin-downloader',
			'bin-tracker',
			'bin-reporter',
			'log-downloader',
			'log-tracker',
			'log-reporter',
			'log-dashboard',
		])
		.withMessage('Need valid filename.'),
	async (req, res) => {
		// Test : invalid.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Some definition.
		const fileName = req.params.file;
		let filePath;
		Log_Global(`INFO : API /api/info/:file request : ${fileName}.`);
		try {
			// Tests : -bin || -log || invalid.
			if (fileName.startsWith('bin-')) {
				filePath = path.join('/var/node/bin', 'distro-' + fileName.replace('bin-', ''));
			} else if (fileName.startsWith('log-')) {
				filePath = path.join('/var/log/distro', fileName.replace('log-', '') + '.log');
			} else {
				return res.status(400).json({ message: 'invalid filename' });
			}
			// Some definition.
			const fileInfo = await Get_GlobalFileInfo(filePath);
			// Test : data exists.
			if (!fileInfo.exists) {
				return res.status(404).json({ message: 'file not found', filePath });
			} else {
				// Websocket.
				io.emit('file-update', { path: filePath, size: fileInfo.size, modified: fileInfo.modified });
				return res.status(200).json({ filePath, fileInfo });
			}
		} catch (error) {
			Log_Global(`ERROR : Unable to retrieve file info for ${fileName} : ${error.message}`);
			res.status(500).json({ message: 'Error retrieving file information', error: error.message });
		}
	}
);

// Route API INSTALL - software.
app.get(
	'/api/install/:software',
	param('software')
		.isIn(['downloader', 'tracker', 'reporter'])
		.withMessage('Need valid software: "downloader", "tracker", or "reporter".'),
	async (req, res) => {
		// Test : invalid.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Some definition.
		const { software: fileName } = req.params;
		const remoteURL = `https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-${fileName}/dev/standalone/distro-${fileName}.sh`;
		const tempPath = path.join('/tmp', `distro-${fileName}`);
		const finalPath = path.join('/var/node/bin', `distro-${fileName}`);
		Log_Global(`INFO : API /api/install/:software request : ${fileName}.`);
		try {
			// Test : get distant.
			const downloadResult = await Get_GlobalDistant(remoteURL);
			if (!downloadResult) {
				Log_Global(`ERROR : Failed to download ${fileName} from ${remoteURL}`);
				return res.status(500).json({ message: `${fileName} software : download error.` });
			}
			// Copy, remove, permission.
			await fs.copyFile(tempPath, finalPath);
			await fs.unlink(tempPath);
			await fs.chmod(finalPath, 0o750);
			Log_Global(`INFO : ${fileName} software successfully installed.`);
			return res.status(200).json({ message: `${fileName} software installed successfully.` });
		} catch (error) {
			Log_Global(`ERROR : An error occurred during installation of ${fileName} : ${error.message}`);
			return res.status(500).json({
				message: `Error installing ${fileName}.`,
				error: error.message,
			});
		}
	}
);

// Route API REMOVE - software.
app.get(
	"/api/remove/:software",
	param('software')
		.isIn(['downloader', 'tracker', 'reporter'])
		.withMessage('Need valid software: "downloader", "tracker", or "reporter".'),
	async (req, res) => {
		// Test : invalid.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Some definition.
		const { software: fileName } = req.params;
		const filePath = path.join('/var/node/bin', `distro-${fileName}`);
		Log_Global(`INFO : API /api/remove/:software request : ${fileName}.`);
		try {
			await fs.unlink(filePath);
			Log_Global(`INFO : ${fileName} software successfully removed.`);
			return res.status(200).json({ message: `${fileName} software removed successfully.` });
		} catch (error) {
			if (error.code === 'ENOENT') {
				Log_Global(`ERROR : File not found for ${fileName} at ${filePath}.`);
				return res.status(404).json({ message: `File not found for ${fileName} at ${filePath}.` });
			}
			Log_Global(`ERROR : An error occurred while removing ${fileName} : ${error.message}`);
			return res.status(500).json({ message: `Error removing ${fileName}.`, error: error.message });
		}
	}
);

// Route API UPDATE - software.
app.get(
	'/api/update/:software',
	param('software')
		.isIn(['downloader', 'tracker', 'reporter'])
		.withMessage('Need valid software: "downloader", "tracker", or "reporter".'),
	async (req, res) => {
		// Test : invalid.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Some definition.
		const { software: fileName } = req.params;
		const remoteURL = `https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-${fileName}/dev/standalone/distro-${fileName}.sh`;
		const tempPath = path.join('/tmp', `distro-${fileName}`);
		const finalPath = path.join('/var/node/bin', `distro-${fileName}`);
		Log_Global(`INFO : API /api/update/:software request : ${fileName}`);
		try {
			// Test : get distant.
			const downloadResult = await Get_GlobalDistant(remoteURL);
			if (!downloadResult) {
				Log_Global(`ERROR : Failed to download ${fileName} from ${remoteURL}`);
				return res.status(500).json({ message: `${fileName} software : download error.` });
			}
			// Copy, remove, permission.
			await fs.copyFile(tempPath, finalPath);
			await fs.unlink(tempPath);
			await fs.chmod(finalPath, 0o750);
			Log_Global(`INFO : ${fileName} software successfully updated.`);
			return res.status(200).json({ message: `${fileName} software updated successfully.` });
		} catch (error) {
			Log_Global(`ERROR : An error occurred during update of ${fileName}: ${error.message}`);
			return res.status(500).json({
				message: `Error installing ${fileName}.`,
				error: error.message,
			});
		}
	}
);

// Route API DOWNLOAD - data.
app.get(
	"/api/download/:type/:data/:source", [
		param('type').isIn(['distant', 'local']).withMessage('Invalid type : "distant" or "local" expected.'),
		param('data').isIn(['tracker', 'reporter']).withMessage('Invalid data : "tracker" or "reporter" expected.'),
		param('source').isIn(['distrotracker.com', 'gitlab.com', 'local']).withMessage('Invalid data : "distrotracker.com", "gitlab.com" or "local" expected.')
	],
	async (req, res) => {
		// Test : invalid.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Some definition.
		const { type: typeName, data: dataName, source: sourceName } = req.params;
		const fileName = dataName === 'tracker' ? 'distro-tracker.current.json' : 'distro-reporter.current.json';
		const tempPath = path.join('/tmp', fileName);
		const finalPath = path.join('/var/node/data', typeName, fileName);
		// Tests : downloader || tracker.
		let command;
		if (typeName === "distant") {
			command = `/var/node/bin/distro-downloader --source ${sourceName} --format json --tool ${dataName} --output ${tempPath} --log`;
		} else if (typeName === "local") {
			command = `/var/node/bin/distro-tracker --format json --output ${tempPath}`;
		}
		Log_Global(`INFO : API /api/download/:type/:data/:source request : ${typeName}:${dataName}:${sourceName}`);
		try {
			await Send_GlobalCommand(command);
			if (!await Test_GlobalFile(tempPath)) {
				Log_Global(`ERROR : File ${fileName} not found at ${tempPath}.`);
				return res.status(500).json({ message: `File ${fileName} not found at ${tempPath}.` });
			}
			// Copy, remove, permission.
			await fs.copyFile(tempPath, finalPath);
			await fs.unlink(tempPath);
			await fs.chmod(finalPath, 0o750);
			Log_Global(`INFO : ${fileName} software successfully downloaded.`);
			return res.status(200).json({ message: `${fileName} software downloaded successfully.` });
		} catch (error) {
			Log_Global(`ERROR : An error occurred during download of ${fileName} : ${error.message}`);
			return res.status(500).json({ message: `Error downloading ${fileName}.`, error: error.message });
		}
	}
);

// Route API LOAD - data.
app.get(
	"/api/load/:data", [
		param('data').isIn(['tracker', 'reporter']).withMessage('Invalid data : "tracker" or "reporter" expected.')
	],
	async (req, res) => {
		// Test : invalid.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Some definition.
		const { data: dataName } = req.params;
		const basePath = `${CONF.data}`;
		const localFilePath = path.join(basePath, 'local', `distro-${dataName}.current.json`);
		const distantFilePath = path.join(basePath, 'distant', `distro-${dataName}.current.json`);
		Log_Global(`INFO : API /api/load/:data request : ${dataName}`);
		try {
			// Test : check for local file.
			if (await Test_GlobalFile(localFilePath)) {
				const data = await fs.readFile(localFilePath, 'utf8');
				const fileInfo = await Get_GlobalFileInfo(localFilePath);
				res.status(200).json(Object.assign(JSON.parse(data), { 
					modification: fileInfo.modified, 
					source: "local" 
				}));
				return;
			}
			// Test : check for distant file.
			if (await Test_GlobalFile(distantFilePath)) {
				const data = await fs.readFile(distantFilePath, 'utf8');
				const fileInfo = await Get_GlobalFileInfo(distantFilePath);
				res.status(200).json(Object.assign(JSON.parse(data), { 
					modification: fileInfo.modified, 
					source: "distant" 
				}));
				return;
			}
			Log_Global(`WARN : No '${localFilePath}' or '${distantFilePath}' data file found. Returning default.`);
			res.status(200).json({ distro: [] });
		} catch (error) {
			Log_Global(`ERROR : Loading ${dataName} data : ${error.message}`);
			res.status(500).json({ error: `Failed to reload ${dataName} data.` });
		}
	}
);

// Route API EMPTY - log.
app.get(
	"/api/empty/:log", [
		param('log').isIn(['downloader', 'tracker', 'reporter', 'dashboard']).withMessage('Invalid data: "downloader", "tracker", "reporter" or "dashboard" expected.')
	],
	async (req, res) => {
		// Test : invalid.
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		// Some definition.
		const fileName = req.params.log;
		try {
			// Some definition.
			const filePath = Get_GlobalLogFilePath(fileName);
			// Test : data exists.
			if (filePath) {
				// Overwrite the contents of the file with an empty string.
				await fs.writeFile(filePath, '', 'utf8');
				return res.status(200).json({ message: `${fileName} log file emptied.` });
			} else {
				Log_Global('ERROR : Log file not found for empty action.');
				return res.status(404).json({ error: 'Log file not found.' });
			}
		} catch (error) {
			Log_Global(`ERROR : Failed to empty log file : ${error.message}`);
			return res.status(500).json({ error: 'Failed to empty log file.' });
		}
	}
);

// Websocket watcher.
watcher
	.on('change', async (filePath) => {
		// Test : don't add content to main log.
		if (filePath !== CONF.log) {
			Log_Global(`INFO : File changed : ${filePath}.`);
		}
		// Some definition.
		let fileInfo = await Get_GlobalFileInfo(filePath);
		// Test : data exists.
		if (fileInfo.exists) {
			io.emit('file-update', { path: filePath, exists: fileInfo.exists, size: fileInfo.size, owner: fileInfo.owner, group: fileInfo.group, permissions: fileInfo.permissions, modified: fileInfo.modified, version: fileInfo.version });
		} else {
			Log_Global(`ERROR : File not found : ${filePath}.`);
		}
	})
	.on('add',  async (filePath) => {
		// Test : don't add content to main log.
		if (filePath !== CONF.log) {
			Log_Global(`INFO : File added : ${filePath}.`);
		}
		// Some definition.
		let fileInfo = await Get_GlobalFileInfo(filePath);
		// Test : data exists.
		if (fileInfo.exists) {
			io.emit('file-update', { path: filePath, exists: fileInfo.exists, size: fileInfo.size, owner: fileInfo.owner, group: fileInfo.group, permissions: fileInfo.permissions, modified: fileInfo.modified, version: fileInfo.version });
		} else {
			Log_Global(`ERROR : File (new) not found : ${filePath}.`);
		}
	})
	.on('unlink', (filePath) => {
		Log_Global(`INFO : File deleted : ${filePath}.`);
		io.emit('file-update', { path: filePath, exists: false, size: null, owner: null, group: null, permissions: null, modified: null, version: null });
	})
	.on('error', (error) => {
		Log_Global(`ERROR : Socket ${error.message}.`);
	});

// Execution.
server.listen(port, () => {
	Log_Global("===== Application available on 'http://localhost:" + port + "' =====");
});