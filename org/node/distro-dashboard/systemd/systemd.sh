#!/bin/bash

# Scheduled downloads of Distro Tracker & Distro Reporter webdatas.

if [[ "$EUID" -ne 0 ]]; then exit 1; fi;

BASE_URL="https://gitlab.com/fredericpetit/self/-/raw/main/org/node/distro-dashboard/systemd";
FILES=("distro-tracker-dl.service" "distro-tracker-dl.timer" "distro-reporter-dl.service" "distro-reporter-dl.timer");

for FILE in "${FILES[@]}"; do
	wget -O "/etc/systemd/system/$FILE" "$BASE_URL/$FILE";
	chmod 644 "/etc/systemd/system/$FILE";
done;

for UNIT in "${FILES[@]}"; do
	if [[ "$UNIT" == *.timer ]]; then
		systemctl enable --now "$UNIT";
	fi;
done;

systemctl daemon-reload;