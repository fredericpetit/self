import { createServer } from 'node:http';
import { exec } from 'node:child_process';
import { appendFileSync } from 'node:fs';
import { userInfo } from 'node:os';

const PORT = 3001;
const HOST = '127.0.0.1';
const LOG_FILE = '/var/log/distro/node.log';

// Utility function to log messages to the log file.
const log = (message) => {
	const timestamp = new Date().toISOString();
	const logMessage = `[${timestamp}] ${message}\n`;
	appendFileSync(LOG_FILE, logMessage, 'utf8');
};

// Log the name of the user running this script.
const user = userInfo().username;
log(`Server started by user: ${user}`);

// Create an HTTP server.
const server = createServer((req, res) => {
log(`Request received: ${req.method} ${req.url}`);
	// Handle the '/bin-update-downloader' & '/data-update-tracker' routes.
	if (req.method === 'GET' && req.url === '/bin-update-downloader') {
		// Handle requests to start the systemd service.
		exec('wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -O /usr/bin/distro-downloader https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-downloader/dev/bin/distro-downloader', (error, stdout, stderr) => {
			// Log the result of the command.
			log(`Request received: ${req.method} ${req.url}`);
			if (error) {
				// Log and respond with an error if the execution fails.
				log(`Error: ${error.message}`);
				res.writeHead(500, { 'Content-Type': 'text/plain' });
				res.end('nok-error');
				return;
			} else if (stderr) {
				// Log and respond with an error if there are warnings/errors.
				log(`Stderr: ${stderr}`);
				res.writeHead(500, { 'Content-Type': 'text/plain' });
				res.end('nok-stderr');
				return;
			} else {
				// Log and respond with success if the execution is successful.
				log(`Service started successfully: ${stdout}`);
				res.writeHead(200, { 'Content-Type': 'text/plain' });
				res.end('up');
			}
		});
	} else if (req.method === 'GET' && req.url === '/data-update-tracker') {
		// Handle requests to start the systemd service.
		exec('/usr/bin/distro-downloader --format json --tool tracker --output /data/distant/distro-tracker.current.json --log', (error, stdout, stderr) => {
			// Log the result of the command.
			log(`Request received: ${req.method} ${req.url}`);
			if (error) {
				// Log and respond with an error if the execution fails.
				log(`Error: ${error.message}`);
				res.writeHead(500, { 'Content-Type': 'text/plain' });
				res.end('nok-error');
				return;
			} else if (stderr) {
				// Log and respond with an error if there are warnings/errors.
				log(`Stderr: ${stderr}`);
				res.writeHead(500, { 'Content-Type': 'text/plain' });
				res.end('nok-stderr');
				return;
			} else {
				// Log and respond with success if the execution is successful.
				log(`Service started successfully: ${stdout}`);
				res.writeHead(200, { 'Content-Type': 'text/plain' });
				res.end('up');
			}
		});
	} else if (req.method === 'GET' && req.url === '/') {
		// Serve a simple response when accessing the root route.
		log(`Root route accessed.`);
		res.writeHead(200, { 'Content-Type': 'text/plain' });
		res.end('nok-valid');
	} else {
		// Handle all other routes (404 Not Found).
		log(`404 Not Found: ${req.method} ${req.url}`);
		res.writeHead(404, { 'Content-Type': 'text/plain' });
		res.end('nok-found');
	}
});

// Start the server and listen on the specified port and host.
server.listen(PORT, HOST, () => {
	log(`Server listening on ${HOST}:${PORT}`);
});