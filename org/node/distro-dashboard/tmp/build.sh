#!/bin/bash

# Test : root rights.
if [[ "$EUID" -ne 0 ]]; then exit 1; fi;

#####
echo -e "##### Volume.\n Create persistent volume.\n";
VOLUME_NAME="distro-data";
docker volume create "$VOLUME_NAME";

#####
echo -e "##### Volume.\n Create volume.\n\n";
IMAGE_NAME="distro:dashboard";
if ! docker image inspect "$IMAGE_NAME" &>/dev/null; then
    IMAGE_NAME="fredericpetit/distro:dashboard";
    docker pull "$IMAGE_NAME";
fi;
VOLUME_NAME="distro-data";
MOUNT_POINT=$(docker volume inspect "$VOLUME_NAME" -f '{{ .Mountpoint }}');
mkdir -p "$MOUNT_POINT/local";
mkdir -p "$MOUNT_POINT/distant";
chown -R 1000:1000 "$MOUNT_POINT";
chmod -R 750 "$MOUNT_POINT";

#####
echo -e "##### Container.\n Create container from image, and attach volume.\n\n";
CONTAINER_NAME="distro-container";
docker run --restart always -p 3000:3000 -d --name "$CONTAINER_NAME" -v "$MOUNT_POINT:/data" "$IMAGE_NAME";
#docker run -p 3000:3000 -d --name "$CONTAINER_NAME" -v "$MOUNT_POINT:/data" "$IMAGE_NAME";
CONTAINER_ID=$(docker ps -q --filter "ancestor=distro:dashboard");
echo -e "'#${CONTAINER_ID}' container launched.\n\n";

#####
echo -e "##### Services.\n On the Docker container host, create and activate the two systemd services and their respective timers.\n";
mkdir -p /var/log/distro;
BASE_URL="https://gitlab.com/fredericpetit/self/-/raw/main/org/node/distro-dashboard/systemd"
FILES=("distro-tracker-dl.service" "distro-tracker-dl.timer" "distro-reporter-dl.service" "distro-reporter-dl.timer");
for FILE in "${FILES[@]}"; do wget -O "/etc/systemd/system/$FILE" "$BASE_URL/$FILE"; done;
find /etc/systemd/system -type f \( -name 'distro-*.service' -o -name 'distro-*.timer' \) -exec chmod +x {} \;
for UNIT in "${FILES[@]}"; do systemctl enable "$UNIT" && systemctl start "$UNIT"; done;
systemctl daemon-reload;

#####
#echo -e "##### Lancement du test web.";
#if command -v firefox &> /dev/null; then
#	USER=$(grep ":1000:" /etc/passwd | cut -d: -f1);
#	su -l "$USER" -s /bin/bash -c "WAYLAND_DISPLAY=$WAYLAND_DISPLAY firefox http://localhost:3000/ &";
#fi;

#####
echo -e "##### Lancement du container.";
docker exec -it "$CONTAINER_NAME" /bin/bash