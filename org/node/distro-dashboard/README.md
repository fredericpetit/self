# README.

## Usage.

### Docker Hub.

```
docker pull fredericpetit/distro-dashboard:latest
```

### Deployment with Ansible.

```
wget --no-use-server-timestamps --timeout=10 --tries=2 -O deploy.yaml https://gitlab.com/fredericpetit/self/-/raw/main/org/node/distro-dashboard/deploy.yaml && \
	ansible-playbook -i inventory deploy.yaml --ask-become-pass;
```

### Docker Compose.

#### 1) Volumes & logs - check rights.

| host            | container       |
|-----------------|-----------------|
| /var/node/bin   | /var/node/bin   |
| /var/node/data  | /var/node/data  |
| /var/node/log   | /var/log/distro |
| /var/log/distro | -				|

```
VOLUMES=(
	"/var/node/bin"
	"/var/node/data/distant"
	"/var/node/data/local"
	"/var/node/log"
	"/var/log/distro/reporter.log"
	"/var/log/distro/tracker.log"
)
mkdir -p /var/log/distro;
for ITEM in "${VOLUMES[@]}"; do
	if [[ "$ITEM" == *.log ]]; then
		install -m 640 -o 1000 -g 1000 /dev/null "$ITEM";
	else
		install -d -m 750 -o 1000 -g 1000 -p "$ITEM";
	fi;
done;
```

#### 2) Composition.

```
wget --no-use-server-timestamps --timeout=10 --tries=2 -O docker-compose.yaml https://gitlab.com/fredericpetit/self/-/raw/main/org/node/distro-dashboard/docker-compose.yaml && \
  docker compose up -d
```

### Services (scheduled downloads of Distro Tracker & Distro Reporter webdatas) on the host.

```
wget --no-use-server-timestamps --timeout=10 --tries=2 -O systemd.sh https://gitlab.com/fredericpetit/self/-/raw/main/org/node/distro-dashboard/systemd/systemd.sh && \
	chmod +x systemd.sh && \
	./systemd.sh
```