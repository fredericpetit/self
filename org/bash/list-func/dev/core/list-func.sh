#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               list-func.sh               | #
# |__________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="list-func";
APP["SECTION"]="1";
APP["SHORTNAME"]="LF";
APP["SLOGAN"]="List custom functions used in a file.";
APP["DESCRIPTION"]="This open-source tool generates text list.";
APP["REQUIREMENTS"]="null";
APP["EXAMPLES"]="null";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="0.0.1|2024-12-12T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="0.0.1";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#mandatory#Single local file path, in comma-separated values.|path.|'PWD'.";
OPTIONS["QUIET"]="2#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="3#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="4#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["QUIET"]=false;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Search_LocalFunctions.
#	DESCRIPTION : list all functions used.
function Search_LocalFunctions() {
	# Some definition.
	local var_path;
	local var_matches;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Init.
	var_matches=$(grep -Po '(?<!function )\b[A-Z][a-zA-Z0-9]*_[A-Z][A-Za-z0-9]*' "$var_path" | grep -v '^# ' | grep -v '^[^a-z]*$' | sort | uniq | paste -sd ',' | sed 's/,/,\n/g');
	# Test : data exists.
	if [[ -n "$var_matches" && "$var_matches" != "" ]]; then
		echo -e "\n\n${var_matches}";
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_files;
	local var_file;
	local var_matches;
	# Init.
	IFS=',' read -ra var_files <<< "${PARAM["SOURCE"]}";
	Restore_IFS;
	# Loop : valuelist files.
	for var_file in "${var_files[@]}"; do
		# Test : data exists.
		if Test_File -PATH "$var_file"; then
			Write_Script -TEXT "Searching functions in '${var_file}' ...";
			var_matches=$(Search_LocalFunctions -PATH "$var_file");
			# Test : data exists.
			if [ -n "$var_matches" ]; then
				Write_Script -TEXT "Functions finded : ${var_matches}";
			else
				Write_Script -TEXT "No functions found.";
			fi;
		else
			Write_Script -TEXT "Impossible to search functions in '${var_file}'.";
		fi;
	done;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;