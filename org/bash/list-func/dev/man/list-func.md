<blockquote id="out2tag">



NAME



	list-func - List custom functions used in a file.





REQUIREMENTS



	x



 

SYNOPSIS



	list-func OPTIONS





DESCRIPTION



	This open-source tool generates text list.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path, in comma-separated values.

 		- state : mandatory.

 		- accepted value(s) : path.

 		- default value : 'PWD'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	null

		null



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	0.0.1 - 2024-12-12

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 0.0.1

	License : GPL-3.0-or-later





</blockquote>
