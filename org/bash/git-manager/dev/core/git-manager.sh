#!/bin/bash
# shellcheck source=/dev/null
#  ____________________________________________
# |                                            | #
# |               git-manager.sh               | #
# |____________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="git-manager";
APP["SECTION"]="1";
APP["SHORTNAME"]="GMNGR";
APP["SLOGAN"]="List, clone, update projects or tokens.";
APP["DESCRIPTION"]="This open-source tool communicates with Github and Gitlab.";
APP["REQUIREMENTS"]="program curl mandatory#program jq mandatory";
APP["EXAMPLES"]="null";
APP["TODO"]="WTF CREDENTIAL.";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="0.0.1|2025-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="0.0.1";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["ACTION"]="1#a#NAME#mandatory#Action to perform.|'list', 'clone', 'update' or 'token'.|no default.";
OPTIONS["PROVIDER"]="2#p#NAME#optionnal#Place where to perform.|'gitlab' or 'github'.|'gitlab'.";
OPTIONS["NAMESPACE"]="3#n#NAME#optionnal#Account where to perform.|string.|Same value as first part of '--credential' option.";
OPTIONS["CREDENTIAL"]="4#c#CREDENTIAL#mandatory#Login/ID & password/token of the product, delimited by ':'.|string.|no default.";
OPTIONS["TYPE"]="5#t#NAME#optionnal#Type of account to use.#'user' or 'org'.|'user'.";
OPTIONS["OUTPUT"]="6#o#FILEPATH#optionnal#Single local folder path to store the final result.|path.|'PWD'.";
OPTIONS["RAW"]="7#r#null#optionnal#Switch to 'raw' output mode.|null|'false' because disabled.";
OPTIONS["QUIET"]="8#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="9#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="10#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--action|-a)
			PARAM["ACTION"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--provider|-p)
			PARAM["PROVIDER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--namespace|-n)
			PARAM["NAMESPACE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--credential|-c)
			PARAM["CREDENTIAL"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -CONTAINS ":"
			shift 2
			;;
		--type|-t)
			PARAM["TYPE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["PROVIDER"]="gitlab";
DEFAULT["LOGIN"]=$(echo "${PARAM[CREDENTIAL]}" | cut -d ':' -f1);
DEFAULT["TYPE"]="user";
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}";
DEFAULT["QUIET"]=false;
# Some script definition.
# ACTION.
if ! [[ "${PARAM[ACTION]}" =~ ^(list|clone|update|token)$ ]]; then Stop_App -CODE 1 -TEXT "Invalid action for '--action' option."; fi;
# PROVIDER.
if [ -z "${PARAM[PROVIDER]}" ]; then
	PARAM["PROVIDER"]="${DEFAULT[PROVIDER]}";
elif ! [[ "${PARAM[PROVIDER]}" =~ ^(gitlab|github)$ ]]; then
	Stop_App -CODE 1 -TEXT "Invalid source for '--provider' option.";
fi;
# CREDENTIAL.
if [ -n "${PARAM[CREDENTIAL]}" ]; then
	PARAM["LOGIN"]=$(echo "${PARAM[CREDENTIAL]}" | cut -d ':' -f1);
	PARAM["PASSWORD"]=$(echo "${PARAM[CREDENTIAL]}" | cut -d ':' -f2);
fi;
# NAMESPACE.
if [ -z "${PARAM[NAMESPACE]}" ]; then PARAM["NAMESPACE"]="${PARAM[LOGIN]}"; fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Get_LocalGitList.
#	DESCRIPTION : get distant data.
function Get_LocalGitList() {
	# Some definition.
	local var_provider;
	local var_namespace;
	#local var_login;
	local var_password;
	local var_data;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PROVIDER)
				var_provider="$2"
				shift 2
				;;
			-NAMESPACE)
				var_namespace="$2"
				shift 2
				;;
			-PASSWORD)
				var_password="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : gitlab || github.
	if [[ "$var_provider" == "gitlab" ]]; then
		var_data=$(curl --silent --header "PRIVATE-TOKEN: ${var_password}" "https://gitlab.com/api/v4/users/${var_namespace}/projects");
		var_result=$(echo "$var_data" | jq -r -c '. | [.[].path] | join(", ")');
	elif [[ "$var_provider" == "github" ]]; then
		var_data=$(curl --silent --header "Accept: application/vnd.github+json" --header "Authorization: Bearer ${var_password}" "https://api.github.com/search/repositories?q=user:${var_namespace}+fork:true+created:>2000-01-01");
		var_result="$var_data";
	fi;
	# Test : good content.
	if [[ -n "$var_result" ]]; then
		echo "$var_result";
	else
		return 1;
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_data;
	# Init.
	Write_Script -TEXT "work from '${PARAM[PROVIDER]}'.";
	# Tests : list || clone || update || token.
	if [[ "${PARAM[ACTION]}" == "list" ]]; then
		Write_Script -TEXT "'${PARAM[PROVIDER]}' data list checking ..." -CHECK;
		var_data=$(Get_LocalGitList -PROVIDER "${PARAM[PROVIDER]}" -NAMESPACE "${PARAM[NAMESPACE]}" -PASSWORD "${PARAM[PASSWORD]}");
		# Test : data exists.
		if [[ -n "$var_data" && "$var_data" != "" ]]; then
			Write_Script -TEXT "Listing available : ${var_data}." -CODE 0 -CHECKED;
		else
			Write_Script -TEXT "Failing check." -CODE 1 -CHECKED;
		fi;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;