<blockquote id="out2tag">



NAME



	git-manager - List, clone, update projects or tokens.





REQUIREMENTS



	program 'curl' (mandatory)



 	program 'jq' (mandatory)



 

SYNOPSIS



	git-manager OPTIONS





DESCRIPTION



	This open-source tool communicates with Github and Gitlab.





OPTIONS



	-a NAME,

	--action NAME

 		Action to perform.

 		- state : mandatory.

 		- accepted value(s) : 'list', 'clone', 'update' or 'token'.

 		- default value : no default.



 	-p NAME,

	--provider NAME

 		Place where to perform.

 		- state : optionnal.

 		- accepted value(s) : 'gitlab' or 'github'.

 		- default value : 'gitlab'.



 	-n NAME,

	--namespace NAME

 		Account where to perform.

 		- state : optionnal.

 		- accepted value(s) : string.

 		- default value : Same value as first part of '--credential' option.



 	-c CREDENTIAL,

	--credential CREDENTIAL

 		Login/ID & password/token of the product, delimited by ':'.

 		- state : mandatory.

 		- accepted value(s) : string.

 		- default value : no default.



 	-t NAME,

	--type NAME

 		Type of account to use.

 		- state : optionnal.

 		- accepted value(s) : Type of account to use.

 		- default value : Type of account to use.



 	-o FILEPATH,

	--output FILEPATH

 		Single local folder path to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD'.



 	-r (no type),

	--raw (no type)

 		Switch to 'raw' output mode.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	null

		null



 

TO-DO



	WTF CREDENTIAL.





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	0.0.1 - 2025-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 0.0.1

	License : GPL-3.0-or-later





</blockquote>
