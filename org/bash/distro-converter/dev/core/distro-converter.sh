#!/bin/bash
# shellcheck source=/dev/null
#  _________________________________________________
# |                                                 | #
# |               distro-converter.sh               | #
# |_________________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-converter";
APP["SECTION"]="1";
APP["SHORTNAME"]="DC";
APP["SLOGAN"]="Standalone local export Distro Tracker output in multiple formats.";
APP["DESCRIPTION"]="This open-source tool transform from and in HTML, Markdown, CSV, YAML & JSON.";
APP["REQUIREMENTS"]="program mktemp mandatory#program jq mandatory#program yq mandatory";
APP["EXAMPLES"]="distro-tracker CSV to HTML list style :|${APP[NAME]} -s distro-tracker.current.csv -d list -o datas.html -f html#distro-tracker CSV to JSON :|${APP[NAME]} -s distro-tracker.current.csv -o datas.json -f json";
APP["TODO"]="Complete all possibilities.";
APP["NOTES"]="Compatible Distro Tracker >= 1.2.0.";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.0.0|2024-12-01T10:00:00+0200|Auto-discovery source. Add mktemp program in requirements and use it for temporary file security protection. CSV can be converted to HTML, Markdown, JSON & YAML.#0.0.1|2024-06-01T10:00:00+0200|Init, import from old distro-csv2html & distro-csv2md.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.0.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#optionnal#Single local file path or single local folder path to Distro Tracker datas.|path.|'PWD' with CSV, YAML & JSON testing files. JSON is the favorite format.";
OPTIONS["DESIGN"]="2#d#NAME#optionnal#Output style for HTML export.|'table' or 'list'.|'table'.";
OPTIONS["OUTPUT"]="3#o#FILEPATH#optionnal#Single local file path or single local folder path to store the final result.|path.|Same filename as '--source' option, with '--format' option value as extension name.";
OPTIONS["FORMAT"]="4#f#EXTENSION#optionnal#Single data format wanted for '--output'. Optionnal if '--output' option is a file with valid extension.|'html', 'csv', 'yaml' or 'json'.|no default.";
OPTIONS["RAW"]="5#r#null#optionnal#Switch to 'raw' output mode.|null|'false' because disabled.";
OPTIONS["QUIET"]="6#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="7#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="8#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--design|-d)
			PARAM["DESIGN"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--format|-f)
			PARAM["FORMAT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--raw|-r)
			PARAM["RAW"]=true
			shift
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["EXTENSIONS"]="yaml|csv|json";
DEFAULT["SOURCE"]="${PWD%[\\/]}${CONF[SEPARATOR]}";
DEFAULT["DESIGN"]="table";
DEFAULT["RAW"]=false;
DEFAULT["QUIET"]=false;
# Some script definition.
# DESIGN.
if [ -z "${PARAM[DESIGN]}" ]; then PARAM["DESIGN"]="${DEFAULT[DESIGN]}"; fi;
# RAW.
if [ -n "${PARAM[RAW]}" ]; then PARAM["QUIET"]=true; fi;
# SOURCE - If it's a folder, input format will be known only after auto-discovery.
if [ -z "${PARAM[SOURCE]}" ]; then PARAM["SOURCE"]="${DEFAULT[SOURCE]}"; fi;
# Tests : custom || distro-tracker.current.{yaml,json} || DR-????????_??????.{yaml,json} .
if ! Test_File -PATH "${PARAM[SOURCE]}"; then
	# Tests : name mode, date mode - Use pipe in '--EXTENSION' because regex treatment.
	var_param_source=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-tracker" -MODE "name");
	if [ -n "$var_param_source" ]; then
		PARAM["SOURCE"]="$var_param_source";
	else
		var_param_source=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-tracker" -TYPE "date");
		if [ -n "$var_param_source" ]; then
			PARAM["SOURCE"]="$var_param_source";
		else
			Stop_App -CODE 1 -TEXT "Need valid folder value for '--source' option.";
		fi;
	fi;
fi;
# Test : file.
if ! Test_File -PATH "${PARAM[SOURCE]}"; then
	Stop_App -CODE 1 -TEXT "Need valid file for '--source' option.";
fi;
# FORMAT.
if [[ -z "${PARAM[FORMAT]}" && "${PARAM[OUTPUT]}" =~ \.(html|md|csv|yaml|json)$ ]]; then
	PARAM["FORMAT"]="$(Get_FileExtension -PATH "${PARAM[OUTPUT]}")";
elif [ -z "${PARAM[FORMAT]}" ]; then
	Stop_App -CODE 1 -TEXT "Need value for '--format' option.";
fi;
if [[ ! "${PARAM[FORMAT]}" =~ ^(html|md|csv|yaml|json)$ ]]; then
	Stop_App -CODE 1 -TEXT "Invalid format : '${PARAM[FORMAT]}'.";
fi;
# RAW.
if [ -z "${PARAM[RAW]}" ]; then
	PARAM["RAW"]="${DEFAULT[RAW]}";
	# Tests : double check if output not defined after try.
	if [ -z "${PARAM[OUTPUT]}" ]; then
		# Define extension in pure Bash.
		var_param_source_filename="${PARAM[SOURCE]##*/}";
		var_param_source_filename="${var_param_source_filename%.*}";
		PARAM["OUTPUT"]="${var_param_source_filename}.${PARAM[FORMAT]}";
	fi;
	if [ -z "${PARAM[OUTPUT]}" ]; then
		Stop_App -CODE 1 -TEXT "Need value for '--output' option when '--raw' is false.";
	fi;
fi;
# SOURCE - Redefine requirements for user-friendly work.
if [[ "${PARAM["SOURCE"]}" != *.yaml || "${PARAM["FORMAT"]}" == "yaml" ]]; then
	APP["REQUIREMENTS"]="program mktemp mandatory#program jq mandatory";
fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# ConvertTo_LocalHtmlTable.
#	DESCRIPTION : construct Bootstrap HTML table.
function ConvertTo_LocalHtmlTable() {
	# Some definition - menu.
	local var_source;
	local var_format;
	local var_output;
	local var_raw;
	# Some definition - next.
	local var_data;
	local var_html_content;
	local var_head;
	local var_indexes;
	local var_index_1;
	local var_index_2;
	local var_index_3;
	local var_index_4;
	local var_index_5;
	local var_index_6;
	local var_href;
	local var_links;
	local var_text;
	local var_items;
	local var_date_check_seconds;
	local var_date_current_seconds;
	local var_date_difference_seconds;
	local var_date_difference_days;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-SOURCE) var_source="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			-OUTPUT) var_output="$2"; shift 2 ;;
			-RAW) var_raw=true; shift ;;
			*) shift ;;
		esac
	done;
	# Construct HTML data.
	var_html_content="<div class=\"table-responsive\">\n<table class=\"mb-0 table table-bordered table-striped\">";
	var_html_content+="\n\t<thead>\n\t\t<tr>";
	var_html_content+="\n\t\t\t<th scope=\"col\">fullname</th>";
	var_html_content+="\n\t\t\t<th scope=\"col\">latest</th>";
	var_html_content+="\n\t\t\t<th scope=\"col\">date</th>";
	var_html_content+="\n\t\t\t<th scope=\"col\">few latest</th>";
	var_html_content+="\n\t\t\t<th scope=\"col\">source</th>";
	var_html_content+="\n\t\t</tr>\n\t</thead>\n\t<tbody class=\"table-group-divider\">";
	# Tests : CSV, JSON, YAML.
	if [[ "$var_format" == "csv" ]]; then
		var_head="$(head -n 1 <"$var_source")";
		var_index_1="$(Get_PositionInArray -DATA "$var_head" -PATTERN "Fullname")";
		var_index_2="$(Get_PositionInArray -DATA "$var_head" -PATTERN "VersionLatest")";
		var_index_3="$(Get_PositionInArray -DATA "$var_head" -PATTERN "VersionLatestDate")";
		var_index_4="$(Get_PositionInArray -DATA "$var_head" -PATTERN "VersionValid")";
		var_index_5="$(Get_PositionInArray -DATA "$var_head" -PATTERN "UrlLatest")";
		var_index_6="$(Get_PositionInArray -DATA "$var_head" -PATTERN "UrlValid")";
		var_index_7="$(Get_PositionInArray -DATA "$var_head" -PATTERN "Source")";
		# 1 2 3 5 7 8 10.
		var_indexes=("$var_index_1" "$var_index_2" "$var_index_3" "$var_index_4" "$var_index_5" "$var_index_6" "$var_index_7");
		# Loop : Process Substitution CSV lines.
		while IFS=',' read -ra var_data; do
			var_html_content+="\n\t\t<tr>";
			var_html_content+="\n\t\t\t<td>${var_data[${var_indexes[0]}]}</td>";
			var_html_content+="\n\t\t\t<td><a target=\"_blank\" href=\"${var_data[${var_indexes[4]}]}\">${var_data[${var_indexes[1]}]}</a></td>";
			# Test : rolling release.
			if [[ "${var_data[${var_indexes[2]}]}" =~ [a-zA-Z] ]]; then
				var_html_content+="\n\t\t\t<td>${var_data[${var_indexes[2]}]}</td>";
			else
				var_date_check_seconds=$(date -d "${var_data[${var_indexes[2]}]}" +%s);
				var_date_current_seconds=$(date +%s);
				var_date_difference_seconds=$((var_date_current_seconds - var_date_check_seconds));
				var_date_difference_days=$((var_date_difference_seconds / 86400));
				# Test : freshness release.
				if [ "$var_date_difference_days" -le 31 ]; then
					var_html_content+="\n\t\t\t<td class=\"table-success\">${var_data[${var_indexes[2]}]}</td>";
				else
					var_html_content+="\n\t\t\t<td>${var_data[${var_indexes[2]}]}</td>";
				fi;
			fi;
			var_html_content+="\n\t\t\t<td>\n\t\t\t\t<ul>";
			IFS=' ' read -ra var_items <<< "${var_data[${var_indexes[3]}]}";
			IFS=' ' read -ra var_links <<< "${var_data[${var_indexes[5]}]}";
			# Loop : valuelist few latest.
			for (( i=0; i<${#var_items[@]}; i++ )); do
				var_href="${var_links[i]}";
				var_text="${var_items[i]}";
				var_html_content+="\n\t\t\t\t\t<li><a target=\"_blank\" href=\"$var_href\">$var_text</a></li>";
			done;
			var_html_content+="\n\t\t\t\t</ul>\n\t\t\t</td>";
			# Test : source release.
			if [ "${var_data[${var_indexes[6]}]}" == "archive" ]; then
				var_html_content+="\n\t\t\t<td class=\"table-warning\">${var_data[${var_indexes[6]}]}</td>";
			else
				var_html_content+="\n\t\t\t<td>${var_data[${var_indexes[6]}]}</td>";
			fi;
			var_html_content+="\n\t\t</tr>";
		done < <(tail -n +2 "$var_source");
		# RàZ.
		Restore_IFS;
	elif [[ "$var_format" == "yaml" ]]; then
		echo "TODO";
	elif [[ "$var_format" == "json" ]]; then
		echo "TODO";
	fi;
	# Finish HTML data.
	var_html_content+="\n\t</tbody>\n</table>\n</div>";
	# Test : show.
	if [[ "$var_raw" == true ]]; then
		echo -e "$var_html_content";
	else
		echo -e "$var_html_content" > "$var_output";
	fi;
}

# ConvertTo_LocalHtmlList.
#	DESCRIPTION : construct Bootstrap HTML list.
function ConvertTo_LocalHtmlList() {
	# Some definition - menu.
	local var_source;
	local var_output;
	local var_raw;
	# Some definition - next.
	local var_data;
	local var_html_content;
	local var_date_check_seconds;
	local var_date_current_seconds;
	local var_date_difference_seconds;
	local var_date_difference_days;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-SOURCE) var_source="$2"; shift 2 ;;
			-OUTPUT) var_output="$2"; shift 2 ;;
			-RAW) var_raw=true; shift ;;
			*) shift ;;
		esac
	done;
	# Construct html data.
	var_html_content="<div>\n<ol class=\"list-group\">";
	# Tests : CSV, JSON, YAML.
	if [[ "$var_format" == "csv" ]]; then
		# Loop : Process Substitution CSV all lines.
		while IFS=',' read -ra var_data; do
			var_html_content+="\n\t<li class=\"list-group-item d-flex justify-content-between align-items-start\">";
			var_html_content+="\n\t\t<div class=\"ms-2 me-auto\">";
			var_html_content+="\n\t\t\t<div class=\"fs-5 fw-bold\">${var_data[1]}</div>";
			var_html_content+="\n\t\t\t<p>all latest : ";
			IFS=' ' read -ra items <<< "${var_data[5]}";
			IFS=' ' read -ra links <<< "${var_data[8]}";
			# Loop : valuelist links.
			for (( j=0; j<${#items[@]}; j++ )); do
				href="${links[j]}";
				text="${items[j]}";
				var_html_content+="<a target=\"_blank\" href=\"$href\">$text</a>";
				if [[ $j -lt $((${#items[@]} - 1)) ]]; then var_html_content+=", "; fi;
			done;
			var_html_content+="</p>";
			var_html_content+="\n\t\t</div>";
			var_html_content+="\n\t\t<span class=\"fs-5 fw-bold badge text-bg-primary rounded-pill\">";
			var_html_content+="\n\t\t\t<a class=\"link-light\" target=\"_blank\" href=\"${var_data[7]}\">${var_data[2]}</a>";
			var_html_content+="\n\t\t</span>";
			var_html_content+="\n\t</li>";
		done < <(tail -n +2 "$var_source");
		Restore_IFS;
	elif [[ "$var_format" == "yaml" ]]; then
		echo "TODO";
	elif [[ "$var_format" == "json" ]]; then
		echo "TODO";
	fi;
	# Finish HTML data.
	var_html_content+="\n</ol>\n</div>";
	# Test : show.
	if [[ "$var_raw" == true ]]; then
		echo -e "$var_html_content";
	else
		echo -e "$var_html_content" > "$var_output";
	fi;
}

# ConvertTo_LocalMarkdown.
#	DESCRIPTION : construct Markdown content.
function ConvertTo_LocalMarkdown() {
	# Some definition - menu.
	local var_source;
	local var_format;
	local var_output;
	local var_raw;
	# Some definition - next.
	local var_data;
	local var_markdown_content;
	local var_head;
	local var_indexes;
	local var_index_1;
	local var_index_2;
	local var_index_3;
	local var_index_4;
	local var_index_5;
	local var_index_6;
	local var_items;
	local var_index_4_list;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-SOURCE) var_source="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			-OUTPUT) var_output="$2"; shift 2 ;;
			-RAW) var_raw=true; shift ;;
			*) shift ;;
		esac
	done;
	# Construct Markdown data.
	var_markdown_content="| fullname    | latest      | date        | few latest  | source      |\n";
	var_markdown_content+="|-------------|-------------|-------------|-------------|-------------|\n";
	# Tests : CSV || YAML || JSON.
	if [[ "$var_format" == "csv" ]]; then
		var_head="$(head -n 1 <"$var_source")";
		var_index_1="$(Get_PositionInArray -DATA "$var_head" -PATTERN "Fullname")";
		var_index_2="$(Get_PositionInArray -DATA "$var_head" -PATTERN "VersionLatest")";
		var_index_3="$(Get_PositionInArray -DATA "$var_head" -PATTERN "VersionLatestDate")";
		var_index_4="$(Get_PositionInArray -DATA "$var_head" -PATTERN "VersionValid")";
		var_index_5="$(Get_PositionInArray -DATA "$var_head" -PATTERN "Source")";
		# 1 2 3 5 10.
		var_indexes=("$var_index_1" "$var_index_2" "$var_index_3" "$var_index_4" "$var_index_5" "$var_index_6" "$var_index_5");
		# Loop : Process Substitution CSV lines.
		while IFS=',' read -ra var_data; do
			IFS=' ' read -ra var_items <<< "${var_data[${var_indexes[3]}]}";
			# Loop : valuelist few latest.
			for (( i=0; i<${#var_items[@]}; i++ )); do
				var_index_4_list+="${var_items[i]}, ";
			done;
			# Construct Markdown data.
			var_markdown_content+="| ${var_data[${var_indexes[0]}]} | ${var_data[${var_indexes[1]}]} | ${var_data[${var_indexes[2]}]} | ${var_index_4_list%, } | ${var_data[${var_indexes[4]}]} |\n";
			# RàZ.
			var_index_4_list="";
		done < <(tail -n +2 "$var_source");
		# RàZ.
		Restore_IFS;
	elif [[ "$var_format" == "yaml" ]]; then
		echo "TODO";
	elif [[ "$var_format" == "json" ]]; then
		echo "TODO";
	fi;
	# Test : show.
	if [[ "$var_raw" == true ]]; then
		echo -e "$var_markdown_content";
	else
		echo -e "$var_markdown_content" > "$var_output";
	fi;
}

# ConvertTo_LocalYaml.
#	DESCRIPTION : construct YAML content.
function ConvertTo_LocalYaml() {
	# Some definition - menu.
	local var_source;
	local var_format;
	local var_output;
	# Some definition - next.
	local var_output_tmp;
	local var_source_array;
	local var_source_element;
	local var_source_index;
	local var_data_imported;
	local var_data_rewrited_eval;
	local var_data_rewrited_map;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-SOURCE) var_source="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			-OUTPUT) var_output="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Tests : CSV, YAML.
	if [[ "$var_format" == "csv" ]]; then
		# Test : TMP security.
		var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.json");
		# Read CSV lines.
		IFS=',' read -ra var_source_array < "${var_source}";
		Restore_IFS;
		# Loop : Process Substitution CSV line.
		while IFS=',' read -ra var_source_element; do
			var_data_imported+="- \n";
			for var_source_index in "${!var_source_array[@]}"; do
				var_data_imported+="  ${var_source_array[$var_source_index]}: ${var_source_element[$var_source_index]}\n";
			done;
		done < <(tail -n +2 "${var_source}");
		Restore_IFS;
		# Import & rewrite.
		var_data_rewrited_eval=$(echo -e "$var_data_imported" | yq eval "{\"revision\": \"${DATE[DATETIME0]}\", \"distro\": .}" -);
		var_data_rewrited_map="$(echo "$var_data_rewrited_eval" | yq eval '
			.distro[] |= (
				.name = .Name |
				.fullname = .Fullname |
				.source = .Source |
				.releases = {
				"latest": {
					"version": (.VersionLatest | tostring),
					"date": .VersionLatestDate,
					"url": .UrlLatest
				},
				"all": ((.VersionAll // "" | tostring) | split(" ")),
				"valid_version": (.VersionValid // "" | tostring | split(" ")),
				"valid_url": (.UrlValid // "" | tostring | split(" ")),
				"nok_version": (.VersionNOK // "" | tostring | split(" ")),
				"nok_url": (.UrlNOK // "" | tostring | split(" "))
				} |
				. |= {
				"name": .name,
				"fullname": .fullname,
				"source": .source,
				"releases": .releases
				}
			)'
		)";
		if echo "$var_data_rewrited_map" | yq eval "
			.distro[] |= (
				.releases.valid = (
					( .releases.valid_version // [] ) as \$versions |
					( .releases.valid_url // [] ) as \$urls |
					( \$versions | to_entries | map({ \"version\": .value, \"url\": (\$urls[.key]) }))
				) |
				.releases.nok = (
					( .releases.nok_version // [] ) as \$versions |
					( .releases.nok_url // [] ) as \$urls |
					( \$versions | to_entries | map({ \"version\": .value, \"url\": (\$urls[.key]) }))
				) |
				del(.releases.valid_version, .releases.valid_url, .releases.nok_version, .releases.nok_url)
			)
		" > "${var_output_tmp}" 2>/dev/null; then
			# Test : data exists.
			if [[ -s "${var_output_tmp}" ]]; then
				if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
			else
				return 1;
			fi;
		else
			return 1;
		fi;
	elif [[ "$var_format" == "json" ]]; then
		echo "TODO";
	fi;
}

# ConvertTo_LocalJson.
#	DESCRIPTION : construct JSON content.
function ConvertTo_LocalJson() {
	# Some definition - menu.
	local var_source;
	local var_format;
	local var_output;
	# Some definition - next.
	local var_output_tmp;
	local var_data_imported;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-SOURCE) var_source="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			-OUTPUT) var_output="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Tests : CSV, YAML.
	if [[ "$var_format" == "csv" ]]; then
		# Test : TMP security.
		var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.json");
		# Import & rewrite.
		var_data_imported=$(jq -R --tab '{
		revision: "'"${DATE[DATETIME0]}"'",
		distro: [
			inputs | split(",") | {
				name: .[0],
				fullname: .[1],
				source: .[10],
				tmp: {
					all: .[4] | split(" ") | map({version: .}),
					valid: .[5] | split(" ") | map({version: .}),
					nok: .[6] | split(" ") | map({version: .})
				},
				releases: {
					latest: {version: .[2], date: .[3], url: .[7]},
					all: .[4] | split(" ") | map({version: ""}),
					valid: .[8] | split(" ") | map({version: "", url: .}),
					nok: .[9] | split(" ") | map({version: "", url: .})
				}
			}
		]}' < "${var_source}" 2>/dev/null || echo -n "");
		if echo "$var_data_imported" | jq --tab '.distro[] |= (
			.releases.all = [
				( range(0; (.releases.all | length)) as $i | .tmp.all[$i].version )
			] | 
			.releases.valid = [ 
				( range(0; (.releases.valid | length)) as $i | { version: .tmp.valid[$i].version, url: .releases.valid[$i].url } )
			] | 
			.releases.nok = [ 
				( range(0; (.releases.nok | length)) as $i | { version: .tmp.nok[$i].version, url: .releases.nok[$i].url } )
			] | 
			del(.tmp)
		)' > "${var_output_tmp}" 2>/dev/null; then
			# Test : data exists.
			if [[ -s "${var_output_tmp}" ]]; then
				if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
			else
				return 1;
			fi;
		else
			return 1;
		fi;
	elif [[ "$var_format" == "yaml" ]]; then
		echo "TODO";
	fi;
}

# Get_LocalDataLength.
#	DESCRIPTION : local generic function to get length of data.
function Get_LocalDataLength() {
	# Some definition - menu.
	local var_data var_format var_length=0;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-DATA) var_data="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Tests : CSV || YAML || JSON.
	case "$var_format" in
		csv) var_length="$(echo "$var_data" | head -n 2 | tail -n 1)"; echo "${#var_length}" ;;
		yaml) echo "$var_data" | yq '.distro | length' 2>/dev/null ;;
		json) echo "$var_data" | jq -r '.distro | length' 2>/dev/null ;;
		*) return 1 ;;
	esac
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_content;
	local var_length;
	local var_format_current;
	local var_format_wanted;
	# Some init.
	var_content="$(<"${PARAM[SOURCE]}")";
	var_format_wanted="${PARAM[FORMAT]}";
	# Init.
	Write_Script -TEXT "convert generation from '${PARAM[SOURCE]}'.";
	# Tests : JSON || YAML.
	var_format_current=$(Get_DataFormat -DATA "$var_content");
	if [[ "$var_format_current" =~ ^(csv|yaml|json)$ ]]; then
		var_length=$(Get_LocalDataLength -DATA "$var_content" -FORMAT "$var_format_current");
	else
		Stop_App -CODE 1 -TEXT "Invalid format for '--source' option.";
	fi;
	# Test : content exists with number control avoid null count.
	if [ -n "$var_length" ] && [[ "$var_length" =~ ^[0-9]+$ ]] && [[ "$var_length" -gt 0 ]]; then
		# Tests : html, md, csv, yaml, json.
		if [[ "$var_format_wanted" == "html" ]]; then
			if [[ "$var_format_current" == "csv" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
				# Test : style.
				if [[ "${PARAM[DESIGN]}" == "table" ]]; then
					# Test : raw.
					if [[ -n "${PARAM[RAW]}" && "${PARAM[RAW]}" == true ]]; then
						# Test : execute.
						ConvertTo_LocalHtmlTable -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current" -RAW;
					else
						# Test : execute.
						if ConvertTo_LocalHtmlTable -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current"; then
							Write_Script -TEXT "'${PARAM[SOURCE]}' content transformed.";
						else
							Write_Script -TEXT "ERROR : '${PARAM[SOURCE]}' can't be transformed.";
						fi;
					fi;
				else
					# Test : raw.
					if [[ -n "${PARAM[RAW]}" && "${PARAM[RAW]}" == true ]]; then
						# Test : execute.
						ConvertTo_LocalHtmlList -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current" -RAW;
					else
						# Test : execute.
						if ConvertTo_LocalHtmlList -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current"; then
							Write_Script -TEXT "'${PARAM[SOURCE]}' content transformed.";
						else
							Write_Script -TEXT "ERROR : '${PARAM[SOURCE]}' can't be transformed.";
						fi;
					fi;
				fi;
			elif [[ "$var_format_current" == "yaml" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			elif [[ "$var_format_current" == "json" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			fi;
		elif [[ "$var_format_wanted" == "md" ]]; then
			if [[ "$var_format_current" == "csv" ]]; then
				# Test : raw.
				if [[ -n "${PARAM[RAW]}" && "${PARAM[RAW]}" == true ]]; then
					# Test : execute.
					ConvertTo_LocalMarkdown -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current" -RAW;
				else
					# Test : execute.
					if ConvertTo_LocalMarkdown -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current"; then
						Write_Script -TEXT "'${PARAM[SOURCE]}' content transformed.";
					else
						Write_Script -TEXT "ERROR : '${PARAM[SOURCE]}' can't be transformed.";
					fi;
				fi;
			elif [[ "$var_format_current" == "yaml" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			elif [[ "$var_format_current" == "json" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			fi;
		elif [[ "$var_format_wanted" == "csv" ]]; then
			if [[ "$var_format_current" == "csv" ]]; then
				Write_Script -TEXT "same format.";
				rm "${PARAM[OUTPUT]}" 2>/dev/null;
			elif [[ "$var_format_current" == "yaml" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			elif [[ "$var_format_current" == "json" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			fi;
		elif [[ "$var_format_wanted" == "yaml" ]]; then
			if [[ "$var_format_current" == "csv" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
				# Test : raw.
				if [[ -n "${PARAM[RAW]}" && "${PARAM[RAW]}" == true ]]; then
					echo "raw";
				else
					# Test : execute.
					if ConvertTo_LocalYaml -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current"; then
						Write_Script -TEXT "'${PARAM[SOURCE]}' content transformed.";
					else
						Write_Script -TEXT "ERROR : '${PARAM[SOURCE]}' can't be transformed.";
					fi;
				fi;
			elif [[ "$var_format_current" == "yaml" ]]; then
				Write_Script -TEXT "same format.";
				rm "${PARAM[OUTPUT]}" 2>/dev/null;
			elif [[ "$var_format_current" == "json" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			fi;
		elif [[ "$var_format_wanted" == "json" ]]; then
			if [[ "$var_format_current" == "csv" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
				# Test : raw.
				if [[ -n "${PARAM[RAW]}" && "${PARAM[RAW]}" == true ]]; then
					echo "raw";
				else
					# Test : execute.
					if ConvertTo_LocalJson -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "$var_format_current"; then
						Write_Script -TEXT "'${PARAM[SOURCE]}' content transformed.";
					else
						Write_Script -TEXT "ERROR : '${PARAM[SOURCE]}' can't be transformed.";
					fi;
				fi;
			elif [[ "$var_format_current" == "yaml" ]]; then
				Write_Script -TEXT "convert from '${var_format_current}' to '${var_format_wanted}'";
			elif [[ "$var_format_current" == "json" ]]; then
				Write_Script -TEXT "same format.";
				rm "${PARAM[OUTPUT]}" 2>/dev/null;
			fi;
		fi;
	else
		# Test : data exists.
		if rm "${PARAM[OUTPUT]}" 2>/dev/null; then
			Write_Script -TEXT "convert removed because can't be generated from null data.";
		else
			Write_Script -TEXT "convert can't be generated from null data and can't be removed.";
		fi;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;