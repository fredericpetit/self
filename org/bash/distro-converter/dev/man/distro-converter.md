<blockquote id="out2tag">



NAME



	distro-converter - Standalone local export Distro Tracker output in multiple formats.





REQUIREMENTS



	program 'mktemp' (mandatory)



 	program 'jq' (mandatory)



 	program 'yq' (mandatory)



 

SYNOPSIS



	distro-converter [ OPTIONS ]





DESCRIPTION



	This open-source tool transform from and in HTML, Markdown, CSV, YAML & JSON.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path or single local folder path to Distro Tracker datas.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD' with CSV, YAML & JSON testing files. JSON is the favorite format.



 	-d NAME,

	--design NAME

 		Output style for HTML export.

 		- state : optionnal.

 		- accepted value(s) : 'table' or 'list'.

 		- default value : 'table'.



 	-o FILEPATH,

	--output FILEPATH

 		Single local file path or single local folder path to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : Same filename as '--source' option, with '--format' option value as extension name.



 	-f EXTENSION,

	--format EXTENSION

 		Single data format wanted for '--output'. Optionnal if '--output' option is a file with valid extension.

 		- state : optionnal.

 		- accepted value(s) : 'html', 'csv', 'yaml' or 'json'.

 		- default value : no default.



 	-r (no type),

	--raw (no type)

 		Switch to 'raw' output mode.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	distro-tracker CSV to HTML list style :

		distro-converter -s distro-tracker.current.csv -d list -o datas.html -f html



 	distro-tracker CSV to JSON :

		distro-converter -s distro-tracker.current.csv -o datas.json -f json



 

TO-DO



	Complete all possibilities.





NOTES



	Compatible Distro Tracker >= 1.2.0.



 

CREDITS & THANKS



	x





CHANGELOG



	1.0.0 - 2024-12-01

		Auto-discovery source. Add mktemp program in requirements and use it for temporary file security protection. CSV can be converted to HTML, Markdown, JSON & YAML.



 	0.0.1 - 2024-06-01

		Init, import from old distro-csv2html & distro-csv2md.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.0.0

	License : GPL-3.0-or-later





</blockquote>
