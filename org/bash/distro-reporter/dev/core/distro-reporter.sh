#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________________
# |                                                | #
# |               distro-reporter.sh               | #
# |________________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-reporter";
APP["SECTION"]="1";
APP["SHORTNAME"]="DR";
APP["SLOGAN"]="Analyze Distro Tracker output for report new versions (with their download URLs) or source changes.";
APP["DESCRIPTION"]="This open-source tool generates update report in JSON or YAML formats.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENTS"]="program mktemp mandatory#program jq mandatory#program yq mandatory";
APP["EXAMPLES"]="generate report with custom name mode in JSON format :|${APP[NAME]} -s test.current.csv -o test.report.json#generate report with date name mode in YAML format :|${APP[NAME]} -f yaml";
APP["TODO"]="null";
APP["NOTES"]="Compatible Distro Tracker >= 1.2.0.";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.1.0|2024-12-01T10:00:00+0200|Add mktemp program in requirements and use it for temporary file security protection. Add fullname information. Auto-discovery column name for CSV files.#1.0.0|2024-10-13T10:00:00+0200|Separate report & publish functions into distro-reporter & distro-publisher.#0.0.2|2024-06-01T10:00:00+0200|New '--action' parameter.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="1.1.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#optionnal#Single local file path or single local folder path to Distro Tracker data. Searches for the second most recent data relative to the source in order to perform the data comparison analysis.|path.|'PWD'.";
OPTIONS["OUTPUT"]="2#o#FILEPATH#optionnal#Single local file path or single local folder path to store the final result. JSON & YAML formats allowed. Use final '/' for set folder path with date name mode, or file path with custom name mode. Set '--format' option for folder path setting if YAML needed.|path.|'PWD/${APP[SHORTNAME]}-DATETIME.FORMAT' (date name mode).";
OPTIONS["FORMAT"]="3#f#EXTENSION#optionnal#Single data format.|'json' or 'yaml'.|'json'.";
OPTIONS["QUIET"]="4#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="5#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="6#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--format|-f)
			PARAM["FORMAT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["SOURCE"]="${PWD%[\\/]}${CONF[SEPARATOR]}";
DEFAULT["OUTPUT_NAME"]="${APP[SHORTNAME]}-${DATE[DATETIME1]}"
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}${DEFAULT[OUTPUT_NAME]}";
DEFAULT["FORMAT"]="json";
DEFAULT["QUIET"]=false;
# Some script definition.
# SOURCE.
if [ -z "${PARAM[SOURCE]}" ]; then PARAM["SOURCE"]="${DEFAULT[SOURCE]}"; fi;
# Test : direct file or folder.
if Test_File -PATH "${PARAM[SOURCE]}"; then
	# Tests : custom name mode, date name mode.
	if [[ "${PARAM["SOURCE"]}" == *".current."* ]]; then
		PARAM["SOURCE_CURRENT"]="${PARAM[SOURCE]}";
		var_param_source_archive_file="${PARAM["SOURCE"]//.current./.archive.}"
		# Test : archive exists.
		if Test_File -PATH "$var_param_source_archive_file"; then
			PARAM["SOURCE_ARCHIVE"]="$var_param_source_archive_file";
		else
			PARAM["SOURCE_ARCHIVE"]="";
		fi;
	else
		PARAM["SOURCE_CURRENT"]="${PARAM[SOURCE]}";
		PARAM["SOURCE_ARCHIVE"]="";
		# Test : current exists.
		if [[ "${PARAM[SOURCE_CURRENT]}" =~ ^(.*DT-[0-9]{8}_[0-9]{6})\.(csv|json|yaml)$ ]]; then
			# FIX OLD var_param_source_current_file_path=$(echo "${BASH_REMATCH[1]}" | sed 's/[0-9]\{8\}_[0-9]\{6\}$//');
			var_param_source_current_file_path="${BASH_REMATCH[1]//[0-9]{8}_[0-9]{6}/}"
			var_param_source_current_file_extension="${BASH_REMATCH[2]}";
			# Loop : auto-discovery - last file win data state.
			for var_param_source_archive_file in "${var_param_source_current_file_path}"????????_??????."${var_param_source_current_file_extension}"; do
				# Test : stop loop before saving path.
				if [[ "$var_param_source_archive_file" == "${PARAM["SOURCE_CURRENT"]}" ]]; then break; fi;
				PARAM["SOURCE_ARCHIVE"]="$var_param_source_archive_file";
			done;
		fi;
	fi;
else
	# Loop : extensions.
	declare -a var_param_source_extensions;
	var_param_source_extensions=("yaml" "csv" "json");
	for var_param_source_extension in "${var_param_source_extensions[@]}"; do
		# Loop : auto-discovery - last file win data state.
		for var_param_source_current_file in "${PARAM[SOURCE]}"*[a-zA-Z]*.current."${var_param_source_extension}"; do
			# Test : current exists.
			if Test_File -PATH "$var_param_source_current_file"; then
				PARAM["SOURCE_CURRENT"]="$var_param_source_current_file";
				var_param_source_archive_file="${PARAM["SOURCE_CURRENT"]//.current./.archive.}"
				# Test : archive exists.
				if Test_File -PATH "$var_param_source_archive_file"; then
					PARAM["SOURCE_ARCHIVE"]="$var_param_source_archive_file";
				else
					PARAM["SOURCE_ARCHIVE"]="";
				fi;
			fi;
		done;
	done;
	# Test : date name mode.
	if [ -z "${PARAM[SOURCE_CURRENT]}" ]; then
		# Loop : auto-discovery - last file win data state.
		for var_param_source_current_file in $(find "${PARAM[SOURCE]%[\\/]}" -type f -name "*-????????_??????.*" \( -iname "*.csv" -o -iname "*.json" -o -iname "*.yaml" \) 2>/dev/null | sort); do
			# Test : current exists.
			if Test_File -PATH "$var_param_source_current_file"; then
				# Avoid different extension.
				var_param_source_current_file_extension=$(Get_FileExtension -PATH "$var_param_source_current_file");
				PARAM["SOURCE_CURRENT"]="$var_param_source_current_file";
			fi;
		done;
		PARAM["SOURCE_ARCHIVE"]="";
		# Loop : auto-discovery - last file win data state.
		for var_param_source_archive_file in "${PARAM[SOURCE]%[\\/]}${CONF[SEPARATOR]}"*-????????_??????."${var_param_source_current_file_extension}"; do
			# Test : stop loop before saving path.
			if [[ "$var_param_source_archive_file" == "${PARAM["SOURCE_CURRENT"]}" ]]; then break; fi;
			PARAM["SOURCE_ARCHIVE"]="$var_param_source_archive_file";
		done;
	fi;
fi;
if [ -z "${PARAM[SOURCE_CURRENT]}" ]; then Stop_App -CODE 1 -TEXT "Need valid value for '--source' option."; fi;
if ! [[ "${PARAM[SOURCE_CURRENT]}" =~ \.(csv|json|yaml)$ ]]; then Stop_App -CODE 1 -TEXT "Invalid format for '--source' option."; fi;
if [[ -n "${PARAM[FORMAT]}" && "${PARAM[FORMAT]}" != "json" && "${PARAM[FORMAT]}" != "yaml" ]]; then
	Stop_App -CODE 1 -TEXT "Unknown format : '${PARAM[FORMAT]}'.";
fi;
# OUTPUT - Tests : exists, folder, file.
if [ -z "${PARAM[OUTPUT]}" ]; then
	# FORMAT & OUTPUT.
	if [[ -n "${PARAM[FORMAT]}" && ( "${PARAM[FORMAT]}" == "yaml" || "${PARAM[FORMAT]}" == "json" ) ]]; then 
		PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}.${PARAM[FORMAT]}";
	else
		PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}.${DEFAULT[FORMAT]}";
		PARAM["FORMAT"]="${DEFAULT[FORMAT]}";
	fi;
elif [[ "${PARAM[OUTPUT]}" == */ ]] && Test_Dir -PATH "${PARAM[OUTPUT]}"; then
	# FORMAT & OUTPUT.
	if [[ -n "${PARAM[FORMAT]}" && ( "${PARAM[FORMAT]}" == "yaml" || "${PARAM[FORMAT]}" == "json" ) ]]; then 
		 PARAM["OUTPUT"]="${PARAM[OUTPUT]}${DEFAULT[OUTPUT_NAME]}.${PARAM[FORMAT]}";
	else
		PARAM["OUTPUT"]="${PARAM[OUTPUT]}${DEFAULT[OUTPUT_NAME]}.${DEFAULT[FORMAT]}";
	fi;
else
	# FORMAT & OUTPUT.
	# Test : priority to '--format'.
	if [ -z "${PARAM[FORMAT]}" ] && [[ "${PARAM[OUTPUT]}" == *.json || "${PARAM[OUTPUT]}" == *.yaml ]]; then
		PARAM["FORMAT"]="${PARAM[OUTPUT]##*.}";
	fi;
	# Tests : avoid double extension.
	if [[ -n "${PARAM[FORMAT]}" && ( "${PARAM[FORMAT]}" == "yaml" || "${PARAM[FORMAT]}" == "json" ) ]]; then 
		PARAM["OUTPUT"]="${PARAM[OUTPUT]%.*}.${PARAM[FORMAT]}";
	else
		PARAM["OUTPUT"]="${PARAM[OUTPUT]}.${DEFAULT[FORMAT]}";
	fi;
fi;
# FORMAT - Redefine requirements for user-friendly work.
if [[ "${PARAM["SOURCE"]}" != *.yaml && "${PARAM[FORMAT]}" != "yaml" ]]; then
	APP["REQUIREMENTS"]="program jq mandatory";
fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# New_LocalReport.
#	DESCRIPTION : init report file with date, with temporary file security protection.
function New_LocalReport() {
	# Some definition.
	local var_output;
	local var_output_tmp;
	local var_format;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : TMP security.
	var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
	if Test_File -PATH "$var_output_tmp"; then
		# Tests : JSON, YAML.
		if [[ -n "$var_format" && "$var_format" == "json" ]]; then
			if jq --null-input --arg date "${DATE[DATETIME0]}" '{"revision": $date,"distro":[]}' > "${var_output_tmp}" 2>/dev/null; then
				# Test : data exists.
				if [[ -s "${var_output_tmp}" ]]; then
					if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
				else
					return 1;
				fi;
			else
				return 1;
			fi;
		elif [[ -n "$var_format" && "$var_format" == "yaml" ]]; then
			# Test : data success.
			if yq eval --null-input ".revision = \"${DATE[DATETIME0]}\" | .distro = []" > "${var_output_tmp}" 2>/dev/null; then
				# Test : data exists.
				if [[ -s "${var_output_tmp}" ]]; then
					if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
				else
					return 1;
				fi;
			else
				return 1;
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}

# Add_LocalReport.
#	DESCRIPTION : add or append content to report file, with temporary file security protection.
# todo : yaml, add or append.
function Add_LocalReport() {
	# Some definition.
	local var_name;
	local var_fullname;
	local var_state;
	local var_type;
	local var_data;
	local var_output;
	local var_format;
	local var_distro;
	local var_distro_archive_version;
	local var_distro_current_version;
	local var_distro_archive_url;
	local var_distro_current_url;
	local var_distro_archive_source;
	local var_distro_current_source;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-FULLNAME)
				var_fullname="$2"
				shift 2
				;;
			-STATE)
				var_state="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : json, yaml.
	if [[ "$var_format" == "json" ]]; then
		# Test : new distribution.
		if [[ "$var_state" == "new" ]]; then
			# Test : TMP security.
			var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
			if Test_File -PATH "$var_output_tmp"; then
				# Test : data success.
				if jq --arg distribution "$var_name" \
					--arg fullname "$var_fullname" \
					'if (.distro | map(select(.name == $distribution)) | length > 0) then
						.distro = (.distro | map(if .name == $distribution then .state = "new" else . end))
					else
						.distro += [{"name": $distribution, "fullname": $fullname, "state": "new"}]
					end' \
					"${var_output}" > "${var_output_tmp}" 2>/dev/null; then
					# Test : data exists.
					if [[ -s "${var_output_tmp}" ]]; then
						if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
					else
						return 1;
					fi;
				else
					return 1;
				fi;
			else
				return 1;
			fi;
		fi;
		# Tests : version, url, source.
		if [[ "$var_type" == "version" ]]; then
			# Test : TMP security.
			var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
			if Test_File -PATH "$var_output_tmp"; then
				var_distro_archive_version=$(echo "$var_data" | cut -d '|' -f1);
				var_distro_current_version=$(echo "$var_data" | cut -d '|' -f2);
				if jq --arg distribution "$var_name" \
					--arg fullname "$var_fullname" \
					--arg version_old "$var_distro_archive_version" \
					--arg version_new "$var_distro_current_version" \
					'if (.distro | map(select(.name == $distribution)) | length > 0) then
						.distro = (.distro | map(if .name == $distribution then .version.old = $version_old | .version.new = $version_new else . end))
					else
						.distro += [{"name": $distribution, "fullname": $fullname, "version": {"old": $version_old, "new": $version_new}}]
					end' \
					"${var_output}" > "${var_output_tmp}" 2>/dev/null; then
					# Test : data exists.
					if [[ -s "${var_output_tmp}" ]]; then
						if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
					else
						return 1;
					fi;
				else
					return 1;
				fi;
			else
				return 1;
			fi;
		elif [[ "$var_type" == "url" ]]; then
			# Test : TMP security.
			var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
			if Test_File -PATH "$var_output_tmp"; then
				var_distro_archive_url=$(echo "$var_data" | cut -d '|' -f1);
				var_distro_current_url=$(echo "$var_data" | cut -d '|' -f2);
				# Test : data success.
				if jq --arg distribution "$var_name" \
					--arg fullname "$var_fullname" \
					--arg url_old "$var_distro_archive_url" \
					--arg url_new "$var_distro_current_url" \
					'if (.distro | map(select(.name == $distribution)) | length > 0) then
						.distro = (.distro | map(if .name == $distribution then .url.old = $url_old | .url.new = $url_new else . end))
					else
						.distro += [{"name": $distribution, "fullname": $fullname, "url": {"old": $url_old, "new": $url_new}}]
					end' \
					"${var_output}" > "${var_output_tmp}" 2>/dev/null; then
					# Test : data exists.
					if [[ -s "${var_output_tmp}" ]]; then
						if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
					else
						return 1;
					fi;
				else
					return 1;
				fi;
			else
				return 1;
			fi;
		elif [[ "$var_type" == "source" ]]; then
			# Test : TMP security.
			var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
			if Test_File -PATH "$var_output_tmp"; then
				var_distro_archive_source=$(echo "$var_data" | cut -d '|' -f1);
				var_distro_current_source=$(echo "$var_data" | cut -d '|' -f2);
				# Test : data success.
				if jq --arg distribution "$var_name" \
					--arg fullname "$var_fullname" \
					--arg source_old "$var_distro_archive_source" \
					--arg source_new "$var_distro_current_source" \
					'if (.distro | map(select(.name == $distribution)) | length > 0) then
						.distro = (.distro | map(if .name == $distribution then .source.old = $source_old | .source.new = $source_new else . end))
					else
						.distro += [{"name": $distribution, "fullname": $fullname, "source": {"old": $source_old, "new": $source_new}}]
					end' \
					"${var_output}" > "${var_output_tmp}" 2>/dev/null; then
					# Test : data exists.
					if [[ -s "${var_output_tmp}" ]]; then
						if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
					else
						return 1;
					fi;
				else
					return 1;
				fi;
			else
				return 1;
			fi;
		else
			return 1;
		fi;
	elif [[ "$var_format" == "yaml" ]]; then
		# Test : distro exists - YAML format need init test.
		var_distro=$(yq '.distro[] | select(.name == "'"$var_name"'") | length' "${var_output}");
		if [ -z "$var_distro" ] || [ "$var_distro" -eq 0 ]; then
			# Test : TMP security.
			var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
			# Test : data success.
			if yq eval '.distro += {"name": "'"$var_name"'", "fullname": "'"$var_fullname"'"}' "${var_output}" > "${var_output_tmp}" 2>/dev/null; then
				# Test : data exists.
				if [[ -s "${var_output_tmp}" ]]; then
					mv "${var_output_tmp}" "${var_output}";
				fi;
			fi;
		fi;
		# Test : new distribution.
		if [[ "$var_state" == "new" ]]; then
			# Test : TMP security.
			var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
			# Test : data success.
			if yq eval '.distro[] |= (select(.name == "'"$var_name"'") | .state = "new")' "${var_output}" > "${var_output_tmp}" 2>/dev/null; then
				# Test : data exists.
				if [[ -s "${var_output_tmp}" ]]; then
					if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
				else
					return 1;
				fi;
			else
				return 1;
			fi;
		fi;
		# Tests : version, url, source.
		if [[ "$var_type" == "version" ]]; then
			var_distro_archive_version=$(echo "$var_data" | cut -d '|' -f1);
			var_distro_current_version=$(echo "$var_data" | cut -d '|' -f2);
			# Test : data exists.
			if [ "$(yq '.distro[] | select(.name == "'"$var_name"'") | has("version")' "$var_output")" == "true" ]; then
				return 1;
			else
				# Test : TMP security.
				var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
				# Test : data success.
				if yq eval '.distro[] |= (select(.name == "'"$var_name"'") | .version = {"old": "'"$var_distro_archive_version"'", "new": "'"$var_distro_current_version"'"})' "${var_output}" > "${var_output_tmp}" 2>/dev/null; then
					# Test : data exists.
					if [[ -s "${var_output_tmp}" ]]; then
						if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
					else
						return 1;
					fi;
				else
					return 1;
				fi;
			fi;
		elif [[ "$var_type" == "url" ]]; then
			var_distro_archive_url=$(echo "$var_data" | cut -d '|' -f1);
			var_distro_current_url=$(echo "$var_data" | cut -d '|' -f2);
			# Test : data exists.
			if [ "$(yq '.distro[] | select(.name == "'"$var_name"'") | has("url")' "$var_output")" == "true" ]; then
				return 1;
			else
				# Test : TMP security.
				var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
				# Test : data success.
				if yq eval '.distro[] |= (select(.name == "'"$var_name"'") | .url = {"old": "'"$var_distro_archive_url"'", "new": "'"$var_distro_current_url"'"})' "${var_output}" > "${var_output_tmp}" 2>/dev/null; then
					# Test : data exists.
					if [[ -s "${var_output_tmp}" ]]; then
						if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
					else
						return 1;
					fi;
				else
					return 1;
				fi;
			fi;
		elif [[ "$var_type" == "source" ]]; then
			var_distro_archive_source=$(echo "$var_data" | cut -d '|' -f1);
			var_distro_current_source=$(echo "$var_data" | cut -d '|' -f2);
			# Test : data exists.
			if [ "$(yq '.distro[] | select(.name == "'"$var_name"'") | has("source")' "$var_output")" == "true" ]; then
				return 1;
			else
				# Test : TMP security.
				var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.${var_format}");
				# Test : data success.
				if yq eval '.distro[] |= (select(.name == "'"$var_name"'") | .source = {"old": "'"$var_distro_archive_source"'", "new": "'"$var_distro_current_source"'"})' "${var_output}" > "${var_output_tmp}" 2>/dev/null; then
					# Test : data exists.
					if [[ -s "${var_output_tmp}" ]]; then
						if mv "${var_output_tmp}" "${var_output}"; then return 0; else return 1; fi;
					else
						return 1;
					fi;
				else
					return 1;
				fi;
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}

# Get_LocalDistroInfo.
#	DESCRIPTION : search for specific information in Distro Tracker datas.
function Get_LocalDistroInfo() {
	# Some definition.
	local var_source;
	local var_name;
	local var_line_first;
	local var_line_distro;
	local var_version;
	local var_url;
	local var_sourced;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-NAME)
				var_name="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : CSV, JSON, YAML.
	if [[ "$var_source" == *.csv ]]; then
		# Define content.
		var_line_first=$(head -n 1 "$var_source");
		var_line_distro=$(grep "^$var_name" "$var_source");
		# Test : data exists.
		if [[ -n "$var_line_distro" && "$var_line_distro" != "" ]]; then
			var_version=$(Search_InCsv -HEADER "$var_line_first" -LINE "$var_line_distro" -PATTERN "VersionLatest");
			var_url=$(Search_InCsv -HEADER "$var_line_first" -LINE "$var_line_distro" -PATTERN "UrlLatest");
			var_sourced=$(Search_InCsv -HEADER "$var_line_first" -LINE "$var_line_distro" -PATTERN "Source");
		else
			return 1;
		fi;
		# Test : data exists.
		if [[ -n "$var_version" && -n "$var_url" && -n "$var_sourced" ]]; then
			echo "$var_version $var_url $var_sourced";
		else
			return 1;
		fi;
	elif [[ "$var_source" == *.json ]]; then
		var_data=$(jq -r --arg var_name "$var_name" '.distro[] | select(.name == $var_name) | {version: .releases.latest.version, url: .releases.latest.url, source: .source}' "$var_source");
		var_version=$(echo "$var_data" | jq -r '.version');
		var_url=$(echo "$var_data" | jq -r '.url');
		var_sourced=$(echo "$var_data" | jq -r '.source');
		# Test : data exists.
		if [[ -n "$var_version" && -n "$var_url" && -n "$var_sourced" ]]; then
			echo "$var_version $var_url $var_sourced";
		else
			return 1;
		fi;
	elif [[ "$var_source" == *.yaml ]]; then
		var_data=$(yq eval ".distro[] | select(.name == \"$var_name\")" "$var_source");
		var_version=$(echo "$var_data" | yq eval '.releases.latest.version');
		var_url=$(echo "$var_data" | yq eval '.releases.latest.url');
		var_sourced=$(echo "$var_data" | yq eval '.source');
		# Test : data exists.
		if [[ -n "$var_version" && -n "$var_url" && -n "$var_sourced" ]]; then
			echo "$var_version $var_url $var_sourced";
		else
			return 1;
		fi;
	fi;
}

# Compare_LocalDistroInfo.
#	DESCRIPTION : check if current content is the same as archive content.
function Compare_LocalDistroInfo() {
	# Some definition.
	local var_name;
	local var_fullname;
	local var_source;
	local var_output;
	local var_format;
	local var_source_archive;
	local var_source_current;
	local var_distro_archive;
	local var_distro_archive_version;
	local var_distro_archive_url;
	local var_distro_archive_source;
	local var_distro_current;
	local var_distro_current_version;
	local var_distro_current_url;
	local var_distro_current_source;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-FULLNAME)
				var_fullname="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : data exists.
	if [[ "$var_source" =~ ":" ]]; then
		echo "";
		Write_Script -TEXT "'${var_fullname}' (codename '$var_name') distribution check ...";
		# Get old and current datas.
		var_source_archive=$(echo "$var_source" | cut -d ':' -f1);
		var_source_current=$(echo "$var_source" | cut -d ':' -f2);
		# Old datas.
		var_distro_archive=$(Get_LocalDistroInfo -NAME "$var_name" -SOURCE "$var_source_archive");
		var_distro_archive_version=$(echo "$var_distro_archive" | cut -d ' ' -f1);
		var_distro_archive_url=$(echo "$var_distro_archive" | cut -d ' ' -f2);
		var_distro_archive_source=$(echo "$var_distro_archive" | cut -d ' ' -f3);
		# Current datas.
		var_distro_current=$(Get_LocalDistroInfo -NAME "$var_name" -SOURCE "$var_source_current");
		var_distro_current_version=$(echo "$var_distro_current" | cut -d ' ' -f1);
		var_distro_current_url=$(echo "$var_distro_current" | cut -d ' ' -f2);
		var_distro_current_source=$(echo "$var_distro_current" | cut -d ' ' -f3);
		# Test : reset security about Distro Tracker output.
		var_distro_archive_version=${var_distro_archive_version:-"null"};
		var_distro_archive_url=${var_distro_archive_url:-"null"};
		var_distro_archive_source=${var_distro_archive_source:-"null"};
		var_distro_current_version=${var_distro_current_version:-"null"};
		var_distro_current_url=${var_distro_current_url:-"null"};
		var_distro_current_source=${var_distro_current_source:-"null"};
		# Test : new distribution.
		if [[ "$var_distro_archive_version" == "null" && "$var_distro_archive_url" == "null" && "$var_distro_archive_source" == "null" ]]; then
			Write_Script -TEXT "'${var_fullname}' (codename '$var_name') distribution is new";
			# add to report.
			if Add_LocalReport -NAME "$var_name" -FULLNAME "$var_fullname" -STATE "new" -OUTPUT "$var_output" -FORMAT "$var_format"; then
				Write_Script -TEXT "    added to report";
			else
				Write_Script -TEXT "    can't be added to report";
			fi;
		fi;
		# version.
		Write_Script -TEXT "version ..." -CHECK;
		if [[ "$var_distro_current_version" == "$var_distro_archive_version" ]]; then
			Write_Script -TEXT "version '$var_distro_current_version'" -CODE 0 -CHECKED;
		else
			Write_Script -TEXT "version '$var_distro_archive_version' > '$var_distro_current_version'" -CODE 1 -CHECKED;
			# add to report.
			if Add_LocalReport -NAME "$var_name" -FULLNAME "$var_fullname" -TYPE "version" -DATA "${var_distro_archive_version}|${var_distro_current_version}" -OUTPUT "$var_output" -FORMAT "$var_format"; then
				Write_Script -TEXT "    added to report";
			else
				Write_Script -TEXT "    can't be added to report";
			fi;
		fi;
		# url.
		Write_Script -TEXT "url ..." -CHECK;
		if [[ "$var_distro_current_url" == "$var_distro_archive_url" ]]; then
			Write_Script -TEXT "url '$var_distro_current_url'" -CODE 0 -CHECKED;
		else
			Write_Script -TEXT "url '$var_distro_archive_url' > '$var_distro_current_url'" -CODE 1 -CHECKED;
			# add to report.
			if Add_LocalReport -NAME "$var_name" -FULLNAME "$var_fullname" -TYPE "url" -DATA "${var_distro_archive_url}|${var_distro_current_url}" -OUTPUT "$var_output" -FORMAT "$var_format"; then
				Write_Script -TEXT "    added to report";
			else
				Write_Script -TEXT "    can't be added to report";
			fi;
		fi;
		# source.
		Write_Script -TEXT "source ..." -CHECK;
		if [[ "$var_distro_current_source" == "$var_distro_archive_source" ]]; then
			Write_Script -TEXT "source '$var_distro_current_source'" -CODE 0 -CHECKED;
		else
			Write_Script -TEXT "source '$var_distro_archive_source' > '$var_distro_current_source'" -CODE 1 -CHECKED;
			# add to report.
			if Add_LocalReport -NAME "$var_name" -FULLNAME "$var_fullname" -TYPE "source" -DATA "${var_distro_archive_source}|${var_distro_current_source}" -OUTPUT "$var_output" -FORMAT "$var_format"; then
				Write_Script -TEXT "    added to report";
			else
				Write_Script -TEXT "    can't be added to report";
			fi;
		fi;
	else
		Write_Script -TEXT "'${var_fullname}' (codename '$var_name') distribution has nothing to check.";
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_content;
	local var_data;
	local var_name;
	local var_fullname;
	# Test : custom data.
	if Test_File -PATH "${PARAM[SOURCE_CURRENT]}"; then
		Write_Script -TEXT "source 'current' finded : '${PARAM[SOURCE_CURRENT]}'.";
		# Test : recap sources exists.
		if [[ -n "${PARAM["SOURCE_ARCHIVE"]}" ]]; then
			Write_Script -TEXT "source 'archive' finded : '${PARAM[SOURCE_ARCHIVE]}'.";
			# Test : report creation.
			if New_LocalReport -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "${PARAM[FORMAT]}"; then
				Write_Script -TEXT "report created.";
				if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
				# Store : file content.
				var_content="$(<"${PARAM[SOURCE_CURRENT]}")";
				# Tests : CSV, JSON, YAML.
				# jq works directly on JSON objects, making accessing properties simple and immediate. In contrast, yq deals with YAML strings, sometimes requiring re-parsing to access properties. The key to avoiding errors is to make sure that yq works on parsable objects at each step, not raw YAML strings.
				if [[ "${PARAM[SOURCE_CURRENT]}" == *.csv ]]; then
					# Loop : Process Substitution distribution.
					while IFS=',' read -r var_name var_fullname _; do
						# Test : debug.
						if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
							echo "DEBUG :: '${PARAM[SOURCE_ARCHIVE]}:${PARAM[SOURCE_CURRENT]}'";
						fi;
						Compare_LocalDistroInfo -NAME "$var_name" -FULLNAME "$var_fullname" -SOURCE "${PARAM[SOURCE_ARCHIVE]}:${PARAM[SOURCE_CURRENT]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "${PARAM[FORMAT]}";
					done < <(echo "$var_content" | tail -n +2);
					Restore_IFS;
				elif [[ "${PARAM[SOURCE_CURRENT]}" == *.json ]]; then
					# Loop : Process Substitution distribution.
					while read -r var_data; do
						var_name="$(echo "$var_data" | jq -r -c '.name')";
						var_fullname="$(echo "$var_data" | jq -r -c '.fullname')";
						# Test : debug.
						if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
							echo "DEBUG :: '${PARAM[SOURCE_ARCHIVE]}:${PARAM[SOURCE_CURRENT]}'";
						fi;
						Compare_LocalDistroInfo -NAME "$var_name" -FULLNAME "$var_fullname" -SOURCE "${PARAM[SOURCE_ARCHIVE]}:${PARAM[SOURCE_CURRENT]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "${PARAM[FORMAT]}";
					done < <(echo "$var_content" | jq -r -c '.distro[]');
				elif [[ "${PARAM[SOURCE_CURRENT]}" == *.yaml ]]; then
					# Loop : Process Substitution distribution.
					while read -r var_name; do
						var_data=$(echo "$var_content" | yq eval ".distro[] | select(.name == \"$var_name\")");
						var_fullname="$(echo "$var_data" | yq eval '.fullname')";
						# Test : debug.
						if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
							echo "DEBUG :: '${PARAM[SOURCE_ARCHIVE]}:${PARAM[SOURCE_CURRENT]}'";
						fi
						Compare_LocalDistroInfo -NAME "$var_name" -FULLNAME "$var_fullname" -SOURCE "${PARAM[SOURCE_ARCHIVE]}:${PARAM[SOURCE_CURRENT]}" -OUTPUT "${PARAM[OUTPUT]}" -FORMAT "${PARAM[FORMAT]}";
					done < <(echo "$var_content" | yq '.distro[].name');
				fi;
			else
				Stop_App -CODE 1 -TEXT "Report can't be created.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "No archive from '--source' option finded.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Need valid value for '--source' option.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;