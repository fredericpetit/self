<blockquote id="out2tag">



NAME



	distro-reporter - Analyze Distro Tracker output for report new versions (with their download URLs) or source changes.





REQUIREMENTS



	program 'mktemp' (mandatory)



 	program 'jq' (mandatory)



 	program 'yq' (mandatory)



 

SYNOPSIS



	distro-reporter [ OPTIONS ]





DESCRIPTION



	This open-source tool generates update report in JSON or YAML formats.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path or single local folder path to Distro Tracker data. Searches for the second most recent data relative to the source in order to perform the data comparison analysis.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD'.



 	-o FILEPATH,

	--output FILEPATH

 		Single local file path or single local folder path to store the final result. JSON & YAML formats allowed. Use final '/' for set folder path with date name mode, or file path with custom name mode. Set '--format' option for folder path setting if YAML needed.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD/DR-DATETIME.FORMAT' (date name mode).



 	-f EXTENSION,

	--format EXTENSION

 		Single data format.

 		- state : optionnal.

 		- accepted value(s) : 'json' or 'yaml'.

 		- default value : 'json'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	generate report with custom name mode in JSON format :

		distro-reporter -s test.current.csv -o test.report.json



 	generate report with date name mode in YAML format :

		distro-reporter -f yaml



 

TO-DO



	x





NOTES



	Compatible Distro Tracker >= 1.2.0.



 

CREDITS & THANKS



	x





CHANGELOG



	1.1.0 - 2024-12-01

		Add mktemp program in requirements and use it for temporary file security protection. Add fullname information. Auto-discovery column name for CSV files.



 	1.0.0 - 2024-10-13

		Separate report & publish functions into distro-reporter & distro-publisher.



 	0.0.2 - 2024-06-01

		New '--action' parameter.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.1.0

	License : GPL-3.0-or-later





</blockquote>
