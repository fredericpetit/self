#!/bin/bash
# first content joined - 2024-10-13.
# first content joined - 2024-10-13.
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_os.sh               | #
# |________________________________________| #


# ==================== #
#          OS          #
#
#	DESCRIPTION : interacting with OS for all APPs.


function Set_Placeholder2() {
	echo "placeholder";
}



# next joined content - 2024-10-13.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_network.sh               | #
# |_____________________________________________| #


# ========================= #
#          NETWORK          #
#
#	DESCRIPTION : network state for all APPs.

# Get_FirstNetwork.
#	DESCRIPTION : get name of the first network interface.
#	RETURN : string or code.
function Get_FirstNetwork() {
	# Some definition.
	local var_result;
	local var_interfaces;
	var_result="fail-network";
	var_interfaces=$(ip -o link show | awk -F': ' '{print $2}');
	# Loop : interfaces.
	for interface in $var_interfaces; do
		if [[ $interface != "lo" ]]; then var_result="$interface"; break; fi;
	done
	echo "$var_result";
}


# Get_Distant.
#	DESCRIPTION : get distant resource.
#	RETURN : string or code.
#	TO-DO : git auth.
function Get_Distant() {
	# Some definition.
	local var_method;
	local var_type;
	local var_url;
	local var_path;
	local var_credential;
	local var_username;
	local var_password;
	local var_overwrite;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-METHOD)
				var_method="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Some default.
	if [[ -z "$var_method" ]]; then var_method="wget"; fi;
	if [[ -z "$var_type" ]]; then var_type="file"; fi;
	# Test : auth.
	if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
		var_username=$(echo -e "$var_credential" | cut -d ':' -f 1)
		var_password=$(echo -e "$var_credential" | cut -d ':' -f 2)
	fi;
	# Tests : method.
	if [[ "$var_method" == "wget" ]]; then
		# Tests : file, string, ping.
		if [[ "$var_type" == "file" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		elif [[ "$var_type" == "string" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				fi;
			fi;
			# Test : good content.
			if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
		elif [[ "$var_type" == "ping" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		fi;
	elif [[ "$var_method" == "curl" ]]; then
		# Test : auth.
		if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
			if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
				if curl -s "$var_url" -u "${var_username}:${var_password}" | grep browser_download_url | cut -d '"' -f 4 | wget --no-config --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			else
				if curl -s "$var_url" -u "${var_username}:${var_password}" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			fi;
		else
			if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
				if curl -s "$var_url" | grep browser_download_url | cut -d '"' -f 4 | wget --no-config --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			else
				if curl -s "$var_url" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			fi;
		fi;
	elif [[ "$var_method" == "git" ]]; then
		if [[ "$var_overwrite" = true ]]; then
			if git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; then return 0; else return 1; fi;
		else
			git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; return 0;
		fi;
	fi;
}



# next joined content - 2024-10-13.
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_fs.sh               | #
# |________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interaction for all APPs.


# Test_Dir.
#	DESCRIPTION : check directory.
#	RETURN : code.
function Test_Dir() {
	# Some definition.
	local var_path;
	local var_create;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : folder.
	if [[ -n "$var_path" ]]; then
		if [[ "$var_create" = true ]]; then mkdir -p "$var_path" &> /dev/null; fi;
		if [ -d "$var_path" ]; then
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod -R "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown -R "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else return 1; fi;
	else return 1; fi;
}


# Test_DirPopulated().
#	DESCRIPTION : check if folder haz content in it.
#	RETURN : code.
function Test_DirPopulated() {
	# Some definition.
	local var_path;
	local var_extension;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-EXTENSION)
				var_extension="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	if [[ -n "$(ls -A "$var_path")" ]]; then
		if [[ -n "$var_extension" ]]; then
			if find "$var_path" -type f -name "*$var_extension" &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		else
			return 0;
		fi;
	else
		return 1;
	fi;
}


# Test_File.
#	DESCRIPTION : check file.
#	RETURN : code.
function Test_File() {
	# Some definition.
	local var_path;
	local var_overwrite;
	local var_create;
	local var_chmod;
	local var_chown;
	local var_exec;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-EXEC)
				var_exec=true
				shift
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		if [ -f "$var_path" ]; then
			if [[ "$var_overwrite" = true ]]; then
				rm "$var_path" &> /dev/null;
				Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				touch "$var_path" &> /dev/null;
			fi;
		else
			if [[ "$var_overwrite" = true ]] || [[ "$var_create" = true ]]; then
				Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				touch "$var_path" &> /dev/null;
			fi;
		fi;
		if [ -f "$var_path" ]; then
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown "$var_chown" "$var_path"; fi;
			if [[ -n "$var_exec" && "$var_exec" = true ]]; then chmod +x "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else return 1; fi;
	else return 1; fi;
}


# Set_File.
#	DESCRIPTION : create file.
#	RETURN : code.
function Set_File() {
	# Some definition.
	local var_path;
	local var_raw;
	local var_source;
	local var_append;
	local var_encode;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-RAW)
				var_raw="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-APPEND)
				var_append=true
				shift
				;;
			-ENCODE)
				var_encode="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		touch "$var_path" &> /dev/null;
		# Tests : echo, cp.
		if [[ -n "$var_raw" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				echo "$var_raw" | tee -a "$var_path" > /dev/null;
			else
				# Test : base64.
				if [[ -n "$var_encode" ]]; then
					echo "$var_raw" | base64 -d > "$var_path";
				else
					echo "$var_raw" | tee "$var_path" > /dev/null;
				fi;
			fi;
		elif [[ -n "$var_source" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				cat "$var_source" >> "$var_path";
			else
				cp "$var_path" "$var_source" &> /dev/null;
			fi;
		else
			return 1;
		fi;
	else return 1; fi;
}


# Find_FileLine.
#	DESCRIPTION : find line started by pattern in a file.
#	RETURN : string or code.
function Find_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file;
	local var_line;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE)
				var_file="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file"; then
		var_line=$(grep "$var_pattern" "$var_file" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line" ]]; then echo "$var_line"; else return 1; fi;
	else
		return 1;
	fi;
}


# Compare_FileLine.
#	DESCRIPTION : search 1st line started by pattern in 1st file, compare it to 2nd file.
#	RETURN : string or code.
function Compare_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file1;
	local var_file2;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1" && Test_File -PATH "$var_file2"; then
		var_line1=$(grep "^$var_pattern" "$var_file1" | head -n 1);
		var_line2=$(grep "^$var_pattern" "$var_file2" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line1" && "$var_line1" != false && "$var_line1" != "null" && \
			-n "$var_line2" && "$var_line2" != false && "$var_line2" != "null" ]]; then
			# Test : return file number 2 content for different content, return false for same content.
			if [[ "$var_line1" != "$var_line2" ]]; then echo "$var_line2"; else return 1; fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Backup_FileLine.
#	DESCRIPTION : recover line in 1st file from a 2nd file.
#	RETURN : string or code.
#	TODO : verify character | escaping.
function Backup_FileLine() {
	# Some definition.
	local var_pattern;
	local var_default;
	local var_file1;
	local var_file2;
	local var_diff;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DEFAULT)
				var_default="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1"; then
		var_diff=$(Compare_FileLine -PATTERN "$var_pattern" -FILE1 "$var_file1" -FILE2 "$var_file2");
		var_pattern="^${var_pattern}";
		# Tests : replace by old content for different content, replace by default for same/empty content.
		if [[ -n "$var_diff" && "$var_diff" != false ]]; then
			sed -i "s|${var_pattern}|${var_diff}|" "$var_file1";
			echo "old";
		elif [[ -n "$var_default" ]]; then
			echo -e "$var_default" >> "$var_file1";
			echo "default";
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}



# next joined content - 2024-10-13.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_user.sh               | #
# |__________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test-Root.
#	DESCRIPTION : check root/sudo.
#	RETURN : int.
function Test-Root {
	if [[ "$(id -u)" -eq 0 ]]; then
		return 0;
	elif groups | grep -qE '\bsudo\b|\bwheel\b'; then
		return 0;
	else
		return 1;
	fi;
}



# next joined content - 2024-10-13.
# shellcheck source=/dev/null
# shellcheck disable=SC2034
#  _____________________________________________
# |                                             | #
# |               func_generic.sh               | #
# |_____________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : everything generic for all APPs.


# CONF - pursuit : safe storage of original $IFS.
if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["IFS"]="$IFS";


# Restore_IFS.
#	DESCRIPTION : restore IFS.
function Restore_IFS() { IFS="${CONF["IFS"]}"; }


# DATE.
declare -gA DATE;
DATE["CAPTURE"]="$(date "+%s%3N")";
# ex EXTRA_TIMESTAMP : execution time diff calc (timestamp in milliseconds).
DATE["TIMESTAMP0"]="${DATE[CAPTURE]}";
# ex CURRENT_DATE : jekyll file name markdown, manuel page.
DATE["DATE0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d")";
# distro files JSON & YAML.
DATE["DATETIME0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%dT%H:%M:%S%z")";
# ex COMPACT_TIMESTAMP & DATE_FILE : output filenames.
DATE["DATETIME1"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y%m%d_%H%M%S")";
# ex FULL_TIMESTAMP & DATE_START_EXEC : page.date for jekyll inside markdown file, added date inside join-files output.
DATE["DATETIME2"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d %H:%M:%S")";
# ex FORMATTED_DATE_TIME & DATE_START : bash execution time.
DATE["DATETIME3"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%d/%m/%Y - %Hh%M")";


# Get_DateDiff.
#	DESCRIPTION : diff calc in seconds & milliseconds.
#	RETURN : string.
function Get_DateDiff() {
	# Some definition.
	local var_start;
	local var_start_sec;
	local var_start_msec;
	local var_end;
	local var_end_msec;
	local var_end_sec;
	local var_diff_sec;
	local var_diff_msec;
	local var_msec_2_digits;
	local var_result;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-START)
				var_start="$2"
				shift 2
				;;
			-END)
				var_end="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Extract seconds & milliseconds.
	var_start_sec="${var_start:0:10}";
	var_start_msec="${var_start:10:3}";
	var_end_sec="${var_end:0:10}";
	var_end_msec="${var_end:10:3}";
	# Remove leading zeros to avoid octal interpretation.
	var_start_sec=$((10#$var_start_sec));
	var_start_msec=$((10#$var_start_msec));
	var_end_sec=$((10#$var_end_sec));
	var_end_msec=$((10#$var_end_msec));
	# Diff calc.
	var_diff_sec=$((var_end_sec - var_start_sec));
	var_diff_msec=$((var_end_msec - var_start_msec));
	# Test : milliseconds diff is negative.
	if [ $var_diff_msec -lt 0 ]; then
		var_diff_sec=$((var_diff_sec - 1));
		var_diff_msec=$((var_diff_msec + 1000));
	fi;
	# Test : milliseconds with two digits.
	if [ $var_diff_msec -lt 10 ]; then
		var_msec_2_digits="0$((var_diff_msec / 10))";
	else
		var_msec_2_digits=$((var_diff_msec / 10));
	fi;
	var_result="${var_diff_sec}.${var_msec_2_digits}";
	echo "$var_result";
}


# COLOR.
declare -gA COLOR;
COLOR[BOLD]='\e[1m';
COLOR[UNDERLINE]='\e[4m';
COLOR[RED_LIGHT]='\e[1;38;5;160m';
COLOR[GREEN_LIGHT]='\e[1;38;5;34m';
COLOR[GREEN_DARK]='\e[1;38;5;28m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;179m';
COLOR[YELLOW_DARK]='\e[1;38;5;178m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;208m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';
COLOR[STOP]='\e[0m';
COLOR[TICK]="[${COLOR[GREEN_LIGHT]}✔${COLOR[STOP]}]";
COLOR[CROSS]="[${COLOR[RED_LIGHT]}✘${COLOR[STOP]}]";


# Write_Script.
#	DESCRIPTION : custom output of the APP.
#	RETURN : string.
function Write_Script() {
	# Some definition.
	local var_text;
	local var_code;
	local var_check;
	local var_checked;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-CODE)
				var_code="$2"
				shift 2
				;;
			-CHECK)
				var_check=true
				shift
				;;
			-CHECKED)
				var_checked=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then 
		# Tests : state, change former line.
		if [ -n "$var_check" ]; then
			echo -ne "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		elif [ -n "$var_checked" ]; then
			if [[ "$var_code" -eq 1 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[CROSS]} ERROR : $var_text";
			elif [[ "$var_code" -eq 0 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[TICK]} $var_text";
			fi;
		else
			echo -e "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		fi;
	fi;
}


# Get_Help.
#	DESCRIPTION : usage of the APP, in the APP.
#	RETURN : string.
function Get_Help() {
	# requirements.
	local var_requirements;
	local var_requirement;
	local var_requirement_type;
	local var_requirement_name;
	local var_requirement_state;
	local var_requirements_text;
	# Test : array exists.
	if [[ -n "${APP[REQUIREMENTS]}" && "${APP[REQUIREMENTS]}" != "null" ]]; then
		IFS='#' read -ra var_requirements <<< "${APP[REQUIREMENTS]}";
		# Loop : valuelist requirements.
		for var_requirement in "${var_requirements[@]}"; do
			var_requirement_type="$(echo "$var_requirement" | cut -d ' ' -f1)";
			var_requirement_name="$(echo "$var_requirement" | cut -d ' ' -f2)";
			var_requirement_state="$(echo "$var_requirement" | cut -d ' ' -f3)";
			var_requirements_text+="$(echo -e "\t${var_requirement_type} '${var_requirement_name}' (${var_requirement_state})\n\n ")";
		done;
	else
		var_requirements_text+="\tx\n\n ";
	fi;
	# synopsis + options.
	local i;
	local var_option;
	local var_option_name;
	local var_option_quick;
	local var_option_type;
	local var_option_state;
	local var_option_dsc;
	local var_option_dsc_def;
	local var_option_dsc_val_ok;
	local var_option_dsc_val_default;
	local var_synopsis_text_mandatory;
	local var_synopsis_text;
	local var_options_text;
	var_synopsis_text_mandatory=false;
	# Loop : valuelist options array inside APP.
	for i in "${OPT_KEYS[@]}"; do
		var_option="${OPTIONS[$i]}";
		var_option_name="$(echo "$i" | tr '[:upper:]' '[:lower:]')";
		var_option_quick="$(echo "$var_option" | cut -d '#' -f2)";
		var_option_type="$(echo "$var_option" | cut -d '#' -f3)";
		var_option_state="$(echo "$var_option" | cut -d '#' -f4)";
		var_option_dsc="$(echo "$var_option" | cut -d '#' -f5)";
		var_option_dsc_def="$(echo "$var_option_dsc" | cut -d '|' -f1)";
		var_option_dsc_val_ok="$(echo "$var_option_dsc" | cut -d '|' -f2)";
		var_option_dsc_val_default="$(echo "$var_option_dsc" | cut -d '|' -f3)";
		# Test : mandatory OPTION.
		if [[ "$var_option_state" == "mandatory" ]]; then var_synopsis_text_mandatory=true; fi;
		# Test : command text.
		if [[ "$var_option_quick" == "null" ]]; then
			var_options_text+="$(echo -e "\t--${var_option_name} (no arg) ")";
		else
			# Test : command type (underline).
			if [[ "$var_option_type" == "null" ]]; then
				var_options_text+="$(echo -e "\t-${var_option_quick} (no type),\n\t--${var_option_name} (no type)\n ")";
			else
				var_options_text+="$(echo -e "\t-${var_option_quick} ${var_option_type},\n\t--${var_option_name} ${var_option_type}\n ")";
			fi;
		fi;
		var_options_text+="$(echo -e "\t\t${var_option_dsc_def}\n ")";
		var_options_text+="$(echo -e "\t\t- state : ${var_option_state}.\n ")";
		# Test : dsc redefine.
		if [[ "$var_option_dsc_val_ok" == "null" ]]; then var_option_dsc_val_ok="nothing."; fi;
		var_options_text+="$(echo -e "\t\t- accepted value(s) : ${var_option_dsc_val_ok}\n ")";
		var_options_text+="$(echo -e "\t\t- default value : ${var_option_dsc_val_default}\n\n ")";
	done;
	# Test : [] for OPTIONS if only one OPTION is mandatory.
	if [[ "$var_synopsis_text_mandatory" == true ]]; then var_synopsis_text="OPTIONS"; else var_synopsis_text="[ OPTIONS ]"; fi;
	# examples.
	local var_examples;
	local var_example;
	local var_example_dsc;
	local var_example_cmd;
	local var_examples_text;
	IFS='#' read -ra var_examples <<< "${APP[EXAMPLES]}";
	# Loop : valuelist examples.
	for var_example in "${var_examples[@]}"; do
		var_example_dsc="$(echo "$var_example" | cut -d '|' -f1)";
		var_example_cmd="$(echo "$var_example" | cut -d '|' -f2)";
		var_examples_text+="$(echo -e "\t${var_example_dsc}\n\t\t${var_example_cmd}\n\n ")";
	done;
	# todo.
	if [[ -z "${APP[TODO]}" || "${APP[TODO]}" == "null" ]]; then APP["TODO"]="x"; fi;
	# notes.
	local var_notes;
	local var_note;
	local var_notes_text;
	if [[ -n "${APP[NOTES]}" && "${APP[NOTES]}" != "null" ]]; then
		IFS='#' read -ra var_notes <<< "${APP[NOTES]}";
		# Loop : valuelist notes.
		for var_note in "${var_notes[@]}"; do
			var_notes_text+="$(echo -e "\t${var_note}\n\n ")";
		done;
	else
		var_notes_text+="\tx\n\n ";
	fi;
	# credits.
	if [[ -z "${APP[CREDITS]}" || "${APP[CREDITS]}" == "null" ]]; then APP["CREDITS"]="x"; fi;
	# changelog.
	local var_changelog;
	local var_change;
	local var_change_version;
	local var_change_date;
	local var_change_dsc;
	local var_changelog_text;
	IFS='#' read -ra var_changelog <<< "${APP[CHANGELOG]}";
	# Loop : valuelist changelog.
	for var_change in "${var_changelog[@]}"; do
		var_change_version="$(echo "$var_change" | cut -d '|' -f1)";
		var_change_date="$(LC_TIME=en_US.UTF-8 date -d "$(echo "$var_change" | cut -d '|' -f2)" "+%Y-%m-%d")";
		var_change_dsc="$(echo "$var_change" | cut -d '|' -f3)";
		var_changelog_text+="$(echo -e "\t${var_change_version} - ${var_change_date}\n\t\t${var_change_dsc}\n\n ")";
	done;
	Restore_IFS;
	echo -e "\nNAME\n\n\t${APP[NAME]} - ${APP[SLOGAN]}\n\n\nREQUIREMENTS\n\n${var_requirements_text}\nSYNOPSIS\n\n\t${APP[NAME]} ${var_synopsis_text}\n\n\nDESCRIPTION\n\n\t${APP[DESCRIPTION]}\n\n\nOPTIONS\n\n${var_options_text}\nEXAMPLES\n\n${var_examples_text}\nTO-DO\n\n\t${APP[TODO]}\n\n\nNOTES\n\n${var_notes_text}\nCREDITS & THANKS\n\n\t${APP[CREDITS]}\n\n\nCHANGELOG\n\n${var_changelog_text}\nMETADATAS\n\n\tAuthor : ${APP[AUTHOR]}\n\tWebsite : ${APP[URL]}\n\tVersion : ${APP[VERSION]}\n\tLicense : ${APP[LICENSE]}\n\n";
}


# Group_Options.
#	DESCRIPTION : global test for search missing options.
#	RETURN : string.
function Group_Options() {
	# Some definition.
	local var_key;
	local var_index;
	declare -ga OPT_KEYS;
	# Loop : indexlist OPTIONS.
	for var_key in "${!OPTIONS[@]}"; do
		var_index="$(echo "${OPTIONS[$var_key]}" | cut -d '#' -f1)";
		OPT_KEYS[$var_index]="$var_key";
	done;
}


# Test_Options.
#	DESCRIPTION : global test for search missing options.
#	RETURN : string.
function Test_Options() {
	# Some definition.
	declare -a var_options;
	local var_options_key;
	local var_options_values;
	local var_options_value_name;
	local var_options_value_type;
	local var_options_value_state;
	declare -a var_options_missing;
	local var_options_missing_list;
	# Some init.
	var_options=();
	var_options_missing=();
	# Test : data exists.
	if [ ${#OPTIONS[@]} -gt 0 ]; then
		var_options=("${!OPTIONS[@]}");
		# Loop : indexlist options.
		for var_options_key in "${!OPTIONS[@]}"; do
			var_options_values="${OPTIONS[$var_options_key]}";
			var_options_value_name="$(echo "$var_options_key" | tr '[:upper:]' '[:lower:]')";
			var_options_value_type="$(echo "$var_options_values" | cut -d '#' -f3)";
			var_options_value_state="$(echo "$var_options_values" | cut -d '#' -f4)";
			# Test : trigger warning.
			if [ "$var_options_value_state" == "mandatory" ]; then
					# Test : trigger array.
					if [[ -z "${PARAM[$var_options_key]}" ]]; then var_options_missing+=("${var_options_value_name}"); fi;
			elif [ "$var_options_value_state" != "optionnal" ]; then
				# Test : option with boolean don't need parameter.
				if [ "$var_options_value_type" != "null" ]; then
					Stop_App -CODE 1 -TEXT "Option '--${var_options_value_name}' type is empty, please report the issue.";
				fi;
			fi;
		done;
		# Test : missing exists.
		if [ ${#var_options_missing[@]} -gt 0 ]; then
			# Loop : missing.
			for i in "${!var_options_missing[@]}"; do
				var_options_missing[i]="--${var_options_missing[i]}";
			done;
			# Test : single, multiple data.
			if [ ${#var_options_missing[@]} -eq 1 ]; then
				Stop_App -CODE 1 -TEXT "Need '${var_options_missing[0]}' option. Type '--help' for read the cute manual.";
			else
				var_options_missing_list="${var_options_missing[*]}";
				var_options_missing_list="${var_options_missing_list// /, }";
				Stop_App -CODE 1 -TEXT "Need '$var_options_missing_list' options. Type '--help' for read the cute manual.";
			fi;
		fi;
	else
		Stop_App -CODE 1 -TEXT "No option provided, please report the issue.";
	fi;
}


# Test_Param.
#	DESCRIPTION : single test for checking parameter (argument) of one option.
#	RETURN : string.
function Test_Param() {
	# Some definition.
	local var_option;
	local var_param;
	local var_type;
	local var_contains;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-OPTION)
				var_option="$2"
				shift 2
				;;
			-PARAM)
				var_param="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-CONTAINS)
				var_contains="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : param mispelled, empty, strict type restriction.
	if [[ "$var_param" == "-"* ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't starting by '-'.";
	elif [[ -z "$var_param" ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't be empty.";
	elif [[ -n "$var_type" ]]; then
		if [[ "$var_type" == "char" ]]; then
			if [[ "$var_param" =~ [0-9] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have integer.";
			fi;
		elif [[ "$var_type" == "int" ]]; then
			if [[ "$var_param" =~ [a-zA-Z] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have character.";
			fi;
		fi;
	fi;
	# Test : character present.
	if [[ -n "$var_contains" ]]; then
		if [[ "$var_param" != *":"* ]]; then
			Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' don't contains '${var_contains}' character.";
		fi;
	fi;
}


# Get_StartingApp.
#	DESCRIPTION : script starter pack.
#	RETURN : string.
function Get_StartingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Title.
		echo -e "${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: ${DATE[DATETIME3]} ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
		# Place & soft env.
		echo -e "- work from '${CONF[PATH]}'.";
		if [ -n "$SNAP" ]; then
			echo -e "- Snapcraft environment detected.";
		elif [ -n "$FLATPAK_ID" ]; then
			echo -e "- Flatpak environment detected.";
		fi;
		echo "";
	fi;
	# Test : requirement.
	if [[ -n ${APP[REQUIREMENTS]} && ${APP[REQUIREMENTS]} != "null" ]]; then
		# Some definition.
		local var_element;
		local var_type;
		local var_name;
		local var_dependencie;
		local var_software;
		IFS='#';
		# Loop : valuelist requirements.
		for var_element in ${APP[REQUIREMENTS]}; do
			# Space delimiter.
			var_type="$(echo "$var_element" | cut -d ' ' -f 1)";
			var_name="$(echo "$var_element" | cut -d ' ' -f 2)";
			var_dependencie="$(echo "$var_element" | cut -d ' ' -f 3)";
			var_software=$(Test_Software -NAME "$var_name" -TYPE "$var_type");
			# Test : software.
			if [[ "$var_software" == "available" ]]; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- required ${var_type} '${var_name}' available.";
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- required ${var_type} '${var_name}' not available (error '${var_software}').";
				fi;
				# Test : dependencie.
				if [[ "$var_dependencie" == "mandatory" ]]; then
					Stop_App  -CODE 1 -TEXT "'${var_type}' ${var_name} mandatory.";
				fi;
			fi;
		done;
		Restore_IFS;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Output (data backup).
	if [[ "${PARAM[BACKUP]}" == true ]]; then
		# Some definition.
		local var_naming;
		# Define naming mode based on output definition.
		if [[ "${PARAM[OUTPUT]}" =~ -${DATE[DATETIME1]}\. ]]; then var_naming="date"; else var_naming="custom"; fi;
		# Test : output mode.
		if [[ "$var_naming" == "date" ]]; then
			# Test : get previous output old.
			if [[ -n "${PARAM[OUTPUT_BACKUP]}" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) in '${PARAM[OUTPUT_BACKUP]}' (date name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (date name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (date name mode).";
				fi;
			fi;
		else
			# Test : move previous output to old.
			if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT]}"; then
					mv "${PARAM[OUTPUT]}" "${PARAM[OUTPUT_BACKUP]}";
					# TMPSOLUTION.
					# Test : move all file formats.
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.json"; then
						mv "${PARAM[OUTPUT]%.csv}.json" "${PARAM[OUTPUT_BACKUP]%.csv}.json";
					fi;
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.yaml"; then
						mv "${PARAM[OUTPUT]%.csv}.yaml" "${PARAM[OUTPUT_BACKUP]%.csv}.yaml";
					fi;
					# /TMPSOLUTION.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) moved to '${PARAM[OUTPUT_BACKUP]}' (custom name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (custom name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (custom name mode).";
				fi;
			fi;
		fi;
		if [[ "${PARAM[QUIET]}" != true && -z "${PARAM[OUTPUT]}" ]]; then echo ""; fi;
	fi;
	# Output (main).
	if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" && "${PARAM[RAW]}" != true ]]; then
		# Tests : folder, file.
		if [[ "${PARAM["OUTPUT"]}" =~ /$ ]]; then
			if Test_Dir -PATH "${PARAM[OUTPUT]}" -CREATE; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- current output to folder '${PARAM[OUTPUT]}'.";
				fi;
			else
				Stop_App -CODE 1 -TEXT "Folder output '${PARAM[OUTPUT]}' can't be writed.";
			fi;
		elif Test_File -PATH "${PARAM[OUTPUT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current output to file '${PARAM[OUTPUT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "File output '${PARAM[OUTPUT]}' can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Report.
	if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]]; then
		if Test_File -PATH "${PARAM[REPORT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- report to '${PARAM[REPORT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "Report '${PARAM[REPORT]}' can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
}


# Get_EndingApp.
#	DESCRIPTION : script ending pack.
#	RETURN : string.
function Get_EndingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Some definition.
		local var_date_start;
		local var_date_end;
		local var_date_end_seconds;
		var_date_start="${DATE["TIMESTAMP0"]}";
		var_date_end="$(date "+%s%3N")";
		var_date_end_seconds=$(Get_DateDiff -START "$var_date_start" -END "$var_date_end");
		echo -e "\n${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: executed in ${var_date_end_seconds} seconds ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
	fi;
}


# Stop_App.
#	DESCRIPTION : end program.
#	RETURN : string and code.
function Stop_App() {
	# Some definition.
	local var_code;
	local var_text;
	local var_quiet;
	local var_help;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CODE)
				var_code="$2"
				shift 2
				;;
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-QUIET)
				var_quiet=true
				shift
				;;
			-HELP)
				var_help=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : splash error.
	if [[ "$var_quiet" != true && -n "$var_text" ]]; then
		Write_Script -TEXT "EXIT ERROR ! ${var_text}";
	fi;
	# Test : splash man.
	if [[ "$var_help" == true ]]; then Get_Help; fi;
	wait;
	exit "$var_code";
}


# Debug_Start.
#	DESCRIPTION : debug app.
function Debug_Start() { set -x; }


# Debug_Stop.
#	DESCRIPTION : debug app.
function Debug_Stop() { set +x; }


# Add_Book.
#	DESCRIPTION : construct associative array.
function Add_Book() {
	# Some definition.
	local var_name="$1";
	# Test : book exists.
	if ! declare -p "$var_name" &>/dev/null; then declare -gA "$var_name"; fi;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		local var_mode;
		local var_current_index;
		local var_current_release;
		local var_archive_index;
		local var_archive_release;
		local var_element_suffixes;
		local var_element_suffixes_var;
		local var_array;
		local var_array_keys;
		local var_array_suffixes;
		# mode, indexes & releases.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release");
		# Loop : var_suffix key.
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			var_element_suffixes_var="var_${var_element_suffixes}";
			declare "${var_element_suffixes_var}"="${var_element}_${var_element_suffixes}";
		done;
		# Construct index & release values from Get_Distribution.
		IFS=',' read -ra var_array <<< "$var_data";
		Restore_IFS;
		declare -a var_array_keys=("$var_mode" "$var_current_index" "$var_current_release" "$var_archive_index" "$var_archive_release");
		# Loop var_suffix value.
		for i in "${!var_array_keys[@]}"; do
			eval "${var_name}[\"${var_array_keys[$i]}\"]=\"${var_array[$i]}\"";
		done;
	elif [[ "$var_name" == "TMP" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		eval "${var_name}[\"${var_element}\"]=\"${var_data}\"";
	fi;
}


# Get_Book.
#	DESCRIPTION : get associative array.
#	RETURN : string or code.
function Get_Book() {
	# Some definition.
	local var_name;
	local var_element;
	local var_data;
	local var_array_suffixes;
	local var_element_suffixes;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-ELEMENT)
				var_element="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Loop: var_suffix key.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release");
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			if [[ "$var_data" == "$var_element_suffixes" ]]; then
				var_result="${DISTRIBUTION[${var_element}_$var_element_suffixes]}";
				break;
			fi;
		done;
	fi;
	# Test : return.
	if [[ -n "$var_result" && "$var_result" != "" ]]; then echo "$var_result"; else return 1; fi;
}


# Remove_Book.
#	DESCRIPTION : remove associative array.
function Remove_Book() {
	# Some definition.
	local var_name="$1";
	unset "$var_name";
}


# Remove_DuplicateInString.
#	DESCRIPTION : remove duplicate value in string.
#	RETURN : string.
function Remove_DuplicateInString() {
	# Some definition.
	local var_string;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	echo "$var_string" | tr ' ' '\n' | sort | uniq | xargs;
}


# Search_InArray.
#	DESCRIPTION : search if value exists in array.
#	RETURN : code.
function Search_InArray() {
	# Some definition.
	local var_search;
	local var_array;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-SEARCH)
				var_search="$2"
				shift 2
				;;
			-ARRAY)
				var_array="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : need all param.
	if [ -z "$var_search" ] || [ -z "$var_array" ]; then return 1; fi;
	# IFS for delimit array.
	IFS=" " read -ra var_array_read <<< "$var_array";
	Restore_IFS;
	# Loop : search in array.
	for element in "${var_array_read[@]}"; do
		if [ "$element" == "$var_search" ]; then return 0; fi;
	done;
	return 1;
}


# Measure_MaxInArray.
#	DESCRIPTION : calculate longest word in array.
#	RETURN : string.
function Measure_MaxInArray() {
	# Some definition.
	local var_count;
	local var_array;
	local var_word;
	local var_length;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		var_count=0;
		for element in "${!var_array[@]}"; do
			var_word="${var_array[$element]}";
			var_length="${#var_word}";
			if [ "$var_length" -gt "$var_count" ]; then var_count="$var_length"; fi;
		done;
		echo "$var_count";
	fi;
}


# Set_RepeatCharacter.
#	DESCRIPTION : calculate how many same character repeat needed.
#	RETURN : string.
function Set_RepeatCharacter() {
	# Some definition.
	local var_character;
	local var_count;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-CHARACTER)
				var_character="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : if arg empty.
	if [ -z "$var_character" ] || [ -z "$var_count" ]; then
		return 1;
	else
		var_result="";
		for ((i=0; i < "$var_count"; i++)); do
			var_result+="$var_character";
		done;
	fi;
	echo "$var_result";
}


# Limit_StringLength.
#	DESCRIPTION : limit number of character in a string.
#	RETURN : string.
#	TODO : replace -DIRECTION by -POSITION.
function Limit_StringLength() {
	# Some definition.
	local var_string;
	local var_count;
	local var_count_before;
	local var_count_after;
	local var_direction;
	local var_spacer;
	local var_spacer_length;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			-DIRECTION)
				var_direction="$2"
				shift 2
				;;
			-SPACER)
				var_spacer="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some defaults.
	if [ -z "$var_direction" ]; then var_direction="after"; fi;
	if [ -z "$var_spacer" ]; then var_spacer="(...)"; fi;
	var_spacer_length="${#var_spacer}";
	# Test : if arg empty.
	if [ -z "$var_string" ] || [ -z "$var_count" ]; then
		return 1;
	else
		# Test : cut needed.
		if [ ${#var_string} -gt "$var_count" ]; then
			# Add spacer length to cut count.
			var_count_before=$((${#var_string} - var_count + var_spacer_length + 1));
			var_count_after=$((var_count - var_spacer_length));
			# Tests : sub place.
			if [ "$var_direction" = "before" ]; then
				var_result+="$var_spacer";
				var_result+=$(echo "$var_string" | cut -c "$var_count_before"-"${#var_string}");
			elif [ "$var_direction" = "after" ]; then
				var_result+=$(echo "$var_string" | cut -c 1-"$var_count_after");
				var_result+="$var_spacer";
			fi;
			echo "$var_result";
		else
			echo "$var_string";
		fi;
	fi;
}


# Get_TableHeader.
#	DESCRIPTION : display a table index.
#	RETURN : string.
function Get_TableHeader() {
	# Some definition.
	local var_array;
	local var_element;
	local var_param;
	local var_param_length;
	local var_dot_length;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a name param and the max character count a value of this param can be in the rows.
		for element_header in "${!var_array[@]}"; do
			var_element="${var_array[$element_header]}";
			var_param="$(echo "$var_element" | cut -d ':' -f1)";
			var_param_length="${#var_param}";
			var_dot_length="$(echo "$var_element" | cut -d ':' -f2)";
			# Calculate how many characters to add more.
			var_param_length_more=$((var_dot_length - var_param_length));
			var_dot_length_more=$((var_param_length - var_dot_length));
			# Ready to construct header.
			var_line1+="$var_param";
			var_line1+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_param_length_more");
			var_line1+="  ";
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length");
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length_more");
			var_line2+="  ";
		done;
		echo -e "$var_line1";
		echo -e "$var_line2";
	fi;
}


# Get_TableRow.
#	DESCRIPTION : display a table row.
#	RETURN : string.
function Get_TableRow() {
	# Some definition.
	local var_array;
	local var_action;
	local var_element;
	local var_value;
	local var_value_length;
	local var_max;
	local var_line;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			-ACTION)
				var_action="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a value, the max character count this value can be.
		for element_row in "${!var_array[@]}"; do
			var_element="${var_array[$element_row]}";
			var_value="$(echo "$var_element" | cut -d ':' -f1)";
			var_max="$(echo "$var_element" | cut -d ':' -f2)";
			# Test : tick cross.
			if [[ "$var_value" == "tick" || "$var_value" == "cross" ]]; then
				if [[ "$var_value" == "tick" ]]; then
					var_value="${COLOR[TICK]}";
				elif [[ "$var_value" == "cross" ]]; then
					var_value="${COLOR[CROSS]}";
				fi;
				var_value_length=3;
			# TODO
			# elif [[ "$var_value" == *"tick"* || "$var_value" == *"cross"* ]]; then
			else
				var_value_length="${#var_value}";
			fi;
			# Calculate how many space to add more.
			var_value_length_more=$((var_max - var_value_length));
			# Ready to construct row.
			var_line+="$var_value";
			var_line+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_value_length_more");
			var_line+="  ";
		done;
		# Test : replace line.
		if [[ "$var_action" == "check" ]]; then
			echo -ne "$var_line";
		else
			echo -e "\\r$var_line";
		fi;
	fi;
}



# next joined content - 2024-10-13.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_pkgs.sh               | #
# |__________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Test_Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Test_Software() {
	# Some definition.
	local var_result;
	local var_name;
	local var_type;
	#local var_action; // SC2034 disable.
	#local var_depend; // SC2034 disable.
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-ACTION)
				#var_action="$2" // SC2034 disable.
				shift 2
				;;
			-DEPEND)
				#var_depend=true // SC2034 disable.
				shift
				;;
			*)
				shift
				;;
		esac
	done
	# Tests : type software.
	if [[ "$var_type" == "program" ]]; then
		# TMP : import OverDeploy routine.
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-program";
		fi;
	elif [[ "$var_type" == "service" ]]; then
		# Test : systemd check.
		# SRC : https://stackoverflow.com/a/53640320/2704550.
		if [[ $(systemctl list-units --all --type service --full --no-legend "${var_name}.service" | sed 's/^\s*//g' | cut -f1 -d' ') == "${var_name}.service" ]]; then
			var_result="available";
		else
			var_result="fail-service";
		fi;
	elif [[ "$var_type" == "command" ]]; then
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-command";
		fi;
	else
		# tmp default.
		var_result="fail-unknow";
	fi;
	echo "$var_result";
}


# Start_Software.
#	DESCRIPTION : test effective restarting of a service.
#	RETURN : code.
function Start_Software() {
	# Some definition.
	local var_name;
	local var_type;
	local var_command;
	local var_user;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-COMMAND)
				var_command="$2"
				shift 2
				;;
			-USER)
				var_user="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done
	# Test : daemon, service.
	if [[ "$var_type" == "daemon" ]]; then
		if $var_command &> /dev/null; then return 0; else return 1; fi;
	elif [[ "$var_type" == "service" ]]; then
		su -l "$var_user" -c "systemctl restart ${var_name}";
		# Test : service state.
		if su -l "$var_user" -c "systemctl is-active --quiet ${var_name}"; then return 0; else return 1; fi;
	fi;
}



# next joined content - 2024-10-13.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_convert.sh               | #
# |_____________________________________________| #


# ========================= #
#          CONVERT          #
#
#	DESCRIPTION : converting content for all APPs.


function Set_Placeholder1() {
	echo "placeholder";
}



# next joined content - 2024-10-13.
# shellcheck source=/dev/null
#  _________________________________________________
# |                                                 | #
# |               distro-publisher.sh               | #
# |_________________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Join_CommonFiles.
#	DESCRIPTION : include common files.
function Join_CommonFiles() {
	# Some definition.
	local var_file;
	# Test & Loop : includes only for dev work.
	if [[ "${0##*.}" == "sh" ]]; then
		if [[ "${CONF["PATH"]}" =~ /core$ ]]; then
			CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common";
			CONF["CONTINUE"]=false;
			for var_file in "${CONF[PATH_COMMON]}${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}"func_*.sh; do
				source "$var_file";
				CONF["CONTINUE"]=true;
			done;
			if [[ "${CONF[CONTINUE]}" == false ]]; then echo "check source code, program need common functions."; exit 1; fi;
		fi;
	fi;
}
Join_CommonFiles;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-publisher";
APP["SECTION"]="1";
APP["SHORTNAME"]="DP";
APP["SLOGAN"]="Treat Distro Reporter output.";
APP["DESCRIPTION"]="This open-source tool publish reports to Facebook & Jekyll.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENTS"]="program jq mandatory";
APP["EXAMPLES"]="null";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.0.0|2024-10-13T10:00:00+0200|Rewriting.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="1.0.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#optionnal#Single local file path or single local folder path to Distro Reporter report.|path.|'PWD/report.json' or 'PWD/DR-????????_??????.json/yaml'.";
OPTIONS["DESTINATION"]="2#d#NAME#mandatory#Product name for publish.|'facebook' or 'jekyll'.|no default.";
OPTIONS["CREDENTIAL"]="3#c#CREDENTIAL#optionnal#Login/ID & password/token of the product, delimited by ':'.|string.|no default.";
OPTIONS["OUTPUT"]="4#o#FOLDERPATH#optionnal#Single local folder path to store the final result (final slash automatically defined) with jekyll '--destination' option.|path.|'PWD/DATE-X-report.md' (date name mode) with jekyll '--destination' option.";
OPTIONS["UPON"]="5#u#null#optionnal#Allow to overwrite previous datas. New and old datas need to be in the same directory.|null|'false' because disabled.";
OPTIONS["QUIET"]="6#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="7#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="8#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Group options inside new array.
Group_Options;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ $# -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--destination|-d)
			PARAM["DESTINATION"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--credential|-c)
			PARAM["CREDENTIAL"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -CONTAINS ":"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--upon|-u)
			PARAM["UPON"]=true
			shift
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			Stop_App -CODE 0 -HELP
			shift
			;;
		--version|-v)
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			shift
			;;
		*)
			Stop_App -CODE 1 -TEXT "Unknown option : '$1'." -HELP
			shift
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["SOURCE"]="${PWD%[\\/]}${CONF[SEPARATOR]}report.json";
DEFAULT["ID"]="294221940452287";
DEFAULT["OUTPUT"]="${PWD%[\\/]}";
DEFAULT["UPON"]="false";
DEFAULT["QUIET"]=false;
# Some script definitions.
# SOURCE.
# Tests : folder, file.
if [ -z "${PARAM[SOURCE]}" ]; then
	if Test_File -PATH "${DEFAULT[SOURCE]}"; then
		PARAM["SOURCE"]="${DEFAULT[SOURCE]}";
	else
		# Loop : auto-discovery - last file win data state.
		for var_param_source_file in $(ls -1 "${PWD%[\\/]}${CONF[SEPARATOR]}"DR-????????_??????.{json,yaml} 2>/dev/null | sort); do
			# Test : current exists.
			if Test_File -PATH "$var_param_source_file"; then
				PARAM["SOURCE"]="$var_param_source_file";
				var_param_source_file_path="${PARAM[SOURCE]}";
			fi;
		done;
		# Test : data exists.
		if [[ "$var_param_source_file_path" == "" ]]; then
			Stop_App -CODE 1 -TEXT "Need valid value for '--source' option (default file access problem).";
		fi;
	fi;
else
	# Test : data exists.
	if ! Test_File -PATH "${PARAM[SOURCE]}"; then
		# Test : folder.
		if [[ "${PARAM[SOURCE]}" == */ ]]; then
			if Test_File -PATH "${PARAM[SOURCE]%[\\/]}${CONF[SEPARATOR]}report.json"; then
				PARAM["SOURCE"]="${PARAM[SOURCE]%[\\/]}${CONF[SEPARATOR]}report.json";
			else
				# Loop : auto-discovery - last file win data state.
				for var_param_source_file in $(ls -1 "${PARAM[SOURCE]%[\\/]}${CONF[SEPARATOR]}"DR-????????_??????.{json,yaml} 2>/dev/null | sort); do
					# Test : current exists.
					if Test_File -PATH "$var_param_source_file"; then
						PARAM["SOURCE"]="$var_param_source_file";
						var_param_source_file_path="${PARAM[SOURCE]}";
					fi;
				done;
				# Test : data exists.
				if [[ "$var_param_source_file_path" == "" ]]; then
					Stop_App -CODE 1 -TEXT "Need valid value for '--source' option (custom folder access problem).";
				fi;
			fi;
		else
			Stop_App -CODE 1 -TEXT "Need valid value for '--source' option (custom file access problem).";
		fi;
	fi;
fi;
if ! [[ "${PARAM[SOURCE]}" =~ \.(csv|json|yaml)$ ]]; then Stop_App -CODE 1 -TEXT "Invalid format for '--source' option."; fi;
# DESTINATION.
if [[ "${PARAM[DESTINATION]}" == "facebook" ]]; then
	if [[ -z "${PARAM[CREDENTIAL]}" ]]; then
		Stop_App -CODE 1 -TEXT "Need '--credential' option for 'facebook' destination." -HELP;
	fi;
elif [[ "${PARAM[DESTINATION]}" == "jekyll" ]]; then
	# OUTPUT.
	if [ -n "${PARAM[OUTPUT]}" ]; then
		# Test : data exists.
		if ! Test_File -PATH "${PARAM[OUTPUT]}"; then
			# Test : folder.
			if Test_Dir -PATH "${PARAM[OUTPUT]}"; then
				# Test : overwrite.
				if [[ "${PARAM[UPON]}" == true ]]; then
					PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}${CONF[SEPARATOR]}${DATE[DATE0]}-0-report.md";
				else
					# Loop : auto-discovery - last file win data state.
					var_param_output="${PARAM[OUTPUT]%[\\/]}";
					for var_param_output_single_file in "${var_param_output}${CONF[SEPARATOR]}${DATE[DATE0]}"-*-report.md; do
						# Test : file not pattern.
						if Test_File -PATH "$var_param_output_single_file"; then
							var_param_output_last_file="$var_param_output_single_file";
						fi;
					done;
					# Test : several reports for the day.
					if [ -n "$var_param_output_last_file" ]; then
						var_param_output_last_file_id=$(echo "$var_param_output_last_file" | grep -Po '\-[0-9]+\-(?=report\.md)' | sed 's/-//g');
						var_param_output_last_file_id=$((var_param_output_last_file_id + 1));
						PARAM["OUTPUT"]="${var_param_output}${CONF[SEPARATOR]}${DATE[DATE0]}-${var_param_output_last_file_id}-report.md";
					else
						PARAM["OUTPUT"]="${var_param_output}${CONF[SEPARATOR]}${DATE[DATE0]}-0-report.md";
					fi;
				fi;
			fi;
		else
			Stop_App -CODE 1 -TEXT "'--output' option need folder path." -HELP;
		fi;
	else
		# Test : overwrite.
		if [[ "${PARAM[UPON]}" == true ]]; then
			PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[DATE0]}-0-report.md";
		else
			# Loop : auto-discovery - last file win data state.
			for var_param_output_single_file in "${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[DATE0]}"-*-report.md; do
				# Test : file not pattern.
				if Test_File -PATH "$var_param_output_single_file"; then
					var_param_output_last_file="$var_param_output_single_file";
				fi;
			done;
			# Test : several reports for the day.
			if [ -n "$var_param_output_last_file" ]; then
				var_param_output_last_file_id=$(echo "$var_param_output_last_file" | grep -Po '\-[0-9]+\-(?=report\.md)' | sed 's/-//g');
				var_param_output_last_file_id=$((var_param_output_last_file_id + 1));
				PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[DATE0]}-${var_param_output_last_file_id}-report.md";
			else
				PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[DATE0]}-0-report.md";
			fi;
		fi;
	fi;
else
	Stop_App -CODE 1 -TEXT "Software not supported.";
fi;
# UPON - don't rewrite but increment by default.
if [ -z "${PARAM[UPON]}" ]; then PARAM["UPON"]="${DEFAULT[UPON]}"; fi;
# FORMAT - Redefine requirements for user-friendly work.
if [[ "${PARAM[DESTINATION]}" == "facebook" ]]; then
	# Tests : format.
	if [[ "${PARAM[SOURCE]}" == *.json ]]; then
		APP["REQUIREMENTS"]="program jq mandatory#program curl mandatory";
	elif [[ "${PARAM[SOURCE]}" == *.yaml ]]; then
		APP["REQUIREMENTS"]="program yq mandatory#program curl mandatory";
	fi;
else
	# Tests : format.
	if [[ "${PARAM[SOURCE]}" == *.json ]]; then
		APP["REQUIREMENTS"]="program jq mandatory";
	elif [[ "${PARAM[SOURCE]}" == *.yaml ]]; then
		APP["REQUIREMENTS"]="program yq mandatory";
	fi;
fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Publish_LocalReportToFacebook.
#	DESCRIPTION : construct post content for Facebook.
function Publish_LocalReportToFacebook() {
	# Some definition.
	local var_content;
	local var_format;
	local var_credential;
	local var_fb_id;
	local var_fb_token;
	local var_fb_post;
	local var_fb_date;
	local var_fb_date_orig;
	local var_fb_response;
	local var_name;
	local var_data;
	local var_version_old;
	local var_version_new;
	local var_version_test;
	local var_url_old;
	local var_url_new;
	local var_source_old;
	local var_source_new;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-CONTENT)
				var_content="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct credentials.
	var_fb_id=$(echo "$var_credential" | cut -d ':' -f1);
	if [[ "$var_fb_id" == "" ]]; then var_fb_id="${DEFAULT[ID]}"; fi;
	var_fb_token=$(echo "$var_credential" | cut -d ':' -f2);
	# Set post.
	Write_Script -TEXT "API call : ID '${var_fb_id}'";
	echo "";
	# Tests : format.
	if [[ "$var_format" == "json" ]]; then
		var_fb_date="$(echo "$var_content" | jq -r -c '.revision')";
		var_fb_date_orig="${var_fb_date/T/ }";
		var_fb_date="${var_fb_date_orig:0:19} ${var_fb_date_orig:19:3}:${var_fb_date_orig:22:2}";
		var_fb_post+="Report - ${var_fb_date}\n";
		# Loop : Process Substitution distribution.
		while read -r var_data; do
			var_name="$(echo "$var_data" | jq -r -c '.name')";
			var_version_old="$(echo "$var_data" | jq -r -c '.version.old')";
			var_version_new="$(echo "$var_data" | jq -r -c '.version.new')";
			var_url_old="$(echo "$var_data" | jq -r -c '.url.old')";
			var_url_new="$(echo "$var_data" | jq -r -c '.url.new')";
			var_source_old="$(echo "$var_data" | jq -r -c '.source.old')";
			var_source_new="$(echo "$var_data" | jq -r -c '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			# Test : version state.
			if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
				var_version_test="downgrade";
			else
				var_version_test="upgrade";
			fi;
			# Test : new distribution.
			if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
				var_fb_post+="\n\ndistribution ${var_name} - new :\n";
				Write_Script -TEXT "publication for new distribution '${var_name}' :";
			else
				var_fb_post+="\n\ndistribution ${var_name} - changes :\n";
				Write_Script -TEXT "publication for changed distribution '${var_name}' :";
			fi;
			# Test : version.
			if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'\n";
				Write_Script -TEXT " version changed";
			elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- version '${var_version_new}'\n";
				Write_Script -TEXT " version new";
			elif [[ "$var_version_old" == "$var_version_new" ]]; then
				Write_Script -TEXT " version not modified";
			else
				var_fb_post+="- version undefined - warning\n";
				Write_Script -TEXT " current version undefined";
			fi;
			# Test : url.
			if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- last url from '${var_url_old}' to '${var_url_new}'\n";
				Write_Script -TEXT " url changed";
			elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- url '${var_url_new}'\n";
				Write_Script -TEXT " url new";
			elif [[ "$var_url_old" == "$var_url_new" ]]; then
				Write_Script -TEXT " url not modified";
			else
				var_fb_post+="- url undefined - warning\n";
				Write_Script -TEXT " current url undefined";
			fi;
			# Test : source.
			if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source from '${var_source_old}' to '${var_source_new}'\n";
				Write_Script -TEXT " source changed";
			elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source '${var_source_new}'\n";
				Write_Script -TEXT " source new";
			elif [[ "$var_source_old" == "$var_source_new" ]]; then
				Write_Script -TEXT " source not modified";
			else
				var_fb_post+="- source undefined - warning\n";
				Write_Script -TEXT " current source undefined";
			fi;
			if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
		done < <(echo "$var_content" | jq -r -c '.distro[]');
	elif [[ "$var_format" == "yaml" ]]; then
		var_fb_date="$(echo "$var_content" | yq '.revision')";
		var_fb_date_orig="${var_fb_date/T/ }";
		var_fb_date="${var_fb_date_orig:0:19} ${var_fb_date_orig:19:3}:${var_fb_date_orig:22:2}";
		var_fb_post+="Report - ${var_fb_date}\n";
		# Loop : Process Substitution distribution.
		while read -r var_name; do
			var_data=$(echo "$var_content" | yq eval ".distro[] | select(.name == \"$var_name\")");
			var_version_old="$(echo "$var_data" | yq eval '.version.old')";
			var_version_new="$(echo "$var_data" | yq eval '.version.new')";
			var_url_old="$(echo "$var_data" | yq eval '.url.old')";
			var_url_new="$(echo "$var_data" | yq eval '.url.new')";
			var_source_old="$(echo "$var_data" | yq eval '.source.old')";
			var_source_new="$(echo "$var_data" | yq eval '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			# Test : version state.
			if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
				var_version_test="downgrade";
			else
				var_version_test="upgrade";
			fi;
			# Test : new distribution.
			if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
				var_fb_post+="\n\ndistribution ${var_name} - new :\n";
				Write_Script -TEXT "publication for new distribution '${var_name}' :";
			else
				var_fb_post+="\n\ndistribution ${var_name} - changes :\n";
				Write_Script -TEXT "publication for changed distribution '${var_name}' :";
			fi;
			# Test : version.
			if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'\n";
				Write_Script -TEXT " version changed";
			elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- version '${var_version_new}'\n";
				Write_Script -TEXT " version new";
			elif [[ "$var_version_old" == "$var_version_new" ]]; then
				Write_Script -TEXT " version not modified";
			else
				var_fb_post+="- version undefined - warning\n";
				Write_Script -TEXT " current version undefined";
			fi;
			# Test : url.
			if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- last url from '${var_url_old}' to '${var_url_new}'\n";
				Write_Script -TEXT " url changed";
			elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- url '${var_url_new}'\n";
				Write_Script -TEXT " url new";
			elif [[ "$var_url_old" == "$var_url_new" ]]; then
				Write_Script -TEXT " url not modified";
			else
				var_fb_post+="- url undefined - warning\n";
				Write_Script -TEXT " current url undefined";
			fi;
			# Test : source.
			if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source from '${var_source_old}' to '${var_source_new}'\n";
				Write_Script -TEXT " source changed";
			elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source '${var_source_new}'\n";
				Write_Script -TEXT " source new";
			elif [[ "$var_source_old" == "$var_source_new" ]]; then
				Write_Script -TEXT " source not modified";
			else
				var_fb_post+="- source undefined - warning\n";
				Write_Script -TEXT " current source undefined";
			fi;
			if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
		done < <(echo "$var_content" | yq '.distro[].name');
	else
		return 1;
	fi;
	var_fb_response=$(curl -s -X POST "https://graph.facebook.com/v21.0/${var_fb_id}/feed?access_token=${var_fb_token}" -H "Content-Type: application/json" -d "{\"message\":\"${var_fb_post}\"}");
	Write_Script -TEXT "API response : '${var_fb_response}'";
}

# Publish_LocalReportToJekyll.
#	DESCRIPTION : construct markdown content for Jekyll.
function Publish_LocalReportToJekyll() {
	# Some definition.
	local var_content;
	local var_format;
	local var_output;
	local var_array;
	local var_data;
	local var_name;
	local var_version_old;
	local var_version_new;
	local var_version_test;
	local var_url_old;
	local var_url_new;
	local var_source_old;
	local var_source_new;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-CONTENT)
				var_content="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Set file.
	echo -e "---\ntitle: \"${DATE[DATE0]}\"\n\nlayout: post\ncategories: changes\ndate: ${DATE[DATETIME2]} +0200\n\nauthor: reporter\n---\n" > "$var_output";
	# Tests : format.
	if [[ "$var_format" == "json" ]]; then
		# Loop : Process Substitution distribution.
		while read -r var_data; do
			var_name="$(echo "$var_data" | jq -r -c '.name')";
			var_version_old="$(echo "$var_data" | jq -r -c '.version.old')";
			var_version_new="$(echo "$var_data" | jq -r -c '.version.new')";
			var_url_old="$(echo "$var_data" | jq -r -c '.url.old')";
			var_url_new="$(echo "$var_data" | jq -r -c '.url.new')";
			var_source_old="$(echo "$var_data" | jq -r -c '.source.old')";
			var_source_new="$(echo "$var_data" | jq -r -c '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			Publish_LocalReportToJekyllPost -NAME "${var_name}" -VERSION "${var_version_old}|${var_version_new}" -URL "${var_url_old}|${var_url_new}" -SOURCE "${var_source_old}|${var_source_new}" -OUTPUT "${var_output}";
		done < <(echo "$var_content" | jq -r -c '.distro[]');
	elif [[ "$var_format" == "yaml" ]]; then
		# Loop : Process Substitution distribution.
		while read -r var_name; do
			var_data=$(echo "$var_content" | yq eval ".distro[] | select(.name == \"$var_name\")");
			var_version_old="$(echo "$var_data" | yq eval '.version.old')";
			var_version_new="$(echo "$var_data" | yq eval '.version.new')";
			var_url_old="$(echo "$var_data" | yq eval '.url.old')";
			var_url_new="$(echo "$var_data" | yq eval '.url.new')";
			var_source_old="$(echo "$var_data" | yq eval '.source.old')";
			var_source_new="$(echo "$var_data" | yq eval '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			Publish_LocalReportToJekyllPost -NAME "${var_name}" -VERSION "${var_version_old}|${var_version_new}" -URL "${var_url_old}|${var_url_new}" -SOURCE "${var_source_old}|${var_source_new}" -OUTPUT "${var_output}";
		done < <(echo "$var_content" | yq '.distro[].name');
	else
		return 1;
	fi;
}

# Publish_LocalReportToJekyllPost.
#	DESCRIPTION : construct markdown content for Jekyll.
function Publish_LocalReportToJekyllPost() {
	# Some definition.
	local var_name;
	local var_version;
	local var_version_old;
	local var_version_new;
	local var_version_test;
	local var_url;
	local var_url_old;
	local var_url_new;
	local var_source;
	local var_source_old;
	local var_source_new;
	local var_output;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct element values.
	var_version_old=$(echo "$var_version" | cut -d '|' -f1);
	var_version_new=$(echo "$var_version" | cut -d '|' -f2);
	var_url_old=$(echo "$var_url" | cut -d '|' -f1);
	var_url_new=$(echo "$var_url" | cut -d '|' -f2);
	var_source_old=$(echo "$var_source" | cut -d '|' -f1);
	var_source_new=$(echo "$var_source" | cut -d '|' -f2);
	# Test : version state.
	if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
		var_version_test="downgrade";
	else
		var_version_test="upgrade";
	fi;
	# Test : new distribution.
	if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
		echo -e "distribution ${var_name} - new :" >> "$var_output";
		Write_Script -TEXT "publication for new distribution '${var_name}' :";
	else
		echo -e "distribution ${var_name} - changes :" >> "$var_output";
		Write_Script -TEXT "publication for changed distribution '${var_name}' :";
	fi;
	# Test : version.
	if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
		echo -e "- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'" >> "$var_output";
		Write_Script -TEXT " version changed";
	elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
		echo -e "- version '${var_version_new}'" >> "$var_output";
		Write_Script -TEXT " version new";
	elif [[ "$var_version_old" == "$var_version_new" ]]; then
		Write_Script -TEXT " version not modified";
	else
		echo -e "- version undefined - warning" >> "$var_output";
		Write_Script -TEXT " current version undefined";
	fi;
	# Test : url.
	if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
		echo -e "- last url from '${var_url_old}' to '${var_url_new}'" >> "$var_output";
		Write_Script -TEXT " url changed";
	elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
		echo -e "- url '${var_url_new}'" >> "$var_output";
		Write_Script -TEXT " url new";
	elif [[ "$var_url_old" == "$var_url_new" ]]; then
		Write_Script -TEXT " url not modified";
	else
		echo -e "- url undefined - warning" >> "$var_output";
		Write_Script -TEXT " current url undefined";
	fi;
	# Test : source.
	if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
		echo -e "- last source from '${var_source_old}' to '${var_source_new}'" >> "$var_output";
		Write_Script -TEXT " source changed";
	elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
		echo -e "- source '${var_source_new}'" >> "$var_output";
		Write_Script -TEXT " source new";
	elif [[ "$var_source_old" == "$var_source_new" ]]; then
		Write_Script -TEXT " source not modified";
	else
		echo -e "- source undefined - warning" >> "$var_output";
		Write_Script -TEXT " current source undefined";
	fi;
	echo "" >> "$var_output";
	if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_format;
	local var_content;
	local var_content_length;
	# Init.
	Write_Script -TEXT "report generation from '${PARAM[SOURCE]}'.";
	if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	var_content="$(<"${PARAM[SOURCE]}")";
	# Tests : format.
	if [[ "${PARAM[SOURCE]}" == *.json ]]; then
		var_format="json";
		var_content_length="$(echo "$var_content" | jq -r ".distro | length")";
	elif [[ "${PARAM[SOURCE]}" == *.yaml ]]; then
		var_format="yaml";
		var_content_length=$(echo "$var_content" | yq '.distro | length');
	else
		Stop_App -CODE 1 -TEXT "Invalid format for '--source' option.";
	fi;
	# Test : content exists with number control avoid null count.
	if [[ "$var_content_length" =~ ^[0-9]+$ ]] && [[ "$var_content_length" -gt 0 ]]; then
		# Tests : website Facebook, software Jekyll.
		if [[ "${PARAM[DESTINATION]}" == "facebook" ]]; then
			if Publish_LocalReportToFacebook -CONTENT "$var_content" -FORMAT "$var_format" -CREDENTIAL "${PARAM[CREDENTIAL]}"; then
				if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
				Write_Script -TEXT "report publication generated.";
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
				Stop_App -CODE 1 -TEXT "report publication failed.";
			fi;
		elif [[ "${PARAM[DESTINATION]}" == "jekyll" ]]; then
			if Publish_LocalReportToJekyll -CONTENT "$var_content" -FORMAT "$var_format" -OUTPUT "${PARAM[OUTPUT]}"; then
				Write_Script -TEXT "report publication generated.";
			else
				Stop_App -CODE 1 -TEXT "report publication failed.";
			fi;
		fi;
	else
		# Test : data exists.
		if rm "${PARAM[OUTPUT]}" 2>/dev/null; then
			Write_Script -TEXT "report removed because can't be generated from null data.";
		else
			Write_Script -TEXT "report can't be generated from null data and can't be removed.";
		fi;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;
