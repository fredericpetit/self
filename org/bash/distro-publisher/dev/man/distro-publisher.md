<blockquote id="out2tag">



NAME



	distro-publisher - Treat Distro Reporter output.





REQUIREMENTS



	program 'jq' (optionnal)



 	program 'yq' (optionnal)



 	program 'ffmpeg' (optionnal)



 

SYNOPSIS



	distro-publisher OPTIONS





DESCRIPTION



	This open-source tool publish reports to Bluesky, Facebook, Jekyll or TikTok.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path or single local folder path to Distro Reporter report.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : try first 'PWD/distro-reporter.json' or 'PWD/distro-reporter.yaml', and after 'PWD/DR-????????_??????.json/yaml'.



 	-d NAME,

	--destination NAME

 		Product name for publish.

 		- state : mandatory.

 		- accepted value(s) : 'bluesky', 'facebook', 'jekyll' or 'tiktok'.

 		- default value : no default.



 	-c CREDENTIAL,

	--credential CREDENTIAL

 		Login/ID & password/token of the product, delimited by ':'.

 		- state : optionnal.

 		- accepted value(s) : string.

 		- default value : no default.



 	-o FOLDERPATH,

	--output FOLDERPATH

 		Single local folder path to store the final result (final slash automatically defined) with jekyll '--destination' option.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD/DATE-X-report.md' (date name mode) with jekyll '--destination' option.



 	-u (no type),

	--upon (no type)

 		Allow to overwrite previous datas. New and old datas need to be in the same directory.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	generate Bluesky post from JSON content in custom name mode (manual choice) :

		distro-publisher -s report.json -d bluesky -c BLUESKY_ID:BLUESKY_TOKEN



 	generate Facebook post from JSON content in custom name mode (manual choice) :

		distro-publisher -s report.json -d facebook -c FACEBOOK_ID:FACEBOOK_TOKEN



 	generate Jekyll post from YAML content in date name mode (auto-discovery) :

		distro-publisher -d jekyll -o ./_posts/



 

TO-DO



	x





NOTES



	Compatible Distro Tracker >= 1.2.0.



 

CREDITS & THANKS



	x





CHANGELOG



	1.1.0 - 2024-12-01

		Add Bluesky posting. Remove showing url in a Facebook post due to false positive on their antispam. Add fullname information.



 	1.0.0 - 2024-10-13

		Refactor.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.1.0

	License : GPL-3.0-or-later





</blockquote>
