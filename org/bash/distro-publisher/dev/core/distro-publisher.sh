#!/bin/bash
# shellcheck source=/dev/null
#  _________________________________________________
# |                                                 | #
# |               distro-publisher.sh               | #
# |_________________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-publisher";
APP["SECTION"]="1";
APP["SHORTNAME"]="DP";
APP["SLOGAN"]="Treat Distro Reporter output.";
APP["DESCRIPTION"]="This open-source tool publish reports to Bluesky, Facebook, Jekyll or TikTok.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENTS"]="program jq optionnal#program yq optionnal#program ffmpeg optionnal";
APP["EXAMPLES"]="generate Bluesky post from JSON content in custom name mode (manual choice) :|${APP[NAME]} -s report.json -d bluesky -c BLUESKY_ID:BLUESKY_TOKEN#generate Facebook post from JSON content in custom name mode (manual choice) :|${APP[NAME]} -s report.json -d facebook -c FACEBOOK_ID:FACEBOOK_TOKEN#generate Jekyll post from YAML content in date name mode (auto-discovery) :|${APP[NAME]} -d jekyll -o ./_posts/";
APP["TODO"]="null";
APP["NOTES"]="Compatible Distro Tracker >= 1.2.0.";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.1.0|2024-12-01T10:00:00+0200|Add Bluesky posting. Remove showing url in a Facebook post due to false positive on their antispam. Add fullname information.#1.0.0|2024-10-13T10:00:00+0200|Refactor.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="1.1.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#optionnal#Single local file path or single local folder path to Distro Reporter report.|path.|try first 'PWD/distro-reporter.json' or 'PWD/distro-reporter.yaml', and after 'PWD/DR-????????_??????.json/yaml'.";
OPTIONS["DESTINATION"]="2#d#NAME#mandatory#Product name for publish.|'bluesky', 'facebook', 'jekyll' or 'tiktok'.|no default.";
OPTIONS["CREDENTIAL"]="3#c#CREDENTIAL#optionnal#Login/ID & password/token of the product, delimited by ':'.|string.|no default.";
OPTIONS["OUTPUT"]="4#o#FOLDERPATH#optionnal#Single local folder path to store the final result (final slash automatically defined) with jekyll '--destination' option.|path.|'PWD/DATE-X-report.md' (date name mode) with jekyll '--destination' option.";
OPTIONS["UPON"]="5#u#null#optionnal#Allow to overwrite previous datas. New and old datas need to be in the same directory.|null|'false' because disabled.";
OPTIONS["QUIET"]="6#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="7#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="8#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--destination|-d)
			PARAM["DESTINATION"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--credential|-c)
			PARAM["CREDENTIAL"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -CONTAINS ":"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--upon|-u)
			PARAM["UPON"]=true
			shift
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["EXTENSIONS"]="yaml|json";
DEFAULT["SOURCE"]="${PWD%[\\/]}${CONF[SEPARATOR]}distro-reporter.json";
if [[ "${PARAM[DESTINATION]}" == "bluesky" ]]; then
	DEFAULT["ID"]="distrotracker.bsky.social";
elif [[ "${PARAM[DESTINATION]}" == "facebook" ]]; then
	DEFAULT["ID"]="294221940452287";
else
	DEFAULT["ID"]="";
fi;
DEFAULT["OUTPUT"]="${PWD%[\\/]}";
DEFAULT["UPON"]="false";
DEFAULT["QUIET"]=false;
# Some script definition.
# SOURCE - If it's a folder, input format will be known only after auto-discovery.
if [ -z "${PARAM[SOURCE]}" ]; then PARAM["SOURCE"]="${DEFAULT[SOURCE]}"; fi;
# Tests : custom || distro-reporter.json || distro-reporter.{json,yaml} || DR-????????_??????.{json,yaml} .
if ! Test_File -PATH "${PARAM[SOURCE]}"; then
	# Tests : name mode, date mode - Use pipe in '--EXTENSION' because regex treatment.
	var_param_source_file=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-reporter" -MODE "name");
	if [ -n "$var_param_source_file" ]; then
		PARAM["SOURCE"]="$var_param_source_file";
	else
		var_param_source_file=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-reporter" -MODE "date");
		if [ -n "$var_param_source_file" ]; then
			PARAM["SOURCE"]="$var_param_source_file";
		else
			Stop_App -CODE 1 -TEXT "Need valid folder path value for '--source' option.";
		fi;
	fi;
fi;
# Test : file.
if ! Test_File -PATH "${PARAM[SOURCE]}"; then
	Stop_App -CODE 1 -TEXT "Need valid file for '--source' option.";
fi;
if ! [[ "${PARAM[SOURCE]}" =~ \.(json|yaml)$ ]]; then Stop_App -CODE 1 -TEXT "Invalid format for '--source' option."; fi;
# DESTINATION.
if [[ "${PARAM[DESTINATION]}" == "bluesky" ]]; then
	if [[ -z "${PARAM[CREDENTIAL]}" ]]; then
		Stop_App -CODE 1 -TEXT "Need '--credential' option for 'bluesky' destination." -HELP;
	fi;
elif [[ "${PARAM[DESTINATION]}" == "facebook" ]]; then
	if [[ -z "${PARAM[CREDENTIAL]}" ]]; then
		Stop_App -CODE 1 -TEXT "Need '--credential' option for 'facebook' destination." -HELP;
	fi;
elif [[ "${PARAM[DESTINATION]}" == "jekyll" ]]; then
	# OUTPUT.
	if [ -n "${PARAM[OUTPUT]}" ]; then
		# Test : data exists || error.
		if ! Test_File -PATH "${PARAM[OUTPUT]}"; then
			# Test : folder || error.
			if Test_Dir -PATH "${PARAM[OUTPUT]}"; then
				# Search : ????-??-??-?-report.md .
				var_param_output_file=$(Search_File -PATH "${PARAM[OUTPUT]}" -EXTENSION "md" -TOOL "distro-publisher" -MODE "date");
				# Test : several reports for the day || new.
				if [ -n "$var_param_output_file" ]; then
					var_param_output_file_id=$(echo "$var_param_output_file" | grep -Po '\-[0-9]+\-(?=report\.md)' | sed 's/-//g');
					# Test : overwrite || increment.
					if [[ "${PARAM[UPON]}" == true ]]; then
						PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}${CONF[SEPARATOR]}${DATE[DATE0]}-${var_param_output_file_id}-report.md";
					else
						var_param_output_file_id=$((var_param_output_file_id + 1));
						PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}${CONF[SEPARATOR]}${DATE[DATE0]}-${var_param_output_file_id}-report.md";
					fi;
				else
					PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}${CONF[SEPARATOR]}${DATE[DATE0]}-0-report.md";
				fi;
			else
				Stop_App -CODE 1 -TEXT "Need valid folder path for '--output' option." -HELP;
			fi;
		else
			Stop_App -CODE 1 -TEXT "Need folder path for '--output' option, not file path." -HELP;
		fi;
	else
		# Search : ????-??-??-?-report.md .
		var_param_output_file=$(Search_File -PATH "${DEFAULT[OUTPUT]}" -EXTENSION "md" -TOOL "distro-publisher" -MODE "date");
		# Test : several reports for the day || new.
		if [ -n "$var_param_output_file" ]; then
			var_param_output_file_id=$(echo "$var_param_output_file" | grep -Po '\-[0-9]+\-(?=report\.md)' | sed 's/-//g');
			# Test : overwrite || increment.
			if [[ "${PARAM[UPON]}" == true ]]; then
				PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[DATE0]}-0-report.md";
			else
				var_param_output_file_id=$((var_param_output_file_id + 1));
				PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[DATE0]}-${var_param_output_file_id}-report.md";
			fi;
		else
			PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}${CONF[SEPARATOR]}${DATE[DATE0]}-0-report.md";
		fi;
	fi;
else
	Stop_App -CODE 1 -TEXT "Software not supported.";
fi;
# UPON - don't rewrite but increment by default.
if [ -z "${PARAM[UPON]}" ]; then PARAM["UPON"]="${DEFAULT[UPON]}"; fi;
# FORMAT - Redefine requirements for user-friendly work.
if [[ "${PARAM[DESTINATION]}" == "bluesky" || "${PARAM[DESTINATION]}" == "facebook" ]]; then
	# Tests : format.
	if [[ "${PARAM[SOURCE]}" == *.json ]]; then
		APP["REQUIREMENTS"]="program jq mandatory#program curl mandatory";
	elif [[ "${PARAM[SOURCE]}" == *.yaml ]]; then
		APP["REQUIREMENTS"]="program yq mandatory#program curl mandatory";
	fi;
elif [[ "${PARAM[DESTINATION]}" == "tiktok" ]]; then
	# Tests : format.
	if [[ "${PARAM[SOURCE]}" == *.json ]]; then
		APP["REQUIREMENTS"]="program jq mandatory#program ffmpeg mandatory";
	elif [[ "${PARAM[SOURCE]}" == *.yaml ]]; then
		APP["REQUIREMENTS"]="program yq mandatory#program ffmpeg mandatory";
	fi;
else
	# Tests : format.
	if [[ "${PARAM[SOURCE]}" == *.json ]]; then
		APP["REQUIREMENTS"]="program jq mandatory";
	elif [[ "${PARAM[SOURCE]}" == *.yaml ]]; then
		APP["REQUIREMENTS"]="program yq mandatory";
	fi;
fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Publish_LocalReportToBluesky.
#	DESCRIPTION : construct post content for Bluesky.
function Publish_LocalReportToBluesky() {
	# Some definition.
	local var_content;
	local var_format;
	local var_credential;
	local var_bs_id;
	local var_bs_password;
	local var_bs_token;
	local var_bs_post;
	local var_bs_date;
	local var_bs_date_orig;
	local var_bs_response;
	local var_name;
	local var_fullname;
	local var_data;
	local var_version_old;
	local var_version_new;
	local var_version_test;
	local var_url_old;
	local var_url_new;
	local var_source_old;
	local var_source_new;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CONTENT)
				var_content="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct credentials.
	var_bs_id=$(echo "$var_credential" | cut -d ':' -f1);
	if [[ "$var_bs_id" == "" ]]; then var_bs_id="${DEFAULT[ID]}"; fi;
	var_bs_password=$(echo "$var_credential" | cut -d ':' -f2);
	# Set post.
	Write_Script -TEXT "API call : ID '${var_bs_id}'";
	echo "";
	# Tests : format.
	if [[ "$var_format" == "json" ]]; then
		var_bs_date="$(echo "$var_content" | jq -r -c '.revision')";
		var_bs_date_orig="${var_bs_date/T/ }";
		var_bs_date="${var_bs_date_orig:0:19} ${var_bs_date_orig:19:3}:${var_bs_date_orig:22:2}";
		var_bs_post+="Report - ${var_bs_date}\n";
		# Loop : Process Substitution distribution.
		while read -r var_data; do
			var_name="$(echo "$var_data" | jq -r -c '.name')";
			var_fullname="$(echo "$var_data" | jq -r -c '.fullname')";
			var_version_old="$(echo "$var_data" | jq -r -c '.version.old')";
			var_version_new="$(echo "$var_data" | jq -r -c '.version.new')";
			var_url_old="$(echo "$var_data" | jq -r -c '.url.old')";
			var_url_new="$(echo "$var_data" | jq -r -c '.url.new')";
			var_source_old="$(echo "$var_data" | jq -r -c '.source.old')";
			var_source_new="$(echo "$var_data" | jq -r -c '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			# Test : version state.
			if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
				var_version_test="downgrade";
			else
				var_version_test="upgrade";
			fi;
			# Test : new distribution.
			if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
				var_bs_post+="\ndistribution ${var_fullname} - new :\n";
				Write_Script -TEXT "publication for new distribution '${var_name}' :";
			else
				var_bs_post+="\ndistribution ${var_fullname} - changes :\n";
				Write_Script -TEXT "publication for changed distribution '${var_name}' :";
			fi;
			# Test : version.
			if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
				var_bs_post+="- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'\n";
				Write_Script -TEXT " version changed";
			elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
				var_bs_post+="- version '${var_version_new}'\n";
				Write_Script -TEXT " version new";
			elif [[ "$var_version_old" == "$var_version_new" ]]; then
				Write_Script -TEXT " version not modified";
			else
				var_bs_post+="- version undefined - warning\n";
				Write_Script -TEXT " current version undefined";
			fi;
			# Test : url.
			if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
				var_bs_post+="- url changed\n";
				Write_Script -TEXT " url changed";
			elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
				var_bs_post+="- url new\n";
				Write_Script -TEXT " url new";
			elif [[ "$var_url_old" == "$var_url_new" ]]; then
				Write_Script -TEXT " url not modified";
			else
				var_bs_post+="- url undefined - warning\n";
				Write_Script -TEXT " current url undefined";
			fi;
			# Test : source.
			if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
				var_bs_post+="- source from '${var_source_old}' to '${var_source_new}'\n";
				Write_Script -TEXT " source changed";
			elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
				var_bs_post+="- source '${var_source_new}'\n";
				Write_Script -TEXT " source new";
			elif [[ "$var_source_old" == "$var_source_new" ]]; then
				Write_Script -TEXT " source not modified";
			else
				var_bs_post+="- source undefined - warning\n";
				Write_Script -TEXT " current source undefined";
			fi;
			if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
		done < <(echo "$var_content" | jq -r -c '.distro[]');
	elif [[ "$var_format" == "yaml" ]]; then
		var_bs_date="$(echo "$var_content" | yq '.revision')";
		var_bs_date_orig="${var_bs_date/T/ }";
		var_bs_date="${var_bs_date_orig:0:19} ${var_bs_date_orig:19:3}:${var_bs_date_orig:22:2}";
		var_bs_post+="Report - ${var_bs_date}\n";
		# Loop : Process Substitution distribution.
		while read -r var_name; do
			var_data=$(echo "$var_content" | yq eval ".distro[] | select(.name == \"$var_name\")");
			var_fullname="$(echo "$var_data" | yq eval '.fullname')";
			var_version_old="$(echo "$var_data" | yq eval '.version.old')";
			var_version_new="$(echo "$var_data" | yq eval '.version.new')";
			var_url_old="$(echo "$var_data" | yq eval '.url.old')";
			var_url_new="$(echo "$var_data" | yq eval '.url.new')";
			var_source_old="$(echo "$var_data" | yq eval '.source.old')";
			var_source_new="$(echo "$var_data" | yq eval '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			# Test : version state.
			if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
				var_version_test="downgrade";
			else
				var_version_test="upgrade";
			fi;
			# Test : new distribution.
			if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
				var_bs_post+="\ndistribution ${var_fullname} - new :\n";
				Write_Script -TEXT "publication for new distribution '${var_name}' :";
			else
				var_bs_post+="\ndistribution ${var_fullname} - changes :\n";
				Write_Script -TEXT "publication for changed distribution '${var_name}' :";
			fi;
			# Test : version.
			if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
				var_bs_post+="- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'\n";
				Write_Script -TEXT " version changed";
			elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
				var_bs_post+="- version '${var_version_new}'\n";
				Write_Script -TEXT " version new";
			elif [[ "$var_version_old" == "$var_version_new" ]]; then
				Write_Script -TEXT " version not modified";
			else
				var_bs_post+="- version undefined - warning\n";
				Write_Script -TEXT " current version undefined";
			fi;
			# Test : url.
			if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
				var_bs_post+="- url changed\n";
				Write_Script -TEXT " url changed";
			elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
				var_bs_post+="- url new\n";
				Write_Script -TEXT " url new";
			elif [[ "$var_url_old" == "$var_url_new" ]]; then
				Write_Script -TEXT " url not modified";
			else
				var_bs_post+="- url undefined - warning\n";
				Write_Script -TEXT " current url undefined";
			fi;
			# Test : source.
			if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
				var_bs_post+="- source from '${var_source_old}' to '${var_source_new}'\n";
				Write_Script -TEXT " source changed";
			elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
				var_bs_post+="- source '${var_source_new}'\n";
				Write_Script -TEXT " source new";
			elif [[ "$var_source_old" == "$var_source_new" ]]; then
				Write_Script -TEXT " source not modified";
			else
				var_bs_post+="- source undefined - warning\n";
				Write_Script -TEXT " current source undefined";
			fi;
			if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
		done < <(echo "$var_content" | yq '.distro[].name');
	else
		return 1;
	fi;
	# Authentification
	var_bs_token=$(curl -s -X POST "https://bsky.social/xrpc/com.atproto.server.createSession" -H "Content-Type: application/json" -d "{\"identifier\": \"$var_bs_id\", \"password\": \"$var_bs_password\"}" | jq -r '.accessJwt');
	# Publication
	var_bs_response=$(curl -X POST "https://bsky.social/xrpc/com.atproto.repo.createRecord" -H "Authorization: Bearer $var_bs_token" -H "Content-Type: application/json" -d "{ \"collection\": \"app.bsky.feed.post\", \"repo\": \"$var_bs_id\", \"record\": { \"text\": \"$var_bs_post\", \"createdAt\": \"$(date --iso-8601=seconds)\" }}");
	Write_Script -TEXT "API response : '${var_bs_response}'";
}

# Publish_LocalReportToFacebook.
#	DESCRIPTION : construct post content for Facebook.
function Publish_LocalReportToFacebook() {
	# Some definition.
	local var_content;
	local var_format;
	local var_credential;
	local var_fb_id;
	local var_fb_token;
	local var_fb_post;
	local var_fb_date;
	local var_fb_date_orig;
	local var_fb_response;
	local var_name;
	local var_fullname;
	local var_data;
	local var_version_old;
	local var_version_new;
	local var_version_test;
	local var_url_old;
	local var_url_new;
	local var_source_old;
	local var_source_new;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CONTENT)
				var_content="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct credentials.
	var_fb_id=$(echo "$var_credential" | cut -d ':' -f1);
	if [[ "$var_fb_id" == "" ]]; then var_fb_id="${DEFAULT[ID]}"; fi;
	var_fb_token=$(echo "$var_credential" | cut -d ':' -f2);
	# Set post.
	Write_Script -TEXT "API call : ID '${var_fb_id}'";
	echo "";
	# Tests : format.
	if [[ "$var_format" == "json" ]]; then
		var_fb_date="$(echo "$var_content" | jq -r -c '.revision')";
		var_fb_date_orig="${var_fb_date/T/ }";
		var_fb_date="${var_fb_date_orig:0:19} ${var_fb_date_orig:19:3}:${var_fb_date_orig:22:2}";
		var_fb_post+="Report - ${var_fb_date}\n";
		# Loop : Process Substitution distribution.
		while read -r var_data; do
			var_name="$(echo "$var_data" | jq -r -c '.name')";
			var_fullname="$(echo "$var_data" | jq -r -c '.fullname')";
			var_version_old="$(echo "$var_data" | jq -r -c '.version.old')";
			var_version_new="$(echo "$var_data" | jq -r -c '.version.new')";
			var_url_old="$(echo "$var_data" | jq -r -c '.url.old')";
			var_url_new="$(echo "$var_data" | jq -r -c '.url.new')";
			var_source_old="$(echo "$var_data" | jq -r -c '.source.old')";
			var_source_new="$(echo "$var_data" | jq -r -c '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			# Test : version state.
			if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
				var_version_test="downgrade";
			else
				var_version_test="upgrade";
			fi;
			# Test : new distribution.
			if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
				var_fb_post+="\n\ndistribution ${var_fullname} - new :\n";
				Write_Script -TEXT "publication for new distribution '${var_name}' :";
			else
				var_fb_post+="\n\ndistribution ${var_fullname} - changes :\n";
				Write_Script -TEXT "publication for changed distribution '${var_name}' :";
			fi;
			# Test : version.
			if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'\n";
				Write_Script -TEXT " version changed";
			elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- version '${var_version_new}'\n";
				Write_Script -TEXT " version new";
			elif [[ "$var_version_old" == "$var_version_new" ]]; then
				Write_Script -TEXT " version not modified";
			else
				var_fb_post+="- version undefined - warning\n";
				Write_Script -TEXT " current version undefined";
			fi;
			# Test : url.
			if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- url changed\n";
				Write_Script -TEXT " url changed";
			elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- url new\n";
				Write_Script -TEXT " url new";
			elif [[ "$var_url_old" == "$var_url_new" ]]; then
				Write_Script -TEXT " url not modified";
			else
				var_fb_post+="- url undefined - warning\n";
				Write_Script -TEXT " current url undefined";
			fi;
			# Test : source.
			if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source from '${var_source_old}' to '${var_source_new}'\n";
				Write_Script -TEXT " source changed";
			elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source '${var_source_new}'\n";
				Write_Script -TEXT " source new";
			elif [[ "$var_source_old" == "$var_source_new" ]]; then
				Write_Script -TEXT " source not modified";
			else
				var_fb_post+="- source undefined - warning\n";
				Write_Script -TEXT " current source undefined";
			fi;
			if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
		done < <(echo "$var_content" | jq -r -c '.distro[]');
	elif [[ "$var_format" == "yaml" ]]; then
		var_fb_date="$(echo "$var_content" | yq '.revision')";
		var_fb_date_orig="${var_fb_date/T/ }";
		var_fb_date="${var_fb_date_orig:0:19} ${var_fb_date_orig:19:3}:${var_fb_date_orig:22:2}";
		var_fb_post+="Report - ${var_fb_date}\n";
		# Loop : Process Substitution distribution.
		while read -r var_name; do
			var_data=$(echo "$var_content" | yq eval ".distro[] | select(.name == \"$var_name\")");
			var_fullname="$(echo "$var_data" | yq eval '.fullname')";
			var_version_old="$(echo "$var_data" | yq eval '.version.old')";
			var_version_new="$(echo "$var_data" | yq eval '.version.new')";
			var_url_old="$(echo "$var_data" | yq eval '.url.old')";
			var_url_new="$(echo "$var_data" | yq eval '.url.new')";
			var_source_old="$(echo "$var_data" | yq eval '.source.old')";
			var_source_new="$(echo "$var_data" | yq eval '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			# Test : version state.
			if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
				var_version_test="downgrade";
			else
				var_version_test="upgrade";
			fi;
			# Test : new distribution.
			if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
				var_fb_post+="\n\ndistribution ${var_fullname} - new :\n";
				Write_Script -TEXT "publication for new distribution '${var_name}' :";
			else
				var_fb_post+="\n\ndistribution ${var_fullname} - changes :\n";
				Write_Script -TEXT "publication for changed distribution '${var_name}' :";
			fi;
			# Test : version.
			if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'\n";
				Write_Script -TEXT " version changed";
			elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
				var_fb_post+="- version '${var_version_new}'\n";
				Write_Script -TEXT " version new";
			elif [[ "$var_version_old" == "$var_version_new" ]]; then
				Write_Script -TEXT " version not modified";
			else
				var_fb_post+="- version undefined - warning\n";
				Write_Script -TEXT " current version undefined";
			fi;
			# Test : url.
			if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- url changed\n";
				Write_Script -TEXT " url changed";
			elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
				var_fb_post+="- url new\n";
				Write_Script -TEXT " url new";
			elif [[ "$var_url_old" == "$var_url_new" ]]; then
				Write_Script -TEXT " url not modified";
			else
				var_fb_post+="- url undefined - warning\n";
				Write_Script -TEXT " current url undefined";
			fi;
			# Test : source.
			if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source from '${var_source_old}' to '${var_source_new}'\n";
				Write_Script -TEXT " source changed";
			elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
				var_fb_post+="- source '${var_source_new}'\n";
				Write_Script -TEXT " source new";
			elif [[ "$var_source_old" == "$var_source_new" ]]; then
				Write_Script -TEXT " source not modified";
			else
				var_fb_post+="- source undefined - warning\n";
				Write_Script -TEXT " current source undefined";
			fi;
			if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
		done < <(echo "$var_content" | yq '.distro[].name');
	else
		return 1;
	fi;
	var_fb_response=$(curl -s -X POST "https://graph.facebook.com/v21.0/${var_fb_id}/feed?access_token=${var_fb_token}" -H "Content-Type: application/json" -d "{\"message\":\"${var_fb_post}\"}");
	Write_Script -TEXT "API response : '${var_fb_response}'";
}

# Publish_LocalReportToJekyll.
#	DESCRIPTION : construct markdown content for Jekyll.
function Publish_LocalReportToJekyll() {
	# Some definition.
	local var_content;
	local var_format;
	local var_output;
	local var_data;
	local var_name;
	local var_fullname;
	declare -a var_fullnames=();
	local var_fullnames_element;
	local var_fullnames_formatted;
	local var_version_old;
	local var_version_new;
	local var_version_test;
	local var_url_old;
	local var_url_new;
	local var_source_old;
	local var_source_new;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CONTENT)
				var_content="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct distribution fullnames array.
	# Tests : format.
	if [[ "$var_format" == "json" ]]; then
		# Loop : Process Substitution fullnames.
		while read -r var_data; do
			var_fullnames+=("$(echo "$var_data" | jq -r -c '.fullname')");
		done < <(echo "$var_content" | jq -r -c '.distro[]');
	elif [[ "$var_format" == "yaml" ]]; then
		# Loop : Process Substitution fullnames.
		while read -r var_name; do
			var_fullnames+=("$(echo "$var_content" | yq eval '.distro[] | select(.name == "'"$var_name"'") | .fullname')");
		done < <(echo "$var_content" | yq '.distro[].name');
	else
		return 1;
	fi;
	# Loop : valuelist comma-separated values.
	for var_fullnames_element in "${var_fullnames[@]}"; do
		var_fullnames_formatted+="${var_fullnames_element}, ";
	done;
	# Set file.
	echo -e "---\ntitle: \"${DATE[DATE0]} (${var_fullnames_formatted%, })\"\n\nlayout: post\ncategories: changes\ndate: ${DATE[DATETIME2]} +0200\n\nauthor: reporter\n---\n" > "$var_output";
	# Tests : format.
	if [[ "$var_format" == "json" ]]; then
		# Loop : Process Substitution distribution.
		while read -r var_data; do
			var_name="$(echo "$var_data" | jq -r -c '.name')";
			var_fullname="$(echo "$var_data" | jq -r -c '.fullname')";
			var_version_old="$(echo "$var_data" | jq -r -c '.version.old')";
			var_version_new="$(echo "$var_data" | jq -r -c '.version.new')";
			var_url_old="$(echo "$var_data" | jq -r -c '.url.old')";
			var_url_new="$(echo "$var_data" | jq -r -c '.url.new')";
			var_source_old="$(echo "$var_data" | jq -r -c '.source.old')";
			var_source_new="$(echo "$var_data" | jq -r -c '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			Publish_LocalReportToJekyllPost -NAME "${var_name}" -FULLNAME "${var_fullname}" -VERSION "${var_version_old}|${var_version_new}" -URL "${var_url_old}|${var_url_new}" -SOURCE "${var_source_old}|${var_source_new}" -OUTPUT "${var_output}";
		done < <(echo "$var_content" | jq -r -c '.distro[]');
	elif [[ "$var_format" == "yaml" ]]; then
		# Loop : Process Substitution distribution.
		while read -r var_name; do
			var_data=$(echo "$var_content" | yq eval ".distro[] | select(.name == \"$var_name\")");
			var_fullname="$(echo "$var_data" | yq eval '.fullname')";
			var_version_old="$(echo "$var_data" | yq eval '.version.old')";
			var_version_new="$(echo "$var_data" | yq eval '.version.new')";
			var_url_old="$(echo "$var_data" | yq eval '.url.old')";
			var_url_new="$(echo "$var_data" | yq eval '.url.new')";
			var_source_old="$(echo "$var_data" | yq eval '.source.old')";
			var_source_new="$(echo "$var_data" | yq eval '.source.new')";
			# Test : reset security about Distro Reporter output.
			var_version_old="${var_version_old:-null}";
			var_version_new="${var_version_new:-null}";
			var_url_old="${var_url_old:-null}";
			var_url_new="${var_url_new:-null}";
			var_source_old="${var_source_old:-null}";
			var_source_new="${var_source_new:-null}";
			Publish_LocalReportToJekyllPost -NAME "${var_name}" -FULLNAME "${var_fullname}" -VERSION "${var_version_old}|${var_version_new}" -URL "${var_url_old}|${var_url_new}" -SOURCE "${var_source_old}|${var_source_new}" -OUTPUT "${var_output}";
		done < <(echo "$var_content" | yq '.distro[].name');
	else
		return 1;
	fi;
}

# Publish_LocalReportToJekyllPost.
#	DESCRIPTION : construct markdown content for Jekyll.
function Publish_LocalReportToJekyllPost() {
	# Some definition.
	local var_name;
	local var_fullname;
	local var_version;
	local var_version_old;
	local var_version_new;
	local var_version_test;
	local var_url;
	local var_url_old;
	local var_url_new;
	local var_source;
	local var_source_old;
	local var_source_new;
	local var_output;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-FULLNAME)
				var_fullname="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct element values.
	var_version_old=$(echo "$var_version" | cut -d '|' -f1);
	var_version_new=$(echo "$var_version" | cut -d '|' -f2);
	var_url_old=$(echo "$var_url" | cut -d '|' -f1);
	var_url_new=$(echo "$var_url" | cut -d '|' -f2);
	var_source_old=$(echo "$var_source" | cut -d '|' -f1);
	var_source_new=$(echo "$var_source" | cut -d '|' -f2);
	# Test : version state.
	if [[ "$(echo -e "$var_version_new\n$var_version_old" | sort -V | head -n 1)" == "$var_version_new" ]]; then
		var_version_test="downgrade";
	else
		var_version_test="upgrade";
	fi;
	# Test : new distribution.
	if [[ "$var_version_old" == "null" && "$var_url_old" == "null" && "$var_source_old" == "null" ]]; then
		echo -e "distribution ${var_fullname} - new :" >> "$var_output";
		Write_Script -TEXT "publication for new distribution '${var_name}' :";
	else
		echo -e "distribution ${var_fullname} - changes :" >> "$var_output";
		Write_Script -TEXT "publication for changed distribution '${var_name}' :";
	fi;
	# Test : version.
	if [[ "$var_version_old" != "null" && "$var_version_new" != "null" ]]; then
		echo -e "- last release ${var_version_test} from '${var_version_old}' to '${var_version_new}'" >> "$var_output";
		Write_Script -TEXT " version changed";
	elif [[ "$var_version_old" == "null" && "$var_version_new" != "null" ]]; then
		echo -e "- version '${var_version_new}'" >> "$var_output";
		Write_Script -TEXT " version new";
	elif [[ "$var_version_old" == "$var_version_new" ]]; then
		Write_Script -TEXT " version not modified";
	else
		echo -e "- version undefined - warning" >> "$var_output";
		Write_Script -TEXT " current version undefined";
	fi;
	# Test : url.
	if [[ "$var_url_old" != "null" && "$var_url_new" != "null" ]]; then
		echo -e "- last url from '${var_url_old}' to '${var_url_new}'" >> "$var_output";
		Write_Script -TEXT " url changed";
	elif [[ "$var_url_old" == "null" && "$var_url_new" != "null" ]]; then
		echo -e "- url '${var_url_new}'" >> "$var_output";
		Write_Script -TEXT " url new";
	elif [[ "$var_url_old" == "$var_url_new" ]]; then
		Write_Script -TEXT " url not modified";
	else
		echo -e "- url undefined - warning" >> "$var_output";
		Write_Script -TEXT " current url undefined";
	fi;
	# Test : source.
	if [[ "$var_source_old" != "null" && "$var_source_new" != "null" ]]; then
		echo -e "- last source from '${var_source_old}' to '${var_source_new}'" >> "$var_output";
		Write_Script -TEXT " source changed";
	elif [[ "$var_source_old" == "null" && "$var_source_new" != "null" ]]; then
		echo -e "- source '${var_source_new}'" >> "$var_output";
		Write_Script -TEXT " source new";
	elif [[ "$var_source_old" == "$var_source_new" ]]; then
		Write_Script -TEXT " source not modified";
	else
		echo -e "- source undefined - warning" >> "$var_output";
		Write_Script -TEXT " current source undefined";
	fi;
	echo "" >> "$var_output";
	if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
}

# Publish_LocalReportToTiktok.
#	DESCRIPTION : construct post content for TikTok.
function Publish_LocalReportToTiktok() {
	# Some definition.
	local var_content;
	local var_format;
	local var_credential;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CONTENT)
				var_content="$2"
				shift 2
				;;
			-FORMAT)
				var_format="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Set post.
	Write_Script -TEXT "API call : ID '${var_bs_id}'";
	echo "";
	# Tests : format.
	if [[ "$var_format" == "json" ]]; then
		# Loop : Process Substitution distribution.
		while read -r var_data; do
			var_name="$(echo "$var_data" | jq -r -c '.name')";
		done < <(echo "$var_content" | jq -r -c '.distro[]');
	elif [[ "$var_format" == "yaml" ]]; then
		# Loop : Process Substitution distribution.
		while read -r var_name; do
			var_data=$(echo "$var_content" | yq eval ".distro[] | select(.name == \"$var_name\")");
		done < <(echo "$var_content" | yq '.distro[].name');
	else
		return 1;
	fi;
}

# Publish_LocalReportToTiktokPost.
#	DESCRIPTION : construct markdown content for Tiktok.
function Publish_LocalReportToTiktokPost() {
	ffmpeg -y -f lavfi -i color=c=black:s=1280x720:d=2 -f lavfi -i color=c=black:s=1280x720:d=2 -f lavfi -i color=c=black:s=1280x720:d=2 -f lavfi -i color=c=black:s=1280x720:d=2 -filter_complex "[0:v]drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf:text='Report GNU/Linux & BSD':fontcolor=white:fontsize=48:x=(w-text_w)/2:y=(h-text_h)/2,fade=t=out:st=1.5:d=0.5[v0];[1:v]drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf:text='Nouvelle distribution Michelle':fontcolor=white:fontsize=48:x=(w-text_w)/2:y=(h-text_h)/2,fade=t=in:st=0:d=0.5,fade=t=out:st=1.5:d=0.5[v1];[2:v]drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf:text='Changement pour la distribution Gertrude':fontcolor=white:fontsize=48:x=(w-text_w)/2:y=(h-text_h)/2,fade=t=in:st=0:d=0.5,fade=t=out:st=1.5:d=0.5[v2];[3:v]drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf:text='/report':fontcolor=white:fontsize=48:x=(w-text_w)/2:y=(h-text_h)/2,fade=t=in:st=0:d=0.5[v3];[v0][v1][v2][v3]concat=n=4:v=1:a=0[outv]" -map "[outv]" output.mp4;
}

# Get_LocalDataLength.
#	DESCRIPTION : local generic function to get length of data.
function Get_LocalDataLength() {
	# Some definition - menu.
	local var_data var_format var_length=0;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-DATA) var_data="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Tests : CSV || YAML || JSON.
	case "$var_format" in
		csv) var_length="$(echo "$var_data" | head -n 2 | tail -n 1)"; echo "${#var_length}" ;;
		yaml) echo "$var_data" | yq '.distro | length' 2>/dev/null ;;
		json) echo "$var_data" | jq -r '.distro | length' 2>/dev/null ;;
		*) return 1 ;;
	esac
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_format;
	local var_content;
	local var_length;
	# Init.
	Write_Script -TEXT "report generation from '${PARAM[SOURCE]}'.";
	if [[ "${PARAM[DESTINATION]}" == "jekyll" && -n "$var_param_output_file" ]]; then
		Write_Script -TEXT "report '${var_param_output_file}' already exists for this day.";
	fi;
	if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	var_content="$(<"${PARAM[SOURCE]}")";
	# Tests : JSON || YAML.
	var_format=$(Get_DataFormat -DATA "$var_content");
	if [[ "$var_format" =~ ^(csv|yaml|json)$ ]]; then
		var_length=$(Get_LocalDataLength -DATA "$var_content" -FORMAT "$var_format");
	else
		Stop_App -CODE 1 -TEXT "Invalid format for '--source' option.";
	fi;
	# Test : content exists with number control avoid null count.
	if [[ "$var_length" =~ ^[0-9]+$ ]] && [[ "$var_length" -gt 0 ]]; then
		# Tests : website Bluesky, website Facebook, software Jekyll.
		if [[ "${PARAM[DESTINATION]}" == "bluesky" ]]; then
			if Publish_LocalReportToBluesky -CONTENT "$var_content" -FORMAT "$var_format" -CREDENTIAL "${PARAM[CREDENTIAL]}"; then
				if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
				Write_Script -TEXT "report publication generated.";
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
				Stop_App -CODE 1 -TEXT "report publication failed.";
			fi;
		elif [[ "${PARAM[DESTINATION]}" == "facebook" ]]; then
			if Publish_LocalReportToFacebook -CONTENT "$var_content" -FORMAT "$var_format" -CREDENTIAL "${PARAM[CREDENTIAL]}"; then
				if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
				Write_Script -TEXT "report publication generated.";
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
				Stop_App -CODE 1 -TEXT "report publication failed.";
			fi;
		elif [[ "${PARAM[DESTINATION]}" == "jekyll" ]]; then
			if Publish_LocalReportToJekyll -CONTENT "$var_content" -FORMAT "$var_format" -OUTPUT "${PARAM[OUTPUT]}"; then
				Write_Script -TEXT "report publication generated.";
			else
				Stop_App -CODE 1 -TEXT "report publication failed.";
			fi;
		elif [[ "${PARAM[DESTINATION]}" == "tiktok" ]]; then
			if Publish_LocalReportToTiktok -CONTENT "$var_content" -FORMAT "$var_format" -OUTPUT "${PARAM[OUTPUT]}"; then
				Write_Script -TEXT "report publication generated.";
			else
				Stop_App -CODE 1 -TEXT "report publication failed.";
			fi;
		fi;
	else
		# Test : data exists.
		if rm "${PARAM[OUTPUT]}" 2>/dev/null; then
			Write_Script -TEXT "report removed because can't be generated from null data.";
		else
			Write_Script -TEXT "report can't be generated from null data and can't be removed.";
		fi;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;