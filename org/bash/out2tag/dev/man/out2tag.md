<blockquote id="out2tag">



NAME



	out2tag - Redirect command output to a tag in a file.





REQUIREMENTS



	x



 

SYNOPSIS



	out2tag OPTIONS





DESCRIPTION



	This open-source tool modifies the contents of one file by adding the contents of another at a specific location.





OPTIONS



	-c COMMAND,

	--command COMMAND

 		Program command, in quotes.

 		- state : mandatory.

 		- accepted value(s) : program command.

 		- default value : no default.



 	-t CODE,

	--tag CODE

 		Single HTML tag with single ID of the HTML tag, in colon separated list.

 		- state : optionnal.

 		- accepted value(s) : choose 'tag:id'.

 		- default value : 'blockquote:out2tag'.



 	-o PATH,

	--output PATH

 		Single local folder path or single file path (file name default is 'README.md' if folder path provided) to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD/README.md'.



 	-s (no type),

	--set (no type)

 		Create output file with tag if doesn't exists.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	this program output to README.md :

		out2tag -c "./out2tag --help" -t blockquote -i out2tag -o README.md -QUIET



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	1.1.0 - 2024-12-01

		Add '--set' option.



 	1.0.0 - 2024-09-01

		New documentation. Lowercase all options.



 	0.2.0 - 2024-06-01

		Get line position content instead tag with only id.



 	0.1.0 - 2024-06-01

		Add '--tag' option, new text function added.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.1.0

	License : GPL-3.0-or-later





</blockquote>
