#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               out2tag.sh               | #
# |________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="out2tag";
APP["SECTION"]="1";
APP["SHORTNAME"]="02T";
APP["SLOGAN"]="Redirect command output to a tag in a file.";
APP["DESCRIPTION"]="This open-source tool modifies the contents of one file by adding the contents of another at a specific location.";
APP["REQUIREMENTS"]="null";
APP["EXAMPLES"]="this program output to README.md :|${APP[NAME]} -c \"./${APP[NAME]} --help\" -t blockquote -i out2tag -o README.md -QUIET";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.1.0|2024-12-01T10:00:00+0200|Add '--set' option.#1.0.0|2024-09-01T10:00:00+0200|New documentation. Lowercase all options.#0.2.0|2024-06-01T10:00:00+0200|Get line position content instead tag with only id.#0.1.0|2024-06-01T10:00:00+0200|Add '--tag' option, new text function added.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.1.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["COMMAND"]="1#c#COMMAND#mandatory#Program command, in quotes.|program command.|no default.";
OPTIONS["TAG"]="2#t#CODE#optionnal#Single HTML tag with single ID of the HTML tag, in colon separated list.|choose 'tag:id'.|'blockquote:out2tag'.";
OPTIONS["OUTPUT"]="3#o#PATH#optionnal#Single local folder path or single file path (file name default is 'README.md' if folder path provided) to store the final result.|path.|'PWD/README.md'.";
OPTIONS["SET"]="4#s#null#optionnal#Create output file with tag if doesn't exists.|null|'false' because disabled.";
OPTIONS["QUIET"]="5#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="6#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="7#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--command|-c)
			PARAM["COMMAND"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--tag|-t)
			PARAM["TAGID"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--set|-s)
			PARAM["SET"]=true
			shift
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["TAGID"]="blockquote:out2tag";
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}README.md";
DEFAULT["SET"]=false;
DEFAULT["QUIET"]=false;
# Some script definition.
# TAGID.
if [ -z "${PARAM[TAGID]}" ]; then
	PARAM["TAG"]="$(echo "${DEFAULT[TAGID]}" | cut -d ':' -f1)";
	PARAM["ID"]="$(echo "${DEFAULT[TAGID]}" | cut -d ':' -f2)";
elif ! [[ "${PARAM[TAGID]}" =~ : ]]; then
	Stop_App -CODE 1 -TEXT "Tag need to be like 'tag:id'.";
else
	PARAM["TAG"]="$(echo "${PARAM[TAGID]}" | cut -d ':' -f1)";
	PARAM["ID"]="$(echo "${PARAM[TAGID]}" | cut -d ':' -f2)";
fi;
# OUTPUT.
if [ -z "${PARAM[OUTPUT]}" ]; then PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}"; else PARAM["OUTPUT"]="${PARAM[OUTPUT]}"; fi;
# SET.
if [ -z "${PARAM[SET]}" ]; then PARAM["SET"]="${DEFAULT[SET]}"; fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Set_Out2Tag.
#	DESCRIPTION : determine where inject command output.
function Set_Out2Tag() {
	# Some definition.
	local var_command;
	local var_tag;
	local var_id;
	local var_output;
	local var_file_line_position;
	local var_file_line_content;
	local var_file_output;
	local var_file_slim;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-COMMAND)
				var_command="$2"
				shift 2
				;;
			-TAG)
				var_tag="$2"
				shift 2
				;;
			-ID)
				var_id="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Define insert position (minus 1 needed for 'sed r' inject method).
	var_file_line_position=$(grep -n "<${var_tag}[^>]*id=\"${var_id}\"[^>]*>" "${var_output}" | cut -d ':' -f 1);
	# Re-define insert position (minus 1 needed for 'sed r' inject method).
	Write_Script -TEXT "content line position is '${var_file_line_position}'.";
	var_file_line_content=$(sed "${var_file_line_position}q;d" "${var_output}");
	var_file_line_position=$(("$var_file_line_position" - 1));
	# Set double output solution.
	var_file_output="${var_output}.output.tmp";
	var_file_slim="${var_output}.slim.tmp";
	# Construct command output.
	echo -e "${var_file_line_content}" > "$var_file_output";
	${var_command} >> "$var_file_output" 2>&1;
	echo -e "</${var_tag}>" >> "$var_file_output";
	echo -e "$(sed G "$var_file_output")" > "$var_file_output";
	# Remove old content & send to slim file.
	sed -e "/${var_file_line_content}/,/<\/${var_tag}>/d" "$var_output" > "$var_file_slim";
	# Test : first line position.
	if [[ "$var_file_line_position" == 0 ]]; then
		# Crush and append original.
		cat "$var_file_output" > "$var_output";
		cat "$var_file_slim" >> "$var_output";
	else
		# Inject command output into slim file & just crush original.
		sed "${var_file_line_position}r ${var_file_output}" "$var_file_slim" > "$var_output";
	fi;
	rm "$var_file_output";
	rm "$var_file_slim";
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_file_line_position;
	# Test : command.
	if ${PARAM[COMMAND]} &> /dev/null; then
		Write_Script -TEXT "check command '${PARAM[COMMAND]}'.";
		# Define insert position (minus 1 needed for 'sed r' inject method).
		var_file_line_position=$(grep -n "<${PARAM[TAG]}[^>]*id=\"${PARAM[ID]}\"[^>]*>" "${PARAM[OUTPUT]}" | cut -d ':' -f 1);
		# Test : tag.
		if [[ "$var_file_line_position" -ge 1 ]]; then
			# Test : tag filling.
			if Set_Out2Tag -COMMAND "${PARAM[COMMAND]}" -TAG "${PARAM[TAG]}" -ID "${PARAM[ID]}" -OUTPUT "${PARAM[OUTPUT]}"; then
				Write_Script -TEXT "'${PARAM[COMMAND]}' command checked.";
			else
				Stop_App -CODE 1 -TEXT "Output for command '${PARAM[COMMAND]}' can't be added.";
			fi;
		else
			# Test : new file.
			if [[ "${PARAM[SET]}" == true ]]; then
				if Set_File -PATH "${PARAM[OUTPUT]}" -RAW "<blockquote id=\"out2tag\">\n\n</blockquote>" -INTERPRET; then
					Write_Script -TEXT "Tag '${PARAM[TAG]}' not finded but file setted.";
					Write_Script -TEXT "check command '${PARAM[COMMAND]}'.";
					# Test : tag filling.
					if Set_Out2Tag -COMMAND "${PARAM[COMMAND]}" -TAG "${PARAM[TAG]}" -ID "${PARAM[ID]}" -OUTPUT "${PARAM[OUTPUT]}"; then
						Write_Script -TEXT "'${PARAM[COMMAND]}' command checked.";
					else
						Stop_App -CODE 1 -TEXT "Output for command '${PARAM[COMMAND]}' can't be added and file not setted.";
					fi;
				else
					Stop_App -CODE 1 -TEXT "Tag '${PARAM[TAG]}' not finded and file not setted.";
				fi;
			else
				Stop_App -CODE 1 -TEXT "Tag '${PARAM[TAG]}' not finded and file set disabled.";
			fi;
		fi;
	else
		Stop_App -CODE 1 -TEXT "Command '${PARAM[COMMAND]}' can't be performed.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;