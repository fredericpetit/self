<blockquote id="out2tag">



NAME



	lab-network - Create a nice Lab.





REQUIREMENTS



	program 'netplan' (mandatory)



 	program 'virsh' (mandatory)



 

SYNOPSIS



	lab-network OPTIONS





DESCRIPTION



	This open source tool creates virtual cards with Netplan & libvirt API.





OPTIONS



	-o FOLDERPATH,

	--output FOLDERPATH

 		Single local folder path to store configuration.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value :  no default.



 	-s NAME,

	--software NAME

 		Product name.

 		- state : mandatory.

 		- accepted value(s) : 'netplan' or 'libvirt'

 		- default value : no default.



 	-n NAMES,

	--network NAMES

 		Network names, delimited by ':'.

 		- state : optionnal.

 		- accepted value(s) : string.

 		- default value :  no default.



 	-b (no type),

	--backup (no type)

 		Backup security.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	create all cards for Init Lab :

		lab-network -q



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	0.1.0 - 2024-12-01

		Refactor.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 0.1.0

	License : GPL-3.0-or-later





</blockquote>
