#!/bin/bash
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               labo-network.sh               | #
# |_____________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="lab-network";
APP["SECTION"]="1";
APP["SHORTNAME"]="LABNET";
APP["SLOGAN"]="Create a nice Lab.";
APP["DESCRIPTION"]="This open source tool creates virtual cards with Netplan & libvirt API.";
APP["REQUIREMENTS"]="program netplan mandatory#program virsh mandatory";
APP["EXAMPLES"]="create all cards for Init Lab :|${APP[NAME]} -q";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="0.1.0|2024-12-01T06:00:00+0200|Refactor.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="0.1.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["OUTPUT"]="1#o#FOLDERPATH#optionnal#Single local folder path to store configuration.|path.| no default.";
OPTIONS["SOFTWARE"]="2#s#NAME#mandatory#Product name.|'netplan' or 'libvirt'|no default.";
OPTIONS["NETWORK"]="3#n#NAMES#optionnal#Network names, delimited by ':'.|string.| no default.";
OPTIONS["BACKUP"]="4#b#null#optionnal#Backup security.|null|'false' because disabled.";
OPTIONS["QUIET"]="5#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="6#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="7#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--software|-s)
			PARAM["SOFTWARE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--network|-n)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--backup|-b)
			PARAM["BACKUP"]=true
			shift
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some default.
declare -gA DEFAULT;
DEFAULT["BACKUP"]=false;
DEFAULT["QUIET"]=false;
# Some script definition.
# OUTPUT.
if Test_File -PATH "${PARAM[OUTPUT]}"; then Stop_App -CODE 1 -TEXT "Path for '--output' option can't be a file path."; fi;
# SOFTWARE.
if ! [[ "${PARAM[SOFTWARE]}" =~ ^(netplan|libvirt)$ ]]; then Stop_App -CODE 1 -TEXT "Invalid name for '--software' option."; fi;
# BACKUP.
if [ -z "${PARAM[BACKUP]}" ]; then PARAM["BACKUP"]="${DEFAULT[BACKUP]}"; fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #


# Set_LocalConfNetplan.
#	DESCRIPTION : .

# Set_LocalConfLibvirt.
#	DESCRIPTION : .

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Tests : netplan YAML, libvirt XML.
	if [[ "${PARAM[DESTINATION]}" == "netplan" ]]; then
		Write_Script -TEXT "Netplan configuration from '${PARAM[SOURCE]}'.";
		if Set_LocalConfNetplan; then
			echo "placeholder.";
		else
			echo "placeholder.";
		fi;
	elif [[ "${PARAM[DESTINATION]}" == "libvirt" ]]; then
		Write_Script -TEXT "Libvirt configuration from '${PARAM[SOURCE]}'.";
		if Set_LocalConfLibvirt; then
			echo "placeholder.";
		else
			echo "placeholder.";
		fi;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;




# no root, just user
#if [[ $USER == "root" ]]; then
#	VIRT_USER="$(id -nu 1000)";
#else
#	VIRT_USER="$USER";
#fi

# path
#VIRT_PATH="/home/$VIRT_USER/Documents/virt";
#VIRT_PATH="/virtualisation";
#if [ ! -d ${VIRT_PATH} ]; then mkdir $VIRT_PATH; fi;
#if [ ! -d ${VIRT_PATH}/bank ]; then mkdir ${VIRT_PATH}/bank; fi;
#if [ ! -d ${VIRT_PATH}/img ]; then mkdir ${VIRT_PATH}/img; fi;
#if [ ! -d ${VIRT_PATH}/network ]; then mkdir ${VIRT_PATH}/network; fi;

# rights
#chmod 755 -R ${VIRT_PATH};
#chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};

# network interface
#VIRT_NETWORK_INTERFACE=$(ip addr show | grep '2: ' | cut -d: -f2 | sed 's/ //g');
#VIRT_NETWORK_FILE="/etc/netplan/01-network-manager-all.yaml";

# netplan
#cat <<EOF > $VIRT_NETWORK_FILE
#network:
#version: 2
#renderer: networkd

#ethernets:
#	${VIRT_NETWORK_INTERFACE}:
#		dhcp4: no
#		dhcp6: no
#		mtu: 1500

#bridges:
#	br0:
#		dhcp4: no
#		dhcp6: no
#		addresses: [192.168.0.10/24]
#		interfaces:
#			- ${VIRT_NETWORK_INTERFACE}
#		routes:
#		- to: 0.0.0.0/0
#			via: 192.168.0.254
#			metric: 100
#			on-link: true
#		mtu: 1500
#		nameservers:
#			addresses: [192.168.0.101]
#		parameters:
#			stp: true
#			forward-delay: 4
#EOF
#netplan apply;

# rights
#chmod 755 -R ${VIRT_PATH};
#chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};








# no root, just user
#if [[ $USER == "root" ]]; then
#	VIRT_USER="$(id -nu 1000)";
#else
#	VIRT_USER="$USER";
#fi

# path
#VIRT_PATH="/home/$VIRT_USER/Documents/virt";
#VIRT_PATH="/virtualisation";
#if [ ! -d ${VIRT_PATH} ]; then mkdir $VIRT_PATH; fi;
#if [ ! -d ${VIRT_PATH}/bank ]; then mkdir ${VIRT_PATH}/bank; fi;
#if [ ! -d ${VIRT_PATH}/img ]; then mkdir ${VIRT_PATH}/img; fi;
#if [ ! -d ${VIRT_PATH}/network ]; then mkdir ${VIRT_PATH}/network; fi;

# rights
#chmod 755 -R ${VIRT_PATH};
#chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};

# br0
#virsh net-undefine br0;
#virsh net-destroy br0;
#cat <<< '<network>
#	<name>br0</name>
#	<forward mode="bridge"/>
#	<bridge name="br0"/>
#</network>' > ${VIRT_PATH}/network/br0.xml;
#virsh net-define --file ${VIRT_PATH}/network/br0.xml;
#virsh net-start br0 && virsh net-autostart br0;

# lan0
#virsh net-undefine lan0;
#virsh net-destroy lan0;
#cat <<< '<network>
#	<name>lan0</name>
#	<bridge name="lan0" stp="on" delay="0"/>
#	<domain name="lan0"/>
#</network>' > ${VIRT_PATH}/network/lan0.xml;
#virsh net-define --file ${VIRT_PATH}/network/lan0.xml;
#virsh net-start lan0 && virsh net-autostart lan0;

# lan1
#virsh net-undefine lan1;
#virsh net-destroy lan1;
#cat <<< '<network>
#	<name>lan1</name>
#	<bridge name="lan1" stp="on" delay="0"/>
#	<domain name="lan1"/>
#</network>' > ${VIRT_PATH}/network/lan1.xml;
#virsh net-define --file ${VIRT_PATH}/network/lan1.xml
#virsh net-start lan1 && virsh net-autostart lan1;

# dmz0
#virsh net-undefine dmz0;
#virsh net-destroy dmz0;
#cat <<< '<network>
#	<name>dmz0</name>
#	<bridge name="dmz0" stp="on" delay="0"/>
#	<domain name="dmz0"/>
#</network>' > ${VIRT_PATH}/network/dmz0.xml;
#virsh net-define --file ${VIRT_PATH}/network/dmz0.xml
#virsh net-start dmz0 && virsh net-autostart dmz0;

# dmz1
#virsh net-undefine dmz1;
#virsh net-destroy dmz1;
#cat <<< '<network>
#	<name>dmz1</name>
#	<bridge name="dmz1" stp="on" delay="0"/>
#	<domain name="dmz1"/>
#</network>' > ${VIRT_PATH}/network/dmz1.xml;
#virsh net-define --file ${VIRT_PATH}/network/dmz1.xml
#virsh net-start dmz1 && virsh net-autostart dmz1;

# list
#virsh net-list --all;

# rights
#chmod 755 -R ${VIRT_PATH};
#chown $VIRT_USER:$VIRT_USER -R ${VIRT_PATH};