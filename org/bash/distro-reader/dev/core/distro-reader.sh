#!/bin/bash
# shellcheck source=/dev/null
#  ______________________________________________
# |                                              | #
# |               distro-reader.sh               | #
# |______________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-reader";
APP["SECTION"]="1";
APP["SHORTNAME"]="DRead";
APP["SLOGAN"]="Read Distro Tracker & Distro Reporter datas.";
APP["DESCRIPTION"]="This open-source tool generates raw output.";
APP["REQUIREMENTS"]="program jq optionnal#program yq optionnal";
APP["EXAMPLES"]="null";
APP["TODO"]="null";
APP["NOTES"]="Compatible Distro Tracker >= 1.2.0.";
APP["CREDITS"]="null";
APP["CHANGELOG"]="0.0.1|2025-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="0.0.1";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#optionnal#Single local file path or single local folder path to Distro Tracker & Distro Reporter datas.|path.|try first 'PWD/distro-{tracker,reporter}.{json,yaml}', and after 'PWD/{DT-DR}-????????_??????.{json,yaml}'.";
OPTIONS["TOOL"]="2#t#NAME#optionnal#Tool from which to read data.|'tracker' or 'reporter'.|no default.";
OPTIONS["CONTENT"]="3#c#NAME#optionnal#Which content to read from the tool.|'main' or 'revision'.|'main'.";
OPTIONS["QUIET"]="4#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'true'.";
OPTIONS["HELP"]="5#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="6#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--tool|-t)
			PARAM["TOOL"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--content|-c)
			PARAM["CONTENT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["EXTENSIONS"]="csv|yaml|json";
DEFAULT["SOURCE"]="${PWD%[\\/]}";
DEFAULT["CONTENT"]="main";
DEFAULT["QUIET"]=true;
# Some script definition.
# CONTENT.
if [[ -z "${PARAM[CONTENT]}" ]]; then PARAM["CONTENT"]="${DEFAULT[CONTENT]}"; fi;
# TOOL.
if [[ ! "${PARAM[TOOL]}" =~ ^(tracker|reporter)$ ]]; then Stop_App -CODE 1 -TEXT "Need valid value for '--tool' option."; fi;
# Test : tracker || reporter.
if [[ "${PARAM[TOOL]}" == "tracker" ]]; then
	# SOURCE - If it's a folder, input format will be known only after auto-discovery.
	if [[ -z "${PARAM[SOURCE]}" ]]; then PARAM["SOURCE"]="${DEFAULT[SOURCE]}"; fi;
	# Tests : custom || distro-tracker.json || distro-tracker.{json,yaml} || DT-????????_??????.{json,yaml} .
	if ! Test_File -PATH "${PARAM[SOURCE]}"; then
		# Tests : name mode, date mode - Use pipe in '--EXTENSION' because regex treatment.
		var_param_source_file=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-tracker" -MODE "name");
		if [[ -n "$var_param_source_file" ]]; then
			PARAM["SOURCE"]="$var_param_source_file";
		else
			var_param_source_file=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-tracker" -MODE "date");
			if [[ -n "$var_param_source_file" ]]; then
				PARAM["SOURCE"]="$var_param_source_file";
			else
				Stop_App -CODE 1 -TEXT "Need valid folder path value for '--source' option with Distro Tracker.";
			fi;
		fi;
	fi;
elif [[ "${PARAM[TOOL]}" == "reporter" ]]; then
	# SOURCE - If it's a folder, input format will be known only after auto-discovery.
	if [[ -z "${PARAM[SOURCE]}" ]]; then PARAM["SOURCE"]="${DEFAULT[SOURCE]}"; fi;
	# Tests : custom || distro-reporter.json || distro-reporter.{json,yaml} || DR-????????_??????.{json,yaml} .
	if ! Test_File -PATH "${PARAM[SOURCE]}"; then
		# Tests : name mode, date mode - Use pipe in '--EXTENSION' because regex treatment.
		var_param_source_file=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-reporter" -MODE "name");
		if [[ -n "$var_param_source_file" ]]; then
			PARAM["SOURCE"]="$var_param_source_file";
		else
			var_param_source_file=$(Search_File -PATH "${PARAM[SOURCE]}" -EXTENSION "${DEFAULT[EXTENSIONS]}" -TOOL "distro-reporter" -MODE "date");
			if [[ -n "$var_param_source_file" ]]; then
				PARAM["SOURCE"]="$var_param_source_file";
			else
				Stop_App -CODE 1 -TEXT "Need valid folder path value for '--source' option with Distro Reporter.";
			fi;
		fi;
	fi;
fi;
# Test : file.
if ! Test_File -PATH "${PARAM[SOURCE]}"; then
	Stop_App -CODE 1 -TEXT "Need valid file for '--source' option.";
fi;
# Redefine requirements for user-friendly work.
var_param_source="$(<"${PARAM[SOURCE]}")";
var_param_format=$(Get_DataFormat -DATA "${var_param_source:0:50}");
if [[ "$var_param_format" == "json" ]]; then
	APP["REQUIREMENTS"]="program jq mandatory";
elif [[ "$var_param_format" == "yaml" ]]; then
	APP["REQUIREMENTS"]="program yq mandatory";
fi;
# QUIET.
if [[ -z "${PARAM[QUIET]}" ]]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Get_LocalDistroTracker.
#	DESCRIPTION : get distro-tracker data.
function Get_LocalDistroTracker() {
	# Some definition - menu.
	local var_path;
	local var_content;
	local var_format;
	# Some definition - next.
	local var_data;
	local var_list;
	local var_index;
	local var_data_distro;
	local var_data_distro_fullname;
	local var_data_distro_release;
	local var_data_distro_release_date;
	local var_data_distro_revision;
	local var_data_distro_yaml;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			-CONTENT) var_content="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Data.
	var_data="$(<"$var_path")";
	var_list=();
	# Tests : CSV || JSON || YAML.
	if [[ "$var_format" == "csv" ]]; then
		# Test : main || revision.
		if [[ "$var_content" == "main" ]]; then
			# Loop : Process Substitution.
			while IFS=',' read -r _ var_data_distro_fullname var_data_distro_release var_data_distro_release_date _; do
				var_list+=("${var_data_distro_fullname} - ${var_data_distro_release} (${var_data_distro_release_date})");
			done < <(echo "$var_data" | tail -n +2);
			var_index=$(( RANDOM % ${#var_list[@]} ));
			# Test : lines.
			if [[ ${#var_list[@]} -gt 0 ]]; then
				echo "${var_list[$var_index]}";
			else
				echo "N/A";
			fi;
		elif [[ "$var_content" == "revision" ]]; then
			echo "N/A";
		else
			echo "N/A";
		fi;
	elif [[ "$var_format" == "json" ]]; then
		# Test : main || revision.
		if [[ "$var_content" == "main" ]]; then
			# Loop : Process Substitution.
			while read -r var_data_distro; do
				var_data_distro_fullname=$(echo "$var_data_distro" | jq -c -r '.fullname');
				var_data_distro_release=$(echo "$var_data_distro" | jq -c -r '.releases.latest.version');
				var_data_distro_release_date=$(echo "$var_data_distro" | jq -c -r '.releases.latest.date');
				var_list+=("${var_data_distro_fullname} - ${var_data_distro_release} (${var_data_distro_release_date})");
			done < <(echo "$var_data" | jq -c -r '.distro[]');
			var_index=$(( RANDOM % ${#var_list[@]} ));
			# Test : lines.
			if [[ ${#var_list[@]} -gt 0 ]]; then
				echo "${var_list[$var_index]}";
			else
				echo "N/A";
			fi;
		elif [[ "$var_content" == "revision" ]]; then
			var_data_distro_revision=$(echo "$var_data" | jq -c -r '.revision');
			echo "$var_data_distro_revision";
		else
			echo "N/A";
		fi;
	elif [[ "$var_format" == "yaml" ]]; then
		# Test : main || revision.
		if [[ "$var_content" == "main" ]]; then
			# Loop : Process Substitution distribution.
			while read -r var_data_distro; do
				var_data_distro_yaml=$(echo "$var_data" | yq eval ".distro[] | select(.name == \"$var_data_distro\")");
				var_data_distro_fullname=$(echo "$var_data_distro_yaml" | yq eval '.fullname');
				var_data_distro_release=$(echo "$var_data_distro_yaml" | yq eval '.releases.latest.version');
				var_data_distro_release_date=$(echo "$var_data_distro_yaml" | yq eval '.releases.latest.date');
				var_list+=("${var_data_distro_fullname} - ${var_data_distro_release} (${var_data_distro_release_date})");
			done < <(echo "$var_data" | yq '.distro[].name');
			var_index=$(( RANDOM % ${#var_list[@]} ));
			# Test : lines.
			if [[ ${#var_list[@]} -gt 0 ]]; then
				echo "${var_list[$var_index]}";
			else
				echo "N/A";
			fi;
		elif [[ "$var_content" == "revision" ]]; then
			var_data_distro_revision=$(echo "$var_data" | yq eval '.revision');
			echo "$var_data_distro_revision";
		else
			echo "N/A";
		fi;
	else
		echo "N/A";
	fi;
}

# Get_LocalDistroReporter.
#	DESCRIPTION : get distro-reporter data.
function Get_LocalDistroReporter() {
	# Some definition -menu.
	local var_path;
	local var_content;
	local var_format;
	# Some definition -next.
	local var_data;
	local var_list;
	local var_data_distro;
	local var_data_distro_fullname;
	local var_data_distro_release;
	local var_data_distro_source;
	local var_data_distro_revision;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			-CONTENT) var_content="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Data.
	var_data="$(<"$var_path")";
	var_list="";
	# Tests : JSON || YAML.
	if [[ "$var_format" == "json" ]]; then
		# Test : main || revision.
		if [[ "$var_content" == "main" ]]; then
			# Loop : Process Substitution distribution.
			while read -r var_data_distro; do
				var_data_distro_fullname=$(echo "$var_data_distro" | jq -c -r '.fullname');
				var_data_distro_release=$(echo "$var_data_distro" | jq -c -r '.version.new');
				var_data_distro_source=$(echo "$var_data_distro" | jq -c -r '.source.new');
				# Tests : release || source || null.
				if [[ "$var_data_distro_release" != "null" ]]; then
					var_list+="${var_data_distro_fullname} (${var_data_distro_release}), ";
				elif [[ "$var_data_distro_source" != "null" ]]; then
					var_list+="${var_data_distro_fullname} (src now '${var_data_distro_source}'), ";
				else
					var_list+="${var_data_distro_fullname} (N/A state), ";
				fi;
			done < <(echo "$var_data" | jq -c -r '.distro[]');
			# Test : line.
			if [[ -n "$var_list" ]]; then
				var_list="${var_list%, }";
				echo "$var_list";
			else
				echo "N/A";
			fi;
		elif [[ "$var_content" == "revision" ]]; then
			var_data_distro_revision=$(echo "$var_data" | jq -c -r '.revision');
			echo "$var_data_distro_revision";
		else
			echo "N/A";
		fi;
	elif [[ "$var_format" == "yaml" ]]; then
		# Test : main || revision.
		if [[ "$var_content" == "main" ]]; then
			# Loop : Process Substitution distribution.
			while read -r var_data_distro; do
				var_data_distro_yaml=$(echo "$var_data" | yq eval ".distro[] | select(.name == \"$var_data_distro\")");
				var_data_distro_fullname=$(echo "$var_data_distro_yaml" | yq eval '.fullname');
				var_data_distro_release=$(echo "$var_data_distro_yaml" | yq eval '.version.new');
				var_data_distro_source=$(echo "$var_data_distro_yaml" | yq eval '.source.new');
				# Tests : release || source || null.
				if [[ "$var_data_distro_release" != "null" ]]; then
					var_list+="${var_data_distro_fullname} (${var_data_distro_release}), ";
				elif [[ "$var_data_distro_source" != "null" ]]; then
					var_list+="${var_data_distro_fullname} (src now '${var_data_distro_source}'), ";
				else
					var_list+="${var_data_distro_fullname} (N/A state), ";
				fi;
			done < <(echo "$var_data" | yq '.distro[].name');
			# Test : line.
			if [[ -n "$var_list" ]]; then
				var_list="${var_list%, }";
				echo "$var_list";
			else
				echo "N/A";
			fi;
		elif [[ "$var_content" == "revision" ]]; then
			var_data_distro_revision=$(echo "$var_data" | yq eval '.revision');
			echo "$var_data_distro_revision";
		else
			echo "N/A";
		fi;
	else
		echo "N/A";
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_content;
	local var_format;
	# Some init.
	var_content="$(<"${PARAM[SOURCE]}")";
	var_format=$(Get_DataFormat -DATA "${var_content:0:50}");
	# DEBUG.
	if [[ "${CONF[DEBUG]}" = true ]]; then echo "${PARAM[SOURCE]}"; fi;
	# Tests : tracker || reporter.
	if [[ "${PARAM[TOOL]}" == "tracker" ]]; then
		# Test : main || revision.
		if [[ "${PARAM[CONTENT]}" =~ ^(main|revision)$ ]]; then
			Get_LocalDistroTracker -PATH "${PARAM[SOURCE]}" -CONTENT "${PARAM[CONTENT]}" -FORMAT "$var_format";
		fi;
	elif [[ "${PARAM[TOOL]}" == "reporter" ]]; then
		# Test : main || revision.
		if [[ "${PARAM[CONTENT]}" =~ ^(main|revision)$ ]]; then
			Get_LocalDistroReporter -PATH "${PARAM[SOURCE]}" -CONTENT "${PARAM[CONTENT]}" -FORMAT "$var_format";
		fi;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;