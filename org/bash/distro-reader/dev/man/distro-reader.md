<blockquote id="out2tag">



NAME



	distro-reader - Read Distro Tracker & Distro Reporter datas.





REQUIREMENTS



	program 'jq' (optionnal)



 	program 'yq' (optionnal)



 

SYNOPSIS



	distro-reader [ OPTIONS ]





DESCRIPTION



	This open-source tool generates raw output.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path or single local folder path to Distro Tracker & Distro Reporter datas.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : try first 'PWD/distro-{tracker,reporter}.{json,yaml}', and after 'PWD/{DT-DR}-????????_??????.{json,yaml}'.



 	-t NAME,

	--tool NAME

 		Tool from which to read data.

 		- state : optionnal.

 		- accepted value(s) : 'tracker' or 'reporter'.

 		- default value : no default.



 	-c NAME,

	--content NAME

 		Which content to read from the tool.

 		- state : optionnal.

 		- accepted value(s) : 'main' or 'revision'.

 		- default value : 'main'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'true'.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	null

		null



 

TO-DO



	x





NOTES



	Compatible Distro Tracker >= 1.2.0.



 

CREDITS & THANKS



	x





CHANGELOG



	0.0.1 - 2025-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 0.0.1

	License : GPL-3.0-or-later





</blockquote>
