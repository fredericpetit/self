#!/bin/bash
# first content joined - 2025-01-06.
# first content joined - 2025-01-06.
# shellcheck source=/dev/null
#  _________________________________________
# |                                         | #
# |               func_app.sh               | #
# |_________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : pre-defined work for all APPs.


# CONF - pursuit : safe storage of original $IFS.
if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["IFS"]="$IFS";


# Restore_IFS.
#	DESCRIPTION : restore IFS.
function Restore_IFS() { IFS="${CONF["IFS"]}"; }


# COLOR.
declare -gA COLOR;
COLOR[BOLD]='\e[1m';
COLOR[UNDERLINE]='\e[4m';
COLOR[RED_LIGHT]='\e[1;38;5;160m';
COLOR[GREEN_LIGHT]='\e[1;38;5;34m';
COLOR[GREEN_DARK]='\e[1;38;5;28m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;179m';
COLOR[YELLOW_DARK]='\e[1;38;5;178m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;208m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';
COLOR[STOP]='\e[0m';
COLOR[TICK]="[${COLOR[GREEN_LIGHT]}✔${COLOR[STOP]}]";
COLOR[CROSS]="[${COLOR[RED_LIGHT]}✘${COLOR[STOP]}]";


# Write_Script.
#	DESCRIPTION : custom output of the APP.
#	RETURN : string.
function Write_Script() {
	# Some definition.
	local var_text;
	local var_code;
	local var_check;
	local var_checked;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-CODE)
				var_code="$2"
				shift 2
				;;
			-CHECK)
				var_check=true
				shift
				;;
			-CHECKED)
				var_checked=true
				shift
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	if [[ "${PARAM[QUIET]}" != true ]]; then 
		# Tests : state, change former line.
		if [ -n "$var_check" ]; then
			echo -ne "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		elif [ -n "$var_checked" ]; then
			if [[ "$var_code" -eq 1 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[CROSS]} ERROR : $var_text";
			elif [[ "$var_code" -eq 0 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[TICK]} $var_text";
			fi;
		else
			echo -e "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		fi;
	fi;
}


# Get_Help.
#	DESCRIPTION : usage of the APP, in the APP.
#	RETURN : string.
function Get_Help() {
	# requirements.
	local var_requirements;
	local var_requirement;
	local var_requirement_type;
	local var_requirement_name;
	local var_requirement_state;
	local var_requirements_text;
	# Test : array exists.
	if [[ -n "${APP[REQUIREMENTS]}" && "${APP[REQUIREMENTS]}" != "null" ]]; then
		IFS='#' read -ra var_requirements <<< "${APP[REQUIREMENTS]}";
		# Loop : valuelist requirements.
		for var_requirement in "${var_requirements[@]}"; do
			var_requirement_type="$(echo "$var_requirement" | cut -d ' ' -f1)";
			var_requirement_name="$(echo "$var_requirement" | cut -d ' ' -f2)";
			var_requirement_state="$(echo "$var_requirement" | cut -d ' ' -f3)";
			var_requirements_text+="$(echo -e "\t${var_requirement_type} '${var_requirement_name}' (${var_requirement_state})\n\n ")";
		done;
	else
		var_requirements_text+="\tx\n\n ";
	fi;
	# synopsis + options.
	local i;
	local var_key;
	local var_option;
	local var_option_name;
	local var_option_quick;
	local var_option_type;
	local var_option_state;
	local var_option_dsc;
	local var_option_dsc_def;
	local var_option_dsc_val_ok;
	local var_option_dsc_val_default;
	local var_synopsis_text_mandatory;
	local var_synopsis_text;
	local var_options_text;
	var_synopsis_text_mandatory=false;
	# Loop : valuelist options array inside APP.
	for i in $(echo "${!OPTIONS_SORT[@]}" | tr ' ' '\n' | sort -n); do
		var_key="${OPTIONS_SORT[$i]}";
		var_option="${OPTIONS[$var_key]}";
		var_option_name="$(echo "$var_key" | tr '[:upper:]' '[:lower:]'| sed 's/_/-/g')";
		var_option_quick="$(echo "$var_option" | cut -d '#' -f2)";
		var_option_type="$(echo "$var_option" | cut -d '#' -f3)";
		var_option_state="$(echo "$var_option" | cut -d '#' -f4)";
		var_option_dsc="$(echo "$var_option" | cut -d '#' -f5)";
		var_option_dsc_def="$(echo "$var_option_dsc" | cut -d '|' -f1)";
		var_option_dsc_val_ok="$(echo "$var_option_dsc" | cut -d '|' -f2)";
		var_option_dsc_val_default="$(echo "$var_option_dsc" | cut -d '|' -f3)";
		# Test : mandatory OPTION.
		if [[ "$var_option_state" == "mandatory" ]]; then var_synopsis_text_mandatory=true; fi;
		# Test : command text.
		if [[ "$var_option_quick" == "null" ]]; then
			var_options_text+="$(echo -e "\t--${var_option_name} (no arg) ")";
		else
			# Test : command type (underline).
			if [[ "$var_option_type" == "null" ]]; then
				var_options_text+="$(echo -e "\t-${var_option_quick} (no type),\n\t--${var_option_name} (no type)\n ")";
			else
				var_options_text+="$(echo -e "\t-${var_option_quick} ${var_option_type},\n\t--${var_option_name} ${var_option_type}\n ")";
			fi;
		fi;
		var_options_text+="$(echo -e "\t\t${var_option_dsc_def}\n ")";
		var_options_text+="$(echo -e "\t\t- state : ${var_option_state}.\n ")";
		# Test : dsc redefine.
		if [[ "$var_option_dsc_val_ok" == "null" ]]; then var_option_dsc_val_ok="nothing."; fi;
		var_options_text+="$(echo -e "\t\t- accepted value(s) : ${var_option_dsc_val_ok}\n ")";
		var_options_text+="$(echo -e "\t\t- default value : ${var_option_dsc_val_default}\n\n ")";
	done;
	# Test : [] for OPTIONS if only one OPTION is mandatory.
	if [[ "$var_synopsis_text_mandatory" == true ]]; then var_synopsis_text="OPTIONS"; else var_synopsis_text="[ OPTIONS ]"; fi;
	# examples.
	local var_examples;
	local var_example;
	local var_example_dsc;
	local var_example_cmd;
	local var_examples_text;
	IFS='#' read -ra var_examples <<< "${APP[EXAMPLES]}";
	# Loop : valuelist examples.
	for var_example in "${var_examples[@]}"; do
		var_example_dsc="$(echo "$var_example" | cut -d '|' -f1)";
		var_example_cmd="$(echo "$var_example" | cut -d '|' -f2)";
		var_examples_text+="$(echo -e "\t${var_example_dsc}\n\t\t${var_example_cmd}\n\n ")";
	done;
	# todo.
	if [[ -z "${APP[TODO]}" || "${APP[TODO]}" == "null" ]]; then APP["TODO"]="x"; fi;
	# notes.
	local var_notes;
	local var_note;
	local var_notes_text;
	if [[ -n "${APP[NOTES]}" && "${APP[NOTES]}" != "null" ]]; then
		IFS='#' read -ra var_notes <<< "${APP[NOTES]}";
		# Loop : valuelist notes.
		for var_note in "${var_notes[@]}"; do
			var_notes_text+="$(echo -e "\t${var_note}\n\n ")";
		done;
	else
		var_notes_text+="\tx\n\n ";
	fi;
	# credits.
	if [[ -z "${APP[CREDITS]}" || "${APP[CREDITS]}" == "null" ]]; then APP["CREDITS"]="x"; fi;
	# changelog.
	local var_changelog;
	local var_change;
	local var_change_version;
	local var_change_date;
	local var_change_dsc;
	local var_changelog_text;
	IFS='#' read -ra var_changelog <<< "${APP[CHANGELOG]}";
	# Loop : valuelist changelog.
	for var_change in "${var_changelog[@]}"; do
		var_change_version="$(echo "$var_change" | cut -d '|' -f1)";
		var_change_date="$(LC_TIME=en_US.UTF-8 date -d "$(echo "$var_change" | cut -d '|' -f2)" "+%Y-%m-%d")";
		var_change_dsc="$(echo "$var_change" | cut -d '|' -f3)";
		var_changelog_text+="$(echo -e "\t${var_change_version} - ${var_change_date}\n\t\t${var_change_dsc}\n\n ")";
	done;
	Restore_IFS;
	echo -e "\nNAME\n\n\t${APP[NAME]} - ${APP[SLOGAN]}\n\n\nREQUIREMENTS\n\n${var_requirements_text}\nSYNOPSIS\n\n\t${APP[NAME]} ${var_synopsis_text}\n\n\nDESCRIPTION\n\n\t${APP[DESCRIPTION]}\n\n\nOPTIONS\n\n${var_options_text}\nEXAMPLES\n\n${var_examples_text}\nTO-DO\n\n\t${APP[TODO]}\n\n\nNOTES\n\n${var_notes_text}\nCREDITS & THANKS\n\n\t${APP[CREDITS]}\n\n\nCHANGELOG\n\n${var_changelog_text}\nMETADATAS\n\n\tAuthor : ${APP[AUTHOR]}\n\tWebsite : ${APP[URL]}\n\tVersion : ${APP[VERSION]}\n\tLicense : ${APP[LICENSE]}\n\n";
}


# Test_Options.
#	DESCRIPTION : global test for search missing options.
#	RETURN : string.
function Test_Options() {
	# Some definition.
	local var_options_key;
	local var_options_values;
	local var_options_value_name;
	local var_options_value_type;
	local var_options_value_state;
	declare -a var_options_missing;
	local var_options_missing_list;
	# Some init.
	var_options_missing=();
	# Test : data exists.
	if [ ${#OPTIONS[@]} -gt 0 ]; then
		# Loop : indexlist options.
		for var_options_key in "${!OPTIONS[@]}"; do
			var_options_values="${OPTIONS[$var_options_key]}";
			var_options_value_name="$(echo "$var_options_key" | tr '[:upper:]' '[:lower:]')";
			var_options_value_type="$(echo "$var_options_values" | cut -d '#' -f3)";
			var_options_value_state="$(echo "$var_options_values" | cut -d '#' -f4)";
			# Test : trigger warning.
			if [ "$var_options_value_state" == "mandatory" ]; then
					# Test : trigger array.
					if [[ -z "${PARAM[$var_options_key]}" ]]; then
						var_options_missing+=("${var_options_value_name}");
					fi;
			elif [ "$var_options_value_state" != "optionnal" ]; then
				# Test : option with boolean don't need parameter.
				if [ "$var_options_value_type" != "null" ]; then
					Stop_App -CODE 1 -TEXT "Option '--${var_options_value_name}' type is empty, please report the issue.";
				fi;
			fi;
		done;
		# Test : missing exists.
		if [ ${#var_options_missing[@]} -gt 0 ]; then
			# Loop : indexlist missing.
			for i in "${!var_options_missing[@]}"; do
				var_options_missing[i]="--${var_options_missing[i]}";
			done;
			# Test : single, multiple data.
			if [ ${#var_options_missing[@]} -eq 1 ]; then
				Stop_App -CODE 1 -TEXT "Need '${var_options_missing[0]}' option. Type '--help' for read the cute manual.";
			else
				var_options_missing_list="${var_options_missing[*]}";
				var_options_missing_list="${var_options_missing_list// /, }";
				Stop_App -CODE 1 -TEXT "Need '$var_options_missing_list' options. Type '--help' for read the cute manual.";
			fi;
		fi;
	else
		Stop_App -CODE 1 -TEXT "No option provided, please report the issue.";
	fi;
}


# Test_Param.
#	DESCRIPTION : single test for checking parameter (argument) of one option.
#	RETURN : string.
function Test_Param() {
	# Some definition.
	local var_option;
	local var_param;
	local var_type;
	local var_contains;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-OPTION)
				var_option="$2"
				shift 2
				;;
			-PARAM)
				var_param="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-CONTAINS)
				var_contains="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Tests : param mispelled, empty, strict type restriction.
	if [[ "$var_param" == "-"* ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't starting by '-'.";
	elif [[ -z "$var_param" ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't be empty.";
	elif [[ -n "$var_type" ]]; then
		if [[ "$var_type" == "char" ]]; then
			if [[ "$var_param" =~ [0-9] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have integer.";
			fi;
		elif [[ "$var_type" == "int" ]]; then
			if [[ "$var_param" =~ [a-zA-Z] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have character.";
			fi;
		fi;
	fi;
	# Test : character present.
	if [[ -n "$var_contains" ]]; then
		if [[ "$var_param" != *"${var_contains}"* ]]; then
			Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' don't contains '${var_contains}' character.";
		fi;
	fi;
}


# Get_StartingApp.
#	DESCRIPTION : script starter pack.
#	RETURN : string.
function Get_StartingApp() {
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Title.
		echo -e "${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: ${DATE[DATETIME3]} ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
		# Place & soft env.
		echo -e "- work from : '${CONF[PATH]}'.";
		if [ -n "$SNAP" ]; then
			echo -e "- Snapcraft environment detected.";
		elif [ -n "$FLATPAK_ID" ]; then
			echo -e "- Flatpak environment detected.";
		fi;
	fi;
	# Test : requirements.
	if [[ -n "${APP[REQUIREMENTS]}" && "${APP[REQUIREMENTS]}" != "null" ]]; then
		# Some definition.
		local var_requirements;
		local var_requirement;
		local var_element;
		local var_type;
		local var_name;
		local var_dependencie;
		local var_software;
		# Indexed array.
		local var_requirement_warn=();
		local var_requirement_warn_element;
		local var_requirement_warn_show=();
		local var_requirement_warn_show_formatted;
		local var_requirement_nok=();
		local var_requirement_nok_element;
		local var_requirement_nok_show=();
		local var_requirement_nok_show_formatted;
		# Auto-discovery from APP book.
		IFS='#' read -ra var_requirements <<< "${APP[REQUIREMENTS]}";
		Restore_IFS;
		# Loop : valuelist requirements.
		for var_requirement in "${var_requirements[@]}"; do
			IFS=' ' read -ra var_element <<< "$var_requirement";
			Restore_IFS;
			var_type="${var_element[0]}";
			var_name="${var_element[1]}";
			var_dependencie="${var_element[2]}";
			var_software=$(Test_Software -NAME "$var_name" -TYPE "$var_type");
			# Test : software.
			if [[ "$var_software" != "available" ]]; then
				# Test : dependencie.
				if [[ "$var_dependencie" == "mandatory" ]]; then
					var_requirement_nok+=("${var_type}#${var_name}#${var_software}");
				else
					var_requirement_warn+=("${var_type}#${var_name}#${var_software}");
				fi;
			fi;
		done;
		# Tests : ok, warn, nok.
		if [[ "${#var_requirement_warn[@]}" -eq 0 && "${#var_requirement_nok[@]}" -eq 0 ]]; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- software requirements : all dependencie(s) available.";
			fi;
		else
			if [[ "${#var_requirement_warn[@]}" -ge 1 && "${#var_requirement_nok[@]}" -eq 0 ]]; then
				# Loop : valuelist requirements warn.
				for var_requirement_warn_element in "${var_requirement_warn[@]}"; do
					IFS='#' read -ra var_requirement_warn_element_spec <<< "$var_requirement_warn_element";
					Restore_IFS;
					var_requirement_warn_show+=("${var_requirement_warn_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_warn_show_formatted=$(IFS=', '; echo "${var_requirement_warn_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[ORANGE_LIGHT]}${var_requirement_warn_show_formatted}${COLOR[STOP]} are required but optionnal.";
				fi;
			elif [[ "${#var_requirement_warn[@]}" -ge 1 && "${#var_requirement_nok[@]}" -ge 1 ]]; then
				# Loop : valuelist requirements warn.
				for var_requirement_warn_element in "${var_requirement_warn[@]}"; do
					IFS='#' read -ra var_requirement_warn_element_spec <<< "$var_requirement_warn_element";
					Restore_IFS;
					var_requirement_warn_show+=("${var_requirement_warn_element_spec[1]}");
				done;
				# Loop : valuelist requirements nok.
				for var_requirement_nok_element in "${var_requirement_nok[@]}"; do
					IFS='#' read -ra var_requirement_nok_element_spec <<< "$var_requirement_nok_element";
					Restore_IFS;
					var_requirement_nok_show+=("${var_requirement_nok_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_warn_show_formatted=$(IFS=', '; echo "${var_requirement_warn_show[*]}");
				var_requirement_nok_show_formatted=$(IFS=', '; echo "${var_requirement_nok_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[RED_LIGHT]}${var_requirement_nok_show_formatted}${COLOR[STOP]} are required and mandatory - ${COLOR[ORANGE_LIGHT]}${var_requirement_warn_show_formatted}${COLOR[STOP]} are required but optionnal.";
				fi;
				Stop_App -CODE 1 -TEXT "Need to fix dependencies.";
			elif [[ "${#var_requirement_warn[@]}" -eq 0 && "${#var_requirement_nok[@]}" -ge 1 ]]; then
				# Loop : valuelist requirements nok.
				for var_requirement_nok_element in "${var_requirement_nok[@]}"; do
					IFS='#' read -ra var_requirement_nok_element_spec <<< "$var_requirement_nok_element";
					Restore_IFS;
					var_requirement_nok_show+=("${var_requirement_nok_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_nok_show_formatted=$(IFS=', '; echo "${var_requirement_nok_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[RED_LIGHT]}${var_requirement_nok_show_formatted}${COLOR[STOP]} are required and mandatory.";
				fi;
				Stop_App -CODE 1 -TEXT "Need to fix dependencies.";
			fi;
		fi;
	fi;
	# Test : output (data backup).
	if [[ "${PARAM[BACKUP]}" == true ]]; then
		# Some definition.
		local var_naming;
		# Define naming mode based on output definition.
		if [[ "${PARAM[OUTPUT]}" =~ -${DATE[DATETIME1]}\. ]]; then var_naming="date"; else var_naming="custom"; fi;
		# Test : output mode.
		if [[ "$var_naming" == "date" ]]; then
			# Test : get previous output old.
			if [[ -n "${PARAM[OUTPUT_BACKUP]}" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) in '${PARAM[OUTPUT_BACKUP]}' (date name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (date name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (date name mode).";
				fi;
			fi;
		else
			# Test : move previous output to old.
			if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT]}"; then
					mv "${PARAM[OUTPUT]}" "${PARAM[OUTPUT_BACKUP]}";
					# TMPSOLUTION.
					# Test : move all file formats.
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.json"; then
						mv "${PARAM[OUTPUT]%.csv}.json" "${PARAM[OUTPUT_BACKUP]%.csv}.json";
					fi;
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.yaml"; then
						mv "${PARAM[OUTPUT]%.csv}.yaml" "${PARAM[OUTPUT_BACKUP]%.csv}.yaml";
					fi;
					# /TMPSOLUTION.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) moved to '${PARAM[OUTPUT_BACKUP]}' (custom name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (custom name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (custom name mode).";
				fi;
			fi;
		fi;
	fi;
	# Test : output (main).
	if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" && "${PARAM[RAW]}" != true ]]; then
		# Tests : folder, file.
		if [[ "${PARAM["OUTPUT"]}" =~ /$ ]]; then
			if Test_Dir -PATH "${PARAM[OUTPUT]}" -CREATE; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- current output to folder '${PARAM[OUTPUT]}'.";
				fi;
			else
				Stop_App -CODE 1 -TEXT "Folder output '${PARAM[OUTPUT]}' can't be writed.";
			fi;
		elif Test_File -PATH "${PARAM[OUTPUT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current output to file '${PARAM[OUTPUT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "File output '${PARAM[OUTPUT]}' can't be writed.";
		fi;
	fi;
	# Test : logging.
	if [[ "$(declare -p LOG 2>/dev/null)" =~ declare\ -A\ LOG ]] && [[ "${#LOG[@]}" -gt 0 && -n "${LOG[PATH]}" ]]; then
		# Some definition.
		local var_log;
		# Test : Logging path ('/var/log' for sure ...).
		if Test_Dir -PATH "${LOG[PATH]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current log to folder '${LOG[PATH]}'.";
			fi;
			# Loop : valuelist logging files.
			for var_log in "${!LOG[@]}"; do
				if [[ "$var_log" != "PATH" ]]; then
					Test_File -PATH "${LOG[$var_log]}" -CREATE;
				fi;
			done;
		else
			Stop_App -CODE 1 -TEXT "Folder output '${LOG[PATH]}' can't be writed.";
		fi;
	fi;
	# Test : credential.
	if [[ -n "${PARAM[CREDENTIAL]}" && "${PARAM[CREDENTIAL]}" != "null" && "${PARAM[QUIET]}" != true ]]; then
		# Some definition.
		local var_credential;
		local var_software;
		# Indexed array.
		var_credential_show=();
		local var_credential_show_formatted;
		# Loop : indexlist credential. 
		for var_credential in "${!CREDENTIAL[@]}"; do
			var_credential_show+=("$var_credential");
		done;
		# Comma-separated values.
		var_credential_show_formatted=$(IFS=', '; echo "${var_credential_show[*]}");
		Restore_IFS;
		echo -e "- credential(s) used for : ${var_credential_show_formatted}.";
	fi;
	if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
}


# Get_EndingApp.
#	DESCRIPTION : script ending pack.
#	RETURN : string.
function Get_EndingApp() {
	# Some definition.
	local var_date_start;
	local var_date_end;
	local var_date_end_seconds;
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Some init;
		var_date_start="${DATE["TIMESTAMP0"]}";
		var_date_end="$(date "+%s%3N")";
		var_date_end_seconds=$(Get_DateDiff -START "$var_date_start" -END "$var_date_end");
		echo -e "\n${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: executed in ${var_date_end_seconds} seconds ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
	fi;
}


# Stop_App.
#	DESCRIPTION : end program.
#	RETURN : string and code.
function Stop_App() {
	# Some definition.
	local var_code;
	local var_text;
	local var_quiet;
	local var_help;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CODE)
				var_code="$2"
				shift 2
				;;
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-QUIET)
				var_quiet=true
				shift
				;;
			-HELP)
				var_help=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : splash error.
	if [[ "$var_quiet" != true && -n "$var_text" ]]; then
		Write_Script -TEXT "EXIT ERROR ! ${var_text}";
	fi;
	# Test : splash man.
	if [[ "$var_help" == true ]]; then Get_Help; fi;
	wait;
	exit "$var_code";
}


# Debug_Start.
#	DESCRIPTION : debug app.
function Debug_Start() { set -x; }


# Debug_Stop.
#	DESCRIPTION : debug app.
function Debug_Stop() { set +x; }


# Get_TableHeader.
#	DESCRIPTION : display a table index.
#	RETURN : string.
function Get_TableHeader() {
	# Some definition.
	local var_array;
	local var_element;
	local var_param;
	local var_param_length;
	local var_dot_length;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a name param and the max character count a value of this param can be in the rows.
		for element_header in "${!var_array[@]}"; do
			var_element="${var_array[$element_header]}";
			var_param="$(echo "$var_element" | cut -d ':' -f1)";
			var_param_length="${#var_param}";
			var_dot_length="$(echo "$var_element" | cut -d ':' -f2)";
			# Calculate how many characters to add more.
			var_param_length_more=$((var_dot_length - var_param_length));
			var_dot_length_more=$((var_param_length - var_dot_length));
			# Ready to construct header.
			var_line1+="$var_param";
			var_line1+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_param_length_more");
			var_line1+="  ";
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length");
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length_more");
			var_line2+="  ";
		done;
		echo -e "$var_line1";
		echo -e "$var_line2";
	fi;
}


# Get_TableRow.
#	DESCRIPTION : display a table row.
#	RETURN : string.
function Get_TableRow() {
	# Some definition.
	local var_array;
	local var_action;
	local var_element;
	local var_value;
	local var_value_length;
	local var_max;
	local var_line;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			-ACTION)
				var_action="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a value, the max character count this value can be.
		for element_row in "${!var_array[@]}"; do
			var_element="${var_array[$element_row]}";
			var_value="$(echo "$var_element" | cut -d ':' -f1)";
			var_max="$(echo "$var_element" | cut -d ':' -f2)";
			# Test : tick cross.
			if [[ "$var_value" == "tick" || "$var_value" == "cross" ]]; then
				if [[ "$var_value" == "tick" ]]; then
					var_value="${COLOR[TICK]}";
				elif [[ "$var_value" == "cross" ]]; then
					var_value="${COLOR[CROSS]}";
				fi;
				var_value_length=3;
			# TODO
			# elif [[ "$var_value" == *"tick"* || "$var_value" == *"cross"* ]]; then
			else
				var_value_length="${#var_value}";
			fi;
			# Calculate how many space to add more.
			var_value_length_more=$((var_max - var_value_length));
			# Ready to construct row.
			var_line+="$var_value";
			var_line+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_value_length_more");
			var_line+="  ";
		done;
		# Test : replace line.
		if [[ "$var_action" == "check" ]]; then
			echo -ne "$var_line";
		else
			echo -e "\\r$var_line";
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_string.sh               | #
# |_____________________________________________| #


# ======================== #
#          STRING          #
#
#	DESCRIPTION : pre-defined work with simple string manipulation.


# Get_DataFormat.
#	DESCRIPTION : get format based on first line content.
#	RETURN : string or code.
function Get_DataFormat() {
	# Some definition.
	local var_data;
	local var_substring;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Restrict test.
	var_substring="${var_data:0:50}";
	# Tests : CSV, JSON, YAML.
	if [[ "$var_substring" =~ , && ! "$var_substring" =~ [:{].*, ]]; then
		echo "csv";
	elif [[ "$var_substring" =~ ^[\{\[] ]]; then
		echo "json";
	elif [[ "$var_substring" =~ : && ! "$var_substring" =~ [,{]:*, ]]; then
		echo "yaml";
	else
		return 1;
	fi;
}


# Remove_DuplicateInString.
#	DESCRIPTION : remove duplicate value in string.
#	RETURN : string.
function Remove_DuplicateInString() {
	# Some definition.
	local var_string;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	echo "$var_string" | tr ' ' '\n' | sort | uniq | xargs;
}


# Search_InCsv.
#	DESCRIPTION : search if value exists in CSV file.
#	RETURN : code.
#	TODO : replace break.
function Search_InCsv() {
	# Some definition.
	local var_header;
	local var_header_cut;
	local var_line;
	local var_line_cut;
	local var_pattern;
	local var_delimiter;
	local var_position;
	local i;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-HEADER)
				var_header="$2"
				shift 2
				;;
			-LINE)
				var_line="$2"
				shift 2
				;;
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_delimiter" ]; then var_delimiter=","; fi;
	# Split headers and data line into fields.
	IFS="$var_delimiter" read -ra var_header_cut <<< "$var_header";
	IFS="$var_delimiter" read -ra var_line_cut <<< "$var_line";
	Restore_IFS;
	# Find the index of the corresponding column.
	var_position=-1;
	for i in "${!var_header_cut[@]}"; do
		if [[ "${var_header_cut[$i]}" == "$var_pattern" ]]; then
			var_position="$i";
			break;
		fi;
	done;
	# Test : valid index.
	if [[ "$var_position" -ge 0 && "$var_position" -lt "${#var_line_cut[@]}" ]]; then
		echo "${var_line_cut[$var_position]}";
	else
		return 1;
	fi;
}


# Set_RepeatCharacter.
#	DESCRIPTION : calculate how many same character repeat needed.
#	RETURN : string.
function Set_RepeatCharacter() {
	# Some definition.
	local var_character;
	local var_count;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-CHARACTER)
				var_character="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : if arg empty.
	if [ -z "$var_character" ] || [ -z "$var_count" ]; then
		return 1;
	else
		var_result="";
		for ((i=0; i < "$var_count"; i++)); do
			var_result+="$var_character";
		done;
	fi;
	echo "$var_result";
}


# Limit_StringLength.
#	DESCRIPTION : limit number of character in a string.
#	RETURN : string.
#	TODO : replace -DIRECTION by -POSITION.
function Limit_StringLength() {
	# Some definition.
	local var_string;
	local var_count;
	local var_count_before;
	local var_count_after;
	local var_direction;
	local var_spacer;
	local var_spacer_length;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			-DIRECTION)
				var_direction="$2"
				shift 2
				;;
			-SPACER)
				var_spacer="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_direction" ]; then var_direction="after"; fi;
	if [ -z "$var_spacer" ]; then var_spacer="(...)"; fi;
	var_spacer_length="${#var_spacer}";
	# Test : if arg empty.
	if [ -z "$var_string" ] || [ -z "$var_count" ]; then
		return 1;
	else
		# Test : cut needed.
		if [ ${#var_string} -gt "$var_count" ]; then
			# Add spacer length to cut count.
			var_count_before=$((${#var_string} - var_count + var_spacer_length + 1));
			var_count_after=$((var_count - var_spacer_length));
			# Tests : sub place.
			if [ "$var_direction" = "before" ]; then
				var_result+="$var_spacer";
				var_result+=$(echo "$var_string" | cut -c "$var_count_before"-"${#var_string}");
			elif [ "$var_direction" = "after" ]; then
				var_result+=$(echo "$var_string" | cut -c 1-"$var_count_after");
				var_result+="$var_spacer";
			fi;
			echo "$var_result";
		else
			echo "$var_string";
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_date.sh               | #
# |__________________________________________| #


# ====================== #
#          DATE          #
#
#	DESCRIPTION : pre-defined working date.


# DATE.
declare -gA DATE;
DATE["CAPTURE"]="$(date "+%s%3N")";
# ex EXTRA_TIMESTAMP : execution time diff calc (timestamp in milliseconds).
DATE["TIMESTAMP0"]="${DATE[CAPTURE]}";
# ex CURRENT_DATE : jekyll file name markdown, manuel page.
DATE["DATE0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d")";
# distro files JSON & YAML.
DATE["DATETIME0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%dT%H:%M:%S%z")";
# ex COMPACT_TIMESTAMP & DATE_FILE : output filenames.
DATE["DATETIME1"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y%m%d_%H%M%S")";
# ex FULL_TIMESTAMP & DATE_START_EXEC : page.date for jekyll inside markdown file, added date inside join-files output.
DATE["DATETIME2"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d %H:%M:%S")";
# ex FORMATTED_DATE_TIME & DATE_START : bash execution time.
DATE["DATETIME3"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%d/%m/%Y - %Hh%M")";


# Get_DateDiff.
#	DESCRIPTION : diff calc in seconds & milliseconds.
#	RETURN : string.
function Get_DateDiff() {
	# Some definition.
	local var_start;
	local var_start_sec;
	local var_start_msec;
	local var_end;
	local var_end_msec;
	local var_end_sec;
	local var_diff_sec;
	local var_diff_msec;
	local var_msec_2_digits;
	local var_result;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-START)
				var_start="$2"
				shift 2
				;;
			-END)
				var_end="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Extract seconds & milliseconds.
	var_start_sec="${var_start:0:10}";
	var_start_msec="${var_start:10:3}";
	var_end_sec="${var_end:0:10}";
	var_end_msec="${var_end:10:3}";
	# Remove leading zeros to avoid octal interpretation.
	var_start_sec=$((10#$var_start_sec));
	var_start_msec=$((10#$var_start_msec));
	var_end_sec=$((10#$var_end_sec));
	var_end_msec=$((10#$var_end_msec));
	# Diff calc.
	var_diff_sec=$((var_end_sec - var_start_sec));
	var_diff_msec=$((var_end_msec - var_start_msec));
	# Test : milliseconds diff is negative.
	if [ $var_diff_msec -lt 0 ]; then
		var_diff_sec=$((var_diff_sec - 1));
		var_diff_msec=$((var_diff_msec + 1000));
	fi;
	# Test : milliseconds with two digits.
	if [ $var_diff_msec -lt 10 ]; then
		var_msec_2_digits="0$((var_diff_msec / 10))";
	else
		var_msec_2_digits=$((var_diff_msec / 10));
	fi;
	var_result="${var_diff_sec}.${var_msec_2_digits}";
	echo "$var_result";
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_network.sh               | #
# |_____________________________________________| #


# ========================= #
#          NETWORK          #
#
#	DESCRIPTION : network state for all APPs.

# Get_FirstNetwork.
#	DESCRIPTION : get name of the first network interface.
#	RETURN : string or code.
function Get_FirstNetwork() {
	# Some definition.
	local var_result;
	local var_interfaces;
	var_result="fail-network";
	var_interfaces=$(ip -o link show | awk -F': ' '{print $2}');
	# Loop : interfaces.
	for interface in $var_interfaces; do
		if [[ $interface != "lo" ]]; then var_result="$interface"; break; fi;
	done
	echo "$var_result";
}


# Get_Distant.
#	DESCRIPTION : get distant resource - file, string, ping, git repository, with auth available.
#	RETURN : string or code.
#	TO-DO : git auth.
function Get_Distant() {
	# Some definition - menu.
	local var_method;
	local var_type;
	local var_url;
	local var_path;
	local var_credential;
	local var_username;
	local var_password;
	local var_overwrite;
	local var_log;
	local var_verbose;
	# Some definition - next.
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-METHOD)
				var_method="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-LOG)
				var_log="$2"
				shift 2
				;;
			-VERBOSE)
				var_verbose=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Some default.
	if [[ -z "$var_method" ]]; then var_method="wget"; fi;
	if [[ -z "$var_type" ]]; then var_type="file"; fi;
	# Test : auth.
	if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" && "$var_credential" == *":"* ]]; then
		var_username=$(echo -e "$var_credential" | cut -d ':' -f 1);
		var_password=$(echo -e "$var_credential" | cut -d ':' -f 2);
	fi;
	# Tests : method.
	if [[ "$var_method" == "wget" ]]; then
		# Tests : file, string, ping.
		if [[ "$var_type" == "file" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if [[ -n "$var_verbose" ]]; then
						wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				else
					if [[ -n "$var_verbose" ]]; then
						wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log"
							if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if [[ -n "$var_verbose" ]]; then
						wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				else
					if [[ -n "$var_verbose" ]]; then
						wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				fi;
			fi;
		elif [[ "$var_type" == "string" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				fi;
			fi;
			# Test : good content.
			if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
		elif [[ "$var_type" == "ping" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		fi;
	elif [[ "$var_method" == "curl" ]]; then
		# Test : auth.
		if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
			var_result=$(curl -s -H "Authorization: token ${var_credential}" "$var_url" | tr -d '\n' | sed 's/ //g');
		else
			var_result=$(curl -s "$var_url" | tr -d '\n' | sed 's/ //g');
		fi;
		# Test : good content.
		if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
	elif [[ "$var_method" == "git" ]]; then
		# Test : replace.
		if [[ "$var_overwrite" = true ]]; then
			if git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; then return 0; else return 1; fi;
		else
			git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; return 0;
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_fs.sh               | #
# |________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interaction for all APPs.


# Test_Dir.
#	DESCRIPTION : check directory.
#	RETURN : code.
function Test_Dir() {
	# Some definition.
	local var_path;
	local var_create;
	local var_chown;
	local var_chmod;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : folder.
	if [[ -n "$var_path" ]]; then
		if [[ "$var_create" = true ]]; then mkdir -p "$var_path" &> /dev/null; fi;
		if [ -d "$var_path" ]; then
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown -R "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod -R "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Test_DirPopulated().
#	DESCRIPTION : check if folder haz content in it.
#	RETURN : code.
function Test_DirPopulated() {
	# Some definition.
	local var_path;
	local var_extension;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-EXTENSION)
				var_extension="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	if [[ -n "$(ls -A "$var_path")" ]]; then
		if [[ -n "$var_extension" ]]; then
			if find "$var_path" -type f -name "*$var_extension" &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		else
			return 0;
		fi;
	else
		return 1;
	fi;
}


# Test_File.
#	DESCRIPTION : check file.
#	RETURN : code.
function Test_File() {
	# Some definition.
	local var_path;
	local var_overwrite;
	local var_create;
	local var_chown;
	local var_chmod;
	local var_exec;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-EXEC)
				var_exec=true
				shift
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		if [ -f "$var_path" ]; then
			if [[ "$var_overwrite" = true ]]; then
				rm "$var_path" &> /dev/null;
				# Tests : dir chown + chmod || chown || chmod || regular.
				if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" && -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown" -CHMOD "$var_chmod";
				elif [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown";
				elif [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHMOD "$var_chmod";
				else
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				fi;
				touch "$var_path" &> /dev/null;
			fi;
		else
			if [[ "$var_overwrite" = true ]] || [[ "$var_create" = true ]]; then
				# Tests : dir chown + chmod || chown || chmod || regular.
				if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" && -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown" -CHMOD "$var_chmod";
				elif [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown";
				elif [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHMOD "$var_chmod";
				else
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				fi;
				touch "$var_path" &> /dev/null;
			fi;
		fi;
		if [ -f "$var_path" ]; then
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_exec" && "$var_exec" = true ]]; then chmod +x "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Search_File.
#	DESCRIPTION : get filepath with regex, favourite extension filter available (the last win).
#	RETURN : string or code.
function Search_File() {
	# Some definition - menu.
	local var_path;
	local var_extension;
	local var_tool;
	local var_mode;
	# Some definition - next.
	local var_extension_favorite;
	local var_file;
	local var_result;
	local var_result_favorite;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="${2%[\\/]}"; shift 2 ;;
			-EXTENSION) var_extension="$2"; shift 2 ;;
			-TOOL) var_tool="$2"; shift 2 ;;
			-MODE) var_mode="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Some init.
	var_extension_favorite="${var_extension##*|}";
	# Tests : distro-tracker/distro-reporter.
	if [[ "$var_tool" == "distro-tracker" || "$var_tool" == "distro-reporter" ]]; then
		# Tests : distro-tracker.current.EXT || distro-reporter.EXT || DT/DR-date.EXT .
		if [[ "$var_mode" == "name" ]]; then
			# Loop : Process Substitution auto-discovery - last file win data state.
			while IFS= read -r var_file; do
				# Test : data exists.
				if Test_File -PATH "$var_file"; then
					var_result="$var_file";
					# Test : favorite extension.
					if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
						var_result_favorite="$var_file";
					fi;
				fi;
			done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/(distro-tracker\.current|distro-reporter)\.(${var_extension})$" 2>/dev/null | sort);
			Restore_IFS;
		elif [[ "$var_mode" == "date" ]]; then
			# Loop : Process Substitution auto-discovery - last file win data state.
			while IFS= read -r var_file; do
				# Test : data exists.
				if Test_File -PATH "$var_file"; then
					var_result="$var_file";
					# Test : favorite extension.
					if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
						var_result_favorite="$var_file";
					fi;
				fi;
			done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/(DT|DR)-([0-9]{8}_[0-9]{6})\.(${var_extension})$" 2>/dev/null | sort);
			Restore_IFS;
		fi;
		# Test : good content.
		if [[ -n "$var_result" && "$var_result" != false && "$var_result" != "null" ]]; then
			# Test : favorite extension.
			if [[ -n "$var_result_favorite" && "$var_result_favorite" != false && "$var_result_favorite" != "null" ]]; then
				echo "$var_result_favorite";
			else
				echo "$var_result";
			fi;
		else
			return 1;
		fi;
	elif [[ "$var_tool" == "distro-publisher" ]]; then
		# Loop : Process Substitution auto-discovery - last file win data state.
		while IFS= read -r var_file; do
			# Test : data exists.
			if Test_File -PATH "$var_file"; then
				var_result="$var_file";
				# Test : favorite extension.
				if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
					var_result_favorite="$var_file";
				fi;
			fi;
		done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/${DATE[DATE0]}-[0-9]{1,3}-report\.(${var_extension})$" 2>/dev/null | sort);
		Restore_IFS;
		# Test : good content.
		if [[ -n "$var_result" && "$var_result" != false && "$var_result" != "null" ]]; then
			# Test : favorite extension.
			if [[ -n "$var_result_favorite" && "$var_result_favorite" != false && "$var_result_favorite" != "null" ]]; then
				echo "$var_result_favorite";
			else
				echo "$var_result";
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Set_File.
#	DESCRIPTION : create file.
#	RETURN : code.
function Set_File() {
	# Some definition.
	local var_path;
	local var_raw;
	local var_interpret;
	local var_source;
	local var_append;
	local var_encode;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-RAW)
				var_raw="$2"
				shift 2
				;;
			-INTERPRET)
				var_interpret=true
				shift
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-APPEND)
				var_append=true
				shift
				;;
			-ENCODE)
				var_encode="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		touch "$var_path" &> /dev/null;
		# Tests : echo, cp.
		if [[ -n "$var_raw" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				# Test : interpreter.
				if [[ -n "$var_interpret" ]]; then
					echo -e "$var_raw" | tee -a "$var_path" > /dev/null;
				else
					echo "$var_raw" | tee -a "$var_path" > /dev/null;
				fi;
			else
				# Test : base64.
				if [[ -n "$var_encode" ]]; then
					echo "$var_raw" | base64 -d > "$var_path";
				else
					# Test : interpreter.
					if [[ -n "$var_interpret" ]]; then
						echo -e "$var_raw" | tee "$var_path" > /dev/null
					else
						echo "$var_raw" | tee "$var_path" > /dev/null;
					fi;
				fi;
			fi;
		elif [[ -n "$var_source" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				cat "$var_source" >> "$var_path";
			else
				cp "$var_path" "$var_source" &> /dev/null;
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Get_FileExtension.
#	DESCRIPTION : get last extension, with avoiding different extensions.
#	RETURN : code.
function Get_FileExtension() {
	# Some definition.
	local var_path;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	echo "$var_path" | rev | cut -d '.' -f 1 | rev;
}


# Find_FileLine.
#	DESCRIPTION : find line started by pattern in a file.
#	RETURN : string or code.
function Find_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file;
	local var_line;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE)
				var_file="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file"; then
		var_line=$(grep "$var_pattern" "$var_file" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line" ]]; then echo "$var_line"; else return 1; fi;
	else
		return 1;
	fi;
}


# Compare_FileLine.
#	DESCRIPTION : search 1st line started by pattern in 1st file, compare it to 2nd file.
#	RETURN : string or code.
function Compare_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file1;
	local var_file2;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1" && Test_File -PATH "$var_file2"; then
		var_line1=$(grep "^$var_pattern" "$var_file1" | head -n 1);
		var_line2=$(grep "^$var_pattern" "$var_file2" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line1" && "$var_line1" != false && "$var_line1" != "null" && \
			-n "$var_line2" && "$var_line2" != false && "$var_line2" != "null" ]]; then
			# Test : return file number 2 content for different content, return false for same content.
			if [[ "$var_line1" != "$var_line2" ]]; then echo "$var_line2"; else return 1; fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Backup_FileLine.
#	DESCRIPTION : recover line in 1st file from a 2nd file.
#	RETURN : string or code.
#	TODO : verify character | escaping.
function Backup_FileLine() {
	# Some definition.
	local var_pattern;
	local var_default;
	local var_file1;
	local var_file2;
	local var_diff;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DEFAULT)
				var_default="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1"; then
		var_diff=$(Compare_FileLine -PATTERN "$var_pattern" -FILE1 "$var_file1" -FILE2 "$var_file2");
		var_pattern="^${var_pattern}";
		# Tests : replace by old content for different content, replace by default for same/empty content.
		if [[ -n "$var_diff" && "$var_diff" != false ]]; then
			sed -i "s|${var_pattern}|${var_diff}|" "$var_file1";
			echo "old";
		elif [[ -n "$var_default" ]]; then
			echo -e "$var_default" >> "$var_file1";
			echo "default";
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
# shellcheck disable=SC2034
#  ___________________________________________
# |                                           | #
# |               func_array.sh               | #
# |___________________________________________| #


# ======================= #
#          ARRAY          #
#
#	DESCRIPTION : pre-defined work with indexed & associative (book) arrays.


# Sort_Array.
#	DESCRIPTION : write new array indexed from original.
#	TODO : auto index and manual index option.
#	RETURN : string.
function Sort_Array() {
	# Some definition.
	local var_name;
	local var_delimiter;
	local var_index;
	local var_key;
	local var_value;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : data exists.
	if [[ -z "$var_name" || -z "$var_delimiter" ]]; then return 1; fi;
	if ! declare -p "$var_name" &>/dev/null; then return 1; fi;
	# Construct array indexed.
	declare -gA "${var_name}_SORT";
	# Get data from original array.
	local -n var_name_orig="$var_name";
	local var_name_sorted="${var_name}_SORT";
	# Loop : get data from original array.
	for var_key in "${!var_name_orig[@]}"; do
		local var_value="${var_name_orig[$var_key]}";
		local var_index="${var_value%%"${var_delimiter}"*}";
		# Stores the key in the sorted array under the extracted index.
		local -n var_name_ref="${var_name_sorted}"
		var_name_ref["$var_index"]="$var_key";
	done;
}


# Search_InArray.
#	DESCRIPTION : search if value exists in indexed array.
#	RETURN : code.
function Search_InArray() {
	# Some definition.
	local var_pattern;
	local var_data;
	local var_array;
	local var_element;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : need all param.
	if [ -z "$var_pattern" ] || [ -z "$var_data" ]; then return 1; fi;
	# IFS for delimit array.
	IFS=" " read -ra var_array <<< "$var_data";
	Restore_IFS;
	# Loop : indexlist search in array.
	for var_element in "${var_array[@]}"; do
		if [[ "$var_element" == "$var_pattern" ]]; then
			return 0;
		fi;
	done;
	return 1;
}


# Get_PositionInArray.
#	DESCRIPTION : search index position by this pattern in indexed array.
#	RETURN : string or code.
function Get_PositionInArray() {
	# Some definition.
	local var_pattern;
	local var_delimiter;
	local var_array;
	local var_element;
	local var_index=-1;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_delimiter" ]; then
		var_delimiter=",";
	fi;
	# Test : need all param.
	if [ -z "$var_pattern" ] || [ -z "$var_data" ]; then
		return 1;
	fi;
	# Split the data using the specified delimiter.
	IFS="$var_delimiter" read -ra var_array <<< "$var_data";
	Restore_IFS;
	# Loop : indexlist through the columns to find the pattern.
	for var_element in "${!var_array[@]}"; do
		if [[ "${var_array[${var_element}]}" == "$var_pattern" ]]; then
			var_index="$var_element";
		fi;
	done;
	# Test : pattern found.
	if [[ "$var_index" -ge 0 ]]; then
		echo "$var_index";
	else
		return 1;
	fi;
}


# Measure_MaxInArray.
#	DESCRIPTION : calculate longest word in array.
#	RETURN : string.
function Measure_MaxInArray() {
	# Some definition.
	local var_count;
	local var_array;
	local var_word;
	local var_length;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		var_count=0;
		for element in "${!var_array[@]}"; do
			var_word="${var_array[$element]}";
			var_length="${#var_word}";
			if [ "$var_length" -gt "$var_count" ]; then var_count="$var_length"; fi;
		done;
		echo "$var_count";
	fi;
}


# Add_Book.
#	DESCRIPTION : construct associative array.
function Add_Book() {
	# Some definition.
	local var_name="$1";
	# Test : book exists.
	if ! declare -p "$var_name" &>/dev/null; then declare -gA "$var_name"; fi;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRO" ]]; then
		# Some definition.
		local var_element;
		local var_data;
		local var_index;
		local var_fullname;
		local var_mode;
		local var_current_index;
		local var_current_release;
		local var_archive_index;
		local var_archive_release;
		local var_element_suffixes;
		local var_element_suffixes_var;
		local var_array;
		local var_array_keys;
		local var_array_suffixes;
		# Some init.
		var_element="$2";
		var_data="$3";
		# mode, indexes & releases.
		declare -a var_array_suffixes=("index" "fullname" "mode" "current_index" "current_release" "archive_index" "archive_release");
		# Loop : var_suffix key.
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			var_element_suffixes_var="var_${var_element_suffixes}";
			declare "${var_element_suffixes_var}"="${var_element}_${var_element_suffixes}";
		done;
		# Construct index & release values from Get_Distribution.
		IFS=',' read -ra var_array <<< "$var_data";
		Restore_IFS;
		declare -a var_array_keys=("$var_index" "$var_fullname" "$var_mode" "$var_current_index" "$var_current_release" "$var_archive_index" "$var_archive_release");
		# Loop var_suffix value.
		for i in "${!var_array_keys[@]}"; do
			eval "${var_name}[\"${var_array_keys[$i]}\"]=\"${var_array[$i]}\"";
		done;
	elif [[ "$var_name" == "TMP" ]]; then
		# Some init.
		local var_element="$2";
		local var_data="$3";
		eval "${var_name}[\"${var_element}\"]=\"${var_data}\"";
	fi;
}


# Get_Book.
#	DESCRIPTION : get associative array.
#	RETURN : string or code.
function Get_Book() {
	# Some definition.
	local var_name;
	local var_element;
	local var_data;
	local var_array_suffixes;
	local var_element_suffixes;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-ELEMENT)
				var_element="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRO" ]]; then
		# Loop: var_suffix key.
		declare -a var_array_suffixes=("index" "fullname" "mode" "current_index" "current_release" "archive_index" "archive_release");
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			if [[ "$var_data" == "$var_element_suffixes" ]]; then
				var_result="${DISTRO[${var_element}_$var_element_suffixes]}";
				break;
			fi;
		done;
	fi;
	# Test : return.
	if [[ -n "$var_result" && "$var_result" != "" ]]; then echo "$var_result"; else return 1; fi;
}


# Set_Book.
#	DESCRIPTION : set associative array from PARAM.
function Set_Book() {
	# Some definition.
	local var_name;
	local var_array;
	local var_element;
	local var_key;
	local var_value;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : new array.
	if ! eval "declare -p $var_name 2>/dev/null" | grep -q 'declare -A'; then
		eval "declare -gA $var_name";
	fi;
	# Test : predefined book.
	if [[ "$var_name" == "CREDENTIAL" ]]; then
		# Loop : valuelist credentials.
		IFS=',' read -ra var_array <<< "${PARAM[CREDENTIAL]}";
		Restore_IFS;
		for var_element in "${var_array[@]}"; do
			var_key=${var_element%%:*};
			var_value=${var_element#*:};
			CREDENTIAL["${var_key^^}"]="$var_value";
		done;
	fi;
}


# Remove_Book.
#	DESCRIPTION : remove associative array.
function Remove_Book() {
	# Some definition.
	local var_name="$1";
	unset "$var_name";
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_user.sh               | #
# |__________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test_Root.
#	DESCRIPTION : check root rights.
#	RETURN : code.
function Test_Root {
	if [[ "$EUID" -eq 0 ]]; then return 0; else return 1; fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_pkgs.sh               | #
# |__________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Test_Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Test_Software() {
	# Some definition - menu.
	local var_name;
	local var_type;
	#local var_action; // SC2034 disable.
	#local var_depend; // SC2034 disable.
	# Some definition - next.
	local var_result;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME) var_name="$2"; shift 2 ;;
			-TYPE) var_type="$2"; shift 2 ;;
			-ACTION) shift 2 ;;
			-DEPEND) shift ;;
			*) shift ;;
		esac
	done
	# Tests : type software.
	if [[ "$var_type" == "program" ]]; then
		# TMP : import OverDeploy routine.
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-program";
		fi;
	elif [[ "$var_type" == "service" ]]; then
		# Test : systemd check.
		# SRC : https://stackoverflow.com/a/53640320/2704550.
		if [[ $(systemctl list-units --all --type service --full --no-legend "${var_name}.service" | sed 's/^\s*//g' | cut -f1 -d' ') == "${var_name}.service" ]]; then
			var_result="available";
		else
			var_result="fail-service";
		fi;
	elif [[ "$var_type" == "command" ]]; then
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-command";
		fi;
	else
		# tmp default.
		var_result="fail-unknow";
	fi;
	echo "$var_result";
}


# Start_Software.
#	DESCRIPTION : test effective restarting of a daemon (bash launch) or service.
#	RETURN : code.
function Start_Software() {
	# Some definition - menu.
	local var_name;
	local var_command;
	local var_enable;
	local var_user;
	local var_gui;
	# Some definition - next.
	local var_type;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME) var_name="$2"; shift 2 ;;
			-COMMAND) var_command="$2"; shift 2 ;;
			-ENABLE) var_enable=true; shift ;;
			-USER) var_user="$2"; shift 2 ;;
			-GUI) var_gui=true; shift ;;
			*) shift ;;
		esac
	done;
	# Tests : mandatory.
	if [[ -z "$var_name" && -z "$var_command" ]]; then return 1; fi;
	# Some init.
	if [ -n "$var_name" ]; then
		var_type=$(Get_FileExtension -PATH "$var_name");
	fi;
	# Tests : service || timer || daemon/command.
	if [ -n "$var_type" ] && [[ "$var_type" == "service" || "$var_type" == "timer" ]]; then
		# Test : no services operations wuthout logging.
		if Test_File -PATH "${LOG[MAIN]}" -CREATE; then
			echo "# ===== ${var_name} ===== #" >> "${LOG[MAIN]}";
			# Test : service ready.
			if [ -n "$var_enable" ]; then
				systemctl enable "$var_name" &>> "${LOG[MAIN]}";
			fi;
			# Test : service enabled = restart || start.
			if systemctl is-enabled "$var_name" &> /dev/null; then
				# Test : launch with user.
				if [ -n "$var_user" ]; then
					su -l "$var_user" -c "systemctl restart ${var_name} &>> ${LOG[MAIN]}";
				else
					systemctl restart "$var_name" &>> "${LOG[MAIN]}";
				fi;
			else
				# Test : launch with user.
				if [ -n "$var_user" ]; then
					su -l "$var_user" -c "systemctl start ${var_name} &>> ${LOG[MAIN]}";
				else
					systemctl start "$var_name" &>> "${LOG[MAIN]}";
				fi;
			fi;
		fi;
		# Test : service loaded.
		if systemctl list-units --type="$var_type" --state=loaded | grep -q "$var_name"; then
			return 0;
		else
			return 1;
		fi;
	else
		# Test : launch with user.
		if [ -n "$var_user" ]; then
			# Test : general user interface.
			if [ -n "$var_gui" ]; then xhost + &> /dev/null; fi;
			# Test : command success.
			if su -l "$var_user" bash -c "export DISPLAY=:0 && $var_command &> /dev/null"; then
				# Test : general user interface.
				if [ -n "$var_gui" ]; then xhost - &> /dev/null; fi;
				return 0;
			else
				# Test : general user interface.
				if [ -n "$var_gui" ]; then xhost - &> /dev/null; fi;
				return 1;
			fi;
		else
			# Test : command success.
			if $var_command &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  _______________________________________________
# |                                               | #
# |               distro-tracker.sh               | #
# |_______________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-tracker";
APP["SECTION"]="1";
APP["SHORTNAME"]="DT";
APP["SLOGAN"]="The IT monitoring tool for find out latest stable versions of GNU/Linux & BSD distributions.";
APP["DESCRIPTION"]="This open-source tool analyzes remote content using multiple regular expressions and standardizes the output into CSV, JSON, or YAML files.";
APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory#program yq mandatory";
APP["EXAMPLES"]="all distributions in quiet mode, with Gitlab & Github tokens :|${APP[NAME]} -q -c gitlab:token,github:token#custom distributions, custom length, custom output, with backup :|${APP[NAME]} -d debian,fedora,freebsd,mageia,slackware -l 10 -o distro-tracker -b#mageia distribution in JSON format with custom URLs :|${APP[NAME]} -d mageia -f json -s https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json#get generated scheduled data from distrotracker.com in YAML format :|${APP[NAME]} -g -f yaml";
APP["TODO"]="Flatpak package.";
APP["NOTES"]="'yq' dependencie is 'yq' by Mike Farah.#Some distributions have two general indexes to scan to retrieve the latest versions, with main and archive. Information gived in terminal output.#Fedora lists in the index of a release an 'OSB' version, a version built with osbuilder.#Alma and CentOS can list 'non-existent' versions.#Alma, Fedora & Ubuntu have both ui or cli mode.#Alpine & XCP-ng nead a sort with head because many releases listed in single release page.";
APP["CREDITS"]="Thanks for my friends Mikaël, Jennifer & Priscilia for their support.";
APP["CHANGELOG"]="1.2.0|2025-01-01T10:00:00+0200|Increase to 30 distributions with the following new entries : LibreELEC, OpenMediaVault, Raspberry Pi OS, Rocky Linux, ShredOS & TrueNAS. Move report function to standalone Distro Reporter & Distro Publisher projects. Move get distant content function to Distro Downloader. Replace '--cli' option with new '--mode' option. Quick '-c' option is now for new '--credential' option. Stronger backup protection with recovering if all content is NOK. Fix requirements because cURL & jq always needed.#1.1.1|2024-10-01T10:00:00+0200|Fix **Linux Kernel** regex. Add additional protection that invalidates any result with two entities in the response.#1.1.0|2024-09-19T10:00:00+0200|Increase to 25 distributions with the following new entries : Linux (Kernel) & Rescuezilla. New documentation, with a standards-compliant man file. New NOK filter for OPNsense, and new URLs for openSUSE & Mageia. Added YAML output format, which is accompanied by the addition of the '--format' option to choose between CSV, JSON or YAML - by default only CSV is output. The default value of the '--length' option is now '3'. All options are lowercase. The revision dates in the JSON & YAML output, and in the report, are in standard ISO 8601 format. Fixed execution time calculation.#1.0.0|2024-07-17T10:00:00+0200|Expansion to 22 GNU/Linux distributions with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, and XCP-ng. General mechanism for retrieving version numbers using the '-LENGTH' option has been revised for more logic : not just X tested versions are returned, but now X valid versions. Addition of the '-SOURCE' option to choose tracking URLs via a local or remote JSON file. Addition of the '-REPORT' option to generate a JSON report to notify of any changes in the latest release of each distribution. Activation of the '-ENV' option. Renaming of the '-DISTRIB' option to '-DISTRO', '-DEPTH' to '-LENGTH', '-SECURITY' to '-BACKUP'. Addition of one-letter shortcuts for each option. Addition of the Compare_NokFilter function to better manage and clarify versions tagged 'NOK' (beta, RC, etc.) : all pingable versions are retrieved, with a final filter on stable versions.#0.5.0|2024-06-01T10:00:00+0200|Add -SECURITY option for copy old datas to new datas if wget fails on entire website.#0.4.0|2024-06-01T09:00:00+0200|Up to 15 distributions by default.#0.3.0|2024-06-01T08:00:00+0200|New strong REGEXP. Add '-LENGTH' option. Add command JQ requirement. Add JSON output. Up to 12 distributions GNU/Linux & BSD.#0.2.0|2024-06-01T07:00:00+0200|Separate checks on index page & release page.#0.1.0|2024-06-01T06:00:00+0200|Add CSV output.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.2.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#optionnal#Single local file path or single distant url to JSON data source for distributions URLs.|path. example at 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json'. CSV & YAML formats available in a next release.|no default, hardcoded URLs are the source until this option is provisioned.";
OPTIONS["CREDENTIAL"]="2#c#CREDENTIAL#optionnal#Login/ID & password/token of source, delimited by ':', in comma-separated values.|string. example 'service:credential,service:credential'.|no default.";
OPTIONS["DISTRO"]="3#d#NAMES#optionnal#Single distribution name or coma separated list, without spaces, of distributions names.|available distributions names are 'alma', 'alpine', 'arch', 'centos', 'clonezilla', 'debian', 'fedora', 'freebsd', 'kaisen', 'kali', 'libreelec', 'linux', 'mageia', 'manjaro', 'mint', 'mx', 'openmediavault', 'opensuse', 'opnsense', 'parrot', 'pfsense', 'proxmox', 'raspios', 'rescuezilla', 'rocky', 'shredos', 'slackware', 'truenas', 'ubuntu' & 'xcpng', or 'all' registered.|'all'.";
OPTIONS["MODE"]="4#m#null#optionnal#Choose Command-Line Interface only or Graphical User Interface for distribution if available.|'cli' or 'ui'.|'ui.";
OPTIONS["LENGTH"]="5#l#NUMBER#optionnal#Number of valid releases by distribution needed.|any number starting from 1.|'3'.";
OPTIONS["OUTPUT"]="6#o#FILEPATH#optionnal#Single file path without extension to store the final result. If provisioned it's custom name mode, if not it's date name mode.|path.|'PWD/${APP[SHORTNAME]}-DATETIME.FORMAT' (date name mode).";
OPTIONS["FORMAT"]="7#f#EXTENSION#optionnal#Single data format for previous '--output' option.|'csv', 'json', 'yaml' or 'all'.|'csv'.";
OPTIONS["BACKUP"]="8#b#null#optionnal#Backup security for keeping and don't removing previous data if entire website checking fails. New and old datas need to be in the same directory. CSV & YAML formats available in a next release.|null|'false' because disabled.";
OPTIONS["QUIET"]="9#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="10#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="11#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--credential|-c)
			PARAM["CREDENTIAL"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -CONTAINS ":"
			shift 2
			;;
		--distro|-d)
			PARAM["DISTRO"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--mode|-m)
			PARAM["MODE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--length|-l)
			PARAM["LENGTH"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "int"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--format|-f)
			PARAM["FORMAT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--backup|-b)
			PARAM["BACKUP"]=true
			shift
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================== #
#          DISTRO          #
#
#	DESCRIPTION : the source.

declare -gA DISTRO=(
	["alma"]="1,AlmaLinux OS,uicli,https://repo.almalinux.org/almalinux,https://repo.almalinux.org/almalinux/xxx/isos/x86_64,null,https://vault.almalinux.org/xxx/isos/x86_64"
	["alpine"]="2,Alpine Linux,cli,https://dl-cdn.alpinelinux.org/alpine,https://dl-cdn.alpinelinux.org/alpine/xxx/releases/x86_64,null,null"
	["arch"]="3,Arch Linux,ui,https://geo.mirror.pkgbuild.com/iso,https://geo.mirror.pkgbuild.com/iso/xxx,null,null"
	["centos"]="4,CentOS Stream,ui,https://mirror.in2p3.fr/pub/linux/centos-stream,https://mirror.in2p3.fr/pub/linux/centos-stream/xxx/BaseOS/x86_64/iso,https://mirror.in2p3.fr/pub/linux/centos-vault,https://mirror.in2p3.fr/pub/linux/centos-vault/xxx/isos/x86_64"
	["clonezilla"]="5,Clonezilla,ui,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable/xxx,null,null"
	["debian"]="6,Debian,ui,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/mirror/cdimage/archive,https://cdimage.debian.org/mirror/cdimage/archive/xxx/amd64/iso-dvd"
	["fedora"]="7,Fedora Linux,uicli,https://mirror.in2p3.fr/pub/fedora/linux/releases,https://mirror.in2p3.fr/pub/fedora/linux/releases/xxx/Workstation/x86_64/iso,null,https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/xxx/Workstation/x86_64/iso"
	["freebsd"]="8,FreeBSD,ui,https://download.freebsd.org/releases/ISO-IMAGES,https://download.freebsd.org/releases/ISO-IMAGES/xxx,null,null"
	["kaisen"]="9,Kaisen Linux,ui,https://iso.kaisenlinux.org/rolling,https://iso.kaisenlinux.org/rolling,null,null"
	["kali"]="10,Kali Linux,ui,https://cdimage.kali.org,https://cdimage.kali.org/kali-xxx,null,null"
	["libreelec"]="11,LibreELEC,ui,https://libreelec.tv/downloads/generic,https://releases.libreelec.tv,null,null"
	["linux"]="12,Linux Kernel,cli,https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x,https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x,null,null"
	["mageia"]="13,Mageia,ui,https://mirrors.kernel.org/mageia/iso,https://mirrors.kernel.org/mageia/iso/xxx/Mageia-xxx-x86_64,null,null"
	["manjaro"]="14,Manjaro Linux,ui,https://manjaro.org/products/download/x86,https://download.manjaro.org/kde/xxx,null,null"
	["mint"]="15,Linux Mint,ui,https://mirrors.edge.kernel.org/linuxmint/stable,https://mirrors.edge.kernel.org/linuxmint/stable/xxx,null,null"
	["mx"]="16,MX Linux,ui,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Old,https://sourceforge.net/projects/mx-linux/files/Old/MX-xxx/KDE"
	["openmediavault"]="17,OpenMediaVault,cli,https://sourceforge.net/projects/openmediavault/files/iso,https://sourceforge.net/projects/openmediavault/files/iso/xxx,null,null"
	["opensuse"]="18,openSUSE (Leap),ui,https://download.opensuse.org/distribution/leap/?jsontable,https://download.opensuse.org/distribution/leap/xxx/iso/?jsontable,null,null"
	["opnsense"]="19,OPNsense,cli,https://mirror.wdc1.us.leaseweb.net/opnsense/releases,https://mirror.wdc1.us.leaseweb.net/opnsense/releases/xxx,null,null"
	["parrot"]="20,Parrot Security,ui,https://deb.parrot.sh/parrot/iso,https://deb.parrot.sh/parrot/iso/xxx,null,null"
	["pfsense"]="21,pfSense,cli,https://atxfiles.netgate.com/mirror/downloads,https://atxfiles.netgate.com/mirror/downloads,null,null"
	["proxmox"]="22,Proxmox,cli,https://enterprise.proxmox.com/iso,https://enterprise.proxmox.com/iso,null,null"
	["raspios"]="23,Raspberry Pi OS,uicli,https://downloads.raspberrypi.com/raspios_arm64/images,https://downloads.raspberrypi.com/raspios_arm64/images/xxx,null,null"
	["rescuezilla"]="24,Rescuezilla,ui,https://api.github.com/repos/rescuezilla/rescuezilla/releases,https://github.com/rescuezilla/rescuezilla/releases/download/xxx,null,null"
	["rocky"]="25,Rocky Linux,uicli,https://download.rockylinux.org/pub/rocky,https://download.rockylinux.org/pub/rocky/xxx/isos/x86_64,https://dl.rockylinux.org/vault/rocky,https://dl.rockylinux.org/vault/rocky/xxx/isos/x86_64"
	["shredos"]="26,ShredOS,cli,https://api.github.com/repos/partialvolume/shredos.x86_64/releases,https://github.com/PartialVolume/shredos.x86_64/releases/download/xxx,null,null"
	["slackware"]="27,Slackware,ui,https://mirrors.slackware.com/slackware/slackware-iso,https://mirrors.slackware.com/slackware/slackware-iso/slackware64-xxx-iso,null,null"
	["truenas"]="28,TrueNAS (Community Edition),cli,https://download.sys.truenas.net/TrueNAS-SCALE-ElectricEel,https://download.sys.truenas.net/TrueNAS-SCALE-ElectricEel/xxx,null,null"
	["ubuntu"]="29,Ubuntu,uicli,https://releases.ubuntu.com,https://releases.ubuntu.com/xxx,null,null"
	["xcpng"]="30,XCP-ng,cli,https://updates.xcp-ng.org/isos,https://updates.xcp-ng.org/isos/xxx,null,null"
);
# Test : sort DISTRO.
if ! Sort_Array -NAME "DISTRO" -DELIMITER ","; then Stop_App -CODE 1 -TEXT "'DISTRO' array parsing failed."; fi;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some default.
declare -gA DEFAULT;
# Loop : distro.
for var_default_distro_index in $(echo "${!DISTRO_SORT[@]}" | tr ' ' '\n' | sort -n); do
	DEFAULT["DISTRO"]+=",${DISTRO_SORT[$var_default_distro_index]}";
done;
DEFAULT["DISTRO"]="${DEFAULT["DISTRO"]#,}";
# DEFAULT["DISTRO"] is a string, need array for Search_InArray custom PARAM["DISTRO"].
IFS=',' read -ra var_default_distro_array <<< "${DEFAULT["DISTRO"]}";
Restore_IFS;
DEFAULT["MODE"]="ui";
DEFAULT["LENGTH"]="3";
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}${APP[SHORTNAME]}-${DATE[DATETIME1]}";
DEFAULT["FORMAT"]="csv";
DEFAULT["BACKUP"]=false;
DEFAULT["QUIET"]=false;
# Some script definition.
# SOURCE.
if [ -z "${PARAM[SOURCE]}" ]; then
	# Loop : indexlist distro.
	for var_param_source_key in "${!DISTRO[@]}"; do
		var_param_source_value="${DISTRO[$var_param_source_key]}";
		Add_Book "DISTRO" "$var_param_source_key" "$var_param_source_value";
	done;
else
	# Custom book DISTRO.
	# Test : locally.
	if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]]; then
		var_data_distro="$(Get_Distant -URL "${PARAM[SOURCE]}" -TYPE "string")";
	else
		if Test_File -PATH "${PARAM[SOURCE]}"; then
			var_data_distro="$(<"${PARAM[SOURCE]}")";
		else
			Stop_App -CODE 1 -TEXT "File in '--source' option is not available.";
		fi;
	fi;
	# Test : data exists.
	if [[ "$var_data_distro" != "" ]]; then
		var_data_index="0";
		# Tests : CSV, JSON, YAML.
		var_data_format=$(Get_DataFormat -DATA "$var_data_distro");
		if [[ "$var_data_format" == "csv" ]]; then
			# Loop : Process Substitution.
			while IFS=',' read -r var_param_file_distro var_fullname var_mode var_current_index var_current_release var_archive_index var_archive_release; do
				# Test : invalid distribution.
				if Search_InArray -PATTERN "$var_param_file_distro" -DATA "${var_default_distro_array[*]}"; then
					var_param_file_values="$var_fullname,$var_mode,$var_current_index,$var_current_release,$var_archive_index,$var_archive_release";
					Add_Book "DISTRO" "$var_param_file_distro" "${var_data_index},${var_param_file_values}";
					# Test : emergency work.
					if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
						for key in "${!DISTRO[@]}"; do echo "key '$key' - value '${DISTRO[$key]}'"; done;
					fi;
					# Increment position.
					((var_data_index++));
				else
					Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_file_distro' from CSV.";
				fi
			done < <(echo "$var_data_distro");
		elif [[ "$var_data_format" == "json" ]]; then
			# Loop : Process Substitution.
			while read -r var_data_distro_array; do
				var_param_file_distro=$(echo "$var_data_distro_array" | jq -c -r '.name');
				var_param_file_values=$(echo "$var_data_distro_array" | jq -c -r '[.fullname, .mode, .current_index, .current_release, .archive_index, .archive_release] | join(",")');
				# Test : invalid distribution.
				if Search_InArray -PATTERN "$var_param_file_distro" -DATA "${var_default_distro_array[*]}"; then
					Add_Book "DISTRO" "$var_param_file_distro" "${var_data_index},${var_param_file_values}";
					# Test : emergency work.
					if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
						for key in "${!DISTRO[@]}"; do echo "key '$key' - value '${DISTRO[$key]}'"; done;
					fi;
					# Increment position.
					((var_data_index++));
				else
					Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_file_distro' from JSON data.";
				fi;
			done < <(echo "$var_data_distro" | jq -c -r '.distro[]');
		elif [[ "$var_data_format" == "yaml" ]]; then
			# Loop : Process Substitution.
			while read -r var_data_distro_name; do
				var_data=$(echo "$var_data_distro" | yq eval ".distro[] | select(.name == \"$var_data_distro_name\")");
				var_param_file_distro="$var_data_distro_name";
				var_param_file_values="$(echo "$var_data" | yq eval '[.fullname, .mode, .current_index, .current_release, .archive_index, .archive_release] | join(",")')";
				
				# TODO : keep null values !
				
				# Test : invalid distribution.
				if Search_InArray -PATTERN "$var_param_file_distro" -DATA "${var_default_distro_array[*]}"; then
					Add_Book "DISTRO" "$var_param_file_distro" "${var_data_index},${var_param_file_values}";
					# Test : emergency work.
					if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
						for key in "${!DISTRO[@]}"; do echo "key '$key' - value '${DISTRO[$key]}'"; done;
					fi;
					# Increment position.
					((var_data_index++));
				else
				Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_file_distro' from YAML data.";
				fi;
			done < <(echo "$var_data_distro" | yq '.distro[].name');
		else
			Stop_App -CODE 1 -TEXT "Format in '--source' option can't be defined.";
		fi;
	else
		# Test : locally.
		if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]]; then
			Stop_App -CODE 1 -TEXT "Distant file in '--source' option has no data.";
		else
			Stop_App -CODE 1 -TEXT "Local file in '--source' option has no data.";
		fi;
	fi;
fi;
# CREDENTIAL.
if [ -n "${PARAM[CREDENTIAL]}" ]; then
	Set_Book -NAME "CREDENTIAL";
fi;
# DISTRO.
if [ -z "${PARAM[DISTRO]}" ] || [[ "${PARAM[DISTRO]}" == "all" ]]; then
	PARAM["DISTRO"]="${DEFAULT[DISTRO]}";
else
	PARAM["DISTRO"]="$(echo "${PARAM["DISTRO"]}" | tr ',' '\n' | sort | tr '\n' ',')";
fi;
IFS=',' read -ra var_param_distro_array <<< "${PARAM["DISTRO"]}";
Restore_IFS;
# Loop : distribution.
for var_param_distro_array_element in "${var_param_distro_array[@]}"; do
	# Test : invalid distribution.
	if ! Search_InArray -PATTERN "$var_param_distro_array_element" -DATA "${var_default_distro_array[*]}"; then
		Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_distro_array_element'.";
	fi;
done;
# MODE.
if [ -z "${PARAM[MODE]}" ]; then PARAM["MODE"]="${DEFAULT[MODE]}"; fi;
# LENGTH.
if [ -z "${PARAM[LENGTH]}" ]; then PARAM["LENGTH"]="${DEFAULT[LENGTH]}"; fi;
# OUTPUT + OUTPUT_BACKUP.
if [ -z "${PARAM[OUTPUT]}" ]; then
	# Loop : last file win previous data state.
	for var_param_output_single_file in "${PWD}${CONF[SEPARATOR]}${APP[SHORTNAME]}"*.csv; do
		PARAM["OUTPUT_BACKUP"]="$var_param_output_single_file";
	done;
	PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}.csv";
else
	PARAM["OUTPUT_BACKUP"]="${PARAM[OUTPUT]%[\\/]}.archive.csv";
	PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}.current.csv";
fi;
# FORMAT - Redefine requirements for user-friendly work.
if [ -z "${PARAM[FORMAT]}" ]; then
	PARAM["FORMAT"]="${DEFAULT[FORMAT]}";
	APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory";
elif [[ "${PARAM[FORMAT]}" == "json" ]]; then
	APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory";
elif [[ "${PARAM[FORMAT]}" == "yaml" ]]; then
	APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory#program yq mandatory";
elif [[ ! "${PARAM[FORMAT]}" =~ ^(csv|json|yaml|all)$ ]]; then
	Stop_App -CODE 1 -TEXT "Unknown format : '${PARAM[FORMAT]}'.";
fi;
# BACKUP.
if [ -z "${PARAM[BACKUP]}" ]; then PARAM["BACKUP"]="${DEFAULT[BACKUP]}"; fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Get_AllVersion.
#	DESCRIPTION : parse HTML index page with some regex.
#	RETURN : string.
function Get_AllVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_data_current;
	local var_data_archive;
	local var_parse;
	local var_parse_current;
	local var_parse_archive;
	local var_array_release;
	local var_array_sorted_release;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_release");
	# Tests : construct HTML data.
	if [[ "$var_distribution_data_current_index" == *"api.github.com"* ]]; then
		# Test : Github auth is recommended.
		if [[ -n "${CREDENTIAL[GITHUB]}" ]]; then
			var_data_current=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index" -CREDENTIAL "${CREDENTIAL[GITHUB]}");
			if [[ "$var_distribution_data_archive_index" != "" && "$var_distribution_data_archive_index" != "null" ]]; then
				var_data_archive=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_archive_index" -CREDENTIAL "${CREDENTIAL[GITHUB]}");
			fi;
		else
			var_data_current=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index");
			if [[ "$var_distribution_data_archive_index" != "" && "$var_distribution_data_archive_index" != "null" ]]; then
				var_data_archive=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_archive_index");
			fi;
		fi;
	else
		var_data_current=$(Get_Distant -METHOD "wget" -URL "$var_distribution_data_current_index" -TYPE "string");
		if [[ "$var_distribution_data_archive_index" != "" && "$var_distribution_data_archive_index" != "null" ]]; then
			var_data_archive=$(Get_Distant -METHOD "wget" -URL "$var_distribution_data_archive_index" -TYPE "string");
		fi;
	fi
	# Tests : construct parsing for the distribution.
	if [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS.
		#
		var_parse_current=$(Get_AllVersionGeneric -DATA "$var_data_current");
		var_parse_archive=$(Get_AllVersionGeneric -DATA "$var_data_archive");
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" =~ ^(clonezilla|openmediavault)$ ]]; then
		#
		# CLONEZILLA, OPENMEDIAVAULT.
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '<a[^>]*href="[^"]*.iso"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | grep -Po '(?<=href=")[^?"]*' | sed 's/debian-//g' | sed 's/-amd64-DVD-1.iso//g' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/"[A-Za-z][^"]*"/""/g' | sed 's/<a href="[^"]*-live">//g' | grep -Po '(?<=href=")[^?"]*' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "libreelec" ]]; then
		#
		# LIBREELEC.
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<h5 class="card-title">[^<]*</h5>' | sed "s/<[^>]*>//g" | sed 's/ //g' | sed 's/LibreELEC//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '(?<=/MX-)[^"]+(?=_KDE_x64.iso/download")' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | sed 's/MX-//g' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "opensuse" ]]; then
		#
		# OPENSUSE.
		#
		var_parse=$(echo "$var_data_current" | jq -r '.data[] | select(.name != "42.3/") | .name | rtrimstr("/")' | tr '\n' ',');
	elif [[ "$var_distribution" =~ ^(rescuezilla|shredos)$ ]]; then
		#
		# RESCUEZILLA & SHREDOS (Github).
		#
		var_parse=$(echo "$var_data_current" | jq -r '.[] | select((.tag_name | contains("rolling") | not) and (.tag_name | contains("rc") | not) and (.tag_name | contains("beta") | not)) | .tag_name | ltrimstr("v")' 2>/dev/null | sed 's/_x86-64_.*//g' | tr '\n' ',') || var_parse="";
	elif [[ "$var_distribution" =~ ^(kaisen|linux|manjaro|pfsense|proxmox)$ ]]; then
		#
		# KAISEN, LINUX (KERNEL), MANJARO, PFSENSE, PROXMOX.
		#
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="(kaisenlinuxrolling|linux-|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)[^"]*(-amd64-MATE.iso|\d+\.tar.gz|manjaro-kde-|-RELEASE-amd64.iso.gz|\d+\.iso)"' | sed 's/href="//g' | sed 's/"//g' | sed -E 's/(kaisenlinuxrolling|linux-|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)//g' | sed -E 's/(-amd64-MATE.iso|.tar.gz|\/manjaro-kde-.*.iso|-RELEASE-amd64.iso.gz|.iso)//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "truenas" ]]; then
		#
		# TRUENAS.
		#
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<section.*</section>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/\.\.//g' | sed 's/\.\///g' | sed 's/\///g' | sed 's/"[^0-9][^"]*"//g' | grep -Po '(?<=href=")[^?"]*' | sed 's/?wrap=1//g' | tr '\n' ',');
	else
		#
		# GENERIC.
		#
		var_parse=$(Get_AllVersionGeneric -DATA "$var_data_current");
	fi;
	# Test : data exists & NO SPACE ALLOWED.
	if [[ -n "$var_parse" && "$var_parse" != "" && "$var_parse" != "null" && ! "$var_parse" =~ " " ]]; then
		# Loop : construct & sort by most recent versions.
		IFS=',' read -ra var_array_release <<< "$var_parse";
		mapfile -t var_array_sorted_release < <(printf "%s\n" "${var_array_release[@]}" | sort -Vr);
		# Construct return.
		var_result=("$(IFS=,; echo "${var_array_sorted_release[*]}")");
		Restore_IFS;
		echo "${var_result[@]}";
	else
		echo "";
	fi;
}

# Get_AllVersionGeneric.
#	DESCRIPTION : parse some HTML index page with single regex.
#	RETURN : string.
function Get_AllVersionGeneric() {
	# Some definition.
	local var_data;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	var_result=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<a href="[^"]*">Parent Directory<\/a>//g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/<a href="RPM-[^"]*">//g' | sed 's/<a href="almalinux-[^"]*">//g' | sed 's/<a href="slackware-[^"]*">//g' | grep -Po '(?<=href=")[^?"]*' | sed "s/\\bkali-//g" | sed "s/\\bv//g" | sed "s/\\bslackware64-//g" | sed "s/-iso\\b//g" | sed 's/raspios_arm64-//g' | tr '\n' ',');
	echo "$var_result";
}

# Get_SingleVersion.
#	DESCRIPTION : parse HTML release page with some regex.
#	RETURN : string.
function Get_SingleVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_version;
	local var_position;
	local var_url;
	local var_filename;
	local var_filedate;
	local var_date;
	local var_data;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			-POSITION)
				var_position="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_release");
	# Tests : construct data.
	if [[ "$var_distribution_data_current_index" == *"api.github.com"* ]]; then
		# Test : Github auth is recommended.
		if [[ -n "${CREDENTIAL[GITHUB]}" ]]; then
			var_data=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index" -CREDENTIAL "${CREDENTIAL[GITHUB]}");
		else
			var_data=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index");
		fi;
	fi;
	# Test : construct pre-built url & date.
	var_url="${var_distribution_data_current_release//xxx/$var_version}";
	# Tests : construct return filename:url.
	if [[ "$var_distribution" == "alma" ]]; then
		#
		# ALMA - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"isos/"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		fi;
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-minimal.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-dvd.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "alpine" ]]; then
		#
		# ALPINE - many releases by release page (need sort).
		#
		var_url="${var_distribution_data_current_release//xxx/v${var_version}}";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-standard-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		fi;
	elif [[ "$var_distribution" == "arch" ]]; then
		#
		# ARCH - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"iso"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-LiveDVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-netinstall.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*latest-x86_64-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "clonezilla" ]]; then
		#
		# CLONEZILLA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-amd64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed "s/.*\/$var_version\///g" | sed 's/\/download//g');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN - one release by release page.
		# remember to match DVD-1 pattern with old releases.
		#
		# Test : fix debian separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="indexlist">(.*?)</table>' | grep -Po '<td class="indexcolname">(.*?)</td>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"debian-${var_version}-amd64-DVD-1.iso\"" | sed 's/href="//g' | sed 's/"//g' | head -n 1);
	elif [[ "$var_distribution" == "fedora" ]]; then
		#
		# FEDORA - one release by release page, uicli.
		# remember to not match -osb- pattern.
		#
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_url="${var_url//Workstation/Server}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_url="${var_url//Workstation/Server}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Server-dvd-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Workstation-Live-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-Live-Workstation-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_url="${var_url//$var_version\/Workstation\/x86_64\/iso/$var_version\/Fedora\/x86_64\/iso}";
					var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
					var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-[^"]*-x86_64-DVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		fi;
	elif [[ "$var_distribution" == "freebsd" ]]; then
		#
		# FREEBSD - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"FreeBSD-${var_version}-RELEASE-amd64-dvd1.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kaisen" ]]; then
		#
		# KAISEN - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}[^\"]*-amd64-MATE.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kali" ]]; then
		#
		# KALI - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="kali-linux-[^"]*-installer-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "libreelec" ]]; then
		#
		# LIBREELEC - all releases in one page (need sort).
		#
		var_url="$var_distribution_data_current_index";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*LibreELEC-[^\"]*${var_version}[^\"]*.img.gz\"" | sed 's/href="//g' | sed 's/"//g' | sed 's/https:\/\/[^\/]*\///g' | sort -Vr | head -n 1);
		var_url="$var_distribution_data_current_release";
	elif [[ "$var_distribution" == "linux" ]]; then
		#
		# LINUX - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-${var_version}.tar.gz\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mageia" ]]; then
		#
		# MAGEIA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "manjaro" ]]; then
		#
		# MANJARO - one release in one page.
		#
		var_data=$(Get_Distant -URL "$var_distribution_data_current_index" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}/manjaro-kde-[^\"]*.iso\"" | sed 's/href="//g' | sed 's/"//g' | sed 's/.*\///g');
	elif [[ "$var_distribution" == "mint" ]]; then
		#
		# MINT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*cinnamon-64bit.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX - one release by release page.
		#
		# Test : fix mx separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*_x64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed 's/.*KDE\///g' | sed 's/\/download//g');
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_url="${var_url//\/KDE/}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*MX-${var_version}_x64.iso/download\"" | sed 's/href="//g' | sed 's/"//g' | sed "s/.*MX-${var_version}\///g" | sed 's/\/download//g');
		fi;
	elif [[ "$var_distribution" == "openmediavault" ]]; then
		#
		# OPENMEDIAVAULT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-amd64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed "s/.*\/$var_version\///g" | sed 's/\/download//g');
	elif [[ "$var_distribution" == "opensuse" ]]; then
		#
		# OPENSUSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | jq -r '.data[] | select((.name | endswith("-DVD-x86_64-Media.iso"))) | .name');
		var_url="${var_url//\/\?jsontable/}";
	elif [[ "$var_distribution" == "opnsense" ]]; then
		#
		# OPNSENSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po "<table.*</table>" | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso.bz2"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "parrot" ]]; then
		#
		# PARROT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-security-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "pfsense" ]]; then
		#
		# PFSENSE - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-${var_version}-[^\"]*.iso.gz\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "proxmox" ]]; then
		#
		# PROXMOX - all releases in one page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"proxmox-ve_${var_version}.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "raspios" ]]; then
		#
		# RASPBERRY PI OS - one release by release page.
		#
		#
		var_url="${var_distribution_data_current_release//xxx/raspios_arm64-${var_version}}";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-raspios-[^\"]*\.(img\.xz|zip)\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "rescuezilla" ]]; then
		#
		# RESCUEZILLA - all releases in one page (Github).
		#
		#
		var_filename=$(echo "$var_data" | jq -r '.[] | .assets | map(select(.browser_download_url | test(".*rescuezilla-'"${var_version}"'-64bit\\..*\\.iso$"))) | last | .browser_download_url | select(. != null)' | sed "s/https:\/\/github.com\/rescuezilla\/rescuezilla\/releases\/download\/${var_version}\///g");
		var_url="${var_distribution_data_current_release//xxx/${var_version}}";
	elif [[ "$var_distribution" == "rocky" ]]; then
		#
		# ROCKY - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"isos/"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		fi;
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Rocky-[^"-]*-x86_64-minimal.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Rocky-[^"-]*-x86_64-dvd1?.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "shredos" ]]; then
		#
		# SHREDOS - all releases in one page (Github).
		#
		#
		var_filename=$(echo "$var_data" | jq -r '.[] | .assets | map(select(.browser_download_url | test("shredos-'"${var_version}"'_x86-64_.*\\.iso$|shredos-'"${var_version}"'_x86-64_.*\\.img$"))) | last | .browser_download_url | select(. != null)' | grep -Po '[^/]+$');
		var_filedate=$(echo "$var_filename" | sed 's/shredos-//g' | sed 's/_[^_]*$//g');
		var_url="${var_distribution_data_current_release//xxx/v${var_filedate}}";
	elif [[ "$var_distribution" == "slackware" ]]; then
		#
		# SLACKWARE - one release by release page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="slackware64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "truenas" ]]; then
		#
		# TRUENAS.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<section.*</section>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/?wrap=1//g' | grep -Po 'href="[^"]*.iso"' | sed 's/.\///g' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "ubuntu" ]]; then
		#
		# UBUNTU - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-live-server-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-desktop-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "xcpng" ]]; then
		#
		# XCP-NG - many releases by release page (need sort).
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"xcp-ng-${var_version}.([0-9]-[0-9]|[0-9]|[0-9]-[a-z]+[0-9]).iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
	fi;
	# Test : get ERR_INV error explicitly.
	if [[ -z "$var_filename" || "$var_filename" == "" || "$var_filename" == *$'\n'* || ${#var_filename} -gt 100 ]]; then
		var_filename="ERR_INV";
		var_date="ERR_INV";
	else
		var_date=$(Get_SingleVersionDateGeneric -DATA "$var_data" -FILENAME "$var_filename" -VERSION "$var_version");
	fi;
	# Construct return filename;url;date.
	echo "$var_filename;$var_url/$var_filename;$var_date";
}

# Get_SingleVersionDateGeneric.
#	DESCRIPTION : convert some date formats to ISO 8601.
#	RETURN : string.
function Get_SingleVersionDateGeneric() {
	# Some definition.
	local var_data;
	local var_filename;
	declare -A var_date_map;
	local var_date;
	local var_date_day;
	local var_date_month;
	local var_date_year;
	declare -a var_patterns;
	local var_pattern;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : rolling.
	if [[ "$var_filename" =~ ^manjaro-.* ]]; then
		var_result="rolling";
	else
		# BASH_REMATCH.
		var_date_map=( ["Jan"]="01" ["Feb"]="02" ["Mar"]="03" ["Apr"]="04" ["May"]="05" ["Jun"]="06" ["Jul"]="07" ["Aug"]="08" ["Sep"]="09" ["Oct"]="10" ["Nov"]="11" ["Dec"]="12" );
		# Date patterns to test : 'DD-MMM-YYYY', 'YYYY-MM-DD', 'YYYY-MMM-DD'.
		# 'DD-MMM-YYYY' = alma, alpine, arch, linux, mageia, mint, parrot, pfsense, proxmox, slackware, xcpng.
		# 'YYYY-MM-DD' = centos, clonezilla, debian, fedora, kaisen, mx, openmediavault, opensuse, raspios, ubuntu.
		# 'YYYY-MMM-DD' = freebsd, kali, opnsense.
		var_patterns=('\d{2}-[A-Za-z]{3}-\d{4}' '\d{4}-\d{2}-\d{2}' '\d{4}-[A-Za-z]{3}-\d{2}');
		# Loop : patterns.
		for var_pattern in "${var_patterns[@]}"; do
			# Remove <td>, <span></span>, title, all inside <a>XXX</a>, and get date.
			var_date=$(echo "$var_data" | grep -Po "<body.*</body>" | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title=\"[^\"]*\"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*${var_pattern}" | sed "s/${var_filename}//g" | grep -Po "${var_pattern}");
			# Test : handle different formats.
			if [[ "$var_date" =~ ^([0-9]{2})-([A-Za-z]{3})-([0-9]{4})$ ]]; then
				var_date_day=${BASH_REMATCH[1]};
				var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
				var_date_year=${BASH_REMATCH[3]};
				var_result="${var_date_year}-${var_date_month}-${var_date_day}";
				break;
			elif [[ "$var_date" =~ ^([0-9]{4})-([0-9]{2})-([0-9]{2})$ ]]; then
				var_date_year=${BASH_REMATCH[1]};
				var_date_month=${BASH_REMATCH[2]};
				var_date_day=${BASH_REMATCH[3]};
				var_result="${var_date_year}-${var_date_month}-${var_date_day}";
				break;
			elif [[ "$var_date" =~ ^([0-9]{4})-([A-Za-z]{3})-([0-9]{2})$ ]]; then
				var_date_year=${BASH_REMATCH[1]};
				var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
				var_date_day=${BASH_REMATCH[3]};
				var_result="${var_date_year}-${var_date_month}-${var_date_day}";
				break;
			fi;
		done;
		# Test : HTML alternate solution.
		# 'YYYY.MM' = libreelec.
		if [[ "$var_result" == "" ]]; then
			var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po "<h5 class=\"[^\"]*\">[^<]*</h5>[^<]*<div class=\"[^\"]*\">[^<]*<h5 class=\"[^\"]*\">LibreELEC ${var_version}</h5>" | grep -Po '\([^()]*\)' | sed 's/(//g' | sed 's/)//g');
			# Test : date exists.
			if [[ -n "$var_date" ]]; then
				var_result="${var_date}-01";
				var_result="${var_result//./-}";
			fi;
		fi;
		# Test : JSON 1st solution.
		# 'YYYY-MM-DD' = rescuezilla.
		if [[ "$var_result" == "" ]]; then
			var_date=$(echo "$var_data" | jq -r 'try (.[] | select((.tag_name | contains("rolling") | not) and (.tag_name | contains("rc") | not) and (.tag_name | contains("beta") | not)) | select((.tag_name | ltrimstr("v")) == "'"${var_version}"'") | .created_at) // empty' 2>/dev/null);
			# Test : date exists.
			if [[ -n "$var_date" && "$var_date" =~ T ]]; then
				var_result=$(echo "$var_date" | cut -d'T' -f1);
			fi;
		fi;
		# Test : JSON 2nd solution.
		# 'YYYY-MM-DD' = shredos.
		if [[ "$var_result" == "" ]]; then
			# Redefine version from filename.
			var_version=$(echo "$var_filename" | sed 's/shredos-//g' | sed 's/_[0-9]\+\.iso//g');
			var_date=$(echo "$var_data" | jq -r 'try (.[] | select((.tag_name | contains("rolling") | not) and (.tag_name | contains("rc") | not) and (.tag_name | contains("beta") | not)) | select((.tag_name | ltrimstr("v")) == "'"${var_version}"'") | .created_at) // empty' 2>/dev/null);
			# Test : date exists.
			if [[ -n "$var_date" && "$var_date" =~ T ]]; then
				var_result=$(echo "$var_date" | cut -d'T' -f1);
			fi;
		fi;
		# Test : JSON 3rd solution.
		# 'timestamp' = opensuse.
		if [[ "$var_result" == "" ]]; then
			var_date=$(echo "$var_data" | jq -r 'try (.data[] | select(.name == "'"${var_filename}"'") | .mtime | strftime("%Y-%m-%d")) // empty' 2>/dev/null);
			# Test : date exists.
			if [[ -n "$var_date" ]]; then var_result="$var_date"; fi;
		fi;
		# Tests : fail.
		if [[ ! "$var_result" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
			var_result="ERR_INV";
		elif [ -z "$var_result" ]; then
			var_result="ERR_INV";
		fi;
	fi;
	# Output result.
	echo "$var_result";
}

# Compare_NokFilter.
#	DESCRIPTION : filter release by nok patterns.
#	RETURN : string or code.
function Compare_NokFilter() {
	# Some definition.
	local var_distribution;
	local var_release;
	local var_filename;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DISTRO)
				var_distribution="$2"
				shift 2
				;;
			-RELEASE)
				var_release="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : nok & specific invalid.
	case "$var_distribution" in
		alpine)
			[[ "$var_filename" == *"_rc"* ]] && var_result="nok-rc"
			;;
		alma)
			if [[ "$var_release" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_release" =~ ^[0-9]$ ]]; then
				var_result="nok-version"
			fi
			;;
		debian)
			if [[ "$var_release" == *"_r"* ]]; then
				var_result="nok-rc"
			elif [[ "$var_release" == *"-"* ]]; then
				var_result="nok-version"
			fi
			;;
		freebsd)
			[[ "$var_filename" == *"RC"* ]] && var_result="nok-rc"
			;;
		kaisen)
			[[ "$var_release" == *"RC"* ]] && var_result="nok-rc"
			;;
		openmediavault)
			[[ "$var_filename" == *"beta"* ]] && var_result="nok-beta"
			;;
		opnsense)
			if [[ "$var_filename" == *"-devel-"* || "$var_filename" =~ \.b[0-9] ]]; then
				var_result="nok-beta"
			elif [[ "$var_filename" =~ \.r[0-9]- ]]; then
				var_result="nok-rc"
			fi
			;;
		parrot)
			[[ "$var_release" == *"-beta"* ]] && var_result="nok-beta"
			;;
		ubuntu)
			[[ "$var_filename" == *"-beta-"* ]] && var_result="nok-beta"
			;;
		xcpng)
			if [[ "$var_filename" == *"-alpha"* ]]; then
				var_result="nok-alpha"
			elif [[ "$var_filename" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_filename" == *"-rc"* ]]; then
				var_result="nok-rc"
			fi
			;;
	esac
	# Test : string, code.
	if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
}

# Convert_CSVToJSON.
#	DESCRIPTION : data CSV to data JSON.
function Convert_CSVToJSON() {
	# Some definition.
	local var_data_imported;
	local var_data_rewrited;
	# Import & rewrite.
	var_data_imported=$(jq -R --tab '{
	revision: "'"${DATE[DATETIME0]}"'",
	distro: [
		inputs | split(",") | {
			name: .[0],
			fullname: .[1],
			source: .[10],
			tmp: {
				all: .[4] | split(" ") | map({version: .}),
				valid: .[5] | split(" ") | map({version: .}),
				nok: .[6] | split(" ") | map({version: .})
			},
			releases: {
				latest: {version: .[2], date: .[3], url: .[7]},
				all: .[4] | split(" ") | map({version: ""}),
				valid: .[8] | split(" ") | map({version: "", url: .}),
				nok: .[9] | split(" ") | map({version: "", url: .})
			}
		}
	]}' < "${PARAM[OUTPUT]}");
	var_data_rewrited="$(echo "$var_data_imported" | jq --tab '.distro[] |= (
		.releases.all = [
			( range(0; (.releases.all | length)) as $i | .tmp.all[$i].version )
		] | 
		.releases.valid = [ 
			( range(0; (.releases.valid | length)) as $i | { version: .tmp.valid[$i].version, url: .releases.valid[$i].url } )
		] | 
		.releases.nok = [ 
			( range(0; (.releases.nok | length)) as $i | { version: .tmp.nok[$i].version, url: .releases.nok[$i].url } )
		] | 
		del(.tmp)
		)'
	)";
	# Final JSON output.
	echo "$var_data_rewrited" > "${PARAM[OUTPUT]%.csv}.json";
}

# Convert_CSVToYAML.
#	DESCRIPTION : data CSV to data YAML.
function Convert_CSVToYAML() {
	# Some definition.
	local var_array_file;
	local var_array_distribution;
	local var_data_imported;
	local var_data_rewrited_eval;
	local var_data_rewrited_map;
	local var_data_rewrited;
	# Loop : Process Substitution.
	IFS=',' read -ra var_array_file < "${PARAM[OUTPUT]}";
	# Construct : convert reading file.
	while IFS=',' read -ra var_array_distribution; do
		var_data_imported+="- \n";
		for i in "${!var_array_file[@]}"; do
			var_data_imported+="  ${var_array_file[$i]}: ${var_array_distribution[$i]}\n";
		done;
	done < <(tail -n +2 "${PARAM[OUTPUT]}");
	Restore_IFS;
	var_data_rewrited_eval=$(echo -e "$var_data_imported" | yq eval "{\"revision\": \"${DATE[DATETIME0]}\", \"distro\": .}" -);
	var_data_rewrited_map="$(echo "$var_data_rewrited_eval" | yq eval '
		.distro[] |= (
			.name = .Name |
			.fullname = .Fullname |
			.source = .Source |
			.releases = {
			"latest": {
				"version": (.VersionLatest | tostring),
				"date": .VersionLatestDate,
				"url": .UrlLatest
			},
			"all": ((.VersionAll // "" | tostring) | split(" ")),
			"valid_version": (.VersionValid // "" | tostring | split(" ")),
			"valid_url": (.UrlValid // "" | tostring | split(" ")),
			"nok_version": (.VersionNOK // "" | tostring | split(" ")),
			"nok_url": (.UrlNOK // "" | tostring | split(" "))
			} |
			. |= {
			"name": .name,
			"fullname": .fullname,
			"source": .source,
			"releases": .releases
			}
		)'
	)";
	var_data_rewrited="$(echo "$var_data_rewrited_map" | yq eval "
		.distro[] |= (
			.releases.valid = (
			( .releases.valid_version // [] ) as \$versions |
			( .releases.valid_url // [] ) as \$urls |
				( \$versions | to_entries | map({ \"version\": .value, \"url\": (\$urls[.key]) }))
			) |
			.releases.nok = (
			( .releases.nok_version // [] ) as \$versions |
			( .releases.nok_url // [] ) as \$urls |
				( \$versions | to_entries | map({ \"version\": .value, \"url\": (\$urls[.key]) }))
			) |
			del(.releases.valid_version, .releases.valid_url, .releases.nok_version, .releases.nok_url)
		)"
	)";
	# Final YAML output.
	echo "$var_data_rewrited" > "${PARAM[OUTPUT]%.csv}.yaml";
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	# array.
	declare -a var_array_param_distribution;
	declare -a var_array_return_distribution;
	local var_array_return_distribution_length;
	# element.
	local var_distribution_name;
	local var_distribution_fullname;
	local var_distribution_mode;
	local var_distribution_indexes;
	local var_release_version;
	local var_release_version_display;
	local var_release_array_filenameurl;
	local var_release_filename;
	local var_release_filename_display;
	local var_release_url;
	local var_release_url_clean;
	local var_release_url_display;
	local var_release_url_valid_last;
	local var_release_date;
	local var_release_date_valid_last;
	local var_release_note;
	local var_release_distant;
	local var_release_check;
	local var_release_nok;
	# count.
	local var_count_release_all;
	local var_count_release_valid;
	# data backup.
	local var_return_backup;
	# result.
	declare -a var_array_result_distribution_nok;
	declare -a var_array_result_distribution_nok_url;
	declare -a var_array_result_distribution_valid;
	local var_array_result_distribution_valid_display;
	declare -a var_array_result_distribution_valid_url;
	# Construct : read data.
	IFS=',' read -ra var_array_param_distribution <<< "${PARAM[DISTRO]}";
	Restore_IFS;
	# Test : emergency work.
 	if [[ "${CONF[DEBUG]}" == false ]]; then
		# TMPSOLUTION.
		if [[ "${PARAM[BACKUP]}" == true && ! "${PARAM[FORMAT]}" =~ ^(json|yaml|all)$ ]]; then
			echo -e "- WARNING : '--backup' only available for CSV format, but will be available for JSON and YAML too, keep in touch !";
		fi;
		# /TMPSOLUTION.
		# Test : Table with param and max value length.
		if [[ "${PARAM[QUIET]}" != true ]]; then Get_TableHeader -ARRAY "ID:4 Version:10 Filename:40 Ping:4 URL:40 Reject:16"; fi;
		# Start or RàZ CSV output.
		echo -e "Name,Fullname,VersionLatest,VersionLatestDate,VersionAll,VersionValid,VersionNOK,UrlLatest,UrlValid,UrlNOK,Source" > "${PARAM[OUTPUT]}";
		# Loop : distribution, with 2 counts control (all & valid).
		for var_distribution_name in "${var_array_param_distribution[@]}"; do
			# Define second value always here.
			var_distribution_fullname=$(echo "${DISTRO[$var_distribution_name]}" | cut -d "," -f2);
			# Construct : read release.
			IFS=',' read -ra var_array_return_distribution <<< "$(Get_AllVersion -NAME "$var_distribution_name")";
			var_array_return_distribution_length=${#var_array_return_distribution[@]};
			Restore_IFS;
			# Test : parsing not available.
			if [ "$var_array_return_distribution_length" -ne 0 ]; then
				# Env & Indexes.
				var_distribution_mode="${DISTRO[${var_distribution_name}_mode]}";
				var_distribution_indexes=$([[ "${DISTRO[${var_distribution_name}_archive_index]}" == "" ]] && echo "1 index" || echo "2 indexes");
				# Display.
				if [[ "${PARAM[QUIET]}" != true ]]; then echo -e "\n${COLOR[GREEN_DARK]}$var_distribution_name${COLOR[STOP]} - $var_array_return_distribution_length release(s) from $var_distribution_indexes ('${var_distribution_mode}' available) :"; fi;
				# Count.
				var_count_release_all=0;
				var_count_release_valid=0;
				# Loop : release.
				while [[ "$var_count_release_valid" -lt "${PARAM[LENGTH]}" && "$var_count_release_all" -lt "$var_array_return_distribution_length" ]]; do
					var_release_version=${var_array_return_distribution[$var_count_release_all]};
					var_release_array_filenameurl=$(Get_SingleVersion -NAME "$var_distribution_name" -VERSION "$var_release_version" -POSITION "$var_count_release_all");
					var_release_filename=$(echo "$var_release_array_filenameurl" | cut -d ";" -f1);
					var_release_url=$(echo "$var_release_array_filenameurl" | cut -d ";" -f2);
					var_release_date=$(echo "$var_release_array_filenameurl" | cut -d ";" -f3);
					# Display.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						var_release_version_display=$(Limit_StringLength -STRING "$var_release_version" -COUNT 10 -DIRECTION "after");
						var_release_filename_display=$(Limit_StringLength -STRING "$var_release_filename" -COUNT 40 -DIRECTION "after");
						var_release_url_clean="${var_release_url#http://}";
						var_release_url_clean="${var_release_url_clean#https://}";
						var_release_url_display=$(Limit_StringLength -STRING "$var_release_url_clean" -COUNT 40 -DIRECTION "after");
						var_release_note="";
					fi;
					# Test : line check.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 [-]:5" -ACTION "check";
					fi;
					# Test : single release file available.
					if Get_Distant -URL "$var_release_url" -TYPE "ping"; then
						var_release_distant=true;
						var_release_check="tick";
					else
						var_release_distant=false;
						var_release_check="cross";
					fi;
					# Test : single release filter NOK with void for no interacting with echo.
					if Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename"; then var_release_nok=true; else var_release_nok=false; fi;



					# TMPSOLUTION ? filename never empty here, some optimization possible (don't test ping if nok).



					# Tests : NOK.
					if [[ "$var_release_nok" == true || "$var_release_distant" == false || "$var_release_filename" == "ERR_INV" ]]; then
						if [[ "$var_release_nok" == true ]]; then
							var_release_note="$(Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename")";
						elif [[ "$var_release_distant" == false ]]; then
							var_release_note="nok-ping";
						elif [[ "$var_release_filename" == "ERR_INV" ]]; then
							var_release_note="nok-file";
						fi;
						# Construct nok distribution return filtered.
						var_array_result_distribution_nok+=("$var_release_version");
						var_array_result_distribution_nok_url+=("$var_release_url");
					else
						# Construct valid distribution return filtered.
						var_array_result_distribution_valid+=("$var_release_version");
						var_array_result_distribution_valid_url+=("$var_release_url");
						var_count_release_valid="$((var_count_release_valid+1))";
						# First and only definition of url & date.
						if [[ -z "$var_release_url_valid_last" || "$var_release_url_valid_last" == "" ]]; then var_release_url_valid_last="$var_release_url"; fi;
						if [[ -z "$var_release_date_valid_last" || "$var_release_date_valid_last" == "" ]]; then var_release_date_valid_last="$var_release_date"; fi;
					fi;
					# Test : line checked.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 ${var_release_check}:5 ${var_release_url_display}:40 ${var_release_note}:16" -ACTION "checked";
					fi;
					var_count_release_all="$((var_count_release_all+1))";
				done;
				# Test : nok content.
				if [ ${#var_array_result_distribution_valid[@]} -le 0 ]; then
					# Test : BACKUP.
					if [[ "${PARAM[BACKUP]}" == true ]]; then
						# Test : backup need previous data.
						if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
							var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
							# Test : pattern.
							if [[ -n "$var_return_backup" ]]; then
								echo -e "${var_return_backup}" >> "${PARAM[OUTPUT]}";
								sed -i "/^${var_distribution_name}/s/,current/,archive/" "${PARAM[OUTPUT]}";
								if [[ "${PARAM[QUIET]}" != true ]]; then
									echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : no valid versions to display, but content recovered from backup.${COLOR[STOP]}";
								fi;
							else
								# Final release CSV output (nok content).
								echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
								if [[ "${PARAM[QUIET]}" != true ]]; then
									echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : no valid versions to display and no previous data in backup file, nok content pushed.${COLOR[STOP]}";
								fi;
							fi;
						else
							# Final release CSV output (nok content).
							echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : no valid versions to display and no previous backup file, nok content pushed.${COLOR[STOP]}";
							fi;
						fi;
					else
						# Final release CSV output (nok content).
						echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
						if [[ "${PARAM[QUIET]}" != true ]]; then
							echo -e "${COLOR[RED_LIGHT]}no valid versions to display and data backup disabled, nok content pushed.${COLOR[STOP]}";
						fi;
					fi;
				else
					if [[ -n ${var_array_result_distribution_valid[*]:1} ]]; then
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]} ${var_array_result_distribution_valid[*]:1}"
					else
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]}";
					fi;
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "latest stable versions are '${var_array_result_distribution_valid_display}'.";
					fi;
					# Final release CSV output (ok content).
					echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
				fi;
				# RàZ nok distribution.
				var_array_result_distribution_nok=();
				var_array_result_distribution_nok_url=();
				# RàZ valid distribution.
				var_array_result_distribution_valid=();
				var_array_result_distribution_valid_display="";
				var_array_result_distribution_valid_url=();
				var_release_date_valid_last="";
				var_release_url_valid_last="";
			else
				# Test : BACKUP.
				if [[ "${PARAM[BACKUP]}" == true ]]; then
					# Test : backup need previous data.
					if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
						var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
						# Test : pattern.
						if [[ -n "$var_return_backup" ]]; then
							echo -e "${var_return_backup}" >> "${PARAM[OUTPUT]}";
							sed -i "/^${var_distribution_name}/s/,current/,archive/" "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty, but content recovered from backup.${COLOR[STOP]}";
							fi;
						else
							echo -e "${var_distribution_name},,,,,,,,,,current" >> "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous data in backup file, default content pushed.${COLOR[STOP]}";
							fi;
						fi;
						# RàZ valid distribution.
						var_return_backup="";
					else
						echo -e "${var_distribution_name},,,,,,,,,,current" >> "${PARAM[OUTPUT]}";
						if [[ "${PARAM[QUIET]}" != true ]]; then
							echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous backup file, default content pushed.${COLOR[STOP]}";
						fi;
					fi;
				else
					# Empty record.
					echo -e "${var_distribution_name},,,,,,,,,,current" >> "${PARAM[OUTPUT]}";
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and data backup disabled, default content pushed.${COLOR[STOP]}";
					fi;
				fi;
			fi;
		done;
		# TMPSOLUTION.
		# csv / json / yaml / all output.
		if [[ "${PARAM[FORMAT]}" == "json" ]]; then
			Convert_CSVToJSON;
			rm "${PARAM[OUTPUT]}";
		elif [[ "${PARAM[FORMAT]}" == "yaml" ]]; then
			Convert_CSVToYAML;
			rm "${PARAM[OUTPUT]}";
		elif [[ "${PARAM[FORMAT]}" == "all" ]]; then
			Convert_CSVToJSON;
			Convert_CSVToYAML;
		fi;
		# /TMPSOLUTION.
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;
