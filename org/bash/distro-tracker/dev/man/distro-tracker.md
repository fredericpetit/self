<blockquote id="out2tag">



NAME



	distro-tracker - The IT monitoring tool for find out latest stable versions of GNU/Linux & BSD distributions.





REQUIREMENTS



	program 'wget' (mandatory)



 	program 'curl' (mandatory)



 	program 'jq' (mandatory)



 	program 'yq' (mandatory)



 

SYNOPSIS



	distro-tracker [ OPTIONS ]





DESCRIPTION



	This open-source tool analyzes remote content using multiple regular expressions and standardizes the output into CSV, JSON, or YAML files.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path or single distant url to JSON data source for distributions URLs.

 		- state : optionnal.

 		- accepted value(s) : path. example at 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json'. CSV & YAML formats available in a next release.

 		- default value : no default, hardcoded URLs are the source until this option is provisioned.



 	-c CREDENTIAL,

	--credential CREDENTIAL

 		Login/ID & password/token of source, delimited by ':', in comma-separated values.

 		- state : optionnal.

 		- accepted value(s) : string. example 'service:credential,service:credential'.

 		- default value : no default.



 	-d NAMES,

	--distro NAMES

 		Single distribution name or coma separated list, without spaces, of distributions names.

 		- state : optionnal.

 		- accepted value(s) : available distributions names are 'alma', 'alpine', 'arch', 'centos', 'clonezilla', 'debian', 'fedora', 'freebsd', 'kaisen', 'kali', 'libreelec', 'linux', 'mageia', 'manjaro', 'mint', 'mx', 'openmediavault', 'opensuse', 'opnsense', 'parrot', 'pfsense', 'proxmox', 'raspios', 'rescuezilla', 'rocky', 'shredos', 'slackware', 'truenas', 'ubuntu' & 'xcpng', or 'all' registered.

 		- default value : 'all'.



 	-m (no type),

	--mode (no type)

 		Choose Command-Line Interface only or Graphical User Interface for distribution if available.

 		- state : optionnal.

 		- accepted value(s) : 'cli' or 'ui'.

 		- default value : 'ui.



 	-l NUMBER,

	--length NUMBER

 		Number of valid releases by distribution needed.

 		- state : optionnal.

 		- accepted value(s) : any number starting from 1.

 		- default value : '3'.



 	-o FILEPATH,

	--output FILEPATH

 		Single file path without extension to store the final result. If provisioned it's custom name mode, if not it's date name mode.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD/DT-DATETIME.FORMAT' (date name mode).



 	-f EXTENSION,

	--format EXTENSION

 		Single data format for previous '--output' option.

 		- state : optionnal.

 		- accepted value(s) : 'csv', 'json', 'yaml' or 'all'.

 		- default value : 'csv'.



 	-b (no type),

	--backup (no type)

 		Backup security for keeping and don't removing previous data if entire website checking fails. New and old datas need to be in the same directory. CSV & YAML formats available in a next release.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	all distributions in quiet mode, with Gitlab & Github tokens :

		distro-tracker -q -c gitlab:token,github:token



 	custom distributions, custom length, custom output, with backup :

		distro-tracker -d debian,fedora,freebsd,mageia,slackware -l 10 -o distro-tracker -b



 	mageia distribution in JSON format with custom URLs :

		distro-tracker -d mageia -f json -s https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json



 	get generated scheduled data from distrotracker.com in YAML format :

		distro-tracker -g -f yaml



 

TO-DO



	Flatpak package.





NOTES



	'yq' dependencie is 'yq' by Mike Farah.



 	Some distributions have two general indexes to scan to retrieve the latest versions, with main and archive. Information gived in terminal output.



 	Fedora lists in the index of a release an 'OSB' version, a version built with osbuilder.



 	Alma and CentOS can list 'non-existent' versions.



 	Alma, Fedora & Ubuntu have both ui or cli mode.



 	Alpine & XCP-ng nead a sort with head because many releases listed in single release page.



 

CREDITS & THANKS



	Thanks for my friends Mikaël, Jennifer & Priscilia for their support.





CHANGELOG



	1.2.0 - 2025-01-01

		Increase to 30 distributions with the following new entries : LibreELEC, OpenMediaVault, Raspberry Pi OS, Rocky Linux, ShredOS & TrueNAS. Move report function to standalone Distro Reporter & Distro Publisher projects. Move get distant content function to Distro Downloader. Replace '--cli' option with new '--mode' option. Quick '-c' option is now for new '--credential' option. Stronger backup protection with recovering if all content is NOK. Fix requirements because cURL & jq always needed.



 	1.1.1 - 2024-10-01

		Fix **Linux Kernel** regex. Add additional protection that invalidates any result with two entities in the response.



 	1.1.0 - 2024-09-19

		Increase to 25 distributions with the following new entries : Linux (Kernel) & Rescuezilla. New documentation, with a standards-compliant man file. New NOK filter for OPNsense, and new URLs for openSUSE & Mageia. Added YAML output format, which is accompanied by the addition of the '--format' option to choose between CSV, JSON or YAML - by default only CSV is output. The default value of the '--length' option is now '3'. All options are lowercase. The revision dates in the JSON & YAML output, and in the report, are in standard ISO 8601 format. Fixed execution time calculation.



 	1.0.0 - 2024-07-17

		Expansion to 22 GNU/Linux distributions with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, and XCP-ng. General mechanism for retrieving version numbers using the '-LENGTH' option has been revised for more logic : not just X tested versions are returned, but now X valid versions. Addition of the '-SOURCE' option to choose tracking URLs via a local or remote JSON file. Addition of the '-REPORT' option to generate a JSON report to notify of any changes in the latest release of each distribution. Activation of the '-ENV' option. Renaming of the '-DISTRIB' option to '-DISTRO', '-DEPTH' to '-LENGTH', '-SECURITY' to '-BACKUP'. Addition of one-letter shortcuts for each option. Addition of the Compare_NokFilter function to better manage and clarify versions tagged 'NOK' (beta, RC, etc.) : all pingable versions are retrieved, with a final filter on stable versions.



 	0.5.0 - 2024-06-01

		Add -SECURITY option for copy old datas to new datas if wget fails on entire website.



 	0.4.0 - 2024-06-01

		Up to 15 distributions by default.



 	0.3.0 - 2024-06-01

		New strong REGEXP. Add '-LENGTH' option. Add command JQ requirement. Add JSON output. Up to 12 distributions GNU/Linux & BSD.



 	0.2.0 - 2024-06-01

		Separate checks on index page & release page.



 	0.1.0 - 2024-06-01

		Add CSV output.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.2.0

	License : GPL-3.0-or-later





</blockquote>
