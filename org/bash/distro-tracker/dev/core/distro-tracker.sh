#!/bin/bash
# shellcheck source=/dev/null
#  _______________________________________________
# |                                               | #
# |               distro-tracker.sh               | #
# |_______________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-tracker";
APP["SECTION"]="1";
APP["SHORTNAME"]="DT";
APP["SLOGAN"]="The IT monitoring tool for find out latest stable versions of GNU/Linux & BSD distributions.";
APP["DESCRIPTION"]="This open-source tool analyzes remote content using multiple regular expressions and standardizes the output into CSV, JSON, or YAML files.";
APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory#program yq mandatory";
APP["EXAMPLES"]="all distributions in quiet mode, with Gitlab & Github tokens :|${APP[NAME]} -q -c gitlab:token,github:token#custom distributions, custom length, custom output, with backup :|${APP[NAME]} -d debian,fedora,freebsd,mageia,slackware -l 10 -o distro-tracker -b#mageia distribution in JSON format with custom URLs :|${APP[NAME]} -d mageia -f json -s https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json#get generated scheduled data from distrotracker.com in YAML format :|${APP[NAME]} -g -f yaml";
APP["TODO"]="Flatpak package.";
APP["NOTES"]="'yq' dependencie is 'yq' by Mike Farah.#Some distributions have two general indexes to scan to retrieve the latest versions, with main and archive. Information gived in terminal output.#Fedora lists in the index of a release an 'OSB' version, a version built with osbuilder.#Alma and CentOS can list 'non-existent' versions.#Alma, Fedora & Ubuntu have both ui or cli mode.#Alpine & XCP-ng nead a sort with head because many releases listed in single release page.";
APP["CREDITS"]="Thanks for my friends Mikaël, Jennifer & Priscilia for their support.";
APP["CHANGELOG"]="1.2.0|2025-01-01T10:00:00+0200|Increase to 30 distributions with the following new entries : LibreELEC, OpenMediaVault, Raspberry Pi OS, Rocky Linux, ShredOS & TrueNAS. Move report function to standalone Distro Reporter & Distro Publisher projects. Move get distant content function to Distro Downloader. Replace '--cli' option with new '--mode' option. Quick '-c' option is now for new '--credential' option. Stronger backup protection with recovering if all content is NOK. Fix requirements because cURL & jq always needed.#1.1.1|2024-10-01T10:00:00+0200|Fix **Linux Kernel** regex. Add additional protection that invalidates any result with two entities in the response.#1.1.0|2024-09-19T10:00:00+0200|Increase to 25 distributions with the following new entries : Linux (Kernel) & Rescuezilla. New documentation, with a standards-compliant man file. New NOK filter for OPNsense, and new URLs for openSUSE & Mageia. Added YAML output format, which is accompanied by the addition of the '--format' option to choose between CSV, JSON or YAML - by default only CSV is output. The default value of the '--length' option is now '3'. All options are lowercase. The revision dates in the JSON & YAML output, and in the report, are in standard ISO 8601 format. Fixed execution time calculation.#1.0.0|2024-07-17T10:00:00+0200|Expansion to 22 GNU/Linux distributions with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, and XCP-ng. General mechanism for retrieving version numbers using the '-LENGTH' option has been revised for more logic : not just X tested versions are returned, but now X valid versions. Addition of the '-SOURCE' option to choose tracking URLs via a local or remote JSON file. Addition of the '-REPORT' option to generate a JSON report to notify of any changes in the latest release of each distribution. Activation of the '-ENV' option. Renaming of the '-DISTRIB' option to '-DISTRO', '-DEPTH' to '-LENGTH', '-SECURITY' to '-BACKUP'. Addition of one-letter shortcuts for each option. Addition of the Compare_NokFilter function to better manage and clarify versions tagged 'NOK' (beta, RC, etc.) : all pingable versions are retrieved, with a final filter on stable versions.#0.5.0|2024-06-01T10:00:00+0200|Add -SECURITY option for copy old datas to new datas if wget fails on entire website.#0.4.0|2024-06-01T09:00:00+0200|Up to 15 distributions by default.#0.3.0|2024-06-01T08:00:00+0200|New strong REGEXP. Add '-LENGTH' option. Add command JQ requirement. Add JSON output. Up to 12 distributions GNU/Linux & BSD.#0.2.0|2024-06-01T07:00:00+0200|Separate checks on index page & release page.#0.1.0|2024-06-01T06:00:00+0200|Add CSV output.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.2.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#optionnal#Single local file path or single distant url to JSON data source for distributions URLs.|path. example at 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-url/data/distro-url.json'. CSV & YAML formats available in a next release.|no default, hardcoded URLs are the source until this option is provisioned.";
OPTIONS["CREDENTIAL"]="2#c#CREDENTIAL#optionnal#Login/ID & password/token of source, delimited by ':', in comma-separated values.|string. example 'service:credential,service:credential'.|no default.";
OPTIONS["DISTRO"]="3#d#NAMES#optionnal#Single distribution name or coma separated list, without spaces, of distributions names.|available distributions names are 'alma', 'alpine', 'arch', 'centos', 'clonezilla', 'debian', 'fedora', 'freebsd', 'kaisen', 'kali', 'libreelec', 'linux', 'mageia', 'manjaro', 'mint', 'mx', 'openmediavault', 'opensuse', 'opnsense', 'parrot', 'pfsense', 'proxmox', 'raspios', 'rescuezilla', 'rocky', 'shredos', 'slackware', 'truenas', 'ubuntu' & 'xcpng', or 'all' registered.|'all'.";
OPTIONS["MODE"]="4#m#null#optionnal#Choose Command-Line Interface only or Graphical User Interface for distribution if available.|'cli' or 'ui'.|'ui.";
OPTIONS["LENGTH"]="5#l#NUMBER#optionnal#Number of valid releases by distribution needed.|any number starting from 1.|'3'.";
OPTIONS["OUTPUT"]="6#o#FILEPATH#optionnal#Single file path without extension to store the final result. If provisioned it's custom name mode, if not it's date name mode.|path.|'PWD/${APP[SHORTNAME]}-DATETIME.FORMAT' (date name mode).";
OPTIONS["FORMAT"]="7#f#EXTENSION#optionnal#Single data format for previous '--output' option.|'csv', 'json', 'yaml' or 'all'.|'csv'.";
OPTIONS["BACKUP"]="8#b#null#optionnal#Backup security for keeping and don't removing previous data if entire website checking fails. New and old datas need to be in the same directory. CSV & YAML formats available in a next release.|null|'false' because disabled.";
OPTIONS["QUIET"]="9#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="10#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="11#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--credential|-c)
			PARAM["CREDENTIAL"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -CONTAINS ":"
			shift 2
			;;
		--distro|-d)
			PARAM["DISTRO"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--mode|-m)
			PARAM["MODE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--length|-l)
			PARAM["LENGTH"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "int"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--format|-f)
			PARAM["FORMAT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2" -TYPE "char"
			shift 2
			;;
		--backup|-b)
			PARAM["BACKUP"]=true
			shift
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================== #
#          DISTRO          #
#
#	DESCRIPTION : the source.

declare -gA DISTRO=(
	["alma"]="1,AlmaLinux OS,uicli,https://repo.almalinux.org/almalinux,https://repo.almalinux.org/almalinux/xxx/isos/x86_64,null,https://vault.almalinux.org/xxx/isos/x86_64"
	["alpine"]="2,Alpine Linux,cli,https://dl-cdn.alpinelinux.org/alpine,https://dl-cdn.alpinelinux.org/alpine/xxx/releases/x86_64,null,null"
	["arch"]="3,Arch Linux,ui,https://geo.mirror.pkgbuild.com/iso,https://geo.mirror.pkgbuild.com/iso/xxx,null,null"
	["centos"]="4,CentOS Stream,ui,https://mirror.in2p3.fr/pub/linux/centos-stream,https://mirror.in2p3.fr/pub/linux/centos-stream/xxx/BaseOS/x86_64/iso,https://mirror.in2p3.fr/pub/linux/centos-vault,https://mirror.in2p3.fr/pub/linux/centos-vault/xxx/isos/x86_64"
	["clonezilla"]="5,Clonezilla,ui,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable/xxx,null,null"
	["debian"]="6,Debian,ui,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/mirror/cdimage/archive,https://cdimage.debian.org/mirror/cdimage/archive/xxx/amd64/iso-dvd"
	["fedora"]="7,Fedora Linux,uicli,https://mirror.in2p3.fr/pub/fedora/linux/releases,https://mirror.in2p3.fr/pub/fedora/linux/releases/xxx/Workstation/x86_64/iso,null,https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/xxx/Workstation/x86_64/iso"
	["freebsd"]="8,FreeBSD,ui,https://download.freebsd.org/releases/ISO-IMAGES,https://download.freebsd.org/releases/ISO-IMAGES/xxx,null,null"
	["kaisen"]="9,Kaisen Linux,ui,https://iso.kaisenlinux.org/rolling,https://iso.kaisenlinux.org/rolling,null,null"
	["kali"]="10,Kali Linux,ui,https://cdimage.kali.org,https://cdimage.kali.org/kali-xxx,null,null"
	["libreelec"]="11,LibreELEC,ui,https://libreelec.tv/downloads/generic,https://releases.libreelec.tv,null,null"
	["linux"]="12,Linux Kernel,cli,https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x,https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x,null,null"
	["mageia"]="13,Mageia,ui,https://mirrors.kernel.org/mageia/iso,https://mirrors.kernel.org/mageia/iso/xxx/Mageia-xxx-x86_64,null,null"
	["manjaro"]="14,Manjaro Linux,ui,https://manjaro.org/products/download/x86,https://download.manjaro.org/kde/xxx,null,null"
	["mint"]="15,Linux Mint,ui,https://mirrors.edge.kernel.org/linuxmint/stable,https://mirrors.edge.kernel.org/linuxmint/stable/xxx,null,null"
	["mx"]="16,MX Linux,ui,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Old,https://sourceforge.net/projects/mx-linux/files/Old/MX-xxx/KDE"
	["openmediavault"]="17,OpenMediaVault,cli,https://sourceforge.net/projects/openmediavault/files/iso,https://sourceforge.net/projects/openmediavault/files/iso/xxx,null,null"
	["opensuse"]="18,openSUSE (Leap),ui,https://download.opensuse.org/distribution/leap/?jsontable,https://download.opensuse.org/distribution/leap/xxx/iso/?jsontable,null,null"
	["opnsense"]="19,OPNsense,cli,https://mirror.wdc1.us.leaseweb.net/opnsense/releases,https://mirror.wdc1.us.leaseweb.net/opnsense/releases/xxx,null,null"
	["parrot"]="20,Parrot Security,ui,https://deb.parrot.sh/parrot/iso,https://deb.parrot.sh/parrot/iso/xxx,null,null"
	["pfsense"]="21,pfSense,cli,https://atxfiles.netgate.com/mirror/downloads,https://atxfiles.netgate.com/mirror/downloads,null,null"
	["proxmox"]="22,Proxmox,cli,https://enterprise.proxmox.com/iso,https://enterprise.proxmox.com/iso,null,null"
	["raspios"]="23,Raspberry Pi OS,uicli,https://downloads.raspberrypi.com/raspios_arm64/images,https://downloads.raspberrypi.com/raspios_arm64/images/xxx,null,null"
	["rescuezilla"]="24,Rescuezilla,ui,https://api.github.com/repos/rescuezilla/rescuezilla/releases,https://github.com/rescuezilla/rescuezilla/releases/download/xxx,null,null"
	["rocky"]="25,Rocky Linux,uicli,https://download.rockylinux.org/pub/rocky,https://download.rockylinux.org/pub/rocky/xxx/isos/x86_64,https://dl.rockylinux.org/vault/rocky,https://dl.rockylinux.org/vault/rocky/xxx/isos/x86_64"
	["shredos"]="26,ShredOS,cli,https://api.github.com/repos/partialvolume/shredos.x86_64/releases,https://github.com/PartialVolume/shredos.x86_64/releases/download/xxx,null,null"
	["slackware"]="27,Slackware,ui,https://mirrors.slackware.com/slackware/slackware-iso,https://mirrors.slackware.com/slackware/slackware-iso/slackware64-xxx-iso,null,null"
	["truenas"]="28,TrueNAS (Community Edition),cli,https://download.sys.truenas.net/TrueNAS-SCALE-ElectricEel,https://download.sys.truenas.net/TrueNAS-SCALE-ElectricEel/xxx,null,null"
	["ubuntu"]="29,Ubuntu,uicli,https://releases.ubuntu.com,https://releases.ubuntu.com/xxx,null,null"
	["xcpng"]="30,XCP-ng,cli,https://updates.xcp-ng.org/isos,https://updates.xcp-ng.org/isos/xxx,null,null"
);
# Test : sort DISTRO.
if ! Sort_Array -NAME "DISTRO" -DELIMITER ","; then Stop_App -CODE 1 -TEXT "'DISTRO' array parsing failed."; fi;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some default.
declare -gA DEFAULT;
# Loop : distro.
for var_default_distro_index in $(echo "${!DISTRO_SORT[@]}" | tr ' ' '\n' | sort -n); do
	DEFAULT["DISTRO"]+=",${DISTRO_SORT[$var_default_distro_index]}";
done;
DEFAULT["DISTRO"]="${DEFAULT["DISTRO"]#,}";
# DEFAULT["DISTRO"] is a string, need array for Search_InArray custom PARAM["DISTRO"].
IFS=',' read -ra var_default_distro_array <<< "${DEFAULT["DISTRO"]}";
Restore_IFS;
DEFAULT["MODE"]="ui";
DEFAULT["LENGTH"]="3";
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}${APP[SHORTNAME]}-${DATE[DATETIME1]}";
DEFAULT["FORMAT"]="csv";
DEFAULT["BACKUP"]=false;
DEFAULT["QUIET"]=false;
# Some script definition.
# SOURCE.
if [ -z "${PARAM[SOURCE]}" ]; then
	# Loop : indexlist distro.
	for var_param_source_key in "${!DISTRO[@]}"; do
		var_param_source_value="${DISTRO[$var_param_source_key]}";
		Add_Book "DISTRO" "$var_param_source_key" "$var_param_source_value";
	done;
else
	# Custom book DISTRO.
	# Test : locally.
	if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]]; then
		var_data_distro="$(Get_Distant -URL "${PARAM[SOURCE]}" -TYPE "string")";
	else
		if Test_File -PATH "${PARAM[SOURCE]}"; then
			var_data_distro="$(<"${PARAM[SOURCE]}")";
		else
			Stop_App -CODE 1 -TEXT "File in '--source' option is not available.";
		fi;
	fi;
	# Test : data exists.
	if [[ "$var_data_distro" != "" ]]; then
		var_data_index="0";
		# Tests : CSV, JSON, YAML.
		var_data_format=$(Get_DataFormat -DATA "$var_data_distro");
		if [[ "$var_data_format" == "csv" ]]; then
			# Loop : Process Substitution.
			while IFS=',' read -r var_param_file_distro var_fullname var_mode var_current_index var_current_release var_archive_index var_archive_release; do
				# Test : invalid distribution.
				if Search_InArray -PATTERN "$var_param_file_distro" -DATA "${var_default_distro_array[*]}"; then
					var_param_file_values="$var_fullname,$var_mode,$var_current_index,$var_current_release,$var_archive_index,$var_archive_release";
					Add_Book "DISTRO" "$var_param_file_distro" "${var_data_index},${var_param_file_values}";
					# Test : emergency work.
					if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
						for key in "${!DISTRO[@]}"; do echo "key '$key' - value '${DISTRO[$key]}'"; done;
					fi;
					# Increment position.
					((var_data_index++));
				else
					Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_file_distro' from CSV.";
				fi
			done < <(echo "$var_data_distro");
		elif [[ "$var_data_format" == "json" ]]; then
			# Loop : Process Substitution.
			while read -r var_data_distro_array; do
				var_param_file_distro=$(echo "$var_data_distro_array" | jq -c -r '.name');
				var_param_file_values=$(echo "$var_data_distro_array" | jq -c -r '[.fullname, .mode, .current_index, .current_release, .archive_index, .archive_release] | join(",")');
				# Test : invalid distribution.
				if Search_InArray -PATTERN "$var_param_file_distro" -DATA "${var_default_distro_array[*]}"; then
					Add_Book "DISTRO" "$var_param_file_distro" "${var_data_index},${var_param_file_values}";
					# Test : emergency work.
					if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
						for key in "${!DISTRO[@]}"; do echo "key '$key' - value '${DISTRO[$key]}'"; done;
					fi;
					# Increment position.
					((var_data_index++));
				else
					Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_file_distro' from JSON data.";
				fi;
			done < <(echo "$var_data_distro" | jq -c -r '.distro[]');
		elif [[ "$var_data_format" == "yaml" ]]; then
			# Loop : Process Substitution.
			while read -r var_data_distro_name; do
				var_data=$(echo "$var_data_distro" | yq eval ".distro[] | select(.name == \"$var_data_distro_name\")");
				var_param_file_distro="$var_data_distro_name";
				var_param_file_values="$(echo "$var_data" | yq eval '[.fullname, .mode, .current_index, .current_release, .archive_index, .archive_release] | join(",")')";
				
				# TODO : keep null values !
				
				# Test : invalid distribution.
				if Search_InArray -PATTERN "$var_param_file_distro" -DATA "${var_default_distro_array[*]}"; then
					Add_Book "DISTRO" "$var_param_file_distro" "${var_data_index},${var_param_file_values}";
					# Test : emergency work.
					if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
						for key in "${!DISTRO[@]}"; do echo "key '$key' - value '${DISTRO[$key]}'"; done;
					fi;
					# Increment position.
					((var_data_index++));
				else
				Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_file_distro' from YAML data.";
				fi;
			done < <(echo "$var_data_distro" | yq '.distro[].name');
		else
			Stop_App -CODE 1 -TEXT "Format in '--source' option can't be defined.";
		fi;
	else
		# Test : locally.
		if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]]; then
			Stop_App -CODE 1 -TEXT "Distant file in '--source' option has no data.";
		else
			Stop_App -CODE 1 -TEXT "Local file in '--source' option has no data.";
		fi;
	fi;
fi;
# CREDENTIAL.
if [ -n "${PARAM[CREDENTIAL]}" ]; then
	Set_Book -NAME "CREDENTIAL";
fi;
# DISTRO.
if [ -z "${PARAM[DISTRO]}" ] || [[ "${PARAM[DISTRO]}" == "all" ]]; then
	PARAM["DISTRO"]="${DEFAULT[DISTRO]}";
else
	PARAM["DISTRO"]="$(echo "${PARAM["DISTRO"]}" | tr ',' '\n' | sort | tr '\n' ',')";
fi;
IFS=',' read -ra var_param_distro_array <<< "${PARAM["DISTRO"]}";
Restore_IFS;
# Loop : distribution.
for var_param_distro_array_element in "${var_param_distro_array[@]}"; do
	# Test : invalid distribution.
	if ! Search_InArray -PATTERN "$var_param_distro_array_element" -DATA "${var_default_distro_array[*]}"; then
		Stop_App -CODE 1 -TEXT "Unknown distribution : '$var_param_distro_array_element'.";
	fi;
done;
# MODE.
if [ -z "${PARAM[MODE]}" ]; then PARAM["MODE"]="${DEFAULT[MODE]}"; fi;
# LENGTH.
if [ -z "${PARAM[LENGTH]}" ]; then PARAM["LENGTH"]="${DEFAULT[LENGTH]}"; fi;
# OUTPUT + OUTPUT_BACKUP.
if [ -z "${PARAM[OUTPUT]}" ]; then
	# Loop : last file win previous data state.
	for var_param_output_single_file in "${PWD}${CONF[SEPARATOR]}${APP[SHORTNAME]}"*.csv; do
		PARAM["OUTPUT_BACKUP"]="$var_param_output_single_file";
	done;
	PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}.csv";
else
	PARAM["OUTPUT_BACKUP"]="${PARAM[OUTPUT]%[\\/]}.archive.csv";
	PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}.current.csv";
fi;
# FORMAT - Redefine requirements for user-friendly work.
if [ -z "${PARAM[FORMAT]}" ]; then
	PARAM["FORMAT"]="${DEFAULT[FORMAT]}";
	APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory";
elif [[ "${PARAM[FORMAT]}" == "json" ]]; then
	APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory";
elif [[ "${PARAM[FORMAT]}" == "yaml" ]]; then
	APP["REQUIREMENTS"]="program wget mandatory#program curl mandatory#program jq mandatory#program yq mandatory";
elif [[ ! "${PARAM[FORMAT]}" =~ ^(csv|json|yaml|all)$ ]]; then
	Stop_App -CODE 1 -TEXT "Unknown format : '${PARAM[FORMAT]}'.";
fi;
# BACKUP.
if [ -z "${PARAM[BACKUP]}" ]; then PARAM["BACKUP"]="${DEFAULT[BACKUP]}"; fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Get_AllVersion.
#	DESCRIPTION : parse HTML index page with some regex.
#	RETURN : string.
function Get_AllVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_data_current;
	local var_data_archive;
	local var_parse;
	local var_parse_current;
	local var_parse_archive;
	local var_array_release;
	local var_array_sorted_release;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_release");
	# Tests : construct HTML data.
	if [[ "$var_distribution_data_current_index" == *"api.github.com"* ]]; then
		# Test : Github auth is recommended.
		if [[ -n "${CREDENTIAL[GITHUB]}" ]]; then
			var_data_current=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index" -CREDENTIAL "${CREDENTIAL[GITHUB]}");
			if [[ "$var_distribution_data_archive_index" != "" && "$var_distribution_data_archive_index" != "null" ]]; then
				var_data_archive=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_archive_index" -CREDENTIAL "${CREDENTIAL[GITHUB]}");
			fi;
		else
			var_data_current=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index");
			if [[ "$var_distribution_data_archive_index" != "" && "$var_distribution_data_archive_index" != "null" ]]; then
				var_data_archive=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_archive_index");
			fi;
		fi;
	else
		var_data_current=$(Get_Distant -METHOD "wget" -URL "$var_distribution_data_current_index" -TYPE "string");
		if [[ "$var_distribution_data_archive_index" != "" && "$var_distribution_data_archive_index" != "null" ]]; then
			var_data_archive=$(Get_Distant -METHOD "wget" -URL "$var_distribution_data_archive_index" -TYPE "string");
		fi;
	fi
	# Tests : construct parsing for the distribution.
	if [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS.
		#
		var_parse_current=$(Get_AllVersionGeneric -DATA "$var_data_current");
		var_parse_archive=$(Get_AllVersionGeneric -DATA "$var_data_archive");
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" =~ ^(clonezilla|openmediavault)$ ]]; then
		#
		# CLONEZILLA, OPENMEDIAVAULT.
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '<a[^>]*href="[^"]*.iso"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | grep -Po '(?<=href=")[^?"]*' | sed 's/debian-//g' | sed 's/-amd64-DVD-1.iso//g' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/"[A-Za-z][^"]*"/""/g' | sed 's/<a href="[^"]*-live">//g' | grep -Po '(?<=href=")[^?"]*' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "libreelec" ]]; then
		#
		# LIBREELEC.
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<h5 class="card-title">[^<]*</h5>' | sed "s/<[^>]*>//g" | sed 's/ //g' | sed 's/LibreELEC//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '(?<=/MX-)[^"]+(?=_KDE_x64.iso/download")' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | sed 's/MX-//g' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "opensuse" ]]; then
		#
		# OPENSUSE.
		#
		var_parse=$(echo "$var_data_current" | jq -r '.data[] | select(.name != "42.3/") | .name | rtrimstr("/")' | tr '\n' ',');
	elif [[ "$var_distribution" =~ ^(rescuezilla|shredos)$ ]]; then
		#
		# RESCUEZILLA & SHREDOS (Github).
		#
		var_parse=$(echo "$var_data_current" | jq -r '.[] | select((.tag_name | contains("rolling") | not) and (.tag_name | contains("rc") | not) and (.tag_name | contains("beta") | not)) | .tag_name | ltrimstr("v")' 2>/dev/null | sed 's/_x86-64_.*//g' | tr '\n' ',') || var_parse="";
	elif [[ "$var_distribution" =~ ^(kaisen|linux|manjaro|pfsense|proxmox)$ ]]; then
		#
		# KAISEN, LINUX (KERNEL), MANJARO, PFSENSE, PROXMOX.
		#
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="(kaisenlinuxrolling|linux-|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)[^"]*(-amd64-MATE.iso|\d+\.tar.gz|manjaro-kde-|-RELEASE-amd64.iso.gz|\d+\.iso)"' | sed 's/href="//g' | sed 's/"//g' | sed -E 's/(kaisenlinuxrolling|linux-|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)//g' | sed -E 's/(-amd64-MATE.iso|.tar.gz|\/manjaro-kde-.*.iso|-RELEASE-amd64.iso.gz|.iso)//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "truenas" ]]; then
		#
		# TRUENAS.
		#
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<section.*</section>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/\.\.//g' | sed 's/\.\///g' | sed 's/\///g' | sed 's/"[^0-9][^"]*"//g' | grep -Po '(?<=href=")[^?"]*' | sed 's/?wrap=1//g' | tr '\n' ',');
	else
		#
		# GENERIC.
		#
		var_parse=$(Get_AllVersionGeneric -DATA "$var_data_current");
	fi;
	# Test : data exists & NO SPACE ALLOWED.
	if [[ -n "$var_parse" && "$var_parse" != "" && "$var_parse" != "null" && ! "$var_parse" =~ " " ]]; then
		# Loop : construct & sort by most recent versions.
		IFS=',' read -ra var_array_release <<< "$var_parse";
		mapfile -t var_array_sorted_release < <(printf "%s\n" "${var_array_release[@]}" | sort -Vr);
		# Construct return.
		var_result=("$(IFS=,; echo "${var_array_sorted_release[*]}")");
		Restore_IFS;
		echo "${var_result[@]}";
	else
		echo "";
	fi;
}

# Get_AllVersionGeneric.
#	DESCRIPTION : parse some HTML index page with single regex.
#	RETURN : string.
function Get_AllVersionGeneric() {
	# Some definition.
	local var_data;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	var_result=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<a href="[^"]*">Parent Directory<\/a>//g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/<a href="RPM-[^"]*">//g' | sed 's/<a href="almalinux-[^"]*">//g' | sed 's/<a href="slackware-[^"]*">//g' | grep -Po '(?<=href=")[^?"]*' | sed "s/\\bkali-//g" | sed "s/\\bv//g" | sed "s/\\bslackware64-//g" | sed "s/-iso\\b//g" | sed 's/raspios_arm64-//g' | tr '\n' ',');
	echo "$var_result";
}

# Get_SingleVersion.
#	DESCRIPTION : parse HTML release page with some regex.
#	RETURN : string.
function Get_SingleVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_version;
	local var_position;
	local var_url;
	local var_filename;
	local var_filedate;
	local var_date;
	local var_data;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			-POSITION)
				var_position="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME "DISTRO" -ELEMENT "$var_distribution" -DATA "archive_release");
	# Tests : construct data.
	if [[ "$var_distribution_data_current_index" == *"api.github.com"* ]]; then
		# Test : Github auth is recommended.
		if [[ -n "${CREDENTIAL[GITHUB]}" ]]; then
			var_data=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index" -CREDENTIAL "${CREDENTIAL[GITHUB]}");
		else
			var_data=$(Get_Distant -METHOD "curl" -URL "$var_distribution_data_current_index");
		fi;
	fi;
	# Test : construct pre-built url & date.
	var_url="${var_distribution_data_current_release//xxx/$var_version}";
	# Tests : construct return filename:url.
	if [[ "$var_distribution" == "alma" ]]; then
		#
		# ALMA - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"isos/"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		fi;
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-minimal.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-dvd.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "alpine" ]]; then
		#
		# ALPINE - many releases by release page (need sort).
		#
		var_url="${var_distribution_data_current_release//xxx/v${var_version}}";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-standard-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		fi;
	elif [[ "$var_distribution" == "arch" ]]; then
		#
		# ARCH - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"iso"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-LiveDVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-netinstall.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*latest-x86_64-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "clonezilla" ]]; then
		#
		# CLONEZILLA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-amd64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed "s/.*\/$var_version\///g" | sed 's/\/download//g');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN - one release by release page.
		# remember to match DVD-1 pattern with old releases.
		#
		# Test : fix debian separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="indexlist">(.*?)</table>' | grep -Po '<td class="indexcolname">(.*?)</td>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"debian-${var_version}-amd64-DVD-1.iso\"" | sed 's/href="//g' | sed 's/"//g' | head -n 1);
	elif [[ "$var_distribution" == "fedora" ]]; then
		#
		# FEDORA - one release by release page, uicli.
		# remember to not match -osb- pattern.
		#
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_url="${var_url//Workstation/Server}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_url="${var_url//Workstation/Server}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Server-dvd-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Workstation-Live-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-Live-Workstation-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_url="${var_url//$var_version\/Workstation\/x86_64\/iso/$var_version\/Fedora\/x86_64\/iso}";
					var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
					var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-[^"]*-x86_64-DVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		fi;
	elif [[ "$var_distribution" == "freebsd" ]]; then
		#
		# FREEBSD - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"FreeBSD-${var_version}-RELEASE-amd64-dvd1.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kaisen" ]]; then
		#
		# KAISEN - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}[^\"]*-amd64-MATE.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kali" ]]; then
		#
		# KALI - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="kali-linux-[^"]*-installer-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "libreelec" ]]; then
		#
		# LIBREELEC - all releases in one page (need sort).
		#
		var_url="$var_distribution_data_current_index";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*LibreELEC-[^\"]*${var_version}[^\"]*.img.gz\"" | sed 's/href="//g' | sed 's/"//g' | sed 's/https:\/\/[^\/]*\///g' | sort -Vr | head -n 1);
		var_url="$var_distribution_data_current_release";
	elif [[ "$var_distribution" == "linux" ]]; then
		#
		# LINUX - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-${var_version}.tar.gz\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mageia" ]]; then
		#
		# MAGEIA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "manjaro" ]]; then
		#
		# MANJARO - one release in one page.
		#
		var_data=$(Get_Distant -URL "$var_distribution_data_current_index" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}/manjaro-kde-[^\"]*.iso\"" | sed 's/href="//g' | sed 's/"//g' | sed 's/.*\///g');
	elif [[ "$var_distribution" == "mint" ]]; then
		#
		# MINT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*cinnamon-64bit.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX - one release by release page.
		#
		# Test : fix mx separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*_x64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed 's/.*KDE\///g' | sed 's/\/download//g');
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_url="${var_url//\/KDE/}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*MX-${var_version}_x64.iso/download\"" | sed 's/href="//g' | sed 's/"//g' | sed "s/.*MX-${var_version}\///g" | sed 's/\/download//g');
		fi;
	elif [[ "$var_distribution" == "openmediavault" ]]; then
		#
		# OPENMEDIAVAULT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-amd64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed "s/.*\/$var_version\///g" | sed 's/\/download//g');
	elif [[ "$var_distribution" == "opensuse" ]]; then
		#
		# OPENSUSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | jq -r '.data[] | select((.name | endswith("-DVD-x86_64-Media.iso"))) | .name');
		var_url="${var_url//\/\?jsontable/}";
	elif [[ "$var_distribution" == "opnsense" ]]; then
		#
		# OPNSENSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po "<table.*</table>" | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso.bz2"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "parrot" ]]; then
		#
		# PARROT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-security-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "pfsense" ]]; then
		#
		# PFSENSE - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-${var_version}-[^\"]*.iso.gz\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "proxmox" ]]; then
		#
		# PROXMOX - all releases in one page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"proxmox-ve_${var_version}.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "raspios" ]]; then
		#
		# RASPBERRY PI OS - one release by release page.
		#
		#
		var_url="${var_distribution_data_current_release//xxx/raspios_arm64-${var_version}}";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-raspios-[^\"]*\.(img\.xz|zip)\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "rescuezilla" ]]; then
		#
		# RESCUEZILLA - all releases in one page (Github).
		#
		#
		var_filename=$(echo "$var_data" | jq -r '.[] | .assets | map(select(.browser_download_url | test(".*rescuezilla-'"${var_version}"'-64bit\\..*\\.iso$"))) | last | .browser_download_url | select(. != null)' | sed "s/https:\/\/github.com\/rescuezilla\/rescuezilla\/releases\/download\/${var_version}\///g");
		var_url="${var_distribution_data_current_release//xxx/${var_version}}";
	elif [[ "$var_distribution" == "rocky" ]]; then
		#
		# ROCKY - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"isos/"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		fi;
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Rocky-[^"-]*-x86_64-minimal.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Rocky-[^"-]*-x86_64-dvd1?.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "shredos" ]]; then
		#
		# SHREDOS - all releases in one page (Github).
		#
		#
		var_filename=$(echo "$var_data" | jq -r '.[] | .assets | map(select(.browser_download_url | test("shredos-'"${var_version}"'_x86-64_.*\\.iso$|shredos-'"${var_version}"'_x86-64_.*\\.img$"))) | last | .browser_download_url | select(. != null)' | grep -Po '[^/]+$');
		var_filedate=$(echo "$var_filename" | sed 's/shredos-//g' | sed 's/_[^_]*$//g');
		var_url="${var_distribution_data_current_release//xxx/v${var_filedate}}";
	elif [[ "$var_distribution" == "slackware" ]]; then
		#
		# SLACKWARE - one release by release page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="slackware64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "truenas" ]]; then
		#
		# TRUENAS.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<section.*</section>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/?wrap=1//g' | grep -Po 'href="[^"]*.iso"' | sed 's/.\///g' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "ubuntu" ]]; then
		#
		# UBUNTU - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test: CLI usage.
		if [[ -n "${PARAM[MODE]}" && "${PARAM[MODE]}" == "cli" ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-live-server-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-desktop-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "xcpng" ]]; then
		#
		# XCP-NG - many releases by release page (need sort).
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"xcp-ng-${var_version}.([0-9]-[0-9]|[0-9]|[0-9]-[a-z]+[0-9]).iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
	fi;
	# Test : get ERR_INV error explicitly.
	if [[ -z "$var_filename" || "$var_filename" == "" || "$var_filename" == *$'\n'* || ${#var_filename} -gt 100 ]]; then
		var_filename="ERR_INV";
		var_date="ERR_INV";
	else
		var_date=$(Get_SingleVersionDateGeneric -DATA "$var_data" -FILENAME "$var_filename" -VERSION "$var_version");
	fi;
	# Construct return filename;url;date.
	echo "$var_filename;$var_url/$var_filename;$var_date";
}

# Get_SingleVersionDateGeneric.
#	DESCRIPTION : convert some date formats to ISO 8601.
#	RETURN : string.
function Get_SingleVersionDateGeneric() {
	# Some definition.
	local var_data;
	local var_filename;
	declare -A var_date_map;
	local var_date;
	local var_date_day;
	local var_date_month;
	local var_date_year;
	declare -a var_patterns;
	local var_pattern;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : rolling.
	if [[ "$var_filename" =~ ^manjaro-.* ]]; then
		var_result="rolling";
	else
		# BASH_REMATCH.
		var_date_map=( ["Jan"]="01" ["Feb"]="02" ["Mar"]="03" ["Apr"]="04" ["May"]="05" ["Jun"]="06" ["Jul"]="07" ["Aug"]="08" ["Sep"]="09" ["Oct"]="10" ["Nov"]="11" ["Dec"]="12" );
		# Date patterns to test : 'DD-MMM-YYYY', 'YYYY-MM-DD', 'YYYY-MMM-DD'.
		# 'DD-MMM-YYYY' = alma, alpine, arch, linux, mageia, mint, parrot, pfsense, proxmox, slackware, xcpng.
		# 'YYYY-MM-DD' = centos, clonezilla, debian, fedora, kaisen, mx, openmediavault, opensuse, raspios, ubuntu.
		# 'YYYY-MMM-DD' = freebsd, kali, opnsense.
		var_patterns=('\d{2}-[A-Za-z]{3}-\d{4}' '\d{4}-\d{2}-\d{2}' '\d{4}-[A-Za-z]{3}-\d{2}');
		# Loop : patterns.
		for var_pattern in "${var_patterns[@]}"; do
			# Remove <td>, <span></span>, title, all inside <a>XXX</a>, and get date.
			var_date=$(echo "$var_data" | grep -Po "<body.*</body>" | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title=\"[^\"]*\"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*${var_pattern}" | sed "s/${var_filename}//g" | grep -Po "${var_pattern}");
			# Test : handle different formats.
			if [[ "$var_date" =~ ^([0-9]{2})-([A-Za-z]{3})-([0-9]{4})$ ]]; then
				var_date_day=${BASH_REMATCH[1]};
				var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
				var_date_year=${BASH_REMATCH[3]};
				var_result="${var_date_year}-${var_date_month}-${var_date_day}";
				break;
			elif [[ "$var_date" =~ ^([0-9]{4})-([0-9]{2})-([0-9]{2})$ ]]; then
				var_date_year=${BASH_REMATCH[1]};
				var_date_month=${BASH_REMATCH[2]};
				var_date_day=${BASH_REMATCH[3]};
				var_result="${var_date_year}-${var_date_month}-${var_date_day}";
				break;
			elif [[ "$var_date" =~ ^([0-9]{4})-([A-Za-z]{3})-([0-9]{2})$ ]]; then
				var_date_year=${BASH_REMATCH[1]};
				var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
				var_date_day=${BASH_REMATCH[3]};
				var_result="${var_date_year}-${var_date_month}-${var_date_day}";
				break;
			fi;
		done;
		# Test : HTML alternate solution.
		# 'YYYY.MM' = libreelec.
		if [[ "$var_result" == "" ]]; then
			var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po "<h5 class=\"[^\"]*\">[^<]*</h5>[^<]*<div class=\"[^\"]*\">[^<]*<h5 class=\"[^\"]*\">LibreELEC ${var_version}</h5>" | grep -Po '\([^()]*\)' | sed 's/(//g' | sed 's/)//g');
			# Test : date exists.
			if [[ -n "$var_date" ]]; then
				var_result="${var_date}-01";
				var_result="${var_result//./-}";
			fi;
		fi;
		# Test : JSON 1st solution.
		# 'YYYY-MM-DD' = rescuezilla.
		if [[ "$var_result" == "" ]]; then
			var_date=$(echo "$var_data" | jq -r 'try (.[] | select((.tag_name | contains("rolling") | not) and (.tag_name | contains("rc") | not) and (.tag_name | contains("beta") | not)) | select((.tag_name | ltrimstr("v")) == "'"${var_version}"'") | .created_at) // empty' 2>/dev/null);
			# Test : date exists.
			if [[ -n "$var_date" && "$var_date" =~ T ]]; then
				var_result=$(echo "$var_date" | cut -d'T' -f1);
			fi;
		fi;
		# Test : JSON 2nd solution.
		# 'YYYY-MM-DD' = shredos.
		if [[ "$var_result" == "" ]]; then
			# Redefine version from filename.
			var_version=$(echo "$var_filename" | sed 's/shredos-//g' | sed 's/_[0-9]\+\.iso//g');
			var_date=$(echo "$var_data" | jq -r 'try (.[] | select((.tag_name | contains("rolling") | not) and (.tag_name | contains("rc") | not) and (.tag_name | contains("beta") | not)) | select((.tag_name | ltrimstr("v")) == "'"${var_version}"'") | .created_at) // empty' 2>/dev/null);
			# Test : date exists.
			if [[ -n "$var_date" && "$var_date" =~ T ]]; then
				var_result=$(echo "$var_date" | cut -d'T' -f1);
			fi;
		fi;
		# Test : JSON 3rd solution.
		# 'timestamp' = opensuse.
		if [[ "$var_result" == "" ]]; then
			var_date=$(echo "$var_data" | jq -r 'try (.data[] | select(.name == "'"${var_filename}"'") | .mtime | strftime("%Y-%m-%d")) // empty' 2>/dev/null);
			# Test : date exists.
			if [[ -n "$var_date" ]]; then var_result="$var_date"; fi;
		fi;
		# Tests : fail.
		if [[ ! "$var_result" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
			var_result="ERR_INV";
		elif [ -z "$var_result" ]; then
			var_result="ERR_INV";
		fi;
	fi;
	# Output result.
	echo "$var_result";
}

# Compare_NokFilter.
#	DESCRIPTION : filter release by nok patterns.
#	RETURN : string or code.
function Compare_NokFilter() {
	# Some definition.
	local var_distribution;
	local var_release;
	local var_filename;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DISTRO)
				var_distribution="$2"
				shift 2
				;;
			-RELEASE)
				var_release="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : nok & specific invalid.
	case "$var_distribution" in
		alpine)
			[[ "$var_filename" == *"_rc"* ]] && var_result="nok-rc"
			;;
		alma)
			if [[ "$var_release" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_release" =~ ^[0-9]$ ]]; then
				var_result="nok-version"
			fi
			;;
		debian)
			if [[ "$var_release" == *"_r"* ]]; then
				var_result="nok-rc"
			elif [[ "$var_release" == *"-"* ]]; then
				var_result="nok-version"
			fi
			;;
		freebsd)
			[[ "$var_filename" == *"RC"* ]] && var_result="nok-rc"
			;;
		kaisen)
			[[ "$var_release" == *"RC"* ]] && var_result="nok-rc"
			;;
		openmediavault)
			[[ "$var_filename" == *"beta"* ]] && var_result="nok-beta"
			;;
		opnsense)
			if [[ "$var_filename" == *"-devel-"* || "$var_filename" =~ \.b[0-9] ]]; then
				var_result="nok-beta"
			elif [[ "$var_filename" =~ \.r[0-9]- ]]; then
				var_result="nok-rc"
			fi
			;;
		parrot)
			[[ "$var_release" == *"-beta"* ]] && var_result="nok-beta"
			;;
		ubuntu)
			[[ "$var_filename" == *"-beta-"* ]] && var_result="nok-beta"
			;;
		xcpng)
			if [[ "$var_filename" == *"-alpha"* ]]; then
				var_result="nok-alpha"
			elif [[ "$var_filename" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_filename" == *"-rc"* ]]; then
				var_result="nok-rc"
			fi
			;;
	esac
	# Test : string, code.
	if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
}

# Convert_CSVToJSON.
#	DESCRIPTION : data CSV to data JSON.
function Convert_CSVToJSON() {
	# Some definition.
	local var_data_imported;
	local var_data_rewrited;
	# Import & rewrite.
	var_data_imported=$(jq -R --tab '{
	revision: "'"${DATE[DATETIME0]}"'",
	distro: [
		inputs | split(",") | {
			name: .[0],
			fullname: .[1],
			source: .[10],
			tmp: {
				all: .[4] | split(" ") | map({version: .}),
				valid: .[5] | split(" ") | map({version: .}),
				nok: .[6] | split(" ") | map({version: .})
			},
			releases: {
				latest: {version: .[2], date: .[3], url: .[7]},
				all: .[4] | split(" ") | map({version: ""}),
				valid: .[8] | split(" ") | map({version: "", url: .}),
				nok: .[9] | split(" ") | map({version: "", url: .})
			}
		}
	]}' < "${PARAM[OUTPUT]}");
	var_data_rewrited="$(echo "$var_data_imported" | jq --tab '.distro[] |= (
		.releases.all = [
			( range(0; (.releases.all | length)) as $i | .tmp.all[$i].version )
		] | 
		.releases.valid = [ 
			( range(0; (.releases.valid | length)) as $i | { version: .tmp.valid[$i].version, url: .releases.valid[$i].url } )
		] | 
		.releases.nok = [ 
			( range(0; (.releases.nok | length)) as $i | { version: .tmp.nok[$i].version, url: .releases.nok[$i].url } )
		] | 
		del(.tmp)
		)'
	)";
	# Final JSON output.
	echo "$var_data_rewrited" > "${PARAM[OUTPUT]%.csv}.json";
}

# Convert_CSVToYAML.
#	DESCRIPTION : data CSV to data YAML.
function Convert_CSVToYAML() {
	# Some definition.
	local var_array_file;
	local var_array_distribution;
	local var_data_imported;
	local var_data_rewrited_eval;
	local var_data_rewrited_map;
	local var_data_rewrited;
	# Loop : Process Substitution.
	IFS=',' read -ra var_array_file < "${PARAM[OUTPUT]}";
	# Construct : convert reading file.
	while IFS=',' read -ra var_array_distribution; do
		var_data_imported+="- \n";
		for i in "${!var_array_file[@]}"; do
			var_data_imported+="  ${var_array_file[$i]}: ${var_array_distribution[$i]}\n";
		done;
	done < <(tail -n +2 "${PARAM[OUTPUT]}");
	Restore_IFS;
	var_data_rewrited_eval=$(echo -e "$var_data_imported" | yq eval "{\"revision\": \"${DATE[DATETIME0]}\", \"distro\": .}" -);
	var_data_rewrited_map="$(echo "$var_data_rewrited_eval" | yq eval '
		.distro[] |= (
			.name = .Name |
			.fullname = .Fullname |
			.source = .Source |
			.releases = {
			"latest": {
				"version": (.VersionLatest | tostring),
				"date": .VersionLatestDate,
				"url": .UrlLatest
			},
			"all": ((.VersionAll // "" | tostring) | split(" ")),
			"valid_version": (.VersionValid // "" | tostring | split(" ")),
			"valid_url": (.UrlValid // "" | tostring | split(" ")),
			"nok_version": (.VersionNOK // "" | tostring | split(" ")),
			"nok_url": (.UrlNOK // "" | tostring | split(" "))
			} |
			. |= {
			"name": .name,
			"fullname": .fullname,
			"source": .source,
			"releases": .releases
			}
		)'
	)";
	var_data_rewrited="$(echo "$var_data_rewrited_map" | yq eval "
		.distro[] |= (
			.releases.valid = (
			( .releases.valid_version // [] ) as \$versions |
			( .releases.valid_url // [] ) as \$urls |
				( \$versions | to_entries | map({ \"version\": .value, \"url\": (\$urls[.key]) }))
			) |
			.releases.nok = (
			( .releases.nok_version // [] ) as \$versions |
			( .releases.nok_url // [] ) as \$urls |
				( \$versions | to_entries | map({ \"version\": .value, \"url\": (\$urls[.key]) }))
			) |
			del(.releases.valid_version, .releases.valid_url, .releases.nok_version, .releases.nok_url)
		)"
	)";
	# Final YAML output.
	echo "$var_data_rewrited" > "${PARAM[OUTPUT]%.csv}.yaml";
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	# array.
	declare -a var_array_param_distribution;
	declare -a var_array_return_distribution;
	local var_array_return_distribution_length;
	# element.
	local var_distribution_name;
	local var_distribution_fullname;
	local var_distribution_mode;
	local var_distribution_indexes;
	local var_release_version;
	local var_release_version_display;
	local var_release_array_filenameurl;
	local var_release_filename;
	local var_release_filename_display;
	local var_release_url;
	local var_release_url_clean;
	local var_release_url_display;
	local var_release_url_valid_last;
	local var_release_date;
	local var_release_date_valid_last;
	local var_release_note;
	local var_release_distant;
	local var_release_check;
	local var_release_nok;
	# count.
	local var_count_release_all;
	local var_count_release_valid;
	# data backup.
	local var_return_backup;
	# result.
	declare -a var_array_result_distribution_nok;
	declare -a var_array_result_distribution_nok_url;
	declare -a var_array_result_distribution_valid;
	local var_array_result_distribution_valid_display;
	declare -a var_array_result_distribution_valid_url;
	# Construct : read data.
	IFS=',' read -ra var_array_param_distribution <<< "${PARAM[DISTRO]}";
	Restore_IFS;
	# Test : emergency work.
 	if [[ "${CONF[DEBUG]}" == false ]]; then
		# TMPSOLUTION.
		if [[ "${PARAM[BACKUP]}" == true && ! "${PARAM[FORMAT]}" =~ ^(json|yaml|all)$ ]]; then
			echo -e "- WARNING : '--backup' only available for CSV format, but will be available for JSON and YAML too, keep in touch !";
		fi;
		# /TMPSOLUTION.
		# Test : Table with param and max value length.
		if [[ "${PARAM[QUIET]}" != true ]]; then Get_TableHeader -ARRAY "ID:4 Version:10 Filename:40 Ping:4 URL:40 Reject:16"; fi;
		# Start or RàZ CSV output.
		echo -e "Name,Fullname,VersionLatest,VersionLatestDate,VersionAll,VersionValid,VersionNOK,UrlLatest,UrlValid,UrlNOK,Source" > "${PARAM[OUTPUT]}";
		# Loop : distribution, with 2 counts control (all & valid).
		for var_distribution_name in "${var_array_param_distribution[@]}"; do
			# Define second value always here.
			var_distribution_fullname=$(echo "${DISTRO[$var_distribution_name]}" | cut -d "," -f2);
			# Construct : read release.
			IFS=',' read -ra var_array_return_distribution <<< "$(Get_AllVersion -NAME "$var_distribution_name")";
			var_array_return_distribution_length=${#var_array_return_distribution[@]};
			Restore_IFS;
			# Test : parsing not available.
			if [ "$var_array_return_distribution_length" -ne 0 ]; then
				# Env & Indexes.
				var_distribution_mode="${DISTRO[${var_distribution_name}_mode]}";
				var_distribution_indexes=$([[ "${DISTRO[${var_distribution_name}_archive_index]}" == "" ]] && echo "1 index" || echo "2 indexes");
				# Display.
				if [[ "${PARAM[QUIET]}" != true ]]; then echo -e "\n${COLOR[GREEN_DARK]}$var_distribution_name${COLOR[STOP]} - $var_array_return_distribution_length release(s) from $var_distribution_indexes ('${var_distribution_mode}' available) :"; fi;
				# Count.
				var_count_release_all=0;
				var_count_release_valid=0;
				# Loop : release.
				while [[ "$var_count_release_valid" -lt "${PARAM[LENGTH]}" && "$var_count_release_all" -lt "$var_array_return_distribution_length" ]]; do
					var_release_version=${var_array_return_distribution[$var_count_release_all]};
					var_release_array_filenameurl=$(Get_SingleVersion -NAME "$var_distribution_name" -VERSION "$var_release_version" -POSITION "$var_count_release_all");
					var_release_filename=$(echo "$var_release_array_filenameurl" | cut -d ";" -f1);
					var_release_url=$(echo "$var_release_array_filenameurl" | cut -d ";" -f2);
					var_release_date=$(echo "$var_release_array_filenameurl" | cut -d ";" -f3);
					# Display.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						var_release_version_display=$(Limit_StringLength -STRING "$var_release_version" -COUNT 10 -DIRECTION "after");
						var_release_filename_display=$(Limit_StringLength -STRING "$var_release_filename" -COUNT 40 -DIRECTION "after");
						var_release_url_clean="${var_release_url#http://}";
						var_release_url_clean="${var_release_url_clean#https://}";
						var_release_url_display=$(Limit_StringLength -STRING "$var_release_url_clean" -COUNT 40 -DIRECTION "after");
						var_release_note="";
					fi;
					# Test : line check.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 [-]:5" -ACTION "check";
					fi;
					# Test : single release file available.
					if Get_Distant -URL "$var_release_url" -TYPE "ping"; then
						var_release_distant=true;
						var_release_check="tick";
					else
						var_release_distant=false;
						var_release_check="cross";
					fi;
					# Test : single release filter NOK with void for no interacting with echo.
					if Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename"; then var_release_nok=true; else var_release_nok=false; fi;



					# TMPSOLUTION ? filename never empty here, some optimization possible (don't test ping if nok).



					# Tests : NOK.
					if [[ "$var_release_nok" == true || "$var_release_distant" == false || "$var_release_filename" == "ERR_INV" ]]; then
						if [[ "$var_release_nok" == true ]]; then
							var_release_note="$(Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename")";
						elif [[ "$var_release_distant" == false ]]; then
							var_release_note="nok-ping";
						elif [[ "$var_release_filename" == "ERR_INV" ]]; then
							var_release_note="nok-file";
						fi;
						# Construct nok distribution return filtered.
						var_array_result_distribution_nok+=("$var_release_version");
						var_array_result_distribution_nok_url+=("$var_release_url");
					else
						# Construct valid distribution return filtered.
						var_array_result_distribution_valid+=("$var_release_version");
						var_array_result_distribution_valid_url+=("$var_release_url");
						var_count_release_valid="$((var_count_release_valid+1))";
						# First and only definition of url & date.
						if [[ -z "$var_release_url_valid_last" || "$var_release_url_valid_last" == "" ]]; then var_release_url_valid_last="$var_release_url"; fi;
						if [[ -z "$var_release_date_valid_last" || "$var_release_date_valid_last" == "" ]]; then var_release_date_valid_last="$var_release_date"; fi;
					fi;
					# Test : line checked.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 ${var_release_check}:5 ${var_release_url_display}:40 ${var_release_note}:16" -ACTION "checked";
					fi;
					var_count_release_all="$((var_count_release_all+1))";
				done;
				# Test : nok content.
				if [ ${#var_array_result_distribution_valid[@]} -le 0 ]; then
					# Test : BACKUP.
					if [[ "${PARAM[BACKUP]}" == true ]]; then
						# Test : backup need previous data.
						if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
							var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
							# Test : pattern.
							if [[ -n "$var_return_backup" ]]; then
								echo -e "${var_return_backup}" >> "${PARAM[OUTPUT]}";
								sed -i "/^${var_distribution_name}/s/,current/,archive/" "${PARAM[OUTPUT]}";
								if [[ "${PARAM[QUIET]}" != true ]]; then
									echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : no valid versions to display, but content recovered from backup.${COLOR[STOP]}";
								fi;
							else
								# Final release CSV output (nok content).
								echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
								if [[ "${PARAM[QUIET]}" != true ]]; then
									echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : no valid versions to display and no previous data in backup file, nok content pushed.${COLOR[STOP]}";
								fi;
							fi;
						else
							# Final release CSV output (nok content).
							echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : no valid versions to display and no previous backup file, nok content pushed.${COLOR[STOP]}";
							fi;
						fi;
					else
						# Final release CSV output (nok content).
						echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
						if [[ "${PARAM[QUIET]}" != true ]]; then
							echo -e "${COLOR[RED_LIGHT]}no valid versions to display and data backup disabled, nok content pushed.${COLOR[STOP]}";
						fi;
					fi;
				else
					if [[ -n ${var_array_result_distribution_valid[*]:1} ]]; then
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]} ${var_array_result_distribution_valid[*]:1}"
					else
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]}";
					fi;
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "latest stable versions are '${var_array_result_distribution_valid_display}'.";
					fi;
					# Final release CSV output (ok content).
					echo -e "${var_distribution_name},${var_distribution_fullname},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
				fi;
				# RàZ nok distribution.
				var_array_result_distribution_nok=();
				var_array_result_distribution_nok_url=();
				# RàZ valid distribution.
				var_array_result_distribution_valid=();
				var_array_result_distribution_valid_display="";
				var_array_result_distribution_valid_url=();
				var_release_date_valid_last="";
				var_release_url_valid_last="";
			else
				# Test : BACKUP.
				if [[ "${PARAM[BACKUP]}" == true ]]; then
					# Test : backup need previous data.
					if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
						var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
						# Test : pattern.
						if [[ -n "$var_return_backup" ]]; then
							echo -e "${var_return_backup}" >> "${PARAM[OUTPUT]}";
							sed -i "/^${var_distribution_name}/s/,current/,archive/" "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty, but content recovered from backup.${COLOR[STOP]}";
							fi;
						else
							echo -e "${var_distribution_name},,,,,,,,,,current" >> "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous data in backup file, default content pushed.${COLOR[STOP]}";
							fi;
						fi;
						# RàZ valid distribution.
						var_return_backup="";
					else
						echo -e "${var_distribution_name},,,,,,,,,,current" >> "${PARAM[OUTPUT]}";
						if [[ "${PARAM[QUIET]}" != true ]]; then
							echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous backup file, default content pushed.${COLOR[STOP]}";
						fi;
					fi;
				else
					# Empty record.
					echo -e "${var_distribution_name},,,,,,,,,,current" >> "${PARAM[OUTPUT]}";
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and data backup disabled, default content pushed.${COLOR[STOP]}";
					fi;
				fi;
			fi;
		done;
		# TMPSOLUTION.
		# csv / json / yaml / all output.
		if [[ "${PARAM[FORMAT]}" == "json" ]]; then
			Convert_CSVToJSON;
			rm "${PARAM[OUTPUT]}";
		elif [[ "${PARAM[FORMAT]}" == "yaml" ]]; then
			Convert_CSVToYAML;
			rm "${PARAM[OUTPUT]}";
		elif [[ "${PARAM[FORMAT]}" == "all" ]]; then
			Convert_CSVToJSON;
			Convert_CSVToYAML;
		fi;
		# /TMPSOLUTION.
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;