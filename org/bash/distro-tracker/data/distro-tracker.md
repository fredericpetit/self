| fullname    | latest      | date        | few latest  | source      |
|-------------|-------------|-------------|-------------|-------------|
| AlmaLinux OS | 9.5 | 2024-11-13 | 9.5, 9.4, 9.3 | current |
| Alpine Linux | 3.21 | 2025-02-13 | 3.21, 3.20, 3.19 | current |
| Arch Linux | 2025.03.01 | 2025-03-01 | 2025.03.01, 2025.02.01, 2025.01.01 | current |
| CentOS Stream | 10-stream | 2025-03-03 | 10-stream, 9-stream, 8.5.2111 | current |
| Clonezilla | 3.2.1-9 | 2025-03-03 | 3.2.1-9, 3.2.0-5, 3.1.3-16 | current |
| Debian | 12.9.0 | 2025-01-11 | 12.9.0, 12.8.0, 12.7.0 | current |
| Fedora Linux | 41 | 2024-10-24 | 41, 40, 39 | current |
| FreeBSD | 14.2 | 2024-11-29 | 14.2, 14.1, 13.5 | current |
| Kaisen Linux | 2.3 | 2023-05-31 | 2.3, 2.2, 2.1 | current |
| Kali Linux | 2025.1a | 2025-03-07 | 2025.1a, 2024.4 | current |
| LibreELEC | 12.0 | 2025-01-01 | 12.0, 11.0, 10.0 | current |
| Linux Kernel | 6.13.6 | 2025-03-07 | 6.13.6, 6.13.5, 6.13.4 | current |
| Mageia | 9 | 2023-08-24 | 9, 8 | current |
| Manjaro Linux | 24.2.1 | rolling | 24.2.1 | current |
| Linux Mint | 22.1 | 2025-01-10 | 22.1, 22, 21.3 | current |
| MX Linux | 23.5 | 2025-01-12 | 23.5, 23.4, 23.3 | current |
| OpenMediaVault | 7.4.17 | 2024-12-23 | 7.4.17, 7.0-32, 7.0-20 | current |
| openSUSE (Leap) | 15.6 | 2024-06-20 | 15.6, 15.5, 15.4 | current |
| OPNsense | 25.1 | 2025-01-28 | 25.1, 24.7, 24.1 | current |
| Parrot Security | 6.3.2 | 2025-01-28 | 6.3.2, 6.2, 6.1 | current |
| pfSense | 2.7.2 | 2023-12-08 | 2.7.2, 2.7.1, 2.7.0 | current |
| Proxmox | 8.3-1 | 2024-11-21 | 8.3-1, 8.2-2, 7.4-1 | current |
| Raspberry Pi OS | 2024-11-19 | 2024-11-19 | 2024-11-19, 2024-11-18, 2024-10-28 | current |
| Rescuezilla | 2.5.1 | 2024-09-09 | 2.5.1, 2.5, 2.4.2 | current |
| Rocky Linux | 9.5 | 2024-11-16 | 9.5, 9.4, 9.3 | current |
| ShredOS | 2024.02.2_26.0 | 2024-05-29 | 2024.02.2_26.0, 2023.08.2_25.0, 2021.08.2_23.5 | current |
| Slackware | 15.0 | 2022-02-03 | 15.0, 14.2, 14.1 | current |
| TrueNAS (Community Edition) | 24.10.2 | ERR_INV | 24.10.2, 24.10.1, 24.10.0.2 | current |
| Ubuntu | 24.10 | 2024-10-09 | 24.10, 24.04.2, 24.04 | current |
| XCP-ng | 8.2 | 2022-02-25 | 8.2, 8.1, 8.0 | current |

