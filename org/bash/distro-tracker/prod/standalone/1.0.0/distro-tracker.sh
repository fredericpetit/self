#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_os.sh               | #
# |________________________________________| #


# ==================== #
#          OS          #
#
#	DESCRIPTION : interacting with OS for all APPs.


function Set_Placeholder5() {
	echo "placeholder";
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_network.sh               | #
# |_____________________________________________| #


# ========================= #
#          NETWORK          #
#
#	DESCRIPTION : network state for all APPs.

# Get_FirstNetwork.
#	DESCRIPTION : get name of the first network interface.
#	RETURN : string or code.
function Get_FirstNetwork() {
	# Some definition.
	local var_result;
	local var_interfaces;
	var_result="fail-network";
	var_interfaces=$(ip -o link show | awk -F': ' '{print $2}');
	# Loop : interfaces.
	for interface in $var_interfaces; do
		if [[ $interface != "lo" ]]; then var_result="$interface"; break; fi;
	done
	echo "$var_result";
}


# Get_Distant.
#	DESCRIPTION : get distant resource.
#	RETURN : string or code.
#	TO-DO : git auth.
function Get_Distant() {
	# Some definition.
	local var_method;
	local var_type;
	local var_url;
	local var_path;
	local var_credential;
	local var_username;
	local var_password;
	local var_overwrite;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-METHOD)
				var_method="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Some default.
	if [[ -z "$var_method" ]]; then var_method="wget"; fi;
	if [[ -z "$var_type" ]]; then var_type="file"; fi;
	# Test : auth.
	if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
		var_username=$(echo -e "$var_credential" | cut -d ':' -f 1)
		var_password=$(echo -e "$var_credential" | cut -d ':' -f 2)
	fi;
	# Tests : method.
	if [[ "$var_method" == "wget" ]]; then
		# Tests : file, string, ping.
		if [[ "$var_type" == "file" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		elif [[ "$var_type" == "string" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				fi;
			fi;
			# Test : good content.
			if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
		elif [[ "$var_type" == "ping" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		fi;
	elif [[ "$var_method" == "curl" ]]; then
		# Test : auth.
		if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
			if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
				if curl -s "$var_url" -u "${var_username}:${var_password}" | grep browser_download_url | cut -d '"' -f 4 | wget --no-config --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			else
				if curl -s "$var_url" -u "${var_username}:${var_password}" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			fi;
		else
			if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
				if curl -s "$var_url" | grep browser_download_url | cut -d '"' -f 4 | wget --no-config --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			else
				if curl -s "$var_url" | grep browser_download_url | cut -d '"' -f 4 | wget --timeout=10 --tries=2 -O "$var_path" -qi - &> /dev/null; then return 0; else return 1; fi;
			fi;
		fi;
	elif [[ "$var_method" == "git" ]]; then
		if [[ "$var_overwrite" = true ]]; then
			if git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; then return 0; else return 1; fi;
		else
			git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; return 0;
		fi;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_fs.sh               | #
# |________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interaction for all APPs.

# Restore_IFS.
#	DESCRIPTION : restore IFS.
function Restore_IFS() { IFS=${CONF["IFS"]}; }


# Test_Dir.
#	DESCRIPTION : check directory.
#	RETURN : code.
function Test_Dir() {
	# Some definition.
	local var_path;
	local var_create;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : folder.
	if [[ -n "$var_path" ]]; then
		if [[ "$var_create" = true ]]; then mkdir -p "$var_path" &> /dev/null; fi;
		if [ -d "$var_path" ]; then
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod -R "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown -R "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else return 1; fi;
	else return 1; fi;
}


# Test_DirPopulated().
#	DESCRIPTION : check if folder haz content in it.
#	RETURN : code.
function Test_DirPopulated() {
	# Some definition.
	local var_path;
	local var_extension;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-EXTENSION)
				var_extension="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	if [[ -n "$(ls -A "$var_path")" ]]; then
		if [[ -n "$var_extension" ]]; then
			if find "$var_path" -type f -name "*$var_extension" &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		else
			return 0;
		fi;
	else
		return 1;
	fi;
}


# Test_File.
#	DESCRIPTION : check file.
#	RETURN : code.
function Test_File() {
	# Some definition.
	local var_path;
	local var_overwrite;
	local var_create;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		if [ -f "$var_path" ]; then
			if [[ "$var_overwrite" = true ]]; then
				rm "$var_path" &> /dev/null;
				Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				touch "$var_path" &> /dev/null;
			fi;
		else
			if [[ "$var_overwrite" = true ]] || [[ "$var_create" = true ]]; then
				Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				touch "$var_path" &> /dev/null;
			fi;
		fi;
		if [ -f "$var_path" ]; then
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else return 1; fi;
	else return 1; fi;
}


# Set_File.
#	DESCRIPTION : create file.
#	RETURN : code.
function Set_File() {
	# Some definition.
	local var_path;
	local var_raw;
	local var_source;
	local var_append;
	local var_encode;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-RAW)
				var_raw="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-APPEND)
				var_append=true
				shift
				;;
			-ENCODE)
				var_encode="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		touch "$var_path" &> /dev/null;
		# Tests : echo, cp.
		if [[ -n "$var_raw" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				echo "$var_raw" | tee -a "$var_path" > /dev/null;
			else
				# Test : base64.
				if [[ -n "$var_encode" ]]; then
					echo "$var_raw" | base64 -d > "$var_path";
				else
					echo "$var_raw" | tee "$var_path" > /dev/null;
				fi;
			fi;
		elif [[ -n "$var_source" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				cat "$var_source" >> "$var_path";
			else
				cp "$var_path" "$var_source" &> /dev/null;
			fi;
		else
			return 1;
		fi;
	else return 1; fi;
}


# Find_FileLine.
#	DESCRIPTION : find line started by pattern in a file.
#	RETURN : string or code.
function Find_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file;
	local var_line;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE)
				var_file="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file"; then
		var_line=$(grep "$var_pattern" "$var_file" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line" ]]; then echo "$var_line"; else return 1; fi;
	else
		return 1;
	fi;
}


# Compare_FileLine.
#	DESCRIPTION : search 1st line started by pattern in 1st file, compare it to 2nd file.
#	RETURN : string or code.
function Compare_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file1;
	local var_file2;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1" && Test_File -PATH "$var_file2"; then
		var_line1=$(grep "^$var_pattern" "$var_file1" | head -n 1);
		var_line2=$(grep "^$var_pattern" "$var_file2" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line1" && "$var_line1" != false && "$var_line1" != "null" && \
			-n "$var_line2" && "$var_line2" != false && "$var_line2" != "null" ]]; then
			# Test : return file number 2 content for different content, return false for same content.
			if [[ "$var_line1" != "$var_line2" ]]; then echo "$var_line2"; else return 1; fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Backup_FileLine.
#	DESCRIPTION : recover line in 1st file from a 2nd file.
#	RETURN : string or code.
#	TODO : verify character | escaping.
function Backup_FileLine() {
	# Some definition.
	local var_pattern;
	local var_default;
	local var_file1;
	local var_file2;
	local var_diff;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DEFAULT)
				var_default="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1"; then
		var_diff=$(Compare_FileLine -PATTERN "$var_pattern" -FILE1 "$var_file1" -FILE2 "$var_file2");
		var_pattern="^${var_pattern}";
		# Tests : replace by old content for different content, replace by default for same/empty content.
		if [[ -n "$var_diff" && "$var_diff" != false ]]; then
			sed -i "s|${var_pattern}|${var_diff}|" "$var_file1";
			echo "old";
		elif [[ -n "$var_default" ]]; then
			echo -e "$var_default" >> "$var_file1";
			echo "default";
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_user.sh               | #
# |__________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test-Root.
#	DESCRIPTION : check root/sudo.
#	RETURN : int.
function Test-Root {
	if [[ "$(id -u)" -eq 0 ]]; then
		return 0;
	elif groups | grep -qE '\bsudo\b|\bwheel\b'; then
		return 0;
	else
		return 1;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
# shellcheck disable=SC2329
# shellcheck disable=SC2034
#  _____________________________________________
# |                                             | #
# |               func_generic.sh               | #
# |_____________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : everything generic for all APPs.


# DATE.
declare -A DATE;
DATE["END"]="";
# y-m-d : jekyll file name markdown.
DATE["CURRENT_DATE"]="$(date "+%Y-%m-%d")";
# d-m-y - h:m:s : jekyll inside markdown file.
DATE["FULL_TIMESTAMP"]="$(date "+%Y-%m-%d %H:%M:%S")";
DATE["COMPACT_TIMESTAMP"]="$(date "+%Y%m%d_%H%M%S")";
DATE["FORMATTED_DATE_TIME"]="$(date "+%d/%m/%Y - %Hh%M")";


# COLOR.
declare -A COLOR;
COLOR[BOLD]='\e[1m';
COLOR[RED_LIGHT]='\e[1;38;5;160m';
COLOR[GREEN_LIGHT]='\e[1;38;5;34m';
COLOR[GREEN_DARK]='\e[1;38;5;28m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;179m';
COLOR[YELLOW_DARK]='\e[1;38;5;178m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;208m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';
COLOR[STOP]='\e[0m';
COLOR[TICK]="[${COLOR[GREEN_LIGHT]}✔${COLOR[STOP]}]";
COLOR[CROSS]="[${COLOR[RED_LIGHT]}✘${COLOR[STOP]}]";


# Write_Script.
#	DESCRIPTION : custom output of the APP.
#	RETURN : string.
function Write_Script() {
	# Some definition.
	local var_text;
	local var_code;
	local var_check;
	local var_checked;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-CODE)
				var_code="$2"
				shift 2
				;;
			-CHECK)
				var_check=true
				shift
				;;
			-CHECKED)
				var_checked=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then 
		# Tests : state, change former line.
		if [ -n "$var_check" ]; then
			echo -ne "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		elif [ -n "$var_checked" ]; then
			if [[ "$var_code" -eq 1 ]]; then
				echo -e "\\r ${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[CROSS]} ERROR : $var_text";
			elif [[ "$var_code" -eq 0 ]]; then
				echo -e "\\r ${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[TICK]} $var_text";
			fi;
		else
			echo -e "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		fi;
	fi;
}


# Get_Help.
#	DESCRIPTION : usage of the APP.
#	RETURN : string.
function Get_Help() {
	# Some definition.
	# requirement.
	local var_requirement;
	local var_requirement_text;
	IFS='#' read -ra var_requirement <<< "${APP[REQUIREMENT]}";
	for element in "${var_requirement[@]}"; do
		var_requirement_text+="$(echo -e "\t${element}\n ")";
	done;
	# param.
	local var_param;
	local var_param_text;
	IFS=' ' read -ra var_param <<< "${APP[PARAM]}";
	for element in "${var_param[@]}"; do var_param_text+="$element "; done;
	local var_param_dsc;
	local var_param_dsc_count;
	local var_param_dsc_text;
	IFS='#' read -ra var_param_dsc <<< "${APP[PARAM_DSC]}";
	local var_param_dsc_count=0
	for element in "${var_param_dsc[@]}"; do
		var_param_dsc_text+="$(echo -e "\t-${var_param[$var_param_dsc_count]} [ value ]\n\t\t${element}\n ")";
		var_param_dsc_count="$((var_param_dsc_count+1))";
	done;
	# option.
	local var_option;
	local var_option_text;
	IFS=' ' read -ra var_option <<< "${APP[OPTION]}";
	for element in "${var_option[@]}"; do var_option_text+="$element "; done;
	local var_option_dsc;
	local var_option_dsc_count;
	local var_option_dsc_text;
	IFS='#' read -ra var_option_dsc <<< "${APP[OPTION_DSC]}";
	local var_option_dsc_count=0
	for element in "${var_option_dsc[@]}"; do
		var_option_dsc_text+="$(echo -e "\t-${var_option[$var_option_dsc_count]}\n\t\t${element}\n ")";
		var_option_dsc_count="$((var_option_dsc_count+1))";
	done;
	# note.
	local var_note;
	local var_note_text;
	IFS='#' read -ra var_note <<< "${APP[NOTE]}";
	for element in "${var_note[@]}"; do
		var_note_text+="$(echo -e "\t${element}\n ")";
	done;
	# changelog.
	local var_changelog;
	local var_changelog_text;
	IFS='#' read -ra var_changelog <<< "${APP[CHANGELOG]}";
	for element in "${var_changelog[@]}"; do
		var_changelog_text+="$(echo -e "\t${element}\n ")";
	done;
	Restore_IFS;
cat <<EOUSAGE

SYNOPSIS
	${APP[NAME]} - ${APP[DESCRIPTION]}

HOME
	${APP[URL]}

REQUIREMENT
$var_requirement_text
UTILISATION
	${APP[NAME]} PARAM [ $var_param_text] OPTION [ $var_option_text]

PARAM
$var_param_dsc_text
OPTION
$var_option_dsc_text
EXAMPLE
	${APP[EXAMPLE]}

TO-DO
	${APP[TODO]}

NOTE
$var_note_text
CREDIT & THANKS
	${APP[CREDIT]}

CHANGELOG
$var_changelog_text
METADATA
	Author : ${APP[AUTHOR]}
	Version : ${APP[VERSION]}
	License : ${APP[LICENSE]}

EOUSAGE
}


# Test_Param.
#	DESCRIPTION : test arguments.
#	RETURN : string.
function Test_Param() {
	# Some definition.
	local var_param;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PARAM)
				var_param="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests :param mispelled, empty.
	if [[ "$var_param" == "-"* ]]; then
		Stop_App  -CODE 1 -TEXT "bad parameter (cant' starting by '-').";
	elif [[ -z "$var_param" ]]; then
		Stop_App  -CODE 1 -TEXT "bad parameter (cant' be empty).";
	fi;
}


# Get_StartingApp.
#	DESCRIPTION : script starter pack.
#	RETURN : string.
function Get_StartingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Title.
		echo -e "${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: ${CONF[DATE_START]} ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
		# Place & soft env.
		echo -e "- work in '${CONF[PATH]}'.";
		if [ -n "$SNAP" ]; then
			echo -e "- Snap environment detected.";
		elif [ -n "$FLATPAK_ID" ]; then
			echo -e "- Flatpak environment detected.";
		fi;
		echo "";
	fi;
	# Test : requirement.
	if [[ -n ${APP[REQUIREMENT]} && ${APP[REQUIREMENT]} != "x" ]]; then
		# Some definition.
		local var_element;
		local var_type;
		local var_name;
		local var_dependencie;
		local var_software;
		IFS='#';
		# Loop : requirement.
		for var_element in ${APP[REQUIREMENT]}; do
			# Space delimiter.
			var_type=$(echo "$var_element" | cut -d ' ' -f 1);
			var_name=$(echo "$var_element" | cut -d ' ' -f 2);
			var_dependencie=$(echo "$var_element" | cut -d ' ' -f 3);
			var_software=$(Test_Software -NAME "$var_name" -TYPE "$var_type");
			# Test : software.
			if [[ "$var_software" == "available" ]]; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- requirement '${var_type}' ${var_name} available.";
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- requirement '${var_type}' ${var_name} not available (error '${var_software}').";
				fi;
				# Test : dependencie.
				if [[ "$var_dependencie" == "mandatory" ]]; then
					Stop_App  -CODE 1 -TEXT "'${var_type}' ${var_name} mandatory.";
				fi;
			fi;
		done;
		Restore_IFS;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Output (data backup).
	if [[ "${PARAM[BACKUP]}" == true ]]; then
		# Test : output date name mode, output custom name mode (two files).
		if [[ "${PARAM[OUTPUT]}" =~ ${DEFAULT[OUTPUT]}\.(csv|xls|xlsx|json|txt)$ ]]; then
			# Test : get previous output old.
			if [[ -n "${PARAM[OUTPUT_BACKUP]}" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) in '${PARAM[OUTPUT_BACKUP]}'.";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (date name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (date name mode).";
				fi;
			fi;
		else
			# Test : move previous output to old.
			if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT]}"; then
					mv "${PARAM[OUTPUT]}" "${PARAM[OUTPUT_BACKUP]}";
					# Test : move all file formats.
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.json"; then
						mv "${PARAM[OUTPUT]%.csv}.json" "${PARAM[OUTPUT_BACKUP]%.csv}.json";
					fi;
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) moved to '${PARAM[OUTPUT_BACKUP]}'.";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (custom name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (custom name mode).";
				fi;
			fi;
		fi;
		if [[ "${PARAM[QUIET]}" != true && -z "${PARAM[OUTPUT]}" ]]; then echo ""; fi;
	fi;
	# Output (main).
	if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" && "${PARAM[RAW]}" != true ]]; then
		if Test_File -PATH "${PARAM[OUTPUT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current output to '${PARAM[OUTPUT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "output can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
	# Report.
	if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]]; then
		if Test_File -PATH "${PARAM[REPORT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- report to '${PARAM[REPORT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "report can't be writed.";
		fi;
		if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
	fi;
}


# Get_EndingApp.
#	DESCRIPTION : script ending pack.
#	RETURN : string.
function Get_EndingApp() {
	# Test : verbose.
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Some definition.
		local var_date_start;
		local var_date_end;
		local var_date_end_seconds;
		var_date_start=$(date -d "$(date "+%Y-%m-%d %H:%M:%S")" "+%s");
		var_date_end=$(date "+%s");
		var_date_end_seconds=$((var_date_end - var_date_start));
		echo -e "\n${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: executed in ${var_date_end_seconds} seconds ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
	fi;
}


# Stop_App.
#	DESCRIPTION : end program.
#	RETURN : string and code.
function Stop_App() {
	# Some definition.
	local var_code;
	local var_text;
	local var_quiet;
	local var_help;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CODE)
				var_code="$2"
				shift 2
				;;
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-QUIET)
				var_quiet=true
				shift
				;;
			-HELP)
				var_help=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : splash error.
	if [[ "$var_quiet" != true && -n "$var_text" ]]; then
		Write_Script -TEXT "EXIT ERROR ! ${var_text}";
	fi;
	# Test : splash man.
	if [[ "$var_help" == true ]]; then Get_Help; fi;
	wait;
	exit "$var_code";
}


# Debug_Start.
#	DESCRIPTION : debug app.
function Debug_Start() { set -x; }


# Debug_Stop.
#	DESCRIPTION : debug app.
function Debug_Stop() { set +x; }


# Add_Book.
#	DESCRIPTION : construct associative array.
function Add_Book() {
	# Some definition.
	local var_name="$1";
	# Test : book exists.
	if ! declare -p "$var_name" &>/dev/null; then declare -gA "$var_name"; fi;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		local var_mode;
		local var_current_index;
		local var_current_release;
		local var_archive_index;
		local var_archive_release;
		local var_element_suffixes;
		local var_element_suffixes_var;
		local var_array;
		local var_array_keys;
		local var_array_suffixes;
		# mode, indexes & releases.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release");
		# Loop : var_suffix key.
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			var_element_suffixes_var="var_${var_element_suffixes}";
			declare "${var_element_suffixes_var}"="${var_element}_${var_element_suffixes}";
		done;
		# Construct index & release values from Get_Distribution.
		IFS=',' read -ra var_array <<< "$var_data";
		Restore_IFS;
		declare -a var_array_keys=("$var_mode" "$var_current_index" "$var_current_release" "$var_archive_index" "$var_archive_release");
		# Loop var_suffix value.
		for i in "${!var_array_keys[@]}"; do
			eval "${var_name}[\"${var_array_keys[$i]}\"]=\"${var_array[$i]}\"";
		done;
	elif [[ "$var_name" == "TMP" ]]; then
		# Some definition.
		local var_element="$2";
		local var_data="$3";
		eval "${var_name}[\"${var_element}\"]=\"${var_data}\"";
	fi;
}


# Get_Book.
#	DESCRIPTION : get associative array.
#	RETURN : string or code.
function Get_Book() {
	# Some definition.
	local var_name;
	local var_element;
	local var_data;
	local var_array_suffixes;
	local var_element_suffixes;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-ELEMENT)
				var_element="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRIBUTION" ]]; then
		# Loop: var_suffix key.
		declare -a var_array_suffixes=("mode" "current_index" "current_release" "archive_index" "archive_release")
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			if [[ "$var_data" == "$var_element_suffixes" ]]; then var_result="${DISTRIBUTION[${var_element}_$var_element_suffixes]}"; break; fi;
		done;
	fi;
	# Test : return.
	if [[ -n "$var_result" && "$var_result" != "" ]]; then echo "$var_result"; else return 1; fi;
}


# Remove_Book.
#	DESCRIPTION : remove associative array.
function Remove_Book() {
	# Some definition.
	local var_name="$1";
	unset "$var_name";
}


# Remove_DuplicateInString.
#	DESCRIPTION : remove duplicate value in string.
#	RETURN : string.
function Remove_DuplicateInString() {
	# Some definition.
	local var_string;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	echo "$var_string" | tr ' ' '\n' | sort | uniq | xargs;
}


# Search_InArray.
#	DESCRIPTION : search if value exists in array.
#	RETURN : code.
function Search_InArray() {
	# Some definition.
	local var_search;
	local var_array;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-SEARCH)
				var_search="$2"
				shift 2
				;;
			-ARRAY)
				var_array="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : need all param.
	if [ -z "$var_search" ] || [ -z "$var_array" ]; then return 1; fi;
	# IFS for delimit array.
	IFS=" " read -ra var_array_read <<< "$var_array";
	Restore_IFS;
	# Loop : search in array.
	for element in "${var_array_read[@]}"; do
		if [ "$element" == "$var_search" ]; then return 0; fi;
	done;
	return 1;
}


# Measure_MaxInArray.
#	DESCRIPTION : calculate longest word in array.
#	RETURN : string.
function Measure_MaxInArray() {
	# Some definition.
	local var_count;
	local var_array;
	local var_word;
	local var_length;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		var_count=0;
		for element in "${!var_array[@]}"; do
			var_word="${var_array[$element]}";
			var_length=${#var_word};
			if [ "$var_length" -gt "$var_count" ]; then var_count="$var_length"; fi;
		done;
		echo "$var_count";
	fi;
}


# Set_RepeatCharacter.
#	DESCRIPTION : calculate how many same character repeat needed.
#	RETURN : string.
function Set_RepeatCharacter() {
	# Some definition.
	local var_character;
	local var_count;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-CHARACTER)
				var_character="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : if arg empty.
	if [ -z "$var_character" ] || [ -z "$var_count" ]; then
		return 1;
	else
		var_result="";
		for ((i=0; i < "$var_count"; i++)); do
			var_result+="$var_character";
		done;
	fi;
	echo "$var_result";
}


# Limit_StringLength.
#	DESCRIPTION : limit number of character in a string.
#	RETURN : string.
#	TODO : replace -DIRECTION by -POSITION.
function Limit_StringLength() {
	# Some definition.
	local var_string;
	local var_count;
	local var_count_before;
	local var_count_after;
	local var_direction;
	local var_spacer;
	local var_spacer_length;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			-DIRECTION)
				var_direction="$2"
				shift 2
				;;
			-SPACER)
				var_spacer="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some defaults.
	if [ -z "$var_direction" ]; then var_direction="after"; fi;
	if [ -z "$var_spacer" ]; then var_spacer="(...)"; fi;
	var_spacer_length=${#var_spacer};
	# Test : if arg empty.
	if [ -z "$var_string" ] || [ -z "$var_count" ]; then
		return 1;
	else
		# Test : cut needed.
		if [ ${#var_string} -gt "$var_count" ]; then
			# Add spacer length to cut count.
			var_count_before=$(( ${#var_string}-var_count+var_spacer_length+1 ));
			var_count_after=$(( var_count-var_spacer_length ));
			# Tests : sub place.
			if [ "$var_direction" = "before" ]; then
				var_result+="$var_spacer";
				var_result+=$(echo "$var_string" | cut -c "$var_count_before"-"${#var_string}");
			elif [ "$var_direction" = "after" ]; then
				var_result+=$(echo "$var_string" | cut -c 1-"$var_count_after");
				var_result+="$var_spacer";
			fi;
			echo "$var_result";
		else
			echo "$var_string";
		fi;
	fi;
}


# Get_TableHeader.
#	DESCRIPTION : display a table index.
#	RETURN : string.
function Get_TableHeader() {
	# Some definition.
	local var_array;
	local var_element;
	local var_param;
	local var_param_length;
	local var_dot_length;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a name param and the max character count a value of this param can be in the rows.
		for element_header in "${!var_array[@]}"; do
			var_element="${var_array[$element_header]}";
			var_param=$(echo "$var_element" | cut -d ":" -f1);
			var_param_length="${#var_param}";
			var_dot_length=$(echo "$var_element" | cut -d ":" -f2);
			# Calculate how many characters to add more.
			var_param_length_more=$(( var_dot_length-var_param_length ));
			var_dot_length_more=$(( var_param_length-var_dot_length ));
			# Ready to construct header.
			var_line1+="$var_param";
			var_line1+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_param_length_more");
			var_line1+="  ";
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length");
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length_more");
			var_line2+="  ";
		done;
		echo -e "$var_line1";
		echo -e "$var_line2";
	fi;
}


# Get_TableRow.
#	DESCRIPTION : display a table row.
#	RETURN : string.
function Get_TableRow() {
	# Some definition.
	local var_array;
	local var_action;
	local var_element;
	local var_value;
	local var_value_length;
	local var_max;
	local var_line;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			-ACTION)
				var_action="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a value, the max character count this value can be.
		for element_row in "${!var_array[@]}"; do
			var_element="${var_array[$element_row]}";
			var_value=$(echo "$var_element" | cut -d ":" -f1);
			var_max=$(echo "$var_element" | cut -d ":" -f2);
			# Test : tick cross.
			if [[ "$var_value" == "tick" || "$var_value" == "cross" ]]; then
				if [[ "$var_value" == "tick" ]]; then
					var_value="${COLOR[TICK]}";
				elif [[ "$var_value" == "cross" ]]; then
					var_value="${COLOR[CROSS]}";
				fi;
				var_value_length=3;
			# TODO
			# elif [[ "$var_value" == *"tick"* || "$var_value" == *"cross"* ]]; then
			else
				var_value_length="${#var_value}";
			fi;
			# Calculate how many space to add more.
			var_value_length_more=$(( var_max-var_value_length ));
			# Ready to construct row.
			var_line+="$var_value";
			var_line+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_value_length_more");
			var_line+="  ";
		done;
		# Test : replace line.
		if [[ "$var_action" == "check" ]]; then
			echo -ne "$var_line";
		else
			echo -e "\\r$var_line";
		fi;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_pkgs.sh               | #
# |__________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Test_Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Test_Software() {
	# Some definition.
	local var_result;
	local var_name;
	local var_type;
	#local var_action; // SC2034 disable.
	#local var_depend; // SC2034 disable.
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-ACTION)
				#var_action="$2" // SC2034 disable.
				shift 2
				;;
			-DEPEND)
				#var_depend=true // SC2034 disable.
				shift
				;;
			*)
				shift
				;;
		esac
	done
	# Tests : type software.
	if [[ "$var_type" == "program" ]]; then
		# TMP : import OverDeploy routine.
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-program";
		fi;
	elif [[ "$var_type" == "service" ]]; then
		# Test : systemd check.
		# SRC : https://stackoverflow.com/a/53640320/2704550.
		if [[ $(systemctl list-units --all --type service --full --no-legend "${var_name}.service" | sed 's/^\s*//g' | cut -f1 -d' ') == "${var_name}.service" ]]; then
			var_result="available";
		else
			var_result="fail-service";
		fi;
	elif [[ "$var_type" == "command" ]]; then
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-command";
		fi;
	else
		# tmp default.
		var_result="fail-unknow";
	fi;
	echo "$var_result";
}


# Start_Software.
#	DESCRIPTION : test effective restarting of a service.
#	RETURN : code.
function Start_Software() {
	# Some definition.
	local var_name;
	local var_type;
	local var_command;
	local var_user;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-COMMAND)
				var_command="$2"
				shift 2
				;;
			-USER)
				var_user="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done
	# Test : daemon, service.
	if [[ "$var_type" == "daemon" ]]; then
		if $var_command &> /dev/null; then return 0; else return 1; fi;
	elif [[ "$var_type" == "service" ]]; then
		su -l "$var_user" -c "systemctl restart ${var_name}";
		# Test : service state.
		if su -l "$var_user" -c "systemctl is-active --quiet ${var_name}"; then return 0; else return 1; fi;
	fi;
}



# next added content date : 20240810_012448.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_convert.sh               | #
# |_____________________________________________| #


# ========================= #
#          CONVERT          #
#
#	DESCRIPTION : converting content for all APPs.


function Set_Placeholder1() {
	echo "placeholder";
}



# shellcheck source=/dev/null
# shellcheck disable=SC2329
# shellcheck disable=SC2034
#  _______________________________________________
# |                                               | #
# |               distro-tracker.sh               | #
# |_______________________________________________| #


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-tracker";
APP["SHORTNAME"]="DT";
APP["DESCRIPTION"]="Find out latest stable versions of GNU/Linux & BSD distributions.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENT"]="program wget mandatory#program jq mandatory";
APP["PARAM"]="SOURCE DISTRO CLI LENGTH OUTPUT REPORT BACKUP";
APP["PARAM_DSC"]="Local filepath or distant url to JSON data. | no default.#Choice between some GNU/Linux & BSD distributions or all registered. | default is 'all'.#Force CLI ISO distribution if available. | default is 'false'.#Number of valid releases needed. | default is '5'.#File path to store the final result, without extension, by date name mode or custom name mode. | default is '${PWD}/${APP[SHORTNAME]}-DATE.csv'.#Generate a JSON report file, only if new releases detected, in same output path, into a file named '*.report.json'. | default is 'false'.#Backup security for keeping and don't remove previous datas if entire website checking fails. New and old datas need to be in the same directory. | default is 'false'.";
APP["OPTION"]="QUIET HELP VERSION";
APP["OPTION_DSC"]="Be silent. | default is 'false'.#Show man.#Show version.";
APP["EXAMPLE"]="${APP[NAME]} -d \"debian,fedora,freebsd,mageia,slackware\" -l \"10\" -o \"distro-tracker\" -s -q";
APP["TODO"]="x";
APP["NOTE"]="First of all, some distributions have two general indexes to scan to retrieve the latest versions, with main and archive. Information gived in terminal output.#Alma, CentOS, Fedora & MX have a main index where everything is listed, but some versions are downloaded from another place.#Alpine, Debian, Kaisen & Ubuntu lists beta/RC versions in the index of a release.#Fedora lists in the index of a release an \"OSB\" version, a version built with osbuilder.#Alma and CentOS list \"non-existent\" versions - a version 9 which refers to a 9.3, a 9-stream twin of 9, well ...#Alma, Fedora & Ubuntu have both ui or cli mode.#Alpine & XPC-NG nead a sort with head because many releases listed in single release page.";
APP["CREDIT"]="x";
APP["CHANGELOG"]="1.0.0 - Expansion to 22 GNU/Linux distributions with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, and XCP-ng. General mechanism for retrieving version numbers using the -LENGTH parameter has been revised for more logic : not just X tested versions are returned, but now X valid versions. Addition of the -SOURCE parameter to choose tracking URLs via a local or remote JSON file. Addition of the -REPORT parameter to generate a JSON report to notify of any changes in the latest release of each distribution. Activation of the -ENV parameter. Renaming of the DISTRIB parameter to -DISTRO, -DEPTH to -LENGTH, -SECURITY to -BACKUP. Addition of one-letter shortcuts for each parameter and option. Addition of the Compare_NokFilter function to better manage and clarify versions tagged \"NOK\" (beta, RC, etc.): all pingable versions are retrieved, with a final filter on stable versions.#0.5.0 - Add -SECURITY option for copy old datas to new datas if wget fails on entire website.#0.4.0 - Up to 15 distributions by default.#0.3.0 - New strong REGEXP, Add -LENGTH parameter, Add command JQ requirement, Add JSON output, Up to 12 distributions GNU/Linux & BSD.#0.2.0 - Separate checks on index page & release page.#0.1.0 - Add CSV output.#0.0.1 - Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="1.0.0";
APP["LICENSE"]="GPL-3.0-or-later";


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

declare -gA CONF;
CONF["DEBUG"]=false;
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
CONF["SEPARATOR"]="/";
CONF["IFS"]=$IFS;
# Test & Loop : includes.
if [[ "${0##*.}" == "sh" ]]; then
	for var_file in "${CONF[PATH]}${CONF[SEPARATOR]}common${CONF[SEPARATOR]}"func_*.sh; do source "$var_file"; done;
fi;
# Dates.
CONF["DATE_START"]="$(date "+%d/%m/%Y - %Hh%M")";
CONF["DATE_START_EXEC"]="$(date "+%Y-%m-%d %H:%M:%S")";
CONF["DATE_END"]="";
CONF["DATE_FILE"]="$(date "+%Y%m%d_%H%M%S")";


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ $# -gt 0 ]]; do
	case "$1" in
		-SOURCE|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-DISTRO|-d)
			PARAM["DISTRO"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-CLI|-c)
			PARAM["CLI"]=true
			shift
			;;
		-LENGTH|-l)
			PARAM["LENGTH"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-OUTPUT|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -PARAM "$2"
			shift 2
			;;
		-REPORT|-r)
			PARAM["REPORT"]=true
			shift
			;;
		-BACKUP|-b)
			PARAM["BACKUP"]=true
			shift
			;;
		-QUIET|-q)
			PARAM["QUIET"]=true
			shift
			;;
		-HELP|-h)
			Stop_App -CODE 0 -HELP
			shift
			;;
		-VERSION|-v)
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			shift
			;;
		*)
			Stop_App -CODE 1 -TEXT "unknown parameter : '$1'." -HELP
			shift
			;;
	esac
done;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["DISTRO"]="alma,alpine,arch,centos,clonezilla,debian,fedora,freebsd,kaisen,kali,mageia,manjaro,mint,mx,opensuse,opnsense,parrot,pfsense,proxmox,slackware,ubuntu,xcpng";
# DEFAULT["DISTRO"] is a string, need array for Search_InArray custom PARAM["DISTRO"].
IFS=',' read -ra var_default_distro_array <<< "${DEFAULT["DISTRO"]}";
Restore_IFS;
DEFAULT["CLI"]=false;
DEFAULT["LENGTH"]="5";
DEFAULT["OUTPUT"]="${PWD}${CONF[SEPARATOR]}${APP[SHORTNAME]}-${CONF[DATE_FILE]}";
DEFAULT["REPORT"]=false;
DEFAULT["BACKUP"]=false;
DEFAULT["QUIET"]=false;
# Some script definitions.
if [ -z "${PARAM[SOURCE]}" ]; then
	# Default book DISTRIBUTION.
	Add_Book "DISTRIBUTION" "alma" "uicli,https://repo.almalinux.org/almalinux,https://repo.almalinux.org/almalinux/xxx/isos/x86_64,,https://vault.almalinux.org/xxx/isos/x86_64";
	Add_Book "DISTRIBUTION" "alpine" "cli,https://dl-cdn.alpinelinux.org/alpine,https://dl-cdn.alpinelinux.org/alpine/xxx/releases/x86_64,,";
	Add_Book "DISTRIBUTION" "arch" "ui,https://geo.mirror.pkgbuild.com/iso,https://geo.mirror.pkgbuild.com/iso/xxx,,";
	Add_Book "DISTRIBUTION" "centos" "ui,https://mirror.in2p3.fr/pub/linux/centos-stream,https://mirror.in2p3.fr/pub/linux/centos-stream/xxx/BaseOS/x86_64/iso,https://mirror.in2p3.fr/pub/linux/centos-vault,https://mirror.in2p3.fr/pub/linux/centos-vault/xxx/isos/x86_64";
	Add_Book "DISTRIBUTION" "clonezilla" "ui,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable/xxx,,";
	Add_Book "DISTRIBUTION" "debian" "ui,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/mirror/cdimage/archive,https://cdimage.debian.org/mirror/cdimage/archive/xxx/amd64/iso-dvd";
	Add_Book "DISTRIBUTION" "fedora" "uicli,https://mirror.in2p3.fr/pub/fedora/linux/releases,https://mirror.in2p3.fr/pub/fedora/linux/releases/xxx/Workstation/x86_64/iso,,https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/xxx/Workstation/x86_64/iso";
	Add_Book "DISTRIBUTION" "freebsd" "ui,https://download.freebsd.org/releases/ISO-IMAGES,https://download.freebsd.org/releases/ISO-IMAGES/xxx,,";
	Add_Book "DISTRIBUTION" "kaisen" "ui,https://iso.kaisenlinux.org/rolling,https://iso.kaisenlinux.org/rolling,,";
	Add_Book "DISTRIBUTION" "kali" "ui,https://cdimage.kali.org,https://cdimage.kali.org/kali-xxx,,";
	Add_Book "DISTRIBUTION" "mageia" "ui,https://fr2.rpmfind.net/linux/mageia/iso,https://fr2.rpmfind.net/linux/mageia/iso/xxx/Mageia-xxx-x86_64,,";
	Add_Book "DISTRIBUTION" "manjaro" "ui,https://manjaro.org/products/download/x86,https://download.manjaro.org/kde/xxx,,";
	Add_Book "DISTRIBUTION" "mx" "ui,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Old,https://sourceforge.net/projects/mx-linux/files/Old/MX-xxx/KDE";
	Add_Book "DISTRIBUTION" "mint" "ui,https://mirrors.edge.kernel.org/linuxmint/stable,https://mirrors.edge.kernel.org/linuxmint/stable/xxx,,";
	Add_Book "DISTRIBUTION" "opensuse" "ui,https://fr2.rpmfind.net/linux/opensuse/distribution/leap,https://fr2.rpmfind.net/linux/opensuse/distribution/leap/xxx/iso,,";
	Add_Book "DISTRIBUTION" "opnsense" "cli,https://mirror.wdc1.us.leaseweb.net/opnsense/releases,https://mirror.wdc1.us.leaseweb.net/opnsense/releases/xxx,,";
	Add_Book "DISTRIBUTION" "parrot" "ui,https://deb.parrot.sh/parrot/iso,https://deb.parrot.sh/parrot/iso/xxx,,";
	Add_Book "DISTRIBUTION" "pfsense" "cli,https://atxfiles.netgate.com/mirror/downloads,https://atxfiles.netgate.com/mirror/downloads,,";
	Add_Book "DISTRIBUTION" "proxmox" "cli,https://enterprise.proxmox.com/iso,https://enterprise.proxmox.com/iso,,";
	Add_Book "DISTRIBUTION" "slackware" "ui,https://mirrors.slackware.com/slackware/slackware-iso,https://mirrors.slackware.com/slackware/slackware-iso/slackware64-xxx-iso,,";
	Add_Book "DISTRIBUTION" "ubuntu" "uicli,https://releases.ubuntu.com,https://releases.ubuntu.com/xxx,,";
	Add_Book "DISTRIBUTION" "xcpng" "cli,https://updates.xcp-ng.org/isos,https://updates.xcp-ng.org/isos/xxx,,";
else
	# Custom book DISTRIBUTION.
	if [[ "${PARAM[SOURCE]}" =~ ^(http|www) ]]; then
		var_data_distro="$(Get_Distant -URL "${PARAM[SOURCE]}" -TYPE "string")";
	else
		if Test_File -PATH "${PARAM[SOURCE]}"; then
			var_data_distro="$(<"${PARAM[SOURCE]}")";
		else
			Stop_App -CODE 1 -TEXT "file in '-SOURCE' parameter is not available.";
		fi;
	fi;
	# Loop : Process Substitution.
	while read -ra var_data_distro_array; do
		var_param_file_distro=$(echo "$var_data_distro_array" | jq -c -r '.name');
		var_param_file_values=$(echo "$var_data_distro_array" | jq -c -r '[.mode, .current_index, .current_release, .archive_index, .archive_release] | join(",")');
		# Test : invalid distribution.
		if Search_InArray -SEARCH "$var_param_file_distro" -ARRAY "${var_default_distro_array[*]}"; then
			Add_Book "DISTRIBUTION" "$var_param_file_distro" "$var_param_file_values";
		else
			Stop_App -CODE 1 -TEXT "unknown distribution : '$var_param_file_distro'.";
		fi;
	done < <(echo "$var_data_distro" | jq -c -r '.distro[]');
fi;
if [ -z "${PARAM[DISTRO]}" ] || [[ "${PARAM[DISTRO]}" == "all" ]]; then
	PARAM["DISTRO"]="${DEFAULT[DISTRO]}";
else
	PARAM["DISTRO"]=$(echo "${PARAM["DISTRO"]}" | tr ',' '\n' | sort | tr '\n' ',');
fi;
IFS=',' read -ra var_param_distro_array <<< "${PARAM["DISTRO"]}";
Restore_IFS;
# Loop : distribution.
for var_param_distro_array_element in "${var_param_distro_array[@]}"; do
	# Test : invalid distribution.
	if ! Search_InArray -SEARCH "$var_param_distro_array_element" -ARRAY "${var_default_distro_array[*]}"; then
		Stop_App -CODE 1 -TEXT "unknown distribution : '$var_param_distro_array_element'.";
	fi;
done;
if [ -z "${PARAM[CLI]}" ]; then PARAM["CLI"]="${DEFAULT[CLI]}"; fi;
if [ -z "${PARAM[LENGTH]}" ]; then PARAM["LENGTH"]="${DEFAULT[LENGTH]}"; fi;
if [ -z "${PARAM[OUTPUT]}" ]; then
	# Loop : last file win previous data state.
	for var_param_output_single_file in "${PWD}${CONF[SEPARATOR]}${APP[SHORTNAME]}"*.csv; do
		PARAM["OUTPUT_BACKUP"]="$var_param_output_single_file";
	done;
	if [[ "${PARAM[REPORT]}" == true ]]; then PARAM["REPORT"]=${DEFAULT[OUTPUT]}.report.json; fi;
	PARAM["OUTPUT"]=${DEFAULT[OUTPUT]}.csv;
else
	PARAM["OUTPUT_BACKUP"]=${PARAM[OUTPUT]%/}.archive.csv;
	if [[ "${PARAM[REPORT]}" == true ]]; then PARAM["REPORT"]=${PARAM[OUTPUT]%/}.report.json; fi;
	PARAM["OUTPUT"]=${PARAM[OUTPUT]%/}.current.csv;
fi;
if [ -z "${PARAM[REPORT]}" ]; then PARAM["REPORT"]="${DEFAULT[REPORT]}"; fi;
if [ -z "${PARAM[BACKUP]}" ]; then PARAM["BACKUP"]="${DEFAULT[BACKUP]}"; fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Get_AllVersion.
#	DESCRIPTION : parse HTML index page with some regex.
#	RETURN : string.
function Get_AllVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_data_current;
	local var_data_archive;
	local var_parse;
	local var_parse_current;
	local var_parse_archive;
	local var_array_release;
	local var_array_sorted_release;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_release");
	# Construct HTML data.
	var_data_current=$(Get_Distant -URL "$var_distribution_data_current_index" -TYPE "string");
	if [[ "$var_distribution_data_archive_index" != "" ]]; then
		var_data_archive=$(Get_Distant -URL "$var_distribution_data_archive_index" -TYPE "string");
	fi;
	# Tests : construct parsing for the distribution.
	if [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS.
		#
		var_parse_current=$(Get_AllVersionGeneric -DATA "$var_data_current");
		var_parse_archive=$(Get_AllVersionGeneric -DATA "$var_data_archive");
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "clonezilla" ]]; then
		#
		# CLONEZILLA.
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | tr '\n' ',');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '<a[^>]*href="[^"]*.iso"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | grep -Po '(?<=href=")[^?"]*' | sed 's/debian-//g' | sed 's/-amd64-DVD-1.iso//g' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '(?s)(<td class="indexcolname">.*?</td>)' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/"[A-Za-z][^"]*"/""/g' | sed 's/<a href="[^"]*-live">//g' | grep -Po '(?<=href=")[^?"]*' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX.
		#
		var_parse_current=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po '(?<=/MX-)[^"]+(?=_KDE_x64.iso/download")' | tr '\n' ',');
		var_parse_archive=$(echo "$var_data_archive" | grep -Po '<body.*</body>' | grep -Po '<table id="files_list">.*</table>' | grep -Po '<tr[^>]*title="[^"]*"[^>]*>' | grep -Po 'title="[^"]*"' | sed 's/"[^0-9]*"//g' | sed 's/title=//g' | sed 's/"//g' | sed 's/MX-//g' | tr '\n' ',');
		# Test : two indexes required.
		if [[ -n "$var_parse_current" && -n "$var_parse_archive" ]]; then var_parse="$var_parse_archive,$var_parse_current"; fi;
	elif [[ "$var_distribution" =~ ^(kaisen|manjaro|pfsense|proxmox)$ ]]; then
		#
		# KAISEN, MANJARO, PFSENSE, PROXMOX.
		#
		#
		var_parse=$(echo "$var_data_current" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="(kaisenlinuxrolling|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)[^"]*(-amd64-MATE.iso|manjaro-kde-|-RELEASE-amd64.iso.gz|\d+\.iso)"' | sed 's/href="//g' | sed 's/"//g' | sed -E 's/(kaisenlinuxrolling|https:\/\/download.manjaro.org\/kde\/|pfSense-CE-|proxmox-ve_)//g' | sed -E 's/(-amd64-MATE.iso|\/manjaro-kde-.*.iso|-RELEASE-amd64.iso.gz|.iso)//g' | tr '\n' ',');
	else
		#
		# GENERIC.
		#
		var_parse=$(Get_AllVersionGeneric -DATA "$var_data_current");
	fi;
	# Test : data exists & NO SPACE ALLOWED.
	if [[ -n "$var_parse" && "$var_parse" != "" && "$var_parse" != "null" && ! "$var_parse" =~ " " ]]; then
		# Loop : construct & sort by most recent versions.
		IFS=',' read -ra var_array_release <<< "$var_parse";
		mapfile -t var_array_sorted_release < <(printf "%s\n" "${var_array_release[@]}" | sort -Vr);
		# Construct return.
		var_result=("$(IFS=,; echo "${var_array_sorted_release[*]}")");
		Restore_IFS;
		echo "${var_result[@]}";
	else
		echo "";
	fi;
}

# Get_AllVersionGeneric.
#	DESCRIPTION : parse some HTML index page with single regex.
#	RETURN : string.
function Get_AllVersionGeneric() {
	# Some definition.
	local var_data;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	var_result=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | sed 's/title="[^"]*"//g' | sed 's/\.\.//g' | sed 's/\///g' | sed 's/"[^0-9]*"//g' | sed 's/<a href="RPM-[^"]*">//g' | sed 's/<a href="almalinux-[^"]*">//g' | sed 's/<a href="slackware-[^"]*">//g' | grep -Po '(?<=href=")[^?"]*' | sed "s/\\bkali-//g" | sed "s/\\bv//g" | sed "s/\\bslackware64-//g" | sed "s/-iso\\b//g" | tr '\n' ',');
	echo "$var_result";
}

# Get_SingleVersion.
#	DESCRIPTION : parse HTML release page with some regex.
#	RETURN : string.
function Get_SingleVersion() {
	# Some definition.
	local var_distribution;
	local var_distribution_data_current_index;
	local var_distribution_data_current_release;
	local var_distribution_data_archive_index;
	local var_distribution_data_archive_release;
	local var_version;
	local var_position;
	local var_url;
	local var_filename;
	local var_date;
	local var_data;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_distribution="$2"
				shift 2
				;;
			-VERSION)
				var_version="$2"
				shift 2
				;;
			-POSITION)
				var_position="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct index & release values from Get_Book_Distribution.
	var_distribution_data_current_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_index");
	var_distribution_data_current_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "current_release");
	var_distribution_data_archive_index=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_index");
	var_distribution_data_archive_release=$(Get_Book -NAME DISTRIBUTION -ELEMENT "$var_distribution" -DATA "archive_release");
	# Test : construct pre-built url & date.
	var_url="${var_distribution_data_current_release//xxx/$var_version}";
	# Tests : construct return filename:url.
	if [[ "$var_distribution" == "alma" ]]; then
		#
		# ALMA - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"isos/"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		fi;
		# Test: CLI usage.
		if [[ -n "${PARAM[CLI]}" && "${PARAM[CLI]}" == true ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-minimal.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's#<a [^>]*latest[^>]*>[^<]*</a>##g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="AlmaLinux-(.*?)-x86_64-dvd.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "alpine" ]]; then
		#
		# ALPINE - many releases by release page (need sort).
		#
		var_url="${var_distribution_data_current_release//xxx/v${var_version}}";
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-standard-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"alpine-${var_version}.[0-9]-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
		fi;
	elif [[ "$var_distribution" == "arch" ]]; then
		#
		# ARCH - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}-x86_64.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "centos" ]]; then
		#
		# CENTOS - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test : change url if archive.
		if [[ "$var_data" != *"iso"* ]]; then
			var_url="${var_distribution_data_archive_release//xxx/$var_version}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-LiveDVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-netinstall.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*latest-x86_64-dvd1.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "clonezilla" ]]; then
		#
		# CLONEZILLA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-amd64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed "s/.*\/$var_version\///g" | sed 's/\/download//g');
	elif [[ "$var_distribution" == "debian" ]]; then
		#
		# DEBIAN - one release by release page.
		# remember to match DVD-1 pattern with old releases.
		#
		# Test : fix debian separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="indexlist">(.*?)</table>' | grep -Po '<td class="indexcolname">(.*?)</td>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"debian-${var_version}-amd64-DVD-1.iso\"" | sed 's/href="//g' | sed 's/"//g' | head -n 1);
	elif [[ "$var_distribution" == "fedora" ]]; then
		#
		# FEDORA - one release by release page, uicli.
		# remember to not match -osb- pattern.
		#
		# Test: CLI usage.
		if [[ -n "${PARAM[CLI]}" && "${PARAM[CLI]}" == true ]]; then
			var_url="${var_url//Workstation/Server}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_url="${var_url//Workstation/Server}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Server-dvd-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			# Test : change url if archive.
			if [[ "$var_data" != *"iso"* ]]; then
				var_url="${var_distribution_data_archive_release//xxx/$var_version}";
				var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			fi;
			var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="Fedora-Workstation-Live-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
			# Tests : nice different naming convention years after years.
			if [[ "$var_filename" != *".iso"* ]]; then
				var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-Live-Workstation-x86_64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
				if [[ "$var_filename" != *".iso"* ]]; then
					var_url="${var_url//$var_version\/Workstation\/x86_64\/iso/$var_version\/Fedora\/x86_64\/iso}";
					var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
					var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'Fedora-[^"]*-x86_64-DVD.iso"' | sed 's/href="//g' | sed 's/"//g');
				fi;
			fi;
		fi;
	elif [[ "$var_distribution" == "freebsd" ]]; then
		#
		# FREEBSD - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"FreeBSD-${var_version}-RELEASE-amd64-dvd1.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kaisen" ]]; then
		#
		# KAISEN - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}[^\"]*-amd64-MATE.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "kali" ]]; then
		#
		# KALI - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="kali-linux-[^"]*-installer-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mageia" ]]; then
		#
		# MAGEIA - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "manjaro" ]]; then
		#
		# MANJARO - one release in one page.
		#
		var_data=$(Get_Distant -URL "$var_distribution_data_current_index" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*${var_version}/manjaro-kde-[^\"]*.iso\"" | sed 's/href="//g' | sed 's/"//g' | sed 's/.*\///g');
	elif [[ "$var_distribution" == "mint" ]]; then
		#
		# MINT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*cinnamon-64bit.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "mx" ]]; then
		#
		# MX - one release by release page.
		#
		# Test : fix mx separate ways.
		if [[ "$var_position" != 0 ]]; then var_url="${var_distribution_data_archive_release//xxx/$var_version}"; fi;
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*_x64.iso/download"' | sed 's/href="//g' | sed 's/"//g' | sed 's/.*KDE\///g' | sed 's/\/download//g');
		# Test : nice different naming convention years after years.
		if [[ "$var_filename" != *".iso"* ]]; then
			var_url="${var_url//\/KDE/}";
			var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
			var_filename=$(echo "$var_data" | grep -Po '<table id="files_list">(.*?)</table>'  | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*MX-${var_version}_x64.iso/download\"" | sed 's/href="//g' | sed 's/"//g' | sed "s/.*MX-${var_version}\///g" | sed 's/\/download//g');
		fi;
	elif [[ "$var_distribution" == "opensuse" ]]; then
		#
		# OPENSUSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/openSUSE-[^"]*-Build[^"]*.iso//g' | sed 's/openSUSE-[^"]*-Current[^"]*.iso//g' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="openSUSE-[^"]*-DVD-x86_64[^>]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "opnsense" ]]; then
		#
		# OPNSENSE - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po "<table.*</table>" | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*.iso.bz2"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "parrot" ]]; then
		#
		# PARROT - one release by release page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="[^"]*-security-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "pfsense" ]]; then
		#
		# PFSENSE - all releases in one page.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"[^\"]*-${var_version}-[^\"]*.iso.gz\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "proxmox" ]]; then
		#
		# PROXMOX - all releases in one page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"proxmox-ve_${var_version}.iso\"" | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "slackware" ]]; then
		#
		# SLACKWARE - one release by release page.
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="slackware64-[^"]*.iso"' | sed 's/href="//g' | sed 's/"//g');
	elif [[ "$var_distribution" == "ubuntu" ]]; then
		#
		# UBUNTU - one release by release page, uicli.
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		# Test: CLI usage.
		if [[ -n "${PARAM[CLI]}" && "${PARAM[CLI]}" == true ]]; then
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-live-server-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		else
			var_filename=$(echo "$var_data" | grep -Po '<table.*</table>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po 'href="ubuntu-[^"]*-desktop-amd64.iso"' | sed 's/href="//g' | sed 's/"//g');
		fi;
	elif [[ "$var_distribution" == "xcpng" ]]; then
		#
		# XCP-NG - many releases by release page (need sort).
		#
		#
		var_data=$(Get_Distant -URL "$var_url" -TYPE "string");
		var_filename=$(echo "$var_data" | grep -Po '<body.*</body>' | grep -Po '<a[^>]*href="[^"]*"[^>]*>' | grep -Po "href=\"xcp-ng-${var_version}.([0-9]-[0-9]|[0-9]|[0-9]-[a-z]+[0-9]).iso\"" | sed 's/href="//g' | sed 's/"//g' | sort -Vr | head -n 1);
	fi;
	# Test : get NaN error explicitly.
	if [[ -z "$var_filename" || "$var_filename" == "" || ${#var_filename} -gt 60 ]]; then var_filename="NaN"; fi;
	# Construct return filename;url;date.
	var_date=$(Get_SingleVersionDateGeneric -DATA "$var_data" -FILENAME "$var_filename");
	echo "$var_filename;$var_url/$var_filename;$var_date";
}

# Get_SingleVersionDateGeneric.
#	DESCRIPTION : convert some date formats to one.
#	RETURN : string.
function Get_SingleVersionDateGeneric() {
	# Some definition.
	local var_data;
	local var_filename;
	local var_date;
	local var_date_day;
	local var_date_month;
	local var_date_year;
	local var_func;
	local var_func_dating;
	local var_return;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# BASH_REMATCH.
	declare -A var_date_map=( ["Jan"]="01" ["Feb"]="02" ["Mar"]="03" ["Apr"]="04" ["May"]="05" ["Jun"]="06" ["Jul"]="07" ["Aug"]="08" ["Sep"]="09" ["Oct"]="10" ["Nov"]="11" ["Dec"]="12" );
	# 'DD-MMM-YYYY' = alma, alpine, arch, mint, parrot, pfsense, proxmox, slackware, xcpng.
	func_234() {
		var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title="[^"]*"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*\d{2}-[A-Za-z]+-\d{4}" | grep -Po "\d{2}-[A-Za-z]+-\d{4}");
		if [[ "$var_date" =~ ^([0-9]{2})-([A-Za-z]{3})-([0-9]{4})$ ]]; then
			var_date_day=${BASH_REMATCH[1]};
			var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
			var_date_year=${BASH_REMATCH[3]};
			echo "$var_date_year/$var_date_month/$var_date_day";
		fi;
	}
	# 'YYYY-MM-DD' = centos, clonezilla, debian, fedora, kaisen, mageia, mx, opensuse, ubuntu.
	func_422() {
		var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title="[^"]*"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*\d{4}-\d{2}-\d{2}" | grep -Po "\d{4}-\d{2}-\d{2}");
		if [[ "$var_date" =~ ^([0-9]{4})-([0-9]{2})-([0-9]{2})$ ]]; then
			var_date_year=${BASH_REMATCH[1]};
			var_date_month=${BASH_REMATCH[2]};
			var_date_day=${BASH_REMATCH[3]};
			echo "$var_date_year/$var_date_month/$var_date_day";
		fi;
	}
	# 'YYYY-MMM-DD' = freebsd, kali, opnsense.
	func_432() {
		var_date=$(echo "$var_data" | grep -Po '<body.*</body>' | sed 's/<td[^>]*>//g' | sed 's/<span[^>]*>[^<]*<\/span>//g' | sed 's/title="[^"]*"//g' | sed 's/>[^<]*<\/a>/>/g' | grep -Po "${var_filename}[^.>]*\"[^\"]*\d{4}-[A-Za-z]+-\d{2}" | grep -Po "\d{4}-[A-Za-z]+-\d{2}");
		if [[ "$var_date" =~ ^([0-9]{4})-([A-Za-z]{3})-([0-9]{2})$ ]]; then
			var_date_year=${BASH_REMATCH[1]};
			var_date_month=${var_date_map[${BASH_REMATCH[2]}]};
			var_date_day=${BASH_REMATCH[3]};
			echo "$var_date_year/$var_date_month/$var_date_day";
		fi;
	}
	var_result="rolling";
	var_func=("func_234" "func_422" "func_432");
	# Loop : remove <td>, <span></span>, title, all inside <a>XXX</a>, and get date.
	for var_func_dating in "${var_func[@]}"; do
		var_return=$($var_func_dating);
		# Test : instead perform head -n 1 each function, get NaN error explicitly.
		if [[ -n "$var_return" && "$var_return" =~ " " ]]; then
			var_result="NaN";
			break;
		elif [[ -n "$var_return" && "$var_return" != "" ]]; then
			var_result="$var_return";
			break;
		fi;
	done;
	echo "$var_result";
}

# Compare_NokFilter.
#	DESCRIPTION : filter release by nok patterns.
#	RETURN : string or code.
function Compare_NokFilter() {
	# Some definition.
	local var_distribution;
	local var_release;
	local var_filename;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DISTRO)
				var_distribution="$2"
				shift 2
				;;
			-RELEASE)
				var_release="$2"
				shift 2
				;;
			-FILENAME)
				var_filename="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : nok & specific invalid.
	case "$var_distribution" in
		alpine)
			[[ "$var_filename" == *"_rc"* ]] && var_result="nok-rc"
			;;
		alma)
			if [[ "$var_release" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_release" =~ ^[0-9]$ ]]; then
				var_result="nok-version"
			fi
			;;
		debian)
			if [[ "$var_release" == *"_r"* ]]; then
				var_result="nok-rc"
			elif [[ "$var_release" == *"-"* ]]; then
				var_result="nok-version"
			fi
			;;
		freebsd)
			[[ "$var_filename" == *"RC"* ]] && var_result="nok-rc"
			;;
		kaisen)
			[[ "$var_release" == *"RC"* ]] && var_result="nok-rc"
			;;
		parrot)
			[[ "$var_release" == *"-beta"* ]] && var_result="nok-beta"
			;;
		ubuntu)
			[[ "$var_filename" == *"-beta-"* ]] && var_result="nok-beta"
			;;
		xcpng)
			if [[ "$var_filename" == *"-alpha"* ]]; then
				var_result="nok-alpha"
			elif [[ "$var_filename" == *"-beta"* ]]; then
				var_result="nok-beta"
			elif [[ "$var_filename" == *"-rc"* ]]; then
				var_result="nok-rc"
			fi
			;;
	esac
	# Test : string, code.
	if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
}

# Add_Report.
#	DESCRIPTION : add distribution with new release and/or new source to JSON report.
function Add_Report() {
	# Some definition.
	local var_distribution;
	local var_release;
	local var_release_new;
	local var_release_old;
	local var_source;
	local var_source_new;
	local var_source_old;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DISTRO)
				var_distribution="$2"
				shift 2
				;;
			-RELEASE)
				var_release="$2"
				shift 2
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : release, source.
	if [[ -n "$var_release" ]]; then
		var_release_new="$(echo "$var_release" | cut -d ":" -f1)";
		var_release_old="$(echo "$var_release" | cut -d ":" -f2)";
	fi;
	if [[ -n "$var_source" ]]; then
		var_source_new="$(echo "$var_source" | cut -d ":" -f1)";
		var_source_old="$(echo "$var_source" | cut -d ":" -f2)";
	fi;
	# Tests : release, source.
	if [[ "$var_release_new" != "$var_release_old" && "$var_source_new" != "$var_source_old" ]]; then
		if jq --arg distribution "$var_distribution" \
		--arg release_new "$var_release_new" \
		--arg release_old "$var_release_old" \
		--arg source_new "$var_source_new" \
		--arg source_old "$var_source_old" \
		'.distro += [{"name": $distribution, "release": {"new": $release_new, "old": $release_old}, "source": {"new": $source_new, "old": $source_old}}]' \
		"${PARAM[REPORT]}" > "${PARAM[REPORT]}.tmp" && mv "${PARAM[REPORT]}.tmp" "${PARAM[REPORT]}"; then
			return 0;
		else
			return 1;
		fi;
	elif [[ "$var_release_new" != "$var_release_old" ]]; then
		if jq --arg distribution "$var_distribution" \
		--arg release_new "$var_release_new" \
		--arg release_old "$var_release_old" \
		'.distro += [{"name": $distribution, "release": {"new": $release_new, "old": $release_old}}]' \
		"${PARAM[REPORT]}" > "${PARAM[REPORT]}.tmp" && mv "${PARAM[REPORT]}.tmp" "${PARAM[REPORT]}"; then
			return 0;
		else
			return 1;
		fi;
	elif [[ "$var_source_new" != "$var_source_old" ]]; then
		if jq --arg distribution "$var_distribution" \
		--arg source_new "$var_source_new" \
		--arg source_old "$var_source_old" \
		'.distro += [{"name": $distribution, "source": {"new": $source_new, "old": $source_old}}]' \
		"${PARAM[REPORT]}" > "${PARAM[REPORT]}.tmp" && mv "${PARAM[REPORT]}.tmp" "${PARAM[REPORT]}"; then
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}

# Convert_CSVToJSON.
#	DESCRIPTION : data CSV to data JSON.
function Convert_CSVToJSON() {
	# Some definition.
	local var_data_imported;
	local var_data_rewrited;
	var_data_imported=$(jq -R '{distro: [
		inputs | split(",") | {
			name: .[0],
			source: .[9],
			tmp: {
				all: .[3] | split(" ") | map({version: .}),
				valid: .[4] | split(" ") | map({version: .}),
				nok: .[5] | split(" ") | map({version: .})
			},
			releases: {
				latest: {version: .[1], date: .[2], url: .[6]},
				all: .[3] | split(" ") | map({version: ""}),
				valid: .[7] | split(" ") | map({version: "", url: .}),
				nok: .[8] | split(" ") | map({version: "", url: .})
			}
		}
	]}' < "${PARAM[OUTPUT]}");
	var_data_rewrited="$(echo "$var_data_imported" | jq '.distro[] |= (
		.releases.all = [
			( range(0; (.releases.all | length)) as $i | .tmp.all[$i].version )
		] | 
		.releases.valid = [ 
			( range(0; (.releases.valid | length)) as $i | { version: .tmp.valid[$i].version, url: .releases.valid[$i].url } )
		] | 
		.releases.nok = [ 
			( range(0; (.releases.nok | length)) as $i | { version: .tmp.nok[$i].version, url: .releases.nok[$i].url } )
		] | 
		del(.tmp)
		)'
	)";
	# Final JSON output with SC2001 shellcheck solution.
	echo "$var_data_rewrited" > "${PARAM[OUTPUT]%.csv}.json";
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	# array.
	declare -a var_array_param_distribution;
	declare -a var_array_return_distribution;
	local var_array_return_distribution_length;
	# element.
	local var_distribution_name;
	local var_distribution_mode;
	local var_distribution_indexes;
	local var_release_version;
	local var_release_version_display;
	local var_release_array_filenameurl;
	local var_release_filename;
	local var_release_filename_display;
	local var_release_url;
	local var_release_url_clean;
	local var_release_url_display;
	local var_release_url_valid_last;
	local var_release_date;
	local var_release_date_valid_last;
	local var_release_note;
	local var_release_distant;
	local var_release_check;
	local var_release_nok;
	# count.
	local var_count_release_all;
	local var_count_release_valid;
	# data backup.
	local var_return_backup;
	local var_return_backup_release;
	local var_return_backup_source;
	# result.
	declare -a var_array_result_distribution_nok;
	declare -a var_array_result_distribution_nok_url;
	declare -a var_array_result_distribution_valid;
	local var_array_result_distribution_valid_display;
	declare -a var_array_result_distribution_valid_url;
	# Construct : read data.
	IFS=',' read -ra var_array_param_distribution <<< "${PARAM[DISTRO]}";
	Restore_IFS;
	# Test : DEBUG.
 	if [[ "${CONF[DEBUG]}" == false ]]; then
		# Test : Table with param and max value length.
		if [[ "${PARAM[QUIET]}" != true ]]; then Get_TableHeader -ARRAY "ID:4 Version:10 Filename:40 Ping:4 URL:40 Reject:16"; fi;
		# Start or RàZ CSV output.
		echo -e "Name,VersionLatest,VersionLatestDate,VersionAll,VersionValid,VersionNOK,UrlLatest,UrlValid,UrlNOK,Source" > "${PARAM[OUTPUT]}";
		# Test : report need previous data.
		if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]] && Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
			# Start or RàZ JSON report.
			jq --null-input --arg date "${CONF[DATE_START_EXEC]}" '{"date": $date,"distro":[]}' > "${PARAM[REPORT]}";
		fi;
		# Loop : distribution, with 2 counts control (all & valid).
		for var_distribution_name in "${var_array_param_distribution[@]}"; do
			# Env & Indexes.
			var_distribution_mode="${DISTRIBUTION[${var_distribution_name}_mode]}";
			var_distribution_indexes=$([[ "${DISTRIBUTION[${var_distribution_name}_archive_index]}" == "" ]] && echo "1 index" || echo "2 indexes");
			# Construct : read release.
			IFS=',' read -ra var_array_return_distribution <<< "$(Get_AllVersion -NAME "$var_distribution_name")";
			var_array_return_distribution_length=${#var_array_return_distribution[@]};
			Restore_IFS;
			# Test : parsing not available.
			if [ "$var_array_return_distribution_length" -ne 0 ]; then
				# Display.
				if [[ "${PARAM[QUIET]}" != true ]]; then echo -e "\n${COLOR[GREEN_DARK]}$var_distribution_name${COLOR[STOP]} - $var_array_return_distribution_length release(s) from $var_distribution_indexes ('${var_distribution_mode}' available) :"; fi;
				# Count.
				var_count_release_all=0;
				var_count_release_valid=0;
				# Loop : release.
				while [[ "$var_count_release_valid" -lt "${PARAM[LENGTH]}" && "$var_count_release_all" -lt "$var_array_return_distribution_length" ]]; do
					var_release_version=${var_array_return_distribution[$var_count_release_all]};
					var_release_array_filenameurl=$(Get_SingleVersion -NAME "$var_distribution_name" -VERSION "$var_release_version" -POSITION "$var_count_release_all");
					var_release_filename=$(echo "$var_release_array_filenameurl" | cut -d ";" -f1);
					var_release_url=$(echo "$var_release_array_filenameurl" | cut -d ";" -f2);
					var_release_date=$(echo "$var_release_array_filenameurl" | cut -d ";" -f3);
					# Display.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						var_release_version_display=$(Limit_StringLength -STRING "$var_release_version" -COUNT 10 -DIRECTION "after");
						var_release_filename_display=$(Limit_StringLength -STRING "$var_release_filename" -COUNT 40 -DIRECTION "after");
						var_release_url_clean="${var_release_url#http://}";
						var_release_url_clean="${var_release_url_clean#https://}";
						var_release_url_display=$(Limit_StringLength -STRING "$var_release_url_clean" -COUNT 40 -DIRECTION "after");
						var_release_note="";
					fi;
					# Test : line check.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 [-]:5" -ACTION "check";
					fi;
					# Test : single release file available.
					if Get_Distant -URL "$var_release_url" -TYPE "ping"; then
						var_release_distant=true;
						var_release_check="tick";
					else
						var_release_distant=false;
						var_release_check="cross";
					fi;
					# Test : single release filter NOK with void for no interacting with echo.
					if Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename" &> /dev/null; then var_release_nok=true; else var_release_nok=false; fi;
					# Tests : NOK.
					if [[ "$var_release_nok" == true || "$var_release_distant" == false || "$var_release_filename" == "" ]]; then
						if [[ "$var_release_nok" == true ]]; then
							var_release_note="$(Compare_NokFilter -DISTRO "$var_distribution_name" -RELEASE "$var_release_version" -FILENAME "$var_release_filename")";
						elif [[ "$var_release_distant" == false ]]; then
							var_release_note="nok-ping";
						elif [[ "$var_release_filename" == "" ]]; then
							var_release_note="nok-empty";
						fi;
						# Construct nok distribution return filtered.
						var_array_result_distribution_nok+=("$var_release_version");
						var_array_result_distribution_nok_url+=("$var_release_url");
					else
						# Construct valid distribution return filtered.
						var_array_result_distribution_valid+=("$var_release_version");
						var_array_result_distribution_valid_url+=("$var_release_url");
						var_count_release_valid="$((var_count_release_valid+1))";
						# First and only definition of url & date.
						if [[ -z "$var_release_url_valid_last" || "$var_release_url_valid_last" == "" ]]; then var_release_url_valid_last="$var_release_url"; fi;
						if [[ -z "$var_release_date_valid_last" || "$var_release_date_valid_last" == "" ]]; then var_release_date_valid_last="$var_release_date"; fi;
					fi;
					# Test : line checked.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						Get_TableRow -ARRAY "#${var_count_release_all}:4 ${var_release_version_display}:10 ${var_release_filename_display}:40 ${var_release_check}:5 ${var_release_url_display}:40 ${var_release_note}:16" -ACTION "checked";
					fi;
					var_count_release_all="$((var_count_release_all+1))";
				done;
				# Final release display output.
				if [ ${#var_array_result_distribution_valid[@]} -le 0 ]; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "${COLOR[RED_LIGHT]}no valid versions to display.${COLOR[STOP]}";
					fi;
				else
					if [[ -n ${var_array_result_distribution_valid[*]:1} ]]; then
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]} ${var_array_result_distribution_valid[*]:1}"
					else
						var_array_result_distribution_valid_display="${COLOR[GREEN_DARK]}${var_array_result_distribution_valid[0]}${COLOR[STOP]}";
					fi;
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "latest stable versions are '${var_array_result_distribution_valid_display}'.";
					fi;
				fi;
				# Final release CSV output.
				echo -e "${var_distribution_name},${var_array_result_distribution_valid[0]},${var_release_date_valid_last},${var_array_return_distribution[*]},${var_array_result_distribution_valid[*]},${var_array_result_distribution_nok[*]},${var_release_url_valid_last},${var_array_result_distribution_valid_url[*]},${var_array_result_distribution_nok_url[*]},current" >> "${PARAM[OUTPUT]}";
				# Test : report need previous data.
				if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]] && Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
					var_return_backup_release="$(echo "$var_return_backup" | cut -d "," -f2)";
					var_return_backup_source="$(echo "$var_return_backup" | cut -d "," -f10)";
					# Test : new release, soure changed.
					if [[ "$var_return_backup_release" != "${var_array_result_distribution_valid[0]}" || "$var_return_backup_source" != "current" ]]; then
						if ! Add_Report -DISTRO "${var_distribution_name}" -RELEASE "${var_array_result_distribution_valid[0]}:${var_return_backup_release}" -SOURCE "current:${var_return_backup_source}"; then
							Stop_App -CODE 1 -TEXT "report can't be generated.";
						fi;
					fi;
				fi;
				# RàZ nok distribution.
				var_array_result_distribution_nok=();
				var_array_result_distribution_nok_url=();
				# RàZ valid distribution.
				var_array_result_distribution_valid=();
				var_array_result_distribution_valid_display="";
				var_array_result_distribution_valid_url=();
				var_release_date_valid_last="";
				var_release_url_valid_last="";
				var_return_backup="";
			else
				# Test : BACKUP.
				if [[ "${PARAM[BACKUP]}" == true ]]; then
					# Test : backup need previous data.
					if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
						var_return_backup=$(Find_FileLine -PATTERN "^${var_distribution_name}," -FILE "${PARAM[OUTPUT_BACKUP]}");
						# Test : pattern.
						if [[ -n "$var_return_backup" ]]; then
							echo -e "${var_return_backup}" >> "${PARAM[OUTPUT]}";
							sed -i "/^${var_distribution_name}/s/,current/,archive/" "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty, content recovered from backup.${COLOR[STOP]}";
							fi;
							# Test : report need previous data.
							if [[ -n "${PARAM[REPORT]}" && "${PARAM[REPORT]}" != false ]]; then
								var_return_backup_source="$(echo "$var_return_backup" | cut -d "," -f10)";
								# Test : source changed.
								if [[ "$var_return_backup_source" != "archive" ]]; then
									if Add_Report -DISTRO "${var_distribution_name}" -SOURCE "archive:${var_return_backup_source}"; then
										if [[ "${PARAM[QUIET]}" != true ]]; then
											echo -e "report added.";
										fi;
									else
										Stop_App -CODE 1 -TEXT "report can't be added.";
									fi;
								fi;
							fi;
						else
							echo -e "${var_distribution_name},,,,,,,,,current" >> "${PARAM[OUTPUT]}";
							if [[ "${PARAM[QUIET]}" != true ]]; then
								echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous data in backup file, default content pushed.${COLOR[STOP]}";
							fi;
						fi;
					else
						echo -e "${var_distribution_name},,,,,,,,,current" >> "${PARAM[OUTPUT]}";
						if [[ "${PARAM[QUIET]}" != true ]]; then
							echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and no previous backup file, default content pushed.${COLOR[STOP]}";
						fi;
					fi;
				else
					# Empty record.
					echo -e "${var_distribution_name},,,,,,,,,current" >> "${PARAM[OUTPUT]}";
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "\n${COLOR[RED_LIGHT]}${var_distribution_name} : parsing empty and data backup disabled, default content pushed.${COLOR[STOP]}";
					fi;
				fi;
			fi;
		done;
		# Final CSV output to JSON output.
		Convert_CSVToJSON;
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;