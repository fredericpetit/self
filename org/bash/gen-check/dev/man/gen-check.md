<blockquote id="out2tag">



NAME



	gen-check - Syntax check.





REQUIREMENTS



	program 'shellcheck' (mandatory)



 

SYNOPSIS



	gen-check OPTIONS





DESCRIPTION



	This open-source tool use ShellCheck to check.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path to Bash script.

 		- state : mandatory.

 		- accepted value(s) : path.

 		- default value : no default.



 	-o FILEPATH,

	--output FILEPATH

 		Single local file path to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD/SOURCE.check'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	null

		null



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	0.0.1 - 2025-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 0.0.1

	License : GPL-3.0-or-later





</blockquote>
