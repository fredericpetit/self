#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               gen-check.sh               | #
# |__________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="gen-check";
APP["SECTION"]="1";
APP["SHORTNAME"]="GenCheck";
APP["SLOGAN"]="Syntax check.";
APP["DESCRIPTION"]="This open-source tool use ShellCheck to check.";
APP["REQUIREMENTS"]="program shellcheck mandatory";
APP["EXAMPLES"]="null";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="0.0.1|2025-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="0.0.1";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#mandatory#Single local file path to Bash script.|path.|no default.";
OPTIONS["OUTPUT"]="2#o#FILEPATH#optionnal#Single local file path to store the final result.|path.|'PWD/SOURCE.check'.";
OPTIONS["QUIET"]="3#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="4#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="5#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["OUTPUT"]="${PARAM[SOURCE]%[\\/]}";
DEFAULT["OUTPUT"]="${DEFAULT[OUTPUT]%.*}.check";
DEFAULT["QUIET"]=false;
# Some script definition.
# OUTPUT.
if [ -z "${PARAM[OUTPUT]}" ]; then
	PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}";
else
	Stop_App -CODE 1 -TEXT "Need value for '--output' option.";
fi;
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Init.
	Write_Script -TEXT "checking '${PARAM[SOURCE]}' ..." -CHECK;
	# Action.
	shellcheck "${PARAM[SOURCE]}" > "${PARAM[OUTPUT]}" 2>&1;
	# Finish.
	Write_Script -TEXT "'${PARAM[SOURCE]}' checked." -CODE 0 -CHECKED;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;