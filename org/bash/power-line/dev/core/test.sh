#!/bin/bash

# dl bash.
wget -O ~/.config/powerline.bash https://gitlab.com/bersace/powerline-bash/raw/master/powerline.bash;
# dl fonts.
sudo apt-get install -y fonts-powerline;
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.3.0/NerdFontsSymbolsOnly.zip && \
	unzip NerdFontsSymbolsOnly.zip -d NerdFontsSymbolsOnly && rm NerdFontsSymbolsOnly.zip;
# install fonts.
mkdir -p ~/.local/share/fonts/NerdFontsSymbolsOnly/ && \
	mv NerdFontsSymbolsOnly/*.ttf ~/.local/share/fonts/NerdFontsSymbolsOnly/ && \
	rm -Rf NerdFontsSymbolsOnly;
# reload fonts.
fc-cache -fv ~/.local/share/fonts;
# install powerline.
cat << 'EOF' >> ~/.bashrc
# POWERLINE BASH.
if [[ -f ${HOME}/.config/powerline.bash ]]; then
	POWERLINE_ICONS=nerd-fonts;
	POWERLINE_HEURE_DATEFORMAT="%H:%M";
	# source powerline.bash
	. ${HOME}/.config/powerline.bash;
	POWERLINE_SEGMENTS="logo hostname heure ${POWERLINE_SEGMENTS}";
	PROMPT_COMMAND='__update_ps1 $?';
fi;
EOF
# custom ll.
echo "alias ll='ls -liaFh --group-directories-first --time-style=\"+%Y-%m-%d %H:%M:%S\"';" > ~/.bash_aliases;