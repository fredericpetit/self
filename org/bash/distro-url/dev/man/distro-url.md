<blockquote id="out2tag">



NAME



	distro-url - Generate distro-tracker URLs.





REQUIREMENTS



	program 'mktemp' (mandatory)



 	program 'jq' (mandatory)



 	program 'yq' (mandatory)



 

SYNOPSIS



	distro-url [ OPTIONS ]





DESCRIPTION



	This open-source tool generate CSV, JSON & YAML files.





OPTIONS



	-o FOLDERPATH,

	--output FOLDERPATH

 		Single local folder path (final slash automatically defined, filename always 'distro-url.FORMAT') to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	default output in quiet mode :

		distro-url -q



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	1.1.0 - 2024-12-01

		Add mktemp program in requirements and use it for temporary file security protection. Add fullname information.



 	1.0.0 - 2024-09-01

		New documentation.



 	0.0.1 - 2024-06-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.1.0

	License : GPL-3.0-or-later





</blockquote>
