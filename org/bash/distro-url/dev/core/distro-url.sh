#!/bin/bash
# shellcheck source=/dev/null
#  ___________________________________________
# |                                           | #
# |               distro-url.sh               | #
# |___________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-url";
APP["SECTION"]="1";
APP["SHORTNAME"]="DU";
APP["SLOGAN"]="Generate distro-tracker URLs.";
APP["DESCRIPTION"]="This open-source tool generate CSV, JSON & YAML files.";
APP["REQUIREMENTS"]="program mktemp mandatory#program jq mandatory#program yq mandatory";
APP["EXAMPLES"]="default output in quiet mode :|${APP[NAME]} -q";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.1.0|2024-12-01T10:00:00+0200|Add mktemp program in requirements and use it for temporary file security protection. Add fullname information.#1.0.0|2024-09-01T10:00:00+0200|New documentation.#0.0.1|2024-06-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.1.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["OUTPUT"]="1#o#FOLDERPATH#optionnal#Single local folder path (final slash automatically defined, filename always 'distro-url.FORMAT') to store the final result.|path.|'PWD'.";
OPTIONS["QUIET"]="2#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="3#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="4#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--output|-o)
			PARAM["OUTPUT"]="${2%[\\/]}${CONF[SEPARATOR]}"
			Test_Param -OPTION "$1" -PARAM "${PARAM[OUTPUT]}"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================== #
#          DISTRO          #
#
#	DESCRIPTION : the source.

declare -gA DISTRO=(
	["alma"]="1,AlmaLinux OS,uicli,https://repo.almalinux.org/almalinux,https://repo.almalinux.org/almalinux/xxx/isos/x86_64,null,https://vault.almalinux.org/xxx/isos/x86_64"
	["alpine"]="2,Alpine Linux,cli,https://dl-cdn.alpinelinux.org/alpine,https://dl-cdn.alpinelinux.org/alpine/xxx/releases/x86_64,null,null"
	["arch"]="3,Arch Linux,ui,https://geo.mirror.pkgbuild.com/iso,https://geo.mirror.pkgbuild.com/iso/xxx,null,null"
	["centos"]="4,CentOS Stream,ui,https://mirror.in2p3.fr/pub/linux/centos-stream,https://mirror.in2p3.fr/pub/linux/centos-stream/xxx/BaseOS/x86_64/iso,https://mirror.in2p3.fr/pub/linux/centos-vault,https://mirror.in2p3.fr/pub/linux/centos-vault/xxx/isos/x86_64"
	["clonezilla"]="5,Clonezilla,ui,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable,https://sourceforge.net/projects/clonezilla/files/clonezilla_live_stable/xxx,null,null"
	["debian"]="6,Debian,ui,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd,https://cdimage.debian.org/mirror/cdimage/archive,https://cdimage.debian.org/mirror/cdimage/archive/xxx/amd64/iso-dvd"
	["fedora"]="7,Fedora Linux,uicli,https://mirror.in2p3.fr/pub/fedora/linux/releases,https://mirror.in2p3.fr/pub/fedora/linux/releases/xxx/Workstation/x86_64/iso,null,https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/xxx/Workstation/x86_64/iso"
	["freebsd"]="8,FreeBSD,ui,https://download.freebsd.org/releases/ISO-IMAGES,https://download.freebsd.org/releases/ISO-IMAGES/xxx,null,null"
	["kaisen"]="9,Kaisen Linux,ui,https://iso.kaisenlinux.org/rolling,https://iso.kaisenlinux.org/rolling,null,null"
	["kali"]="10,Kali Linux,ui,https://cdimage.kali.org,https://cdimage.kali.org/kali-xxx,null,null"
	["libreelec"]="11,LibreELEC,ui,https://libreelec.tv/downloads/generic,https://releases.libreelec.tv,null,null"
	["linux"]="12,Linux Kernel,cli,https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x,https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x,null,null"
	["mageia"]="13,Mageia,ui,https://mirrors.kernel.org/mageia/iso,https://mirrors.kernel.org/mageia/iso/xxx/Mageia-xxx-x86_64,null,null"
	["manjaro"]="14,Manjaro Linux,ui,https://manjaro.org/products/download/x86,https://download.manjaro.org/kde/xxx,null,null"
	["mint"]="15,Linux Mint,ui,https://mirrors.edge.kernel.org/linuxmint/stable,https://mirrors.edge.kernel.org/linuxmint/stable/xxx,null,null"
	["mx"]="16,MX Linux,ui,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Final/KDE,https://sourceforge.net/projects/mx-linux/files/Old,https://sourceforge.net/projects/mx-linux/files/Old/MX-xxx/KDE"
	["openmediavault"]="17,OpenMediaVault,cli,https://sourceforge.net/projects/openmediavault/files/iso,https://sourceforge.net/projects/openmediavault/files/iso/xxx,null,null"
	["opensuse"]="18,openSUSE (Leap),ui,https://download.opensuse.org/distribution/leap/?jsontable,https://download.opensuse.org/distribution/leap/xxx/iso/?jsontable,null,null"
	["opnsense"]="19,OPNsense,cli,https://mirror.wdc1.us.leaseweb.net/opnsense/releases,https://mirror.wdc1.us.leaseweb.net/opnsense/releases/xxx,null,null"
	["parrot"]="20,Parrot Security,ui,https://deb.parrot.sh/parrot/iso,https://deb.parrot.sh/parrot/iso/xxx,null,null"
	["pfsense"]="21,pfSense,cli,https://atxfiles.netgate.com/mirror/downloads,https://atxfiles.netgate.com/mirror/downloads,null,null"
	["proxmox"]="22,Proxmox,cli,https://enterprise.proxmox.com/iso,https://enterprise.proxmox.com/iso,null,null"
	["raspios"]="23,Raspberry Pi OS,uicli,https://downloads.raspberrypi.com/raspios_arm64/images,https://downloads.raspberrypi.com/raspios_arm64/images/xxx,null,null"
	["rescuezilla"]="24,Rescuezilla,ui,https://api.github.com/repos/rescuezilla/rescuezilla/releases,https://github.com/rescuezilla/rescuezilla/releases/download/xxx,null,null"
	["rocky"]="25,Rocky Linux,uicli,https://download.rockylinux.org/pub/rocky,https://download.rockylinux.org/pub/rocky/xxx/isos/x86_64,https://dl.rockylinux.org/vault/rocky,https://dl.rockylinux.org/vault/rocky/xxx/isos/x86_64"
	["shredos"]="26,ShredOS,cli,https://api.github.com/repos/partialvolume/shredos.x86_64/releases,https://github.com/PartialVolume/shredos.x86_64/releases/download/xxx,null,null"
	["slackware"]="27,Slackware,ui,https://mirrors.slackware.com/slackware/slackware-iso,https://mirrors.slackware.com/slackware/slackware-iso/slackware64-xxx-iso,null,null"
	["truenas"]="28,TrueNAS (Community Edition),cli,https://download.sys.truenas.net/TrueNAS-SCALE-ElectricEel,https://download.sys.truenas.net/TrueNAS-SCALE-ElectricEel/xxx,null,null"
	["ubuntu"]="29,Ubuntu,uicli,https://releases.ubuntu.com,https://releases.ubuntu.com/xxx,null,null"
	["xcpng"]="30,XCP-ng,cli,https://updates.xcp-ng.org/isos,https://updates.xcp-ng.org/isos/xxx,null,null"
);
# Test : sort DISTRO.
if ! Sort_Array -NAME "DISTRO" -DELIMITER ","; then Stop_App -CODE 1 -TEXT "'DISTRO' array parsing failed."; fi;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}";
# OUTPUT.
if [ -z "${PARAM[OUTPUT]}" ]; then PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}"; fi;
PARAM["OUTPUT_CSV"]="${PARAM[OUTPUT]}distro-url.csv";
PARAM["OUTPUT_JSON"]="${PARAM[OUTPUT]}distro-url.json";
PARAM["OUTPUT_YAML"]="${PARAM[OUTPUT]}distro-url.yaml";
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Add_LocalDistro.
#	DESCRIPTION : put in CSV, JSON & YAML.
function Add_LocalDistro() {
	# Some definition - menu.
	local var_name;
	local var_data;
	local var_format;
	# Some definition - next.
	local var_array;
	local var_element;
	local var_output_tmp;
	local var_entry;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME) var_name="$2"; shift 2 ;;
			-DATA) var_data="$2"; shift 2 ;;
			-FORMAT) var_format="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Rewrite data without index.
	var_data="${var_data#*,}";
	# Array : data.
	IFS=',' read -ra var_array <<< "$var_data";
	Restore_IFS;
	# Loop : indexlist data.
	for var_element in "${!var_array[@]}"; do
		# Test : empty data.
		if [[ -z "${var_array[$var_element]}" ]]; then
			Stop_App -CODE 1 -TEXT "Index position '${var_element}' for distro '${var_name}' is empty."
		fi;
	done;
	# Tests : csv, json, yaml.
	if [[ "$var_format" == "csv" ]]; then
		# CSV routine.
		echo -e "${var_name},${var_data}" >> "${PARAM[OUTPUT_CSV]}";
	elif [[ "$var_format" == "json" ]]; then
		# TMP security.
		var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.json");
		if ! Test_File -PATH "$var_output_tmp"; then
			Stop_App -CODE 1 -TEXT "Temp JSON file not writable.";
		fi;
		# Construct JSON.
		var_entry=$(jq -n --tab \
			--arg name "$var_name" \
			--arg fullname "${var_array[0]}" \
			--arg mode "${var_array[1]}" \
			--arg current_index "${var_array[2]}" \
			--arg current_release "${var_array[3]}" \
			--arg archive_index "${var_array[4]}" \
			--arg archive_release "${var_array[5]}" \
			'{
				name: $name,
				fullname: $fullname,
				mode: $mode,
				current_index: $current_index,
				current_release: $current_release,
				archive_index: $archive_index,
				archive_release: $archive_release
			}'
		);
		# Test : security.
		if ! jq --tab --argjson var_entry "$var_entry" '.distro += [$var_entry]' "${PARAM[OUTPUT_JSON]}" > "$var_output_tmp" 2>/dev/null; then
			rm -f "$var_output_tmp";
			Stop_App -CODE 1 -TEXT "JSON routine failed while updating JSON file.";
		else
			# Validating.
			mv "$var_output_tmp" "${PARAM[OUTPUT_JSON]}";
		fi;
	elif [[ "$var_format" == "yaml" ]]; then
		# TMP security.
		var_output_tmp=$(mktemp "${TMPDIR:-/tmp}/tmp.XXXXXX.yaml");
		if ! Test_File -PATH "$var_output_tmp"; then
			Stop_App -CODE 1 -TEXT "Temp YAML file not writable.";
		fi;
		# Construct YAML.
		var_entry="[
			{
				\"name\": \"$var_name\",
				\"fullname\": \"${var_array[0]}\",
				\"mode\": \"${var_array[1]}\",
				\"current_index\": \"${var_array[2]}\",
				\"current_release\": \"${var_array[3]}\",
				\"archive_index\": \"${var_array[4]}\",
				\"archive_release\": \"${var_array[5]}\"
			}
		]";
		# Test : security.
		if ! yq eval ".distro += $var_entry" "${PARAM[OUTPUT_YAML]}" > "$var_output_tmp" 2>/dev/null; then
			rm -f "$var_output_tmp";
			Stop_App -CODE 1 -TEXT "YAML routine failed while updating YAML file.";
		else
			# Validating.
			mv "$var_output_tmp" "${PARAM[OUTPUT_YAML]}";
		fi;
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_count;
	local var_index;
	local var_key;
	local var_value;
	# Init CSV, JSON & YAML files.
	echo -e "name,fullname,mode,current_index,current_release,archive_index,archive_release" > "${PARAM[OUTPUT_CSV]}";
	jq -n --arg revision "${DATE[DATETIME0]}" '{revision: $revision, distro: []}' > "${PARAM[OUTPUT_JSON]}";
	echo "revision: \"${DATE[DATETIME0]}\"" > "${PARAM[OUTPUT_YAML]}";
	echo "distro: []" >> "${PARAM[OUTPUT_YAML]}";
	# Loop : indexlist distro.
	var_count=0;
	for var_index in $(echo "${!DISTRO_SORT[@]}" | tr ' ' '\n' | sort -n); do
		var_key="${DISTRO_SORT[$var_index]}";
		var_value="${DISTRO[$var_key]}";
		Write_Script -TEXT "check distro '${var_key}' ..." -CHECK;
		# Test : debug.
		if [[ "${PARAM[QUIET]}" != true && "${CONF[DEBUG]}" == true ]]; then
			echo "DEBUG :: key '${var_key}' | value '${var_value}'";
		fi;
		var_count=$(echo "$var_value" | cut -d ',' -f1);
		# Test : CSV.
		if Add_LocalDistro -NAME "$var_key" -DATA "$var_value" -FORMAT "csv"; then
			Write_Script -TEXT "distro #${var_count} '${var_key}' (CSV) checked." -CODE 0 -CHECKED;
		else
			Write_Script -TEXT "distro #${var_count} '${var_key}' (CSV) check fail." -CODE 1 -CHECKED;
		fi;
		# Test : JSON.
		if Add_LocalDistro -NAME "$var_key" -DATA "$var_value" -FORMAT "json"; then
			Write_Script -TEXT "distro #${var_count} '${var_key}' (JSON) checked." -CODE 0 -CHECKED;
		else
			Write_Script -TEXT "distro #${var_count} '${var_key}' (JSON) check fail." -CODE 1 -CHECKED;
		fi;
		# Test : YAML.
		if Add_LocalDistro -NAME "$var_key" -DATA "$var_value" -FORMAT "yaml"; then
			Write_Script -TEXT "distro #${var_count} '${var_key}' (YAML) checked." -CODE 0 -CHECKED;
		else
			Write_Script -TEXT "distro #${var_count} '${var_key}' (YAML) check fail." -CODE 1 -CHECKED;
		fi;
	done;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;