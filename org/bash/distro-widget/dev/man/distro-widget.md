<blockquote id="out2tag">



NAME



	distro-widget - Set a fancy widget with Conky & systemd jobs.





REQUIREMENTS



	program 'conky' (mandatory)



 

SYNOPSIS



	distro-widget OPTIONS





DESCRIPTION



	This open-source tool generates some stats with Distro Tracker datas integration.





OPTIONS



	--source-tracker (no arg) 		Single local file path or single distant url to Distro Tracker data.

 		- state : optionnal.

 		- accepted value(s) : path or url.

 		- default value : 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv'.



 	--source-reporter (no arg) 		Single local file path or single distant url to Distro Reporter data.

 		- state : optionnal.

 		- accepted value(s) : path or url.

 		- default value : 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json'.



 	--source-downloader (no arg) 		Single local file path or single distant url to Distro Downloader.

 		- state : optionnal.

 		- accepted value(s) : path or url.

 		- default value : 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-downloader/dev/bin/distro-downloader'.



 	--source-reader (no arg) 		Single local file path or single distant url to Distro Reader.

 		- state : optionnal.

 		- accepted value(s) : path or url.

 		- default value : 'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reader/dev/standalone/distro-reader.sh'.



 	-u NAME,

	--username NAME

 		Local username for configuration.

 		- state : mandatory.

 		- accepted value(s) : no default.

 		- default value : 



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	install for user 'test' with two formats for sources :

		distro-widget -u test --source-tracker https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv --source-reporter https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json



 

TO-DO



	x





NOTES



	Compatible Distro Tracker >= 1.2.0.



 

CREDITS & THANKS



	x





CHANGELOG



	1.1.0 - 2025-01-01

		Add systemd jobs. Replace distro-get with standalone generics distro-downloader & distro-reader.



 	1.0.0 - 2024-10-20

		Distro Tracker datas integration.



 	0.2.0 - 2024-01-03

		USER parameter instead PATH.



 	0.1.0 - 2024-01-02

		Style unified with Windows version.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.1.0

	License : GPL-3.0-or-later





</blockquote>
