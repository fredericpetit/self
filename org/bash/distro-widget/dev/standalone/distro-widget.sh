#!/bin/bash
# first content joined - 2025-01-06.
# first content joined - 2025-01-06.
# shellcheck source=/dev/null
#  _________________________________________
# |                                         | #
# |               func_app.sh               | #
# |_________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : pre-defined work for all APPs.


# CONF - pursuit : safe storage of original $IFS.
if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["IFS"]="$IFS";


# Restore_IFS.
#	DESCRIPTION : restore IFS.
function Restore_IFS() { IFS="${CONF["IFS"]}"; }


# COLOR.
declare -gA COLOR;
COLOR[BOLD]='\e[1m';
COLOR[UNDERLINE]='\e[4m';
COLOR[RED_LIGHT]='\e[1;38;5;160m';
COLOR[GREEN_LIGHT]='\e[1;38;5;34m';
COLOR[GREEN_DARK]='\e[1;38;5;28m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;179m';
COLOR[YELLOW_DARK]='\e[1;38;5;178m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;208m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';
COLOR[STOP]='\e[0m';
COLOR[TICK]="[${COLOR[GREEN_LIGHT]}✔${COLOR[STOP]}]";
COLOR[CROSS]="[${COLOR[RED_LIGHT]}✘${COLOR[STOP]}]";


# Write_Script.
#	DESCRIPTION : custom output of the APP.
#	RETURN : string.
function Write_Script() {
	# Some definition.
	local var_text;
	local var_code;
	local var_check;
	local var_checked;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-CODE)
				var_code="$2"
				shift 2
				;;
			-CHECK)
				var_check=true
				shift
				;;
			-CHECKED)
				var_checked=true
				shift
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	if [[ "${PARAM[QUIET]}" != true ]]; then 
		# Tests : state, change former line.
		if [ -n "$var_check" ]; then
			echo -ne "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		elif [ -n "$var_checked" ]; then
			if [[ "$var_code" -eq 1 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[CROSS]} ERROR : $var_text";
			elif [[ "$var_code" -eq 0 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[TICK]} $var_text";
			fi;
		else
			echo -e "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		fi;
	fi;
}


# Get_Help.
#	DESCRIPTION : usage of the APP, in the APP.
#	RETURN : string.
function Get_Help() {
	# requirements.
	local var_requirements;
	local var_requirement;
	local var_requirement_type;
	local var_requirement_name;
	local var_requirement_state;
	local var_requirements_text;
	# Test : array exists.
	if [[ -n "${APP[REQUIREMENTS]}" && "${APP[REQUIREMENTS]}" != "null" ]]; then
		IFS='#' read -ra var_requirements <<< "${APP[REQUIREMENTS]}";
		# Loop : valuelist requirements.
		for var_requirement in "${var_requirements[@]}"; do
			var_requirement_type="$(echo "$var_requirement" | cut -d ' ' -f1)";
			var_requirement_name="$(echo "$var_requirement" | cut -d ' ' -f2)";
			var_requirement_state="$(echo "$var_requirement" | cut -d ' ' -f3)";
			var_requirements_text+="$(echo -e "\t${var_requirement_type} '${var_requirement_name}' (${var_requirement_state})\n\n ")";
		done;
	else
		var_requirements_text+="\tx\n\n ";
	fi;
	# synopsis + options.
	local i;
	local var_key;
	local var_option;
	local var_option_name;
	local var_option_quick;
	local var_option_type;
	local var_option_state;
	local var_option_dsc;
	local var_option_dsc_def;
	local var_option_dsc_val_ok;
	local var_option_dsc_val_default;
	local var_synopsis_text_mandatory;
	local var_synopsis_text;
	local var_options_text;
	var_synopsis_text_mandatory=false;
	# Loop : valuelist options array inside APP.
	for i in $(echo "${!OPTIONS_SORT[@]}" | tr ' ' '\n' | sort -n); do
		var_key="${OPTIONS_SORT[$i]}";
		var_option="${OPTIONS[$var_key]}";
		var_option_name="$(echo "$var_key" | tr '[:upper:]' '[:lower:]'| sed 's/_/-/g')";
		var_option_quick="$(echo "$var_option" | cut -d '#' -f2)";
		var_option_type="$(echo "$var_option" | cut -d '#' -f3)";
		var_option_state="$(echo "$var_option" | cut -d '#' -f4)";
		var_option_dsc="$(echo "$var_option" | cut -d '#' -f5)";
		var_option_dsc_def="$(echo "$var_option_dsc" | cut -d '|' -f1)";
		var_option_dsc_val_ok="$(echo "$var_option_dsc" | cut -d '|' -f2)";
		var_option_dsc_val_default="$(echo "$var_option_dsc" | cut -d '|' -f3)";
		# Test : mandatory OPTION.
		if [[ "$var_option_state" == "mandatory" ]]; then var_synopsis_text_mandatory=true; fi;
		# Test : command text.
		if [[ "$var_option_quick" == "null" ]]; then
			var_options_text+="$(echo -e "\t--${var_option_name} (no arg) ")";
		else
			# Test : command type (underline).
			if [[ "$var_option_type" == "null" ]]; then
				var_options_text+="$(echo -e "\t-${var_option_quick} (no type),\n\t--${var_option_name} (no type)\n ")";
			else
				var_options_text+="$(echo -e "\t-${var_option_quick} ${var_option_type},\n\t--${var_option_name} ${var_option_type}\n ")";
			fi;
		fi;
		var_options_text+="$(echo -e "\t\t${var_option_dsc_def}\n ")";
		var_options_text+="$(echo -e "\t\t- state : ${var_option_state}.\n ")";
		# Test : dsc redefine.
		if [[ "$var_option_dsc_val_ok" == "null" ]]; then var_option_dsc_val_ok="nothing."; fi;
		var_options_text+="$(echo -e "\t\t- accepted value(s) : ${var_option_dsc_val_ok}\n ")";
		var_options_text+="$(echo -e "\t\t- default value : ${var_option_dsc_val_default}\n\n ")";
	done;
	# Test : [] for OPTIONS if only one OPTION is mandatory.
	if [[ "$var_synopsis_text_mandatory" == true ]]; then var_synopsis_text="OPTIONS"; else var_synopsis_text="[ OPTIONS ]"; fi;
	# examples.
	local var_examples;
	local var_example;
	local var_example_dsc;
	local var_example_cmd;
	local var_examples_text;
	IFS='#' read -ra var_examples <<< "${APP[EXAMPLES]}";
	# Loop : valuelist examples.
	for var_example in "${var_examples[@]}"; do
		var_example_dsc="$(echo "$var_example" | cut -d '|' -f1)";
		var_example_cmd="$(echo "$var_example" | cut -d '|' -f2)";
		var_examples_text+="$(echo -e "\t${var_example_dsc}\n\t\t${var_example_cmd}\n\n ")";
	done;
	# todo.
	if [[ -z "${APP[TODO]}" || "${APP[TODO]}" == "null" ]]; then APP["TODO"]="x"; fi;
	# notes.
	local var_notes;
	local var_note;
	local var_notes_text;
	if [[ -n "${APP[NOTES]}" && "${APP[NOTES]}" != "null" ]]; then
		IFS='#' read -ra var_notes <<< "${APP[NOTES]}";
		# Loop : valuelist notes.
		for var_note in "${var_notes[@]}"; do
			var_notes_text+="$(echo -e "\t${var_note}\n\n ")";
		done;
	else
		var_notes_text+="\tx\n\n ";
	fi;
	# credits.
	if [[ -z "${APP[CREDITS]}" || "${APP[CREDITS]}" == "null" ]]; then APP["CREDITS"]="x"; fi;
	# changelog.
	local var_changelog;
	local var_change;
	local var_change_version;
	local var_change_date;
	local var_change_dsc;
	local var_changelog_text;
	IFS='#' read -ra var_changelog <<< "${APP[CHANGELOG]}";
	# Loop : valuelist changelog.
	for var_change in "${var_changelog[@]}"; do
		var_change_version="$(echo "$var_change" | cut -d '|' -f1)";
		var_change_date="$(LC_TIME=en_US.UTF-8 date -d "$(echo "$var_change" | cut -d '|' -f2)" "+%Y-%m-%d")";
		var_change_dsc="$(echo "$var_change" | cut -d '|' -f3)";
		var_changelog_text+="$(echo -e "\t${var_change_version} - ${var_change_date}\n\t\t${var_change_dsc}\n\n ")";
	done;
	Restore_IFS;
	echo -e "\nNAME\n\n\t${APP[NAME]} - ${APP[SLOGAN]}\n\n\nREQUIREMENTS\n\n${var_requirements_text}\nSYNOPSIS\n\n\t${APP[NAME]} ${var_synopsis_text}\n\n\nDESCRIPTION\n\n\t${APP[DESCRIPTION]}\n\n\nOPTIONS\n\n${var_options_text}\nEXAMPLES\n\n${var_examples_text}\nTO-DO\n\n\t${APP[TODO]}\n\n\nNOTES\n\n${var_notes_text}\nCREDITS & THANKS\n\n\t${APP[CREDITS]}\n\n\nCHANGELOG\n\n${var_changelog_text}\nMETADATAS\n\n\tAuthor : ${APP[AUTHOR]}\n\tWebsite : ${APP[URL]}\n\tVersion : ${APP[VERSION]}\n\tLicense : ${APP[LICENSE]}\n\n";
}


# Test_Options.
#	DESCRIPTION : global test for search missing options.
#	RETURN : string.
function Test_Options() {
	# Some definition.
	local var_options_key;
	local var_options_values;
	local var_options_value_name;
	local var_options_value_type;
	local var_options_value_state;
	declare -a var_options_missing;
	local var_options_missing_list;
	# Some init.
	var_options_missing=();
	# Test : data exists.
	if [ ${#OPTIONS[@]} -gt 0 ]; then
		# Loop : indexlist options.
		for var_options_key in "${!OPTIONS[@]}"; do
			var_options_values="${OPTIONS[$var_options_key]}";
			var_options_value_name="$(echo "$var_options_key" | tr '[:upper:]' '[:lower:]')";
			var_options_value_type="$(echo "$var_options_values" | cut -d '#' -f3)";
			var_options_value_state="$(echo "$var_options_values" | cut -d '#' -f4)";
			# Test : trigger warning.
			if [ "$var_options_value_state" == "mandatory" ]; then
					# Test : trigger array.
					if [[ -z "${PARAM[$var_options_key]}" ]]; then
						var_options_missing+=("${var_options_value_name}");
					fi;
			elif [ "$var_options_value_state" != "optionnal" ]; then
				# Test : option with boolean don't need parameter.
				if [ "$var_options_value_type" != "null" ]; then
					Stop_App -CODE 1 -TEXT "Option '--${var_options_value_name}' type is empty, please report the issue.";
				fi;
			fi;
		done;
		# Test : missing exists.
		if [ ${#var_options_missing[@]} -gt 0 ]; then
			# Loop : indexlist missing.
			for i in "${!var_options_missing[@]}"; do
				var_options_missing[i]="--${var_options_missing[i]}";
			done;
			# Test : single, multiple data.
			if [ ${#var_options_missing[@]} -eq 1 ]; then
				Stop_App -CODE 1 -TEXT "Need '${var_options_missing[0]}' option. Type '--help' for read the cute manual.";
			else
				var_options_missing_list="${var_options_missing[*]}";
				var_options_missing_list="${var_options_missing_list// /, }";
				Stop_App -CODE 1 -TEXT "Need '$var_options_missing_list' options. Type '--help' for read the cute manual.";
			fi;
		fi;
	else
		Stop_App -CODE 1 -TEXT "No option provided, please report the issue.";
	fi;
}


# Test_Param.
#	DESCRIPTION : single test for checking parameter (argument) of one option.
#	RETURN : string.
function Test_Param() {
	# Some definition.
	local var_option;
	local var_param;
	local var_type;
	local var_contains;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-OPTION)
				var_option="$2"
				shift 2
				;;
			-PARAM)
				var_param="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-CONTAINS)
				var_contains="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Tests : param mispelled, empty, strict type restriction.
	if [[ "$var_param" == "-"* ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't starting by '-'.";
	elif [[ -z "$var_param" ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't be empty.";
	elif [[ -n "$var_type" ]]; then
		if [[ "$var_type" == "char" ]]; then
			if [[ "$var_param" =~ [0-9] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have integer.";
			fi;
		elif [[ "$var_type" == "int" ]]; then
			if [[ "$var_param" =~ [a-zA-Z] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have character.";
			fi;
		fi;
	fi;
	# Test : character present.
	if [[ -n "$var_contains" ]]; then
		if [[ "$var_param" != *"${var_contains}"* ]]; then
			Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' don't contains '${var_contains}' character.";
		fi;
	fi;
}


# Get_StartingApp.
#	DESCRIPTION : script starter pack.
#	RETURN : string.
function Get_StartingApp() {
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Title.
		echo -e "${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: ${DATE[DATETIME3]} ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
		# Place & soft env.
		echo -e "- work from : '${CONF[PATH]}'.";
		if [ -n "$SNAP" ]; then
			echo -e "- Snapcraft environment detected.";
		elif [ -n "$FLATPAK_ID" ]; then
			echo -e "- Flatpak environment detected.";
		fi;
	fi;
	# Test : requirements.
	if [[ -n "${APP[REQUIREMENTS]}" && "${APP[REQUIREMENTS]}" != "null" ]]; then
		# Some definition.
		local var_requirements;
		local var_requirement;
		local var_element;
		local var_type;
		local var_name;
		local var_dependencie;
		local var_software;
		# Indexed array.
		local var_requirement_warn=();
		local var_requirement_warn_element;
		local var_requirement_warn_show=();
		local var_requirement_warn_show_formatted;
		local var_requirement_nok=();
		local var_requirement_nok_element;
		local var_requirement_nok_show=();
		local var_requirement_nok_show_formatted;
		# Auto-discovery from APP book.
		IFS='#' read -ra var_requirements <<< "${APP[REQUIREMENTS]}";
		Restore_IFS;
		# Loop : valuelist requirements.
		for var_requirement in "${var_requirements[@]}"; do
			IFS=' ' read -ra var_element <<< "$var_requirement";
			Restore_IFS;
			var_type="${var_element[0]}";
			var_name="${var_element[1]}";
			var_dependencie="${var_element[2]}";
			var_software=$(Test_Software -NAME "$var_name" -TYPE "$var_type");
			# Test : software.
			if [[ "$var_software" != "available" ]]; then
				# Test : dependencie.
				if [[ "$var_dependencie" == "mandatory" ]]; then
					var_requirement_nok+=("${var_type}#${var_name}#${var_software}");
				else
					var_requirement_warn+=("${var_type}#${var_name}#${var_software}");
				fi;
			fi;
		done;
		# Tests : ok, warn, nok.
		if [[ "${#var_requirement_warn[@]}" -eq 0 && "${#var_requirement_nok[@]}" -eq 0 ]]; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- software requirements : all dependencie(s) available.";
			fi;
		else
			if [[ "${#var_requirement_warn[@]}" -ge 1 && "${#var_requirement_nok[@]}" -eq 0 ]]; then
				# Loop : valuelist requirements warn.
				for var_requirement_warn_element in "${var_requirement_warn[@]}"; do
					IFS='#' read -ra var_requirement_warn_element_spec <<< "$var_requirement_warn_element";
					Restore_IFS;
					var_requirement_warn_show+=("${var_requirement_warn_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_warn_show_formatted=$(IFS=', '; echo "${var_requirement_warn_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[ORANGE_LIGHT]}${var_requirement_warn_show_formatted}${COLOR[STOP]} are required but optionnal.";
				fi;
			elif [[ "${#var_requirement_warn[@]}" -ge 1 && "${#var_requirement_nok[@]}" -ge 1 ]]; then
				# Loop : valuelist requirements warn.
				for var_requirement_warn_element in "${var_requirement_warn[@]}"; do
					IFS='#' read -ra var_requirement_warn_element_spec <<< "$var_requirement_warn_element";
					Restore_IFS;
					var_requirement_warn_show+=("${var_requirement_warn_element_spec[1]}");
				done;
				# Loop : valuelist requirements nok.
				for var_requirement_nok_element in "${var_requirement_nok[@]}"; do
					IFS='#' read -ra var_requirement_nok_element_spec <<< "$var_requirement_nok_element";
					Restore_IFS;
					var_requirement_nok_show+=("${var_requirement_nok_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_warn_show_formatted=$(IFS=', '; echo "${var_requirement_warn_show[*]}");
				var_requirement_nok_show_formatted=$(IFS=', '; echo "${var_requirement_nok_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[RED_LIGHT]}${var_requirement_nok_show_formatted}${COLOR[STOP]} are required and mandatory - ${COLOR[ORANGE_LIGHT]}${var_requirement_warn_show_formatted}${COLOR[STOP]} are required but optionnal.";
				fi;
				Stop_App -CODE 1 -TEXT "Need to fix dependencies.";
			elif [[ "${#var_requirement_warn[@]}" -eq 0 && "${#var_requirement_nok[@]}" -ge 1 ]]; then
				# Loop : valuelist requirements nok.
				for var_requirement_nok_element in "${var_requirement_nok[@]}"; do
					IFS='#' read -ra var_requirement_nok_element_spec <<< "$var_requirement_nok_element";
					Restore_IFS;
					var_requirement_nok_show+=("${var_requirement_nok_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_nok_show_formatted=$(IFS=', '; echo "${var_requirement_nok_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[RED_LIGHT]}${var_requirement_nok_show_formatted}${COLOR[STOP]} are required and mandatory.";
				fi;
				Stop_App -CODE 1 -TEXT "Need to fix dependencies.";
			fi;
		fi;
	fi;
	# Test : output (data backup).
	if [[ "${PARAM[BACKUP]}" == true ]]; then
		# Some definition.
		local var_naming;
		# Define naming mode based on output definition.
		if [[ "${PARAM[OUTPUT]}" =~ -${DATE[DATETIME1]}\. ]]; then var_naming="date"; else var_naming="custom"; fi;
		# Test : output mode.
		if [[ "$var_naming" == "date" ]]; then
			# Test : get previous output old.
			if [[ -n "${PARAM[OUTPUT_BACKUP]}" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) in '${PARAM[OUTPUT_BACKUP]}' (date name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (date name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (date name mode).";
				fi;
			fi;
		else
			# Test : move previous output to old.
			if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT]}"; then
					mv "${PARAM[OUTPUT]}" "${PARAM[OUTPUT_BACKUP]}";
					# TMPSOLUTION.
					# Test : move all file formats.
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.json"; then
						mv "${PARAM[OUTPUT]%.csv}.json" "${PARAM[OUTPUT_BACKUP]%.csv}.json";
					fi;
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.yaml"; then
						mv "${PARAM[OUTPUT]%.csv}.yaml" "${PARAM[OUTPUT_BACKUP]%.csv}.yaml";
					fi;
					# /TMPSOLUTION.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) moved to '${PARAM[OUTPUT_BACKUP]}' (custom name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (custom name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (custom name mode).";
				fi;
			fi;
		fi;
	fi;
	# Test : output (main).
	if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" && "${PARAM[RAW]}" != true ]]; then
		# Tests : folder, file.
		if [[ "${PARAM["OUTPUT"]}" =~ /$ ]]; then
			if Test_Dir -PATH "${PARAM[OUTPUT]}" -CREATE; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- current output to folder '${PARAM[OUTPUT]}'.";
				fi;
			else
				Stop_App -CODE 1 -TEXT "Folder output '${PARAM[OUTPUT]}' can't be writed.";
			fi;
		elif Test_File -PATH "${PARAM[OUTPUT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current output to file '${PARAM[OUTPUT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "File output '${PARAM[OUTPUT]}' can't be writed.";
		fi;
	fi;
	# Test : logging.
	if [[ "$(declare -p LOG 2>/dev/null)" =~ declare\ -A\ LOG ]] && [[ "${#LOG[@]}" -gt 0 && -n "${LOG[PATH]}" ]]; then
		# Some definition.
		local var_log;
		# Test : Logging path ('/var/log' for sure ...).
		if Test_Dir -PATH "${LOG[PATH]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current log to folder '${LOG[PATH]}'.";
			fi;
			# Loop : valuelist logging files.
			for var_log in "${!LOG[@]}"; do
				if [[ "$var_log" != "PATH" ]]; then
					Test_File -PATH "${LOG[$var_log]}" -CREATE;
				fi;
			done;
		else
			Stop_App -CODE 1 -TEXT "Folder output '${LOG[PATH]}' can't be writed.";
		fi;
	fi;
	# Test : credential.
	if [[ -n "${PARAM[CREDENTIAL]}" && "${PARAM[CREDENTIAL]}" != "null" && "${PARAM[QUIET]}" != true ]]; then
		# Some definition.
		local var_credential;
		local var_software;
		# Indexed array.
		var_credential_show=();
		local var_credential_show_formatted;
		# Loop : indexlist credential. 
		for var_credential in "${!CREDENTIAL[@]}"; do
			var_credential_show+=("$var_credential");
		done;
		# Comma-separated values.
		var_credential_show_formatted=$(IFS=', '; echo "${var_credential_show[*]}");
		Restore_IFS;
		echo -e "- credential(s) used for : ${var_credential_show_formatted}.";
	fi;
	if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
}


# Get_EndingApp.
#	DESCRIPTION : script ending pack.
#	RETURN : string.
function Get_EndingApp() {
	# Some definition.
	local var_date_start;
	local var_date_end;
	local var_date_end_seconds;
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Some init;
		var_date_start="${DATE["TIMESTAMP0"]}";
		var_date_end="$(date "+%s%3N")";
		var_date_end_seconds=$(Get_DateDiff -START "$var_date_start" -END "$var_date_end");
		echo -e "\n${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: executed in ${var_date_end_seconds} seconds ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
	fi;
}


# Stop_App.
#	DESCRIPTION : end program.
#	RETURN : string and code.
function Stop_App() {
	# Some definition.
	local var_code;
	local var_text;
	local var_quiet;
	local var_help;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CODE)
				var_code="$2"
				shift 2
				;;
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-QUIET)
				var_quiet=true
				shift
				;;
			-HELP)
				var_help=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : splash error.
	if [[ "$var_quiet" != true && -n "$var_text" ]]; then
		Write_Script -TEXT "EXIT ERROR ! ${var_text}";
	fi;
	# Test : splash man.
	if [[ "$var_help" == true ]]; then Get_Help; fi;
	wait;
	exit "$var_code";
}


# Debug_Start.
#	DESCRIPTION : debug app.
function Debug_Start() { set -x; }


# Debug_Stop.
#	DESCRIPTION : debug app.
function Debug_Stop() { set +x; }


# Get_TableHeader.
#	DESCRIPTION : display a table index.
#	RETURN : string.
function Get_TableHeader() {
	# Some definition.
	local var_array;
	local var_element;
	local var_param;
	local var_param_length;
	local var_dot_length;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a name param and the max character count a value of this param can be in the rows.
		for element_header in "${!var_array[@]}"; do
			var_element="${var_array[$element_header]}";
			var_param="$(echo "$var_element" | cut -d ':' -f1)";
			var_param_length="${#var_param}";
			var_dot_length="$(echo "$var_element" | cut -d ':' -f2)";
			# Calculate how many characters to add more.
			var_param_length_more=$((var_dot_length - var_param_length));
			var_dot_length_more=$((var_param_length - var_dot_length));
			# Ready to construct header.
			var_line1+="$var_param";
			var_line1+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_param_length_more");
			var_line1+="  ";
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length");
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length_more");
			var_line2+="  ";
		done;
		echo -e "$var_line1";
		echo -e "$var_line2";
	fi;
}


# Get_TableRow.
#	DESCRIPTION : display a table row.
#	RETURN : string.
function Get_TableRow() {
	# Some definition.
	local var_array;
	local var_action;
	local var_element;
	local var_value;
	local var_value_length;
	local var_max;
	local var_line;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			-ACTION)
				var_action="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a value, the max character count this value can be.
		for element_row in "${!var_array[@]}"; do
			var_element="${var_array[$element_row]}";
			var_value="$(echo "$var_element" | cut -d ':' -f1)";
			var_max="$(echo "$var_element" | cut -d ':' -f2)";
			# Test : tick cross.
			if [[ "$var_value" == "tick" || "$var_value" == "cross" ]]; then
				if [[ "$var_value" == "tick" ]]; then
					var_value="${COLOR[TICK]}";
				elif [[ "$var_value" == "cross" ]]; then
					var_value="${COLOR[CROSS]}";
				fi;
				var_value_length=3;
			# TODO
			# elif [[ "$var_value" == *"tick"* || "$var_value" == *"cross"* ]]; then
			else
				var_value_length="${#var_value}";
			fi;
			# Calculate how many space to add more.
			var_value_length_more=$((var_max - var_value_length));
			# Ready to construct row.
			var_line+="$var_value";
			var_line+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_value_length_more");
			var_line+="  ";
		done;
		# Test : replace line.
		if [[ "$var_action" == "check" ]]; then
			echo -ne "$var_line";
		else
			echo -e "\\r$var_line";
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_string.sh               | #
# |_____________________________________________| #


# ======================== #
#          STRING          #
#
#	DESCRIPTION : pre-defined work with simple string manipulation.


# Get_DataFormat.
#	DESCRIPTION : get format based on first line content.
#	RETURN : string or code.
function Get_DataFormat() {
	# Some definition.
	local var_data;
	local var_substring;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Restrict test.
	var_substring="${var_data:0:50}";
	# Tests : CSV, JSON, YAML.
	if [[ "$var_substring" =~ , && ! "$var_substring" =~ [:{].*, ]]; then
		echo "csv";
	elif [[ "$var_substring" =~ ^[\{\[] ]]; then
		echo "json";
	elif [[ "$var_substring" =~ : && ! "$var_substring" =~ [,{]:*, ]]; then
		echo "yaml";
	else
		return 1;
	fi;
}


# Remove_DuplicateInString.
#	DESCRIPTION : remove duplicate value in string.
#	RETURN : string.
function Remove_DuplicateInString() {
	# Some definition.
	local var_string;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	echo "$var_string" | tr ' ' '\n' | sort | uniq | xargs;
}


# Search_InCsv.
#	DESCRIPTION : search if value exists in CSV file.
#	RETURN : code.
#	TODO : replace break.
function Search_InCsv() {
	# Some definition.
	local var_header;
	local var_header_cut;
	local var_line;
	local var_line_cut;
	local var_pattern;
	local var_delimiter;
	local var_position;
	local i;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-HEADER)
				var_header="$2"
				shift 2
				;;
			-LINE)
				var_line="$2"
				shift 2
				;;
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_delimiter" ]; then var_delimiter=","; fi;
	# Split headers and data line into fields.
	IFS="$var_delimiter" read -ra var_header_cut <<< "$var_header";
	IFS="$var_delimiter" read -ra var_line_cut <<< "$var_line";
	Restore_IFS;
	# Find the index of the corresponding column.
	var_position=-1;
	for i in "${!var_header_cut[@]}"; do
		if [[ "${var_header_cut[$i]}" == "$var_pattern" ]]; then
			var_position="$i";
			break;
		fi;
	done;
	# Test : valid index.
	if [[ "$var_position" -ge 0 && "$var_position" -lt "${#var_line_cut[@]}" ]]; then
		echo "${var_line_cut[$var_position]}";
	else
		return 1;
	fi;
}


# Set_RepeatCharacter.
#	DESCRIPTION : calculate how many same character repeat needed.
#	RETURN : string.
function Set_RepeatCharacter() {
	# Some definition.
	local var_character;
	local var_count;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-CHARACTER)
				var_character="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : if arg empty.
	if [ -z "$var_character" ] || [ -z "$var_count" ]; then
		return 1;
	else
		var_result="";
		for ((i=0; i < "$var_count"; i++)); do
			var_result+="$var_character";
		done;
	fi;
	echo "$var_result";
}


# Limit_StringLength.
#	DESCRIPTION : limit number of character in a string.
#	RETURN : string.
#	TODO : replace -DIRECTION by -POSITION.
function Limit_StringLength() {
	# Some definition.
	local var_string;
	local var_count;
	local var_count_before;
	local var_count_after;
	local var_direction;
	local var_spacer;
	local var_spacer_length;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			-DIRECTION)
				var_direction="$2"
				shift 2
				;;
			-SPACER)
				var_spacer="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_direction" ]; then var_direction="after"; fi;
	if [ -z "$var_spacer" ]; then var_spacer="(...)"; fi;
	var_spacer_length="${#var_spacer}";
	# Test : if arg empty.
	if [ -z "$var_string" ] || [ -z "$var_count" ]; then
		return 1;
	else
		# Test : cut needed.
		if [ ${#var_string} -gt "$var_count" ]; then
			# Add spacer length to cut count.
			var_count_before=$((${#var_string} - var_count + var_spacer_length + 1));
			var_count_after=$((var_count - var_spacer_length));
			# Tests : sub place.
			if [ "$var_direction" = "before" ]; then
				var_result+="$var_spacer";
				var_result+=$(echo "$var_string" | cut -c "$var_count_before"-"${#var_string}");
			elif [ "$var_direction" = "after" ]; then
				var_result+=$(echo "$var_string" | cut -c 1-"$var_count_after");
				var_result+="$var_spacer";
			fi;
			echo "$var_result";
		else
			echo "$var_string";
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_date.sh               | #
# |__________________________________________| #


# ====================== #
#          DATE          #
#
#	DESCRIPTION : pre-defined working date.


# DATE.
declare -gA DATE;
DATE["CAPTURE"]="$(date "+%s%3N")";
# ex EXTRA_TIMESTAMP : execution time diff calc (timestamp in milliseconds).
DATE["TIMESTAMP0"]="${DATE[CAPTURE]}";
# ex CURRENT_DATE : jekyll file name markdown, manuel page.
DATE["DATE0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d")";
# distro files JSON & YAML.
DATE["DATETIME0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%dT%H:%M:%S%z")";
# ex COMPACT_TIMESTAMP & DATE_FILE : output filenames.
DATE["DATETIME1"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y%m%d_%H%M%S")";
# ex FULL_TIMESTAMP & DATE_START_EXEC : page.date for jekyll inside markdown file, added date inside join-files output.
DATE["DATETIME2"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d %H:%M:%S")";
# ex FORMATTED_DATE_TIME & DATE_START : bash execution time.
DATE["DATETIME3"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%d/%m/%Y - %Hh%M")";


# Get_DateDiff.
#	DESCRIPTION : diff calc in seconds & milliseconds.
#	RETURN : string.
function Get_DateDiff() {
	# Some definition.
	local var_start;
	local var_start_sec;
	local var_start_msec;
	local var_end;
	local var_end_msec;
	local var_end_sec;
	local var_diff_sec;
	local var_diff_msec;
	local var_msec_2_digits;
	local var_result;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-START)
				var_start="$2"
				shift 2
				;;
			-END)
				var_end="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Extract seconds & milliseconds.
	var_start_sec="${var_start:0:10}";
	var_start_msec="${var_start:10:3}";
	var_end_sec="${var_end:0:10}";
	var_end_msec="${var_end:10:3}";
	# Remove leading zeros to avoid octal interpretation.
	var_start_sec=$((10#$var_start_sec));
	var_start_msec=$((10#$var_start_msec));
	var_end_sec=$((10#$var_end_sec));
	var_end_msec=$((10#$var_end_msec));
	# Diff calc.
	var_diff_sec=$((var_end_sec - var_start_sec));
	var_diff_msec=$((var_end_msec - var_start_msec));
	# Test : milliseconds diff is negative.
	if [ $var_diff_msec -lt 0 ]; then
		var_diff_sec=$((var_diff_sec - 1));
		var_diff_msec=$((var_diff_msec + 1000));
	fi;
	# Test : milliseconds with two digits.
	if [ $var_diff_msec -lt 10 ]; then
		var_msec_2_digits="0$((var_diff_msec / 10))";
	else
		var_msec_2_digits=$((var_diff_msec / 10));
	fi;
	var_result="${var_diff_sec}.${var_msec_2_digits}";
	echo "$var_result";
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_network.sh               | #
# |_____________________________________________| #


# ========================= #
#          NETWORK          #
#
#	DESCRIPTION : network state for all APPs.

# Get_FirstNetwork.
#	DESCRIPTION : get name of the first network interface.
#	RETURN : string or code.
function Get_FirstNetwork() {
	# Some definition.
	local var_result;
	local var_interfaces;
	var_result="fail-network";
	var_interfaces=$(ip -o link show | awk -F': ' '{print $2}');
	# Loop : interfaces.
	for interface in $var_interfaces; do
		if [[ $interface != "lo" ]]; then var_result="$interface"; break; fi;
	done
	echo "$var_result";
}


# Get_Distant.
#	DESCRIPTION : get distant resource - file, string, ping, git repository, with auth available.
#	RETURN : string or code.
#	TO-DO : git auth.
function Get_Distant() {
	# Some definition - menu.
	local var_method;
	local var_type;
	local var_url;
	local var_path;
	local var_credential;
	local var_username;
	local var_password;
	local var_overwrite;
	local var_log;
	local var_verbose;
	# Some definition - next.
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-METHOD)
				var_method="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-LOG)
				var_log="$2"
				shift 2
				;;
			-VERBOSE)
				var_verbose=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Some default.
	if [[ -z "$var_method" ]]; then var_method="wget"; fi;
	if [[ -z "$var_type" ]]; then var_type="file"; fi;
	# Test : auth.
	if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" && "$var_credential" == *":"* ]]; then
		var_username=$(echo -e "$var_credential" | cut -d ':' -f 1);
		var_password=$(echo -e "$var_credential" | cut -d ':' -f 2);
	fi;
	# Tests : method.
	if [[ "$var_method" == "wget" ]]; then
		# Tests : file, string, ping.
		if [[ "$var_type" == "file" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if [[ -n "$var_verbose" ]]; then
						wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				else
					if [[ -n "$var_verbose" ]]; then
						wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log"
							if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if [[ -n "$var_verbose" ]]; then
						wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				else
					if [[ -n "$var_verbose" ]]; then
						wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				fi;
			fi;
		elif [[ "$var_type" == "string" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				fi;
			fi;
			# Test : good content.
			if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
		elif [[ "$var_type" == "ping" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		fi;
	elif [[ "$var_method" == "curl" ]]; then
		# Test : auth.
		if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
			var_result=$(curl -s -H "Authorization: token ${var_credential}" "$var_url" | tr -d '\n' | sed 's/ //g');
		else
			var_result=$(curl -s "$var_url" | tr -d '\n' | sed 's/ //g');
		fi;
		# Test : good content.
		if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
	elif [[ "$var_method" == "git" ]]; then
		# Test : replace.
		if [[ "$var_overwrite" = true ]]; then
			if git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; then return 0; else return 1; fi;
		else
			git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; return 0;
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_fs.sh               | #
# |________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interaction for all APPs.


# Test_Dir.
#	DESCRIPTION : check directory.
#	RETURN : code.
function Test_Dir() {
	# Some definition.
	local var_path;
	local var_create;
	local var_chown;
	local var_chmod;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : folder.
	if [[ -n "$var_path" ]]; then
		if [[ "$var_create" = true ]]; then mkdir -p "$var_path" &> /dev/null; fi;
		if [ -d "$var_path" ]; then
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown -R "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod -R "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Test_DirPopulated().
#	DESCRIPTION : check if folder haz content in it.
#	RETURN : code.
function Test_DirPopulated() {
	# Some definition.
	local var_path;
	local var_extension;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-EXTENSION)
				var_extension="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	if [[ -n "$(ls -A "$var_path")" ]]; then
		if [[ -n "$var_extension" ]]; then
			if find "$var_path" -type f -name "*$var_extension" &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		else
			return 0;
		fi;
	else
		return 1;
	fi;
}


# Test_File.
#	DESCRIPTION : check file.
#	RETURN : code.
function Test_File() {
	# Some definition.
	local var_path;
	local var_overwrite;
	local var_create;
	local var_chown;
	local var_chmod;
	local var_exec;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-EXEC)
				var_exec=true
				shift
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		if [ -f "$var_path" ]; then
			if [[ "$var_overwrite" = true ]]; then
				rm "$var_path" &> /dev/null;
				# Tests : dir chown + chmod || chown || chmod || regular.
				if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" && -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown" -CHMOD "$var_chmod";
				elif [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown";
				elif [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHMOD "$var_chmod";
				else
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				fi;
				touch "$var_path" &> /dev/null;
			fi;
		else
			if [[ "$var_overwrite" = true ]] || [[ "$var_create" = true ]]; then
				# Tests : dir chown + chmod || chown || chmod || regular.
				if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" && -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown" -CHMOD "$var_chmod";
				elif [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown";
				elif [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHMOD "$var_chmod";
				else
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				fi;
				touch "$var_path" &> /dev/null;
			fi;
		fi;
		if [ -f "$var_path" ]; then
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_exec" && "$var_exec" = true ]]; then chmod +x "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Search_File.
#	DESCRIPTION : get filepath with regex, favourite extension filter available (the last win).
#	RETURN : string or code.
function Search_File() {
	# Some definition - menu.
	local var_path;
	local var_extension;
	local var_tool;
	local var_mode;
	# Some definition - next.
	local var_extension_favorite;
	local var_file;
	local var_result;
	local var_result_favorite;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="${2%[\\/]}"; shift 2 ;;
			-EXTENSION) var_extension="$2"; shift 2 ;;
			-TOOL) var_tool="$2"; shift 2 ;;
			-MODE) var_mode="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Some init.
	var_extension_favorite="${var_extension##*|}";
	# Tests : distro-tracker/distro-reporter.
	if [[ "$var_tool" == "distro-tracker" || "$var_tool" == "distro-reporter" ]]; then
		# Tests : distro-tracker.current.EXT || distro-reporter.EXT || DT/DR-date.EXT .
		if [[ "$var_mode" == "name" ]]; then
			# Loop : Process Substitution auto-discovery - last file win data state.
			while IFS= read -r var_file; do
				# Test : data exists.
				if Test_File -PATH "$var_file"; then
					var_result="$var_file";
					# Test : favorite extension.
					if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
						var_result_favorite="$var_file";
					fi;
				fi;
			done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/(distro-tracker\.current|distro-reporter)\.(${var_extension})$" 2>/dev/null | sort);
			Restore_IFS;
		elif [[ "$var_mode" == "date" ]]; then
			# Loop : Process Substitution auto-discovery - last file win data state.
			while IFS= read -r var_file; do
				# Test : data exists.
				if Test_File -PATH "$var_file"; then
					var_result="$var_file";
					# Test : favorite extension.
					if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
						var_result_favorite="$var_file";
					fi;
				fi;
			done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/(DT|DR)-([0-9]{8}_[0-9]{6})\.(${var_extension})$" 2>/dev/null | sort);
			Restore_IFS;
		fi;
		# Test : good content.
		if [[ -n "$var_result" && "$var_result" != false && "$var_result" != "null" ]]; then
			# Test : favorite extension.
			if [[ -n "$var_result_favorite" && "$var_result_favorite" != false && "$var_result_favorite" != "null" ]]; then
				echo "$var_result_favorite";
			else
				echo "$var_result";
			fi;
		else
			return 1;
		fi;
	elif [[ "$var_tool" == "distro-publisher" ]]; then
		# Loop : Process Substitution auto-discovery - last file win data state.
		while IFS= read -r var_file; do
			# Test : data exists.
			if Test_File -PATH "$var_file"; then
				var_result="$var_file";
				# Test : favorite extension.
				if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
					var_result_favorite="$var_file";
				fi;
			fi;
		done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/${DATE[DATE0]}-[0-9]{1,3}-report\.(${var_extension})$" 2>/dev/null | sort);
		Restore_IFS;
		# Test : good content.
		if [[ -n "$var_result" && "$var_result" != false && "$var_result" != "null" ]]; then
			# Test : favorite extension.
			if [[ -n "$var_result_favorite" && "$var_result_favorite" != false && "$var_result_favorite" != "null" ]]; then
				echo "$var_result_favorite";
			else
				echo "$var_result";
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Set_File.
#	DESCRIPTION : create file.
#	RETURN : code.
function Set_File() {
	# Some definition.
	local var_path;
	local var_raw;
	local var_interpret;
	local var_source;
	local var_append;
	local var_encode;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-RAW)
				var_raw="$2"
				shift 2
				;;
			-INTERPRET)
				var_interpret=true
				shift
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-APPEND)
				var_append=true
				shift
				;;
			-ENCODE)
				var_encode="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		touch "$var_path" &> /dev/null;
		# Tests : echo, cp.
		if [[ -n "$var_raw" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				# Test : interpreter.
				if [[ -n "$var_interpret" ]]; then
					echo -e "$var_raw" | tee -a "$var_path" > /dev/null;
				else
					echo "$var_raw" | tee -a "$var_path" > /dev/null;
				fi;
			else
				# Test : base64.
				if [[ -n "$var_encode" ]]; then
					echo "$var_raw" | base64 -d > "$var_path";
				else
					# Test : interpreter.
					if [[ -n "$var_interpret" ]]; then
						echo -e "$var_raw" | tee "$var_path" > /dev/null
					else
						echo "$var_raw" | tee "$var_path" > /dev/null;
					fi;
				fi;
			fi;
		elif [[ -n "$var_source" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				cat "$var_source" >> "$var_path";
			else
				cp "$var_path" "$var_source" &> /dev/null;
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Get_FileExtension.
#	DESCRIPTION : get last extension, with avoiding different extensions.
#	RETURN : code.
function Get_FileExtension() {
	# Some definition.
	local var_path;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	echo "$var_path" | rev | cut -d '.' -f 1 | rev;
}


# Find_FileLine.
#	DESCRIPTION : find line started by pattern in a file.
#	RETURN : string or code.
function Find_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file;
	local var_line;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE)
				var_file="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file"; then
		var_line=$(grep "$var_pattern" "$var_file" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line" ]]; then echo "$var_line"; else return 1; fi;
	else
		return 1;
	fi;
}


# Compare_FileLine.
#	DESCRIPTION : search 1st line started by pattern in 1st file, compare it to 2nd file.
#	RETURN : string or code.
function Compare_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file1;
	local var_file2;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1" && Test_File -PATH "$var_file2"; then
		var_line1=$(grep "^$var_pattern" "$var_file1" | head -n 1);
		var_line2=$(grep "^$var_pattern" "$var_file2" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line1" && "$var_line1" != false && "$var_line1" != "null" && \
			-n "$var_line2" && "$var_line2" != false && "$var_line2" != "null" ]]; then
			# Test : return file number 2 content for different content, return false for same content.
			if [[ "$var_line1" != "$var_line2" ]]; then echo "$var_line2"; else return 1; fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Backup_FileLine.
#	DESCRIPTION : recover line in 1st file from a 2nd file.
#	RETURN : string or code.
#	TODO : verify character | escaping.
function Backup_FileLine() {
	# Some definition.
	local var_pattern;
	local var_default;
	local var_file1;
	local var_file2;
	local var_diff;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DEFAULT)
				var_default="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1"; then
		var_diff=$(Compare_FileLine -PATTERN "$var_pattern" -FILE1 "$var_file1" -FILE2 "$var_file2");
		var_pattern="^${var_pattern}";
		# Tests : replace by old content for different content, replace by default for same/empty content.
		if [[ -n "$var_diff" && "$var_diff" != false ]]; then
			sed -i "s|${var_pattern}|${var_diff}|" "$var_file1";
			echo "old";
		elif [[ -n "$var_default" ]]; then
			echo -e "$var_default" >> "$var_file1";
			echo "default";
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
# shellcheck disable=SC2034
#  ___________________________________________
# |                                           | #
# |               func_array.sh               | #
# |___________________________________________| #


# ======================= #
#          ARRAY          #
#
#	DESCRIPTION : pre-defined work with indexed & associative (book) arrays.


# Sort_Array.
#	DESCRIPTION : write new array indexed from original.
#	TODO : auto index and manual index option.
#	RETURN : string.
function Sort_Array() {
	# Some definition.
	local var_name;
	local var_delimiter;
	local var_index;
	local var_key;
	local var_value;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : data exists.
	if [[ -z "$var_name" || -z "$var_delimiter" ]]; then return 1; fi;
	if ! declare -p "$var_name" &>/dev/null; then return 1; fi;
	# Construct array indexed.
	declare -gA "${var_name}_SORT";
	# Get data from original array.
	local -n var_name_orig="$var_name";
	local var_name_sorted="${var_name}_SORT";
	# Loop : get data from original array.
	for var_key in "${!var_name_orig[@]}"; do
		local var_value="${var_name_orig[$var_key]}";
		local var_index="${var_value%%"${var_delimiter}"*}";
		# Stores the key in the sorted array under the extracted index.
		local -n var_name_ref="${var_name_sorted}"
		var_name_ref["$var_index"]="$var_key";
	done;
}


# Search_InArray.
#	DESCRIPTION : search if value exists in indexed array.
#	RETURN : code.
function Search_InArray() {
	# Some definition.
	local var_pattern;
	local var_data;
	local var_array;
	local var_element;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : need all param.
	if [ -z "$var_pattern" ] || [ -z "$var_data" ]; then return 1; fi;
	# IFS for delimit array.
	IFS=" " read -ra var_array <<< "$var_data";
	Restore_IFS;
	# Loop : indexlist search in array.
	for var_element in "${var_array[@]}"; do
		if [[ "$var_element" == "$var_pattern" ]]; then
			return 0;
		fi;
	done;
	return 1;
}


# Get_PositionInArray.
#	DESCRIPTION : search index position by this pattern in indexed array.
#	RETURN : string or code.
function Get_PositionInArray() {
	# Some definition.
	local var_pattern;
	local var_delimiter;
	local var_array;
	local var_element;
	local var_index=-1;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_delimiter" ]; then
		var_delimiter=",";
	fi;
	# Test : need all param.
	if [ -z "$var_pattern" ] || [ -z "$var_data" ]; then
		return 1;
	fi;
	# Split the data using the specified delimiter.
	IFS="$var_delimiter" read -ra var_array <<< "$var_data";
	Restore_IFS;
	# Loop : indexlist through the columns to find the pattern.
	for var_element in "${!var_array[@]}"; do
		if [[ "${var_array[${var_element}]}" == "$var_pattern" ]]; then
			var_index="$var_element";
		fi;
	done;
	# Test : pattern found.
	if [[ "$var_index" -ge 0 ]]; then
		echo "$var_index";
	else
		return 1;
	fi;
}


# Measure_MaxInArray.
#	DESCRIPTION : calculate longest word in array.
#	RETURN : string.
function Measure_MaxInArray() {
	# Some definition.
	local var_count;
	local var_array;
	local var_word;
	local var_length;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		var_count=0;
		for element in "${!var_array[@]}"; do
			var_word="${var_array[$element]}";
			var_length="${#var_word}";
			if [ "$var_length" -gt "$var_count" ]; then var_count="$var_length"; fi;
		done;
		echo "$var_count";
	fi;
}


# Add_Book.
#	DESCRIPTION : construct associative array.
function Add_Book() {
	# Some definition.
	local var_name="$1";
	# Test : book exists.
	if ! declare -p "$var_name" &>/dev/null; then declare -gA "$var_name"; fi;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRO" ]]; then
		# Some definition.
		local var_element;
		local var_data;
		local var_index;
		local var_fullname;
		local var_mode;
		local var_current_index;
		local var_current_release;
		local var_archive_index;
		local var_archive_release;
		local var_element_suffixes;
		local var_element_suffixes_var;
		local var_array;
		local var_array_keys;
		local var_array_suffixes;
		# Some init.
		var_element="$2";
		var_data="$3";
		# mode, indexes & releases.
		declare -a var_array_suffixes=("index" "fullname" "mode" "current_index" "current_release" "archive_index" "archive_release");
		# Loop : var_suffix key.
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			var_element_suffixes_var="var_${var_element_suffixes}";
			declare "${var_element_suffixes_var}"="${var_element}_${var_element_suffixes}";
		done;
		# Construct index & release values from Get_Distribution.
		IFS=',' read -ra var_array <<< "$var_data";
		Restore_IFS;
		declare -a var_array_keys=("$var_index" "$var_fullname" "$var_mode" "$var_current_index" "$var_current_release" "$var_archive_index" "$var_archive_release");
		# Loop var_suffix value.
		for i in "${!var_array_keys[@]}"; do
			eval "${var_name}[\"${var_array_keys[$i]}\"]=\"${var_array[$i]}\"";
		done;
	elif [[ "$var_name" == "TMP" ]]; then
		# Some init.
		local var_element="$2";
		local var_data="$3";
		eval "${var_name}[\"${var_element}\"]=\"${var_data}\"";
	fi;
}


# Get_Book.
#	DESCRIPTION : get associative array.
#	RETURN : string or code.
function Get_Book() {
	# Some definition.
	local var_name;
	local var_element;
	local var_data;
	local var_array_suffixes;
	local var_element_suffixes;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-ELEMENT)
				var_element="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRO" ]]; then
		# Loop: var_suffix key.
		declare -a var_array_suffixes=("index" "fullname" "mode" "current_index" "current_release" "archive_index" "archive_release");
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			if [[ "$var_data" == "$var_element_suffixes" ]]; then
				var_result="${DISTRO[${var_element}_$var_element_suffixes]}";
				break;
			fi;
		done;
	fi;
	# Test : return.
	if [[ -n "$var_result" && "$var_result" != "" ]]; then echo "$var_result"; else return 1; fi;
}


# Set_Book.
#	DESCRIPTION : set associative array from PARAM.
function Set_Book() {
	# Some definition.
	local var_name;
	local var_array;
	local var_element;
	local var_key;
	local var_value;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : new array.
	if ! eval "declare -p $var_name 2>/dev/null" | grep -q 'declare -A'; then
		eval "declare -gA $var_name";
	fi;
	# Test : predefined book.
	if [[ "$var_name" == "CREDENTIAL" ]]; then
		# Loop : valuelist credentials.
		IFS=',' read -ra var_array <<< "${PARAM[CREDENTIAL]}";
		Restore_IFS;
		for var_element in "${var_array[@]}"; do
			var_key=${var_element%%:*};
			var_value=${var_element#*:};
			CREDENTIAL["${var_key^^}"]="$var_value";
		done;
	fi;
}


# Remove_Book.
#	DESCRIPTION : remove associative array.
function Remove_Book() {
	# Some definition.
	local var_name="$1";
	unset "$var_name";
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_user.sh               | #
# |__________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test_Root.
#	DESCRIPTION : check root rights.
#	RETURN : code.
function Test_Root {
	if [[ "$EUID" -eq 0 ]]; then return 0; else return 1; fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_pkgs.sh               | #
# |__________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Test_Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Test_Software() {
	# Some definition - menu.
	local var_name;
	local var_type;
	#local var_action; // SC2034 disable.
	#local var_depend; // SC2034 disable.
	# Some definition - next.
	local var_result;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME) var_name="$2"; shift 2 ;;
			-TYPE) var_type="$2"; shift 2 ;;
			-ACTION) shift 2 ;;
			-DEPEND) shift ;;
			*) shift ;;
		esac
	done
	# Tests : type software.
	if [[ "$var_type" == "program" ]]; then
		# TMP : import OverDeploy routine.
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-program";
		fi;
	elif [[ "$var_type" == "service" ]]; then
		# Test : systemd check.
		# SRC : https://stackoverflow.com/a/53640320/2704550.
		if [[ $(systemctl list-units --all --type service --full --no-legend "${var_name}.service" | sed 's/^\s*//g' | cut -f1 -d' ') == "${var_name}.service" ]]; then
			var_result="available";
		else
			var_result="fail-service";
		fi;
	elif [[ "$var_type" == "command" ]]; then
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-command";
		fi;
	else
		# tmp default.
		var_result="fail-unknow";
	fi;
	echo "$var_result";
}


# Start_Software.
#	DESCRIPTION : test effective restarting of a daemon (bash launch) or service.
#	RETURN : code.
function Start_Software() {
	# Some definition - menu.
	local var_name;
	local var_command;
	local var_enable;
	local var_user;
	local var_gui;
	# Some definition - next.
	local var_type;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME) var_name="$2"; shift 2 ;;
			-COMMAND) var_command="$2"; shift 2 ;;
			-ENABLE) var_enable=true; shift ;;
			-USER) var_user="$2"; shift 2 ;;
			-GUI) var_gui=true; shift ;;
			*) shift ;;
		esac
	done;
	# Tests : mandatory.
	if [[ -z "$var_name" && -z "$var_command" ]]; then return 1; fi;
	# Some init.
	if [ -n "$var_name" ]; then
		var_type=$(Get_FileExtension -PATH "$var_name");
	fi;
	# Tests : service || timer || daemon/command.
	if [ -n "$var_type" ] && [[ "$var_type" == "service" || "$var_type" == "timer" ]]; then
		# Test : no services operations wuthout logging.
		if Test_File -PATH "${LOG[MAIN]}" -CREATE; then
			echo "# ===== ${var_name} ===== #" >> "${LOG[MAIN]}";
			# Test : service ready.
			if [ -n "$var_enable" ]; then
				systemctl enable "$var_name" &>> "${LOG[MAIN]}";
			fi;
			# Test : service enabled = restart || start.
			if systemctl is-enabled "$var_name" &> /dev/null; then
				# Test : launch with user.
				if [ -n "$var_user" ]; then
					su -l "$var_user" -c "systemctl restart ${var_name} &>> ${LOG[MAIN]}";
				else
					systemctl restart "$var_name" &>> "${LOG[MAIN]}";
				fi;
			else
				# Test : launch with user.
				if [ -n "$var_user" ]; then
					su -l "$var_user" -c "systemctl start ${var_name} &>> ${LOG[MAIN]}";
				else
					systemctl start "$var_name" &>> "${LOG[MAIN]}";
				fi;
			fi;
		fi;
		# Test : service loaded.
		if systemctl list-units --type="$var_type" --state=loaded | grep -q "$var_name"; then
			return 0;
		else
			return 1;
		fi;
	else
		# Test : launch with user.
		if [ -n "$var_user" ]; then
			# Test : general user interface.
			if [ -n "$var_gui" ]; then xhost + &> /dev/null; fi;
			# Test : command success.
			if su -l "$var_user" bash -c "export DISPLAY=:0 && $var_command &> /dev/null"; then
				# Test : general user interface.
				if [ -n "$var_gui" ]; then xhost - &> /dev/null; fi;
				return 0;
			else
				# Test : general user interface.
				if [ -n "$var_gui" ]; then xhost - &> /dev/null; fi;
				return 1;
			fi;
		else
			# Test : command success.
			if $var_command &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		fi;
	fi;
}



# next joined content - 2025-01-06.
# shellcheck source=/dev/null
#  ______________________________________________
# |                                              | #
# |               distro-widget.sh               | #
# |______________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-widget";
APP["SECTION"]="1";
APP["SHORTNAME"]="DW";
APP["SLOGAN"]="Set a fancy widget with Conky & systemd jobs.";
APP["DESCRIPTION"]="This open-source tool generates some stats with Distro Tracker datas integration.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENTS"]="program conky mandatory";
APP["EXAMPLES"]="install for user 'test' with two formats for sources :|${APP[NAME]} -u test --source-tracker https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv --source-reporter https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json";
APP["TODO"]="null";
APP["NOTES"]="Compatible Distro Tracker >= 1.2.0.";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.1.0|2025-01-01T10:00:00+0200|Add systemd jobs. Replace distro-get with standalone generics distro-downloader & distro-reader.#1.0.0|2024-10-20T10:00:00+0200|Distro Tracker datas integration.#0.2.0|2024-01-03T10:00:00+0200|USER parameter instead PATH.#0.1.0|2024-01-02T10:00:00+0200|Style unified with Windows version.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="1.1.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE_TRACKER"]="1#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Tracker data.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv'.";
OPTIONS["SOURCE_REPORTER"]="2#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Reporter data.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json'.";
OPTIONS["SOURCE_DOWNLOADER"]="3#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Downloader.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-downloader/dev/bin/distro-downloader'.";
OPTIONS["SOURCE_READER"]="4#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Reader.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reader/dev/standalone/distro-reader.sh'.";
OPTIONS["USERNAME"]="5#u#NAME#mandatory#Local username for configuration.|no default.";
OPTIONS["QUIET"]="6#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="7#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="8#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source-tracker)
			PARAM["SOURCE_TRACKER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--source-reporter)
			PARAM["SOURCE_REPORTER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--source-downloader)
			PARAM["SOURCE_DOWNLOADER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--source-reader)
			PARAM["SOURCE_READER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--username|-u)
			PARAM["USERNAME"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			echo "${APP[VERSION]}"
			shift
			Stop_App -CODE 0
			;;
		*)
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '$1'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ===================== #
#          LOG          #
#
#	DESCRIPTION : everything that help debugging the APP.

declare -gA LOG;
LOG["PATH"]="${CONF[SEPARATOR]}var${CONF[SEPARATOR]}log${CONF[SEPARATOR]}distro";
LOG["MAIN"]="${LOG[PATH]}${CONF[SEPARATOR]}widget.log";
LOG["TRACKER"]="${LOG[PATH]}${CONF[SEPARATOR]}tracker.log";
LOG["REPORTER"]="${LOG[PATH]}${CONF[SEPARATOR]}reporter.log";


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["SOURCE_TRACKER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv";
DEFAULT["SOURCE_REPORTER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json";
DEFAULT["SOURCE_DOWNLOADER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-downloader/dev/bin/distro-downloader";
DEFAULT["SOURCE_READER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reader/dev/standalone/distro-reader.sh";
DEFAULT["QUIET"]=false;
# Some script definition.
# PATH (auto-defined).
PARAM["PATH"]="${CONF[SEPARATOR]}home${CONF[SEPARATOR]}${PARAM[USERNAME]}${CONF[SEPARATOR]}.config";
if ! Test_Dir -PATH "${PARAM["PATH"]}${CONF[SEPARATOR]}conky" -CREATE; then
	Stop_App -CODE 1 -TEXT "Conky configuration folder can't be created.";
fi;
# SOURCEs.
if [ -z "${PARAM[SOURCE_TRACKER]}" ]; then PARAM["SOURCE_TRACKER"]="${DEFAULT[SOURCE_TRACKER]}"; fi;
if [ -z "${PARAM[SOURCE_REPORTER]}" ]; then PARAM["SOURCE_REPORTER"]="${DEFAULT[SOURCE_REPORTER]}"; fi;
if [ -z "${PARAM[SOURCE_DOWNLOADER]}" ]; then PARAM["SOURCE_DOWNLOADER"]="${DEFAULT[SOURCE_DOWNLOADER]}"; fi;
if [ -z "${PARAM[SOURCE_READER]}" ]; then PARAM["SOURCE_READER"]="${DEFAULT[SOURCE_READER]}"; fi;
if ! [[ "${PARAM[SOURCE_TRACKER]}" =~ \.(csv|json|yaml)$ ]]; then
	Stop_App -CODE 1 -TEXT "Invalid format for '--source-tracker' option.";
fi;
if ! [[ "${PARAM[SOURCE_REPORTER]}" =~ \.(json|yaml)$ ]]; then
	Stop_App -CODE 1 -TEXT "Invalid format for '--source-reporter' option.";
fi;
var_file_tracker_extension=$(Get_FileExtension -PATH "${PARAM[SOURCE_TRACKER]}");
var_file_reporter_extension=$(Get_FileExtension -PATH "${PARAM[SOURCE_REPORTER]}");
# Tests : redefine requirements for user-friendly work.
if [[ "$var_file_tracker_extension" == "json" || "$var_file_reporter_extension" == "json" ]]; then
	APP["REQUIREMENTS"]+="#program jq mandatory";
elif [[ "$var_file_tracker_extension" == "yaml" || "$var_file_reporter_extension" == "yaml" ]]; then
	APP["REQUIREMENTS"]+="#program yq mandatory";
fi;
# SOURCE TRACKER.
# Tests : distant file, local file.
# The program downloads remote content for the first time or just makes sure that local content is not empty.
if [[ "${PARAM[SOURCE_TRACKER]}" =~ ^(http|www) ]]; then
	var_file_tracker="${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-tracker.${var_file_tracker_extension}";
	if Get_Distant -URL "${PARAM[SOURCE_TRACKER]}" -TYPE "file" -PATH "$var_file_tracker"; then
		if Test_File -PATH "$var_file_tracker"; then
			var_data_tracker="$(<"$var_file_tracker")";
			# Test : data exists.
			if [[ "$var_data_tracker" == "" ]]; then
				Stop_App -CODE 1 -TEXT "Distant file in '--source-tracker' option has no data.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "Distant file in '--source-tracker' option not available.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Distant file in '--source-tracker' option not reachable.";
	fi;
else
	if Test_File -PATH "${PARAM[SOURCE_TRACKER]}"; then
		var_data_tracker="$(<"${PARAM[SOURCE_TRACKER]}")";
		# Test : data exists.
		if [[ "$var_data_tracker" == "" ]]; then
			Stop_App -CODE 1 -TEXT "Local file in '--source-tracker' option has no data.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Local file in '--source-tracker' option is not available.";
	fi;
fi;
# SOURCE REPORTER.
# Tests : distant file, local file.
if [[ "${PARAM[SOURCE_REPORTER]}" =~ ^(http|www) ]]; then
	var_file_reporter="${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reporter.${var_file_reporter_extension}";
	if Get_Distant -URL "${PARAM[SOURCE_REPORTER]}" -TYPE "file" -PATH "$var_file_reporter"; then
		if Test_File -PATH "$var_file_reporter"; then
			var_data_reporter="$(<"$var_file_reporter")";
			# Test : data exists.
			if [[ "$var_data_reporter" == "" ]]; then
				Stop_App -CODE 1 -TEXT "Distant file in '--source-reporter' option has no data.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "Distant file in '--source-reporter' option not available.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Distant file in '--source-reporter' option not reachable.";
	fi;
else
	if Test_File -PATH "${PARAM[SOURCE_REPORTER]}"; then
		var_data_reporter="$(<"${PARAM[SOURCE_REPORTER]}")";
		# Test : data exists.
		if [[ "$var_data_reporter" == "" ]]; then
			Stop_App -CODE 1 -TEXT "Local file in '--source-reporter' option has no data.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Local file in '--source-reporter' option is not available.";
	fi;
fi;
# WIDGETs (auto-defined).
PARAM["WIDGET_IP"]=$(Get_FirstNetwork);
var_img="iVBORw0KGgoAAAANSUhEUgAAAaQAAABOCAYAAABv5GJlAAAI2HpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjapZhpcis5DoT/8xRzBO4Aj8M1om8wx58PpZJXtduvRwqpylQRJJBAImm3//vXcf/hFaWoy0W0tlo9r9xyi50b9Y9Xv76Dz9f3Yyjfd+HzuMv7nhQZSlzT40+tj2t4jt8TntfQuSsfDOm8fxiff2j5tq9fDN0LJdtR5GbdhtptKMXHD+E20B9u+dpUProwbg/u+Y8w8HH2NZ6j5X74y99ZiN4qrJNi3Ckkz3dK9waSfbJL3X64vhsP2mC/v0t67oSAvIrT26uxo2NbzS8f+oTK290XtOK5Y/QVrRzvR9KXINe368txF8prVK7Qf1g5630XP4+LmmO2oy/Rt885S8/lM170XAl1vZ16unjd8dxgCVtaHVurXvgUTMj1bryVrJ6kwvLTD94ztBBB5YQcVujhhH1dZ5hsMcftonAT44zpGtQkscUJbiFle4cTJbW0koLkvGDPKb7tJVzLNj/dtZqy8go8GgPGAlP++O3+dMI5VgohWCyBPjzwjdGCzTYMOfvmMRAJ5w5quQL8fH99Ga4JBItF2UqkEdjxMDFKeGeCdAGdeLBwfdRgkHUbIEQsXdhMSCAAaiGVUIOXGCUEAqkA1Nk6BRQHCIRS4mKTMadUwUajLc0UCdejsUSGHeOQGUiUVJOATUsdsHIu5I9kJYd6SSWXUmqBCUsrvaaaa6m1SjVS7JIkOylSRUSlSdekWYtWFVVt2ltsCdIsrTZp2lrrnTU7ljuzOw/0PuJII4/iRh0ydLTRJ+kz8yyzTpk62+wrrrTgj1WXLF1t9R02qbTzLrtu2brb7odUO8mdfMqpR46edvobajes395/gFq4UYsXUvagvKHGqMjTRDA6KYYZgEWXA4iLQUBCR8PMa8g5GnKGmW+RqiiWe8UwW8EQA8G8QywnPLFz8YGoIfd/4eYkf8It/lvknEH3h8h9x+0Vasva0LwQe1ShBdUnqu9IIGXSKHVY9vdFkGYlfPgepjuhJAixrnS2rErZgt2pUQe3evyoNtJ3Hmd2fJlWPSd2eMeIShL71bSnS7Wdk/Yw3jwjZHlhlPmxj9VmGiHNE/eG72oY+eBFUTn7LKcdc5c1HLjs0UWxWNLC6kgq1W9wgEjP8KvLmafPdI7sWT6Ycw97BH9j8lrUS5pjNP9qb/rdWD65BFyDw8mcXdbU59Z+4aieKn5D8m9+OtsZPffp6run7+b+0Uu623bvjn4y90+efvPTPey9ufrC088g5O8GL1/dN2c/gfp7TN3PoP4eU/czqO+edpHgd1uVVI+FasylsqsUeqvSwnIlx65h5ZYKVCQsTO+Y+aQ6qeexxtCUoI4eViqzXxiNnnXXEcZGGJ+MUtDq6PWhz7366nlMMQaBZxoaLkNZNawFN7Whuo9S5msVyavFfWak1jetaZ8KIbi6O9O0pQl19K5z+I1kwNLqzIfR6uhrVrhNiC56R/zZa69E+NBM/QJiE6NCWUNeB0rK66p0VBgB406hCbv6fnyq1DvBDaEWY6da49m1rqonTB+am0LgI1TkN4TbiXYkFfA19G6TQ+pb+q4t6YJ5BkGo8+JdeJOA7wMSzIbY6ioD9SQyIZ2YdcyjfkdYs1VPehWdJF7urEuj2OP4XPxZDX7SFlIaRnbZ+XkyQaRLnN2DkJ99ZNVFvyCytdE2Jvk8Z95bNHSaQouxJkDQZjR6DOUijmxVE4xlDVRjOSvC3ZwnFOm/uo0m9gz+mVQAi3YklT6JNPrN5owWr4SMZC398crOm1nfeXU3ou5jsgsCkpKCKmd73KC0Plzd14F/cyV1ktuRbXYduDe8VScQahzajp8NmGXG0mlzSprIbE10hpPT5HnSV/bAe5kc/MylQt+1a7VMsUwqyDTRvWmHQNZLYZKI9TExG5S+ogGqtmWtmeZ5hkMqx9ppKkK/HpP0Xf40fiex6Nw07YTAphFPkm8rf/I7zY78KXTKfGzTiQY5ddRp7pQr1Qv4Uqch7E3v6UvHWQmQANiTGzR6wOf4SijWpi9LaGlDNdvx96ASOgoqrEjCIMgLhyw79ZBxRf3h2GWLWG3DfouiGmul1NmiCYxTy+C8lseGPajHSQXtHDNphOzRIWXSv6GGI5TTFUMYDdKilXQiWI4WSseoBh3SHPjQ06lmqmpwdgHCo9WWnJnKjfsqZLT5RpFssNAQT5Nd2Sw1nDqVdHauVP8DyMJi1EUhxyFkKrzvziVAUymwXWhKF8qarEf5sCeSdo1kPJtY66D8w8UVOFF2I8EOXI6PczNvV9lUKZIH5i3GQ+3RAGAz0ye6YSWF5Ndw7zbfTELjh/WaGSQxUS7pNKpwlkOF0A+AWY3hxXwJxmd63L2ILWHVdi8BBTY5HGE4cMswGpz97B3PCiVDgrVO6yDjShmYCvhpTy9X+LAp+HVlYidILTRa7+FTdB6eOFx5uYL1P+nUxhz7ZDqT0W/8+3i5jwF7Ea+0xjJyLke6/ylcV9G++fN3qPwiZO726M2fjwH7sMQ/hsw9One68veEZ/v5FLZfBc29Re1LAjxD9mKRl2FzP6fZ77PM/Zxmv88y93Oa/T7L3DNiO6cdCycXOrbPIj0HjsKV8oXIZoVXZ7WDFi0qbk7StMVOc8WxE/DTu06HmykH+G4WiG0dicgJnXNBGKfAZi0ggjoaC4aOHrmERqQX9itgiLuqAzUya2P+2XMNJAGni7qaH5GhZf9oK9rQTXtZYEPcWixoltqiaSYEHY2THXdOkComIzAfVqDTV46ewMn+QEVXoVk1E3G7jVELwbDfs49jCdHjaEL40eTJMbwQEPbPHAY7ZLuUA6wM8iW2Ih1mZVe2IfFdyubk3BFFqRwPZvAq5Lzmcmi8TmAWmqBC0DESBcTj42AZNwHzJnTpBxuWb7SlPr/KDtSVuNw6nTrUhaChg3G8Re+ilKBq3zhdn96m3xwhad+mITfqkfNaRN0Nmq9oTKFN6Q7C3ymOkpo1bmSL5D3QdNZspAB44eAspBzSyv0PVyvGLfHdfAoAAAGDaUNDUElDQyBwcm9maWxlAAB4nH2RPUjDUBSFT1NFKRWHdhBxCFKdLIiKOGoVilAh1AqtOpi89A+aNCQpLo6Ca8HBn8Wqg4uzrg6ugiD4A+Ls4KToIiXelxRaxHjh8T7Oe+dw332A0KgwzeoaBzTdNtPJhJjNrYo9rwghAl7DMrOMOUlKwbe+7hHg+12cZ/nf+7P61LzFgIBIPMsM0ybeIJ7etA3O+8RRVpJV4nPiMZMaJH7kuuLxG+eiywLPjJqZ9DxxlFgsdrDSwaxkasRTxDFV0ylfyHqsct7irFVqrNUnf2E4r68sc53WEJJYxBIkiFBQQxkV2IjTrpNiIU3nCR//oOuXyKWQqwxGjgVUoUF2/eB/8Hu2VmFywksKJ4DuF8f5GAF6doFm3XG+jx2neQIEn4Erve2vNoCZT9LrbS12BPRvAxfXbU3ZAy53gIEnQzZlVwrSEgoF4P2MvikHRG6B0Jo3t9Y5Th+ADM0qdQMcHAKjRcpe93l3b+fc/r3Tmt8PEcBygLuxJicAAA0aaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA0LjQuMC1FeGl2MiI+CiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgeG1wTU06RG9jdW1lbnRJRD0iZ2ltcDpkb2NpZDpnaW1wOjUzMTVjNzYzLWU0MDUtNDcxNi1hNmRjLTU5ZTk1ZmZjMTZkMSIKICAgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3ZTc2MmI2Zi0zNjM0LTRlZDAtOTE1OC03YmQ1OWIyYTllMzAiCiAgIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkYjYyYWI1Yi1lMTA4LTQ3N2QtODA3Yy00ODc3ZjRjMWI2NmMiCiAgIGRjOkZvcm1hdD0iaW1hZ2UvcG5nIgogICBHSU1QOkFQST0iMi4wIgogICBHSU1QOlBsYXRmb3JtPSJMaW51eCIKICAgR0lNUDpUaW1lU3RhbXA9IjE3MjkyOTYxNzUzMzEyMTgiCiAgIEdJTVA6VmVyc2lvbj0iMi4xMC4zMCIKICAgdGlmZjpPcmllbnRhdGlvbj0iMSIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJzYXZlZCIKICAgICAgc3RFdnQ6Y2hhbmdlZD0iLyIKICAgICAgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDowOWYyYTBkYi04YzBhLTRjYTUtOWQ0NS0yNmY1ODM3NDEyZWMiCiAgICAgIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkdpbXAgMi4xMCAoTGludXgpIgogICAgICBzdEV2dDp3aGVuPSIyMDI0LTEwLTE5VDAyOjAyOjU1KzAyOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAKPD94cGFja2V0IGVuZD0idyI/Ps0ZiVoAAAAGYktHRAC8ABoAJ5jPzacAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfoChMAAje0VBQSAAAPzUlEQVR42u3deXCUdZ4G8Oftt7vTnas7HcjViUkg0EpIR4KBcIVgrbPoUFOxFCojBgalmBldtVyhZMdhKBapioPjrE6NWhTHShaHxZktdJxdxxlXCOFajmwaIyREyGEHEkinSei73+79AxKJ3ek0biRvO8/nnyRvv9cvUHnq+76/Qwh+9HwQRERE40zBXwEREcmBcvAbYfGvBf46iIjoTht8UscKiYiIZIGBREREDCQiIiIGEhERMZCIiIgYSEREJEtK/gqIiORr0+Yac6OlaYnL7V4oimKRw+HKtNn7EQze2TkNBEGAQZ+MhATtJUmSzmg1moPF5sIPN25Ybxmzawz2/+Y4JCIieairPybsrn23stPa9WJPj222FAjI8j5FhQJpaYbjOcasV1ZUP7a/fH7ZN0rJoRxiIBERyaoiMjVamna2Xuyce+v2FH0SFi0og9GYBb0uGYLizr5xCQYCsF/rh9XahU8PHUOffWDY5wX5OUeKzYVPbNywvpmBREQU4555bt3SE6fP7HC63EkAoIlTY/WqKlSUL0BeXi5UKhW8Xh8kSRqfikgUoVar4PP50NbWjgN1h7B91164PV4AQLxWM1BaUvTkb17f+h4DiYgoRr2w7qVnDx879brT5QYAVFdVYuWK5TCkGtDSch4nTjbgyNFTaG5th88/PoGkUoowFeRi7pyZKL1vBqZOnQJbrw3v7N6D2r37B0MJ88pmPverrVveYCAREcVmZbTP6XJDKYp4tWYDKhaWo+nzc9i2vRanLc2yvO8SswlrVlejcNrdOHCwDmvXb4ZfkhCv1aC0pGhZtJUS57IjIpKBTZtrTDcf00Epitj25lbMnz8PtXv24SfP/ly2YQQApy3N+MmzP0ftnn2YP38etr25FUpRhNPlxonTZ3Zs2lxjup3zMZCIiMZJXf0xodHStHPwndGrNRtgNhfht29tx7Zd+2KmHdt27cNv39oOs7kIr9ZsAAA4Xe6kRkvTzrr6Y1E/fWMgERGNk92171YO9qarrqpExcJy/G7vH/D7/X+Jubb8fv9f8Lu9f0DFwnJUV1UCAFovds7dXftuJQOJiEjmOq1dLwI3etOtXLH8xjujMJXRhAQlnl6cjY3LJmNFeTpU4p195Z+pVaJ0ghb36OKgUggRK6Wmz89h5Yrl0MSph7WRgUREJFObNteYe3psswFg9aoqGFIN2La9NmQ/rUqBXzxuwoLidNyTq8eDs4z4h8U5o55fqRDw7CIjtlUX4q3l9+DHczOhuM0cK0yJw0tzc/BSeR5WlhjxVGkOXp6fi8enGKAZ4WTbttfCkGrA6lVVAICeHtvsTZtrzAwkIiKZarQ0LRmcgaGifAFaWs6H7cBQUahDeop22LZ7CwxQKyOny8qydJRMMkCtVECjEjG7IBUPTzdEfX+laVqsmZWDLJ0Gt14pXiViVnYKnp+RBW2YSu20pRktLedRUb4AACAFAmi0NC1hIBERyZTL7V4I3JiBIS8vFydONoTdLz5ODNmmFhVIS1JFPP/UzKSQbaaMpKjuTSsKeNScCZU4GBGhwZORGIdH8lPCHn/iZAPy8nKRok8a1lYGEhGRDImiWAQAixaUQaVS4cjRU2H3u+4OHQQbBDAxKfLc2J4wg2d18aqo7u3v8pKRrFHevBLCfL2RUYUTk6AUQsPqyNFTUKlUWLSgbFhbGUhERDLkcLgyAcBozILX60Nza3vY/Zye8LMy6LSRA6nf6QvZlqiJboGHrGTN1yojIbRSCt54fGcMcx/Nre3wen0wGrOGtZWBREQkQzZ7PwBAr0uGJEkjTgfUcdUd7okZ0nTqiOe/OuAN2aZVi5icoh69ehvqsBAMU5sNpwnzHsnnlyBJEvS65GFtZSAREcnQ4HpGo83abbX54POHLj8xQRcX8bgLPc6w281ZiaPeW/eAJ7QiCvOzFAzikss/4nkG2xbt2k0MJCIiGfMHgnCE+aOfYdBGPO5/rQ5IUmgQTM9OHvWadR398EkBRHyHBKDN7kS/f+zWamIgERHJXHefK2Rbul4T8Zh+TwBdfaFVUk5qPHKTI3duuOKWcPiiLUxl9FWF5JECeP8L25i2k4FERCRzXVdDgyVRq8KUiZEf2539ciBkmygIeHBa6qjX/PdzNtRd6IV/WJV14/tepxc7G7vQHqbjxP+Hkv/URETy1tx5HRX3Dt8mCMAckx7nr3SPeNzHZ/uwaHpaSNfs4rv0yGnqRedA5EDZe86Gv168hnnGJKRolAgEgmizu1Hf7UAgOPbtZIVERCRzR1r64fGFvqsxjfI+6KrTjxZraA83lVKBqpkZUV37qkfC+xfs+NfPr2L3uV7UXf52woiBREQUAzz+IDq7r4dsv2tiAnJH6ca9v6EHUpgEMWUk4QcmvazayUAiIooBZ9tDKx2FQsD3iiO/Dzrf64Gl3R72s8XT03FvmpaBRERE0fuo4erNrtjDzZhsgHKUabxrj1+GwxPadVylVGBlWTZyk1SyaCMDiYgoBvQ6JFzoCu01l5ygwvfNKRGP7XNLeP9UV9h3PwlxSjy3MBfTUzUMJCIiis6Bxitht1cUpUEUIldJf23tx/+09ob9LFGjxI/n3YUlBToGEhERje7Ts9fCjkmaqNPgkZmjjy3afvwyWi4NhP1MrVRgyfQMvDDHiLsSx+cRHgOJiCiGfNIwfNzRYF10vzkdExMiDy0NBIF/OdiJ9iuOsJ8LAKakJWLtwjysuTcNuXc4mBhIREQx5D8b+9DR81UX8MHXQgkaJZ6syB71eI8UxCufdODcpZFn4FYpBMzI1mPdgjz805xsPFqQgmn6OAjfcts4UwMRUYx575AVz1dOhXCzqhkMpWl36bCkKAUfnumLeLw3EMRrdV/iRyVpmJ1vwEiLTSgEIFuvRXayBhWTUuHxBeDw+OHw+NF81YEPOq6N6SBZVkhERDHmVLsDp8/3hoSIAKBylhHTM0YfWxQIAjtP9eDfjn+J/puziYdWQMMnVVWLCqRoVTAma7EoLxWPF6SOabsYSEREMWjbJ1/CNrRu0VfUSgVW35+LjMToHoAd6ryOTR9fQEOHHf7gSAvyhV9+YtrERIzlLEIMJCKiGHTdE8C7BzoQCPPMzJCoxj8+mA+9RozqXAO+AN4+eRm//O+LaLRegzdkAG6YJcwBBIJjO6kdA4mIKEYdvXAdnzRcDtkeBJCu02Lt4jzo4qL/M98+4MNbJy/jpY8v4L/O9qDN5oTXHwhfIQmApXtgTDs6sFMDEVEM2324GzkT4mEyJg+rZ4IAslPj8bPvT8Jrf25Dt8MfffXlD+CDL+z44As71AoBU3VqTNJpkKJRIlGthALA+V4n/mwdGNO2MJCIiGJYEMDrH7Vjw8MFyEzRfr2OQbpOixcfnIQdBzvQdMV92+f3BoL4rM+Dz/o833pb+MiOiCjGDXgCePWPF9Db7xmqkG6VkqjGMw/kY/FUnazbwUAiIvoO6HH48dqfvsDVfk/Ynm9qpYils7KxdlEOJmhFWbaBgURE9B3RYfdhy/ut6Op1hv1cEIB7spLxi4cKsHR66qjLVjCQiIjoG+t1Stjyxws4++W1EfeJjxPx99PTseWhSfiBSQ+VTIKJgURE9B1z3RtAzUft+MTSDSnCWCFDghpLzJmoeWgyVhZPRH7y+C7Ux152RETfUbUnetDa7cDSWUbo49Uj7pekUWLu5FSU5Rtw2e7GF1ccaLg0gM/7PBjjsa8MJCKiv1VHOxxovNSKH83OwIxcPRQRhrIqAGTqNMhIisO8fANcXgk9/R70XPeg1+FFr9MPp0+C3SvB6vTDExjbtGIgERGNA0EQEAwGEQwEvvVrOX0BvFnfhcJmGx6ekY68CQlh9/t6vGiUInJStMjRaW/eK4CbX/1SAJ/3DGDHuasjzmc32DZBiO4dFd8hERGNA4P+xswK9mv9EEURKuW33xW76YobL3/cjl31HeiwOaN4HDfy5KqiQkBRWjIeumWGiEEqpQhRFGG/1j+srQwkIiIZSkjQXgIAq7ULarUKpoLcO3btwx3X8c8ft+HtujacvdR/c746RHiYF2Zy1ZvZZEyKC9nbVJALtVoFq7VrWFtHw0d2RETjQJKkMwAyPz10DD/z+TB3zkx8du7CHb2H05ddOH3ZikSVAvfnJcOUlgijToM4UTFKxYShCfN6Xb6QvebOmQmfz4dPDx27ta0MJCIiOdJqNAcBfK/PPoC2tnaU3jcD23a9Ny73ct0XwAfn7cB5O0RBQEmaBgUGLTIS45AaH4cElXhzrNIt69MGAWu/C3/qDB3vVHrfDLS1taPPPnBrWxlIRERyVGwu/PBiu3WLFAjgQN0hPLFqJUrMJpy2NI9v5RYM4kS3Cye6XUPbFAKgV4swapWIExWIEwW4/UE02Fz4epeMErMJU6dOwc5d7wAARIUCxebCD/8jiqzlOyQionGwccN6S1qa4TgAbN+1F7ZeG9asrpblvQaCgM0j4Yzdg5O9LhzuceJUmDACgDWrq2HrtWH7rr0AgLQ0w/GNG9ZborkOA4mIaJzkGLNeAQC3x4t3du9B4bS7sWbVsphtz5pVy1A47W68s3sP3B7vsDYykIiIZGxF9WP7C/JzjgBA7d79OHCwDj+segSPVj4Qc215tPIB/LDqERw4WIfavfsBAAX5OUdWVD+2n4FERCRz5fPLgsXmwifitZoBAFi7fjMsljN4+qerY6pSWrNqGZ7+6WpYLGewdv1mAEC8VjNQbC58onx+WdTTOTCQiIjG0cYN65tLS4qejNdq4JckrHlqHerrD6N6+TK8/cbLKDGbZHvvJWYT3n7jZVQvX4b6+sNY89Q6+CUJ8VoNSkuKnty4Yf1t9dAQgh89HwQAYfGvBf7XICIaHy+se+nZw8dOve503VhmvLqqEitXLIch1YCWlvM4cbIBR46eQnNrO3x+aVzuUaUUYSrIxdw5M1F63wxMnToFtl4b3tm9Z+gxXbxWg3llM5/71dYtb0R73qEcYiAREcnDM8+tW3ri9JkdTpc7CQA0cWqsXlWFivIFyMvLhUqlgtfrgySNTyCJogi1WgWfz4e2tnYcqDuE7bv2DnVgiNdqBkpLip78zetbb2tAFQOJiEiGNm2uMTVamna2Xuyce+v2FH0SFi0og9GYBb0uGYLizr5xCQYCsF/rh9XahU8PHRsa9DqoID/nSLG58InbfUzHQCIikrG6+mPC7tp3KzutXS/29NhmS3dgRvBvVDEpFEhLMxzPMWa9sqL6sf2304GBgUREFHsVk7nR0rTE5XYvFEWxyOFwZdrs/QjeyZXzcGMJCYM+GQkJ2kuSJJ3RajQHi82FH0Y76JWBREREsjeYQ+z2TUREssBAIiIiBhIREREDiYiIGEhEREQMJCIikqWhbt9ERESskIiI6G/e/wECJa/2LPDUiwAAAABJRU5ErkJggg==";
PARAM["WIDGET_IMG"]="$var_img";
PARAM["WIDGET_AUTOSTART"]="[Desktop Entry]\nName=distro-widget\nGenericName=distro-widget\nComment=distro-widget\nType=Application\nExec=conky --daemonize\nNoDisplay=false\nHidden=false\nX-GNOME-Autostart-enabled=true\nX-GNOME-Autostart-Delay=0";
PARAM["WIDGET_CONFIGURATION"]=$(cat <<EOF
conky.config = {
	maximum_width = 420,
	minimum_width = 420,
	minimum_height = 420,
	alignment = 'top_right',
	background = true,
	own_window = true,
	own_window_class = 'Conky',
	own_window_transparent = false,
	own_window_type = 'desktop',
	own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
	own_window_argb_visual = true,
	own_window_argb_value = 200,
	own_window_colour = '000000',
	double_buffer = true,
	no_buffers = true,
	use_spacer = 'none',
	use_xft = true,
	xftalpha = 1,
	font = 'Sans:size=10',
	update_interval = 10,
	uppercase = false,
	override_utf8_locale = true,
	stippled_borders = 0,
	border_width = 0,
	draw_borders = false,
	draw_graph_borders = true,
	draw_outline = false,
	draw_shades = false,
	show_graph_scale = true,
	show_graph_range = true,
	gap_x = 22,
	gap_y = 51,
	net_avg_samples = 2,
	cpu_avg_samples = 2,
	short_units = true,
	pad_percents = 2,
	text_buffer_size = 2048,
	out_to_console = false,
	out_to_stderr = false,
	extra_newline = false,
	color1 = '#C0C0C0',
	color2 = '#FFFFFF',
	color3 = '#FFB871'
}
conky.text = [[
\${image ~/.config/conky/distro-tracker.png -s 420x78}
\${voffset -26}
\${offset 16}\${color1}OS : \${font Sans:size=10:style=Bold}\${color2}\${exec lsb_release -d | sed -e 's/.*: //' | awk '{print \$2,\$3,\$4}'}\${font}\${color}
\${offset 16}\${color1}Kernel : \${color2}\$kernel\${color}
\${offset 16}\${color1}System : \${color2}\${exec cat /sys/class/dmi/id/product_name}\${color}
\${voffset 10}
\${offset 16}\${color1}Uptime : \${color2}\$uptime\${alignr 16}\${color1}Hostname : \${font Sans:size=10:style=Bold}\${color2}\${nodename}\${font}\${color}
\${hr 1}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}C P U  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}model :\${alignr 10}\${execi 1000 cat /proc/cpuinfo | grep 'model name' | sed -e 's/model name.*: //' | sed -e 's/ @.*//' | uniq}
\${offset 6}frequency :\${alignr 10}\${freq_g}GHz
\${offset 6}temperature :\${alignr 10}\${texeci 30 sensors | grep "Package id 0" | cut -d ':' -f 2 | cut -d '(' -f 1 | tr -d ' '}
\${offset 6}running :\${alignr 20}\$running_processes
\${offset 6}load :\${alignr 10}\${cpu}%
\${voffset -10}
\${offset 6}\${cpubar cpu0 16,407}
\${voffset -10}
\${offset 6}processes \${alignr}\${offset -60}PID \${alignr}\${offset -30}CPU% \${alignr}\${offset -10}mem%
\${offset 6}\${top name 1}\${alignr}\${offset -83}\${top pid 1}\${alignr}\${offset -45}\${top cpu 1}\${alignr}\${offset -10}\${top mem 1}
\${offset 6}\${top name 2}\${alignr}\${offset -83}\${top pid 2}\${alignr}\${offset -45}\${top cpu 2}\${alignr}\${offset -10}\${top mem 2}
\${offset 6}\${top name 3}\${alignr}\${offset -83}\${top pid 3}\${alignr}\${offset -45}\${top cpu 3}\${alignr}\${offset -10}\${top mem 3}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}M E M O R Y  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}RAM : \$memperc%\${alignr}\${offset -6}\$mem / \$memmax
\${offset 6}\${membar 16,407}
\${voffset -6}
\${offset 6}SWAP : \$swapperc%\${alignr}\${offset -6}\$swap / \$swapmax
\${offset 6}\${swapbar 16,407}
\${voffset -6}
\${offset 6}DISK : \$fs_used_perc%\${alignr}\${offset -6}\$fs_used / \$fs_size
\${offset 6}\${fs_bar 16,407}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}N E T W O R K  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}Default Gateway :\${alignr 10}\${gw_ip}
\${offset 6}Internal IP Address :\${alignr 10}\${addrs ${PARAM[WIDGET_IP]}}
\${offset 6}Upload / Download Total :\${alignr 10}\${totalup ${PARAM[WIDGET_IP]}} / \${totaldown ${PARAM[WIDGET_IP]}}
\${offset 6}Upload / Download Speed :\${alignr 10}\${upspeed ${PARAM[WIDGET_IP]}} / \${downspeed ${PARAM[WIDGET_IP]}}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}T R A C K  &  R E P O R T  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}\${font Sans:size=10:style=Bold}\${color2}random check : \${color}\${font}\${color1}\${exec bash ${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh -s ${var_file_tracker} -t tracker -c main}\${color}
\${offset 6}\${font Sans:size=10:style=Bold}\${color2}last report : \${color}\${font}\${color1}\${exec bash ${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh -s ${var_file_reporter} -t reporter -c main | fold -s -w 46 | sed '1!s/^/  /'} | \${exec bash ${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh -s ${var_file_reporter} -t reporter -c revision | xargs -I{} date -d {} '+%d/%m - %Hh%M'}\${color}
\${offset 6}\${font Sans:size=10:style=Bold}\${color2}more on website : \${color}\${font}\${color1}https://distrotracker.com | conf. v${APP[VERSION]}\${color}
]]
EOF
);
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Install_LocalConky.
#	DESCRIPTION : add Conky configuration.
function Install_LocalConky() {
	# Some definition - menu.
	local var_path;
	local var_user;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			-USER) var_user="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Test : widget autostart.
	if Test_File -PATH "${var_path}${CONF[SEPARATOR]}autostart${CONF[SEPARATOR]}conky.desktop" -CREATE -OVERWRITE -CHOWN "${var_user}:${var_user}" -CHMOD "755"; then
		Set_File -PATH "${var_path}${CONF[SEPARATOR]}autostart${CONF[SEPARATOR]}conky.desktop" -RAW "${PARAM[WIDGET_AUTOSTART]}" -INTERPRET;
		# Test : widget configuration.
		if Test_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}conky.conf" -CREATE -OVERWRITE -CHOWN "${var_user}:${var_user}" -CHMOD "755"; then
			Set_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}conky.conf" -RAW "${PARAM[WIDGET_CONFIGURATION]}";
			# Test : widget img header.
			if Test_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-tracker.png" -CREATE -OVERWRITE -CHOWN "${var_user}:${var_user}" -CHMOD "755"; then
				Set_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-tracker.png" -RAW "${PARAM[WIDGET_IMG]}" -ENCODE "base64";
				# Test : widget reader home script.
				if Test_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh" -CREATE -OVERWRITE -EXEC -CHOWN "${var_user}:${var_user}" -CHMOD "755" && Get_Distant -URL "${DEFAULT[SOURCE_READER]}" -TYPE "file" -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh"; then
					# Test : widget downloader bin.
					if Test_File -PATH "${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader" -CREATE -OVERWRITE -EXEC && Get_Distant -URL "${DEFAULT[SOURCE_DOWNLOADER]}" -TYPE "file" -PATH "${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader"; then
						return 0;
					else
						Write_Script -TEXT "distant file (downloader) can't be downloaded.";
						return 1;
					fi;
				else
					Write_Script -TEXT "distant file (reader) can't be downloaded.";
					return 1;
				fi;
			else
				Write_Script -TEXT"autostart file can't be writed.";
				return 1;
			fi;
		else
			Write_Script -TEXT "configuration file can't be writed.";
			return 1;
		fi;
	else
		Write_Script -TEXT "image file can't be writed.";
		return 1;
	fi;
}

# Install_LocalTask.
#	DESCRIPTION : add systemd configuration.
function Install_LocalTask() {
	# Some definition - menu.
	local var_path;
	# Some definition - next.
	local var_tracker_service;
	local var_tracker_timer;
	local var_reporter_service;
	local var_reporter_timer;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Some init.
	var_tracker_service="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-tracker.service";
	var_tracker_timer="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-tracker.timer";
	var_reporter_service="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-reporter.service";
	var_reporter_timer="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-reporter.timer";
	# Test : data exists.
	if Test_File -PATH "$var_tracker_service" -CREATE -OVERWRITE && \
		Test_File -PATH "$var_tracker_timer" -CREATE -OVERWRITE && \
		Test_File -PATH "$var_reporter_service" -CREATE -OVERWRITE && \
		Test_File -PATH "$var_reporter_timer" -CREATE -OVERWRITE; then
		# tracker service.
cat <<EOF > "$var_tracker_service"
[Unit]
Description=Download Distro Tracker Data

[Service]
Type=oneshot
ExecStart=${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader --format json --type tracker --output ${var_path}${CONF[SEPARATOR]}distro-tracker.json
StandardOutput=append:${LOG[TRACKER]}
StandardError=append:${LOG[TRACKER]}

[Install]
WantedBy=multi-user.target
EOF
		# tracker timer.
cat <<EOF > "$var_tracker_timer"
[Unit]
Description=Run Distro Tracker Daily at 12:30 PM

[Timer]
OnCalendar=*-*-* 12:30:00
Persistent=true

[Install]
WantedBy=timers.target
EOF
		# reporter service.
cat <<EOF > "$var_reporter_service"
[Unit]
Description=Download Distro Reporter Data

[Service]
Type=oneshot
ExecStart=${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader --format json --type reporter --output ${var_path}${CONF[SEPARATOR]}distro-reporter.json
StandardOutput=append:${LOG[REPORTER]}
StandardError=append:${LOG[REPORTER]}

[Install]
WantedBy=multi-user.target
EOF
		# reporter timer.
cat <<EOF > "$var_reporter_timer"
[Unit]
Description=Run Distro Reporter Daily at 13 PM

[Timer]
OnCalendar=*-*-* 13:00:00
Persistent=true

[Install]
WantedBy=timers.target
EOF
		# Test : service exists.
		if Start_Software -NAME "distro-tracker.service" -ENABLE && \
		Start_Software -NAME "distro-tracker.timer" -ENABLE && \
		Start_Software -NAME "distro-reporter.service" -ENABLE && \
		Start_Software -NAME "distro-reporter.timer" -ENABLE; then
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Test : rights.
	if Test_Root; then
		# Test : install.
		if Install_LocalConky -PATH "${PARAM[PATH]}" -USER "${PARAM[USERNAME]}"; then
			Write_Script -TEXT "Conky configuration installed with '$var_file_tracker' tracker data & '$var_file_reporter' reporter data.";
			# Test : conky restarting.
			if Start_Software -COMMAND "conky -d" -USER "${PARAM[USERNAME]}" -GUI; then
				Write_Script -TEXT "Conky launched.";
				Write_Script -TEXT "installing systemd services & timers ...";
				# Test : systemd services & timers.
				if Install_LocalTask -PATH "${PARAM[PATH]}${CONF[SEPARATOR]}conky"; then
					Write_Script -TEXT "systemd tasks installed.";
				else
					Write_Script -TEXT "systemd tasks not installed.";
				fi;
			else
				Write_Script -TEXT "Conky not launched.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "Conky configuration not installed.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Root rights needed.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;
