#!/bin/bash
# shellcheck source=/dev/null
#  ______________________________________________
# |                                              | #
# |               distro-widget.sh               | #
# |______________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-widget";
APP["SECTION"]="1";
APP["SHORTNAME"]="DW";
APP["SLOGAN"]="Set a fancy widget with Conky & systemd jobs.";
APP["DESCRIPTION"]="This open-source tool generates some stats with Distro Tracker datas integration.";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["REQUIREMENTS"]="program conky mandatory";
APP["EXAMPLES"]="install for user 'test' with two formats for sources :|${APP[NAME]} -u test --source-tracker https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv --source-reporter https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json";
APP["TODO"]="null";
APP["NOTES"]="Compatible Distro Tracker >= 1.2.0.";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.1.0|2025-01-01T10:00:00+0200|Add systemd jobs. Replace distro-get with standalone generics distro-downloader & distro-reader.#1.0.0|2024-10-20T10:00:00+0200|Distro Tracker datas integration.#0.2.0|2024-01-03T10:00:00+0200|USER parameter instead PATH.#0.1.0|2024-01-02T10:00:00+0200|Style unified with Windows version.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["VERSION"]="1.1.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE_TRACKER"]="1#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Tracker data.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv'.";
OPTIONS["SOURCE_REPORTER"]="2#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Reporter data.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json'.";
OPTIONS["SOURCE_DOWNLOADER"]="3#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Downloader.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-downloader/dev/bin/distro-downloader'.";
OPTIONS["SOURCE_READER"]="4#null#FILEPATH#optionnal#Single local file path or single distant url to Distro Reader.|path or url.|'https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reader/dev/standalone/distro-reader.sh'.";
OPTIONS["USERNAME"]="5#u#NAME#mandatory#Local username for configuration.|no default.";
OPTIONS["QUIET"]="6#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="7#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="8#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source-tracker)
			PARAM["SOURCE_TRACKER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--source-reporter)
			PARAM["SOURCE_REPORTER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--source-downloader)
			PARAM["SOURCE_DOWNLOADER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--source-reader)
			PARAM["SOURCE_READER"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--username|-u)
			PARAM["USERNAME"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			echo "${APP[VERSION]}"
			shift
			Stop_App -CODE 0
			;;
		*)
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '$1'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ===================== #
#          LOG          #
#
#	DESCRIPTION : everything that help debugging the APP.

declare -gA LOG;
LOG["PATH"]="${CONF[SEPARATOR]}var${CONF[SEPARATOR]}log${CONF[SEPARATOR]}distro";
LOG["MAIN"]="${LOG[PATH]}${CONF[SEPARATOR]}widget.log";
LOG["TRACKER"]="${LOG[PATH]}${CONF[SEPARATOR]}tracker.log";
LOG["REPORTER"]="${LOG[PATH]}${CONF[SEPARATOR]}reporter.log";


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["SOURCE_TRACKER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.csv";
DEFAULT["SOURCE_REPORTER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json";
DEFAULT["SOURCE_DOWNLOADER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-downloader/dev/bin/distro-downloader";
DEFAULT["SOURCE_READER"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reader/dev/standalone/distro-reader.sh";
DEFAULT["QUIET"]=false;
# Some script definition.
# PATH (auto-defined).
PARAM["PATH"]="${CONF[SEPARATOR]}home${CONF[SEPARATOR]}${PARAM[USERNAME]}${CONF[SEPARATOR]}.config";
if ! Test_Dir -PATH "${PARAM["PATH"]}${CONF[SEPARATOR]}conky" -CREATE; then
	Stop_App -CODE 1 -TEXT "Conky configuration folder can't be created.";
fi;
# SOURCEs.
if [ -z "${PARAM[SOURCE_TRACKER]}" ]; then PARAM["SOURCE_TRACKER"]="${DEFAULT[SOURCE_TRACKER]}"; fi;
if [ -z "${PARAM[SOURCE_REPORTER]}" ]; then PARAM["SOURCE_REPORTER"]="${DEFAULT[SOURCE_REPORTER]}"; fi;
if [ -z "${PARAM[SOURCE_DOWNLOADER]}" ]; then PARAM["SOURCE_DOWNLOADER"]="${DEFAULT[SOURCE_DOWNLOADER]}"; fi;
if [ -z "${PARAM[SOURCE_READER]}" ]; then PARAM["SOURCE_READER"]="${DEFAULT[SOURCE_READER]}"; fi;
if ! [[ "${PARAM[SOURCE_TRACKER]}" =~ \.(csv|json|yaml)$ ]]; then
	Stop_App -CODE 1 -TEXT "Invalid format for '--source-tracker' option.";
fi;
if ! [[ "${PARAM[SOURCE_REPORTER]}" =~ \.(json|yaml)$ ]]; then
	Stop_App -CODE 1 -TEXT "Invalid format for '--source-reporter' option.";
fi;
var_file_tracker_extension=$(Get_FileExtension -PATH "${PARAM[SOURCE_TRACKER]}");
var_file_reporter_extension=$(Get_FileExtension -PATH "${PARAM[SOURCE_REPORTER]}");
# Tests : redefine requirements for user-friendly work.
if [[ "$var_file_tracker_extension" == "json" || "$var_file_reporter_extension" == "json" ]]; then
	APP["REQUIREMENTS"]+="#program jq mandatory";
elif [[ "$var_file_tracker_extension" == "yaml" || "$var_file_reporter_extension" == "yaml" ]]; then
	APP["REQUIREMENTS"]+="#program yq mandatory";
fi;
# SOURCE TRACKER.
# Tests : distant file, local file.
# The program downloads remote content for the first time or just makes sure that local content is not empty.
if [[ "${PARAM[SOURCE_TRACKER]}" =~ ^(http|www) ]]; then
	var_file_tracker="${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-tracker.${var_file_tracker_extension}";
	if Get_Distant -URL "${PARAM[SOURCE_TRACKER]}" -TYPE "file" -PATH "$var_file_tracker"; then
		if Test_File -PATH "$var_file_tracker"; then
			var_data_tracker="$(<"$var_file_tracker")";
			# Test : data exists.
			if [[ "$var_data_tracker" == "" ]]; then
				Stop_App -CODE 1 -TEXT "Distant file in '--source-tracker' option has no data.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "Distant file in '--source-tracker' option not available.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Distant file in '--source-tracker' option not reachable.";
	fi;
else
	if Test_File -PATH "${PARAM[SOURCE_TRACKER]}"; then
		var_data_tracker="$(<"${PARAM[SOURCE_TRACKER]}")";
		# Test : data exists.
		if [[ "$var_data_tracker" == "" ]]; then
			Stop_App -CODE 1 -TEXT "Local file in '--source-tracker' option has no data.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Local file in '--source-tracker' option is not available.";
	fi;
fi;
# SOURCE REPORTER.
# Tests : distant file, local file.
if [[ "${PARAM[SOURCE_REPORTER]}" =~ ^(http|www) ]]; then
	var_file_reporter="${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reporter.${var_file_reporter_extension}";
	if Get_Distant -URL "${PARAM[SOURCE_REPORTER]}" -TYPE "file" -PATH "$var_file_reporter"; then
		if Test_File -PATH "$var_file_reporter"; then
			var_data_reporter="$(<"$var_file_reporter")";
			# Test : data exists.
			if [[ "$var_data_reporter" == "" ]]; then
				Stop_App -CODE 1 -TEXT "Distant file in '--source-reporter' option has no data.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "Distant file in '--source-reporter' option not available.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Distant file in '--source-reporter' option not reachable.";
	fi;
else
	if Test_File -PATH "${PARAM[SOURCE_REPORTER]}"; then
		var_data_reporter="$(<"${PARAM[SOURCE_REPORTER]}")";
		# Test : data exists.
		if [[ "$var_data_reporter" == "" ]]; then
			Stop_App -CODE 1 -TEXT "Local file in '--source-reporter' option has no data.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Local file in '--source-reporter' option is not available.";
	fi;
fi;
# WIDGETs (auto-defined).
PARAM["WIDGET_IP"]=$(Get_FirstNetwork);
var_img="iVBORw0KGgoAAAANSUhEUgAAAaQAAABOCAYAAABv5GJlAAAI2HpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjapZhpcis5DoT/8xRzBO4Aj8M1om8wx58PpZJXtduvRwqpylQRJJBAImm3//vXcf/hFaWoy0W0tlo9r9xyi50b9Y9Xv76Dz9f3Yyjfd+HzuMv7nhQZSlzT40+tj2t4jt8TntfQuSsfDOm8fxiff2j5tq9fDN0LJdtR5GbdhtptKMXHD+E20B9u+dpUProwbg/u+Y8w8HH2NZ6j5X74y99ZiN4qrJNi3Ckkz3dK9waSfbJL3X64vhsP2mC/v0t67oSAvIrT26uxo2NbzS8f+oTK290XtOK5Y/QVrRzvR9KXINe368txF8prVK7Qf1g5630XP4+LmmO2oy/Rt885S8/lM170XAl1vZ16unjd8dxgCVtaHVurXvgUTMj1bryVrJ6kwvLTD94ztBBB5YQcVujhhH1dZ5hsMcftonAT44zpGtQkscUJbiFle4cTJbW0koLkvGDPKb7tJVzLNj/dtZqy8go8GgPGAlP++O3+dMI5VgohWCyBPjzwjdGCzTYMOfvmMRAJ5w5quQL8fH99Ga4JBItF2UqkEdjxMDFKeGeCdAGdeLBwfdRgkHUbIEQsXdhMSCAAaiGVUIOXGCUEAqkA1Nk6BRQHCIRS4mKTMadUwUajLc0UCdejsUSGHeOQGUiUVJOATUsdsHIu5I9kJYd6SSWXUmqBCUsrvaaaa6m1SjVS7JIkOylSRUSlSdekWYtWFVVt2ltsCdIsrTZp2lrrnTU7ljuzOw/0PuJII4/iRh0ydLTRJ+kz8yyzTpk62+wrrrTgj1WXLF1t9R02qbTzLrtu2brb7odUO8mdfMqpR46edvobajes395/gFq4UYsXUvagvKHGqMjTRDA6KYYZgEWXA4iLQUBCR8PMa8g5GnKGmW+RqiiWe8UwW8EQA8G8QywnPLFz8YGoIfd/4eYkf8It/lvknEH3h8h9x+0Vasva0LwQe1ShBdUnqu9IIGXSKHVY9vdFkGYlfPgepjuhJAixrnS2rErZgt2pUQe3evyoNtJ3Hmd2fJlWPSd2eMeIShL71bSnS7Wdk/Yw3jwjZHlhlPmxj9VmGiHNE/eG72oY+eBFUTn7LKcdc5c1HLjs0UWxWNLC6kgq1W9wgEjP8KvLmafPdI7sWT6Ycw97BH9j8lrUS5pjNP9qb/rdWD65BFyDw8mcXdbU59Z+4aieKn5D8m9+OtsZPffp6run7+b+0Uu623bvjn4y90+efvPTPey9ufrC088g5O8GL1/dN2c/gfp7TN3PoP4eU/czqO+edpHgd1uVVI+FasylsqsUeqvSwnIlx65h5ZYKVCQsTO+Y+aQ6qeexxtCUoI4eViqzXxiNnnXXEcZGGJ+MUtDq6PWhz7366nlMMQaBZxoaLkNZNawFN7Whuo9S5msVyavFfWak1jetaZ8KIbi6O9O0pQl19K5z+I1kwNLqzIfR6uhrVrhNiC56R/zZa69E+NBM/QJiE6NCWUNeB0rK66p0VBgB406hCbv6fnyq1DvBDaEWY6da49m1rqonTB+am0LgI1TkN4TbiXYkFfA19G6TQ+pb+q4t6YJ5BkGo8+JdeJOA7wMSzIbY6ioD9SQyIZ2YdcyjfkdYs1VPehWdJF7urEuj2OP4XPxZDX7SFlIaRnbZ+XkyQaRLnN2DkJ99ZNVFvyCytdE2Jvk8Z95bNHSaQouxJkDQZjR6DOUijmxVE4xlDVRjOSvC3ZwnFOm/uo0m9gz+mVQAi3YklT6JNPrN5owWr4SMZC398crOm1nfeXU3ou5jsgsCkpKCKmd73KC0Plzd14F/cyV1ktuRbXYduDe8VScQahzajp8NmGXG0mlzSprIbE10hpPT5HnSV/bAe5kc/MylQt+1a7VMsUwqyDTRvWmHQNZLYZKI9TExG5S+ogGqtmWtmeZ5hkMqx9ppKkK/HpP0Xf40fiex6Nw07YTAphFPkm8rf/I7zY78KXTKfGzTiQY5ddRp7pQr1Qv4Uqch7E3v6UvHWQmQANiTGzR6wOf4SijWpi9LaGlDNdvx96ASOgoqrEjCIMgLhyw79ZBxRf3h2GWLWG3DfouiGmul1NmiCYxTy+C8lseGPajHSQXtHDNphOzRIWXSv6GGI5TTFUMYDdKilXQiWI4WSseoBh3SHPjQ06lmqmpwdgHCo9WWnJnKjfsqZLT5RpFssNAQT5Nd2Sw1nDqVdHauVP8DyMJi1EUhxyFkKrzvziVAUymwXWhKF8qarEf5sCeSdo1kPJtY66D8w8UVOFF2I8EOXI6PczNvV9lUKZIH5i3GQ+3RAGAz0ye6YSWF5Ndw7zbfTELjh/WaGSQxUS7pNKpwlkOF0A+AWY3hxXwJxmd63L2ILWHVdi8BBTY5HGE4cMswGpz97B3PCiVDgrVO6yDjShmYCvhpTy9X+LAp+HVlYidILTRa7+FTdB6eOFx5uYL1P+nUxhz7ZDqT0W/8+3i5jwF7Ea+0xjJyLke6/ylcV9G++fN3qPwiZO726M2fjwH7sMQ/hsw9One68veEZ/v5FLZfBc29Re1LAjxD9mKRl2FzP6fZ77PM/Zxmv88y93Oa/T7L3DNiO6cdCycXOrbPIj0HjsKV8oXIZoVXZ7WDFi0qbk7StMVOc8WxE/DTu06HmykH+G4WiG0dicgJnXNBGKfAZi0ggjoaC4aOHrmERqQX9itgiLuqAzUya2P+2XMNJAGni7qaH5GhZf9oK9rQTXtZYEPcWixoltqiaSYEHY2THXdOkComIzAfVqDTV46ewMn+QEVXoVk1E3G7jVELwbDfs49jCdHjaEL40eTJMbwQEPbPHAY7ZLuUA6wM8iW2Ih1mZVe2IfFdyubk3BFFqRwPZvAq5Lzmcmi8TmAWmqBC0DESBcTj42AZNwHzJnTpBxuWb7SlPr/KDtSVuNw6nTrUhaChg3G8Re+ilKBq3zhdn96m3xwhad+mITfqkfNaRN0Nmq9oTKFN6Q7C3ymOkpo1bmSL5D3QdNZspAB44eAspBzSyv0PVyvGLfHdfAoAAAGDaUNDUElDQyBwcm9maWxlAAB4nH2RPUjDUBSFT1NFKRWHdhBxCFKdLIiKOGoVilAh1AqtOpi89A+aNCQpLo6Ca8HBn8Wqg4uzrg6ugiD4A+Ls4KToIiXelxRaxHjh8T7Oe+dw332A0KgwzeoaBzTdNtPJhJjNrYo9rwghAl7DMrOMOUlKwbe+7hHg+12cZ/nf+7P61LzFgIBIPMsM0ybeIJ7etA3O+8RRVpJV4nPiMZMaJH7kuuLxG+eiywLPjJqZ9DxxlFgsdrDSwaxkasRTxDFV0ylfyHqsct7irFVqrNUnf2E4r68sc53WEJJYxBIkiFBQQxkV2IjTrpNiIU3nCR//oOuXyKWQqwxGjgVUoUF2/eB/8Hu2VmFywksKJ4DuF8f5GAF6doFm3XG+jx2neQIEn4Erve2vNoCZT9LrbS12BPRvAxfXbU3ZAy53gIEnQzZlVwrSEgoF4P2MvikHRG6B0Jo3t9Y5Th+ADM0qdQMcHAKjRcpe93l3b+fc/r3Tmt8PEcBygLuxJicAAA0aaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJYTVAgQ29yZSA0LjQuMC1FeGl2MiI+CiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iCiAgICB4bWxuczpHSU1QPSJodHRwOi8vd3d3LmdpbXAub3JnL3htcC8iCiAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgeG1wTU06RG9jdW1lbnRJRD0iZ2ltcDpkb2NpZDpnaW1wOjUzMTVjNzYzLWU0MDUtNDcxNi1hNmRjLTU5ZTk1ZmZjMTZkMSIKICAgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3ZTc2MmI2Zi0zNjM0LTRlZDAtOTE1OC03YmQ1OWIyYTllMzAiCiAgIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpkYjYyYWI1Yi1lMTA4LTQ3N2QtODA3Yy00ODc3ZjRjMWI2NmMiCiAgIGRjOkZvcm1hdD0iaW1hZ2UvcG5nIgogICBHSU1QOkFQST0iMi4wIgogICBHSU1QOlBsYXRmb3JtPSJMaW51eCIKICAgR0lNUDpUaW1lU3RhbXA9IjE3MjkyOTYxNzUzMzEyMTgiCiAgIEdJTVA6VmVyc2lvbj0iMi4xMC4zMCIKICAgdGlmZjpPcmllbnRhdGlvbj0iMSIKICAgeG1wOkNyZWF0b3JUb29sPSJHSU1QIDIuMTAiPgogICA8eG1wTU06SGlzdG9yeT4KICAgIDxyZGY6U2VxPgogICAgIDxyZGY6bGkKICAgICAgc3RFdnQ6YWN0aW9uPSJzYXZlZCIKICAgICAgc3RFdnQ6Y2hhbmdlZD0iLyIKICAgICAgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDowOWYyYTBkYi04YzBhLTRjYTUtOWQ0NS0yNmY1ODM3NDEyZWMiCiAgICAgIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkdpbXAgMi4xMCAoTGludXgpIgogICAgICBzdEV2dDp3aGVuPSIyMDI0LTEwLTE5VDAyOjAyOjU1KzAyOjAwIi8+CiAgICA8L3JkZjpTZXE+CiAgIDwveG1wTU06SGlzdG9yeT4KICA8L3JkZjpEZXNjcmlwdGlvbj4KIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAKPD94cGFja2V0IGVuZD0idyI/Ps0ZiVoAAAAGYktHRAC8ABoAJ5jPzacAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfoChMAAje0VBQSAAAPzUlEQVR42u3deXCUdZ4G8Oftt7vTnas7HcjViUkg0EpIR4KBcIVgrbPoUFOxFCojBgalmBldtVyhZMdhKBapioPjrE6NWhTHShaHxZktdJxdxxlXCOFajmwaIyREyGEHEkinSei73+79AxKJ3ek0biRvO8/nnyRvv9cvUHnq+76/Qwh+9HwQRERE40zBXwEREcmBcvAbYfGvBf46iIjoTht8UscKiYiIZIGBREREDCQiIiIGEhERMZCIiIgYSEREJEtK/gqIiORr0+Yac6OlaYnL7V4oimKRw+HKtNn7EQze2TkNBEGAQZ+MhATtJUmSzmg1moPF5sIPN25Ybxmzawz2/+Y4JCIieairPybsrn23stPa9WJPj222FAjI8j5FhQJpaYbjOcasV1ZUP7a/fH7ZN0rJoRxiIBERyaoiMjVamna2Xuyce+v2FH0SFi0og9GYBb0uGYLizr5xCQYCsF/rh9XahU8PHUOffWDY5wX5OUeKzYVPbNywvpmBREQU4555bt3SE6fP7HC63EkAoIlTY/WqKlSUL0BeXi5UKhW8Xh8kSRqfikgUoVar4PP50NbWjgN1h7B91164PV4AQLxWM1BaUvTkb17f+h4DiYgoRr2w7qVnDx879brT5QYAVFdVYuWK5TCkGtDSch4nTjbgyNFTaG5th88/PoGkUoowFeRi7pyZKL1vBqZOnQJbrw3v7N6D2r37B0MJ88pmPverrVveYCAREcVmZbTP6XJDKYp4tWYDKhaWo+nzc9i2vRanLc2yvO8SswlrVlejcNrdOHCwDmvXb4ZfkhCv1aC0pGhZtJUS57IjIpKBTZtrTDcf00Epitj25lbMnz8PtXv24SfP/ly2YQQApy3N+MmzP0ftnn2YP38etr25FUpRhNPlxonTZ3Zs2lxjup3zMZCIiMZJXf0xodHStHPwndGrNRtgNhfht29tx7Zd+2KmHdt27cNv39oOs7kIr9ZsAAA4Xe6kRkvTzrr6Y1E/fWMgERGNk92171YO9qarrqpExcJy/G7vH/D7/X+Jubb8fv9f8Lu9f0DFwnJUV1UCAFovds7dXftuJQOJiEjmOq1dLwI3etOtXLH8xjujMJXRhAQlnl6cjY3LJmNFeTpU4p195Z+pVaJ0ghb36OKgUggRK6Wmz89h5Yrl0MSph7WRgUREJFObNteYe3psswFg9aoqGFIN2La9NmQ/rUqBXzxuwoLidNyTq8eDs4z4h8U5o55fqRDw7CIjtlUX4q3l9+DHczOhuM0cK0yJw0tzc/BSeR5WlhjxVGkOXp6fi8enGKAZ4WTbttfCkGrA6lVVAICeHtvsTZtrzAwkIiKZarQ0LRmcgaGifAFaWs6H7cBQUahDeop22LZ7CwxQKyOny8qydJRMMkCtVECjEjG7IBUPTzdEfX+laVqsmZWDLJ0Gt14pXiViVnYKnp+RBW2YSu20pRktLedRUb4AACAFAmi0NC1hIBERyZTL7V4I3JiBIS8vFydONoTdLz5ODNmmFhVIS1JFPP/UzKSQbaaMpKjuTSsKeNScCZU4GBGhwZORGIdH8lPCHn/iZAPy8nKRok8a1lYGEhGRDImiWAQAixaUQaVS4cjRU2H3u+4OHQQbBDAxKfLc2J4wg2d18aqo7u3v8pKRrFHevBLCfL2RUYUTk6AUQsPqyNFTUKlUWLSgbFhbGUhERDLkcLgyAcBozILX60Nza3vY/Zye8LMy6LSRA6nf6QvZlqiJboGHrGTN1yojIbRSCt54fGcMcx/Nre3wen0wGrOGtZWBREQkQzZ7PwBAr0uGJEkjTgfUcdUd7okZ0nTqiOe/OuAN2aZVi5icoh69ehvqsBAMU5sNpwnzHsnnlyBJEvS65GFtZSAREcnQ4HpGo83abbX54POHLj8xQRcX8bgLPc6w281ZiaPeW/eAJ7QiCvOzFAzikss/4nkG2xbt2k0MJCIiGfMHgnCE+aOfYdBGPO5/rQ5IUmgQTM9OHvWadR398EkBRHyHBKDN7kS/f+zWamIgERHJXHefK2Rbul4T8Zh+TwBdfaFVUk5qPHKTI3duuOKWcPiiLUxl9FWF5JECeP8L25i2k4FERCRzXVdDgyVRq8KUiZEf2539ciBkmygIeHBa6qjX/PdzNtRd6IV/WJV14/tepxc7G7vQHqbjxP+Hkv/URETy1tx5HRX3Dt8mCMAckx7nr3SPeNzHZ/uwaHpaSNfs4rv0yGnqRedA5EDZe86Gv168hnnGJKRolAgEgmizu1Hf7UAgOPbtZIVERCRzR1r64fGFvqsxjfI+6KrTjxZraA83lVKBqpkZUV37qkfC+xfs+NfPr2L3uV7UXf52woiBREQUAzz+IDq7r4dsv2tiAnJH6ca9v6EHUpgEMWUk4QcmvazayUAiIooBZ9tDKx2FQsD3iiO/Dzrf64Gl3R72s8XT03FvmpaBRERE0fuo4erNrtjDzZhsgHKUabxrj1+GwxPadVylVGBlWTZyk1SyaCMDiYgoBvQ6JFzoCu01l5ygwvfNKRGP7XNLeP9UV9h3PwlxSjy3MBfTUzUMJCIiis6Bxitht1cUpUEUIldJf23tx/+09ob9LFGjxI/n3YUlBToGEhERje7Ts9fCjkmaqNPgkZmjjy3afvwyWi4NhP1MrVRgyfQMvDDHiLsSx+cRHgOJiCiGfNIwfNzRYF10vzkdExMiDy0NBIF/OdiJ9iuOsJ8LAKakJWLtwjysuTcNuXc4mBhIREQx5D8b+9DR81UX8MHXQgkaJZ6syB71eI8UxCufdODcpZFn4FYpBMzI1mPdgjz805xsPFqQgmn6OAjfcts4UwMRUYx575AVz1dOhXCzqhkMpWl36bCkKAUfnumLeLw3EMRrdV/iRyVpmJ1vwEiLTSgEIFuvRXayBhWTUuHxBeDw+OHw+NF81YEPOq6N6SBZVkhERDHmVLsDp8/3hoSIAKBylhHTM0YfWxQIAjtP9eDfjn+J/puziYdWQMMnVVWLCqRoVTAma7EoLxWPF6SOabsYSEREMWjbJ1/CNrRu0VfUSgVW35+LjMToHoAd6ryOTR9fQEOHHf7gSAvyhV9+YtrERIzlLEIMJCKiGHTdE8C7BzoQCPPMzJCoxj8+mA+9RozqXAO+AN4+eRm//O+LaLRegzdkAG6YJcwBBIJjO6kdA4mIKEYdvXAdnzRcDtkeBJCu02Lt4jzo4qL/M98+4MNbJy/jpY8v4L/O9qDN5oTXHwhfIQmApXtgTDs6sFMDEVEM2324GzkT4mEyJg+rZ4IAslPj8bPvT8Jrf25Dt8MfffXlD+CDL+z44As71AoBU3VqTNJpkKJRIlGthALA+V4n/mwdGNO2MJCIiGJYEMDrH7Vjw8MFyEzRfr2OQbpOixcfnIQdBzvQdMV92+f3BoL4rM+Dz/o833pb+MiOiCjGDXgCePWPF9Db7xmqkG6VkqjGMw/kY/FUnazbwUAiIvoO6HH48dqfvsDVfk/Ynm9qpYils7KxdlEOJmhFWbaBgURE9B3RYfdhy/ut6Op1hv1cEIB7spLxi4cKsHR66qjLVjCQiIjoG+t1Stjyxws4++W1EfeJjxPx99PTseWhSfiBSQ+VTIKJgURE9B1z3RtAzUft+MTSDSnCWCFDghpLzJmoeWgyVhZPRH7y+C7Ux152RETfUbUnetDa7cDSWUbo49Uj7pekUWLu5FSU5Rtw2e7GF1ccaLg0gM/7PBjjsa8MJCKiv1VHOxxovNSKH83OwIxcPRQRhrIqAGTqNMhIisO8fANcXgk9/R70XPeg1+FFr9MPp0+C3SvB6vTDExjbtGIgERGNA0EQEAwGEQwEvvVrOX0BvFnfhcJmGx6ekY68CQlh9/t6vGiUInJStMjRaW/eK4CbX/1SAJ/3DGDHuasjzmc32DZBiO4dFd8hERGNA4P+xswK9mv9EEURKuW33xW76YobL3/cjl31HeiwOaN4HDfy5KqiQkBRWjIeumWGiEEqpQhRFGG/1j+srQwkIiIZSkjQXgIAq7ULarUKpoLcO3btwx3X8c8ft+HtujacvdR/c746RHiYF2Zy1ZvZZEyKC9nbVJALtVoFq7VrWFtHw0d2RETjQJKkMwAyPz10DD/z+TB3zkx8du7CHb2H05ddOH3ZikSVAvfnJcOUlgijToM4UTFKxYShCfN6Xb6QvebOmQmfz4dPDx27ta0MJCIiOdJqNAcBfK/PPoC2tnaU3jcD23a9Ny73ct0XwAfn7cB5O0RBQEmaBgUGLTIS45AaH4cElXhzrNIt69MGAWu/C3/qDB3vVHrfDLS1taPPPnBrWxlIRERyVGwu/PBiu3WLFAjgQN0hPLFqJUrMJpy2NI9v5RYM4kS3Cye6XUPbFAKgV4swapWIExWIEwW4/UE02Fz4epeMErMJU6dOwc5d7wAARIUCxebCD/8jiqzlOyQionGwccN6S1qa4TgAbN+1F7ZeG9asrpblvQaCgM0j4Yzdg5O9LhzuceJUmDACgDWrq2HrtWH7rr0AgLQ0w/GNG9ZborkOA4mIaJzkGLNeAQC3x4t3du9B4bS7sWbVsphtz5pVy1A47W68s3sP3B7vsDYykIiIZGxF9WP7C/JzjgBA7d79OHCwDj+segSPVj4Qc215tPIB/LDqERw4WIfavfsBAAX5OUdWVD+2n4FERCRz5fPLgsXmwifitZoBAFi7fjMsljN4+qerY6pSWrNqGZ7+6WpYLGewdv1mAEC8VjNQbC58onx+WdTTOTCQiIjG0cYN65tLS4qejNdq4JckrHlqHerrD6N6+TK8/cbLKDGbZHvvJWYT3n7jZVQvX4b6+sNY89Q6+CUJ8VoNSkuKnty4Yf1t9dAQgh89HwQAYfGvBf7XICIaHy+se+nZw8dOve503VhmvLqqEitXLIch1YCWlvM4cbIBR46eQnNrO3x+aVzuUaUUYSrIxdw5M1F63wxMnToFtl4b3tm9Z+gxXbxWg3llM5/71dYtb0R73qEcYiAREcnDM8+tW3ri9JkdTpc7CQA0cWqsXlWFivIFyMvLhUqlgtfrgySNTyCJogi1WgWfz4e2tnYcqDuE7bv2DnVgiNdqBkpLip78zetbb2tAFQOJiEiGNm2uMTVamna2Xuyce+v2FH0SFi0og9GYBb0uGYLizr5xCQYCsF/rh9XahU8PHRsa9DqoID/nSLG58InbfUzHQCIikrG6+mPC7tp3KzutXS/29NhmS3dgRvBvVDEpFEhLMxzPMWa9sqL6sf2304GBgUREFHsVk7nR0rTE5XYvFEWxyOFwZdrs/QjeyZXzcGMJCYM+GQkJ2kuSJJ3RajQHi82FH0Y76JWBREREsjeYQ+z2TUREssBAIiIiBhIREREDiYiIGEhEREQMJCIikqWhbt9ERESskIiI6G/e/wECJa/2LPDUiwAAAABJRU5ErkJggg==";
PARAM["WIDGET_IMG"]="$var_img";
PARAM["WIDGET_AUTOSTART"]="[Desktop Entry]\nName=distro-widget\nGenericName=distro-widget\nComment=distro-widget\nType=Application\nExec=conky --daemonize\nNoDisplay=false\nHidden=false\nX-GNOME-Autostart-enabled=true\nX-GNOME-Autostart-Delay=0";
PARAM["WIDGET_CONFIGURATION"]=$(cat <<EOF
conky.config = {
	maximum_width = 420,
	minimum_width = 420,
	minimum_height = 420,
	alignment = 'top_right',
	background = true,
	own_window = true,
	own_window_class = 'Conky',
	own_window_transparent = false,
	own_window_type = 'desktop',
	own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
	own_window_argb_visual = true,
	own_window_argb_value = 200,
	own_window_colour = '000000',
	double_buffer = true,
	no_buffers = true,
	use_spacer = 'none',
	use_xft = true,
	xftalpha = 1,
	font = 'Sans:size=10',
	update_interval = 10,
	uppercase = false,
	override_utf8_locale = true,
	stippled_borders = 0,
	border_width = 0,
	draw_borders = false,
	draw_graph_borders = true,
	draw_outline = false,
	draw_shades = false,
	show_graph_scale = true,
	show_graph_range = true,
	gap_x = 22,
	gap_y = 51,
	net_avg_samples = 2,
	cpu_avg_samples = 2,
	short_units = true,
	pad_percents = 2,
	text_buffer_size = 2048,
	out_to_console = false,
	out_to_stderr = false,
	extra_newline = false,
	color1 = '#C0C0C0',
	color2 = '#FFFFFF',
	color3 = '#FFB871'
}
conky.text = [[
\${image ~/.config/conky/distro-tracker.png -s 420x78}
\${voffset -26}
\${offset 16}\${color1}OS : \${font Sans:size=10:style=Bold}\${color2}\${exec lsb_release -d | sed -e 's/.*: //' | awk '{print \$2,\$3,\$4}'}\${font}\${color}
\${offset 16}\${color1}Kernel : \${color2}\$kernel\${color}
\${offset 16}\${color1}System : \${color2}\${exec cat /sys/class/dmi/id/product_name}\${color}
\${voffset 10}
\${offset 16}\${color1}Uptime : \${color2}\$uptime\${alignr 16}\${color1}Hostname : \${font Sans:size=10:style=Bold}\${color2}\${nodename}\${font}\${color}
\${hr 1}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}C P U  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}model :\${alignr 10}\${execi 1000 cat /proc/cpuinfo | grep 'model name' | sed -e 's/model name.*: //' | sed -e 's/ @.*//' | uniq}
\${offset 6}frequency :\${alignr 10}\${freq_g}GHz
\${offset 6}temperature :\${alignr 10}\${texeci 30 sensors | grep "Package id 0" | cut -d ':' -f 2 | cut -d '(' -f 1 | tr -d ' '}
\${offset 6}running :\${alignr 20}\$running_processes
\${offset 6}load :\${alignr 10}\${cpu}%
\${voffset -10}
\${offset 6}\${cpubar cpu0 16,407}
\${voffset -10}
\${offset 6}processes \${alignr}\${offset -60}PID \${alignr}\${offset -30}CPU% \${alignr}\${offset -10}mem%
\${offset 6}\${top name 1}\${alignr}\${offset -83}\${top pid 1}\${alignr}\${offset -45}\${top cpu 1}\${alignr}\${offset -10}\${top mem 1}
\${offset 6}\${top name 2}\${alignr}\${offset -83}\${top pid 2}\${alignr}\${offset -45}\${top cpu 2}\${alignr}\${offset -10}\${top mem 2}
\${offset 6}\${top name 3}\${alignr}\${offset -83}\${top pid 3}\${alignr}\${offset -45}\${top cpu 3}\${alignr}\${offset -10}\${top mem 3}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}M E M O R Y  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}RAM : \$memperc%\${alignr}\${offset -6}\$mem / \$memmax
\${offset 6}\${membar 16,407}
\${voffset -6}
\${offset 6}SWAP : \$swapperc%\${alignr}\${offset -6}\$swap / \$swapmax
\${offset 6}\${swapbar 16,407}
\${voffset -6}
\${offset 6}DISK : \$fs_used_perc%\${alignr}\${offset -6}\$fs_used / \$fs_size
\${offset 6}\${fs_bar 16,407}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}N E T W O R K  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}Default Gateway :\${alignr 10}\${gw_ip}
\${offset 6}Internal IP Address :\${alignr 10}\${addrs ${PARAM[WIDGET_IP]}}
\${offset 6}Upload / Download Total :\${alignr 10}\${totalup ${PARAM[WIDGET_IP]}} / \${totaldown ${PARAM[WIDGET_IP]}}
\${offset 6}Upload / Download Speed :\${alignr 10}\${upspeed ${PARAM[WIDGET_IP]}} / \${downspeed ${PARAM[WIDGET_IP]}}
\${voffset -2}
\${offset 6}\${color3}\${font Roboto:style=Bold:size=16}T R A C K  &  R E P O R T  \${hr 2}\${font}\${color}
\${voffset -10}
\${offset 6}\${font Sans:size=10:style=Bold}\${color2}random check : \${color}\${font}\${color1}\${exec bash ${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh -s ${var_file_tracker} -t tracker -c main}\${color}
\${offset 6}\${font Sans:size=10:style=Bold}\${color2}last report : \${color}\${font}\${color1}\${exec bash ${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh -s ${var_file_reporter} -t reporter -c main | fold -s -w 46 | sed '1!s/^/  /'} | \${exec bash ${PARAM[PATH]}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh -s ${var_file_reporter} -t reporter -c revision | xargs -I{} date -d {} '+%d/%m - %Hh%M'}\${color}
\${offset 6}\${font Sans:size=10:style=Bold}\${color2}more on website : \${color}\${font}\${color1}https://distrotracker.com | conf. v${APP[VERSION]}\${color}
]]
EOF
);
# QUIET.
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Install_LocalConky.
#	DESCRIPTION : add Conky configuration.
function Install_LocalConky() {
	# Some definition - menu.
	local var_path;
	local var_user;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			-USER) var_user="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Test : widget autostart.
	if Test_File -PATH "${var_path}${CONF[SEPARATOR]}autostart${CONF[SEPARATOR]}conky.desktop" -CREATE -OVERWRITE -CHOWN "${var_user}:${var_user}" -CHMOD "755"; then
		Set_File -PATH "${var_path}${CONF[SEPARATOR]}autostart${CONF[SEPARATOR]}conky.desktop" -RAW "${PARAM[WIDGET_AUTOSTART]}" -INTERPRET;
		# Test : widget configuration.
		if Test_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}conky.conf" -CREATE -OVERWRITE -CHOWN "${var_user}:${var_user}" -CHMOD "755"; then
			Set_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}conky.conf" -RAW "${PARAM[WIDGET_CONFIGURATION]}";
			# Test : widget img header.
			if Test_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-tracker.png" -CREATE -OVERWRITE -CHOWN "${var_user}:${var_user}" -CHMOD "755"; then
				Set_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-tracker.png" -RAW "${PARAM[WIDGET_IMG]}" -ENCODE "base64";
				# Test : widget reader home script.
				if Test_File -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh" -CREATE -OVERWRITE -EXEC -CHOWN "${var_user}:${var_user}" -CHMOD "755" && Get_Distant -URL "${DEFAULT[SOURCE_READER]}" -TYPE "file" -PATH "${var_path}${CONF[SEPARATOR]}conky${CONF[SEPARATOR]}distro-reader.sh"; then
					# Test : widget downloader bin.
					if Test_File -PATH "${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader" -CREATE -OVERWRITE -EXEC && Get_Distant -URL "${DEFAULT[SOURCE_DOWNLOADER]}" -TYPE "file" -PATH "${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader"; then
						return 0;
					else
						Write_Script -TEXT "distant file (downloader) can't be downloaded.";
						return 1;
					fi;
				else
					Write_Script -TEXT "distant file (reader) can't be downloaded.";
					return 1;
				fi;
			else
				Write_Script -TEXT"autostart file can't be writed.";
				return 1;
			fi;
		else
			Write_Script -TEXT "configuration file can't be writed.";
			return 1;
		fi;
	else
		Write_Script -TEXT "image file can't be writed.";
		return 1;
	fi;
}

# Install_LocalTask.
#	DESCRIPTION : add systemd configuration.
function Install_LocalTask() {
	# Some definition - menu.
	local var_path;
	# Some definition - next.
	local var_tracker_service;
	local var_tracker_timer;
	local var_reporter_service;
	local var_reporter_timer;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Some init.
	var_tracker_service="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-tracker.service";
	var_tracker_timer="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-tracker.timer";
	var_reporter_service="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-reporter.service";
	var_reporter_timer="${CONF[SEPARATOR]}etc${CONF[SEPARATOR]}systemd${CONF[SEPARATOR]}system${CONF[SEPARATOR]}distro-reporter.timer";
	# Test : data exists.
	if Test_File -PATH "$var_tracker_service" -CREATE -OVERWRITE && \
		Test_File -PATH "$var_tracker_timer" -CREATE -OVERWRITE && \
		Test_File -PATH "$var_reporter_service" -CREATE -OVERWRITE && \
		Test_File -PATH "$var_reporter_timer" -CREATE -OVERWRITE; then
		# tracker service.
cat <<EOF > "$var_tracker_service"
[Unit]
Description=Download Distro Tracker Data

[Service]
Type=oneshot
ExecStart=${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader --format json --type tracker --output ${var_path}${CONF[SEPARATOR]}distro-tracker.json
StandardOutput=append:${LOG[TRACKER]}
StandardError=append:${LOG[TRACKER]}

[Install]
WantedBy=multi-user.target
EOF
		# tracker timer.
cat <<EOF > "$var_tracker_timer"
[Unit]
Description=Run Distro Tracker Daily at 12:30 PM

[Timer]
OnCalendar=*-*-* 12:30:00
Persistent=true

[Install]
WantedBy=timers.target
EOF
		# reporter service.
cat <<EOF > "$var_reporter_service"
[Unit]
Description=Download Distro Reporter Data

[Service]
Type=oneshot
ExecStart=${CONF[SEPARATOR]}usr${CONF[SEPARATOR]}bin${CONF[SEPARATOR]}distro-downloader --format json --type reporter --output ${var_path}${CONF[SEPARATOR]}distro-reporter.json
StandardOutput=append:${LOG[REPORTER]}
StandardError=append:${LOG[REPORTER]}

[Install]
WantedBy=multi-user.target
EOF
		# reporter timer.
cat <<EOF > "$var_reporter_timer"
[Unit]
Description=Run Distro Reporter Daily at 13 PM

[Timer]
OnCalendar=*-*-* 13:00:00
Persistent=true

[Install]
WantedBy=timers.target
EOF
		# Test : service exists.
		if Start_Software -NAME "distro-tracker.service" -ENABLE && \
		Start_Software -NAME "distro-tracker.timer" -ENABLE && \
		Start_Software -NAME "distro-reporter.service" -ENABLE && \
		Start_Software -NAME "distro-reporter.timer" -ENABLE; then
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Test : rights.
	if Test_Root; then
		# Test : install.
		if Install_LocalConky -PATH "${PARAM[PATH]}" -USER "${PARAM[USERNAME]}"; then
			Write_Script -TEXT "Conky configuration installed with '$var_file_tracker' tracker data & '$var_file_reporter' reporter data.";
			# Test : conky restarting.
			if Start_Software -COMMAND "conky -d" -USER "${PARAM[USERNAME]}" -GUI; then
				Write_Script -TEXT "Conky launched.";
				Write_Script -TEXT "installing systemd services & timers ...";
				# Test : systemd services & timers.
				if Install_LocalTask -PATH "${PARAM[PATH]}${CONF[SEPARATOR]}conky"; then
					Write_Script -TEXT "systemd tasks installed.";
				else
					Write_Script -TEXT "systemd tasks not installed.";
				fi;
			else
				Write_Script -TEXT "Conky not launched.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "Conky configuration not installed.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Root rights needed.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;