#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               gen-man.sh               | #
# |________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="gen-man";
APP["SECTION"]="1";
APP["SHORTNAME"]="GenMan";
APP["SLOGAN"]="Create man file.";
APP["DESCRIPTION"]="This open-source tool analyzes local content and create a man file about what it does.";
APP["REQUIREMENTS"]="null";
APP["EXAMPLES"]="man output of gen-man.sh in quiet mode :|${APP[NAME]} -s gen-man.sh -o ./documentation -q";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.0.0|2024-09-01T10:00:00+0200|New documentation.#0.0.1|2024-06-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.0.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#mandatory#Single local file path of source content, with the same programmatic structure.|path.|no default.";
OPTIONS["OUTPUT"]="2#o#FOLDERPATH#optionnal#Single local folder path (final slash automatically defined, file name always 'SOURCE_FILENAME.SECTION') to store the final result.|path.|'PWD'.";
OPTIONS["QUIET"]="3#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="4#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="5#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="${2%[\\/]}${CONF[SEPARATOR]}"
			Test_Param -OPTION "$1" -PARAM "${PARAM[OUTPUT]}"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["OUTPUT"]="${PWD%[\\/]}";
DEFAULT["QUIET"]=false;
# Some script definition.
if [[ "$(grep -Po '(?<=APP\["SECTION"\]=")[^"]*' "${PARAM[SOURCE]}" 2>/dev/null)" =~ ^[1-9]$ ]]; then
	var_global_source_section="$(grep -Po '(?<=APP\["SECTION"\]=")[^"]*' "${PARAM[SOURCE]}")";
	var_global_source_filename="$(basename -- "${PARAM[SOURCE]}" | cut -d '.' -f1)";
	if [ -z "${PARAM[OUTPUT]}" ]; then
		PARAM["OUTPUT"]="${DEFAULT[OUTPUT]%[\\/]}${CONF[SEPARATOR]}${var_global_source_filename}.${var_global_source_section}";
	else
		PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}${CONF[SEPARATOR]}${var_global_source_filename}.${var_global_source_section}";
	fi;
else
	Stop_App -CODE 1 -TEXT "Need valid bash section inside '--source' option.";
fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Set_Man.
#	DESCRIPTION : write output with manual.
#	RETURN : string.
function Set_Man() {
	# Some definition.
	local var_source;
	local var_output;
	local var_source_name;
	local var_source_section;
	local var_source_slogan;
	local var_source_description;
	local var_source_examples;
	local var_source_author;
	local var_source_url;
	local var_source_version;
	local var_source_license;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct values from file source.
	var_source_name=$(grep -Po '(?<=APP\["NAME"\]=")[^"]*' "$var_source");
	var_source_section=$(grep -Po '(?<=APP\["SECTION"\]=")[^"]*' "$var_source");
	var_source_shortname=$(grep -Po '(?<=APP\["SHORTNAME"\]=")[^"]*' "$var_source");
	var_source_slogan=$(grep -Po '(?<=APP\["SLOGAN"\]=")[^"]*' "$var_source");
	var_source_description=$(grep -Po '(?<=APP\["DESCRIPTION"\]=")[^"]*' "$var_source");
	var_source_examples=$(grep -Po '(?<=APP\["EXAMPLES"\]=")[^"]*' "$var_source");
	var_source_author=$(grep -Po '(?<=APP\["AUTHOR"\]=")[^"]*' "$var_source");
	var_source_url=$(grep -Po '(?<=APP\["URL"\]=")[^"]*' "$var_source");
	var_source_version=$(grep -Po '(?<=APP\["VERSION"\]=")[^"]*' "$var_source");
	var_source_license=$(grep -Po '(?<=APP\["LICENSE"\]=")[^"]*' "$var_source");
	# synopsis.
	local var_synopsis_text_mandatory;
	local var_synopsis_text;
	var_synopsis_text_mandatory=false;
	# options.
	# Loop : options array inside APP.
	while IFS='=' read -r var_option var_value; do
		# Extraction du nom de la clé et de la valeur
		if [[ $var_option =~ ^OPTIONS\[\"([^\"]+)\"\]$ ]]; then
			var_name="${BASH_REMATCH[1]}";
			var_value="${var_value#\"}";
			var_value="${var_value%\";}";
			local "$var_name"="$var_value";
			var_option_name="$(echo "$var_name" | tr '[:upper:]' '[:lower:]' | sed 's/_/-/g')";
			var_option_quick="$(echo "$var_value" | cut -d '#' -f2)";
			var_option_type="$(echo "$var_value" | cut -d '#' -f3)";
			var_option_state="$(echo "$var_value" | cut -d '#' -f4)";
			var_option_dsc="$(echo "$var_value" | cut -d '#' -f5)";
			var_option_dsc_def=$(echo "$var_option_dsc" | cut -d '|' -f1 | sed "s/\${APP\[NAME\]}/${var_source_name}/g" | sed "s/\${APP\[SHORTNAME\]}/${var_source_shortname}/g");
			var_option_dsc_val_ok=$(echo "$var_option_dsc" | cut -d '|' -f2 | sed "s/\${APP\[NAME\]}/${var_source_name}/g" | sed "s/\${APP\[SHORTNAME\]}/${var_source_shortname}/g");
			var_option_dsc_val_default=$(echo "$var_option_dsc" | cut -d '|' -f3 | sed "s/\${APP\[NAME\]}/${var_source_name}/g" | sed "s/\${APP\[SHORTNAME\]}/${var_source_shortname}/g");
			# Test : mandatory OPTION.
			if [[ "$var_option_state" == "mandatory" ]]; then var_synopsis_text_mandatory=true; fi;
			# Test : command text.
			if [[ "$var_option_quick" == "null" ]]; then
				var_options_text+=".TP\n\\\fB--${var_option_name}\\\fR (no arg)";
			else
				if [[ "$var_option_type" == "null" ]]; then
					var_options_text+=".TP\n\\\fB-${var_option_quick}\\\fR (no type),\n.TQ\n\\\fB--${var_option_name}\\\fR (no type)";
				else
					var_options_text+=".TP\n\\\fB-${var_option_quick}\\\fR ${var_option_type},\n.TQ\n\\\fB--${var_option_name}\\\fR ${var_option_type}";
				fi;
			fi;
			var_options_text+="\n${var_option_dsc_def}\n.br";
			var_options_text+="\n- state : ${var_option_state}.\n.br";
			# Test : dsc redefine.
			if [[ "$var_option_dsc_val_ok" == "null" ]]; then var_option_dsc_val_ok="nothing."; fi;
			var_options_text+="\n- accepted value(s) : ${var_option_dsc_val_ok}\n.br";
			var_options_text+="\n- default value : ${var_option_dsc_val_default}\n";
		fi;
	done < "$var_source";
	# Test : [] for OPTIONS if only one OPTION is mandatory.
	if [[ "$var_synopsis_text_mandatory" == true ]]; then var_synopsis_text="OPTIONS"; else var_synopsis_text="[ OPTIONS ]"; fi;
	# examples.
	local var_examples;
	local var_example;
	local var_example_dsc;
	local var_example_cmd;
	local var_examples_text;
	IFS='#' read -ra var_examples <<< "$var_source_examples";
	# Loop : valuelist examples.
	for var_example in "${var_examples[@]}"; do
		var_example_dsc="$(echo "$var_example" | cut -d '|' -f1)";
		var_example_cmd="$(echo "$var_example" | cut -d '|' -f2 | sed "s/\${APP\[NAME\]}/${var_source_name}/g")";
		var_examples_text+=".TP\n${var_example_dsc}\n${var_example_cmd}\n";
	done;
	Restore_IFS;
	echo -e ".TH ${var_source_name} ${var_source_section} ${DATE[DATE0]} ${var_source_version} ${var_source_name} Manual\n\n.SH NAME\n\\\fB${var_source_name}\\\fR - ${var_source_slogan}\n\n.SH SYNOPSIS\n\\\fB${var_source_name}\\\fR ${var_synopsis_text}\n\n.SH DESCRIPTION\n${var_source_description} - Go to ${var_source_url} to follow the development.\n\n.SH OPTIONS\n${var_options_text}\n.SH EXAMPLES\n${var_examples_text}\n.SH BUGS\n${var_source_url}\n\n.SH AUTHOR\n${var_source_author}\n\n.SH COPYRIGHT\n${var_source_license}" > "$var_output";
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Test : get content.
	if Test_File -PATH "${PARAM[SOURCE]}"; then
		# Test : manual filling.
		if Set_Man -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}"; then
			Write_Script -TEXT "manual created.";
		else
			Stop_App -CODE 1 -TEXT "Manual can't be created.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Need valid value for '--source' option.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;