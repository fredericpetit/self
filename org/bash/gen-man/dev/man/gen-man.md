<blockquote id="out2tag">



NAME



	gen-man - Create man file.





REQUIREMENTS



	x



 

SYNOPSIS



	gen-man OPTIONS





DESCRIPTION



	This open-source tool analyzes local content and create a man file about what it does.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path of source content, with the same programmatic structure.

 		- state : mandatory.

 		- accepted value(s) : path.

 		- default value : no default.



 	-o FOLDERPATH,

	--output FOLDERPATH

 		Single local folder path (final slash automatically defined, file name always 'SOURCE_FILENAME.SECTION') to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	man output of gen-man.sh in quiet mode :

		gen-man -s gen-man.sh -o ./documentation -q



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	1.0.0 - 2024-09-01

		New documentation.



 	0.0.1 - 2024-06-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.0.0

	License : GPL-3.0-or-later





</blockquote>
