<blockquote id="out2tag">



NAME



	gen-changelog - Create changelog file.





REQUIREMENTS



	x



 

SYNOPSIS



	gen-changelog OPTIONS





DESCRIPTION



	This open-source tool analyzes local content and create a changelog file about.





OPTIONS



	-s FILEPATH,

	--source FILEPATH

 		Single local file path of source content, with CHANGELOG variable.

 		- state : mandatory.

 		- accepted value(s) : path.

 		- default value : no default.



 	-o FOLDERPATH,

	--output FOLDERPATH

 		Single local folder path (final slash automatically defined, file name always 'changelog') to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	changelog output of gen-changelog.sh in quiet mode :

		gen-changelog -s gen-changelog.sh -o ./documentation -q



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	1.0.0 - 2024-09-01

		New documentation.



 	0.0.1 - 2024-06-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.0.0

	License : GPL-3.0-or-later





</blockquote>
