#!/bin/bash
# shellcheck source=/dev/null
#  ______________________________________________
# |                                              | #
# |               gen-changelog.sh               | #
# |______________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="gen-changelog";
APP["SECTION"]="1";
APP["SHORTNAME"]="GenLog";
APP["SLOGAN"]="Create changelog file.";
APP["DESCRIPTION"]="This open-source tool analyzes local content and create a changelog file about.";
APP["REQUIREMENTS"]="null";
APP["EXAMPLES"]="changelog output of gen-changelog.sh in quiet mode :|${APP[NAME]} -s gen-changelog.sh -o ./documentation -q";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.0.0|2024-09-01T10:00:00+0200|New documentation.#0.0.1|2024-06-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.0.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATH#mandatory#Single local file path of source content, with CHANGELOG variable.|path.|no default.";
OPTIONS["OUTPUT"]="2#o#FOLDERPATH#optionnal#Single local folder path (final slash automatically defined, file name always 'changelog') to store the final result.|path.|'PWD'.";
OPTIONS["QUIET"]="3#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="4#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="5#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="${2%[\\/]}${CONF[SEPARATOR]}"
			Test_Param -OPTION "$1" -PARAM "${PARAM[OUTPUT]}"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}";
DEFAULT["QUIET"]=false;
# Some script definition.
if [ -z "${PARAM[OUTPUT]}" ]; then
	PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}";
else
	PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}${CONF[SEPARATOR]}";
fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Set_Changelog.
#	DESCRIPTION : write output with changelog.
#	RETURN : string.
function Set_Changelog() {
	# Some definition.
	local var_source;
	local var_output;
	local var_source_name;
	local var_source_section;
	local var_source_changelog;
	local var_source_author;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-OUTPUT)
				var_output="${2%[\\/]}"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Construct values from file source.
	var_source_name=$(grep -Po '(?<=APP\["NAME"\]=")[^"]*' "$var_source");
	var_source_section=$(grep -Po '(?<=APP\["SECTION"\]=")[^"]*' "$var_source");
	var_source_changelog=$(grep -Po '(?<=APP\["CHANGELOG"\]=")[^"]*' "$var_source");
	var_source_author=$(grep -Po '(?<=APP\["AUTHOR"\]=")[^"]*' "$var_source");
	# changelog.
	local var_changelog;
	local var_change;
	local var_change_date;
	local var_change_version;
	local var_change_dsc;
	local var_changelog_text;
	# Read : each entry from changelog become folder.
	IFS='#' read -ra var_changelog <<< "$var_source_changelog";
	Restore_IFS;
	# Loop : valuelist changelog.
	for var_change in "${var_changelog[@]}"; do
		var_change_version="$(echo "$var_change" | cut -d '|' -f1)";
		# Create folder & files for each release.
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/.gitkeep" -CREATE;
		# bin.
		Test_Dir -PATH "${var_output}/${var_source_name}-${var_change_version}/usr/bin" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/usr/bin/.gitkeep" -CREATE;
		# man.
		Test_Dir -PATH "${var_output}/${var_source_name}-${var_change_version}/usr/share/man/man${var_source_section}" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/usr/share/man/man${var_source_section}/.gitkeep" -CREATE;
		# doc.
		Test_Dir -PATH "${var_output}/${var_source_name}-${var_change_version}/usr/share/doc/${var_source_name}" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/usr/share/doc/${var_source_name}/.gitkeep" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/usr/share/doc/${var_source_name}/copyright" -CREATE;
		# debian.
		Test_Dir -PATH "${var_output}/${var_source_name}-${var_change_version}/debian" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/debian/.gitkeep" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/debian/changelog" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/debian/control" -CREATE;
		Test_File -PATH "${var_output}/${var_source_name}-${var_change_version}/debian/rules" -CREATE -EXEC;
		# Set changelog file.
		var_change_date="$(LC_ALL=en_US.UTF-8 date -d "$(echo "$var_change" | cut -d '|' -f2)" +"%a, %d %b %Y %T %z")";
		var_change_dsc="$(echo "$var_change" | cut -d '|' -f3)";
		var_changelog_text="$(echo -e "${var_source_name} (${var_change_version}-1) UNRELEASED; urgency=medium\n\n  * ${var_change_dsc}\n\n -- ${var_source_author}  ${var_change_date}")";
		# Set changelog & copy license.
		echo -e "${var_changelog_text}" > "${var_output}/${var_source_name}-${var_change_version}/debian/changelog";
		cat "${var_output}/copyright" > "${var_output}/${var_source_name}-${var_change_version}/usr/share/doc/${var_source_name}/copyright";
	done;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Test : get content.
	if Test_File -PATH "${PARAM[SOURCE]}"; then
		# Test : changelog filling.
		if Set_Changelog -SOURCE "${PARAM[SOURCE]}" -OUTPUT "${PARAM[OUTPUT]}"; then
			Write_Script -TEXT "changelog created.";
		else
			Stop_App -CODE 1 -TEXT "Changelog can't be created.";
		fi;
	else
		Stop_App -CODE 1 -TEXT "Need valid value for '--source' option.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;