<blockquote id="out2tag">



NAME



	distro-downloader - Download Distro Tracker & Distro Reporter datas.





REQUIREMENTS



	program 'wget' (mandatory)



 

SYNOPSIS



	distro-downloader OPTIONS





DESCRIPTION



	This open-source tool download raw datas.





OPTIONS



	-s NAME,

	--source NAME

 		Place from which to download datas.

 		- state : optionnal.

 		- accepted value(s) : 'distrotracker.com' or 'gitlab.com'.

 		- default value : 'distrotracker.com'.



 	-t NAME,

	--tool NAME

 		Tool from which to download datas.

 		- state : mandatory.

 		- accepted value(s) : 'tracker' or 'reporter'.

 		- default value : no default.



 	-f NAME,

	--format NAME

 		Needed datas format.

 		- state : mandatory.

 		- accepted value(s) : 'csv', 'yaml' or 'json'.

 		- default value : no default.



 	-l (no type),

	--log (no type)

 		Log into /var/log/distro/downloader.log instead raw output.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-o FILEPATH,

	--output FILEPATH

 		Single local file path or single local folder path to store the final result.

 		- state : mandatory.

 		- accepted value(s) : path.

 		- default value : no default.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'true'.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	null

		null



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	0.0.1 - 2025-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 0.0.1

	License : GPL-3.0-or-later





</blockquote>
