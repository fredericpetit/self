#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________________
# |                                                  | #
# |               distro-downloader.sh               | #
# |__________________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="distro-downloader";
APP["SECTION"]="1";
APP["SHORTNAME"]="DDownload";
APP["SLOGAN"]="Download Distro Tracker & Distro Reporter datas.";
APP["DESCRIPTION"]="This open-source tool download raw datas.";
APP["REQUIREMENTS"]="program wget mandatory";
APP["EXAMPLES"]="null";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="0.0.1|2025-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="0.0.1";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#NAME#optionnal#Place from which to download datas.|'distrotracker.com' or 'gitlab.com'.|'distrotracker.com'.";
OPTIONS["TOOL"]="2#t#NAME#mandatory#Tool from which to download datas.|'tracker' or 'reporter'.|no default.";
OPTIONS["FORMAT"]="3#f#NAME#mandatory#Needed datas format.|'csv', 'yaml' or 'json'.|no default.";
OPTIONS["LOG"]="4#l#null#optionnal#Log into /var/log/distro/downloader.log instead raw output.|null|'false' because disabled.";
OPTIONS["OUTPUT"]="5#o#FILEPATH#mandatory#Single local file path or single local folder path to store the final result.|path.|no default.";
OPTIONS["QUIET"]="6#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'true'.";
OPTIONS["HELP"]="7#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="8#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# ===================== #
#          LOG          #
#
#	DESCRIPTION : everything that help debugging the APP.

declare -gA LOG;
LOG["PATH"]="${CONF[SEPARATOR]}var${CONF[SEPARATOR]}log${CONF[SEPARATOR]}distro";
LOG["DOWNLOADER"]="${LOG[PATH]}${CONF[SEPARATOR]}downloader.log";


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--tool|-t)
			PARAM["TOOL"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--format|-f)
			PARAM["FORMAT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--log|-l)
			PARAM["LOG"]=true
			shift
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["TRACKER_EXTENSIONS"]="csv|yaml|json";
DEFAULT["TRACKER_DT"]="https://distrotracker.com/datas";
DEFAULT["TRACKER_GL"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current";
DEFAULT["REPORTER_EXTENSIONS"]="yaml|json";
DEFAULT["REPORTER_DT"]="https://distrotracker.com/report";
DEFAULT["REPORTER_GL"]="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter";
DEFAULT["SOURCE"]="distrotracker.com";
DEFAULT["TOOL"]="tracker";
DEFAULT["LOG"]=false;
DEFAULT["QUIET"]=true;
# Some script definition.
# SOURCE.
if [[ -z "${PARAM[SOURCE]}" ]]; then PARAM["SOURCE"]="${DEFAULT[SOURCE]}"; fi;
if [[ ! "${PARAM[SOURCE]}" =~ ^(distrotracker.com|gitlab.com)$ ]]; then echo "fail-source"; exit 1; fi;
# TOOL.
if [[ ! "${PARAM[TOOL]}" =~ ^(tracker|reporter)$ ]]; then echo "fail-tool"; exit 1; fi;
if [[ "${PARAM[TOOL]}" == "tracker" ]]; then
	# FORMAT.
	if [[ "${PARAM[FORMAT]}" =~ ^(${DEFAULT["TRACKER_EXTENSIONS"]})$ ]]; then
		# SOURCE.
		if [[ "${PARAM[SOURCE]}" == "distrotracker.com" ]]; then
			PARAM["SOURCE"]="${DEFAULT[TRACKER_DT]}.${PARAM[FORMAT]}";
		elif [[ "${PARAM[SOURCE]}" == "gitlab.com" ]]; then
			PARAM["SOURCE"]="${DEFAULT[TRACKER_GL]}.${PARAM[FORMAT]}";
		fi;
	else
		echo "fail-format";
		exit 1;
	fi;
elif [[ "${PARAM[TOOL]}" == "reporter" ]]; then
	# FORMAT.
	if [[ "${PARAM[FORMAT]}" =~ ^(${DEFAULT["REPORTER_EXTENSIONS"]})$ ]]; then
		# SOURCE.
		if [[ "${PARAM[SOURCE]}" == "distrotracker.com" ]]; then
			PARAM["SOURCE"]="${DEFAULT[REPORTER_DT]}.${PARAM[FORMAT]}";
		elif [[ "${PARAM[SOURCE]}" == "gitlab.com" ]]; then
			PARAM["SOURCE"]="${DEFAULT[REPORTER_GL]}.${PARAM[FORMAT]}";
		fi;
	else
		echo "fail-format";
		exit 1;
	fi;
fi;
# LOG.
if [[ -z "${PARAM[LOG]}" ]]; then PARAM["LOG"]="${DEFAULT[LOG]}"; fi;
# QUIET - here, except for Get_Distant output.
if [[ -z "${PARAM[QUIET]}" ]]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Test : simple output but logging file.
	if [[ -n "${PARAM[LOG]}" ]]; then
		# Test : data exists.
		if Get_Distant -TYPE "file" -URL "${PARAM[SOURCE]}" -PATH "${PARAM[OUTPUT]}" -LOG "${LOG[DOWNLOADER]}"; then
			return 0;
		else
			return 1;
		fi;
	else
		# Test : data exists.
		if Get_Distant -TYPE "file" -URL "${PARAM[SOURCE]}" -PATH "${PARAM[OUTPUT]}" -VERBOSE; then
			return 0;
		else
			return 1;
		fi;
fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;