#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_pkgs.sh               | #
# |__________________________________________| #


# ====================== #
#          PKGS          #
#
#	DESCRIPTION : software interacting for all APPs.


# Test_Software.
#	DESCRIPTION : test the state of a software in the system.
#	RETURN : string.
function Test_Software() {
	# Some definition - menu.
	local var_name;
	local var_type;
	#local var_action; // SC2034 disable.
	#local var_depend; // SC2034 disable.
	# Some definition - next.
	local var_result;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME) var_name="$2"; shift 2 ;;
			-TYPE) var_type="$2"; shift 2 ;;
			-ACTION) shift 2 ;;
			-DEPEND) shift ;;
			*) shift ;;
		esac
	done
	# Tests : type software.
	if [[ "$var_type" == "program" ]]; then
		# TMP : import OverDeploy routine.
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-program";
		fi;
	elif [[ "$var_type" == "service" ]]; then
		# Test : systemd check.
		# SRC : https://stackoverflow.com/a/53640320/2704550.
		if [[ $(systemctl list-units --all --type service --full --no-legend "${var_name}.service" | sed 's/^\s*//g' | cut -f1 -d' ') == "${var_name}.service" ]]; then
			var_result="available";
		else
			var_result="fail-service";
		fi;
	elif [[ "$var_type" == "command" ]]; then
		if command -v "$var_name" &> /dev/null; then
			var_result="available";
		else
			var_result="fail-command";
		fi;
	else
		# tmp default.
		var_result="fail-unknow";
	fi;
	echo "$var_result";
}


# Start_Software.
#	DESCRIPTION : test effective restarting of a daemon (bash launch) or service.
#	RETURN : code.
function Start_Software() {
	# Some definition - menu.
	local var_name;
	local var_command;
	local var_enable;
	local var_user;
	local var_gui;
	# Some definition - next.
	local var_type;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME) var_name="$2"; shift 2 ;;
			-COMMAND) var_command="$2"; shift 2 ;;
			-ENABLE) var_enable=true; shift ;;
			-USER) var_user="$2"; shift 2 ;;
			-GUI) var_gui=true; shift ;;
			*) shift ;;
		esac
	done;
	# Tests : mandatory.
	if [[ -z "$var_name" && -z "$var_command" ]]; then return 1; fi;
	# Some init.
	if [ -n "$var_name" ]; then
		var_type=$(Get_FileExtension -PATH "$var_name");
	fi;
	# Tests : service || timer || daemon/command.
	if [ -n "$var_type" ] && [[ "$var_type" == "service" || "$var_type" == "timer" ]]; then
		# Test : no services operations wuthout logging.
		if Test_File -PATH "${LOG[MAIN]}" -CREATE; then
			echo "# ===== ${var_name} ===== #" >> "${LOG[MAIN]}";
			# Test : service ready.
			if [ -n "$var_enable" ]; then
				systemctl enable "$var_name" &>> "${LOG[MAIN]}";
			fi;
			# Test : service enabled = restart || start.
			if systemctl is-enabled "$var_name" &> /dev/null; then
				# Test : launch with user.
				if [ -n "$var_user" ]; then
					su -l "$var_user" -c "systemctl restart ${var_name} &>> ${LOG[MAIN]}";
				else
					systemctl restart "$var_name" &>> "${LOG[MAIN]}";
				fi;
			else
				# Test : launch with user.
				if [ -n "$var_user" ]; then
					su -l "$var_user" -c "systemctl start ${var_name} &>> ${LOG[MAIN]}";
				else
					systemctl start "$var_name" &>> "${LOG[MAIN]}";
				fi;
			fi;
		fi;
		# Test : service loaded.
		if systemctl list-units --type="$var_type" --state=loaded | grep -q "$var_name"; then
			return 0;
		else
			return 1;
		fi;
	else
		# Test : launch with user.
		if [ -n "$var_user" ]; then
			# Test : general user interface.
			if [ -n "$var_gui" ]; then xhost + &> /dev/null; fi;
			# Test : command success.
			if su -l "$var_user" bash -c "export DISPLAY=:0 && $var_command &> /dev/null"; then
				# Test : general user interface.
				if [ -n "$var_gui" ]; then xhost - &> /dev/null; fi;
				return 0;
			else
				# Test : general user interface.
				if [ -n "$var_gui" ]; then xhost - &> /dev/null; fi;
				return 1;
			fi;
		else
			# Test : command success.
			if $var_command &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		fi;
	fi;
}