#!/bin/bash
# shellcheck source=/dev/null
#  ________________________________________
# |                                        | #
# |               func_fs.sh               | #
# |________________________________________| #


# ==================== #
#          FS          #
#
#	DESCRIPTION : filesystem interaction for all APPs.


# Test_Dir.
#	DESCRIPTION : check directory.
#	RETURN : code.
function Test_Dir() {
	# Some definition.
	local var_path;
	local var_create;
	local var_chown;
	local var_chmod;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : folder.
	if [[ -n "$var_path" ]]; then
		if [[ "$var_create" = true ]]; then mkdir -p "$var_path" &> /dev/null; fi;
		if [ -d "$var_path" ]; then
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown -R "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod -R "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Test_DirPopulated().
#	DESCRIPTION : check if folder haz content in it.
#	RETURN : code.
function Test_DirPopulated() {
	# Some definition.
	local var_path;
	local var_extension;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-EXTENSION)
				var_extension="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	if [[ -n "$(ls -A "$var_path")" ]]; then
		if [[ -n "$var_extension" ]]; then
			if find "$var_path" -type f -name "*$var_extension" &> /dev/null; then
				return 0;
			else
				return 1;
			fi;
		else
			return 0;
		fi;
	else
		return 1;
	fi;
}


# Test_File.
#	DESCRIPTION : check file.
#	RETURN : code.
function Test_File() {
	# Some definition.
	local var_path;
	local var_overwrite;
	local var_create;
	local var_chown;
	local var_chmod;
	local var_exec;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-CREATE)
				var_create=true
				shift
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-EXEC)
				var_exec=true
				shift
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		if [ -f "$var_path" ]; then
			if [[ "$var_overwrite" = true ]]; then
				rm "$var_path" &> /dev/null;
				# Tests : dir chown + chmod || chown || chmod || regular.
				if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" && -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown" -CHMOD "$var_chmod";
				elif [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown";
				elif [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHMOD "$var_chmod";
				else
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				fi;
				touch "$var_path" &> /dev/null;
			fi;
		else
			if [[ "$var_overwrite" = true ]] || [[ "$var_create" = true ]]; then
				# Tests : dir chown + chmod || chown || chmod || regular.
				if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" && -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown" -CHMOD "$var_chmod";
				elif [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHOWN "$var_chown";
				elif [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE -CHMOD "$var_chmod";
				else
					Test_Dir -PATH "$(dirname "$var_path")" -CREATE;
				fi;
				touch "$var_path" &> /dev/null;
			fi;
		fi;
		if [ -f "$var_path" ]; then
			if [[ -n "$var_chown" && "$var_chown" != false && "$var_chown" != "null" ]]; then chown "$var_chown" "$var_path"; fi;
			if [[ -n "$var_chmod" && "$var_chmod" != false && "$var_chmod" != "null" ]]; then chmod "$var_chmod" "$var_path"; fi;
			if [[ -n "$var_exec" && "$var_exec" = true ]]; then chmod +x "$var_path"; fi;
			if [[ -n "$var_chattr" && "$var_chattr" != false && "$var_chattr" != "null" ]]; then chattr +i "$var_path"; fi;
			return 0;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Search_File.
#	DESCRIPTION : get filepath with regex, favourite extension filter available (the last win).
#	RETURN : string or code.
function Search_File() {
	# Some definition - menu.
	local var_path;
	local var_extension;
	local var_tool;
	local var_mode;
	# Some definition - next.
	local var_extension_favorite;
	local var_file;
	local var_result;
	local var_result_favorite;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="${2%[\\/]}"; shift 2 ;;
			-EXTENSION) var_extension="$2"; shift 2 ;;
			-TOOL) var_tool="$2"; shift 2 ;;
			-MODE) var_mode="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Some init.
	var_extension_favorite="${var_extension##*|}";
	# Tests : distro-tracker/distro-reporter.
	if [[ "$var_tool" == "distro-tracker" || "$var_tool" == "distro-reporter" ]]; then
		# Tests : distro-tracker.current.EXT || distro-reporter.EXT || DT/DR-date.EXT .
		if [[ "$var_mode" == "name" ]]; then
			# Loop : Process Substitution auto-discovery - last file win data state.
			while IFS= read -r var_file; do
				# Test : data exists.
				if Test_File -PATH "$var_file"; then
					var_result="$var_file";
					# Test : favorite extension.
					if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
						var_result_favorite="$var_file";
					fi;
				fi;
			done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/(distro-tracker\.current|distro-reporter)\.(${var_extension})$" 2>/dev/null | sort);
			Restore_IFS;
		elif [[ "$var_mode" == "date" ]]; then
			# Loop : Process Substitution auto-discovery - last file win data state.
			while IFS= read -r var_file; do
				# Test : data exists.
				if Test_File -PATH "$var_file"; then
					var_result="$var_file";
					# Test : favorite extension.
					if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
						var_result_favorite="$var_file";
					fi;
				fi;
			done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/(DT|DR)-([0-9]{8}_[0-9]{6})\.(${var_extension})$" 2>/dev/null | sort);
			Restore_IFS;
		fi;
		# Test : good content.
		if [[ -n "$var_result" && "$var_result" != false && "$var_result" != "null" ]]; then
			# Test : favorite extension.
			if [[ -n "$var_result_favorite" && "$var_result_favorite" != false && "$var_result_favorite" != "null" ]]; then
				echo "$var_result_favorite";
			else
				echo "$var_result";
			fi;
		else
			return 1;
		fi;
	elif [[ "$var_tool" == "distro-publisher" ]]; then
		# Loop : Process Substitution auto-discovery - last file win data state.
		while IFS= read -r var_file; do
			# Test : data exists.
			if Test_File -PATH "$var_file"; then
				var_result="$var_file";
				# Test : favorite extension.
				if [[ $(Get_FileExtension -PATH "$var_file") == "$var_extension_favorite" ]]; then
					var_result_favorite="$var_file";
				fi;
			fi;
		done < <(find "$var_path" -type f -regextype posix-extended -regex ".*/${DATE[DATE0]}-[0-9]{1,3}-report\.(${var_extension})$" 2>/dev/null | sort);
		Restore_IFS;
		# Test : good content.
		if [[ -n "$var_result" && "$var_result" != false && "$var_result" != "null" ]]; then
			# Test : favorite extension.
			if [[ -n "$var_result_favorite" && "$var_result_favorite" != false && "$var_result_favorite" != "null" ]]; then
				echo "$var_result_favorite";
			else
				echo "$var_result";
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Set_File.
#	DESCRIPTION : create file.
#	RETURN : code.
function Set_File() {
	# Some definition.
	local var_path;
	local var_raw;
	local var_interpret;
	local var_source;
	local var_append;
	local var_encode;
	local var_chmod;
	local var_chown;
	local var_chattr;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			-RAW)
				var_raw="$2"
				shift 2
				;;
			-INTERPRET)
				var_interpret=true
				shift
				;;
			-SOURCE)
				var_source="$2"
				shift 2
				;;
			-APPEND)
				var_append=true
				shift
				;;
			-ENCODE)
				var_encode="$2"
				shift 2
				;;
			-CHMOD)
				var_chmod="$2"
				shift 2
				;;
			-CHOWN)
				var_chown="$2"
				shift 2
				;;
			-CHATTR)
				var_chattr=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : file.
	if [[ -n "$var_path" ]]; then
		touch "$var_path" &> /dev/null;
		# Tests : echo, cp.
		if [[ -n "$var_raw" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				# Test : interpreter.
				if [[ -n "$var_interpret" ]]; then
					echo -e "$var_raw" | tee -a "$var_path" > /dev/null;
				else
					echo "$var_raw" | tee -a "$var_path" > /dev/null;
				fi;
			else
				# Test : base64.
				if [[ -n "$var_encode" ]]; then
					echo "$var_raw" | base64 -d > "$var_path";
				else
					# Test : interpreter.
					if [[ -n "$var_interpret" ]]; then
						echo -e "$var_raw" | tee "$var_path" > /dev/null
					else
						echo "$var_raw" | tee "$var_path" > /dev/null;
					fi;
				fi;
			fi;
		elif [[ -n "$var_source" ]]; then
			# Test : add.
			if [[ -n "$var_append" ]]; then
				cat "$var_source" >> "$var_path";
			else
				cp "$var_path" "$var_source" &> /dev/null;
			fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Get_FileExtension.
#	DESCRIPTION : get last extension, with avoiding different extensions.
#	RETURN : code.
function Get_FileExtension() {
	# Some definition.
	local var_path;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATH)
				var_path="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	echo "$var_path" | rev | cut -d '.' -f 1 | rev;
}


# Find_FileLine.
#	DESCRIPTION : find line started by pattern in a file.
#	RETURN : string or code.
function Find_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file;
	local var_line;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE)
				var_file="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file"; then
		var_line=$(grep "$var_pattern" "$var_file" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line" ]]; then echo "$var_line"; else return 1; fi;
	else
		return 1;
	fi;
}


# Compare_FileLine.
#	DESCRIPTION : search 1st line started by pattern in 1st file, compare it to 2nd file.
#	RETURN : string or code.
function Compare_FileLine() {
	# Some definition.
	local var_pattern;
	local var_file1;
	local var_file2;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1" && Test_File -PATH "$var_file2"; then
		var_line1=$(grep "^$var_pattern" "$var_file1" | head -n 1);
		var_line2=$(grep "^$var_pattern" "$var_file2" | head -n 1);
		# Test : pattern finded.
		if [[ -n "$var_line1" && "$var_line1" != false && "$var_line1" != "null" && \
			-n "$var_line2" && "$var_line2" != false && "$var_line2" != "null" ]]; then
			# Test : return file number 2 content for different content, return false for same content.
			if [[ "$var_line1" != "$var_line2" ]]; then echo "$var_line2"; else return 1; fi;
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}


# Backup_FileLine.
#	DESCRIPTION : recover line in 1st file from a 2nd file.
#	RETURN : string or code.
#	TODO : verify character | escaping.
function Backup_FileLine() {
	# Some definition.
	local var_pattern;
	local var_default;
	local var_file1;
	local var_file2;
	local var_diff;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DEFAULT)
				var_default="$2"
				shift 2
				;;
			-FILE1)
				var_file1="$2"
				shift 2
				;;
			-FILE2)
				var_file2="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Getting content.
	if Test_File -PATH "$var_file1"; then
		var_diff=$(Compare_FileLine -PATTERN "$var_pattern" -FILE1 "$var_file1" -FILE2 "$var_file2");
		var_pattern="^${var_pattern}";
		# Tests : replace by old content for different content, replace by default for same/empty content.
		if [[ -n "$var_diff" && "$var_diff" != false ]]; then
			sed -i "s|${var_pattern}|${var_diff}|" "$var_file1";
			echo "old";
		elif [[ -n "$var_default" ]]; then
			echo -e "$var_default" >> "$var_file1";
			echo "default";
		else
			return 1;
		fi;
	else
		return 1;
	fi;
}