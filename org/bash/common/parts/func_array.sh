#!/bin/bash
# shellcheck source=/dev/null
# shellcheck disable=SC2034
#  ___________________________________________
# |                                           | #
# |               func_array.sh               | #
# |___________________________________________| #


# ======================= #
#          ARRAY          #
#
#	DESCRIPTION : pre-defined work with indexed & associative (book) arrays.


# Sort_Array.
#	DESCRIPTION : write new array indexed from original.
#	TODO : auto index and manual index option.
#	RETURN : string.
function Sort_Array() {
	# Some definition.
	local var_name;
	local var_delimiter;
	local var_index;
	local var_key;
	local var_value;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				;;
		esac
	done;
	# Tests : data exists.
	if [[ -z "$var_name" || -z "$var_delimiter" ]]; then return 1; fi;
	if ! declare -p "$var_name" &>/dev/null; then return 1; fi;
	# Construct array indexed.
	declare -gA "${var_name}_SORT";
	# Get data from original array.
	local -n var_name_orig="$var_name";
	local var_name_sorted="${var_name}_SORT";
	# Loop : get data from original array.
	for var_key in "${!var_name_orig[@]}"; do
		local var_value="${var_name_orig[$var_key]}";
		local var_index="${var_value%%"${var_delimiter}"*}";
		# Stores the key in the sorted array under the extracted index.
		local -n var_name_ref="${var_name_sorted}"
		var_name_ref["$var_index"]="$var_key";
	done;
}


# Search_InArray.
#	DESCRIPTION : search if value exists in indexed array.
#	RETURN : code.
function Search_InArray() {
	# Some definition.
	local var_pattern;
	local var_data;
	local var_array;
	local var_element;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : need all param.
	if [ -z "$var_pattern" ] || [ -z "$var_data" ]; then return 1; fi;
	# IFS for delimit array.
	IFS=" " read -ra var_array <<< "$var_data";
	Restore_IFS;
	# Loop : indexlist search in array.
	for var_element in "${var_array[@]}"; do
		if [[ "$var_element" == "$var_pattern" ]]; then
			return 0;
		fi;
	done;
	return 1;
}


# Get_PositionInArray.
#	DESCRIPTION : search index position by this pattern in indexed array.
#	RETURN : string or code.
function Get_PositionInArray() {
	# Some definition.
	local var_pattern;
	local var_delimiter;
	local var_array;
	local var_element;
	local var_index=-1;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_delimiter" ]; then
		var_delimiter=",";
	fi;
	# Test : need all param.
	if [ -z "$var_pattern" ] || [ -z "$var_data" ]; then
		return 1;
	fi;
	# Split the data using the specified delimiter.
	IFS="$var_delimiter" read -ra var_array <<< "$var_data";
	Restore_IFS;
	# Loop : indexlist through the columns to find the pattern.
	for var_element in "${!var_array[@]}"; do
		if [[ "${var_array[${var_element}]}" == "$var_pattern" ]]; then
			var_index="$var_element";
		fi;
	done;
	# Test : pattern found.
	if [[ "$var_index" -ge 0 ]]; then
		echo "$var_index";
	else
		return 1;
	fi;
}


# Measure_MaxInArray.
#	DESCRIPTION : calculate longest word in array.
#	RETURN : string.
function Measure_MaxInArray() {
	# Some definition.
	local var_count;
	local var_array;
	local var_word;
	local var_length;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		var_count=0;
		for element in "${!var_array[@]}"; do
			var_word="${var_array[$element]}";
			var_length="${#var_word}";
			if [ "$var_length" -gt "$var_count" ]; then var_count="$var_length"; fi;
		done;
		echo "$var_count";
	fi;
}


# Add_Book.
#	DESCRIPTION : construct associative array.
function Add_Book() {
	# Some definition.
	local var_name="$1";
	# Test : book exists.
	if ! declare -p "$var_name" &>/dev/null; then declare -gA "$var_name"; fi;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRO" ]]; then
		# Some definition.
		local var_element;
		local var_data;
		local var_index;
		local var_fullname;
		local var_mode;
		local var_current_index;
		local var_current_release;
		local var_archive_index;
		local var_archive_release;
		local var_element_suffixes;
		local var_element_suffixes_var;
		local var_array;
		local var_array_keys;
		local var_array_suffixes;
		# Some init.
		var_element="$2";
		var_data="$3";
		# mode, indexes & releases.
		declare -a var_array_suffixes=("index" "fullname" "mode" "current_index" "current_release" "archive_index" "archive_release");
		# Loop : var_suffix key.
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			var_element_suffixes_var="var_${var_element_suffixes}";
			declare "${var_element_suffixes_var}"="${var_element}_${var_element_suffixes}";
		done;
		# Construct index & release values from Get_Distribution.
		IFS=',' read -ra var_array <<< "$var_data";
		Restore_IFS;
		declare -a var_array_keys=("$var_index" "$var_fullname" "$var_mode" "$var_current_index" "$var_current_release" "$var_archive_index" "$var_archive_release");
		# Loop var_suffix value.
		for i in "${!var_array_keys[@]}"; do
			eval "${var_name}[\"${var_array_keys[$i]}\"]=\"${var_array[$i]}\"";
		done;
	elif [[ "$var_name" == "TMP" ]]; then
		# Some init.
		local var_element="$2";
		local var_data="$3";
		eval "${var_name}[\"${var_element}\"]=\"${var_data}\"";
	fi;
}


# Get_Book.
#	DESCRIPTION : get associative array.
#	RETURN : string or code.
function Get_Book() {
	# Some definition.
	local var_name;
	local var_element;
	local var_data;
	local var_array_suffixes;
	local var_element_suffixes;
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			-ELEMENT)
				var_element="$2"
				shift 2
				;;
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : distrotracker.
	if [[ "$var_name" == "DISTRO" ]]; then
		# Loop: var_suffix key.
		declare -a var_array_suffixes=("index" "fullname" "mode" "current_index" "current_release" "archive_index" "archive_release");
		for var_element_suffixes in "${var_array_suffixes[@]}"; do
			if [[ "$var_data" == "$var_element_suffixes" ]]; then
				var_result="${DISTRO[${var_element}_$var_element_suffixes]}";
				break;
			fi;
		done;
	fi;
	# Test : return.
	if [[ -n "$var_result" && "$var_result" != "" ]]; then echo "$var_result"; else return 1; fi;
}


# Set_Book.
#	DESCRIPTION : set associative array from PARAM.
function Set_Book() {
	# Some definition.
	local var_name;
	local var_array;
	local var_element;
	local var_key;
	local var_value;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-NAME)
				var_name="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : new array.
	if ! eval "declare -p $var_name 2>/dev/null" | grep -q 'declare -A'; then
		eval "declare -gA $var_name";
	fi;
	# Test : predefined book.
	if [[ "$var_name" == "CREDENTIAL" ]]; then
		# Loop : valuelist credentials.
		IFS=',' read -ra var_array <<< "${PARAM[CREDENTIAL]}";
		Restore_IFS;
		for var_element in "${var_array[@]}"; do
			var_key=${var_element%%:*};
			var_value=${var_element#*:};
			CREDENTIAL["${var_key^^}"]="$var_value";
		done;
	fi;
}


# Remove_Book.
#	DESCRIPTION : remove associative array.
function Remove_Book() {
	# Some definition.
	local var_name="$1";
	unset "$var_name";
}