#!/bin/bash
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_string.sh               | #
# |_____________________________________________| #


# ======================== #
#          STRING          #
#
#	DESCRIPTION : pre-defined work with simple string manipulation.


# Get_DataFormat.
#	DESCRIPTION : get format based on first line content.
#	RETURN : string or code.
function Get_DataFormat() {
	# Some definition.
	local var_data;
	local var_substring;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-DATA)
				var_data="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Restrict test.
	var_substring="${var_data:0:50}";
	# Tests : CSV, JSON, YAML.
	if [[ "$var_substring" =~ , && ! "$var_substring" =~ [:{].*, ]]; then
		echo "csv";
	elif [[ "$var_substring" =~ ^[\{\[] ]]; then
		echo "json";
	elif [[ "$var_substring" =~ : && ! "$var_substring" =~ [,{]:*, ]]; then
		echo "yaml";
	else
		return 1;
	fi;
}


# Remove_DuplicateInString.
#	DESCRIPTION : remove duplicate value in string.
#	RETURN : string.
function Remove_DuplicateInString() {
	# Some definition.
	local var_string;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	echo "$var_string" | tr ' ' '\n' | sort | uniq | xargs;
}


# Search_InCsv.
#	DESCRIPTION : search if value exists in CSV file.
#	RETURN : code.
#	TODO : replace break.
function Search_InCsv() {
	# Some definition.
	local var_header;
	local var_header_cut;
	local var_line;
	local var_line_cut;
	local var_pattern;
	local var_delimiter;
	local var_position;
	local i;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-HEADER)
				var_header="$2"
				shift 2
				;;
			-LINE)
				var_line="$2"
				shift 2
				;;
			-PATTERN)
				var_pattern="$2"
				shift 2
				;;
			-DELIMITER)
				var_delimiter="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_delimiter" ]; then var_delimiter=","; fi;
	# Split headers and data line into fields.
	IFS="$var_delimiter" read -ra var_header_cut <<< "$var_header";
	IFS="$var_delimiter" read -ra var_line_cut <<< "$var_line";
	Restore_IFS;
	# Find the index of the corresponding column.
	var_position=-1;
	for i in "${!var_header_cut[@]}"; do
		if [[ "${var_header_cut[$i]}" == "$var_pattern" ]]; then
			var_position="$i";
			break;
		fi;
	done;
	# Test : valid index.
	if [[ "$var_position" -ge 0 && "$var_position" -lt "${#var_line_cut[@]}" ]]; then
		echo "${var_line_cut[$var_position]}";
	else
		return 1;
	fi;
}


# Set_RepeatCharacter.
#	DESCRIPTION : calculate how many same character repeat needed.
#	RETURN : string.
function Set_RepeatCharacter() {
	# Some definition.
	local var_character;
	local var_count;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-CHARACTER)
				var_character="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Test : if arg empty.
	if [ -z "$var_character" ] || [ -z "$var_count" ]; then
		return 1;
	else
		var_result="";
		for ((i=0; i < "$var_count"; i++)); do
			var_result+="$var_character";
		done;
	fi;
	echo "$var_result";
}


# Limit_StringLength.
#	DESCRIPTION : limit number of character in a string.
#	RETURN : string.
#	TODO : replace -DIRECTION by -POSITION.
function Limit_StringLength() {
	# Some definition.
	local var_string;
	local var_count;
	local var_count_before;
	local var_count_after;
	local var_direction;
	local var_spacer;
	local var_spacer_length;
	local var_result;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-STRING)
				var_string="$2"
				shift 2
				;;
			-COUNT)
				var_count="$2"
				shift 2
				;;
			-DIRECTION)
				var_direction="$2"
				shift 2
				;;
			-SPACER)
				var_spacer="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Some default.
	if [ -z "$var_direction" ]; then var_direction="after"; fi;
	if [ -z "$var_spacer" ]; then var_spacer="(...)"; fi;
	var_spacer_length="${#var_spacer}";
	# Test : if arg empty.
	if [ -z "$var_string" ] || [ -z "$var_count" ]; then
		return 1;
	else
		# Test : cut needed.
		if [ ${#var_string} -gt "$var_count" ]; then
			# Add spacer length to cut count.
			var_count_before=$((${#var_string} - var_count + var_spacer_length + 1));
			var_count_after=$((var_count - var_spacer_length));
			# Tests : sub place.
			if [ "$var_direction" = "before" ]; then
				var_result+="$var_spacer";
				var_result+=$(echo "$var_string" | cut -c "$var_count_before"-"${#var_string}");
			elif [ "$var_direction" = "after" ]; then
				var_result+=$(echo "$var_string" | cut -c 1-"$var_count_after");
				var_result+="$var_spacer";
			fi;
			echo "$var_result";
		else
			echo "$var_string";
		fi;
	fi;
}