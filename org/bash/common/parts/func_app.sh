#!/bin/bash
# shellcheck source=/dev/null
#  _________________________________________
# |                                         | #
# |               func_app.sh               | #
# |_________________________________________| #


# ========================= #
#          GENERIC          #
#
#	DESCRIPTION : pre-defined work for all APPs.


# CONF - pursuit : safe storage of original $IFS.
if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["IFS"]="$IFS";


# Restore_IFS.
#	DESCRIPTION : restore IFS.
function Restore_IFS() { IFS="${CONF["IFS"]}"; }


# COLOR.
declare -gA COLOR;
COLOR[BOLD]='\e[1m';
COLOR[UNDERLINE]='\e[4m';
COLOR[RED_LIGHT]='\e[1;38;5;160m';
COLOR[GREEN_LIGHT]='\e[1;38;5;34m';
COLOR[GREEN_DARK]='\e[1;38;5;28m';
COLOR[BLUE_LIGHT]='\e[1;38;5;25m';
COLOR[BLUE_DARK]='\e[1;38;5;27m';
COLOR[YELLOW_LIGHT]='\e[1;38;5;179m';
COLOR[YELLOW_DARK]='\e[1;38;5;178m';
COLOR[ORANGE_LIGHT]='\e[1;38;5;208m';
COLOR[ORANGE_DARK]='\e[1;38;5;202m';
COLOR[STOP]='\e[0m';
COLOR[TICK]="[${COLOR[GREEN_LIGHT]}✔${COLOR[STOP]}]";
COLOR[CROSS]="[${COLOR[RED_LIGHT]}✘${COLOR[STOP]}]";


# Write_Script.
#	DESCRIPTION : custom output of the APP.
#	RETURN : string.
function Write_Script() {
	# Some definition.
	local var_text;
	local var_code;
	local var_check;
	local var_checked;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-CODE)
				var_code="$2"
				shift 2
				;;
			-CHECK)
				var_check=true
				shift
				;;
			-CHECKED)
				var_checked=true
				shift
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	if [[ "${PARAM[QUIET]}" != true ]]; then 
		# Tests : state, change former line.
		if [ -n "$var_check" ]; then
			echo -ne "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		elif [ -n "$var_checked" ]; then
			if [[ "$var_code" -eq 1 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[CROSS]} ERROR : $var_text";
			elif [[ "$var_code" -eq 0 ]]; then
				echo -e "\\r${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: ${COLOR[TICK]} $var_text";
			fi;
		else
			echo -e "${COLOR[ORANGE_DARK]}${APP[SHORTNAME]}${COLOR[STOP]} :: $var_text";
		fi;
	fi;
}


# Get_Help.
#	DESCRIPTION : usage of the APP, in the APP.
#	RETURN : string.
function Get_Help() {
	# requirements.
	local var_requirements;
	local var_requirement;
	local var_requirement_type;
	local var_requirement_name;
	local var_requirement_state;
	local var_requirements_text;
	# Test : array exists.
	if [[ -n "${APP[REQUIREMENTS]}" && "${APP[REQUIREMENTS]}" != "null" ]]; then
		IFS='#' read -ra var_requirements <<< "${APP[REQUIREMENTS]}";
		# Loop : valuelist requirements.
		for var_requirement in "${var_requirements[@]}"; do
			var_requirement_type="$(echo "$var_requirement" | cut -d ' ' -f1)";
			var_requirement_name="$(echo "$var_requirement" | cut -d ' ' -f2)";
			var_requirement_state="$(echo "$var_requirement" | cut -d ' ' -f3)";
			var_requirements_text+="$(echo -e "\t${var_requirement_type} '${var_requirement_name}' (${var_requirement_state})\n\n ")";
		done;
	else
		var_requirements_text+="\tx\n\n ";
	fi;
	# synopsis + options.
	local i;
	local var_key;
	local var_option;
	local var_option_name;
	local var_option_quick;
	local var_option_type;
	local var_option_state;
	local var_option_dsc;
	local var_option_dsc_def;
	local var_option_dsc_val_ok;
	local var_option_dsc_val_default;
	local var_synopsis_text_mandatory;
	local var_synopsis_text;
	local var_options_text;
	var_synopsis_text_mandatory=false;
	# Loop : valuelist options array inside APP.
	for i in $(echo "${!OPTIONS_SORT[@]}" | tr ' ' '\n' | sort -n); do
		var_key="${OPTIONS_SORT[$i]}";
		var_option="${OPTIONS[$var_key]}";
		var_option_name="$(echo "$var_key" | tr '[:upper:]' '[:lower:]'| sed 's/_/-/g')";
		var_option_quick="$(echo "$var_option" | cut -d '#' -f2)";
		var_option_type="$(echo "$var_option" | cut -d '#' -f3)";
		var_option_state="$(echo "$var_option" | cut -d '#' -f4)";
		var_option_dsc="$(echo "$var_option" | cut -d '#' -f5)";
		var_option_dsc_def="$(echo "$var_option_dsc" | cut -d '|' -f1)";
		var_option_dsc_val_ok="$(echo "$var_option_dsc" | cut -d '|' -f2)";
		var_option_dsc_val_default="$(echo "$var_option_dsc" | cut -d '|' -f3)";
		# Test : mandatory OPTION.
		if [[ "$var_option_state" == "mandatory" ]]; then var_synopsis_text_mandatory=true; fi;
		# Test : command text.
		if [[ "$var_option_quick" == "null" ]]; then
			var_options_text+="$(echo -e "\t--${var_option_name} (no arg) ")";
		else
			# Test : command type (underline).
			if [[ "$var_option_type" == "null" ]]; then
				var_options_text+="$(echo -e "\t-${var_option_quick} (no type),\n\t--${var_option_name} (no type)\n ")";
			else
				var_options_text+="$(echo -e "\t-${var_option_quick} ${var_option_type},\n\t--${var_option_name} ${var_option_type}\n ")";
			fi;
		fi;
		var_options_text+="$(echo -e "\t\t${var_option_dsc_def}\n ")";
		var_options_text+="$(echo -e "\t\t- state : ${var_option_state}.\n ")";
		# Test : dsc redefine.
		if [[ "$var_option_dsc_val_ok" == "null" ]]; then var_option_dsc_val_ok="nothing."; fi;
		var_options_text+="$(echo -e "\t\t- accepted value(s) : ${var_option_dsc_val_ok}\n ")";
		var_options_text+="$(echo -e "\t\t- default value : ${var_option_dsc_val_default}\n\n ")";
	done;
	# Test : [] for OPTIONS if only one OPTION is mandatory.
	if [[ "$var_synopsis_text_mandatory" == true ]]; then var_synopsis_text="OPTIONS"; else var_synopsis_text="[ OPTIONS ]"; fi;
	# examples.
	local var_examples;
	local var_example;
	local var_example_dsc;
	local var_example_cmd;
	local var_examples_text;
	IFS='#' read -ra var_examples <<< "${APP[EXAMPLES]}";
	# Loop : valuelist examples.
	for var_example in "${var_examples[@]}"; do
		var_example_dsc="$(echo "$var_example" | cut -d '|' -f1)";
		var_example_cmd="$(echo "$var_example" | cut -d '|' -f2)";
		var_examples_text+="$(echo -e "\t${var_example_dsc}\n\t\t${var_example_cmd}\n\n ")";
	done;
	# todo.
	if [[ -z "${APP[TODO]}" || "${APP[TODO]}" == "null" ]]; then APP["TODO"]="x"; fi;
	# notes.
	local var_notes;
	local var_note;
	local var_notes_text;
	if [[ -n "${APP[NOTES]}" && "${APP[NOTES]}" != "null" ]]; then
		IFS='#' read -ra var_notes <<< "${APP[NOTES]}";
		# Loop : valuelist notes.
		for var_note in "${var_notes[@]}"; do
			var_notes_text+="$(echo -e "\t${var_note}\n\n ")";
		done;
	else
		var_notes_text+="\tx\n\n ";
	fi;
	# credits.
	if [[ -z "${APP[CREDITS]}" || "${APP[CREDITS]}" == "null" ]]; then APP["CREDITS"]="x"; fi;
	# changelog.
	local var_changelog;
	local var_change;
	local var_change_version;
	local var_change_date;
	local var_change_dsc;
	local var_changelog_text;
	IFS='#' read -ra var_changelog <<< "${APP[CHANGELOG]}";
	# Loop : valuelist changelog.
	for var_change in "${var_changelog[@]}"; do
		var_change_version="$(echo "$var_change" | cut -d '|' -f1)";
		var_change_date="$(LC_TIME=en_US.UTF-8 date -d "$(echo "$var_change" | cut -d '|' -f2)" "+%Y-%m-%d")";
		var_change_dsc="$(echo "$var_change" | cut -d '|' -f3)";
		var_changelog_text+="$(echo -e "\t${var_change_version} - ${var_change_date}\n\t\t${var_change_dsc}\n\n ")";
	done;
	Restore_IFS;
	echo -e "\nNAME\n\n\t${APP[NAME]} - ${APP[SLOGAN]}\n\n\nREQUIREMENTS\n\n${var_requirements_text}\nSYNOPSIS\n\n\t${APP[NAME]} ${var_synopsis_text}\n\n\nDESCRIPTION\n\n\t${APP[DESCRIPTION]}\n\n\nOPTIONS\n\n${var_options_text}\nEXAMPLES\n\n${var_examples_text}\nTO-DO\n\n\t${APP[TODO]}\n\n\nNOTES\n\n${var_notes_text}\nCREDITS & THANKS\n\n\t${APP[CREDITS]}\n\n\nCHANGELOG\n\n${var_changelog_text}\nMETADATAS\n\n\tAuthor : ${APP[AUTHOR]}\n\tWebsite : ${APP[URL]}\n\tVersion : ${APP[VERSION]}\n\tLicense : ${APP[LICENSE]}\n\n";
}


# Test_Options.
#	DESCRIPTION : global test for search missing options.
#	RETURN : string.
function Test_Options() {
	# Some definition.
	local var_options_key;
	local var_options_values;
	local var_options_value_name;
	local var_options_value_type;
	local var_options_value_state;
	declare -a var_options_missing;
	local var_options_missing_list;
	# Some init.
	var_options_missing=();
	# Test : data exists.
	if [ ${#OPTIONS[@]} -gt 0 ]; then
		# Loop : indexlist options.
		for var_options_key in "${!OPTIONS[@]}"; do
			var_options_values="${OPTIONS[$var_options_key]}";
			var_options_value_name="$(echo "$var_options_key" | tr '[:upper:]' '[:lower:]')";
			var_options_value_type="$(echo "$var_options_values" | cut -d '#' -f3)";
			var_options_value_state="$(echo "$var_options_values" | cut -d '#' -f4)";
			# Test : trigger warning.
			if [ "$var_options_value_state" == "mandatory" ]; then
					# Test : trigger array.
					if [[ -z "${PARAM[$var_options_key]}" ]]; then
						var_options_missing+=("${var_options_value_name}");
					fi;
			elif [ "$var_options_value_state" != "optionnal" ]; then
				# Test : option with boolean don't need parameter.
				if [ "$var_options_value_type" != "null" ]; then
					Stop_App -CODE 1 -TEXT "Option '--${var_options_value_name}' type is empty, please report the issue.";
				fi;
			fi;
		done;
		# Test : missing exists.
		if [ ${#var_options_missing[@]} -gt 0 ]; then
			# Loop : indexlist missing.
			for i in "${!var_options_missing[@]}"; do
				var_options_missing[i]="--${var_options_missing[i]}";
			done;
			# Test : single, multiple data.
			if [ ${#var_options_missing[@]} -eq 1 ]; then
				Stop_App -CODE 1 -TEXT "Need '${var_options_missing[0]}' option. Type '--help' for read the cute manual.";
			else
				var_options_missing_list="${var_options_missing[*]}";
				var_options_missing_list="${var_options_missing_list// /, }";
				Stop_App -CODE 1 -TEXT "Need '$var_options_missing_list' options. Type '--help' for read the cute manual.";
			fi;
		fi;
	else
		Stop_App -CODE 1 -TEXT "No option provided, please report the issue.";
	fi;
}


# Test_Param.
#	DESCRIPTION : single test for checking parameter (argument) of one option.
#	RETURN : string.
function Test_Param() {
	# Some definition.
	local var_option;
	local var_param;
	local var_type;
	local var_contains;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-OPTION)
				var_option="$2"
				shift 2
				;;
			-PARAM)
				var_param="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-CONTAINS)
				var_contains="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Tests : param mispelled, empty, strict type restriction.
	if [[ "$var_param" == "-"* ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't starting by '-'.";
	elif [[ -z "$var_param" ]]; then
		Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't be empty.";
	elif [[ -n "$var_type" ]]; then
		if [[ "$var_type" == "char" ]]; then
			if [[ "$var_param" =~ [0-9] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have integer.";
			fi;
		elif [[ "$var_type" == "int" ]]; then
			if [[ "$var_param" =~ [a-zA-Z] ]]; then
				Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' can't have character.";
			fi;
		fi;
	fi;
	# Test : character present.
	if [[ -n "$var_contains" ]]; then
		if [[ "$var_param" != *"${var_contains}"* ]]; then
			Stop_App  -CODE 1 -TEXT "Parameter '${var_option}' don't contains '${var_contains}' character.";
		fi;
	fi;
}


# Get_StartingApp.
#	DESCRIPTION : script starter pack.
#	RETURN : string.
function Get_StartingApp() {
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Title.
		echo -e "${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: ${DATE[DATETIME3]} ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
		# Place & soft env.
		echo -e "- work from : '${CONF[PATH]}'.";
		if [ -n "$SNAP" ]; then
			echo -e "- Snapcraft environment detected.";
		elif [ -n "$FLATPAK_ID" ]; then
			echo -e "- Flatpak environment detected.";
		fi;
	fi;
	# Test : requirements.
	if [[ -n "${APP[REQUIREMENTS]}" && "${APP[REQUIREMENTS]}" != "null" ]]; then
		# Some definition.
		local var_requirements;
		local var_requirement;
		local var_element;
		local var_type;
		local var_name;
		local var_dependencie;
		local var_software;
		# Indexed array.
		local var_requirement_warn=();
		local var_requirement_warn_element;
		local var_requirement_warn_show=();
		local var_requirement_warn_show_formatted;
		local var_requirement_nok=();
		local var_requirement_nok_element;
		local var_requirement_nok_show=();
		local var_requirement_nok_show_formatted;
		# Auto-discovery from APP book.
		IFS='#' read -ra var_requirements <<< "${APP[REQUIREMENTS]}";
		Restore_IFS;
		# Loop : valuelist requirements.
		for var_requirement in "${var_requirements[@]}"; do
			IFS=' ' read -ra var_element <<< "$var_requirement";
			Restore_IFS;
			var_type="${var_element[0]}";
			var_name="${var_element[1]}";
			var_dependencie="${var_element[2]}";
			var_software=$(Test_Software -NAME "$var_name" -TYPE "$var_type");
			# Test : software.
			if [[ "$var_software" != "available" ]]; then
				# Test : dependencie.
				if [[ "$var_dependencie" == "mandatory" ]]; then
					var_requirement_nok+=("${var_type}#${var_name}#${var_software}");
				else
					var_requirement_warn+=("${var_type}#${var_name}#${var_software}");
				fi;
			fi;
		done;
		# Tests : ok, warn, nok.
		if [[ "${#var_requirement_warn[@]}" -eq 0 && "${#var_requirement_nok[@]}" -eq 0 ]]; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- software requirements : all dependencie(s) available.";
			fi;
		else
			if [[ "${#var_requirement_warn[@]}" -ge 1 && "${#var_requirement_nok[@]}" -eq 0 ]]; then
				# Loop : valuelist requirements warn.
				for var_requirement_warn_element in "${var_requirement_warn[@]}"; do
					IFS='#' read -ra var_requirement_warn_element_spec <<< "$var_requirement_warn_element";
					Restore_IFS;
					var_requirement_warn_show+=("${var_requirement_warn_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_warn_show_formatted=$(IFS=', '; echo "${var_requirement_warn_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[ORANGE_LIGHT]}${var_requirement_warn_show_formatted}${COLOR[STOP]} are required but optionnal.";
				fi;
			elif [[ "${#var_requirement_warn[@]}" -ge 1 && "${#var_requirement_nok[@]}" -ge 1 ]]; then
				# Loop : valuelist requirements warn.
				for var_requirement_warn_element in "${var_requirement_warn[@]}"; do
					IFS='#' read -ra var_requirement_warn_element_spec <<< "$var_requirement_warn_element";
					Restore_IFS;
					var_requirement_warn_show+=("${var_requirement_warn_element_spec[1]}");
				done;
				# Loop : valuelist requirements nok.
				for var_requirement_nok_element in "${var_requirement_nok[@]}"; do
					IFS='#' read -ra var_requirement_nok_element_spec <<< "$var_requirement_nok_element";
					Restore_IFS;
					var_requirement_nok_show+=("${var_requirement_nok_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_warn_show_formatted=$(IFS=', '; echo "${var_requirement_warn_show[*]}");
				var_requirement_nok_show_formatted=$(IFS=', '; echo "${var_requirement_nok_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[RED_LIGHT]}${var_requirement_nok_show_formatted}${COLOR[STOP]} are required and mandatory - ${COLOR[ORANGE_LIGHT]}${var_requirement_warn_show_formatted}${COLOR[STOP]} are required but optionnal.";
				fi;
				Stop_App -CODE 1 -TEXT "Need to fix dependencies.";
			elif [[ "${#var_requirement_warn[@]}" -eq 0 && "${#var_requirement_nok[@]}" -ge 1 ]]; then
				# Loop : valuelist requirements nok.
				for var_requirement_nok_element in "${var_requirement_nok[@]}"; do
					IFS='#' read -ra var_requirement_nok_element_spec <<< "$var_requirement_nok_element";
					Restore_IFS;
					var_requirement_nok_show+=("${var_requirement_nok_element_spec[1]}");
				done;
				# Comma-separated values.
				var_requirement_nok_show_formatted=$(IFS=', '; echo "${var_requirement_nok_show[*]}");
				Restore_IFS;
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- software requirements : ${COLOR[RED_LIGHT]}${var_requirement_nok_show_formatted}${COLOR[STOP]} are required and mandatory.";
				fi;
				Stop_App -CODE 1 -TEXT "Need to fix dependencies.";
			fi;
		fi;
	fi;
	# Test : output (data backup).
	if [[ "${PARAM[BACKUP]}" == true ]]; then
		# Some definition.
		local var_naming;
		# Define naming mode based on output definition.
		if [[ "${PARAM[OUTPUT]}" =~ -${DATE[DATETIME1]}\. ]]; then var_naming="date"; else var_naming="custom"; fi;
		# Test : output mode.
		if [[ "$var_naming" == "date" ]]; then
			# Test : get previous output old.
			if [[ -n "${PARAM[OUTPUT_BACKUP]}" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT_BACKUP]}"; then
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) in '${PARAM[OUTPUT_BACKUP]}' (date name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (date name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (date name mode).";
				fi;
			fi;
		else
			# Test : move previous output to old.
			if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" ]]; then
				if Test_File -PATH "${PARAM[OUTPUT]}"; then
					mv "${PARAM[OUTPUT]}" "${PARAM[OUTPUT_BACKUP]}";
					# TMPSOLUTION.
					# Test : move all file formats.
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.json"; then
						mv "${PARAM[OUTPUT]%.csv}.json" "${PARAM[OUTPUT_BACKUP]%.csv}.json";
					fi;
					if Test_File -PATH  "${PARAM[OUTPUT]%.csv}.yaml"; then
						mv "${PARAM[OUTPUT]%.csv}.yaml" "${PARAM[OUTPUT_BACKUP]%.csv}.yaml";
					fi;
					# /TMPSOLUTION.
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- previous output (data backup) moved to '${PARAM[OUTPUT_BACKUP]}' (custom name mode).";
					fi;
				else
					if [[ "${PARAM[QUIET]}" != true ]]; then
						echo -e "- data backup enable without previous data (custom name mode).";
					fi;
				fi;
			else
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- data backup enable but impossible to search previous data (custom name mode).";
				fi;
			fi;
		fi;
	fi;
	# Test : output (main).
	if [[ -n "${PARAM[OUTPUT]}" && "${PARAM[OUTPUT]}" != "" && "${PARAM[OUTPUT]}" != "null" && "${PARAM[RAW]}" != true ]]; then
		# Tests : folder, file.
		if [[ "${PARAM["OUTPUT"]}" =~ /$ ]]; then
			if Test_Dir -PATH "${PARAM[OUTPUT]}" -CREATE; then
				if [[ "${PARAM[QUIET]}" != true ]]; then
					echo -e "- current output to folder '${PARAM[OUTPUT]}'.";
				fi;
			else
				Stop_App -CODE 1 -TEXT "Folder output '${PARAM[OUTPUT]}' can't be writed.";
			fi;
		elif Test_File -PATH "${PARAM[OUTPUT]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current output to file '${PARAM[OUTPUT]}'.";
			fi;
		else
			Stop_App -CODE 1 -TEXT "File output '${PARAM[OUTPUT]}' can't be writed.";
		fi;
	fi;
	# Test : logging.
	if [[ "$(declare -p LOG 2>/dev/null)" =~ declare\ -A\ LOG ]] && [[ "${#LOG[@]}" -gt 0 && -n "${LOG[PATH]}" ]]; then
		# Some definition.
		local var_log;
		# Test : Logging path ('/var/log' for sure ...).
		if Test_Dir -PATH "${LOG[PATH]}" -CREATE; then
			if [[ "${PARAM[QUIET]}" != true ]]; then
				echo -e "- current log to folder '${LOG[PATH]}'.";
			fi;
			# Loop : valuelist logging files.
			for var_log in "${!LOG[@]}"; do
				if [[ "$var_log" != "PATH" ]]; then
					Test_File -PATH "${LOG[$var_log]}" -CREATE;
				fi;
			done;
		else
			Stop_App -CODE 1 -TEXT "Folder output '${LOG[PATH]}' can't be writed.";
		fi;
	fi;
	# Test : credential.
	if [[ -n "${PARAM[CREDENTIAL]}" && "${PARAM[CREDENTIAL]}" != "null" && "${PARAM[QUIET]}" != true ]]; then
		# Some definition.
		local var_credential;
		local var_software;
		# Indexed array.
		var_credential_show=();
		local var_credential_show_formatted;
		# Loop : indexlist credential. 
		for var_credential in "${!CREDENTIAL[@]}"; do
			var_credential_show+=("$var_credential");
		done;
		# Comma-separated values.
		var_credential_show_formatted=$(IFS=', '; echo "${var_credential_show[*]}");
		Restore_IFS;
		echo -e "- credential(s) used for : ${var_credential_show_formatted}.";
	fi;
	if [[ "${PARAM[QUIET]}" != true ]]; then echo ""; fi;
}


# Get_EndingApp.
#	DESCRIPTION : script ending pack.
#	RETURN : string.
function Get_EndingApp() {
	# Some definition.
	local var_date_start;
	local var_date_end;
	local var_date_end_seconds;
	if [[ "${PARAM[QUIET]}" != true ]]; then
		# Some init;
		var_date_start="${DATE["TIMESTAMP0"]}";
		var_date_end="$(date "+%s%3N")";
		var_date_end_seconds=$(Get_DateDiff -START "$var_date_start" -END "$var_date_end");
		echo -e "\n${COLOR[YELLOW_DARK]}[${COLOR[STOP]} ${COLOR[ORANGE_DARK]}${APP[NAME]}${COLOR[STOP]} :: executed in ${var_date_end_seconds} seconds ${COLOR[YELLOW_DARK]}]${COLOR[STOP]}";
	fi;
}


# Stop_App.
#	DESCRIPTION : end program.
#	RETURN : string and code.
function Stop_App() {
	# Some definition.
	local var_code;
	local var_text;
	local var_quiet;
	local var_help;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-CODE)
				var_code="$2"
				shift 2
				;;
			-TEXT)
				var_text="$2"
				shift 2
				;;
			-QUIET)
				var_quiet=true
				shift
				;;
			-HELP)
				var_help=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Test : splash error.
	if [[ "$var_quiet" != true && -n "$var_text" ]]; then
		Write_Script -TEXT "EXIT ERROR ! ${var_text}";
	fi;
	# Test : splash man.
	if [[ "$var_help" == true ]]; then Get_Help; fi;
	wait;
	exit "$var_code";
}


# Debug_Start.
#	DESCRIPTION : debug app.
function Debug_Start() { set -x; }


# Debug_Stop.
#	DESCRIPTION : debug app.
function Debug_Stop() { set +x; }


# Get_TableHeader.
#	DESCRIPTION : display a table index.
#	RETURN : string.
function Get_TableHeader() {
	# Some definition.
	local var_array;
	local var_element;
	local var_param;
	local var_param_length;
	local var_dot_length;
	local var_line1;
	local var_line2;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a name param and the max character count a value of this param can be in the rows.
		for element_header in "${!var_array[@]}"; do
			var_element="${var_array[$element_header]}";
			var_param="$(echo "$var_element" | cut -d ':' -f1)";
			var_param_length="${#var_param}";
			var_dot_length="$(echo "$var_element" | cut -d ':' -f2)";
			# Calculate how many characters to add more.
			var_param_length_more=$((var_dot_length - var_param_length));
			var_dot_length_more=$((var_param_length - var_dot_length));
			# Ready to construct header.
			var_line1+="$var_param";
			var_line1+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_param_length_more");
			var_line1+="  ";
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length");
			var_line2+=$(Set_RepeatCharacter -CHARACTER "-" -COUNT "$var_dot_length_more");
			var_line2+="  ";
		done;
		echo -e "$var_line1";
		echo -e "$var_line2";
	fi;
}


# Get_TableRow.
#	DESCRIPTION : display a table row.
#	RETURN : string.
function Get_TableRow() {
	# Some definition.
	local var_array;
	local var_action;
	local var_element;
	local var_value;
	local var_value_length;
	local var_max;
	local var_line;
	# Loop : arguments.
	while [ "$#" -gt 0 ]; do
		case "$1" in
			-ARRAY)
				IFS=' ' read -ra var_array <<< "$2"
				shift 2
				;;
			-ACTION)
				var_action="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	Restore_IFS;
	# Test : if array empty.
	if [ ${#var_array[@]} -eq 0 ]; then
		return 1;
	else
		# Loop : element is compounded by a value, the max character count this value can be.
		for element_row in "${!var_array[@]}"; do
			var_element="${var_array[$element_row]}";
			var_value="$(echo "$var_element" | cut -d ':' -f1)";
			var_max="$(echo "$var_element" | cut -d ':' -f2)";
			# Test : tick cross.
			if [[ "$var_value" == "tick" || "$var_value" == "cross" ]]; then
				if [[ "$var_value" == "tick" ]]; then
					var_value="${COLOR[TICK]}";
				elif [[ "$var_value" == "cross" ]]; then
					var_value="${COLOR[CROSS]}";
				fi;
				var_value_length=3;
			# TODO
			# elif [[ "$var_value" == *"tick"* || "$var_value" == *"cross"* ]]; then
			else
				var_value_length="${#var_value}";
			fi;
			# Calculate how many space to add more.
			var_value_length_more=$((var_max - var_value_length));
			# Ready to construct row.
			var_line+="$var_value";
			var_line+=$(Set_RepeatCharacter -CHARACTER " " -COUNT "$var_value_length_more");
			var_line+="  ";
		done;
		# Test : replace line.
		if [[ "$var_action" == "check" ]]; then
			echo -ne "$var_line";
		else
			echo -e "\\r$var_line";
		fi;
	fi;
}