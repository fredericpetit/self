#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_user.sh               | #
# |__________________________________________| #


# ====================== #
#          USER          #
#
#	DESCRIPTION : user account (system & ldap) interacting for all APPs.


# Test_Root.
#	DESCRIPTION : check root rights.
#	RETURN : code.
function Test_Root {
	if [[ "$EUID" -eq 0 ]]; then return 0; else return 1; fi;
}