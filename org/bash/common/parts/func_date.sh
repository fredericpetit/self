#!/bin/bash
# shellcheck source=/dev/null
#  __________________________________________
# |                                          | #
# |               func_date.sh               | #
# |__________________________________________| #


# ====================== #
#          DATE          #
#
#	DESCRIPTION : pre-defined working date.


# DATE.
declare -gA DATE;
DATE["CAPTURE"]="$(date "+%s%3N")";
# ex EXTRA_TIMESTAMP : execution time diff calc (timestamp in milliseconds).
DATE["TIMESTAMP0"]="${DATE[CAPTURE]}";
# ex CURRENT_DATE : jekyll file name markdown, manuel page.
DATE["DATE0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d")";
# distro files JSON & YAML.
DATE["DATETIME0"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%dT%H:%M:%S%z")";
# ex COMPACT_TIMESTAMP & DATE_FILE : output filenames.
DATE["DATETIME1"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y%m%d_%H%M%S")";
# ex FULL_TIMESTAMP & DATE_START_EXEC : page.date for jekyll inside markdown file, added date inside join-files output.
DATE["DATETIME2"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%Y-%m-%d %H:%M:%S")";
# ex FORMATTED_DATE_TIME & DATE_START : bash execution time.
DATE["DATETIME3"]="$(date -d "@${DATE[CAPTURE]:0:10}" "+%d/%m/%Y - %Hh%M")";


# Get_DateDiff.
#	DESCRIPTION : diff calc in seconds & milliseconds.
#	RETURN : string.
function Get_DateDiff() {
	# Some definition.
	local var_start;
	local var_start_sec;
	local var_start_msec;
	local var_end;
	local var_end_msec;
	local var_end_sec;
	local var_diff_sec;
	local var_diff_msec;
	local var_msec_2_digits;
	local var_result;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-START)
				var_start="$2"
				shift 2
				;;
			-END)
				var_end="$2"
				shift 2
				;;
			*)
				shift
				Stop_App -CODE 1
				;;
		esac
	done;
	# Extract seconds & milliseconds.
	var_start_sec="${var_start:0:10}";
	var_start_msec="${var_start:10:3}";
	var_end_sec="${var_end:0:10}";
	var_end_msec="${var_end:10:3}";
	# Remove leading zeros to avoid octal interpretation.
	var_start_sec=$((10#$var_start_sec));
	var_start_msec=$((10#$var_start_msec));
	var_end_sec=$((10#$var_end_sec));
	var_end_msec=$((10#$var_end_msec));
	# Diff calc.
	var_diff_sec=$((var_end_sec - var_start_sec));
	var_diff_msec=$((var_end_msec - var_start_msec));
	# Test : milliseconds diff is negative.
	if [ $var_diff_msec -lt 0 ]; then
		var_diff_sec=$((var_diff_sec - 1));
		var_diff_msec=$((var_diff_msec + 1000));
	fi;
	# Test : milliseconds with two digits.
	if [ $var_diff_msec -lt 10 ]; then
		var_msec_2_digits="0$((var_diff_msec / 10))";
	else
		var_msec_2_digits=$((var_diff_msec / 10));
	fi;
	var_result="${var_diff_sec}.${var_msec_2_digits}";
	echo "$var_result";
}