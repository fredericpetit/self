#!/bin/bash
# shellcheck source=/dev/null
#  _____________________________________________
# |                                             | #
# |               func_network.sh               | #
# |_____________________________________________| #


# ========================= #
#          NETWORK          #
#
#	DESCRIPTION : network state for all APPs.

# Get_FirstNetwork.
#	DESCRIPTION : get name of the first network interface.
#	RETURN : string or code.
function Get_FirstNetwork() {
	# Some definition.
	local var_result;
	local var_interfaces;
	var_result="fail-network";
	var_interfaces=$(ip -o link show | awk -F': ' '{print $2}');
	# Loop : interfaces.
	for interface in $var_interfaces; do
		if [[ $interface != "lo" ]]; then var_result="$interface"; break; fi;
	done
	echo "$var_result";
}


# Get_Distant.
#	DESCRIPTION : get distant resource - file, string, ping, git repository, with auth available.
#	RETURN : string or code.
#	TO-DO : git auth.
function Get_Distant() {
	# Some definition - menu.
	local var_method;
	local var_type;
	local var_url;
	local var_path;
	local var_credential;
	local var_username;
	local var_password;
	local var_overwrite;
	local var_log;
	local var_verbose;
	# Some definition - next.
	local var_result;
	# Loop : arguments.
	while [[ $# -gt 0 ]]; do
		case "$1" in
			-METHOD)
				var_method="$2"
				shift 2
				;;
			-TYPE)
				var_type="$2"
				shift 2
				;;
			-URL)
				var_url="$2"
				shift 2
				;;
			-PATH)
				var_path="$2"
				shift 2
				;;
			-CREDENTIAL)
				var_credential="$2"
				shift 2
				;;
			-OVERWRITE)
				var_overwrite=true
				shift
				;;
			-LOG)
				var_log="$2"
				shift 2
				;;
			-VERBOSE)
				var_verbose=true
				shift
				;;
			*)
				shift
				;;
		esac
	done;
	# Some default.
	if [[ -z "$var_method" ]]; then var_method="wget"; fi;
	if [[ -z "$var_type" ]]; then var_type="file"; fi;
	# Test : auth.
	if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" && "$var_credential" == *":"* ]]; then
		var_username=$(echo -e "$var_credential" | cut -d ':' -f 1);
		var_password=$(echo -e "$var_credential" | cut -d ':' -f 2);
	fi;
	# Tests : method.
	if [[ "$var_method" == "wget" ]]; then
		# Tests : file, string, ping.
		if [[ "$var_type" == "file" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if [[ -n "$var_verbose" ]]; then
						wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				else
					if [[ -n "$var_verbose" ]]; then
						wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log"
							if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if [[ -n "$var_verbose" ]]; then
						wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				else
					if [[ -n "$var_verbose" ]]; then
						wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path";
					elif [[ -n "$var_log" ]]; then
						# Test : log exists.
						if Test_File -PATH "$var_log"; then
							echo -e "####################\n[${DATE[DATETIME2]}] [INFO] DOWNLOADER :" >> "$var_log";
							if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 "$var_url" -O "$var_path" &>> "$var_log"; then return 0; else return 1; fi;
						else
							return 1;
						fi;
					else
						if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q "$var_url" -O "$var_path" &> /dev/null; then return 0; else return 1; fi;
					fi;
				fi;
			fi;
		elif [[ "$var_type" == "string" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 --user "$var_username" --password "$var_password" -q -O- "$var_url" | tr -d '\n');
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					var_result=$(wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				else
					var_result=$(wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q -O- "$var_url" | tr -d '\n');
				fi;
			fi;
			# Test : good content.
			if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
		elif [[ "$var_type" == "ping" ]]; then
			# Test : auth.
			if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --user "$var_username" --password "$var_password" --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			else
				if [[ -n "$SNAP" || -n "$FLATPAK_ID" ]]; then
					if wget --no-config --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				else
					if wget --no-check-certificate --no-use-server-timestamps --timeout=10 --tries=2 -q --spider "$var_url" &> /dev/null; then return 0; else return 1; fi;
				fi;
			fi;
		fi;
	elif [[ "$var_method" == "curl" ]]; then
		# Test : auth.
		if [[ -n "$var_credential" && "$var_credential" != "" && "$var_credential" != "null" ]]; then
			var_result=$(curl -s -H "Authorization: token ${var_credential}" "$var_url" | tr -d '\n' | sed 's/ //g');
		else
			var_result=$(curl -s "$var_url" | tr -d '\n' | sed 's/ //g');
		fi;
		# Test : good content.
		if [[ -n "$var_result" ]]; then echo "$var_result"; else return 1; fi;
	elif [[ "$var_method" == "git" ]]; then
		# Test : replace.
		if [[ "$var_overwrite" = true ]]; then
			if git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; then return 0; else return 1; fi;
		else
			git clone --depth=1 "$var_url" "$var_path" --quiet &> /dev/null; return 0;
		fi;
	fi;
}