#!/bin/bash
# shellcheck source=/dev/null
#  ___________________________________________
# |                                           | #
# |               join-files.sh               | #
# |___________________________________________| #


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

if ! declare -p CONF 2>/dev/null | grep -q 'declare -A'; then declare -gA CONF; fi;
CONF["DEBUG"]=false;
CONF["SEPARATOR"]="/";
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";
# Test : includes only for dev work.
if [[ "${0##*.}" == "sh" && "${CONF["PATH"]}" =~ /core$ ]]; then
	CONF["PATH_COMMON"]="$(dirname "$(dirname "$(dirname "${CONF["PATH"]}")")")${CONF[SEPARATOR]}common${CONF[SEPARATOR]}parts${CONF[SEPARATOR]}";
	CONF["CONTINUE"]=false;
	# Loop : using while for performance reasons.
	while IFS= read -r var_file; do
		source "$var_file" && CONF["CONTINUE"]=true;
	done < <(find "${CONF[PATH_COMMON]}" -type f -regextype posix-extended -regex ".*/func_[^/]+\.sh$" 2>/dev/null);
	Restore_IFS;
	[[ "${CONF[CONTINUE]}" == false ]] && { echo "Check source code, program needs common functions."; exit 1; };
fi;


# ============================= #
#          APPLICATION          #
#
#	DESCRIPTION : everything that defines the APP.

declare -gA APP;
APP["NAME"]="join-files";
APP["SECTION"]="1";
APP["SHORTNAME"]="JF";
APP["SLOGAN"]="Join some files into single file.";
APP["DESCRIPTION"]="This open-source tool generates a new file from some files.";
APP["REQUIREMENTS"]="null";
APP["EXAMPLES"]="join two Bash scripts :|${APP[NAME]} -s func1.sh,func2.sh -o funcs.sh";
APP["TODO"]="null";
APP["NOTES"]="null";
APP["CREDITS"]="null";
APP["CHANGELOG"]="1.0.0|2024-09-01T10:00:00+0200|New documentation. Option '--source' instead '--files'. Lowercase all options.#0.0.2|2024-06-01T10:00:00+0200|Add Test_File security.#0.0.1|2024-01-01T10:00:00+0200|Init.";
APP["AUTHOR"]="Frédéric Petit <contact@fredericpetit.fr>";
APP["URL"]="https://gitlab.com/fredericpetit/self/";
APP["VERSION"]="1.0.0";
APP["LICENSE"]="GPL-3.0-or-later";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

declare -gA OPTIONS;
OPTIONS["SOURCE"]="1#s#FILEPATHS#mandatory#Multiple local file paths of files to join, in coma separated list.|paths.|no default.";
OPTIONS["PATTERN"]="2#p#CODE#optionnal#Interpreter pattern to remove inside files after the first joined.|some string.|bash shebang.";
OPTIONS["OUTPUT"]="3#o#FILEPATH#optionnal#Single local file path to store the final result.|path.|'PWD/${APP[SHORTNAME]}-TIMESTAMP.out'.";
OPTIONS["QUIET"]="4#q#null#optionnal#Runs the program silently, producing no output on the screen.|null|'false' because disabled.";
OPTIONS["HELP"]="5#h#null#optionnal#Show this help screen.|null|'false' because disabled.";
OPTIONS["VERSION"]="6#v#null#optionnal#Show current version of this program.|null|'false' because disabled.";
# Test : sort OPTIONS.
if ! Sort_Array -NAME "OPTIONS" -DELIMITER "#"; then Stop_App -CODE 1 -TEXT "'OPTIONS' array parsing failed."; fi;


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--source|-s)
			PARAM["SOURCE"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--pattern|-p)
			PARAM["PATTERN"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--output|-o)
			PARAM["OUTPUT"]="$2"
			Test_Param -OPTION "$1" -PARAM "$2"
			shift 2
			;;
		--quiet|-q)
			PARAM["QUIET"]=true
			shift
			;;
		--help|-h)
			shift
			Stop_App -CODE 0 -HELP
			;;
		--version|-v)
			shift
			echo "${APP[VERSION]}"
			Stop_App -CODE 0
			;;
		*)
			PARAM["VOID"]="$1"
			shift
			Stop_App -CODE 1 -TEXT "Unknown option : '${PARAM[VOID]}'." -HELP
			;;
	esac
done;
# Test missing options.
Test_Options;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set an array of calling parameter(s) of the script and take default(s) value(s) if needed.

# Some defaults.
declare -gA DEFAULT;
DEFAULT["PATTERN"]="#!/bin/bash";
DEFAULT["OUTPUT"]="${PWD%[\\/]}${CONF[SEPARATOR]}${APP[NAME]}-${DATE[DATETIME1]}.out";
DEFAULT["QUIET"]=false;
# Some script definition.
if [ -z "${PARAM[PATTERN]}" ]; then PARAM["PATTERN"]="${DEFAULT[PATTERN]}"; fi;
if [ -z "${PARAM[OUTPUT]}" ]; then PARAM["OUTPUT"]="${DEFAULT[OUTPUT]}"; else PARAM["OUTPUT"]="${PARAM[OUTPUT]%[\\/]}"; fi;
if [ -z "${PARAM[QUIET]}" ]; then PARAM["QUIET"]="${DEFAULT[QUIET]}"; fi;


# =========================== #
#          FUNCTIONS          #

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Some definition.
	local var_array;
	local var_element;
	local var_count;
	local var_storage;
	local var_storage_firstline;
	local var_date;
	IFS=',' read -ra var_array <<< "${PARAM[SOURCE]}";
	Restore_IFS;
	var_count=1;
	# Test : init output.
	if ! Test_File -PATH "${PARAM[OUTPUT]}" -OVERWRITE; then
		Stop_App -CODE 1 -TEXT "Output can't be writed.";
	fi;
	# Loop : collect in a new file.
	for var_element in "${var_array[@]}"; do
		var_storage="$(<"$var_element")";
		var_storage_firstline="$(head -n 1 "$var_element")";
		var_date="${DATE[DATE0]}";
		# Remove pattern starting the second file.
		if [[ $var_count -eq 1 ]]; then
			if [[ "${PARAM[QUIET]}" != true ]]; then echo "- file #${var_count} '${var_element}' - first file."; fi;
			echo "$var_storage" > "${PARAM[OUTPUT]}";
			sed -i "2i # first content joined - ${var_date}." "${PARAM[OUTPUT]}";
		elif [[ "$var_storage_firstline" == "${PARAM[PATTERN]}" ]]; then
			if [[ "${PARAM[QUIET]}" != true ]]; then echo "- file #${var_count} '${var_element}' - add with pattern removed."; fi;
			echo -e "\n\n\n# next joined content - ${var_date}." >> "${PARAM[OUTPUT]}";
			var_storage="$(tail -n +2 <<< "$var_storage")";
			echo "$var_storage" >> "${PARAM[OUTPUT]}";
		else
			if [[ "${PARAM[QUIET]}" != true ]]; then echo "- file #${var_count} '${var_element}' - simple add."; fi;
			echo -e "\n\n\n# next joined content - ${var_date}." >> "${PARAM[OUTPUT]}";
			echo "$var_storage" >> "${PARAM[OUTPUT]}";
		fi;
		var_count="$((var_count + 1))";
	done;
	# Test : correct output.
	if ! Test_File -PATH "${PARAM[OUTPUT]}"; then
		Stop_App -CODE 1 -TEXT "Output can't be writed.";
	fi;
}


# ======================= #
#          CALLS          #

# Hello.
Get_StartingApp;
# Let's go.
Start_App;
# Goodbye.
Get_EndingApp;