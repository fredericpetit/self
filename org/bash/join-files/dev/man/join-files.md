<blockquote id="out2tag">



NAME



	join-files - Join some files into single file.





REQUIREMENTS



	x



 

SYNOPSIS



	join-files OPTIONS





DESCRIPTION



	This open-source tool generates a new file from some files.





OPTIONS



	-s FILEPATHS,

	--source FILEPATHS

 		Multiple local file paths of files to join, in coma separated list.

 		- state : mandatory.

 		- accepted value(s) : paths.

 		- default value : no default.



 	-p CODE,

	--pattern CODE

 		Interpreter pattern to remove inside files after the first joined.

 		- state : optionnal.

 		- accepted value(s) : some string.

 		- default value : bash shebang.



 	-o FILEPATH,

	--output FILEPATH

 		Single local file path to store the final result.

 		- state : optionnal.

 		- accepted value(s) : path.

 		- default value : 'PWD/JF-TIMESTAMP.out'.



 	-q (no type),

	--quiet (no type)

 		Runs the program silently, producing no output on the screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-h (no type),

	--help (no type)

 		Show this help screen.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 	-v (no type),

	--version (no type)

 		Show current version of this program.

 		- state : optionnal.

 		- accepted value(s) : nothing.

 		- default value : 'false' because disabled.



 

EXAMPLES



	join two Bash scripts :

		join-files -s func1.sh,func2.sh -o funcs.sh



 

TO-DO



	x





NOTES



	x



 

CREDITS & THANKS



	x





CHANGELOG



	1.0.0 - 2024-09-01

		New documentation. Option '--source' instead '--files'. Lowercase all options.



 	0.0.2 - 2024-06-01

		Add Test_File security.



 	0.0.1 - 2024-01-01

		Init.



 

METADATAS



	Author : Frédéric Petit <contact@fredericpetit.fr>

	Website : https://gitlab.com/fredericpetit/self/

	Version : 1.0.0

	License : GPL-3.0-or-later





</blockquote>
