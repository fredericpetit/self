Param (
	[Parameter(Mandatory=$true)][string]$mode,
	[Parameter(Mandatory=$true)][string]$data,
	[string]$path
)

# Test : root rights.
function Check-Admin {
	if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
		Write-Host "Root rights needed." -ForegroundColor Red;
		exit;
	}
}
Check-Admin;

# Test : default paths.
if ($mode -eq 'export') {
	# Tests : where get data.
	if ($data -eq 'choco') {
		$path = $PSScriptRoot + "\data\data-choco.csv";
	} elseif ($data -eq 'vscode') {
		$path = $PSScriptRoot + "\data\data-vscode.csv";
	} elseif ($data -eq 'shortcut') {
		$path = $PSScriptRoot + "\data\data-shortcut.csv";
	} else {
		Write-Host " Need valid 'data'." -ForegroundColor Red;
		exit;
	}
} elseif ($mode -eq 'import') {
	# Tests : where get data.
	if ($data -eq 'choco') {
		$path = "https://gitlab.com/fredericpetit/self/-/raw/main/org/powershell/importexport-choco/data/data-choco.csv";
	} elseif ($data -eq 'vscode') {
		$path = "https://gitlab.com/fredericpetit/self/-/raw/main/org/powershell/importexport-choco/data/data-vscode.csv";
	} elseif ($data -eq 'shortcut') {
		$path = "https://gitlab.com/fredericpetit/self/-/raw/main/org/powershell/importexport-choco/data/data-shortcut.csv";
	} else {
		Write-Host " Need valid 'data'." -ForegroundColor Red;
		exit;
	}
} else {
	Write-Host " Need valid 'mode'." -ForegroundColor Red;
	exit;
}

# Test : create default directory.
$defaultDirectory = "$PSScriptRoot\data"
if (-not (Test-Path $defaultDirectory)) { New-Item -ItemType Directory -Path $defaultDirectory | Out-Null; }

#
#
function Export-Packages {
	$packages = choco list | Select-Object -Skip 1 | Where-Object {$_ -match '\s\d'} | ForEach-Object {
		$elements = $_ -split '\s+';
		# Test : installed packages without last line.
		if ($elements.Count -ge 2) {
			Write-Host "check package : '$($elements[0])'." -ForegroundColor Cyan;
			[PSCustomObject]@{
				Name    = $elements[0]
				Version = $elements[1]
			}
		}
	}
	# Test : data exists.
	if ($packages) {
		$packages | Export-Csv -Path $path -NoTypeInformation -QuoteFields None;
		Write-Host "$($packages.Count) datas exported to '$path'.";
	} else {
		Write-Host "nothing to export." -ForegroundColor Yellow;
	}
}

#
#
function Export-Extensions {
	$extensions = code --list-extensions --show-versions | ForEach-Object {
		$elements = $_ -split '@'
		# Test : installed extensions.
		if ($elements.Count -eq 2) {
			Write-Host "check extension : $($elements[0])" -ForegroundColor Cyan;
			[PSCustomObject]@{
				Name    = $elements[0]
				Version = $elements[1]
			}
		}
	}
	# Test : data exists.
	if ($extensions) {
		$extensions | Export-Csv -Path $path -NoTypeInformation -QuoteFields None;
		Write-Host "$($extensions.Count) datas exported to '$path'.";
	} else {
		Write-Host "nothing to export." -ForegroundColor Yellow;
	}
}

#
#
function Export-Shortcuts {
	$TaskbarFolder = "$env:APPDATA\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar";
	# Test : data exists.
	if (Test-Path $TaskbarFolder) {
		$shortcuts = Get-ChildItem -Path $TaskbarFolder -Filter *.lnk;
		$pinnedApps = $shortcuts | ForEach-Object {
			$wshShell = New-Object -ComObject WScript.Shell
			$link = $wshShell.CreateShortcut($_.FullName)
			# Test : icon.
			$iconLocation = if ($link.IconLocation) {
				$link.IconLocation;
			} else {
				"icon-404";
			}
			Write-Host "pinned app : '$($_.BaseName)'." -ForegroundColor Cyan;
			[PSCustomObject]@{
				Name	= $_.BaseName
				Target	= $link.TargetPath
				Icon	= $iconLocation
			}
		}
		# Test : data exists.
		if ($pinnedApps) {
			# Exporter la liste en CSV avec des guillemets si nécessaire
			$pinnedApps | Export-Csv -Path $path -NoTypeInformation -UseQuotes AsNeeded;
			Write-Host "$($pinnedApps.Count) datas exported to '$path'.";
		} else {
			Write-Host "nothing to export." -ForegroundColor Yellow;
		}
	} else {
        Write-Host "nothing to parse." -ForegroundColor Red
    }
}

#
#
function Import-Packages {
	try {
		# Test : valid URL.
		if ($path -match '^https?://') {
			$tempFile = "$env:TEMP\temp_data.csv";
			Invoke-WebRequest -Uri $path -OutFile $tempFile;
			$path = $tempFile;
		}
		if (Test-Path $path) {
			$packages = Import-Csv -Path $path;
			foreach ($package in $packages) {
				if (-not [string]::IsNullOrWhiteSpace($package.Name)) {
					Write-Host "install package : '$($package.Name)'." -ForegroundColor Cyan;
					choco install $package.Name -y --ignore-checksum;
				} else {
					Write-Host "invalid name : '$($package.Name)'." -ForegroundColor Yellow;
				}
			}
			Write-Host "import from '$path' finished.";
		} else {
			Write-Host "file doesn't exists." -ForegroundColor Red;
		}
	} catch {
		Write-Host "error : $_" -ForegroundColor Red;
	}
}

#
#
function Import-Extensions {
	try {
		if ($path -match '^https?://') {
			$tempFile = "$env:TEMP\temp_data.csv";
			Invoke-WebRequest -Uri $path -OutFile $tempFile;
			$path = $tempFile;
		}
		if (Test-Path $path) {
			$extensions = Import-Csv -Path $path
			foreach ($extension in $extensions) {
				if (-not [string]::IsNullOrWhiteSpace($extension.Name)) {
					Write-Host "install extension : '$($extension.Name)@$($extension.Version)'." -ForegroundColor Cyan;
					code --install-extension "$($extension.Name)@$($extension.Version)";
				} else {
					Write-Host "invalid name : '$($extension.Name)'." -ForegroundColor Yellow;
				}
			}
			Write-Host "import from '$path' finished.";
		} else {
			Write-Host "file doesn't exists." -ForegroundColor Red;
		}
	} catch {
		Write-Host "error : $_" -ForegroundColor Red;
	}
}

# Test : action.
if ($mode -eq 'export') {
	# Tests : where get data.
	if ($data -eq 'choco') {
		Export-Packages;
	} elseif ($data -eq 'vscode') {
		Export-Extensions;
	} elseif ($data -eq 'shortcut') {
		Export-Shortcuts;
	}
} elseif ($mode -eq 'import') {
	# Tests : where get data.
	if ($data -eq 'choco') {
		Import-Packages;
	} elseif ($data -eq 'vscode') {
		Import-Extensions;
	} elseif ($data -eq 'shortcut') {
		Import-Shortcuts;
	}
}