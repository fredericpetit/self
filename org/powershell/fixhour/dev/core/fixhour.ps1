# Vérifier si PowerShell est exécuté en tant qu'administrateur
if (-not ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
	Write-Host "Veuillez exécuter ce script en tant qu'administrateur."
	exit
}

# Définir le fuseau horaire sur Romance Standard Time (Europe/Paris)
$timezone = "Romance Standard Time"  # Utiliser l'identifiant correct pour Paris

# Vérifier si le fuseau horaire est disponible
$availableTimeZones = Get-TimeZone -ListAvailable | Where-Object { $_.Id -eq $timezone }

if ($availableTimeZones) {
	try {
			# Changer le fuseau horaire
			Set-TimeZone -Id $timezone
			Write-Host "Le fuseau horaire a été réglé sur $timezone."

			# Relancer le service de temps Windows
			Write-Host "Relance du service de temps Windows..."
			Stop-Service w32time -Force
			Start-Service w32time
			Write-Host "Le service de temps Windows a été relancé."
	} catch {
			Write-Host "Erreur lors du changement de fuseau horaire : $_"
	}
} else {
	Write-Host "Le fuseau horaire spécifié n'est pas disponible : $timezone"
}



# Définir le serveur de temps
$ntpServer = "ntp.univ-reims.fr"

# Vérifier si le service de temps Windows est en cours d'exécution
if ((Get-Service -Name w32time).Status -ne 'Running') {
	Write-Host "Le service de temps Windows n'est pas en cours d'exécution. Démarrage du service..."
	Start-Service w32time
}

# Resynchroniser l'heure avec le serveur de temps
Write-Host "Resynchronisation de l'heure avec le serveur de temps $ntpServer..."
w32tm /resync /force

# Vérifier si la resynchronisation a réussi
if ($LastExitCode -eq 0) {
	Write-Host "L'heure a été resynchronisée avec succès."
} else {
	Write-Host "Erreur lors de la resynchronisation de l'heure : $LastExitCode"
}





# Nom de la tâche planifiée
$taskName = "Service FixHour"

# Vérifier si la tâche existe
$task = Get-ScheduledTask | Where-Object { $_.TaskName -eq $taskName }

if ($task -and $task.State -eq 'Ready') {
	Write-Host "La tâche '$taskName' existe et est active."
} else {
	Write-Host "La tâche '$taskName' n'existe pas ou n'est pas active. Création de la tâche..."

	# Définir l'action (exécuter le script PowerShell)
	$action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-ExecutionPolicy Bypass -File 'C:\Scripts\SyncTime.ps1'"

	# Définir le déclencheur (toutes les 6 heures)
	$trigger = New-ScheduledTaskTrigger -AtStartup
	$trigger.Repetition.Interval = (New-TimeSpan -Hours 6)
	$trigger.Repetition.Duration = [TimeSpan]::MaxValue

	# Enregistrer la tâche planifiée
	Register-ScheduledTask -Action $action -Trigger $trigger -TaskName $taskName -Description "Synchronisation de l'heure toutes les 6 heures" -User  "SYSTEM" -RunLevel Highest

	Write-Host "La tâche '$taskName' a été créée avec succès."
}