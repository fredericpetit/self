# Self - a Bash, PowerShell & Python development home.

_Personnal main development repository for utilities with shared function._

| param | value |
| ------ | ------ |
| **URL git self** | [https://gitlab.com/fredericpetit/self](https://gitlab.com/fredericpetit/self) |
| **SCHEDULE** (pipeline) | Distro Tracker 11h00 everyday |

## ./src/bash/bin drawer.

### 'distro-tracker' (ver. <span id="distro-tracker">1.0.0</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="distro-tracker">



SYNOPSIS

	distro-tracker - Find out latest stable versions of GNU/Linux & BSD distributions.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	program wget mandatory

 	program jq mandatory

 

UTILISATION

	distro-tracker PARAM [ SOURCE DISTRO CLI LENGTH OUTPUT REPORT BACKUP ] OPTION [ QUIET HELP VERSION ]



PARAM

	-SOURCE [ value ]

		Local filepath or distant url to JSON data. | no default.

 	-DISTRO [ value ]

		Choice between some GNU/Linux & BSD distributions or all registered. | default is 'all'.

 	-CLI [ value ]

		Force CLI ISO distribution if available. | default is 'false'.

 	-LENGTH [ value ]

		Number of valid releases needed. | default is '5'.

 	-OUTPUT [ value ]

		File path to store the final result, without extension, by date name mode or custom name mode. | default is '/builds/fredericpetit/self/DT-DATE.csv'.

 	-REPORT [ value ]

		Generate a JSON report file, only if new releases detected, in same output path, into a file named '*.report.json'. | default is 'false'.

 	-BACKUP [ value ]

		Backup security for keeping and don't remove previous datas if entire website checking fails. New and old datas need to be in the same directory. | default is 'false'.

 

OPTION

	-QUIET

		Be silent. | default is 'false'.

 	-HELP

		Show man.

 	-VERSION

		Show version.

 

EXAMPLE

	distro-tracker -d "debian,fedora,freebsd,mageia,slackware" -l "10" -o "distro-tracker" -s -q



TO-DO

	x



NOTE

	First of all, some distributions have two general indexes to scan to retrieve the latest versions, with main and archive. Information gived in terminal output.

 	Alma, CentOS, Fedora & MX have a main index where everything is listed, but some versions are downloaded from another place.

 	Alpine, Debian, Kaisen & Ubuntu lists beta/RC versions in the index of a release.

 	Fedora lists in the index of a release an "OSB" version, a version built with osbuilder.

 	Alma and CentOS list "non-existent" versions - a version 9 which refers to a 9.3, a 9-stream twin of 9, well ...

 	Alma, Fedora & Ubuntu have both ui or cli mode.

 	Alpine & XPC-NG nead a sort with head because many releases listed in single release page.

 

CREDIT & THANKS

	x



CHANGELOG

	1.0.0 - Expansion to 22 GNU/Linux distributions with the following new additions : Arch, Mageia, OPNsense, Parrot, Proxmox, Slackware, and XCP-ng. General mechanism for retrieving version numbers using the -LENGTH parameter has been revised for more logic : not just X tested versions are returned, but now X valid versions. Addition of the -SOURCE parameter to choose tracking URLs via a local or remote JSON file. Addition of the -REPORT parameter to generate a JSON report to notify of any changes in the latest release of each distribution. Activation of the -ENV parameter. Renaming of the DISTRIB parameter to -DISTRO, -DEPTH to -LENGTH, -SECURITY to -BACKUP. Addition of one-letter shortcuts for each parameter and option. Addition of the Compare_NokFilter function to better manage and clarify versions tagged "NOK" (beta, RC, etc.): all pingable versions are retrieved, with a final filter on stable versions.

 	0.5.0 - Add -SECURITY option for copy old datas to new datas if wget fails on entire website.

 	0.4.0 - Up to 15 distributions by default.

 	0.3.0 - New strong REGEXP, Add -LENGTH parameter, Add command JQ requirement, Add JSON output, Up to 12 distributions GNU/Linux & BSD.

 	0.2.0 - Separate checks on index page & release page.

 	0.1.0 - Add CSV output.

 	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 1.0.0

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Find out latest stable versions of GNU/Linux & BSD distributions. |
| **usage** | ![illustration](assets/img/distro-tracker.png) |

<br />
<br />

### 'distro-manager' (ver. <span id="distro-manager">0.0.1</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="distro-manager">



SYNOPSIS

	distro-manager - Manage ISOs of GNU/Linux & BSD distributions tracked by Distro Tracker.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	x

 

UTILISATION

	distro-manager PARAM [ x ] OPTION [ QUIET HELP VERSION ]



PARAM

	-x [ value ]

		x

 

OPTION

	-QUIET

		Be silent. | default is 'false'.

 	-HELP

		Show man.

 	-VERSION

		Show version.

 

EXAMPLE

	distro-manager -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.0.1

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Manage ISOs of GNU/Linux & BSD distributions tracked by Distro Tracker. |
| **usage** | ![illustration](assets/img/distro-manager.png) |

<br />
<br />

### 'distro-csv2html' (ver. <span id="distro-csv2html">0.2.0</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="distro-csv2html">



SYNOPSIS

	distro-csv2html - Standalone HTML export for Distro Tracker CSV output, with Bootstrap classes; in table format or list format.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	x

 

UTILISATION

	distro-csv2html PARAM [ SOURCE OUTPUT STYLE ] OPTION [ RAW QUIET HELP VERSION ]



PARAM

	-SOURCE [ value ]

		File path of Distro Tracker CSV output. | default is '/builds/fredericpetit/self/distro-tracker.current.csv'.

 	-OUTPUT [ value ]

		File path to store the final result, without extension. | default is '/builds/fredericpetit/self/DTCSV2H.html'.

 	-STYLE [ value ]

		Output style between table or list | default is 'table'.

 

OPTION

	-RAW

		Switch to 'raw' output mode. | default is 'false'.

 	-QUIET

		Be silent. | default is 'false'.

 	-HELP

		Show man.

 	-VERSION

		Show version.

 

EXAMPLE

	distro-csv2html -SOURCE "distro-tracker.current.csv" -OUTPUT "distro-tracker" -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	0.2.0 - Add STYLE parameter, for having table format or list format.

 	0.1.0 - Add Bootstrap classes.

 	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.2.0

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Standalone HTML export for Distro Tracker CSV output. |
| **usage** | ![illustration](assets/img/distro-csv2html.png) |

<br />
<br />

### 'distro-csv2md' (ver. <span id="distro-csv2md">0.0.1</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="distro-csv2md">



SYNOPSIS

	distro-csv2md - Standalone Markdown export for Distro Tracker CSV output.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	x

 

UTILISATION

	distro-csv2md PARAM [ x ] OPTION [ RAW QUIET HELP VERSION ]



PARAM

	-x [ value ]

		x

 

OPTION

	-RAW

		Be silent. | default is 'false'.

 	-QUIET

		Show man.

 	-HELP

		Show version.

 

EXAMPLE

	distro-csv2md -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.0.1

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Standalone HTML export for Distro Tracker CSV output. |
| **usage** | ![illustration](assets/img/distro-csv2md.png) |

<br />
<br />

### 'distro-reporter' (ver. <span id="distro-reporter">0.0.1</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="distro-reporter">



SYNOPSIS

	distro-reporter - Analyze Distro Tracker report output and publish updates to Facebook, Jekyll, & Wordpress.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	program wget mandatory

 	program jq mandatory

 	program curl mandatory

 

UTILISATION

	distro-reporter PARAM [ SOURCE DESTINATION FB_ID FB_TOKEN OUTPUT INCREMENT ] OPTION [ RAW QUIET HELP VERSION ]



PARAM

	-SOURCE [ value ]

		Local filepath or distant url to JSON data. | no default.

 	-DESTINATION [ value ]

		Choice between "facebook", "jekyll" or "wordpress". | no default.

 	-FB_ID [ value ]

		Facebook page ID. | no default.

 	-FB_TOKEN [ value ]

		Facebook user token. | no default.

 	-OUTPUT [ value ]

		For file output, folder path to store the final result, without last slash. | default is '/builds/fredericpetit/self/'.

 	-INCREMENT [ value ]

		Increment security for keeping and don't remove previous datas. New and old datas need to be in the same directory. | default is 'true'.

 

OPTION

	-RAW

		Be silent. | default is 'false'.

 	-QUIET

		Show man.

 	-HELP

		Show version.

 

EXAMPLE

	distro-reporter -DESTINATION "jekyll" -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.0.1

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Analyze Distro Tracker report output and publish updates to Facebook, Jekyll, or Wordpress. |
| **usage** | ![illustration](assets/img/distro-reporter.png) |

<br />
<br />

### 'join-files' (ver. <span id="join-files">0.0.2</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="join-files">



SYNOPSIS

	join-files - Join some files into single file.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	x

 

UTILISATION

	join-files PARAM [ FILES PATTERN OUTPUT ] OPTION [ QUIET HELP VERSION ]



PARAM

	-FILES [ value ]

		Path of files to join. | no default.

 	-PATTERN [ value ]

		String to remove before join files | default is the bash shebang.

 	-OUTPUT [ value ]

		File path to store the final result. | default is '/builds/fredericpetit/self/joinf-DATE.out'.

 

OPTION

	-QUIET

		Be silent. | default is 'false'.

 	-HELP

		Show man.

 	-VERSION

		Show version.

 

EXAMPLE

	join-files -FILES "func1.sh,func2.sh" -PATTERN "#!/bin/bash" -OUTPUT "funcs" -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	0.0.2 - Add Test_File security

 	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.0.2

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Join some files into single file. |
| **usage** | ![illustration](assets/img/join-files.png) |

<br />
<br />

### 'svg2png' (ver. <span id="svg2png">0.0.1</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="svg2png">



SYNOPSIS

	svg2png - Convert SVG files to PNG format with predefined tools.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	program inkscape

 	command imagemagick

 

UTILISATION

	svg2png PARAM [ PATH TOOL SIZE ] OPTION [ QUIET HELP VERSION ]



PARAM

	-PATH [ value ]

		Path where perform the changes (puting files in one or multiple folders). | default is '/builds/fredericpetit/self'.

 	-TOOL [ value ]

		Choice between some tools. | default is 'inkscape'.

 	-SIZE [ value ]

		Size for the converted file. | no default.

 

OPTION

	-QUIET

		Be silent. | default is 'false'.

 	-HELP

		Show man.

 	-VERSION

		Show version.

 

EXAMPLE

	svg2png -PATH "/home/adminlocal/files" -TOOL "inkscape" -SIZE "16x16" -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	x

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.0.1

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Convert SVG files to PNG format with predefined tools. |
| **usage** | ![illustration](assets/img/svg2png.png) |

<br />
<br />

### 'out2tag' (ver. <span id="out2tag">0.2.0</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="out2tag">



SYNOPSIS

	out2tag - Redirect command output to a tag in a file.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	x

 

UTILISATION

	out2tag PARAM [ COMMAND TAG ID OUTPUT ] OPTION [ QUIET HELP VERSION ]



PARAM

	-COMMAND [ value ]

		Text source from output command. | no default.

 	-TAG [ value ]

		TAG name for insertion. | default is 'blockquote'.

 	-ID [ value ]

		ID tag for insertion. | default is 'out2tag'.

 	-OUTPUT [ value ]

		File path of the documentation. | default is '/builds/fredericpetit/self/README.md'.

 

OPTION

	-QUIET

		Be silent. | default is 'false'.

 	-HELP

		Show man.

 	-VERSION

		Show version.

 

EXAMPLE

	out2tag -COMMAND "./out2tag.sh -HELP" -TAG "blockquote" -ID "out2tag" -OUTPUT "README.md" -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	0.2.0 - Get line position content instead tag with only id.

 	0.1.0 - Add TAG parameter, new text function added.

 	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.2.0

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Redirect command output to a tag in file. |
| **usage** | ![illustration](assets/img/out2tag.png) |

<br />
<br />

### 'tux-widget' (ver. <span id="tux-widget">0.2.0</span>).

<details>
<summary><b>CLICK HERE TO EXPAND AND READ THE MANUAL.</b></summary>
<blockquote id="tux-widget">



SYNOPSIS

	tux-widget - Set a fancy Tux widget with Conky.



HOME

	https://gitlab.com/fredericpetit/self/



REQUIREMENT

	program conky

 

UTILISATION

	tux-widget PARAM [ USER ] OPTION [ QUIET HELP VERSION ]



PARAM

	-USER [ value ]

		User account for configuration | no default.

 

OPTION

	-QUIET

		Be silent. | default is 'false'.

 	-HELP

		Show man.

 	-VERSION

		Show version.

 

EXAMPLE

	tux-widget -USER "fred" -QUIET



TO-DO

	x



NOTE

	x

 

CREDIT & THANKS

	x



CHANGELOG

	0.2.0 - USER parameter instead PATH

 	0.1.0 - Style unified with Windows version

 	0.0.1 - Init.

 

METADATA

	Author : Frédéric Petit <contact@fredericpetit.fr>

	Version : 0.2.0

	License : GPL-3.0-or-later



</blockquote>
</details>

| | |
| ------ | ------ |
| **function** | Set a fancy Tux widget with Conky. |
| **usage** | ![illustration](assets/img/tux-widget_gnulinux.png) |

<br />
<br />

## ./src/wordpress/bin drawer.

<br />![boy](assets/img/boy.gif)