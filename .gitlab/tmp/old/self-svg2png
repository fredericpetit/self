#!/bin/bash
#  __________________________________________
# |                                          | #
# |               self-svg2png               | #
# |__________________________________________| #

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                   @@ #
# @@               USAGE               @@ #
# @@                                   @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

function usage {
cat <<EOUSAGE

SYNOPSIS
    ${APP[NAME]} - convert SVG files to PNG files.

REQUIREMENTS
	inkscape program.

UTILISATION
    $(basename $0) [ --path PATH ]

DOWNLOAD
    wget --timeout=10 --tries=2 -q -N https://gitlab.com/fredericpetit/self/-/raw/main/src/linux/self-svg2png -O /usr/bin/self-svg2png && \\
    chmod +x /usr/bin/self-svg2png

PARAMETERS
    --path PATH
        Path where perform the changes, put files in one or multiple folders. | default is '${PWD}/'

EXAMPLE
    self-svg2png

TO-DO
	urgent : x
	later : x

NOTES
    x

CREDITS & THANKS
	x

METADATA
	Author : ${APP[AUTHOR]}
	Version : ${APP[VERSION]}
	License : ${APP[LICENSE]}

.
EOUSAGE
}

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                   @@ #
# @@               FUNCS               @@ #
# @@                                   @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# debugs
function debug_start() { set -x; }
function debug_end() { set +x; }

# fnEcho() .
#
#   ARGS :
#   - $1 :
#       - string : 'start'
#       - string : 'end'
#
#   RETURN : string
#
function fnEcho() {
    if [[ -n "$1" && "$1" == "start"  ]]; then
        echo -e "\n ${APP[NAME]} :: started in '${PWD}/' from '${CONF[PATH]}', at $(fnGetDate).\n";
    elif [[ -n "$1" && "$1" == "end" ]]; then
        CONF[DATE_END]="$(expr $(date +%s) - ${CONF[DATE_START]})";
        echo -e "\n ${APP[NAME]} :: finished in '${PWD}/' from '${CONF[PATH]}', at $(fnGetDate) (executed in $((${CONF[DATE_END]}%3600/60))m:$((${CONF[DATE_END]}%60))s).\n";
    fi
}

# terminate() - end programm.
#
#   ARGS :
#       - $1 : error code
#
#   RETURN :
#       - code
#
function terminate() { wait; exit $1; }

# fnGetDate() .
#
#   ARGS :
#   - $1 :
#       - string : 'full', 'backup', 'sql' or null
#
#   RETURN : string
#
function fnGetDate() {
    local fullDate=$(date +"%A %d %B %Y - %Hh%M:%S");
    # SRC : https://stackoverflow.com/a/50806120/2704550
    local fullDate=( $fullDate );
    local fullDate=${fullDate[@]^};
    local backupDate=$(date +"%Y-%m-%d_%H-%M-%S");
    local sqlDate=$(date +"%Y-%m-%d %H:%M:%S");
    local slimDate=$(date +"%Hh%M:%S");
    if [[ -n "$1" && "$1" == "full" ]]; then echo $fullDate;
    elif [[ -n "$1" && "$1" == "backup" ]]; then echo $backupDate;
    elif [[ -n "$1" && "$1" == "sql" ]]; then echo $sqlDate;
    else echo $slimDate; fi;
}

# fnConvert() - convert with inkscape.
#
#   - $1 :
#       - string : file path
#   - $2 :
#       - boolean : spacer
#
#   RETURN : string
#
function fnConvert() {
    local current_file=$(basename "$1");
    local current_folder=$(dirname "$1");
    # test : directory
    if [ ! -d "${current_folder}/svg2png" &> /dev/null ]; then mkdir "${current_folder}"/svg2png; fi;
    # convert now
    echo " ¤ "$([[ $2 = true ]] && echo " " || echo "")"convert file '${current_file}'.";
    local current_file=$(echo $current_file | sed -e "s/\.svg//g");
    inkscape "$1" -o "${current_folder}/svg2png/${current_file}.png";
}

# fnApp() - init the app.
#
#   ARGS : no arg
#
#   RETURN : no return
#
function fnApp() {
	# loop elements
	for element in ${PARAMS[PATH_SEARCH]}; do
        # test : directory
        if [ -d "$element" &> /dev/null ]; then
            local current_folder=$(basename ${element});
            # test : not again
            if [[ "$current_folder" != "svg2png" ]]; then
                echo " ¤ check folder '${current_folder}' ...";
                for file in "${element}"/*.svg; do
                    # test : svg type
                    if [[ "$file" == *".svg" ]]; then
                        local current_file=$(basename "$file");
                        echo " ¤  check file '${current_file}' ...";
                        fnConvert "${file}" true;
                    fi
                done
            fi
        fi
        # test : file
        if [ -f "$element" &> /dev/null ]; then
            # test : svg type
            if [[ "$element" == *".svg" ]]; then
                local current_file=$(basename "$element");
                echo " ¤ check file '${current_file}' ...";
                fnConvert "${element}" false;
            fi
        fi
	done
}

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                  @@ #
# @@               VARS               @@ #
# @@                                  @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# app.
declare -A APP;
APP[NAME]="self-svg2png";
APP[VERSION]="0.1.0-DEV";
APP[AUTHOR]="Frédéric Petit <contact@fredericpetit.fr>";
APP[LICENSE]="CC BY-SA 4.0";
# conf.
declare -A CONF;
CONF[PATH]=$(dirname $(readlink -f $0))/;
CONF[DATE_START]=$(date +%s);
# params.
declare -A PARAMS;
# loop arguments
while [[ $# > 0 ]]; do
	key="$1";
	case $key in
        --path*)
            PARAMS[PATH]=$(echo $key | sed -e "s#--path=##g" | sed -e "s#--path##g")
			shift
			;;
		-h*|--help)
            echo -e "\n :: USAGE ::\n"
			usage
			shift
            terminate 0
			;;
        *)
            echo -e "\n unknown parameter: '$key'\n"
            usage
            terminate 1
            ;;
	esac
done
# some defaults.
if [ -z "${PARAMS[PATH]}" ]; then PARAMS[PATH]="${PWD}/"; fi;
if [[ "${PARAMS[PATH]}" != *"/" ]]; then echo -e "\n --path parameter incorrect.\n"; terminate 1; fi;
PARAMS[PATH_SEARCH]="${PARAMS[PATH]}*";

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                 @@ #
# @@               APP               @@ #
# @@                                 @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

fnEcho "start";
fnApp;
fnEcho "end";