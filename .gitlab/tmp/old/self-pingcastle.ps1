#  _____________________________________________
# |                                             | #
# |               self-pingcastle               | #
# |_____________________________________________| #

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                 @@ #
# @@               APP               @@ #
# @@                                 @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# standalone download : Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/fredericpetit/fredos/-/raw/main/deploy.ps1'))
############################

##### rights
#Set-ExecutionPolicy -Scope "CurrentUser" -ExecutionPolicy "Unrestricted" -Confirm:$false -Force

##### variables
$GetDate = Get-Date;
$GetNetwork = "1";
$GetIp = "192.168.1.2";
$GetIpDNS = "192.168.1.1";
$GetIpSubnet1 = "192.168.1.0/24";
$GetIpSubnet2 = "172.16.26.0/24";
$GetDomaineName = "global";
$GetDomaineNameNetbios = $GetDomaineName.ToUpper();
$GetDomainTLD = "intra";
$GetDomainFull = "$GetDomaineName.$GetDomainTLD";
$GetDomainDistinguished = "DC=$GetDomaineName,DC=$GetDomainTLD";
$GetPasswordServer = "formule1!formule1!";
$GetPasswordUser = "Formule1!Formule1!";
$GetPathMain = "C:\fredos";
$PathNewOUs = "$GetPathMain" + "\datas\ous.csv";
$PathNewGroups = "$GetPathMain" + "\datas\groups.csv";
$PathNewUsers = "$GetPathMain" + "\datas\users.csv";
if ((Get-CimInstance Win32_OperatingSystem).ProductType -gt 1) {
    $GetOS = "server";
    $GetName = "srv-" + "$GetDomaineName";
    $GetNameNetbios = "SRV-" + "$GetDomaineNameNetbios";
} else {
    $GetOS = "desktop";
    $GetName = "clt-" + "$GetDomaineName" + "-win1";
    $GetNameNetbios = "CLT-" + "$GetDomaineNameNetbios";
}
$ArrayAdmins = @("adminglobal", "adminlocal");






function fnProtectAD() {


    ###########################################################################
    #
    # shortcuts
    #
    ###########################################################################
    $shortcut2Path = "C:\Users\Administrateur\Desktop\PingCastle.exe.lnk";
    If (-NOT (Test-Path "$shortcut2Path")) {
        $shortcut2Source = "C:\Program Files\PingCastle\PingCastle.exe";
        $shortcut2Obj = New-Object -ComObject ("WScript.Shell");
        $shortcut2 = $shortcut2Obj.CreateShortcut($shortcut2Path);
        $shortcut2.TargetPath = $shortcut2Source;
        $shortcut2.WindowStyle = 1;
        $shortcut2.Save();
    }
    $shortcut3Path = "C:\Users\Administrateur\Desktop\PingCastle.lnk";
    If (-NOT (Test-Path "$shortcut3Path")) {
        $shortcut3Source = "C:\Program Files\PingCastle\";
        $shortcut3Obj = New-Object -ComObject ("WScript.Shell");
        $shortcut3 = $shortcut3Obj.CreateShortcut($shortcut3Path);
        $shortcut3.TargetPath = $shortcut3Source;
        $shortcut3.WindowStyle = 1;
        $shortcut3.Save();
    }


    ###########################################################################
    #
    # rule P-RecycleBin : ensure that the Recycle Bin feature is enabled.
    # src : https://hkeylocalmachine.com/?p=760
    #
    ###########################################################################
    Write-Host "##### rule P-RecycleBin." -ForegroundColor "blue";
    $recyclebin = Get-ADOptionalFeature -Identity 'Recycle Bin Feature' -Server "$GetDomainFull";
    if (($recyclebin.enabledscopes).count -eq 0) {
        Enable-ADOptionalFeature -Identity 'Recycle Bin Feature' -Scope ForestOrConfigurationSet -Target "$GetDomainFull" -Confirm:$False;
        Write-Host "rule P-RecycleBin corrected." -ForegroundColor "green";
    } else {
        Write-Host "rule P-RecycleBin already satisfied." -ForegroundColor "green";
        Write-Host "Recycle Bin is enabled for these Domain Controllers :";
        foreach ($scope in $recyclebin.enabledscopes) {
            Write-Host ($scope -split ",")[1].substring(3);
        }
    }
    Write-Host "";


    ###########################################################################
    #
    # rule S-ADRegistration : ensure that basic users cannot register extra computers in the domain.
    #
    ###########################################################################
    Write-Host "##### rule S-ADRegistration." -ForegroundColor "blue";
    Set-ADdomain -Identity "$GetDomainFull" -Replace @{"ms-DS-MachineAccountQuota"="0"};
    Write-Host "rule S-ADRegistration corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # rule S-DC-SubnetMissing : ensure that the minimum set of subnet(s) has been configured in the domain.
    # src : https://theitbros.com/active-directory-sites-and-subnets/
    #
    ###########################################################################
    #Get-ADReplicationSite -Properties *;
    Write-Host "##### rule S-DC-SubnetMissing." -ForegroundColor "blue";
    #$subnet = Get-ADReplicationSite -Identity 'Default-First-Site-Name' -Server "$GetDomainFull" -ErrorAction SilentlyContinue;
    #If (($subnet).count -eq 1) {
        Get-ADReplicationSite -Identity 'Default-First-Site-Name' | Rename-ADObject -NewName DefaultSite -Server "$GetDomainFull" -Confirm:$False;
        New-ADReplicationSubnet -Name "$GetIpSubnet1" -Site 'DefaultSite' -Location "Paris,France" -Server "$GetDomainFull" -Confirm:$False;
        If ($GetNetwork -eq "2") {
            New-ADReplicationSubnet -Name "$GetIpSubnet2" -Site 'DefaultSite' -Location "Paris,France" -Server "$GetDomainFull" -Confirm:$False;
            Write-Host "rule S-DC-SubnetMissing corrected." -ForegroundColor "green";
        }
    #} else {
        #Write-Host "rule S-DC-SubnetMissing already satisfied." -ForegroundColor "green";
    #}
    Write-Host "rule S-DC-SubnetMissing corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # ADMINs ACCOUNTS : rules P-SchemaAdmin, P-Delegated & S-PwdNeverExpires : ensure that no account can make unexpected modifications to the schema, ensure that all Administrator Accounts have the configuration flag "this account is sensitive and cannot be delegated", ensure that every account has a password which is compliant with password expiration policies.
    #
    ###########################################################################
    Write-Host "##### rules P-SchemaAdmin, P-Delegated & S-PwdNeverExpires." -ForegroundColor "blue";
    Set-ADUser -Identity "Administrateur" -PasswordNeverExpires $False -AccountNotDelegated $True;
    Remove-ADGroupMember -Identity 'Administrateurs du schéma' -Members 'Administrateur' -Server "$GetDomainFull" -Confirm:$False;
    Add-ADGroupMember -Identity 'Protected Users' -Members 'Administrateur' -Server "$GetDomainFull";
    Write-Host "rules P-SchemaAdmin, P-Delegated & S-PwdNeverExpires corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # PSO : rules A-MinPwdLen & A-NoServicePolicy : verify if the password policy of the domain enforces users to have at least 8 characters in their password, give information regarding a best practice for the Service Account password policy.
    # UI : https://www.blackhillsinfosec.com/increase-minimum-character-password-length-15-policies-active-directory/
    # src : https://www.it-connect.fr/active-directory-utilisation-des-gmsa-group-managed-service-accounts/
    # src : https://devblogs.microsoft.com/powershell-community/understanding-get-acl-and-ad-drive-output/
    #
    # USAGES
    # Get-ADComputer -Properties DistinguishedName -Filter * | Format-List
    # Get-ADServiceAccount -Properties * -Filter * | Format-List
    # Install-ADServiceAccount "gMSA-$GetDomaineName"; / Uninstall-ADServiceAccount "gMSA-$GetDomaineName";
    # Test-AdServiceAccount -Identity "gMSA-$GetDomaineName"
    #
    ###########################################################################
    Write-Host "##### rule A-MinPwdLen." -ForegroundColor "blue";
    Set-ADDefaultDomainPasswordPolicy -Identity "$GetDomainFull" `
        -ComplexityEnabled:$true  `
        -LockoutDuration:"00:30:00" `
        -LockoutObservationWindow:"00:30:00" `
        -LockoutThreshold:"5" `
        -MaxPasswordAge:"30.00:00:00" `
        -MinPasswordAge:"1.00:00:00" `
        -MinPasswordLength:"10" `
        -PasswordHistoryCount:"10";
    Write-Host "rule A-MinPwdLen corrected." -ForegroundColor "green";
    Write-Host "";

    Write-Host "##### rule A-NoServicePolicy." -ForegroundColor "blue";
    (Get-Acl -Path "AD:CN=Managed Service Accounts,DC=global,DC=intra").Access | Format-Table -AutoSize;
    # key
    if ((Get-KdsRootKey).count -eq 0) {
        Add-KdsRootKey -EffectiveTime ((Get-Date).AddHours(-10));
        Write-Host "root key $(Get-KdsRootKey).KeyId rebuilded." -ForegroundColor "green";
    } else {
        Test-KdsRootKey -KeyId (Get-KdsRootKey).KeyId;
        Write-Host "root key $(Get-KdsRootKey).KeyId already enabled." -ForegroundColor "green";
    }
    # user & pso
    If (Get-ADServiceAccount -Filter "Name -eq 'gMSA-$GetDomaineName'" -ErrorAction Stop) {
        Write-Host "pso need to be rebuild ..." -ForegroundColor "blue";
        Remove-ADFineGrainedPasswordPolicy -Name 'ManagedServiceAccounts';
        Remove-ADFineGrainedPasswordPolicySubject -Identity 'PSO:ManagedServiceAccounts';
        Remove-ADComputerServiceAccount -Identity "$GetName" -ServiceAccount "gMSA-$GetDomaineName";
    } else {
        Write-Host "new pso needed ..." -ForegroundColor "blue";
    }
    New-ADServiceAccount -Name "gMSA-$GetDomaineName" `
        -Description "gMSA account" `
        -DNSHostName "gMSA-$GetDomaineName.$GetDomainFull" `
        -ManagedPasswordIntervalInDays 30 `
        -PrincipalsAllowedToRetrieveManagedPassword "$GetName$" `
        -Enabled $True;
    Add-ADComputerServiceAccount -Identity "$GetName" -ServiceAccount "gMSA-$GetDomaineName";
    Add-ADGroupMember -Identity 'Admins du domaine' -Members "gMSA-$GetDomaineName$" -Server "$GetDomainFull";
    Add-ADGroupMember -Identity 'Protected Users' -Members "gMSA-$GetDomaineName$" -Server "$GetDomainFull";
    # policy
    New-ADFineGrainedPasswordPolicy -Name 'ManagedServiceAccounts' `
        -Precedence 100 `
        -ComplexityEnabled:$true `
        -LockoutDuration:"01:00:00" `
        -LockoutObservationWindow:"01:00:00" `
        -LockoutThreshold:"5" `
        -MaxPasswordAge:"30.00:00:00" `
        -MinPasswordAge:"1.00:00:00" `
        -MinPasswordLength:"20" `
        -PasswordHistoryCount:"10" `
        -ProtectedFromAccidentalDeletion:$true `
        -ReversibleEncryptionEnabled:$false;
    Get-ADServiceAccount -Identity "gMSA-$GetDomaineName" | Add-ADFineGrainedPasswordPolicySubject -Identity 'ManagedServiceAccounts';
    Write-Host "rule A-NoServicePolicy corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # rule A-DnsZoneAUCreateChild : check if Authenticated Users has the right to create DNS records.
    # UI : manually rights in DNS (TO-DO in PS)
    # src : https://devblogs.microsoft.com/powershell-community/understanding-get-acl-and-ad-drive-output/
    #
    # USAGES
    # Get-DnsServerZone
    # Get-ADObject -Filter 'Name -eq "global.intra"' -SearchBase 'CN=MicrosoftDNS,DC=DomainDnsZones,DC=global,DC=intra' -Properties 'ObjectClass' | Format-List
    # (Get-Acl -Path "AD:DC=global.intra,CN=MicrosoftDNS,DC=DomainDnsZones,DC=global,DC=intra").Access | Where-Object {($_.IdentityReference -Like "*Utilisateurs authentifiés*")} | Format-List
    #
    ###########################################################################
    Write-Host "##### rule A-DnsZoneAUCreateChild." -ForegroundColor "blue";
    Write-Host "rule A-DnsZoneAUCreateChild corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # rule A-DC-Spooler : ensure that credentials cannot be extracted from the DC via its printer spooler.
    #
    ###########################################################################
    Write-Host "##### rule A-DC-Spooler." -ForegroundColor "blue";
    If ((Get-Service -Name 'Spooler' -ErrorAction Stop).count -eq 0) {
        Write-Host "service not started ...";
    } else {
        Write-Host "service stopped ...";
        Set-Service -Name 'Spooler' -Status 'stopped' -StartupType 'disabled';
    }
    Write-Host "rule A-DC-Spooler corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # rule A-NoGPOLLMNR : ensure that local name resolution protocol (LLMNR) cannot be used to collect credentials by performing a network attack.
    # src : https://www.blackhillsinfosec.com/how-to-disable-llmnr-why-you-want-to/
    #
    ###########################################################################
    Write-Host "##### rule A-NoGPOLLMNR." -ForegroundColor "blue";
    # GPO
    If (Get-GPO -Name "GPO_LLMNR" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_LLMNR" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_LLMNR = New-GPO -Name "GPO_LLMNR" -Domain "$GetDomainFull";
    $GPO_LLMNR | Set-GPRegistryValue -Key 'HKLM\Software\Policies\Microsoft\Windows NT\DNSClient\' -ValueName "EnableMulticast" -Value 0 -Type 'DWord' | Out-Null;
    New-GPLink -Name "GPO_LLMNR" -Target "OU=F1-BOARD,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_LLMNR" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_LLMNR" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished" | Out-Null;
    Write-Host "rule A-NoGPOLLMNR corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # AUDIT : rules A-AuditDC & A-AuditPowershell (TO-DO in PS) : ensure that the audit policy on domain controllers collect the right set of events, ensure that PowerShell logging is enabled.
    # src : https://superuser.com/a/1059881
    # src : https://www.microsoftpressstore.com/articles/article.aspx?p=2228450&seqNum=7
    # src : https://itluke.online/2017/10/13/how-to-change-advanced-audit-settings-with-powershell/
    # src : https://www.kazamiya.net/en/PolAdtEv
    # src : https://www.kazamiya.net/files/PolAdtEv_Structure_en_rev2.pdf
    # do psexec -s -i regedit
    #
    ###########################################################################
    Write-Host "##### rule A-AuditDC." -ForegroundColor "blue";
    # GPO
    If (Get-GPO -Name "GPO_AuditDC" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_AuditDC" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_AuditDC = New-GPO -Name "GPO_AuditDC" -Domain "$GetDomainFull";
    $GPO_AuditDC | Set-GPPrefRegistryValue -Context 'Computer' -Key 'HKEY_LOCAL_MACHINE\SECURITY\Policy\PolAdtEv\' -ValueName '(default)' -Value ([byte[]](0x00,0x01,0x00,0x00,0x09,0x00,0x00,0x00,0x84,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x00,0x00,0x05,0x00,0x0b,0x00,0x0e,0x00,0x03,0x00,0x06,0x00,0x06,0x00,0x06,0x00,0x04,0x00,0x04,0x00)) -Type 'Binary' -Action 'Create' | Out-Null;
    $GPO_AuditDC | Set-GPPrefRegistryValue -Context 'Computer' -Key 'HKEY_LOCAL_MACHINE\SECURITY\Policy\PolAdtEv\' -ValueName '(default)' -Value ([byte[]](0x00,0x01,0x00,0x00,0x09,0x00,0x00,0x00,0x84,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x03,0x00,0x00,0x00,0x05,0x00,0x0b,0x00,0x0e,0x00,0x03,0x00,0x06,0x00,0x06,0x00,0x06,0x00,0x04,0x00,0x04,0x00)) -Type 'Binary' -Action 'Update' | Out-Null;
    New-GPLink -Name "GPO_AuditDC" -Target "OU=F1-BOARD,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_AuditDC" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_AuditDC" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished" | Out-Null;
    Write-Host "rule A-AuditDC corrected." -ForegroundColor "green";
    Write-Host "";

    Write-Host "##### rule A-AuditPowershell." -ForegroundColor "blue";
    #(Get-Module ActiveDirectory).LogPipelineExecutionDetails = $true;
    # GPO
    If (Get-GPO -Name "GPO_AuditPowershell" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_AuditPowershell" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_AuditPowershell = New-GPO -Name "GPO_AuditPowershell" -Domain "$GetDomainFull";
    # src : https://www.it-connect.fr/powershell-activer-la-journalisation-des-commandes-et-scripts/#II_PowerShell_et_Transcript
    $GPO_AuditPowershell | Set-GPRegistryValue -Key 'HKLM\Software\Policies\Microsoft\Windows\PowerShell\Transcription\' -ValueName "EnableTranscripting" -Value 1 -Type 'DWord' | Out-Null;
    $GPO_AuditPowershell | Set-GPRegistryValue -Key 'HKLM\Software\Policies\Microsoft\Windows\PowerShell\Transcription\' -ValueName "EnableInvocationHeader" -Value 1 -Type 'DWord' | Out-Null;
    $GPO_AuditPowershell | Set-GPRegistryValue -Key 'HKLM\Software\Policies\Microsoft\Windows\PowerShell\Transcription\' -ValueName "OutputDirectory" -Value "" -Type 'String' | Out-Null;
    $GPO_AuditPowershell | Set-GPRegistryValue -Key 'HKLM\Software\Policies\Microsoft\Windows\PowerShell\ModuleLogging\' -ValueName "EnableModuleLogging" -Value 1 -Type 'DWord' | Out-Null;
    # src : https://www.it-connect.fr/powershell-activer-la-journalisation-des-commandes-et-scripts/#IV_PowerShell_et_le_module_Logging
    $GPO_AuditPowershell | Set-GPRegistryValue -Key 'HKLM\Software\Policies\Microsoft\Windows\PowerShell\ModuleLogging\ModuleNames\' -ValueName "*" -Value "*" -Type 'String' | Out-Null;
    # src : https://www.it-connect.fr/powershell-activer-la-journalisation-des-commandes-et-scripts/#III_PowerShell_et_Script_Block_Logging
    $GPO_AuditPowershell | Set-GPRegistryValue -Key 'HKLM\Software\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging\' -ValueName "EnableScriptBlockLogging" -Value 1 -Type 'DWord' | Out-Null;
    New-GPLink -Name "GPO_AuditPowershell" -Target "OU=F1-BOARD,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_AuditPowershell" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_AuditPowershell" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished" | Out-Null;
    Write-Host "rule A-AuditPowershell corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # NetCease : rule A-NoNetSessionHardening : ensure that mitigations are in place against the Bloodhound tool.
    # src : https://p0w3rsh3ll.wordpress.com/2017/08/23/netcease-module/
    #
    # USAGES :
    # Get-NetSessionEnumPermission | Select TranslatedSID,SecurityIdentifier,AccessMask,AceType | Format-Table -AutoSize
    #
    ###########################################################################
    # NOTE : for local action make Set-NetSessionEnumPermission -Verbose -Confirm:$false
    Write-Host "##### rule A-NoNetSessionHardening." -ForegroundColor "blue";
    Install-Module -Name 'NetCease' -Force;
    # GPO
    If (Get-GPO -Name "GPO_NetCease" -Domain "$GetDomainFull" -ErrorAction SilentlyContinue) {
        Remove-GPO -Name "GPO_NetCease" -Domain "$GetDomainFull" -Confirm:$False;
    }
    $GPO_NetCease = New-GPO -Name "GPO_NetCease" -Domain "$GetDomainFull";
    $GPO_NetCease | Set-GPPrefRegistryValue -Context 'Computer' -Key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LanmanServer\DefaultSecurity\' -ValueName 'SrvsvcSessionInfo' -Value ([byte[]](0x01,0x00,0x04,0x80,0x14,0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x2C,0x00,0x00,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x12,0x00,0x00,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x12,0x00,0x00,0x00,0x02,0x00,0x8C,0x00,0x06,0x00,0x00,0x00,0x00,0x00,0x14,0x00,0xFF,0x01,0x1F,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x03,0x00,0x00,0x00,0x00,0x00,0x14,0x00,0xFF,0x01,0x1F,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x04,0x00,0x00,0x00,0x00,0x00,0x14,0x00,0xFF,0x01,0x1F,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x06,0x00,0x00,0x00,0x00,0x00,0x18,0x00,0x13,0x00,0x0F,0x00,0x01,0x02,0x00,0x00,0x00,0x00,0x00,0x05,0x20,0x00,0x00,0x00,0x20,0x02,0x00,0x00,0x00,0x00,0x18,0x00,0x13,0x00,0x0F,0x00,0x01,0x02,0x00,0x00,0x00,0x00,0x00,0x05,0x20,0x00,0x00,0x00,0x23,0x02,0x00,0x00,0x00,0x00,0x18,0x00,0x13,0x00,0x0F,0x00,0x01,0x02,0x00,0x00,0x00,0x00,0x00,0x05,0x20,0x00,0x00,0x00,0x25,0x02,0x00,0x00)) -Type 'Binary' -Action 'Create' | Out-Null;
    $GPO_NetCease | Set-GPPrefRegistryValue -Context 'Computer' -Key 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\LanmanServer\DefaultSecurity\' -ValueName 'SrvsvcSessionInfo' -Value ([byte[]](0x01,0x00,0x04,0x80,0x14,0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x2C,0x00,0x00,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x12,0x00,0x00,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x12,0x00,0x00,0x00,0x02,0x00,0x8C,0x00,0x06,0x00,0x00,0x00,0x00,0x00,0x14,0x00,0xFF,0x01,0x1F,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x03,0x00,0x00,0x00,0x00,0x00,0x14,0x00,0xFF,0x01,0x1F,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x04,0x00,0x00,0x00,0x00,0x00,0x14,0x00,0xFF,0x01,0x1F,0x00,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x06,0x00,0x00,0x00,0x00,0x00,0x18,0x00,0x13,0x00,0x0F,0x00,0x01,0x02,0x00,0x00,0x00,0x00,0x00,0x05,0x20,0x00,0x00,0x00,0x20,0x02,0x00,0x00,0x00,0x00,0x18,0x00,0x13,0x00,0x0F,0x00,0x01,0x02,0x00,0x00,0x00,0x00,0x00,0x05,0x20,0x00,0x00,0x00,0x23,0x02,0x00,0x00,0x00,0x00,0x18,0x00,0x13,0x00,0x0F,0x00,0x01,0x02,0x00,0x00,0x00,0x00,0x00,0x05,0x20,0x00,0x00,0x00,0x25,0x02,0x00,0x00)) -Type 'Binary' -Action 'Update' | Out-Null;
    New-GPLink -Name "GPO_NetCease" -Target "OU=F1-BOARD,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_NetCease" -Target "OU=F1-TEAMS-RACERS,$GetDomainDistinguished" | Out-Null;
    New-GPLink -Name "GPO_NetCease" -Target "OU=F1-TEAMS-STAFF,$GetDomainDistinguished" | Out-Null;
    Restart-Service -Name 'LanmanServer' -Force -Verbose;
    Write-Host "rule A-NoNetSessionHardening corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # Old : rule A-PreWin2000AuthenticatedUsers : check if the "Pre-Windows 2000 Compatible Access" group contains authenticated users.
    #
    ###########################################################################
    Write-Host "##### rule A-PreWin2000AuthenticatedUsers." -ForegroundColor "blue";
    Get-ADGroup -Identity "Accès compatible pré-Windows 2000" -Server "$GetDomainFull" | Set-ADGroup -Clear member -Confirm:$False;
    Write-Host "rule A-PreWin2000AuthenticatedUsers corrected." -ForegroundColor "green";
    Write-Host "";


    ###########################################################################
    #
    # LDAP : rule A-DsHeuristicsLDAPSecurity : identify domains having mitigation for CVE-2021-42291 not set to enabled.
    #
    ###########################################################################
    Write-Host "##### rule A-DsHeuristicsLDAPSecurity." -ForegroundColor "blue";
    $TargetDN = ("CN=Directory Service,CN=Windows NT,CN=Services,CN=Configuration," + (Get-ADDomain).DistinguishedName);
    $ValuedsHeuristics = (Get-ADObject -Identity $TargetDN -Properties dsHeuristics).dsHeuristics;
    if (($ValuedsHeuristics -eq "00000000010000000002000000011")) {
        Write-Host "rule A-DsHeuristicsLDAPSecurity already satisfied." -ForegroundColor "green";
    } else {
        Write-Host "rule A-DsHeuristicsLDAPSecurity need to be corrected : value = $ValuedsHeuristics !" -ForegroundColor "red";
        Get-ADObject -Identity $TargetDN -Properties dsHeuristics |  Set-ADObject -Replace @{dsHeuristics="00000000010000000002000000011"};
        Write-Host "rule A-DsHeuristicsLDAPSecurity corrected." -ForegroundColor "green";
    }
    Write-Host "";


    ###########################################################################
    #
    # LAPS : rule A-LAPS-Not-Installed : make sure that there is a proper password policy in place for the native local administrator account.
    # src : https://www.it-connect.fr/chapitres/installation-de-laps-sur-un-controleur-de-domaine/
    # src : https://rdr-it.com/laps-securisation-comptes-administrateur-local-installation-configuration/
    #
    ###########################################################################
    Write-Host "##### rule A-LAPS-Not-Installed." -ForegroundColor "blue";
    Add-ADGroupMember -Identity 'Administrateurs du schéma' -Members 'Administrateur' -Server "$GetDomainFull";
    Import-module -SkipEditionCheck AdmPwd.PS;
    Update-AdmPwdADSchema;
    Set-AdmPwdComputerSelfPermission -OrgUnit "OU=F1-ADMINS,$GetDomainDistinguished";
    Set-AdmPwdReadPasswordPermission -Identity "OU=F1-ADMINS,$GetDomainDistinguished" -AllowedPrincipals "locale-security";
    Set-AdmPwdResetPasswordPermission -Identity "OU=F1-ADMINS,$GetDomainDistinguished" -AllowedPrincipals "locale-security";
    If (-NOT (Test-Path "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies\PolicyDefinitions\AdmPwd.admx")) {
        Copy-Item -Path "C:\Windows\PolicyDefinitions\AdmPwd.admx" -Destination "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies\PolicyDefinitions\";
    }
    If (-NOT (Test-Path "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies\PolicyDefinitions\fr-fr\AdmPwd.adml")) {
        Copy-Item -Path "C:\Windows\PolicyDefinitions\en-US\AdmPwd.adml" -Destination "C:\Windows\SYSVOL\sysvol\$GetDomainFull\Policies\PolicyDefinitions\fr-fr\";
    }
    Remove-ADGroupMember -Identity 'Administrateurs du schéma' -Members 'Administrateur' -Server "$GetDomainFull" -Confirm:$False;
    Write-Host "rule A-LAPS-Not-Installed corrected." -ForegroundColor "green";


    ###########################################################################
    #
    # Report
    #
    ###########################################################################
    Get-GPOReport -Name "Default Domain Policy" -Domain "$GetDomainFull" -ReportType HTML -Path "C:\fredos\audit\GPO_default.html";

}