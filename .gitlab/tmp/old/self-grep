#!/bin/bash
#  _______________________________________
# |                                       | #
# |               self-grep               | #
# |_______________________________________| #

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                   @@ #
# @@               USAGE               @@ #
# @@                                   @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

function usage {
cat <<EOUSAGE

SYNOPSIS
    ${APP[NAME]} - shortcut for searching a pattern everywhere with grep with recursive, line-number and word-regexp options.

REQUIREMENTS
    x

UTILISATION
    $(basename $0) [ --path PATH ] [ --pattern PATTERN ]

DOWNLOAD
    wget --timeout=10 --tries=2 -q -N https://gitlab.com/fredericpetit/self/-/raw/main/src/linux/self-grep -O /usr/bin/self-grep && \\
    chmod +x /usr/bin/self-grep

PARAMETERS
    --path PATH
        path where search | no default
    --pattern PATTERN
        text to search | no default

EXAMPLES
    self-grep --path=/usr/share/gnome-shell/extensions/ --pattern=bing && history -c

TO-DO
    urgent : x
    later : x

NOTES
    x

CREDITS & THANKS
    x

SOURCES
    Mohammad Mullick <https://stackoverflow.com/a/16957078>

METADATA
    Author : ${APP[AUTHOR]}
    Version : ${APP[VERSION]}
    License : ${APP[LICENSE]}

.
EOUSAGE
}

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                   @@ #
# @@               FUNCS               @@ #
# @@                                   @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# debugs
function debug_start() { set -x; }
function debug_end() { set +x; }

# terminate() - end programm.
#
#   ARGS :
#       - $1 : code
#
#   RETURN : no return
#
function terminate() { wait; exit $1; }

# fnApp() - init the app.
#
#   ARGS : no arg
#
#   RETURN : string
#
function fnApp() { grep -rnw "${PARAMS[PATH]}" -e "${PARAMS[PATTERN]}"; }

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                  @@ #
# @@               VARS               @@ #
# @@                                  @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# app.
declare -A APP;
APP[NAME]="self-grep";
APP[VERSION]="0.1.0-DEV";
APP[AUTHOR]="Frédéric Petit <contact@fredericpetit.fr>";
APP[LICENSE]="CC BY-SA 4.0";
# conf.
declare -A CONF;
CONF[PATH]=$(dirname $(readlink -f $0))/;
# params.
declare -A PARAMS;
if [[ $# < 2 ]]; then echo -e "\n one or many parameters missed.\n"; usage; terminate 1; fi;
# loop arguments
while [[ $# > 0 ]]; do
	key="$1";
	case $key in
        --path*)
            PARAMS[PATH]=$(echo $key | sed -e "s#--path=##g" | sed -e "s#--path##g")
			shift
			;;
        --pattern*)
            PARAMS[PATTERN]=$(echo $key | sed -e "s#--pattern=##g" | sed -e "s#--pattern##g")
			shift
			;;
		-h*|--help)
            echo -e "\n :: USAGE ::\n"
			usage
			shift
            terminate 0
			;;
        *)
            echo -e "\n unknown parameter: '$key'\n"
            usage
            terminate 1
            ;;
	esac
done
# tests : some defaults.
if [ -z "${PARAMS[PATH]}" ]; then echo -e "\n --path parameter missed or empty.\n"; terminate 1; fi;
if [[ "${PARAMS[PATH]}" != *"/" ]]; then echo -e "\n --path parameter incorrect.\n"; terminate 1; fi;
if [ -z "${PARAMS[PATTERN]}" ]; then echo -e "\n --pattern parameter missed or empty.\n"; terminate 1; fi;

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                 @@ #
# @@               APP               @@ #
# @@                                 @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

fnApp;