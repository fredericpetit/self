## Listing
### _/src/linux_ drawer
- **self-git** : clone all GIT distant repositories (both public and private) from a namespace's JSON data, or change the access token of all local repositories.
- **self-grep** : shortcut for searching a pattern everywhere with grep.
- **self-gse** : GNOME Shell Extensions deployer is a CLI for deploy GNOME extensions.
- **self-nc** : shortcut for using netcat.
- **self-nlist** : list in a file all virtual hosts from Nginx.
- **self-ssh** : create, publish and copy ssh key with most recent best practices.