#!/bin/bash
#  _______________________________________________
# |                                               | #
# |                                               | #
# |               changeGitToken.sh               | #
# |                                               | #
# |_______________________________________________| #
#

########################################################
#       __        __   ___ 
# |  | /__`  /\  / _` |__  
# \__/ .__/ /~~\ \__> |___ 
#
########################################################

function usage {
cat <<EOUSAGE

SYNOPSIS
    ${APP[NAME]} - Change GIT repository token.

REQUIREMENTS
    - GIT program

UTILISATION
    - script : $(basename $0) [ OPTIONS ]

OPTIONS
    --path PATH
        Path where perform the changes | default is '$PWD/'
    --provider PROVIDER
        Provider, gitlab.com or github.com | default is 'gitlab.com'
    --namespace NAMESPACE
        user namespace | default is 'fredericpetit'
    --token TOKEN
        Token ID from the provider

EXAMPLES
    - gitlab example :
        wget --timeout=10 --tries=2 -q -N https://gitlab.com/fredericpetit/self/-/raw/main/old/changeGitToken.sh && \\
        chmod +x changeGitToken.sh && bash changeGitToken.sh --token=xxxxxxxxxx && \\
        history -c
    - github example :
        wget --timeout=10 --tries=2 -q -N https://gitlab.com/fredericpetit/self/-/raw/main/old/changeGitToken.sh && \\
        chmod +x changeGitToken.sh && bash changeGitToken.sh --provider=github.com --namespace=localhosthd --token=xxxxxxxxxx && \\
        history -c

TO-DO
    - urgent : x
    - later : x

NOTES
    x

CREDITS & THANKS
    x

SOURCES
    x

METADATA
    - Author : ${APP[AUTHOR]}
    - Version : ${APP[VERSION]}
    - License : ${APP[LICENSE]}

.
EOUSAGE
}

########################################################
#  ___            __  ___    __        __  
# |__  |  | |\ | /  `  |  | /  \ |\ | /__` 
# |    \__/ | \| \__,  |  | \__/ | \| .__/ 
#
########################################################

# debugs
function debug_start() { set -x; }
function debug_end() { set +x; }

# fnEcho() - for multiple.
#
#   ARGS :
#   - $1 :
#       - string : 'start'
#       - string : 'end'
#
#   RETURN : string
#
function fnEcho() {
    if [[ -n "$1" && "$1" == "start"  ]]; then
        echo -e "\n ${APP[NAME]} :: started in '${CONF[PATH]}', at $(fnGetDate).\n";
    elif [[ -n "$1" && "$1" == "end" ]]; then
        CONF[DATE_END]="$(expr $(date +%s) - ${CONF[DATE_START]})";
        echo -e "\n ${APP[NAME]} :: finished in '${CONF[PATH]}', at $(fnGetDate) (executed in $((${CONF[DATE_END]}%3600/60))m:$((${CONF[DATE_END]}%60))s).\n";
    fi
}

# terminate() - end programm.
#
#   ARGS :
#       - $1 : code
#
#   RETURN : no return
#
function terminate() { wait; fnEcho "end"; exit $1; }

# fnGetDate() - for multiple.
#
#   ARGS :
#   - $1 :
#       - string : 'full', 'backup', 'sql' or null
#
#   RETURN : string
#
function fnGetDate() {
    local fullDate=$(date +"%A %d %B %Y - %Hh%M:%S");
    # SRC : https://stackoverflow.com/a/50806120/2704550
    local fullDate=( $fullDate );
    local fullDate=${fullDate[@]^};
    local backupDate=$(date +"%Y-%m-%d_%H-%M-%S");
    local slimDate=$(date +"%Hh%M:%S");
    local sqlDate=$(date +"%Y-%m-%d %H:%M:%S");
    if [[ -n "$1" && "$1" == "full" ]]; then
        echo $fullDate;
    elif [[ -n "$1" && "$1" == "backup" ]]; then
        echo $backupDate;
    elif [[ -n "$1" && "$1" == "sql" ]]; then
        echo $sqlDate;
    else
        echo $slimDate;
    fi;
}

# repeat() - repeat a character.
#
#   ARGS :
#       - $1 : string, number for repeat
#       - $2 : string, character
#
#   RETURN :
#       - string
#
function repeat() { for i in $(seq 1 $1); do echo -n "$2"; done; }

# fnApp() - init the app.
#
#   ARGS : no arg
#
#   RETURN : no return
#
function fnApp() {
    # loop max counter characters
    for element in ${PARAMS[PATH]}*/; do
        if [ -d "$element" ]; then
            if [[ ${CONF[MAX_COUNTER]} -lt ${#element} ]]; then
                CONF[MAX_COUNTER]=${#element};
            fi
        fi
    done
    # loop git command
    for element in ${PARAMS[PATH]}*/; do
        if [ -d "$element" ]; then
            local counter=$((${CONF[MAX_COUNTER]} + 5 - ${#element}));
            echo -e " ¤ path '${element}' $(repeat ${counter} .) repo '$(basename ${element})'";
            git -C ${element} remote set-url origin ${PARAMS[URL]}/$(basename ${element});
        fi
    done
}

############################
#            __          __        ___  __  
# \  /  /\  |__) |  /\  |__) |    |__  /__` 
#  \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/ 
#
############################

# app.
declare -A APP;
APP[NAME]="changeGitToken";
APP[VERSION]="0.1.0-DEV";
APP[AUTHOR]="Frédéric Petit <contact@fredericpetit.fr>";
APP[LICENSE]="CC BY-SA 4.0";
# conf.
declare -A CONF;
CONF[PATH]=$(dirname $(readlink -f $0))/;
CONF[DATE_START]=$(date +%s);
CONF[PATH_COUNTER]=${#CONF[PATH]};
CONF[MAX_COUNTER]=0;
# params.
declare -A PARAMS;
if [[ $# < 1 ]]; then echo -e "\n one or many parameters missed.\n"; usage; terminate 1; fi;
# loop arguments
while [[ $# > 0 ]]; do
	key="$1";
	case $key in
        --path*)
            PARAMS[PATH]=$(echo $key | sed -e "s#--path=##g" | sed -e "s#--path##g")
			shift
			;;
        --provider*)
            PARAMS[PROVIDER]=$(echo $key | sed -e "s#--provider=##g" | sed -e "s#--provider##g")
			shift
			;;
        --namespace*)
            PARAMS[NAMESPACE]=$(echo $key | sed -e "s#--namespace=##g" | sed -e "s#--namespace##g")
			shift
			;;
		--token*)
            PARAMS[TOKEN]=$(echo $key | sed -e "s#--token=##g" | sed -e "s#--token##g")
			shift
			;;
		-h*|--help)
            echo -e "\n :: USAGE ::\n"
			usage
			shift
            terminate 0
			;;
        *)
            echo -e "\n unknown parameter: '$key'\n"
            usage
            terminate 1
            ;;
	esac
done
# some defaults.
if [ -z "${PARAMS[NAMESPACE]}" ]; then PARAMS[NAMESPACE]="fredericpetit"; fi;
if [ -z "${PARAMS[PATH]}" ]; then PARAMS[PATH]="${PWD}/"; fi;
if [[ "${PARAMS[PATH]}" != *"/" ]]; then echo -e "\n --path parameter incorrect.\n"; terminate 1; fi;
if [ -z "${PARAMS[PROVIDER]}" ]; then PARAMS[PROVIDER]="gitlab.com"; fi;
if [ -z "${PARAMS[TOKEN]}" ]; then echo -e "\n --token parameter missed or empty.\n"; terminate 1; fi;
PARAMS[URL]="https://oauth2:${PARAMS[TOKEN]}@${PARAMS[PROVIDER]}/${PARAMS[NAMESPACE]}";

########################################################
#      __   __  
#  /\  |__) |__) 
# /~~\ |    |    
#
########################################################

fnEcho "start";
fnApp;
fnEcho "end";