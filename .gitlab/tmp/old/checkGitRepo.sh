#!/bin/bash
#  _____________________________________________
# |                                             | #
# |                                             | #
# |               checkGitRepo.sh               | #
# |                                             | #
# |_____________________________________________| #
#

########################################################
#       __        __   ___ 
# |  | /__`  /\  / _` |__  
# \__/ .__/ /~~\ \__> |___ 
#
########################################################

function usage {
cat <<EOUSAGE

SYNOPSIS
    ${APP[NAME]} - Auto-clone GIT repository from user JSON datas.

REQUIREMENTS
    - GIT & curl programs

UTILISATION
    - script : $(basename $0) [ OPTIONS ]

OPTIONS
    --namespace NAMESPACE
        user namespace | default is 'fredericpetit'
    --output
        json output | default is false
    --path PATH
        Path where perform the changes | default is '$PWD/'
    --provider PROVIDER
        Provider, gitlab.com or github.com | default is 'gitlab.com'
    --token TOKEN
        Token ID from the provider
    --type TYPE
        type, 'org' or 'user' | default is 'user'

EXAMPLES
    - gitlab example :
        wget --timeout=10 --tries=2 -q -N https://gitlab.com/fredericpetit/self/-/raw/main/old/checkGitRepo.sh && \\
        chmod +x checkGitRepo.sh && bash checkGitRepo.sh --token=xxxxxxxxxx && \\
        history -c
    - github example :
        wget --timeout=10 --tries=2 -q -N https://gitlab.com/fredericpetit/self/-/raw/main/old/checkGitRepo.sh && \\
        chmod +x checkGitRepo.sh && bash checkGitRepo.sh --provider=github.com --namespace=localhosthd --token=xxxxxxxxxx && \\
        history -c

TO-DO
    - urgent : x
    - later : x

NOTES
    x

CREDITS & THANKS
    x

SOURCES
    x

METADATA
    - Author : ${APP[AUTHOR]}
    - Version : ${APP[VERSION]}
    - License : ${APP[LICENSE]}

.
EOUSAGE
}

########################################################
#  ___            __  ___    __        __  
# |__  |  | |\ | /  `  |  | /  \ |\ | /__` 
# |    \__/ | \| \__,  |  | \__/ | \| .__/ 
#
########################################################

# debugs
function debug_start() { set -x; }
function debug_end() { set +x; }

# fnEcho() - for multiple.
#
#   ARGS :
#   - $1 :
#       - string : 'start'
#       - string : 'end'
#
#   RETURN : string
#
function fnEcho() {
    if [[ -n "$1" && "$1" == "start"  ]]; then
        echo -e "\n ${APP[NAME]} :: started in '${CONF[PATH]}', at $(fnGetDate).\n";
    elif [[ -n "$1" && "$1" == "end" ]]; then
        CONF[DATE_END]="$(expr $(date +%s) - ${CONF[DATE_START]})";
        echo -e "\n ${APP[NAME]} :: finished in '${CONF[PATH]}', at $(fnGetDate) (executed in $((${CONF[DATE_END]}%3600/60))m:$((${CONF[DATE_END]}%60))s).\n";
    fi
}

# terminate() - end programm.
#
#   ARGS :
#       - $1 : code
#
#   RETURN : no return
#
function terminate() { wait; fnEcho "end"; exit $1; }

# fnGetDate() - for multiple.
#
#   ARGS :
#   - $1 :
#       - string : 'full', 'backup', 'sql' or null
#
#   RETURN : string
#
function fnGetDate() {
    local fullDate=$(date +"%A %d %B %Y - %Hh%M:%S");
    # SRC : https://stackoverflow.com/a/50806120/2704550
    local fullDate=( $fullDate );
    local fullDate=${fullDate[@]^};
    local backupDate=$(date +"%Y-%m-%d_%H-%M-%S");
    local slimDate=$(date +"%Hh%M:%S");
    local sqlDate=$(date +"%Y-%m-%d %H:%M:%S");
    if [[ -n "$1" && "$1" == "full" ]]; then
        echo $fullDate;
    elif [[ -n "$1" && "$1" == "backup" ]]; then
        echo $backupDate;
    elif [[ -n "$1" && "$1" == "sql" ]]; then
        echo $sqlDate;
    else
        echo $slimDate;
    fi;
}

# fnCheckPrg() - check if it's a program is available.
#
#   ARGS :
#   - $1 :
#       - string : program name
#
#   RETURN : code
#
function fnCheckPrg() { if command -v "$1" &> /dev/null; then return 0; else return 1; fi; }

# fnApp() - init the app.
#
#   ARGS : no arg
#
#   RETURN : no return
#
function fnApp() {
    if $(fnCheckPrg "git"); then
        if $(fnCheckPrg "curl"); then
            # test : source provider
            if [[ "${PARAMS[PROVIDER]}" == "gitlab.com" ]]; then
                local url=$(curl --silent --header "PRIVATE-TOKEN: ${PARAMS[TOKEN]}" "https://gitlab.com/api/v4/users/${PARAMS[NAMESPACE]}/projects");
                local command=$(echo ${url} | jq -r -c '. | [.[].path]');
                local arrayKeys=$(echo ${command} | jq "keys | .[]");
                # test : json output
                if [[ "${PARAMS[OUTPUT]}" == true ]]; then echo "$url" | tee -a "${PARAMS[PATH]}/gitlab_$(fnGetDate 'backup').json" > /dev/null; fi;
                # test : json keys
                if [[ -n ${arrayKeys} ]]; then
                    # loop elements
                    for element in ${arrayKeys}; do
                        local repository=$(echo $command | jq -r -c ".[$element]");
                        if [ -d "${PARAMS[PATH]}${repository}" &> /dev/null ]; then
                            echo -e " ¤ path '${PARAMS[PATH]}' : existing repo '${repository}'";
                        else
                            echo -e " ¤ path '${PARAMS[PATH]}' : new repo '${repository}'";
                            git clone --depth=1 ${PARAMS[URL]}${repository}.git ${PARAMS[PATH]}${repository};
                        fi
                    done
                fi
            elif [[ "${PARAMS[PROVIDER]}" == "github.com" ]]; then
                local url=$(curl --silent --header "Accept: application/vnd.github+json" --header "Authorization: Bearer ${PARAMS[TOKEN]}" "https://api.github.com/search/repositories?q=user:${PARAMS[NAMESPACE]}+fork:true+created:>2000-01-01");
                local command=$(echo ${url} | jq -r -c '.items | [.[].full_name]');
                local arrayKeys=$(echo ${command} | jq "keys | .[]");
                # test : json output
                if [[ "${PARAMS[OUTPUT]}" == true ]]; then echo "$url" | tee -a "${PARAMS[PATH]}/github_$(fnGetDate 'backup').json" > /dev/null; fi;
                # test : json keys
                if [[ -n ${arrayKeys} ]]; then
                    # loop elements
                    for element in ${arrayKeys}; do
                        local repository=$(echo $command | jq -r -c ".[$element]" | sed -e "s#${PARAMS[NAMESPACE]}/##g" | tr '[:upper:]' '[:lower:]');
                        if [ -d "${PARAMS[PATH]}${repository}" &> /dev/null ]; then
                            echo -e " ¤ path '${PARAMS[PATH]}' : existing repo '${repository}'";
                        else
                            echo -e " ¤ path '${PARAMS[PATH]}' : new repo '${repository}'";
                            git clone --depth=1 ${PARAMS[URL]}${repository}.git ${PARAMS[PATH]}${repository};
                        fi
                    done
                fi
            else
                echo -e " ¤ /!\ provider not supported. exit."; terminate 1; fi;
        else echo -e " ¤ /!\ curl program needed. exit."; terminate 1; fi;
    else echo -e " ¤ /!\ GIT program needed. exit."; terminate 1; fi;
}

########################################################
#            __          __        ___  __  
# \  /  /\  |__) |  /\  |__) |    |__  /__` 
#  \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/ 
#
########################################################

# app.
declare -A APP;
APP[NAME]="checkGitRepo";
APP[VERSION]="0.1.0-DEV";
APP[AUTHOR]="Frédéric Petit <contact@fredericpetit.fr>";
APP[LICENSE]="CC BY-SA 4.0";
# conf.
declare -A CONF;
CONF[PATH]=$(dirname $(readlink -f $0))/;
CONF[DATE_START]=$(date +%s);
# params.
declare -A PARAMS;
if [[ $# < 1 ]]; then echo -e "\n one or many parameters missed.\n"; usage; terminate 1; fi;
# loop arguments
while [[ $# > 0 ]]; do
	key="$1";
	case $key in
        --namespace*)
            PARAMS[NAMESPACE]=$(echo $key | sed -e "s#--namespace=##g" | sed -e "s#--namespace##g")
			shift
			;;
        --output*)
            PARAMS[OUTPUT]=true
            shift
			;;
        --path*)
            PARAMS[PATH]=$(echo $key | sed -e "s#--path=##g" | sed -e "s#--path##g")
			shift
			;;
        --provider*)
            PARAMS[PROVIDER]=$(echo $key | sed -e "s#--provider=##g" | sed -e "s#--provider##g")
			shift
			;;
        --type*)
            PARAMS[TYPE]=$(echo $key | sed -e "s#--type=##g" | sed -e "s#--type##g")
			shift
			;;
		--token*)
            PARAMS[TOKEN]=$(echo $key | sed -e "s#--token=##g" | sed -e "s#--token##g")
			shift
			;;
		-h*|--help)
            echo -e "\n :: USAGE ::\n"
			usage
			shift
            terminate 0
			;;
        *)
            echo -e "\n unknown parameter: '$key'\n"
            usage
            terminate 1
            ;;
	esac
done
# some defaults.
if [ -z "${PARAMS[NAMESPACE]}" ]; then PARAMS[NAMESPACE]="fredericpetit"; fi;
if [ -z "${PARAMS[OUTPUT]}" ]; then PARAMS[OUTPUT]=false; fi;
if [ -z "${PARAMS[PATH]}" ]; then PARAMS[PATH]="${PWD}/"; fi;
if [[ "${PARAMS[PATH]}" != *"/" ]]; then echo -e "\n --path parameter incorrect.\n"; terminate 1; fi;
if [ -z "${PARAMS[PROVIDER]}" ]; then PARAMS[PROVIDER]="gitlab.com"; fi;
if [ -z "${PARAMS[TOKEN]}" ]; then echo -e "\n --token parameter missed or empty.\n"; terminate 1; fi;
if [ -z "${PARAMS[TYPE]}" ]; then PARAMS[TYPE]="user"; fi;
PARAMS[URL]="https://oauth2:${PARAMS[TOKEN]}@${PARAMS[PROVIDER]}/${PARAMS[NAMESPACE]}/";

########################################################
#      __   __  
#  /\  |__) |__) 
# /~~\ |    |    
#
########################################################

fnEcho "start";
fnApp;
fnEcho "end";