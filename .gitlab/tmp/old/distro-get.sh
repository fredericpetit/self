#!/bin/bash
# shellcheck source=/dev/null
#  ___________________________________________
# |                                           | #
# |               distro-get.sh               | #
# |___________________________________________| #


# USAGE : Tiny service to download or read data from Distro Tracker (CSV, JSON or YAML formats) & Distro Reporter (JSON or YAML formats).
# VERSION : 01-01-2025.
# LICENSE : GPL-3.0-or-later.


# =============================== #
#          CONFIGURATION          #
#
#	DESCRIPTION : everything that configures the APP.

declare -gA CONF;
CONF["PATH"]="$(dirname "$(readlink -f "$0")")";


# ========================= #
#          OPTIONS          #
#
#	DESCRIPTION : everything that set the APP.

# --action : 'download' or 'read'.
# --source : for read '--action', choose beetween 'distro-tracker' or 'distro-reporter' files.
# --type : 'tracker' or 'reporter'.


# =================================== #
#          PARAMETERS (call)          #
#
#	DESCRIPTION : calling parameter(s) of the script.

# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--action|-a) var_param_action="$2"; shift 2 ;;
		--source|-s) var_param_source="$2"; shift 2 ;;
		--type|-t) var_param_type="$2"; shift 2 ;;
		*) shift; echo "invalid"; exit 1 ;;
	esac
done;


# ======================================== #
#          PARAMETERS (variables)          #
#
#	DESCRIPTION : set of calling parameter(s) of the script.

if [[ ! "$var_param_action" =~ ^(download|read)$ ]]; then exit; fi;
if [[ "$var_param_action" == "download" ]]; then
	if [ -z "$var_param_source" ]; then 
		if [[ "$var_param_type" == "tracker" ]]; then
			var_param_source="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-tracker/data/distro-tracker.current.json";
		elif [[ "$var_param_type" == "reporter" ]]; then
			var_param_source="https://gitlab.com/fredericpetit/self/-/raw/main/org/bash/distro-reporter/data/distro-reporter.json";
		fi;
	fi;
	if [[ ! "$var_param_source" =~ ^(http|www) ]]; then exit; fi;
elif [[ "$var_param_action" == "read" ]]; then
	if [ -z "$var_param_type" ]; then exit; fi;
	if [[ ! "$var_param_type" =~ ^(tracker|reporter)$ ]]; then exit; fi;
	if [[ "$var_param_source" =~ ^(http|www) ]]; then exit; fi;
	if [ -z "$var_param_source" ]; then 
		if [[ "$var_param_type" == "tracker" ]]; then
			var_param_source="${CONF[PATH]}/distro-tracker.json";
		elif [[ "$var_param_type" == "reporter" ]]; then
			var_param_source="${CONF[PATH]}/distro-reporter.json";
		fi;
	fi;
fi;
if ! [[ "$var_param_source" =~ \.(csv|json|yaml)$ ]]; then exit; fi;


# =========================== #
#          FUNCTIONS          #

# Set_LocalDistroTracker.
#	DESCRIPTION : set distro-tracker data.
function Set_LocalDistroTracker() {
	# Some definition.
	local var_url;
	local var_extension;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-URL) var_url="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	var_extension="$(echo "$var_url" | rev | cut -d '.' -f 1 | rev)";
	wget --no-check-certificate --timeout=10 --tries=2 -O "${CONF[PATH]}/distro-tracker.${var_extension}" "$var_url";
}

# Get_LocalDistroTracker.
#	DESCRIPTION : get distro-tracker data.
function Get_LocalDistroTracker() {
	# Some definition.
	local var_path;
	local var_data;
	local var_data_distro;
	local var_data_distro_name;
	local var_data_distro_release;
	local var_data_distro_release_date;
	local var_list;
	local var_index;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Data.
	var_data="$(<"$var_path")";
	var_list=();
	# Tests : CSV, JSON, YAML.
	if [[ "$var_path" == *.csv ]]; then
		# Loop : Process Substitution.
		while IFS=',' read -r var_data_distro_name var_data_distro_release var_data_distro_release_date _; do
			var_list+=("${var_data_distro_name} - ${var_data_distro_release} (${var_data_distro_release_date})");
		done < <(echo "$var_data" | tail -n +2);
		var_index=$(( RANDOM % ${#var_list[@]} ));
		# Test : lines.
		if [[ ${#var_list[@]} -gt 0 ]]; then
			echo "${var_list[$var_index]}";
		else
			echo "N/A";
		fi;
	elif [[ "$var_path" == *.json ]]; then
		# Loop : Process Substitution.
		while read -r var_data_distro; do
			var_data_distro_name=$(echo "$var_data_distro" | jq -c -r '.name');
			var_data_distro_release=$(echo "$var_data_distro" | jq -c -r '.releases.latest.version');
			var_data_distro_release_date=$(echo "$var_data_distro" | jq -c -r '.releases.latest.date');
			var_list+=("${var_data_distro_name} - ${var_data_distro_release} (${var_data_distro_release_date})");
		done < <(echo "$var_data" | jq -c -r '.distro[]');
		var_index=$(( RANDOM % ${#var_list[@]} ));
		# Test : lines.
		if [[ ${#var_list[@]} -gt 0 ]]; then
			echo "${var_list[$var_index]}";
		else
			echo "N/A";
		fi;
	elif [[ "$var_path" == *.yaml ]]; then
		# Loop : Process Substitution distribution.
		while read -r var_data_distro; do
			var_data_distro_name=$(echo "$var_data" | yq eval ".distro[] | select(.name == \"$var_data_distro\")");
			var_data_distro_release=$(echo "$var_data_distro_name" | yq eval '.releases.latest.version');
			var_data_distro_release_date=$(echo "$var_data_distro_name" | yq eval '.releases.latest.date');
			var_list+=("${var_data_distro_name} - ${var_data_distro_release} (${var_data_distro_release_date})");
		done < <(echo "$var_data" | yq '.distro[].name');
		var_index=$(( RANDOM % ${#var_list[@]} ));
		# Test : lines.
		if [[ ${#var_list[@]} -gt 0 ]]; then
			echo "${var_list[$var_index]}";
		else
			echo "N/A";
		fi;
	else
		echo "N/A";
	fi;
}

# Set_LocalDistroReporter.
#	DESCRIPTION : set distro-reporter data.
function Set_LocalDistroReporter() {
	# Some definition.
	local var_url;
	local var_extension;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-URL) var_url="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	var_extension="$(echo "$var_url" | rev | cut -d '.' -f 1 | rev)";
	wget --no-check-certificate --timeout=10 --tries=2 -O "${CONF[PATH]}/distro-reporter.${var_extension}" "$var_url";
}

# Get_LocalDistroReporter.
#	DESCRIPTION : get distro-reporter data.
function Get_LocalDistroReporter() {
	# Some definition.
	local var_path;
	local var_data;
	local var_data_revision;
	local var_data_revision_formatted;
	local var_data_distro;
	local var_data_distro_name;
	local var_data_distro_release;
	local var_list;
	# Loop : arguments.
	while [[ "$#" -gt 0 ]]; do
		case "$1" in
			-PATH) var_path="$2"; shift 2 ;;
			*) shift ;;
		esac
	done;
	# Data.
	var_data="$(<"$var_path")";
	var_list="";
	# Tests : JSON, YAML.
	if [[ "$var_path" == *.json ]]; then
		var_data_revision=$(echo "$var_data" | jq -c -r '.revision');
		var_data_revision_formatted=$(date -d "${var_data_revision}" "+%A %d %B - %Hh%M");
		# Loop : Process Substitution distribution.
		while read -r var_data_distro; do
			var_data_distro_name=$(echo "$var_data_distro" | jq -c -r '.name');
			var_data_distro_release=$(echo "$var_data_distro" | jq -c -r '.version.new');
			var_list+="${var_data_distro_name} (${var_data_distro_release}), ";
		done < <(echo "$var_data" | jq -c -r '.distro[]');
		# Test : line.
		if [[ -n "$var_list" ]]; then
			var_list="${var_list%, }.";
			echo "${var_list} | ${var_data_revision_formatted}";
		else
			echo "N/A | ${var_data_revision_formatted}";
		fi;
	elif [[ "$var_path" == *.yaml ]]; then
		var_data_revision=$(echo "$var_data" | yq eval '.revision');
		var_data_revision_formatted=$(date -d "${var_data_revision}" "+%A %d %B - %Hh%M");
		# Loop : Process Substitution distribution.
		while read -r var_data_distro; do
			var_data_distro_name=$(echo "$var_data" | yq eval ".distro[] | select(.name == \"$var_data_distro\")");
			var_data_distro_release=$(echo "$var_data_distro_name" | yq eval '.version.new');
			var_list+="${var_data_distro_name} (${var_data_distro_release}), ";
		done < <(echo "$var_data" | yq '.distro[].name');
		# Test : line.
		if [[ -n "$var_list" ]]; then
			var_list="${var_list%, }.";
			echo "${var_list} | ${var_data_revision_formatted}";
		else
			echo "N/A | ${var_data_revision_formatted}";
		fi;
	else
		echo "N/A";
	fi;
}

# Start_App.
#	DESCRIPTION : starting calls by the APP.
function Start_App() {
	# Tests : download, read.
	if [[ "$var_param_action" == "download" ]]; then
		# Tests : tracker, reporter.
		if [[ "$var_param_type" == "tracker" ]]; then
			Set_LocalDistroTracker -URL "$var_param_source";
		elif [[ "$var_param_type" == "reporter" ]]; then
			Set_LocalDistroReporter -URL "$var_param_source";
		fi;
	elif [[ "$var_param_action" == "read" ]]; then
		# Tests : tracker, reporter.
		if [[ "$var_param_type" == "tracker" ]]; then
			Get_LocalDistroTracker -PATH "$var_param_source";
		elif [[ "$var_param_type" == "reporter" ]]; then
			Get_LocalDistroReporter -PATH "$var_param_source";
		fi;
	fi;
}


# ======================= #
#          CALLS          #

# Let's go.
Start_App;