#  ____________________________________________
# |                                            | #
# |               convention.ps1               | #
# |____________________________________________| #



# First of all.
Param(
	$EXAMPLE1,
	$EXAMPLE2
)



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                         @@ #
# @@               APPLICATION               @@ #
# @@                                         @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes all to define the APP.

$APPLICATION = @{};
$APPLICATION["NAME"] = "CONVENTION";
$APPLICATION["DESCRIPTION"] = "my convention";
$APPLICATION["VERSION"] = "1";
$APPLICATION["AUTHOR"] = "Frédéric Petit <contact@fredericpetit.fr>";
$APPLICATION["LICENSE"] = "CC BY-SA 4.0";
$APPLICATION["URL"] = "https://gitlab.com/fredericpetit/convention.git";



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                           @@ #
# @@               CONFIGURATION               @@ #
# @@                                           @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes all fix configuration of the APP.

$CONFIGURATION = @{};
$CONFIGURATION["PATH"] = "$PSScriptRoot";
$CONFIGURATION["PATH_SOURCE"] = $CONFIGURATION["PATH"] + "\source\windows";
# loop includes.
Get-ChildItem $CONFIGURATION["PATH_SOURCE"] -Filter "func_*.ps1" | Foreach-Object { . "$($CONFIGURATION.PATH_SOURCE)\$_"; }
$CONFIGURATION["DATE"] = "";



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                      @@ #
# @@               LANGUAGE               @@ #
# @@                                      @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes all language files, by choice.

$LC = Import-LocalizedData -BaseDirectory (Join-Path -Path "$($CONFIGURATION.PATH)" -ChildPath "locale");



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                   @@ #
# @@               USAGE               @@ #
# @@                                   @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes the MAN of the APP.

function Get-Usage() { Write-Host "$($LC.msg_usage -f $($APPLICATION.NAME), $($APPLICATION.DESCRIPTION), $($APPLICATION.AUTHOR), $($APPLICATION.VERSION), $($APPLICATION.LICENSE))"; }



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                        @@ #
# @@               PARAMETERS               @@ #
# @@                                        @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes all redefinable parameters of the APP, set by script arguments.

$PARAMETERS = @{};
$PARAMETERS["EXAMPLE1"] = $EXAMPLE1;
$PARAMETERS["EXAMPLE2"] = $EXAMPLE2;



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                      @@ #
# @@               DEFAULTS               @@ #
# @@                                      @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes all fixed defaults parameters of the APP.

$DEFAULTS = @{};
$DEFAULTS["EXAMPLE1"] = "DEFAULT1";
$DEFAULTS["EXAMPLE1"] = "DEFAULT2";



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                         @@ #
# @@               DEFINITIONS               @@ #
# @@                                         @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes all the final parameters of the APP, by choice between redefinable parameters and fixed defaults.

If (!$PARAMETERS["EXAMPLE1"]) { $PARAMETERS["EXAMPLE1"] = $DEFAULTS["EXAMPLE1"]; }
If (!$PARAMETERS["EXAMPLE2"]) { $PARAMETERS["EXAMPLE2"] = $DEFAULTS["EXAMPLE2"]; }



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@                                  @@ #
# @@               INIT               @@ #
# @@                                  @@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# DESCRIPTION : here goes the init of the APP.

Write-Host " OD :: $($LC.msg_rootresult)." -ForegroundColor Green;

Get-Usage;