ConvertFrom-StringData -StringData @'

msg_usage = \nSYNOPSIS\n\t{0} - {1}\n\nREQUIREMENTS\n\tx\n\nUTILISATION\n\tx\n\nOPTIONS\n\tx\n\nPARAMETERS\n\tx\n\nEXAMPLE\n\tx\n\nTO-DO\n\turgent : x\n\tlater : x\n\nNOTES\n\t- enable editor.tabCompletion in VSCode.\n\nCREDITS & THANKS\n\tx\n\nSOURCES\n\tx\n\nMETADATA\n\tAuthor : {2}\n\tVersion : {3}\n\tLicense : {4}\n\t
msg_rootcheck = Thanks to execute this script with Root rights
msg_rootresult = Root rights verified

'@