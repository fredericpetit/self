---
- name: Configuration Nextcloud
  hosts: docker
  become: true
  gather_facts: true
  vars_files:
    - /home/ansible/.ansible/playbooks/group_vars/all.yml

  tasks:
    - name: Vérifier si l'ancien sources.list est présent dans le container
      command: docker exec {{ nextcloud_web_container }} stat /etc/apt/sources.list
      register: old_repository_stat
      failed_when: false
      changed_when: false

    - name: Supprimer l'ancien fichier sources.list si il existe
      command: docker exec -u root {{ nextcloud_web_container }} rm -f /etc/apt/sources.list
      when: old_repository_stat.rc != 0

    - name: Vérifier si les nouveaux dépôts complets sont déjà présents dans le container
      command: docker exec {{ nextcloud_web_container }} stat /etc/apt/sources.list.d/debian.sources
      register: new_repository_stat
      failed_when: false
      changed_when: false

    - name:  Déplacer le fichier debian.sources si il existe
      command: docker exec -u root {{ nextcloud_web_container }} mv /var/tmp/debian.sources /etc/apt/sources.list.d/debian.sources
      when: new_repository_stat.rc != 0

    - name: Mettre à jour le cache APT
      command: docker exec -u root {{ nextcloud_web_container }} apt-get update

    - name: Installations dans le container
      command: docker exec -u root {{ nextcloud_web_container }} apt-get install -y wget dirmngr gnupg2 apt-utils ca-certificates debian-archive-keyring software-properties-common nano tree iproute2

    - name: Créer /root/.gnupg avec permissions
      command: docker exec -u root {{ nextcloud_web_container }} install -d -m 700 -o root -g root /root/.gnupg

    - name: Créer /etc/apt/keyrings avec permissions
      command: docker exec -u root {{ nextcloud_web_container }} install -m 0755 -d /etc/apt/keyrings

    - name: Vérifier si la clé GPG PHP est déjà présente dans le container
      command: docker exec {{ nextcloud_web_container }} stat /etc/apt/trusted.gpg.d/php-archive-keyring.gpg
      register: php_key_stat
      failed_when: false
      changed_when: false

    - name: Télécharger et ajouter la clé GPG de PHP si elle n'est pas présente
      shell: |
        docker exec -u root {{ nextcloud_web_container }} wget https://packages.sury.org/php/apt.gpg -O /tmp/php-archive-keyring.gpg
        docker exec -u root {{ nextcloud_web_container }} gpg --no-tty --dearmor -o /etc/apt/trusted.gpg.d/php-archive-keyring.gpg /tmp/php-archive-keyring.gpg
      when: php_key_stat.rc != 0

    - name: Définir les permissions sur la clé GPG de PHP
      command: docker exec -u root {{ nextcloud_web_container }} chmod 0644 /etc/apt/trusted.gpg.d/php-archive-keyring.gpg
      when: php_key_stat.rc != 0

    - name: Vérifier si le dépôt PHP est déjà présente dans le container
      command: docker exec {{ nextcloud_web_container }} stat /etc/apt/sources.list.d/php.sources
      register: php_repository_stat
      failed_when: false
      changed_when: false

    - name: Déplacer le fichier php.sources
      command: docker exec -u root {{ nextcloud_web_container }} mv /var/tmp/php.sources /etc/apt/sources.list.d/php.sources
      when: php_repository_stat.rc != 0

    - name: Mettre à jour le cache APT
      command: >
        docker exec -u root {{ nextcloud_web_container }} apt-get update

    ####################################################################

    - name: Ajouter un trusted_domain
      command: >
        docker exec -u 33 {{ nextcloud_web_container }} php /var/www/html/occ config:system:set trusted_domains 1 --value=partage.m4i.lan
      register: result

    - name: Ajouter un autre trusted_domain
      command: >
        docker exec -u 33 {{ nextcloud_web_container }} php /var/www/html/occ config:system:set trusted_domains 2 --value=10.51.10.4
      register: result

    - name: Vérifier la configuration des trusted_domains
      command: >
        docker exec -u 33 {{ nextcloud_web_container }} php /var/www/html/occ config:system:get trusted_domains
      register: trusted_domains_result

    - name: Afficher la liste des trusted_domains
      debug:
        msg: "{{ trusted_domains_result.stdout }}"
      when: trusted_domains_result.stdout is defined

    - name: Désactiver la vérification des mots de passe - A FAIRE la désactivation de vérification ne fonctionne pas
      command: >
        docker exec -u 33 {{ nextcloud_web_container }} php /var/www/html/occ config:system:set security.check_compromised_passwords --value="false" --type="bool"

    - name: Supprimer les utilisateurs
      command: >
        docker exec -u 33 {{ nextcloud_web_container }} bash -c 'php /var/www/html/occ user:delete {{ item.username }}'
      loop:
        - { username: "yohan" }
        - { username: "rina" }
        - { username: "metin" }
        - { username: "fred" }
      loop_control:
        label: "{{ item.username }}"

    - name: Ajouter les utilisateurs
      command: >
        docker exec -u 33 {{ nextcloud_web_container }} bash -c 'export OC_PASS={{ item.password }} && php /var/www/html/occ user:add {{ item.username }} --password-from-env --email {{ item.email }}'
      loop:
        - { username: "yohan", email: "yohan@m4i.lan", password: "aze@@@AZE@@@123!!!" }
        - { username: "rina", email: "rina@m4i.lan", password: "aze@@@AZE@@@123!!!" }
        - { username: "metin", email: "metin@m4i.lan", password: "aze@@@AZE@@@123!!!" }
        - { username: "fred", email: "fred@m4i.lan", password: "aze@@@AZE@@@123!!!" }
      loop_control:
        label: "{{ item.username }}"
