# Présentation

Chrony est un serveur NTP pour la synchronisation de l'heure.

# Déploiement playbook de Linux à Windows.

WinRM est plus pertinent que SSH pour Ansible sous Windows car il est natif à Windows, permet une meilleure gestion des sessions, supporte les modules Ansible sans configuration supplémentaire et offre une intégration plus fluide avec PowerShell.

Modifier les paramètres du fichier inventory an ajoutant :

```
[windows:vars]
ansible_user=M4i.lan\Administrateur
ansible_password=XXXXX
ansible_connection=winrm
ansible_winrm_transport=ntlm
ansible_winrm_server_cert_validation=ignore
ansible_remote_tmp=C:\\Windows\\Temp\\ansible
ansible_winrm_port=5985
```

## note.

Les doubles backslashes sont surtout utiles pour les chemins Windows (C:\\Windows\\Temp\\ansible), mais pas pour les noms d'utilisateur avec un domaine (M4i.lan\Administrateur).

# Initialiser la connexion sur le poste Windows Server.

## Activer le service WinRM.
winrm quickconfig

## Configurer WinRM pour accepter les connexions à distance.
Set-Item WSMan:\Localhost\Service\AllowUnencrypted -Value true
Set-Item WSMan:\Localhost\Client\TrustedHosts -Value "*"

## Redémarrer le service WinRM.
Restart-Service WinRM

## Configuration réseau.
New-NetFirewallRule -DisplayName "WinRM HTTP" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5985
New-NetFirewallRule -DisplayName "WinRM HTTPS" -Direction Inbound -Action Allow -Protocol TCP -LocalPort 5986

# Tester la connexion WinRM.

- http: `curl -v http://10.51.10.2:5985/wsman`
- https: `curl -v https://10.51.10.2:5986/wsman`
