#!/bin/bash

# /!\ DEUXIÈME SCRIPT : À réaliser uniquement sur la machine serveur Ansible, idéalement depuis /home/ansible/.ansible/scripts/, par le compte 'ansible'.
# Les comptes 'root' et 'user' sont supposés fonctionnels et avoir pour mot de passe 'root'.
# Si des erreurs apparaissent, vérifiez les informations ci-dessus.
# Définir les machines cibles dans TARGET_HOSTS (juste l'adresse IP du serveur Docker dans le cas le plus classique ici).


# ============================ #
#          PARAMÉTRES          #

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--target|-t)
			PARAM["TARGET"]="$2"
			shift 2
			;;
		*)
			shift
			;;
	esac
done;


# =========================== #
#          VARIABLES          #

# Variables hôte local.
LOCAL_USER="ansible";
SSH_DIR="/home/$LOCAL_USER/.ssh";
KEY_PATH="$SSH_DIR/id_rsa";
# Variables hôte(s) cible(s) pour l'utilisateur non-Ansible.
TARGET_USER_LAMBDA="user";
TARGET_PASSWORD_LAMBDA="root";
# Initialiser le tableau TARGET_HOSTS.
TARGET_HOSTS=();
# Vérifier si PARAM["TARGET"] est défini.
if [[ -n "${PARAM["TARGET"]}" ]]; then
	# Diviser la chaîne PARAM["TARGET"] par des virgules et alimenter TARGET_HOSTS.
	IFS=',' read -r -a addresses <<< "${PARAM["TARGET"]}";
	for address in "${addresses[@]}"; do
		TARGET_HOSTS+=("$address");
	done;
else
	echo "Renseignez au moins une cible.";
	exit;
fi;
# Variables hôte(s) cible(s) pour le service Ansible.
ANSIBLE_USER="ansible";
ANSIBLE_PASSWORD="azeAZE123-";


##### ÉTAPE 1.
echo -e "\n\n===== ÉTAPE 1 : création de l'utilisateur Ansible sur les hôtes distants =====";

for TARGET_HOST in "${TARGET_HOSTS[@]}"; do
	sshpass -p "$TARGET_PASSWORD_LAMBDA" ssh -o StrictHostKeyChecking=no $TARGET_USER_LAMBDA@$TARGET_HOST >/dev/null 2>&1 <<EOF
	echo "$TARGET_PASSWORD_LAMBDA" | su -l root -c "useradd -m -s /bin/bash $ANSIBLE_USER && echo '$ANSIBLE_USER:$ANSIBLE_PASSWORD' | chpasswd" > /dev/null 2>&1;
EOF
	echo "Utilisateur 'ansible@$TARGET_HOST' traité.";
done;


##### ÉTAPE 2.
echo -e "\n\n===== ÉTAPE 2 : création du dossier .ssh local si nécessaire =====";

if [[ ! -d "$SSH_DIR" ]]; then
	mkdir -p "$SSH_DIR"
	chown "$LOCAL_USER:$LOCAL_USER" "$SSH_DIR"
	chmod 700 "$SSH_DIR"
	echo "Dossier '$SSH_DIR' créé."
else
	echo "Dossier '$SSH_DIR' existe déjà."
fi;


##### ÉTAPE 3.
echo -e "\n\n===== ÉTAPE 3 : génération de la clé locale si elle n'existe pas =====";

if [[ ! -f "$KEY_PATH" ]]; then
	sudo -u "$LOCAL_USER" ssh-keygen -t rsa -b 4096 -f "$KEY_PATH" -N "" -q;
	sudo chmod 600 "$KEY_PATH";
	sudo chmod 644 "$KEY_PATH.pub";
	echo "Clé  '$KEY_PATH' créée.";
else
	echo "Clé '$KEY_PATH' existe déjà.";
fi;


##### ÉTAPE 4.
echo -e "\n\n===== ÉTAPE 4 : copie de la clé sur chaque hôte cible - de compte Ansible à compte Ansible, installation de sudo avec su -l root,  ajout de l'utilisateur au groupe sudo avec su -l root =====";

for TARGET_HOST in "${TARGET_HOSTS[@]}"; do
	echo "$ANSIBLE_PASSWORD" | sudo -u $ANSIBLE_USER sshpass ssh-copy-id -o StrictHostKeyChecking=no -i "$KEY_PATH.pub" "$ANSIBLE_USER@$TARGET_HOST" > /dev/null 2>&1;
	echo "'$TARGET_HOST' clé traitée.";
	ssh -i "$KEY_PATH" -o StrictHostKeyChecking=no $ANSIBLE_USER@$TARGET_HOST >/dev/null 2>&1 <<EOF
		echo "$TARGET_PASSWORD_LAMBDA" | su -l root -c "rm /etc/apt/sources.list" > /dev/null 2>&1;
		echo "$TARGET_PASSWORD_LAMBDA" | su -l root -c "cat <<'INNER_EOF' > /etc/apt/sources.list.d/debian.sources
#------------------------------------------------------------------------------#
#                   Debian Bookworm 12 OFFICIAL REPOSITORIES
#------------------------------------------------------------------------------#

### Debian Binary Repositories
X-Repolib-Name: debian
Types: deb
Enabled: yes
URIs: https://deb.debian.org/debian
Suites: bookworm bookworm-updates bookworm-backports
Components: main contrib non-free-firmware non-free

### Debian Source Repositories
X-Repolib-Name: debian-src
Types: deb-src
Enabled: yes
URIs: https://deb.debian.org/debian
Suites: bookworm bookworm-updates bookworm-backports
Components: main contrib non-free-firmware non-free

### Debian Security Repositories
X-Repolib-Name: debian-security
Types: deb
Enabled: yes
URIs: https://security.debian.org/debian-security
Suites: bookworm-security
Components: main contrib non-free-firmware non-free

### Debian Security Source Repositories
X-Repolib-Name: debian-security-src
Types: deb-src
Enabled: yes
URIs: https://security.debian.org/debian-security
Suites: bookworm-security
Components: main contrib non-free-firmware non-free
INNER_EOF" > /dev/null 2>&1;
		echo "$TARGET_PASSWORD_LAMBDA" | su -l root -c "apt-get update -y && apt-get install -y sudo" > /dev/null 2>&1;
		echo "$TARGET_PASSWORD_LAMBDA" | su -l root -c "usermod -aG sudo $ANSIBLE_USER" > /dev/null 2>&1;
		echo "$TARGET_PASSWORD_LAMBDA" | su -l root -c "sed -i \"/%sudo/s/ALL=(ALL:ALL) ALL/ALL=(ALL:ALL) NOPASSWD: ALL/\" /etc/sudoers" > /dev/null 2>&1;
EOF
    echo "Utilisateur '$ANSIBLE_USER' ajouté au groupe sudo et modification de /etc/sudoers sur '$TARGET_HOST'.";
done;