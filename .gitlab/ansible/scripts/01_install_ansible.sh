#!/bin/bash

# /!\ PREMIER SCRIPT : À réaliser uniquement sur la machine serveur Ansible, idéalement dans /root/, par le compte 'root'.
# Les comptes 'root' et 'user' sont supposés fonctionnels et avoir pour mot de passe 'root'.
# Si des erreurs apparaissent, vérifiez les informations ci-dessus.


# Test root.
if [[ $EUID -ne 0 ]]; then echo "Root needed."; exit 1; fi;


# ============================ #
#          PARAMÉTRES          #

declare -gA PARAM;
# Loop : arguments.
while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--network|-n)
			PARAM["NETWORK"]="$2"
			shift 2
			;;
		*)
			shift
			;;
	esac
done;


##### ÉTAPE 1.
echo -e "\n\n===== ÉTAPE 1 : configuration du dépôt officiel Debian =====";

rm /etc/apt/sources.list &> /dev/null;
cat <<EOF | tee /etc/apt/sources.list.d/debian.sources > /dev/null
#------------------------------------------------------------------------------#
#                   Debian Bookworm 12 OFFICIAL REPOSITORIES
#------------------------------------------------------------------------------#

### Debian Binary Repositories
X-Repolib-Name: debian
Types: deb
Enabled: yes
URIs: https://deb.debian.org/debian
Suites: bookworm bookworm-updates bookworm-backports
Components: main contrib non-free-firmware non-free

### Debian Source Repositories
X-Repolib-Name: debian-src
Types: deb-src
Enabled: yes
URIs: https://deb.debian.org/debian
Suites: bookworm bookworm-updates bookworm-backports
Components: main contrib non-free-firmware non-free

### Debian Security Repositories
X-Repolib-Name: debian-security
Types: deb
Enabled: yes
URIs: https://security.debian.org/debian-security
Suites: bookworm-security
Components: main contrib non-free-firmware non-free

### Debian Security Source Repositories
X-Repolib-Name: debian-security-src
Types: deb-src
Enabled: yes
URIs: https://security.debian.org/debian-security
Suites: bookworm-security
Components: main contrib non-free-firmware non-free
EOF


##### ÉTAPE 2.
echo -e "\n\n===== ÉTAPE 2 : installation de sudo nano tree, et activation de sudo sans mot de passe =====";

apt-get update && apt-get install -y sudo nano tree;
cp /etc/sudoers /etc/sudoers.$(date +"%Y-%m-%d_%H-%M-%S").backup;
useradd -m -s /bin/bash ansible;
echo "ansible:azeAZE123-" | chpasswd;
usermod -aG sudo ansible;
if grep -q '%sudo' /etc/sudoers; then
	sed -i '/%sudo/s/ALL=(ALL:ALL) ALL/ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers;
else
	echo '%sudo   ALL=(ALL:ALL) NOPASSWD: ALL' | tee -a /etc/sudoers;
fi;


##### ÉTAPE 3.
echo -e "\n\n===== ÉTAPE 3 : configuration du dépôt Ansible =====";

KEY="6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367";
KEYSERVER="keyserver.ubuntu.com";
apt-get update;
apt-get install -y wget curl apt-utils ca-certificates debian-archive-keyring software-properties-common gnupg2 dirmngr;
if [[ -e "/etc/apt/keyrings/ansible-archive-keyring.gpg" ]]; then
	echo "Clé déjà présente.";
else
	echo "Clé à ajouter : '$KEY' ...";
	install -d -m 700 -o root -g root /root/.gnupg;
	install -m 0755 -d /etc/apt/keyrings;
	gpg --keyserver ${KEYSERVER} --recv-keys ${KEY};
	gpg --export ${KEY} | sudo tee /etc/apt/trusted.gpg.d/ansible-archive-keyring.gpg > /dev/null;
fi;
mkdir -p /etc/apt/sources.list.d;
cat <<EOF | tee /etc/apt/sources.list.d/ansible.sources > /dev/null
### Ansible Repository
Types: deb
URIs: http://ppa.launchpad.net/ansible/ansible/ubuntu
Suites: jammy
Components: main
Signed-By: /etc/apt/trusted.gpg.d/ansible-archive-keyring.gpg
EOF


##### ÉTAPE 4.
echo -e "\n\n===== ÉTAPE 4 : installation d'Ansible =====";

apt-get update && apt-get install -y python3 python3-passlib ansible;


##### ÉTAPE 5.
echo -e "\n\n===== ÉTAPE 5 : création des répertoires et fichiers (à modifier ensuite) =====";

# Définir le chemin de base.
BASE_DIR="/home/ansible/.ansible";
# Tableau contenant les répertoires et fichiers avec leurs permissions.
ANSIBLE_STRUCTURE=(
	"playbooks:700"
	"playbooks/group_vars:700"
	"playbooks/group_vars/all.yaml:600"
	"playbooks/host_vars:700"
	"playbooks/01_secure_ssh.yaml:600"
	"playbooks/02_install_docker.yaml:600"
	"playbooks/03_network.yaml:600"
	"playbooks/04_install_db.yaml:600"
	"playbooks/05_install_accueil.yaml:600"
	"playbooks/100_clean_docker.yaml:600"
	"scripts:700"
	"scripts/02_init_users_ssh.sh:700"
	"tmp:700"
	"confs:700"
	"websites:700"
	"inventory:600"
);
ANSIBLE_STRUCTURE_DMZ=(
	"playbooks/06_install_guacamole.yaml:600"
	"playbooks/07_config_guacamole.yaml:600"
	"playbooks/08_config_zabbix.yaml:600"
	"playbooks/09_install_postfix.yaml:600"
);
ANSIBLE_STRUCTURE_LAN=(
	"playbooks/06_install_proxy.yaml:600"
	"playbooks/07_install_zabbix.yaml:600"
	"playbooks/08_config_zabbix.yaml:600"
	"playbooks/09_install_glpi.yaml:600"
	"playbooks/10_config_glpi.yaml:600"
	"playbooks/11_install_dashboard.yaml:600"
);
# Vérifier la valeur de NETWORK et ajouter la structure appropriée.
if [ "${PARAM[NETWORK]}" == "dmz" ]; then
	ANSIBLE_STRUCTURE+=("${ANSIBLE_STRUCTURE_DMZ[@]}");
else
	ANSIBLE_STRUCTURE+=("${ANSIBLE_STRUCTURE_LAN[@]}");
fi;
# Créer les répertoires et fichiers avec 'install'.
for item in "${ANSIBLE_STRUCTURE[@]}"; do
	path=$(echo "$item" | cut -d':' -f1);
	permissions=$(echo "$item" | cut -d':' -f2);
	if [[ "$path" == *".yaml" || "$path" == *".sh" || "$path" == "inventory" ]]; then
		install -m "$permissions" -o ansible -g ansible /dev/null "$BASE_DIR/$path";
		echo "Fichier $BASE_DIR/$path créé.";
	else
		install -d -m "$permissions" -o ansible -g ansible "$BASE_DIR/$path";
		echo "Dossier $BASE_DIR/$path créé.";
	fi;
done;
echo "Arborescence créée dans $BASE_DIR.";


##### ÉTAPE 6.
echo -e "\n\n===== ÉTAPE 6 : .bashrc & environment =====";
sed -i 's|# alias ll='"'"'ls $LS_OPTIONS -l'"'"'|alias ll='"'"'ls -liaFh'"'"'|' /root/.bashrc;
sed -i 's/#alias ll='"'"'ls -l'"'"'/alias ll='"'"'ls -liaFh'"'"'/' "/home/ansible/.bashrc";
source "/root/.bashrc";
echo "Alias ll mis à jour.";
echo "EDITOR=nano" | tee -a /etc/environment;
source /etc/environment;
echo "EDITOR (pour ansible-vault, notamment) mis à jour.";


##### ÉTAPE 7.
echo -e "\n\n===== ÉTAPE 7 : fixation des droits =====";
chown ansible:ansible /home/ansible/.ansible;
echo "Droits fixés.";