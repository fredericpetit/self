# 2025-03-02
# À répartir dans /scripts et /playbooks puis exécuter dans l'ordre, 
# pour re-construire de manière automatisée une partie des infras LAN & DMZ façon Infra As Code, 
# en mode non-interactif.


# Particularités.
# - un fichier commun /!\ À CRÉER /!\ de variables globales : group_vars/all.yml
# - un fichier commun /!\ À CRÉER /!\ de variables globales protégées avec ansible-vault : group_vars/secrets.yml
# - un fichier commun /!\ À CRÉER /!\ inventory
# - importer les playbooks suivant LAN ou DMZ
# - chaque container DB a son propre port, 3305 est celui de Adminer (DB manager)
# - reconfiguration du compte "Admin" zabbix en login "Administrateur" et nouveau mot de passe via requêtage
# - tous les volumes sont nommés


# ANSIBLE VAULT.
# Pour éditer facilement : export EDITOR=nano
# Le fichier secrets.yaml est protégé, pour exécuter un playbook contenant un mot de passe, faire par exemple
# ansible-playbook -i /home/ansible/.ansible/inventory /home/ansible/.ansible/playbooks/08_install_zabbix.yaml --ask-vault-pass


# À faire.
# pré-configuration GLPI langue, pré-requis
# SWARM


# Info - debug - obtenir un token JSON via l'API zabbix.
# curl -X POST http://adresse_ip/api_jsonrpc.php \-H "Content-Type: application/json" -d '{"jsonrpc": "2.0", "method": "user.login", "params": { "username": "Admin", "password": "zabbix" }, "id": 1}'


# Info - debug - vider une table SQL lite.
# sqlite3 /data/database.sqlite "DELETE FROM proxy_host;"


# Scripts communs.
# Exécuter le premier avec le compte 'root', puis le second avec le compte 'ansible'.
# Installation d'Ansible sur le serveur dédié - variable network pour le réseau voulu.
wget --no-use-server-timestamps --timeout=10 --tries=2 https://gitlab.com/fredericpetit/self/-/raw/main/.gitlab/ansible/scripts/01_install_ansible.sh -O /root/01_install_ansible.sh && chmod +x /root/01_install_ansible.sh
./01_install_ansible.sh --network dmz
OU
./01_install_ansible.sh --network lan
# Configuration des utilisateurs sur les cibles - variable target pour les cibles voulues.
wget --no-use-server-timestamps --timeout=10 --tries=2 https://gitlab.com/fredericpetit/self/-/raw/main/.gitlab/ansible/scripts/02_init_users_ssh.sh -O /home/ansible/.ansible/scripts/02_init_users_ssh.sh && chmod +x /home/ansible/.ansible/scripts/02_init_users_ssh.sh
./02_init_users_ssh.sh --target 192.168.122.201,192.168.122.202


# Playbooks communs.
ansible-playbook -i /home/ansible/.ansible/inventory /home/ansible/.ansible/playbooks/01_secure_ssh.yaml
ansible-playbook -i /home/ansible/.ansible/inventory /home/ansible/.ansible/playbooks/02_install_docker.yaml
ansible-playbook -i /home/ansible/.ansible/inventory /home/ansible/.ansible/playbooks/100_clean_docker.yaml
ansible-playbook -i /home/ansible/.ansible/inventory /home/ansible/.ansible/playbooks/100_info_docker.yaml
ansible-playbook -i /home/ansible/.ansible/inventory /home/ansible/.ansible/playbooks/100_update_docker.yaml


# Playbooks LAN.
# Zabbix : attendre la fin de l'installation avant de lancer la configuration.
# GLPI : exécuter l'installation web avant de lancer la configuration.


# Playbooks DMZ.