#!/bin/bash

# Test root.
if [[ $EUID -ne 0 ]]; then echo "Root needed."; exit 1; fi;

# Update package list and install necessary packages.
apt-get update && apt-get install -y curl apt-utils ca-certificates debian-archive-keyring software-properties-common gnupg2 dirmngr;

# Create directory for APT keyrings.
install -m 0755 -d /etc/apt/keyrings;

# Download and dearmor the AnyDesk GPG key.
curl -fsSL https://keys.anydesk.com/repos/DEB-GPG-KEY | gpg --dearmor -o /etc/apt/keyrings/anydesk-archive-keyring.gpg;

# Add the AnyDesk apt repository using deb822 format.
cat <<EOF | tee /etc/apt/sources.list.d/anydesk.sources
Types: deb
URIs: https://deb.anydesk.com
Suites: all
Components: main
Signed-By: /etc/apt/keyrings/anydesk-archive-keyring.gpg
EOF

# Update apt caches and install the AnyDesk client.
apt-get update && apt-get install -y anydesk;