#!/bin/bash

if ! command -v docker &> /dev/null && command -v sudo &> /dev/null; then
	sudo apt update && \
	sudo apt install -y git curl apt-utils ca-certificates debian-archive-keyring software-properties-common && \
	sudo install -m 0755 -d /etc/apt/keyrings && \
	curl -fsSL https://download.docker.com/linux/$(. /etc/os-release && echo $ID)/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
	sudo chmod a+r /etc/apt/keyrings/docker.gpg && \
	echo -e "Types: deb\nURIs: https://download.docker.com/linux/$(. /etc/os-release && echo $ID)\nSuites: $(lsb_release -cs)\nComponents: stable\nArchitectures: amd64\nSigned-By: /etc/apt/keyrings/docker.gpg" | sudo tee /etc/apt/sources.list.d/docker.sources && \
	sudo apt update && \
	sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin && \
	sudo usermod -aG docker $USER;
fi

read -rsp "Enter your token : " TOKEN;
echo

read -rp "Choose an option (all, basic, node, ruby, dashboard) [default : all] : " CHOICE;
CHOICE=${CHOICE:-all};

if [[ ! "$CHOICE" =~ ^(all|basic|node|ruby|dashboard)$ ]]; then
	echo "Invalid choice. Defaulting to 'all'.";
	CHOICE="all";
fi

echo "$TOKEN" | docker login -u fredericpetit --password-stdin;

if [[ $? -eq 0 ]]; then
	echo "Docker login successful !";
	echo "Selected option : '$CHOICE'.";
else
	echo "Docker login failed !";
	exit 1;
fi;

if [[ "$CHOICE" == "all" || "$CHOICE" == "basic" ]]; then
	docker builder prune -f;
	mkdir build;
	wget --no-use-server-timestamps --timeout=10 --tries=2 -O ./build/Dockerfile https://gitlab.com/fredericpetit/self/-/raw/main/.gitlab/docker/Dockerfile-debian-gitlab-basic && \
	cd build && docker build --no-cache -t debian-gitlab:basic . && \
	docker tag debian-gitlab:basic fredericpetit/debian-gitlab:basic && docker push fredericpetit/debian-gitlab:basic && \
	docker rmi debian-gitlab:basic
	rm -Rf ./build;
fi;

if [[ "$CHOICE" == "all" || "$CHOICE" == "node" ]]; then
	docker builder prune -f;
	mkdir build;
	wget --no-use-server-timestamps --timeout=10 --tries=2 -O ./build/Dockerfile https://gitlab.com/fredericpetit/self/-/raw/main/.gitlab/docker/Dockerfile-debian-gitlab-node && \
	cd build && docker build --no-cache -t debian-gitlab:node . && \
	docker tag debian-gitlab:node fredericpetit/debian-gitlab:node && docker push fredericpetit/debian-gitlab:node && \
	docker rmi debian-gitlab:node;
	rm -Rf ./build;
fi;

if [[ "$CHOICE" == "all" || "$CHOICE" == "ruby" ]]; then
	docker builder prune -f;
	mkdir build;
	wget --no-use-server-timestamps --timeout=10 --tries=2 -O ./build/Dockerfile https://gitlab.com/fredericpetit/self/-/raw/main/.gitlab/docker/Dockerfile-debian-gitlab-ruby && \
	wget --no-use-server-timestamps --timeout=10 --tries=2 -O ./build/Gemfile https://gitlab.com/fredericpetit/self/-/raw/main/.gitlab/ruby/Gemfile && \
	cd build && docker build --no-cache -t debian-gitlab:ruby . && \
	docker tag debian-gitlab:ruby fredericpetit/debian-gitlab:ruby && docker push fredericpetit/debian-gitlab:ruby && \
	docker rmi debian-gitlab:ruby;
	rm -Rf ./build;
fi;

if [[ "$CHOICE" == "all" || "$CHOICE" == "dashboard" ]]; then
	docker builder prune -f;
	mkdir build;
	wget --no-use-server-timestamps --timeout=10 --tries=2 -O ./build/Dockerfile https://gitlab.com/fredericpetit/self/-/raw/main/org/node/distro-dashboard/Dockerfile && \
	cd build && docker build --no-cache -t distro-dashboard:latest . && \
	docker tag distro-dashboard:latest fredericpetit/distro-dashboard:latest && docker push fredericpetit/distro-dashboard:latest && \
	docker rmi distro-dashboard:latest;
	rm -Rf ./build;
fi;