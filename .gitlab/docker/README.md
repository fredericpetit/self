# Docker (on Ubuntu 24.10).

## Installation.

```
sudo apt update && \
sudo apt install -y git curl apt-utils ca-certificates debian-archive-keyring software-properties-common && \
sudo install -m 0755 -d /etc/apt/keyrings && \
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release && echo $ID)/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
sudo chmod a+r /etc/apt/keyrings/docker.gpg && \
echo -e "Types: deb\nURIs: https://download.docker.com/linux/$(. /etc/os-release && echo $ID)\nSuites: $(lsb_release -cs)\nComponents: stable\nArchitectures: amd64\nSigned-By: /etc/apt/keyrings/docker.gpg" | sudo tee /etc/apt/sources.list.d/docker.sources && \
sudo apt update && \
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin && \
sudo usermod -aG docker $USER;
```

## Remove all containers and images.

- `docker rm -f $(docker ps -aq); docker rmi -f $(docker images -aq);`

## Create images.

### debian-gitlab:basic, debian-gitlab:node, debian-gitlab:ruby & distro-dashboard:latest.

```
wget --no-use-server-timestamps --timeout=10 --tries=2 -O ./build.sh https://gitlab.com/fredericpetit/self/-/raw/main/.gitlab/docker/build.sh && \
  chmod +x ./build.sh && \
  ./build.sh
```

### Tests - open a new container of an image in non-persistent mode.

#### debian-gitlab:basic.

- `docker run -it fredericpetit/debian-gitlab:basic /bin/bash`

#### debian-gitlab:node.

- `docker run -it fredericpetit/debian-gitlab:node /bin/bash`

#### debian-gitlab:ruby.

- `docker run -it fredericpetit/debian-gitlab:ruby /bin/bash`