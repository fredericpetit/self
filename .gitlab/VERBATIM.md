# VERBATIM.

`Model : 2023-04-13-self.`

| param | value |
| ------ | ------ |
| **license** | Choose license from [spdx.org/licenses](https://spdx.org/licenses/). |
| **tokens** | Create private token `GITLAB_TOKEN` with rights `api`, `write_repository`, `write_registry`. |
| **CI/CD variables** | Add `CUSTOM_EMAIL` (Protected, Masked, Expanded), `CUSTOM_NAME` (Protected, Expanded) and `GITLAB_TOKEN` (Protected, Masked, Expanded). |
| **CI/CD files** | Create _./.gitlab/default-ci.yml_ & _./.gitlab/extend-ci.yml_, based on '_Self_' home, and _./gitlab-ci.yml_. |
| **Composer** | x |
| **pipeline trigger** | Actions "build" / "track" / "test" / "build-track" / "build-test" / "build-track-test" keywords in commit. Source "web" do "build-track-test" action. |
| **pipeline schedule** | "distro-tracker" do "track" action 11h00 everyday. |

## Rights.
- adapt some visibility rights (web access releases for example).

## Screenshots.
For terminal output :
```
wget -O ~/.config/powerline.bash https://gitlab.com/bersace/powerline-bash/raw/master/powerline.bash
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/NerdFontsSymbolsOnly.zip
unzip NerdFontsSymbolsOnly.zip -d NerdFontsSymbolsOnly
mkdir -p ~/.fonts
cp NerdFontsSymbolsOnly/*.ttf ~/.fonts/
fc-cache -f -v
```

~/.bash_aliases :
```
POWERLINE_ICONS=nerd-fonts

declare -A POWERLINE_ICONS_OVERRIDES
POWERLINE_ICONS_OVERRIDES=(
	[ubuntu]=$'\ue712'
)

. ${HOME}/.config/powerline.bash

POWERLINE_SEGMENTS="logo hostname ${POWERLINE_SEGMENTS}"
PROMPT_COMMAND='__update_ps1 $?'
```

On Ubuntu, in a gtk.css file :
```
decoration, decoration:backdrop, .csd.popup decoration, .fullscreen decoration, .maximized decoration, .tiled decoration, .tiled decoration:backdrop {box-shadow: none; border: 1px solid silver;}
window, window.background, window:backdrop, .csd.popup window, .fullscreen window, .maximized window, .tiled window, .tiled window:backdrop {box-shadow: none; border: 1px solid silver;}
```