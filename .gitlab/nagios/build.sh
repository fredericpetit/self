#!/bin/bash

# Pré-requis.
apt-get update;
apt-get install -y autoconf gcc libc6 make wget unzip apache2 apache2-utils php libgd-dev;
apt-get install -y openssl libssl-dev;
apt-get install -y autoconf gcc libc6 libmcrypt-dev make libssl-dev wget bc gawk dc build-essential snmp libnet-snmp-perl gettext;

# DL Core.
cd /tmp;
wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.5.9.tar.gz;
tar xzf nagioscore.tar.gz;

# DL Plugins.
cd /tmp;
wget --no-check-certificate -O nagios-plugins.tar.gz https://github.com/nagios-plugins/nagios-plugins/archive/release-2.4.12.tar.gz;
tar zxf nagios-plugins.tar.gz;

# Build Core.
cd /tmp/nagioscore-nagios-4.5.9/;
./configure --with-httpd-conf=/etc/apache2/sites-enabled;
make all;
make install-groups-users;
usermod -a -G nagios www-data;
make install;
make install-daemoninit;
make install-commandmode;
make install-config;
make install-webconf;
a2enmod rewrite;
a2enmod cgi;

# Accés Core.
iptables -I INPUT -p tcp --destination-port 80 -j ACCEPT;
apt-get install -y iptables-persistent;
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin;

# Services Core.
systemctl restart apache2.service;
systemctl start nagios.service;

# Build Plugins.
cd /tmp/nagios-plugins-release-2.4.12/;
./tools/setup;
./configure;
make;
make install;

# Service Plugins.
systemctl start nagios.service;
systemctl stop nagios.service
systemctl restart nagios.service;
systemctl status nagios.service;