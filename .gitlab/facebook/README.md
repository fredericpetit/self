# Facebook API.

1) Type d'app Entreprise.

2) Générer un token d'accès utilisateur court terme (app DT - user Token user) sur https://developers.facebook.com/tools/explorer/, avec les droits suivants :

- `pages_manage_metadata`
- `pages_manage_posts`
- `pages_read_engagement`
- `pages_show_list`

3) Échanger le token court terme contre un token longue durée : `curl -X GET "https://graph.facebook.com/v21.0/oauth/access_token?grant_type=fb_exchange_token&client_id=YOUR_APP_ID&client_secret=YOUR_APP_SECRET&fb_exchange_token=SHORT_LIVED_TOKEN"`


4) Récupérer le token longue durée et l'utiliser pour obtenir le token d'accès page : `curl -X GET "https://graph.facebook.com/v21.0/me/accounts?access_token=LONG_LIVED_USER_ACCESS_TOKEN"`