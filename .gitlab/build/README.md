# Building.

## Snapcraft (on Ubuntu 24.04).

### Install.
```
sudo snap refresh; \
sudo snap install lxd; \
sudo lxd init; \
sudo usermod -aG lxd $USER; \
sudo snap install snapcraft --classic; \
sudo apt-get update && sudo systemctl stop unattended-upgrades; \
sudo apt-get install -y git
```
### distro-tracker.

#### Prepare.
```
mkdir self; \
mkdir -p distro-tracker/usr/bin; \
mkdir -p distro-tracker/usr/share/man/man1/; \
git clone https://gitlab.com/fredericpetit/self.git self; \
mv ./self/org/bash/distro-tracker/prod/bin/1.1.1/distro-tracker distro-tracker/usr/bin/; \
mv ./self/org/bash/distro-tracker/dev/man/distro-tracker.1.gz distro-tracker/usr/share/man/man1/; \
mv ./self/org/bash/distro-tracker/prod/package/snapcraft/snapcraft.yaml distro-tracker/snapcraft.yaml; \
rm -Rf self/; \
cd distro-tracker/
```

#### Build.
```
snapcraft
sudo snap install distro-tracker_1.1.1_amd64.snap --dangerous
snapcraft login
snapcraft upload --release=stable distro-tracker_1.1.1_amd64.snap
```

### distro-reporter.

#### Prepare.
```
mkdir self; \
mkdir -p distro-reporter/usr/bin; \
mkdir -p distro-reporter/usr/share/man/man1/; \
git clone https://gitlab.com/fredericpetit/self.git self; \
mv ./self/org/bash/distro-reporter/prod/bin/1.0.0/distro-reporter distro-reporter/usr/bin/; \
mv ./self/org/bash/distro-reporter/dev/man/distro-reporter.1.gz distro-reporter/usr/share/man/man1/; \
mv ./self/org/bash/distro-reporter/prod/package/snapcraft/snapcraft.yaml distro-reporter/snapcraft.yaml; \
rm -Rf self/; \
cd distro-reporter/
```

#### Build.
```
snapcraft
sudo snap install distro-reporter_1.0.0_amd64.snap --dangerous
snapcraft login
snapcraft upload --release=stable distro-reporter_1.0.0_amd64.snap
```

## Flatpak distro-tracker (on Ubuntu 24.04).

### Install.
```
sudo apt-get update && sudo systemctl stop unattended-upgrades && sudo systemctl daemon-reload; \
sudo apt install -y git flatpak flatpak-builder; \
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo; \
sudo flatpak install --assumeyes flathub org.freedesktop.Platform//23.08; \
sudo flatpak install --assumeyes flathub org.freedesktop.Sdk//23.08
sudo flatpak install --assumeyes flathub org.flatpak.Builder
```

### Prepare.
```
mkdir self; \
mkdir -p distro-tracker/bin; \
git clone https://gitlab.com/fredericpetit/self.git self; \
mv self/src/bash/bin/distro-tracker distro-tracker/bin/; \
mv self/src/bash/package/flatpak/com.distrotracker.Distro-Tracker.appdata.xml distro-tracker/com.distrotracker.Distro-Tracker.appdata.xml; \
mv self/src/bash/package/flatpak/com.distrotracker.Distro-Tracker.json distro-tracker/com.distrotracker.Distro-Tracker.json; \
rm -Rf self/;
```

### Build.
```
cd distro-tracker/; \
flatpak-builder --verbose --force-clean --repo=flatpak-repo build-dir com.distrotracker.Distro-Tracker.json;
```
- reboot.

### Run.
```
cd distro-tracker/; \
flatpak remote-add --user --no-gpg-verify flatpak-repo flatpak-repo; \
flatpak uninstall --assumeyes --user com.distrotracker.Distro-Tracker; \
flatpak install --assumeyes --user flatpak-repo com.distrotracker.Distro-Tracker; \
flatpak run com.distrotracker.Distro-Tracker
```

### Test.
```
flatpak run --command=flatpak-builder-lint org.flatpak.Builder manifest com.distrotracker.Distro-Tracker.json; \
flatpak run --command=flatpak-builder-lint org.flatpak.Builder repo flatpak-repo;
```

### Publish.
```
git clone --branch=new-pr git@github.com:your_github_username/flathub.git && cd flathub
```